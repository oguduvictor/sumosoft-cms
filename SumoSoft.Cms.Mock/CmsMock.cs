﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using Moq;
using SumoSoft.Cms.Constants;
using SumoSoft.Cms.DataAccess;
using SumoSoft.Cms.Domain;
using SumoSoft.Cms.Mock.Dtos;
using SumoSoft.Cms.Repositories.Contracts;
using SumoSoft.Cms.Services.Contracts;

#pragma warning disable 1591

namespace SumoSoft.Cms.Mock
{
    public class CmsMock
    {
        private readonly ICmsService _cmsService;
        private readonly ICountryRepository _countryRepository;

        public CmsMock(
            ICmsService cmsService, ICountryRepository countryRepository)
        {
            _cmsService = cmsService;
            _countryRepository = countryRepository;
        }

        public Email Email(string emailName, string emailViewName)
        {
            var email = new Email();
            var emailLocalizedKit = new EmailLocalizedKit();

            email.Name = emailName;
            email.ViewName = emailViewName;
            email.EmailLocalizedKits = new List<EmailLocalizedKit> { emailLocalizedKit };

            //emailLocalizedKit.Country = _countryService.GetCurrentCountry(dbContext);
            emailLocalizedKit.Content = "<p>Email Content</p>";
            emailLocalizedKit.Subject = "Email Subject";

            return email;
        }

        /// <summary>
        /// Instantiates a new Country and sets its LanguageCode as the CurrentCulture of the Thread
        /// </summary>
        public static Country CurrentCountry()
        {
            const string languageCode = "en-GB";

            Thread.CurrentThread.CurrentCulture = new CultureInfo(languageCode);

            var country = new Country();

            country.Id = Guid.NewGuid();
            country.LanguageCode = languageCode;
            country.Name = "United Kingdom";

            return country;
        }

        /// <summary>
        /// Instantiates a new User and sets it as the authenticated User in HttpContext.Current
        /// </summary>
        public static User AuthenticatedUser()
        {
            var user = new User();
            var userRole = new UserRole();

            user.Id = Guid.NewGuid();
            user.Role = userRole;
            user.Email = "authenticated.user@email.com";
            user.Password = "password";

            userRole.Name = SharedConstants.DefaultUserRole;

            if (HttpContext.Current == null) HttpContext.Current = new HttpContext(new HttpRequest(null, "http://test.com", null), new HttpResponse(new StringWriter()));

            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(user.Id.ToString()), new string[0]);

            return user;
        }

        /// <summary>
        /// Instantiates a new User and sets it as the authenticated User in HttpContext.Current
        /// </summary>
        public static User PartialUser()
        {
            var user = new User();
            var userRole = new UserRole();

            user.Id = Guid.NewGuid();
            user.Role = userRole;

            userRole.Name = SharedConstants.DefaultUserRole;

            SetCookie(SharedConstants.PartialUserCookie, user.Id.ToString());

            return user;
        }

        /// <summary>
        /// Stores an in-memory Cookie in the HttpContext.Current.Response
        /// </summary>
        public static void SetCookie(string name, string value)
        {
            if (HttpContext.Current == null) HttpContext.Current = new HttpContext(new HttpRequest(null, "http://test.com", null), new HttpResponse(new StringWriter()));

            HttpContext.Current.Request.Cookies.Add(new HttpCookie(name, value));
        }

        public static DbSet<T> DbSet<T>(List<T> entities) where T : class
        {
            var dbSet = new Mock<DbSet<T>>();

            dbSet.As<IQueryable<T>>().Setup(q => q.Provider).Returns(() => entities.AsQueryable().Provider);
            dbSet.As<IQueryable<T>>().Setup(q => q.Expression).Returns(() => entities.AsQueryable().Expression);
            dbSet.As<IQueryable<T>>().Setup(q => q.ElementType).Returns(() => entities.AsQueryable().ElementType);
            dbSet.As<IQueryable<T>>().Setup(q => q.GetEnumerator()).Returns(() => entities.AsQueryable().GetEnumerator());
            dbSet.Setup(x => x.AsNoTracking()).Returns(dbSet.Object);
            dbSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => entities.FirstOrDefault(d => ids[0] != null && d != null && new Guid(d.GetType().GetProperty("Id").GetValue(d, null).ToString()) == (Guid)ids[0]));
            dbSet.Setup(set => set.Add(It.IsAny<T>())).Callback<T>(entities.Add);
            dbSet.Setup(set => set.AddRange(It.IsAny<IEnumerable<T>>())).Callback<IEnumerable<T>>(entities.AddRange);
            dbSet.Setup(set => set.Remove(It.IsAny<T>())).Callback<T>(t => entities.Remove(t));
            dbSet.Setup(set => set.RemoveRange(It.IsAny<IEnumerable<T>>())).Callback<IEnumerable<T>>(ts =>
            {
                foreach (var t in ts)
                {
                    entities.Remove(t);
                }
            });

            return dbSet.Object;
        }

        public static CmsDbContext DbContext(DbSetsDto dbSets)
        {
            var dbContextMock = new Mock<CmsDbContext>();

            dbContextMock.Setup(x => x.Addresses).Returns(DbSet(dbSets.Addresses));
            dbContextMock.Setup(x => x.Countries).Returns(DbSet(dbSets.Countries));
            dbContextMock.Setup(x => x.EcommerceCoupons).Returns(DbSet(dbSets.EcommerceCoupons));
            dbContextMock.Setup(x => x.ContentSections).Returns(DbSet(dbSets.ContentSections));
            dbContextMock.Setup(x => x.ContentSectionLocalizedKits).Returns(DbSet(dbSets.ContentSectionLocalizedKits));
            dbContextMock.Setup(x => x.SeoSections).Returns(DbSet(dbSets.SeoSections));
            dbContextMock.Setup(x => x.SeoSectionLocalizedKits).Returns(DbSet(dbSets.SeoSectionLocalizedKits));
            dbContextMock.Setup(x => x.BlogCategories).Returns(DbSet(dbSets.BlogCategories));
            dbContextMock.Setup(x => x.BlogComments).Returns(DbSet(dbSets.BlogComments));
            dbContextMock.Setup(x => x.BlogPosts).Returns(DbSet(dbSets.BlogPosts));
            dbContextMock.Setup(x => x.EcommerceAttributes).Returns(DbSet(dbSets.EcommerceAttributes));
            dbContextMock.Setup(x => x.EcommerceAttributeOptions).Returns(DbSet(dbSets.EcommerceAttributeOptions));
            dbContextMock.Setup(x => x.EcommerceAttributeLocalizedKits).Returns(DbSet(dbSets.EcommerceAttributeLocalizedKits));
            dbContextMock.Setup(x => x.EcommerceCategories).Returns(DbSet(dbSets.EcommerceCategories));
            dbContextMock.Setup(x => x.EcommerceCategoryLocalizedKits).Returns(DbSet(dbSets.EcommerceCategoryLocalizedKits));
            dbContextMock.Setup(x => x.EcommerceCarts).Returns(DbSet(dbSets.EcommerceCarts));
            dbContextMock.Setup(x => x.EcommerceCartItems).Returns(DbSet(dbSets.EcommerceCartItems));
            dbContextMock.Setup(x => x.EcommerceCartItemVariants).Returns(DbSet(dbSets.EcommerceCartItemVariants));
            dbContextMock.Setup(x => x.EcommerceCartShippingBoxes).Returns(DbSet(dbSets.EcommerceCartShippingBoxes));
            dbContextMock.Setup(x => x.EcommerceOrderItemProductVariants).Returns(DbSet(dbSets.EcommerceOrderItemProductVariants));
            dbContextMock.Setup(x => x.EcommerceOrderAddresses).Returns(DbSet(dbSets.EcommerceOrderAddresses));
            dbContextMock.Setup(x => x.EcommerceOrders).Returns(DbSet(dbSets.EcommerceOrders));
            dbContextMock.Setup(x => x.EcommerceOrderAddresses).Returns(DbSet(dbSets.EcommerceOrderAddresses));
            dbContextMock.Setup(x => x.EcommerceOrderItems).Returns(DbSet(dbSets.EcommerceOrderItems));
            dbContextMock.Setup(x => x.EcommerceOrderItemProductVariants).Returns(DbSet(dbSets.EcommerceOrderItemProductVariants));
            dbContextMock.Setup(x => x.EcommerceOrderCoupons).Returns(DbSet(dbSets.EcommerceOrderCoupons));
            dbContextMock.Setup(x => x.EcommerceOrderCredits).Returns(DbSet(dbSets.EcommerceOrderCredits));
            dbContextMock.Setup(x => x.EcommerceProducts).Returns(DbSet(dbSets.EcommerceProducts));
            dbContextMock.Setup(x => x.EcommerceProductAttributes).Returns(DbSet(dbSets.EcommerceProductAttributes));
            dbContextMock.Setup(x => x.EcommerceProductVariants).Returns(DbSet(dbSets.EcommerceProductVariants));
            dbContextMock.Setup(x => x.EcommerceProductLocalizedKits).Returns(DbSet(dbSets.EcommerceProductLocalizedStrings));
            dbContextMock.Setup(x => x.EcommerceProductRelated).Returns(DbSet(dbSets.EcommerceProductRelated));
            dbContextMock.Setup(x => x.EcommerceProductStockUnits).Returns(DbSet(dbSets.EcommerceProductStockUnits));
            dbContextMock.Setup(x => x.EcommerceProductImages).Returns(DbSet(dbSets.EcommerceProductImages));
            dbContextMock.Setup(x => x.EcommerceProductImageKits).Returns(DbSet(dbSets.EcommerceProductImageKits));
            dbContextMock.Setup(x => x.EcommerceOrderShippingBoxes).Returns(DbSet(dbSets.EcommerceOrderShippingBoxes));
            dbContextMock.Setup(x => x.EcommerceShippingBoxes).Returns(DbSet(dbSets.EcommerceShippingBoxes));
            dbContextMock.Setup(x => x.EcommerceShippingBoxLocalizedKits).Returns(DbSet(dbSets.EcommerceShippingBoxLocalizedKits));
            dbContextMock.Setup(x => x.EcommerceVariants).Returns(DbSet(dbSets.EcommerceVariants));
            dbContextMock.Setup(x => x.EcommerceVariantLocalizedKits).Returns(DbSet(dbSets.EcommerceVariantLocalizedKits));
            dbContextMock.Setup(x => x.EcommerceVariantOptionLocalizedKits).Returns(DbSet(dbSets.EcommerceVariantOptionLocalizedKits));
            dbContextMock.Setup(x => x.EcommerceVariantOptions).Returns(DbSet(dbSets.EcommerceVariantOptions));
            dbContextMock.Setup(x => x.Emails).Returns(DbSet(dbSets.Emails));
            dbContextMock.Setup(x => x.SentEmails).Returns(DbSet(dbSets.SentEmails));
            dbContextMock.Setup(x => x.EmailLocalizedKits).Returns(DbSet(dbSets.EmailLocalizedKits));
            dbContextMock.Setup(x => x.Users).Returns(DbSet(dbSets.Users));
            dbContextMock.Setup(x => x.UserRoles).Returns(DbSet(dbSets.UserRoles));
            dbContextMock.Setup(x => x.UserLocalizedKits).Returns(DbSet(dbSets.UserLocalizedKits));
            dbContextMock.Setup(x => x.EcommerceUserCredits).Returns(DbSet(dbSets.EcommerceUserCredits));

            dbContextMock.Setup(x => x.SetValues(It.IsAny<BaseEntity>(), It.IsAny<BaseEntity>())).Callback<BaseEntity, BaseEntity>((oldEntity, newEntity) =>
                {
                    newEntity.ModifiedDate = DateTime.Now;

                    newEntity.CreatedDate = oldEntity.CreatedDate;

                    var oldEntityProperties = oldEntity.GetType().GetProperties();

                    foreach (var oldEntityProperty in oldEntityProperties)
                    {
                        if (!oldEntityProperty.CanWrite || !IsSimpleType(oldEntityProperty.PropertyType)) continue;

                        var newEntityProperty = newEntity.GetType().GetProperty(oldEntityProperty.Name);
                        oldEntityProperty.SetValue(oldEntity, newEntityProperty.GetValue(newEntity, null), null);
                    }
                });

            return dbContextMock.Object;
        }

        private static bool IsSimpleType(Type type)
        {
            return type.IsPrimitive || new[]
            {
                typeof (Enum),
                typeof (string),
                typeof (decimal),
                typeof (DateTime),
                typeof (DateTimeOffset),
                typeof (TimeSpan),
                typeof (Guid)
            }.Contains(type) ||
            Convert.GetTypeCode(type) != TypeCode.Object ||
            type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>) &&
            IsSimpleType(type.GetGenericArguments()[0]);
        }

        public static CmsDbContext DbContext(params object[] objects)
        {
            var dbSetsDto = new DbSetsDto();

            foreach (var item in objects)
            {
                var itemType = item.GetType().Name;

                switch (itemType)
                {
                    case nameof(User):
                        dbSetsDto.Users.Add((User)item);
                        break;

                    case nameof(UserRole):
                        dbSetsDto.UserRoles.Add((UserRole)item);
                        break;

                    case nameof(UserLocalizedKit):
                        dbSetsDto.UserLocalizedKits.Add((UserLocalizedKit)item);
                        break;

                    case nameof(Address):
                        dbSetsDto.Addresses.Add((Address)item);
                        break;

                    case nameof(SeoSectionLocalizedKit):
                        dbSetsDto.SeoSectionLocalizedKits.Add((SeoSectionLocalizedKit)item);
                        break;

                    case nameof(SeoSection):
                        dbSetsDto.SeoSections.Add((SeoSection)item);
                        break;

                    case nameof(BlogCategory):
                        dbSetsDto.BlogCategories.Add((BlogCategory)item);
                        break;

                    case nameof(BlogComment):
                        dbSetsDto.BlogComments.Add((BlogComment)item);
                        break;

                    case nameof(BlogPost):
                        dbSetsDto.BlogPosts.Add((BlogPost)item);
                        break;

                    case nameof(Country):
                        dbSetsDto.Countries.Add((Country)item);
                        break;

                    case nameof(ContentSection):
                        dbSetsDto.ContentSections.Add((ContentSection)item);
                        break;

                    case nameof(ContentSectionLocalizedKit):
                        dbSetsDto.ContentSectionLocalizedKits.Add((ContentSectionLocalizedKit)item);
                        break;

                    case nameof(EcommerceCoupon):
                        dbSetsDto.EcommerceCoupons.Add((EcommerceCoupon)item);
                        break;

                    case nameof(EcommerceAttribute):
                        dbSetsDto.EcommerceAttributes.Add((EcommerceAttribute)item);
                        break;

                    case nameof(EcommerceAttributeOption):
                        dbSetsDto.EcommerceAttributeOptions.Add((EcommerceAttributeOption)item);
                        break;

                    case nameof(EcommerceAttributeLocalizedKit):
                        dbSetsDto.EcommerceAttributeLocalizedKits.Add((EcommerceAttributeLocalizedKit)item);
                        break;

                    case nameof(EcommerceCategory):
                        dbSetsDto.EcommerceCategories.Add((EcommerceCategory)item);
                        break;

                    case nameof(EcommerceCategoryLocalizedKit):
                        dbSetsDto.EcommerceCategoryLocalizedKits.Add((EcommerceCategoryLocalizedKit)item);
                        break;

                    case nameof(EcommerceCart):
                        dbSetsDto.EcommerceCarts.Add((EcommerceCart)item);
                        break;

                    case nameof(EcommerceCartItem):
                        dbSetsDto.EcommerceCartItems.Add((EcommerceCartItem)item);
                        break;

                    case nameof(EcommerceCartItemVariant):
                        dbSetsDto.EcommerceCartItemVariants.Add((EcommerceCartItemVariant)item);
                        break;

                    case nameof(EcommerceCartShippingBox):
                        dbSetsDto.EcommerceCartShippingBoxes.Add((EcommerceCartShippingBox)item);
                        break;

                    case nameof(EcommerceProduct):
                        dbSetsDto.EcommerceProducts.Add((EcommerceProduct)item);
                        break;

                    case nameof(EcommerceProductAttribute):
                        dbSetsDto.EcommerceProductAttributes.Add((EcommerceProductAttribute)item);
                        break;

                    case nameof(EcommerceProductVariant):
                        dbSetsDto.EcommerceProductVariants.Add((EcommerceProductVariant)item);
                        break;

                    case nameof(EcommerceProductLocalizedKit):
                        dbSetsDto.EcommerceProductLocalizedStrings.Add((EcommerceProductLocalizedKit)item);
                        break;

                    case nameof(EcommerceProductRelatedProducts):
                        dbSetsDto.EcommerceProductRelated.Add((EcommerceProductRelatedProducts)item);
                        break;

                    case nameof(EcommerceProductStockUnit):
                        dbSetsDto.EcommerceProductStockUnits.Add((EcommerceProductStockUnit)item);
                        break;

                    case nameof(EcommerceProductImage):
                        dbSetsDto.EcommerceProductImages.Add((EcommerceProductImage)item);
                        break;

                    case nameof(EcommerceProductImageKit):
                        dbSetsDto.EcommerceProductImageKits.Add((EcommerceProductImageKit)item);
                        break;

                    case nameof(EcommerceOrder):
                        dbSetsDto.EcommerceOrders.Add((EcommerceOrder)item);
                        break;

                    case nameof(EcommerceOrderAddress):
                        dbSetsDto.EcommerceOrderAddresses.Add((EcommerceOrderAddress)item);
                        break;

                    case nameof(EcommerceOrderShippingBox):
                        dbSetsDto.EcommerceOrderShippingBoxes.Add((EcommerceOrderShippingBox)item);
                        break;

                    case nameof(EcommerceOrderItem):
                        dbSetsDto.EcommerceOrderItems.Add((EcommerceOrderItem)item);
                        break;

                    case nameof(EcommerceOrderItemVariant):
                        dbSetsDto.EcommerceOrderItemProductVariants.Add((EcommerceOrderItemVariant)item);
                        break;

                    case nameof(EcommerceOrderCoupon):
                        dbSetsDto.EcommerceOrderCoupons.Add((EcommerceOrderCoupon)item);
                        break;

                    case nameof(EcommerceOrderCredit):
                        dbSetsDto.EcommerceOrderCredits.Add((EcommerceOrderCredit)item);
                        break;

                    case nameof(EcommerceShippingBox):
                        dbSetsDto.EcommerceShippingBoxes.Add((EcommerceShippingBox)item);
                        break;

                    case nameof(EcommerceShippingBoxLocalizedKit):
                        dbSetsDto.EcommerceShippingBoxLocalizedKits.Add((EcommerceShippingBoxLocalizedKit)item);
                        break;

                    case nameof(EcommerceVariant):
                        dbSetsDto.EcommerceVariants.Add((EcommerceVariant)item);
                        break;

                    case nameof(EcommerceVariantLocalizedKit):
                        dbSetsDto.EcommerceVariantLocalizedKits.Add((EcommerceVariantLocalizedKit)item);
                        break;

                    case nameof(EcommerceVariantOption):
                        dbSetsDto.EcommerceVariantOptions.Add((EcommerceVariantOption)item);
                        break;

                    case nameof(EcommerceVariantOptionLocalizedKit):
                        dbSetsDto.EcommerceVariantOptionLocalizedKits.Add((EcommerceVariantOptionLocalizedKit)item);
                        break;

                    case nameof(Email):
                        dbSetsDto.Emails.Add((Email)item);
                        break;

                    case nameof(EmailLocalizedKit):
                        dbSetsDto.EmailLocalizedKits.Add((EmailLocalizedKit)item);
                        break;

                    case nameof(SentEmail):
                        dbSetsDto.SentEmails.Add((SentEmail)item);
                        break;

                    case nameof(EcommerceUserCredit):
                        dbSetsDto.EcommerceUserCredits.Add((EcommerceUserCredit)item);
                        break;

                }

            }

            return DbContext(dbSetsDto);
        }

        public static void Set(CmsDbContext dbContext)
        {
            if (HttpContext.Current == null) HttpContext.Current = new HttpContext(new HttpRequest(null, "http://test.com", null), new HttpResponse(new StringWriter()));

            HttpContext.Current.Items["_dbContext"] = dbContext;
        }
    }
}