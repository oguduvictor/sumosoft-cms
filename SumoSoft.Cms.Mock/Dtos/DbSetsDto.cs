﻿using System.Collections.Generic;
using Castle.Components.DictionaryAdapter;
using JetBrains.Annotations;
using SumoSoft.Cms.Domain;

#pragma warning disable 1591

namespace SumoSoft.Cms.Mock.Dtos
{
    public class DbSetsDto
    {
        [NotNull]
        public List<User> Users { get; set; } = new List<User>();
        [NotNull]
        public List<UserRole> UserRoles { get; set; } = new List<UserRole>();
        [NotNull]
        public List<UserLocalizedKit> UserLocalizedKits { get; set; } = new List<UserLocalizedKit>();
        [NotNull]
        public List<EcommerceUserCredit> EcommerceUserCredits { get; set; } = new List<EcommerceUserCredit>();
        [NotNull]
        public List<Address> Addresses { get; set; } = new List<Address>();
        [NotNull]
        public List<Country> Countries { get; set; } = new List<Country>();
        [NotNull]
        public List<CmsSettings> CmsSettings { get; set; } = new List<CmsSettings>();
        [NotNull]
        public List<ContentSection> ContentSections { get; set; } = new List<ContentSection>();
        [NotNull]
        public List<ContentSectionLocalizedKit> ContentSectionLocalizedKits { get; set; } = new List<ContentSectionLocalizedKit>();
        [NotNull]
        public List<SeoSection> SeoSections { get; set; } = new List<SeoSection>();
        [NotNull]
        public List<SeoSectionLocalizedKit> SeoSectionLocalizedKits { get; set; } = new List<SeoSectionLocalizedKit>();
        [NotNull]
        public List<Log> Logs { get; set; } = new List<Log>();
        [NotNull]
        public List<Email> Emails { get; set; } = new List<Email>();
        [NotNull]
        public List<SentEmail> SentEmails { get; set; } = new List<SentEmail>();
        [NotNull]
        public List<EmailLocalizedKit> EmailLocalizedKits { get; set; } = new List<EmailLocalizedKit>();
        [NotNull]
        public List<EcommerceCoupon> EcommerceCoupons { get; set; } = new List<EcommerceCoupon>();
        [NotNull]
        public List<BlogCategory> BlogCategories { get; set; } = new List<BlogCategory>();
        [NotNull]
        public List<BlogComment> BlogComments { get; set; } = new List<BlogComment>();
        [NotNull]
        public List<BlogPost> BlogPosts { get; set; } = new List<BlogPost>();
        [NotNull]
        public List<EcommerceAttribute> EcommerceAttributes { get; set; } = new List<EcommerceAttribute>();
        [NotNull]
        public List<EcommerceAttributeOption> EcommerceAttributeOptions { get; set; } = new List<EcommerceAttributeOption>();
        [NotNull]
        public List<EcommerceAttributeLocalizedKit> EcommerceAttributeLocalizedKits { get; set; } = new List<EcommerceAttributeLocalizedKit>() ;
        [NotNull]
        public List<EcommerceAttributeOptionLocalizedKit> EcommerceAttributeOptionLocalizedKits { get; set; } = new List<EcommerceAttributeOptionLocalizedKit>();
        [NotNull]
        public List<EcommerceCategory> EcommerceCategories { get; set; } = new List<EcommerceCategory>();
        [NotNull]
        public List<EcommerceCategoryLocalizedKit> EcommerceCategoryLocalizedKits { get; set; } = new List<EcommerceCategoryLocalizedKit>();
        [NotNull]
        public List<EcommerceCart> EcommerceCarts { get; set; } = new List<EcommerceCart>();
        [NotNull]
        public List<EcommerceCartItem> EcommerceCartItems { get; set; } = new List<EcommerceCartItem>();
        [NotNull]
        public List<EcommerceCartItemVariant> EcommerceCartItemVariants { get; set; } = new List<EcommerceCartItemVariant>();
        [NotNull]
        public List<EcommerceCartShippingBox> EcommerceCartShippingBoxes { get; set; } = new List<EcommerceCartShippingBox>();
        [NotNull]
        public List<EcommerceOrder> EcommerceOrders { get; set; } = new List<EcommerceOrder>();
        [NotNull]
        public List<EcommerceOrderItem> EcommerceOrderItems { get; set; } = new List<EcommerceOrderItem>();
        [NotNull]
        public List<EcommerceOrderItemVariant> EcommerceOrderItemProductVariants { get; set; } = new List<EcommerceOrderItemVariant>();
        [NotNull]
        public List<EcommerceOrderAddress> EcommerceOrderAddresses { get; set; } = new List<EcommerceOrderAddress>();
        [NotNull]
        public List<EcommerceOrderShippingBox> EcommerceOrderShippingBoxes { get; set; } = new List<EcommerceOrderShippingBox>();
        [NotNull]
        public List<EcommerceOrderCoupon> EcommerceOrderCoupons { get; set; } = new List<EcommerceOrderCoupon>();
        [NotNull]
        public List<EcommerceOrderCredit> EcommerceOrderCredits { get; set; } = new List<EcommerceOrderCredit>();
        [NotNull]
        public List<EcommerceProductLocalizedKit> EcommerceProductLocalizedKits { get; set; } =  new List<EcommerceProductLocalizedKit>();
        [NotNull]
        public List<EcommerceVariant> EcommerceVariants { get; set; } = new List<EcommerceVariant>();
        [NotNull]
        public List<EcommerceVariantLocalizedKit> EcommerceVariantLocalizedKits { get; set; } = new List<EcommerceVariantLocalizedKit>();
        [NotNull]
        public List<EcommerceVariantOption> EcommerceVariantOptions { get; set; } = new List<EcommerceVariantOption>();
        [NotNull]
        public List<EcommerceVariantOptionLocalizedKit> EcommerceVariantOptionLocalizedKits { get; set; } = new List<EcommerceVariantOptionLocalizedKit>();
        [NotNull]
        public List<EcommerceProduct> EcommerceProducts { get; set; } = new List<EcommerceProduct>();
        [NotNull]
        public List<EcommerceProductAttribute> EcommerceProductAttributes { get; set; } = new List<EcommerceProductAttribute>();
        [NotNull]
        public List<EcommerceProductVariant> EcommerceProductVariants { get; set; } = new List<EcommerceProductVariant>();
        [NotNull]
        public List<EcommerceProductLocalizedKit> EcommerceProductLocalizedStrings { get; set; } = new List<EcommerceProductLocalizedKit>();
        [NotNull]
        public List<EcommerceProductRelatedProducts> EcommerceProductRelated { get; set; } = new List<EcommerceProductRelatedProducts>();
        [NotNull]
        public List<EcommerceShippingBox> EcommerceShippingBoxes { get; set; } = new List<EcommerceShippingBox>();
        [NotNull]
        public List<EcommerceShippingBoxLocalizedKit> EcommerceShippingBoxLocalizedKits { get; set; } = new List<EcommerceShippingBoxLocalizedKit>();
        [NotNull]
        public List<EcommerceProductStockUnit> EcommerceProductStockUnits { get; set; } = new EditableList<EcommerceProductStockUnit>();
        [NotNull]
        public List<EcommerceProductImage> EcommerceProductImages { get; set; } = new List<EcommerceProductImage>();
        [NotNull]
        public List<EcommerceProductImageKit> EcommerceProductImageKits { get; set; } = new List<EcommerceProductImageKit>();

    }
}