﻿namespace SumoSoft.Cms.UtilityServices
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using FluentValidation.Results;
    using JetBrains.Annotations;
    using Newtonsoft.Json;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <inheritdoc />
    public class UtilityService : IUtilityService
    {
        /// <summary>
        /// Convert an object into a JsonResult using CamelCasePropertyNamesContractResolver.
        /// </summary>
        public virtual JsonResult GetCamelCaseJsonResult(object model)
        {
            return new JsonResult
            {
                Data = model.ToCamelCaseJson(),
                MaxJsonLength = int.MaxValue
            };
        }

        /// <summary>
        /// Converts a json string to an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public virtual T FromJson<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        /// <summary>
        ///
        /// </summary>
        public virtual FormResponse GetFormResponse(ModelStateDictionary modelState, string redirectUrl = null)
        {
            var formResult = new FormResponse();

            formResult.RedirectUrl = redirectUrl;

            for (var i = 0; i < modelState.Values.Count; i++)
            {
                var propertyName = modelState.Keys.ElementAt(i);

                var value = modelState.Values.ElementAt(i);

                foreach (var error in value.Errors ?? new ModelErrorCollection())
                {
                    formResult.Errors.Add(new FormError(propertyName, error.ErrorMessage));
                }

            }

            return formResult;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual FormResponse GetFormResponse(ValidationResult validationResult, string redirectUrl = null)
        {
            var formResult = new FormResponse();

            formResult.RedirectUrl = redirectUrl;

            var errorGroups = validationResult.Errors.GroupBy(x => x.ErrorMessage);

            foreach (var errorGroup in errorGroups)
            {
                var propertyName = errorGroup.First().PropertyName;

                var errorMessage = errorGroup.First().ErrorMessage;

                var errorGroupCount = errorGroup.Count();

                if (errorGroupCount > 1)
                {
                    errorMessage = errorMessage + $" ({errorGroupCount} errors)";
                }

                formResult.Errors.Add(new FormError(propertyName, errorMessage));
            }

            return formResult;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual FormResponse<T> GetFormResponse<T>(ValidationResult validationResult, T target, string redirectUrl = null)
        {
            var formResult = new FormResponse<T>();

            formResult.RedirectUrl = redirectUrl;
            formResult.Target = target;

            var errorGroups = validationResult.Errors.GroupBy(x => x.ErrorMessage);

            foreach (var errorGroup in errorGroups)
            {
                var propertyName = errorGroup.First().PropertyName;

                var errorMessage = errorGroup.First().ErrorMessage;

                var errorGroupCount = errorGroup.Count();

                if (errorGroupCount > 1)
                {
                    errorMessage = errorMessage + $" ({errorGroupCount} errors)";
                }

                formResult.Errors.Add(new FormError(propertyName, errorMessage));
            }

            return formResult;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual JsonResult GetFormResponseJson(ValidationResult validationResult, string redirectUrl = null)
        {
            var formResult = this.GetFormResponse(validationResult, redirectUrl);

            return this.GetCamelCaseJsonResult(formResult);
        }

        /// <summary>
        ///
        /// </summary>
        public virtual JsonResult GetFormResponseJson(string redirectUrl = null)
        {
            var formResult = new FormResponse
            {
                RedirectUrl = redirectUrl
            };

            return this.GetCamelCaseJsonResult(formResult);
        }

        /// <summary>
        ///
        /// </summary>
        public virtual JsonResult GetFormResponseJson<T>(ValidationResult validationResult, T targetDto, string redirectUrl = null)
        {
            if (typeof(T).IsSubclassOf(typeof(BaseEntity)))
            {
                throw new Exception("The parameter targetDto must be a DTO. Passing a Domain Entity can cause Self-referencing loops in the JSON serialization.");
            }

            var formResult = this.GetFormResponse(validationResult, targetDto, redirectUrl);

            return this.GetCamelCaseJsonResult(formResult);
        }

        /// <summary>
        ///
        /// </summary>
        public virtual JsonResult GetFormResponseJson(ModelStateDictionary modelState, string redirectUrl = null)
        {
            var formResult = this.GetFormResponse(modelState, redirectUrl);

            return this.GetCamelCaseJsonResult(formResult);
        }

        /// <summary>
        /// Returns a strength score from 0 to 4.
        /// </summary>
        public virtual int CheckPasswordStrength(string password, bool isEncrypted = false)
        {
            var zx = new Zxcvbn.Zxcvbn();

            if (isEncrypted)
            {
                password = this.Decrypt(password);
            }

            return zx.EvaluatePassword(password).Score;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual string Encrypt(string inputString)
        {
            var password = GetSha1();
            var result = string.Empty;
            var algorithmSim = Rijndael.Create();
            var sdata = Encoding.ASCII.GetBytes(inputString);
            var sMemory = new MemoryStream();
            var salt = Encoding.Unicode.GetBytes(password);
            var secretKey = new PasswordDeriveBytes(password, salt, "SHA1", 2);
            const CryptoEnums.KeySize keySize = CryptoEnums.KeySize.Bits_256;
            var keyBytes = secretKey.GetBytes((int)keySize / 8);
            algorithmSim.Mode = CipherMode.CBC;
            var vi = secretKey.GetBytes(16);
            var encryptor = algorithmSim.CreateEncryptor(keyBytes, vi);
            var cStream = new CryptoStream(sMemory, encryptor, CryptoStreamMode.Write);

            try
            {
                cStream.Write(sdata, 0, sdata.Length);
                cStream.FlushFinalBlock();
                var cryptoByte = sMemory.ToArray();

                result = Convert.ToBase64String(cryptoByte, 0, cryptoByte.GetLength(0));
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: {0}", e.Message);
            }
            finally
            {
                cStream.Close();
                sMemory.Close();
            }

            return result;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual string Decrypt(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return string.Empty;
            }

            var password = GetSha1();
            var result = string.Empty;
            var algorithmSim = Rijndael.Create();
            var sdata = Convert.FromBase64String(inputString);
            var sMemory = new MemoryStream(sdata, 0, sdata.Length);
            var salt = Encoding.Unicode.GetBytes(password);
            var secretKey = new PasswordDeriveBytes(password, salt, "SHA1", 2);
            algorithmSim.Mode = CipherMode.CBC;
            const CryptoEnums.KeySize keySize = CryptoEnums.KeySize.Bits_256;
            var keyBytes = secretKey.GetBytes((int)keySize / 8);
            var vi = secretKey.GetBytes(16);
            var encryptor = algorithmSim.CreateDecryptor(keyBytes, vi);
            var cStream = new CryptoStream(sMemory, encryptor, CryptoStreamMode.Read);

            try
            {
                var sr = new StreamReader(cStream);
                result = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: {0}", e.Message);
            }
            finally
            {
                cStream.Close();
                sMemory.Close();
            }

            return result;
        }

        /// <summary>
        /// Gets the Url of the current domain.
        /// </summary>
        public virtual string GetDomainUrl()
        {
            return
                HttpContext.Current.Request.Url.Scheme +
                Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host +
                (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
        }

        /// <summary>
        /// Returns a Web.Config AppSetting Value.
        /// </summary>
        [NotNull]
        public virtual string GetAppSetting(string key, string fallbackValue = "")
        {
            var value = System.Web.Configuration.WebConfigurationManager.AppSettings[key];

            return value ?? fallbackValue;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual HttpCookie GetCookie(string name)
        {
            return HttpContext.Current?.Request.Cookies[name];
        }

        /// <summary>
        ///
        /// </summary>
        public virtual void SetCookie(string name, string value, int days)
        {
            var cookie = new HttpCookie(name)
            {
                Expires = DateTime.Now.AddDays(days),
                Value = value
            };

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        ///
        /// </summary>
        public virtual void RemoveCookie(string name)
        {
            var cookie = this.GetCookie(name);

            if (cookie != null)
            {
                this.SetCookie(name, null, -1);
            }
        }

        /// <inheritdoc />
        public virtual List<List<string>> GetDataFromCsv(Stream csvFile)
        {
            var table = new List<List<string>>();

            using (var reader = new StreamReader(csvFile, Encoding.UTF8))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    table.Add(line.Split(',').ToList());
                }
            }

            return table;
        }

        private static string GetSha1()
        {
            var stringBuilder = new StringBuilder();
            var cypherKey = System.Web.Configuration.WebConfigurationManager.AppSettings["CypherKey"];
            var stream = SHA1.Create().ComputeHash(new ASCIIEncoding().GetBytes(cypherKey));
            foreach (var t in stream)
            {
                stringBuilder.AppendFormat("{0:x2}", t);
            }

            return stringBuilder.ToString();
        }
    }
}
