﻿namespace SumoSoft.Cms.UtilityServices.Interfaces
{
    using System.Collections.Generic;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;
    using FluentValidation.Results;
    using SumoSoft.Cms.Dto;

    public interface IUtilityService
    {
        int CheckPasswordStrength(string password, bool isEncrypted = false);

        string Decrypt(string inputString);

        string Encrypt(string inputString);

        string GetAppSetting(string key, string fallbackValue = "");

        T FromJson<T>(string jsonString);

        JsonResult GetCamelCaseJsonResult(object model);

        HttpCookie GetCookie(string name);

        List<List<string>> GetDataFromCsv(Stream csvFile);

        string GetDomainUrl();

        FormResponse GetFormResponse(ModelStateDictionary modelState, string redirectUrl = null);

        FormResponse GetFormResponse(ValidationResult validationResult, string redirectUrl = null);

        FormResponse<T> GetFormResponse<T>(ValidationResult validationResult, T target, string redirectUrl = null);

        JsonResult GetFormResponseJson(string redirectUrl = null);

        JsonResult GetFormResponseJson(ModelStateDictionary modelState, string redirectUrl = null);

        JsonResult GetFormResponseJson(ValidationResult validationResult, string redirectUrl = null);

        JsonResult GetFormResponseJson<T>(ValidationResult validationResult, T targetDto, string redirectUrl = null);

        void RemoveCookie(string name);

        void SetCookie(string name, string value, int days);
    }
}