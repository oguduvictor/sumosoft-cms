﻿namespace SumoSoft.Cms.MailChimp.Dto
{
    public class MailChimpMergeFieldsDto
    {
        public string FNAME { get; set; }

        public string LNAME { get; set; }
    }
}
