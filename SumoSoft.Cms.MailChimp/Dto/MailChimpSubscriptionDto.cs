﻿namespace SumoSoft.Cms.MailChimp.Dto
{
    public class MailChimpSubscriptionDto
    {
        public string email_address { get; set; }

        public string status { get; set; }

        public string language { get; set; }

        public MailChimpLocationDto location { get; set; }

        public MailChimpMergeFieldsDto merge_fields { get; set; }
    }
}
