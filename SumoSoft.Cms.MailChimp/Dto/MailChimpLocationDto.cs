﻿namespace SumoSoft.Cms.MailChimp.Dto
{
    public class MailChimpLocationDto
    {
        public string country_code { get; set; }
    }
}
