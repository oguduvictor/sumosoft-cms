﻿namespace SumoSoft.Cms.MailChimp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading;
    using Newtonsoft.Json;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.LogServices.Interfaces;
    using SumoSoft.Cms.MailChimp.Dto;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class CmsMailChimpService
    {
        private readonly ILogService logService;
        private readonly IUtilityService utilityService;

        /// <summary>
        ///
        /// </summary>
        /// <param name="logService"></param>
        /// <param name="utilityService"></param>
        public CmsMailChimpService(
            ILogService logService,
            IUtilityService utilityService)
        {
            this.logService = logService;
            this.utilityService = utilityService;
        }

        /// <summary>
        /// Add user to a specific mailchimp list.
        /// </summary>
        public FormResponse AddSubscription(CmsDbContext dbContext, string email, string firstName, string lastName)
        {
            try
            {
                var apiServer = this.utilityService.GetAppSetting("MailChimpServer");

                if (string.IsNullOrEmpty(apiServer))
                {
                    throw new NullReferenceException("The MailChimpServer was null. Please make sure the required key has been added to the Web.Config file.");
                }

                var apiKey = this.utilityService.GetAppSetting("MailChimpApiKey");

                if (string.IsNullOrEmpty(apiKey))
                {
                    throw new NullReferenceException("The MailChimpApiKey was null. Please make sure the required key has been added to the Web.Config file.");
                }

                var listId = this.utilityService.GetAppSetting("MailChimpListId");

                if (string.IsNullOrEmpty(listId))
                {
                    throw new NullReferenceException("The MailChimpListId was null. Please make sure the required key has been added to the Web.Config file.");
                }

                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", apiKey);

                    var currentCultureName = Thread.CurrentThread.CurrentCulture.Name.Split('-');

                    var model = new MailChimpSubscriptionDto
                    {
                        email_address = email.ToPrettyEmailAddress(),
                        status = "subscribed",
                        language = currentCultureName.First(),
                        location = new MailChimpLocationDto
                        {
                            country_code = currentCultureName.Last()
                        },
                        merge_fields = new MailChimpMergeFieldsDto
                        {
                            FNAME = firstName ?? string.Empty,
                            LNAME = lastName ?? string.Empty
                        }
                    };

                    var stringContent = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

                    var httpResponseMessage = httpClient.PostAsync($"https://{apiServer}.api.mailchimp.com/3.0/lists/{listId}/members/", stringContent).Result;

                    var formResponse = new FormResponse
                    {
                        SuccessMessage = "Success"
                    };

                    if (!httpResponseMessage.IsSuccessStatusCode)
                    {
                        formResponse.Errors.Add(new FormError(httpResponseMessage.StatusCode.GetDescription()));
                    }

                    return formResponse;
                }
            }
            catch (Exception e)
            {
                this.logService.Log(dbContext, LogTypesEnum.Exception, "An error occurred while trying to subscribe a User to MailChimp", e.ToString());

                return new FormResponse
                {
                    Errors = new List<FormError> { new FormError("An error occurred. Please try again later.") }
                };
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public FormResponse RemoveSubscription(string email)
        {
            throw new NotImplementedException();
        }
    }
}
