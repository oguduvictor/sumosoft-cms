﻿namespace SumoSoft.Cms.CountryServices
{
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Web;
    using JetBrains.Annotations;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class CountryService : ICountryService
    {
        /// <inheritdoc />
        public virtual Country GetCountryByLanguageCode(CmsDbContext dbContext, string languageCode)
        {
            return dbContext.Countries.FirstOrDefault(x => x.LanguageCode == languageCode);
        }

        /// <inheritdoc />
        public virtual Country GetCurrentCountry(CmsDbContext dbContext)
        {
            return dbContext.Countries.First(x => x.LanguageCode == CultureInfo.CurrentCulture.Name);
        }

        /// <inheritdoc />
        public virtual Country GetShippingCountryOrCurrentCountry(CmsDbContext dbContext, [CanBeNull] Cart cart)
        {
            return cart?.ShippingAddress?.Country ?? this.GetCurrentCountry(dbContext);
        }

        /// <inheritdoc />
        public virtual Country GetDefaultCountry(CmsDbContext dbContext)
        {
            return dbContext.Countries.FirstOrDefault(x => x.IsDefault);
        }

        /// <inheritdoc />
        public virtual string GetCurrentLanguageCode()
        {
            return Thread.CurrentThread.CurrentCulture.Name;
        }

        /// <summary>
        /// Gets the current country url parameter.
        /// </summary>
        public virtual string GetCurrentCountryUrl()
        {
            return HttpContext.Current.Request.RequestContext.RouteData.GetCountry();
        }

        /// <summary>
        /// Method takes a url without the http protocol and returns the localized version.
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        public virtual string GetLocalizedUrl(string localPath)
        {
            var countryUrl = this.GetCurrentCountryUrl();

            return string.IsNullOrEmpty(countryUrl) ? localPath : $"/{countryUrl}/{localPath}".Replace("//", "/");
        }
    }
}
