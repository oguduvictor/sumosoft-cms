﻿namespace SumoSoft.Cms.CountryServices.Interfaces
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;

    /// <summary>
    ///
    /// </summary>
    public interface ICountryService
    {
        /// <summary>
        /// Returns the Country that matches a certain LanguageCode.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        Country GetCountryByLanguageCode(CmsDbContext dbContext, string languageCode);

        /// <summary>
        /// Returns the Country that matches the CultureInfo of the current Thread.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        [NotNull]
        Country GetCurrentCountry(CmsDbContext dbContext);

        /// <summary>
        /// Returns the Country that matches the CultureInfo of the current Thread.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cart"></param>
        /// <returns></returns>
        [NotNull]
        Country GetShippingCountryOrCurrentCountry(CmsDbContext dbContext, Cart cart);

        /// <summary>
        /// Gets Thread.CurrentThread.CurrentCulture.Name.
        /// </summary>
        /// <returns></returns>
        string GetCurrentLanguageCode();

        /// <summary>
        /// Returns the Country that matches the CultureInfo of the current Thread.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        Country GetDefaultCountry(CmsDbContext dbContext);

        /// <summary>
        /// Get localized Url.
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        string GetLocalizedUrl(string localPath);
    }
}