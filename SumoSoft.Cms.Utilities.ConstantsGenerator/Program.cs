﻿namespace SumoSoft.Cms.Utilities.ConstantsGenerator
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Extensions;

    internal class Program
    {
        private static readonly CmsDbContext dbContext = new CmsDbContext();

        private static void Main()
        {
            var path = ConfigurationManager.AppSettings["Path"];
            var pathTs = ConfigurationManager.AppSettings["PathTs"];
            var customNamespace = ConfigurationManager.AppSettings["Namespace"];

            Console.WriteLine("Please enter the name of the class to be updated (or type 'all' to update all the classes)");

            while (true)
            {
                var command = Console.ReadLine()?.ToLower();

                try
                {
                    switch (command)
                    {
                        case "all":
                            Console.WriteLine("Creating ContentSectionName.cs...");
                            ContentSectionName(customNamespace, $"{path}ContentSectionName.cs");

                            Console.WriteLine("Creating ContentSectionName.ts...");
                            ContentSectionNameTs(pathTs);

                            Console.WriteLine("Creating CategoryName.cs...");
                            CategoryName(customNamespace, $"{path}CategoryName.cs");

                            Console.WriteLine("Creating CategoryName.ts...");
                            CategoryNameTs(pathTs);

                            Console.WriteLine("Creating VariantName.cs...");
                            VariantName(customNamespace, $"{path}VariantName.cs");

                            Console.WriteLine("Creating VariantName.ts...");
                            VariantNameTs(pathTs);

                            Console.WriteLine("Creating VariantOptionName.cs...");
                            VariantOptionName(customNamespace, $"{path}VariantOptionName.cs");

                            Console.WriteLine("Creating VariantOptionName.ts...");
                            VariantOptionNameTs(pathTs);

                            Console.WriteLine("Creating AttributeName.cs...");
                            AttributeName(customNamespace, $"{path}AttributeName.cs");

                            Console.WriteLine("Creating AttributeName.ts...");
                            AttributeNameTs(pathTs);

                            Console.WriteLine("Creating AttributeOptionName.cs...");
                            AttributeOptionName(customNamespace, $"{path}AttributeOptionName.cs");

                            Console.WriteLine("Creating AttributeOptionName.ts...");
                            AttributeOptionNameTs(pathTs);

                            Console.WriteLine("Creating CountryName.cs...");
                            CountryName(customNamespace, $"{path}CountryName.cs");

                            Console.WriteLine("Creating CountryName.ts...");
                            CountryNameTs(pathTs);

                            Console.WriteLine("Creating CountryLanguageCode.cs...");
                            CountryLanguageCode(customNamespace, $"{path}CountryLanguageCode.cs");

                            Console.WriteLine("Creating CountryLanguageCode.ts...");
                            CountryLanguageCodeTs(pathTs);

                            Console.WriteLine("Creating EmailName.cs...");
                            EmailName(customNamespace, $"{path}EmailName.cs");

                            Console.WriteLine("Creating EmailName.ts...");
                            EmailNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "contentsectionname":
                            Console.WriteLine("Creating ContentSectionName.cs...");
                            ContentSectionName(customNamespace, $"{path}ContentSectionName.cs");

                            Console.WriteLine("Creating ContentSectionName.ts...");
                            ContentSectionNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "categoryname":
                            Console.WriteLine("Creating CategoryName.cs...");
                            CategoryName(customNamespace, $"{path}CategoryName.cs");

                            Console.WriteLine("Creating CategoryName.ts...");
                            CategoryNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "variantname":
                            Console.WriteLine("Creating VariantName.cs...");
                            VariantName(customNamespace, $"{path}VariantName.cs");

                            Console.WriteLine("Creating VariantName.ts...");
                            VariantNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "variantoptionname":
                            Console.WriteLine("Creating VariantOptionName.cs...");
                            VariantOptionName(customNamespace, $"{path}VariantOptionName.cs");

                            Console.WriteLine("Creating VariantOptionName.ts...");
                            VariantOptionNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "attributename":
                            Console.WriteLine("Creating AttributeName.cs...");
                            AttributeName(customNamespace, $"{path}AttributeName.cs");

                            Console.WriteLine("Creating AttributeName.ts...");
                            AttributeNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "attributeoptionname":
                            Console.WriteLine("Creating AttributeOptionName.cs...");
                            AttributeOptionName(customNamespace, $"{path}AttributeOptionName.cs");

                            Console.WriteLine("Creating AttributeOptionName.ts...");
                            AttributeOptionNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "countryname":
                            Console.WriteLine("Creating CountryName.cs...");
                            CountryName(customNamespace, $"{path}CountryName.cs");

                            Console.WriteLine("Creating CountryName.ts...");
                            CountryNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "countrylanguagecode":
                            Console.WriteLine("Creating CountryLanguageCode.cs...");
                            CountryLanguageCode(customNamespace, $"{path}CountryLanguageCode.cs");

                            Console.WriteLine("Creating CountryLanguageCode.ts...");
                            CountryLanguageCodeTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        case "emailname":
                            Console.WriteLine("Creating EmailName.cs...");
                            EmailName(customNamespace, $"{path}EmailName.cs");

                            Console.WriteLine("Creating EmailName.ts...");
                            EmailNameTs(pathTs);

                            Console.WriteLine("Completed");
                            break;
                        default:
                            Console.WriteLine("Invalid command");
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private static void ContentSectionName(string customNamespace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNamespace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class ContentSectionName");
                streamWriter.WriteLine("	{");

                foreach (var contentSection in dbContext.ContentSections.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(contentSection.Name)) continue;

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace(contentSection.Name, "").ToUpperFirstLetter()} = \"{contentSection.Name}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void ContentSectionNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}ContentSectionName.ts"))
            {
                streamWriter.WriteLine("export namespace ContentSectionName {");

                foreach (var contentSection in dbContext.ContentSections.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(contentSection.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace(contentSection.Name, "").ToLowerFirstLetter()} = \"{contentSection.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void CategoryName(string customNamespace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNamespace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class CategoryName");
                streamWriter.WriteLine("	{");

                foreach (var category in dbContext.Categories.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(category.Name)) continue;

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace(category.Name, "").ToUpperFirstLetter()} = \"{category.Name}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void CategoryNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}CategoryName.ts"))
            {
                streamWriter.WriteLine("export namespace CategoryName {");

                foreach (var category in dbContext.Categories.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(category.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace(category.Name, "").ToLowerFirstLetter()} = \"{category.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void VariantName(string customNameSpace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNameSpace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class VariantName");
                streamWriter.WriteLine("	{");

                foreach (var variant in dbContext.Variants.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(variant.Name)) continue;

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace(variant.Name, "").ToUpperFirstLetter()} = \"{variant.Name}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void VariantNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}VariantName.ts"))
            {
                streamWriter.WriteLine("export namespace VariantName {");

                foreach (var variant in dbContext.Variants.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(variant.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace(variant.Name, "").ToLowerFirstLetter()} = \"{variant.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void VariantOptionName(string customNameSpace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNameSpace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class VariantOptionName");
                streamWriter.WriteLine("	{");

                foreach (var variantOption in dbContext.VariantOptions.OrderBy(x => x.Variant.Name).ThenBy(x => x.Name).ToList().DistinctBy(x => x.Variant?.Name + x.Name))
                {
                    if (string.IsNullOrEmpty(variantOption.Name)) continue;

                    var line = $"		public const string {new Regex("[^a-zA-z0-9]+").Replace($"{variantOption.Variant?.Name}_{variantOption.Name}", "").ToUpperFirstLetter()} = \"{variantOption.Name}\";";

                    streamWriter.WriteLine(line);
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void VariantOptionNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}VariantOptionName.ts"))
            {
                streamWriter.WriteLine("export namespace VariantOptionName {");

                foreach (var variantOption in dbContext.VariantOptions.OrderBy(x => x.Variant.Name).ThenBy(x => x.Name).ToList().DistinctBy(x => x.Variant?.Name + x.Name))
                {
                    if (string.IsNullOrEmpty(variantOption.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace($"{variantOption.Variant?.Name}_{variantOption.Name}", "").ToLowerFirstLetter()} = \"{variantOption.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void AttributeName(string customNameSpace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNameSpace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class AttributeName");
                streamWriter.WriteLine("	{");

                foreach (var attribute in dbContext.Attributes.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(attribute.Name)) continue;

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace(attribute.Name, "").ToUpperFirstLetter()} = \"{attribute.Name}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void AttributeNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}AttributeName.ts"))
            {
                streamWriter.WriteLine("export namespace AttributeName {");

                foreach (var attribute in dbContext.Attributes.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(attribute.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace(attribute.Name, "").ToLowerFirstLetter()} = \"{attribute.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void AttributeOptionName(string customNameSpace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNameSpace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class AttributeOptionName");
                streamWriter.WriteLine("	{");

                foreach (var attributeOption in dbContext.AttributeOptions.OrderBy(x => x.Attribute.Name).ThenBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(attributeOption.Name)) continue;

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace($"{attributeOption.Attribute?.Name}_{attributeOption.Name}", "").ToUpperFirstLetter()} = \"{attributeOption.Name}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void AttributeOptionNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}AttributeOptionName.ts"))
            {
                streamWriter.WriteLine("export namespace AttributeOptionName {");

                foreach (var attributeOption in dbContext.AttributeOptions.OrderBy(x => x.Attribute.Name).ThenBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(attributeOption.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace($"{attributeOption.Attribute?.Name}_{attributeOption.Name}", "").ToLowerFirstLetter()} = \"{attributeOption.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void CountryName(string customNameSpace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNameSpace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class CountryName");
                streamWriter.WriteLine("	{");

                foreach (var country in dbContext.Countries.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(country.Name)) continue;

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace(country.Name, "").ToUpperFirstLetter()} = \"{country.Name}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void CountryNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}CountryName.ts"))
            {
                streamWriter.WriteLine("export namespace CountryName {");

                foreach (var country in dbContext.Countries.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(country.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace(country.Name, "").ToLowerFirstLetter()} = \"{country.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void CountryLanguageCode(string customNameSpace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNameSpace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class CountryLanguageCode");
                streamWriter.WriteLine("	{");

                foreach (var country in dbContext.Countries.OrderBy(x => x.LanguageCode))
                {
                    if (string.IsNullOrEmpty(country.LanguageCode)) continue;

                    var constantName = string.Join("_", country.LanguageCode.Split('-'));

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace(constantName, "").ToUpperFirstLetter()} = \"{country.LanguageCode}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void CountryLanguageCodeTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}CountryLanguageCode.ts"))
            {
                streamWriter.WriteLine("export namespace CountryLanguageCode {");

                foreach (var country in dbContext.Countries.OrderBy(x => x.LanguageCode))
                {
                    if (string.IsNullOrEmpty(country.LanguageCode)) continue;

                    var constantName = string.Join("_", country.LanguageCode.Split('-'));

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace(constantName, "").ToLowerFirstLetter()} = \"{country.LanguageCode}\";");
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void EmailName(string customNameSpace, string path)
        {
            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.WriteLine($"namespace {customNameSpace}");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("	public class EmailName");
                streamWriter.WriteLine("	{");

                foreach (var email in dbContext.Emails.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(email.Name)) continue;

                    streamWriter.WriteLine($"		public const string {new Regex("[^a-zA-z0-9]+").Replace(email.Name, "").ToUpperFirstLetter()} = \"{email.Name}\";");
                }

                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }

        private static void EmailNameTs(string path)
        {
            using (var streamWriter = File.CreateText($"{path}EmailName.ts"))
            {
                streamWriter.WriteLine("export namespace EmailName {");

                foreach (var email in dbContext.Emails.OrderBy(x => x.Name))
                {
                    if (string.IsNullOrEmpty(email.Name)) continue;

                    streamWriter.WriteLine($"   export const {new Regex("[^a-zA-z0-9]+").Replace(email.Name, "").ToLowerFirstLetter()} = \"{email.Name}\";");
                }

                streamWriter.WriteLine("}");
            }
        }
    }
}
