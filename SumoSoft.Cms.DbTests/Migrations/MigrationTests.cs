﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using SumoSoft.Cms.Constants;

namespace SumoSoft.Cms.DbTests.Migrations
{
    [TestClass]
    public class MigrationTests : BaseDbTests
    {
        [TestMethod]
        public void RunAllMigration()
        {
            Assert.IsTrue(_dbContext.Database.CompatibleWithModel(false));
        }

        [TestMethod]
        public void RunPendingMigration()
        {
            var dbMigrator = GetDbMigrator();

            if (dbMigrator.GetPendingMigrations().Any())
            {
                dbMigrator.Update();
            }

            Assert.IsTrue(_dbContext.Database.CompatibleWithModel(false));
        }

        [TestMethod]
        public void RollbackLastMigration()
        {
            var dbMigrator = GetDbMigrator();

            if (dbMigrator.GetPendingMigrations().Any())
            {
                dbMigrator.Update();
            }

            var migrations = new List<string> { "0" };

            migrations.AddRange(dbMigrator.GetLocalMigrations());

            if (migrations.Count > 1)
            {
                var rollback = migrations[1];

                dbMigrator.Update(rollback);

                if (dbMigrator.GetPendingMigrations().Any())
                {
                    dbMigrator.Update();
                }
            }

            Assert.IsFalse(_dbContext.Database.CompatibleWithModel(false));
        }

        [TestMethod]
        public void RollbackAllMigration()
        {
            var dbMigrator = GetDbMigrator();

            var migrations = new List<string> { "0" };

            migrations.AddRange(dbMigrator.GetLocalMigrations().Reverse());

            for (var index = 1; index < migrations.Count; index++)
            {
                dbMigrator.Update(migrations[index]);
            }

            if (dbMigrator.GetPendingMigrations().Any())
            {
                dbMigrator.Update();
            }

            Assert.IsTrue(_dbContext.Database.CompatibleWithModel(false));
        }

        private static DbMigrator GetDbMigrator()
        {
            var connectionString = ConfigurationManager.ConnectionStrings[SharedConstants.DbConnectionString].ConnectionString;
            var providerName = ConfigurationManager.ConnectionStrings[SharedConstants.DbConnectionString].ProviderName;

            var cfg = new DataAccess.Migrations.Configuration
            {
                TargetDatabase = new DbConnectionInfo(connectionString, providerName)
            };

            return new DbMigrator(cfg);
        }
    }
}