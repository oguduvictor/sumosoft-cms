﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SumoSoft.Cms.Services.Interfaces;

namespace SumoSoft.Cms.DbTests.Services
{
    [TestClass]
    public class CmsServiceTests : BaseDbTests
    {
        private readonly ICmsService _cmsService;

        public CmsServiceTests(ICmsService cmsService)
        {
            _cmsService = cmsService;
        }

        [TestMethod]
        public void CreateOrder_CouponOverflow()
        {
            _coupon.Amount = 1000;

            _dbContext.SaveChanges();

            _cmsService.CreateOrder(_dbContext);
        }

        //[TestMethod]
        //public void GetCart()
        //{
        //    var country = new Country();
        //    var user = new User();
        //    var cart = new EcommerceCart();
        //    var shippingAddress = new Address();
        //    var cartShippingBox = new EcommerceCartShippingBox();
        //    var cartItem = new EcommerceCartItem();
        //    var product = new EcommerceProduct();
        //    var tax1 = new EcommerceTax();
        //    var tax2 = new EcommerceTax();

        //    // --------------------------------------------------------------

        //    user.Cart = cart;
        //    user.Country = country;

        //    cart.User = user;
        //    cart.CartShippingBoxes.Add(cartShippingBox);
        //    cart.ShippingAddress = shippingAddress;

        //    shippingAddress.ModifiedDate = DateTime.Now.AddDays(-1);
        //    shippingAddress.Country = country;

        //    cartShippingBox.CartItems.Add(cartItem);

        //    tax1.ModifiedDate = DateTime.Now; // ok
        //    tax1.ShippingAddress = shippingAddress;

        //    tax2.ModifiedDate = DateTime.Now.AddDays(-2); // obsolete
        //    tax2.ShippingAddress = shippingAddress;

        //    cartItem.Product = product;
        //    cartItem.TaxesOverride.Add(tax1);
        //    cartItem.TaxesOverride.Add(tax2);

        //    // --------------------------------------------------------------

        //    _dbContext.Countries.Add(country);
        //    _dbContext.Users.Add(user);
        //    _dbContext.EcommerceCarts.Add(cart);
        //    _dbContext.Addresses.Add(shippingAddress);
        //    _dbContext.EcommerceCartShippingBoxes.Add(cartShippingBox);
        //    _dbContext.EcommerceCartItems.Add(cartItem);
        //    _dbContext.EcommerceTaxes.Add(tax1);
        //    _dbContext.EcommerceTaxes.Add(tax2);
        //    _dbContext.EcommerceProducts.Add(product);

        //    _dbContext.SaveChanges();

        //    // --------------------------------------------------------------

        //    _cmsService.AuthenticatePartialUser(user.Id);

        //    var getCart = _cmsService.GetCart();

        //    Assert.IsTrue(_dbContext.EcommerceTaxes.Count() == 1);
        //}

        //[TestMethod]
        //public void ApplyCreditToCart_NoLocalizedKits()
        //{
        //    var country = CmsMock.CurrentCountry();
        //    var user = new User();
        //    var cart = new EcommerceCart();

        //    user.Cart = cart;

        //    _dbContext.Countries.Add(country);
        //    _dbContext.Users.Add(user);
        //    _dbContext.EcommerceCarts.Add(cart);
        //    _dbContext.SaveChanges();

        //    _cmsService.UserSignIn(dbContext, user.Id);

        //    _cmsService.ApplyCreditToCart(10);

        //    Assert.IsNotNull(user.Cart.UserCredit);
        //}

        //[TestMethod]
        //public void AuthenticateUser()
        //{
        //    var authenticatingUser = new User();
        //    var authenticatingUserCart = new EcommerceCart();
        //    var partialUser = new User();
        //    var partialUserCart = new EcommerceCart();

        //    authenticatingUser.Email = "email";
        //    authenticatingUser.Password = _cmsService.Encrypt("password");
        //    authenticatingUser.Cart = authenticatingUserCart;

        //    partialUser.Role = new UserRole();
        //    partialUser.Cart = partialUserCart;

        //    _dbContext.Users.Add(authenticatingUser);
        //    _dbContext.Users.Add(partialUser);

        //    _dbContext.SaveChanges();

        //    CmsMock.SetCookie(SharedConstants.PartialUserCookie, partialUser.Id.ToString());

        //    _cmsService.AuthenticateUser("email", "password");

        //    //Assert.IsNull(_cartService.GetById(dbContext, authenticatingUserCart.Id));
        //    //Assert.IsNull(_userService.GetById(dbContext, partialUser.Id));
        //    //Assert.AreEqual(partialUserCart.Id, _userService.GetById(dbContext, authenticatingUser.Id)?.Cart?.Id);
        //}

        //[TestMethod]
        //public void RegisterUser()
        //{
        //    var country = CmsMock.CurrentCountry();
        //    var role = new UserRole();

        //    role.Name = SharedConstants.DefaultUserRole;

        //    _dbContext.Countries.Add(country);
        //    _dbContext.UserRoles.Add(role);
        //    _dbContext.SaveChanges();

        //    var result = _cmsService.RegisterUser(new User
        //    {
        //        Email = "user@email.com",
        //        Password = "@3f7#3f?",
        //        FirstName = "FirstName",
        //        LastName = "LastName",
        //        Country = country
        //    });

        //    Assert.IsTrue(result.IsValid);
        //    Assert.AreEqual(1, _dbContext.Users.Count());
        //}

        //[TestMethod]
        //public void RegisterUser_WeakPassword()
        //{
        //    var country = CmsMock.CurrentCountry();
        //    var role = new UserRole();

        //    role.Name = SharedConstants.DefaultUserRole;

        //    _dbContext.Countries.Add(country);
        //    _dbContext.UserRoles.Add(role);
        //    _dbContext.SaveChanges();

        //    var result = _cmsService.RegisterUser(new User
        //    {
        //        Email = "user@email.com",
        //        Password = "12345678"
        //    });

        //    Assert.IsTrue(result.IsValid);
        //}

        //[TestMethod]
        //public void RegisterUser_UserExists()
        //{
        //    var country = CmsMock.CurrentCountry();
        //    var role = new UserRole();
        //    var user = new User();

        //    user.Email = "user@email.com";
        //    user.Password = "12345678";

        //    role.Name = SharedConstants.DefaultUserRole;

        //    _dbContext.Countries.Add(country);
        //    _dbContext.UserRoles.Add(role);
        //    _dbContext.Users.Add(user);
        //    _dbContext.SaveChanges();

        //    //var result = _userService.RegisterUser(user.Email, user.Password, new RegisterUserDto());

        //    //Assert.AreEqual(RegisterUserErrorEnum.AlreadyRegistered, result.Error);
        //}

        //[TestMethod]
        //public void RegisterUser_PartialUserExists()
        //{
        //    var partialUser = new User();
        //    var partialUserCart = new EcommerceCart();
        //    var country = CmsMock.CurrentCountry();
        //    var role = new UserRole();

        //    partialUser.Cart = partialUserCart;

        //    role.Name = SharedConstants.DefaultUserRole;

        //    _dbContext.Users.Add(partialUser);
        //    _dbContext.EcommerceCarts.Add(partialUserCart);
        //    _dbContext.Countries.Add(country);
        //    _dbContext.UserRoles.Add(role);
        //    _dbContext.SaveChanges();

        //    CmsMock.SetCookie(SharedConstants.PartialUserCookie, partialUser.Id.ToString());

        //    var result = _cmsService.RegisterUser(new User
        //    {
        //        Email = "user@email.com",
        //        Password = "@3f7#3f?",
        //        FirstName = "FirstName",
        //        LastName = "LastName",
        //        Country = country
        //    });

        //    //Assert.IsNotNull(result.RegisteredUser);
        //    Assert.AreEqual(1, _dbContext.Users.Count());
        //    //Assert.AreEqual(partialUserCart.Id, result.RegisteredUser.Cart?.Id);
        //}

        //[TestMethod]
        //public void CreateOrder()
        //{
        //    var currentCountry = CmsMock.CurrentCountry();
        //    var variant = new EcommerceVariant();
        //    var variantOption = new EcommerceVariantOption();
        //    var product = new EcommerceProduct();
        //    var productVariant = new EcommerceProductVariant();
        //    var address = new Address();
        //    var shippingBox = new EcommerceShippingBox();
        //    var cart = new EcommerceCart();
        //    var cartShippingBox = new EcommerceCartShippingBox();
        //    var cartItem = new EcommerceCartItem();
        //    var cartItemVariant = new EcommerceCartItemVariant();

        //    variant.Name = "variantName";
        //    variant.Type = VariantTypesEnum.Options;
        //    variant.VariantOptions.Add(variantOption);

        //    variantOption.Variant = variant;
        //    variantOption.Name = "variantOptionName";

        //    product.UrlToken = "productUrlToken";
        //    product.ProductVariants.Add(productVariant);

        //    productVariant.Product = product;
        //    productVariant.Variant = variant;

        //    address.Country = currentCountry;

        //    cart.CartShippingBoxes.Add(cartShippingBox);
        //    cart.ShippingAddress = address;

        //    cartShippingBox.CartItems.Add(cartItem);
        //    cartShippingBox.Cart = cart;
        //    cartShippingBox.ShippingBox = shippingBox;

        //    cartItem.Product = product;
        //    cartItem.CartItemVariants.Add(cartItemVariant);

        //    cartItemVariant.Variant = variant;
        //    cartItemVariant.VariantOptionValue = variantOption;

        //    // -----------------------------------------------------------------

        //    _dbContext.Countries.Add(currentCountry);
        //    _dbContext.EcommerceVariants.Add(variant);
        //    _dbContext.EcommerceVariantOptions.Add(variantOption);
        //    _dbContext.EcommerceProducts.Add(product);
        //    _dbContext.EcommerceProductVariants.Add(productVariant);
        //    _dbContext.Addresses.Add(address);
        //    _dbContext.EcommerceShippingBoxes.Add(shippingBox);
        //    _dbContext.EcommerceCarts.Add(cart);
        //    _dbContext.EcommerceCartShippingBoxes.Add(cartShippingBox);
        //    _dbContext.EcommerceCartItems.Add(cartItem);
        //    _dbContext.EcommerceCartItemVariants.Add(cartItemVariant);
        //    _dbContext.SaveChanges();

        //    // -----------------------------------------------------------------

        //    var order = _cmsService.CreateOrder(cart);

        //    Assert.IsNotNull(order);
        //}

        //[TestMethod]
        //public void ApplyAndRemoveCoupon()
        //{
        //    var cart = _cmsService.CreateCart();
        //    var coupon = new EcommerceCoupon();

        //    coupon.ExpirationDate = DateTime.Now;

        //    _dbContext.EcommerceCoupons.Add(coupon);
        //    _dbContext.SaveChanges();

        //    _cmsService.ApplyCouponToCart(coupon.Code);

        //    Assert.IsNotNull(cart.Coupon);
        //    Assert.AreEqual(-1, coupon.ValidityTimes);

        //    _cmsService.RemoveCouponFromCart();

        //    Assert.IsNull(cart.Coupon);
        //    Assert.AreEqual(0, coupon.ValidityTimes);
        //}
    }
}
