﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SumoSoft.Cms.DataAccess;
using SumoSoft.Cms.Domain;

namespace SumoSoft.Cms.DbTests
{
    [TestClass]
    public class BaseDbTests
    {
        public readonly CmsDbContext _dbContext;
        public User _user = new User();
        public Country _country = new Country();
        public EcommerceTax _tax = new EcommerceTax();
        public EcommerceProduct _product = new EcommerceProduct();
        public EcommerceProductLocalizedKit _productLocalizedKit = new EcommerceProductLocalizedKit();
        public EcommerceProductVariant _productVariant = new EcommerceProductVariant();
        public EcommerceVariant _variant = new EcommerceVariant();
        public EcommerceVariantOption _variantOption = new EcommerceVariantOption();
        public EcommerceProductStockUnit _productStockUnit = new EcommerceProductStockUnit();
        public EcommerceProductStockUnitLocalizedKit _productStockUnitLocalizedKit = new EcommerceProductStockUnitLocalizedKit();
        public EcommerceCart _cart = new EcommerceCart();
        public EcommerceCartShippingBox _cartShippingBox = new EcommerceCartShippingBox();
        public EcommerceCartItem _cartItem = new EcommerceCartItem();
        public EcommerceCartItemVariant _cartItemVariant = new EcommerceCartItemVariant();
        public EcommerceCoupon _coupon = new EcommerceCoupon();

        public BaseDbTests()
        {
            // ------------------------------------------------------------------
            // Create base objects
            // ------------------------------------------------------------------

            _user.Cart = _cart;
            
            _country.LanguageCode = "en-GB";
            _country.Taxes.Add(_tax);

            _tax.Country = _country;

            _product.ProductLocalizedKits.Add(_productLocalizedKit);
            _product.ProductStockUnits.Add(_productStockUnit);
            _product.ProductVariants.Add(_productVariant);

            _productLocalizedKit.Country = _country;
            _productLocalizedKit.Product = _product;

            _productVariant.Product = _product;
            _productVariant.Variant = _variant;
            _productVariant.DefaultVariantOptionValue = _variantOption;

            _variantOption.Variant = _variant;

            _productStockUnit.ReleaseDate = DateTime.Now;
            _productStockUnit.Product = _product;
            _productStockUnit.ProductStockUnitLocalizedKits.Add(_productStockUnitLocalizedKit);
            _productStockUnit.VariantOptions.Add(_variantOption);

            _productStockUnitLocalizedKit.BasePrice = 100;
            _productStockUnitLocalizedKit.ProductStockUnit = _productStockUnit;
            _productStockUnitLocalizedKit.Country = _country;
            
            _cart.User = _user;
            _cart.CartShippingBoxes.Add(_cartShippingBox);
            _cart.Coupon = _coupon;
            
            _cartShippingBox.Cart = _cart;
            _cartShippingBox.CartItems.Add(_cartItem);

            _cartItem.CartShippingBox = _cartShippingBox;
            _cartItem.Product = _product;
            _cartItem.CartItemVariants.Add(_cartItemVariant);

            _cartItemVariant.Variant = _variant;
            _cartItemVariant.VariantOptionValue = _variantOption;

            _coupon.ExpirationDate = DateTime.Now.AddDays(1);

            // ------------------------------------------------------------------
            // Create db
            // ------------------------------------------------------------------

            _dbContext = new CmsDbContext();

            if (_dbContext.Database.Exists())
            {
                _dbContext.Database.Delete();
                _dbContext.Database.Create();
            }

            _dbContext.Users.Add(_user);
            _dbContext.Countries.Add(_country);
            _dbContext.EcommerceTaxes.Add(_tax);
            _dbContext.EcommerceProducts.Add(_product);
            _dbContext.EcommerceProductLocalizedKits.Add(_productLocalizedKit);
            _dbContext.EcommerceProductVariants.Add(_productVariant);
            _dbContext.EcommerceVariantOptions.Add(_variantOption);
            _dbContext.EcommerceProductStockUnits.Add(_productStockUnit);
            _dbContext.EcommerceProductStockUnitLocalizedKits.Add(_productStockUnitLocalizedKit);
            _dbContext.EcommerceCarts.Add(_cart);
            _dbContext.EcommerceCartShippingBoxes.Add(_cartShippingBox);
            _dbContext.EcommerceCartItems.Add(_cartItem);

            _dbContext.SaveChanges();

            // ------------------------------------------------------------------
            // Set current country
            // ------------------------------------------------------------------

            Thread.CurrentThread.CurrentCulture = new CultureInfo(_country.LanguageCode);

            // ------------------------------------------------------------------
            // Set authenticated user
            // ------------------------------------------------------------------

            if (HttpContext.Current == null) HttpContext.Current = new HttpContext(new HttpRequest(null, "http://test.com", null), new HttpResponse(new StringWriter()));

            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(_user.Id.ToString()), new string[0]);
        }

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Directory.GetCurrentDirectory());
        }
    }
}
