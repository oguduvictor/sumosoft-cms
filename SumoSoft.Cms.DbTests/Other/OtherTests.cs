﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SumoSoft.Cms.Domain;
using SumoSoft.Cms.Services.Interfaces;

namespace SumoSoft.Cms.DbTests.Other
{
    [TestClass]
    public class OtherTests : BaseDbTests
    {
        [TestMethod]
        public void Entity_Id()
        {
            var initialId = Guid.NewGuid();

            var user = new User(initialId);

            _dbContext.Users.Add(user);

            _dbContext.SaveChanges();

            Assert.AreEqual(initialId, user.Id);
        }
    }
}
