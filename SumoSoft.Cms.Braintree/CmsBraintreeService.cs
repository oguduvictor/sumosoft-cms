﻿#pragma warning disable SA1200 // Using directives should be placed correctly
using System;
using System.Collections.Generic;
using Braintree;
using JetBrains.Annotations;
using SumoSoft.Cms.CountryServices.Interfaces;
using SumoSoft.Cms.DataAccess;
using SumoSoft.Cms.Domain;
using SumoSoft.Cms.Dto;
using SumoSoft.Cms.Extensions;
using SumoSoft.Cms.UtilityServices.Interfaces;
using Environment = Braintree.Environment;

#pragma warning disable 1591

// ---------------------------------------------------------------------------------------------------
// Result Objects (on Success and Errors):
// https://developers.braintreepayments.com/reference/general/result-objects/dotnet
//
// Sale Transactions:
// https://developers.braintreepayments.com/reference/request/transaction/sale/dotnet
// ---------------------------------------------------------------------------------------------------

namespace SumoSoft.Cms.Braintree
{
    public class CmsBraintreeService
    {
        private readonly ICountryService countryService;
        private readonly IUtilityService utilityService;

        public CmsBraintreeService(
            IUtilityService utilityService,
            ICountryService countryService)
        {
            this.utilityService = utilityService;
            this.countryService = countryService;
        }

        /// <summary>
        /// Create a new BraintreeGateway using the Web.config settings.
        /// </summary>
        public BraintreeGateway CreateGateway()
        {
            var environment = this.utilityService.GetAppSetting("BraintreeEnvironment");
            if (string.IsNullOrEmpty(environment))
            {
                throw new NullReferenceException("No valid setting for 'BraintreeEnvironment' found in Web.Config");
            }

            var merchantId = this.utilityService.GetAppSetting("BraintreeMerchantId");
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new NullReferenceException("No valid setting for 'BraintreeMerchantId' found in Web.Config");
            }

            var publicKey = this.utilityService.GetAppSetting("BraintreePublicKey");
            if (string.IsNullOrEmpty(publicKey))
            {
                throw new NullReferenceException("No valid setting for 'BraintreePublicKey' found in Web.Config");
            }

            var privateKey = this.utilityService.GetAppSetting("BraintreePrivateKey");
            if (string.IsNullOrEmpty(privateKey))
            {
                throw new NullReferenceException("No valid setting for 'BraintreePrivateKey' found in Web.Config");
            }

            return new BraintreeGateway
            {
                Environment = environment == "production" ? Environment.PRODUCTION : Environment.SANDBOX,
                MerchantId = merchantId,
                PublicKey = publicKey,
                PrivateKey = privateKey
            };
        }

        /// <summary>
        /// Return the MerchantAccountId for the current country using the We.config settings.
        /// </summary>
        public string GetMerchantAccountId(CmsDbContext dbContext)
        {
            var merchantAccountId = this.countryService.GetCurrentCountry(dbContext).AsNotNull().PaymentAccountId;
            if (string.IsNullOrEmpty(merchantAccountId))
            {
                throw new NullReferenceException("Null or empty 'PaymentAccountId' found in Country settings");
            }
            return merchantAccountId;
        }

        /// <summary>
        /// Find a Braintree Customer by Id.
        /// If no Customer is found, return null.
        /// </summary>
        [CanBeNull]
        public Customer FindCustomer(BraintreeGateway gateway, string userId)
        {
            try
            {
                return gateway.Customer.Find(userId);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Add or Update a Braintree Customer based on the properties of a User.
        /// Id, Email, First and Last Name are automatically included,
        /// to add more fields use the optional parameter "customFields".
        /// </summary>
        [NotNull]
        public Customer AddOrUpdateCustomer(BraintreeGateway gateway, User user, Dictionary<string, string> customFields = null)
        {
            var customerRequest = new CustomerRequest
            {
                Id = user.Id.ToString(),
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CustomFields = customFields
            };

            Customer customer;

            try
            {
                var updateCustomerResult = gateway.Customer.Update(user.Id.ToString(), customerRequest);
                customer = updateCustomerResult.Target;
            }
            catch
            {
                var createCustomerResult = gateway.Customer.Create(customerRequest);
                customer = createCustomerResult.Target;
            }

            return customer;
        }

        /// <summary>
        /// Find the Braintree Transaction associated to the Order.
        /// If the Transaction is not found, return null.
        /// </summary>
        [CanBeNull]
        public Transaction FindOrderTransaction(BraintreeGateway gateway, Order order)
        {
            try
            {
                return gateway.Transaction.Find(order.TransactionId);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Void or Refund and Order Transaction according to its Status.
        /// If the Transaction has already been settled, a Refund will be procesed.
        /// If the Transaction is settling, a Void operation will be processed.
        /// </summary>
        [CanBeNull]
        public FormResponse VoidOrRefundOrderTransaction(BraintreeGateway gateway, Order order)
        {
            var formResponse = new FormResponse();

            var transaction = this.FindOrderTransaction(gateway, order);

            if (transaction == null)
            {
                formResponse.Errors.Add(new FormError("Transaction not found"));

                return formResponse;
            }

            Result<Transaction> result;

            if (transaction.Status == TransactionStatus.SETTLED || transaction.Status == TransactionStatus.SETTLING)
            {
                result = gateway.Transaction.Refund(order.TransactionId);
            }
            else
            {
                result = gateway.Transaction.Void(order.TransactionId);
            }

            if (!result.IsSuccess())
            {
                formResponse.Errors.Add(new FormError(result.Message));
            }

            return formResponse;
        }
    }
}
