namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceCategoryLocalizedkit_FeaturedImage : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategoryLocalizedKits", "FeaturedImage", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCategoryLocalizedKits", "FeaturedImage");
        }
    }
}
