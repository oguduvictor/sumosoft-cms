namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_138c : DbMigration
    {
        public override void Up()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceOptionGroupLocalizedStrings_dbo.EcommerceOptionGroups_Group_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceVariantGroupLocalizedStrings] DROP [FK_dbo.EcommerceOptionGroupLocalizedStrings_dbo.EcommerceOptionGroups_Group_Id]");
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceOptions_dbo.EcommerceOptionGroups_Group_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceVariants] DROP [FK_dbo.EcommerceOptions_dbo.EcommerceOptionGroups_Group_Id]");

            this.DropForeignKey("dbo.EcommerceVariantGroupLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceVariantGroupLocalizedStrings", "Group_Id", "dbo.EcommerceVariantGroups");
            this.DropForeignKey("dbo.EcommerceVariants", "Group_Id", "dbo.EcommerceVariantGroups");

            this.DropIndex("dbo.EcommerceVariantGroupLocalizedStrings", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceVariantGroupLocalizedStrings", new[] { "Group_Id" });
            this.DropIndex("dbo.EcommerceVariants", new[] { "Group_Id" });

            this.AddColumn("dbo.EcommerceProductAttributes", "IntegerValue", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceVariants", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceVariants", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceVariantLocalizedStrings", "Label", c => c.String());

            this.DropColumn("dbo.EcommerceVariants", "Group_Id");
            this.DropColumn("dbo.EcommerceVariantLocalizedStrings", "Name");
            this.DropTable("dbo.EcommerceVariantGroups");
            this.DropTable("dbo.EcommerceVariantGroupLocalizedStrings");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceVariantGroupLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Group_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceVariantGroups",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceVariantLocalizedStrings", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceVariants", "Group_Id", c => c.Guid());
            this.DropColumn("dbo.EcommerceVariantLocalizedStrings", "Label");
            this.DropColumn("dbo.EcommerceVariants", "SortOrder");
            this.DropColumn("dbo.EcommerceVariants", "Name");
            this.DropColumn("dbo.EcommerceProductAttributes", "IntegerValue");
            this.CreateIndex("dbo.EcommerceVariantGroupLocalizedStrings", "Group_Id");
            this.CreateIndex("dbo.EcommerceVariantGroupLocalizedStrings", "Country_Id");
            this.CreateIndex("dbo.EcommerceVariants", "Group_Id");
            this.AddForeignKey("dbo.EcommerceVariants", "Group_Id", "dbo.EcommerceVariantGroups", "Id");
            this.AddForeignKey("dbo.EcommerceVariantGroupLocalizedStrings", "Group_Id", "dbo.EcommerceVariantGroups", "Id");
            this.AddForeignKey("dbo.EcommerceVariantGroupLocalizedStrings", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
