namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Tags : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.BlogTags", "Post_Id", "dbo.BlogPosts");
            this.DropIndex("dbo.BlogTags", new[] { "Post_Id" });
            this.AddColumn("dbo.EcommerceProducts", "Tags", c => c.String());
            this.AddColumn("dbo.EcommerceVariantOptions", "Tags", c => c.String());
            this.DropTable("dbo.BlogTags");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.BlogTags",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Frequency = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Post_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.DropColumn("dbo.EcommerceVariantOptions", "Tags");
            this.DropColumn("dbo.EcommerceProducts", "Tags");
            this.CreateIndex("dbo.BlogTags", "Post_Id");
            this.AddForeignKey("dbo.BlogTags", "Post_Id", "dbo.BlogPosts", "Id");
        }
    }
}
