namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_157a : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceOrders", "User_Id", "dbo.Users");
            this.DropIndex("dbo.EcommerceOrders", new[] { "User_Id" });
            this.CreateTable(
                "dbo.EcommerceOrderStatus",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOrderStatusLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    OrderStatus_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceOrderStatus", t => t.OrderStatus_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.OrderStatus_Id);

            this.DropColumn("dbo.EcommerceOrders", "User_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrders", "User_Id", c => c.Guid());
            this.DropForeignKey("dbo.EcommerceOrderStatusLocalizedStrings", "OrderStatus_Id", "dbo.EcommerceOrderStatus");
            this.DropForeignKey("dbo.EcommerceOrderStatusLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceOrderStatusLocalizedStrings", new[] { "OrderStatus_Id" });
            this.DropIndex("dbo.EcommerceOrderStatusLocalizedStrings", new[] { "Country_Id" });
            this.DropTable("dbo.EcommerceOrderStatusLocalizedStrings");
            this.DropTable("dbo.EcommerceOrderStatus");
            this.CreateIndex("dbo.EcommerceOrders", "User_Id");
            this.AddForeignKey("dbo.EcommerceOrders", "User_Id", "dbo.Users", "Id");
        }
    }
}
