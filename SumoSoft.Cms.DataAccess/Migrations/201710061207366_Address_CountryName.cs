namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Address_CountryName : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.Addresses", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.Addresses", new[] { "Country_Id" });
            this.AddColumn("dbo.Addresses", "CountryName", c => c.String());
            this.DropColumn("dbo.Addresses", "Country_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.Addresses", "Country_Id", c => c.Guid());
            this.DropColumn("dbo.Addresses", "CountryName");
            this.CreateIndex("dbo.Addresses", "Country_Id");
            this.AddForeignKey("dbo.Addresses", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
