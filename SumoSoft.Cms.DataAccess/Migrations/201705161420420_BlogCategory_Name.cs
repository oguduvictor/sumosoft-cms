namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class BlogCategory_Name : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategoryLocalizedKits", "Description", c => c.String());
            this.AlterColumn("dbo.EcommerceVariants", "UrlToken", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceVariantOptions", "UrlToken", c => c.String(maxLength: 250));
            this.CreateIndex("dbo.EcommerceVariants", "UrlToken", name: "IX_Variant_UrlToken");
            this.CreateIndex("dbo.EcommerceVariantOptions", "UrlToken", name: "IX_VariantOption_UrlToken");
            this.RenameColumn(table: "dbo.BlogCategories", name: "Title", newName: "Name");

        }

        public override void Down()
        {
            this.DropIndex("dbo.EcommerceVariantOptions", "IX_VariantOption_UrlToken");
            this.DropIndex("dbo.EcommerceVariants", "IX_Variant_UrlToken");
            this.AlterColumn("dbo.EcommerceVariantOptions", "UrlToken", c => c.String());
            this.AlterColumn("dbo.EcommerceVariants", "UrlToken", c => c.String());
            this.DropColumn("dbo.EcommerceCategoryLocalizedKits", "Description");
            this.RenameColumn(table: "dbo.BlogCategories", name: "Name", newName: "Title");

        }
    }
}
