namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_163a : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.SentEmails",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    From = c.String(),
                    To = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Email_Id = c.Guid(),
                    Order_Id = c.Guid(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Emails", t => t.Email_Id)
                .ForeignKey("dbo.EcommerceOrders", t => t.Order_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Email_Id)
                .Index(t => t.Order_Id)
                .Index(t => t.User_Id);

            this.CreateTable(
                "dbo.Emails",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    ViewName = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EmailLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Subject = c.String(),
                    Content = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Email_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Emails", t => t.Email_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Email_Id);

            this.AddColumn("dbo.EcommerceCartItems", "OrderStockStatus", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "Order_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderItems", "ProductId", c => c.Guid(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "OrderStockStatus", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrders", "UserId", c => c.Guid(nullable: false));
            this.CreateIndex("dbo.EcommerceOrderItemProductVariants", "Order_Id");
            this.AddForeignKey("dbo.EcommerceOrderItemProductVariants", "Order_Id", "dbo.EcommerceOrders", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceOrderItemProductVariants", "Order_Id", "dbo.EcommerceOrders");
            this.DropForeignKey("dbo.SentEmails", "User_Id", "dbo.Users");
            this.DropForeignKey("dbo.SentEmails", "Order_Id", "dbo.EcommerceOrders");
            this.DropForeignKey("dbo.SentEmails", "Email_Id", "dbo.Emails");
            this.DropForeignKey("dbo.EmailLocalizedStrings", "Email_Id", "dbo.Emails");
            this.DropForeignKey("dbo.EmailLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EmailLocalizedStrings", new[] { "Email_Id" });
            this.DropIndex("dbo.EmailLocalizedStrings", new[] { "Country_Id" });
            this.DropIndex("dbo.SentEmails", new[] { "User_Id" });
            this.DropIndex("dbo.SentEmails", new[] { "Order_Id" });
            this.DropIndex("dbo.SentEmails", new[] { "Email_Id" });
            this.DropIndex("dbo.EcommerceOrderItemProductVariants", new[] { "Order_Id" });
            this.DropColumn("dbo.EcommerceOrders", "UserId");
            this.DropColumn("dbo.EcommerceOrderItems", "OrderStockStatus");
            this.DropColumn("dbo.EcommerceOrderItems", "ProductId");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "Order_Id");
            this.DropColumn("dbo.EcommerceCartItems", "OrderStockStatus");
            this.DropTable("dbo.EmailLocalizedStrings");
            this.DropTable("dbo.Emails");
            this.DropTable("dbo.SentEmails");
        }
    }
}
