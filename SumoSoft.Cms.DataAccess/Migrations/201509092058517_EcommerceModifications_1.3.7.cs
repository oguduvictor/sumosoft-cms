namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_137 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalProductStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseLocalProductStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalProductStockIfLocalProductStockIsEmpty", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalOptionStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseLocalOptionStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalOptionStockIfLocalOptionStockIsEmpty", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "OptionThatOverridesTheStock_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProductLocalizedStrings", "Stock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOptions", "Stock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOptionLocalizedStrings", "Stock", c => c.Int(nullable: false));
            this.CreateIndex("dbo.EcommerceProducts", "OptionThatOverridesTheStock_Id");
            this.AddForeignKey("dbo.EcommerceProducts", "OptionThatOverridesTheStock_Id", "dbo.EcommerceOptions", "Id");
        }

        public override void Down()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceProducts_dbo.EcommerceVariants_VariantThatOverridesTheStock_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceProducts] DROP [FK_dbo.EcommerceProducts_dbo.EcommerceVariants_VariantThatOverridesTheStock_Id]");

            this.DropForeignKey("dbo.EcommerceProducts", "OptionThatOverridesTheStock_Id", "dbo.EcommerceOptions");
            this.DropIndex("dbo.EcommerceProducts", new[] { "OptionThatOverridesTheStock_Id" });
            this.DropColumn("dbo.EcommerceOptionLocalizedStrings", "Stock");
            this.DropColumn("dbo.EcommerceOptions", "Stock");
            this.DropColumn("dbo.EcommerceProductLocalizedStrings", "Stock");
            this.DropColumn("dbo.EcommerceProducts", "OptionThatOverridesTheStock_Id");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalOptionStockIfLocalOptionStockIsEmpty");
            this.DropColumn("dbo.EcommerceProducts", "UseLocalOptionStock");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalOptionStock");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalProductStockIfLocalProductStockIsEmpty");
            this.DropColumn("dbo.EcommerceProducts", "UseLocalProductStock");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalProductStock");
        }
    }
}
