namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Remove_CartItem_UserCredit : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceCartItems", "UserCredit_Id", "dbo.EcommerceUserCredits");
            this.DropForeignKey("dbo.EcommerceOrderItems", "OrderCredit_Id", "dbo.EcommerceOrderCredits");
            this.DropIndex("dbo.EcommerceCartItems", new[] { "UserCredit_Id" });
            this.DropIndex("dbo.EcommerceOrderItems", new[] { "OrderCredit_Id" });
            this.DropColumn("dbo.EcommerceCartItems", "UserCredit_Id");
            this.DropColumn("dbo.EcommerceOrderItems", "OrderCredit_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "OrderCredit_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCartItems", "UserCredit_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceOrderItems", "OrderCredit_Id");
            this.CreateIndex("dbo.EcommerceCartItems", "UserCredit_Id");
            this.AddForeignKey("dbo.EcommerceOrderItems", "OrderCredit_Id", "dbo.EcommerceOrderCredits", "Id");
            this.AddForeignKey("dbo.EcommerceCartItems", "UserCredit_Id", "dbo.EcommerceUserCredits", "Id");
        }
    }
}
