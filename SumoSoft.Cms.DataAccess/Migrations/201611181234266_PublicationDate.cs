namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PublicationDate : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.BlogPosts", "PublicationDate", c => c.DateTime(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.BlogPosts", "PublicationDate");
        }
    }
}
