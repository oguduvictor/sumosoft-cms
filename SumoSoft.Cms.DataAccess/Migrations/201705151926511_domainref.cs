namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class domainref : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn(table: "dbo.EcommerceVariantOptionLocalizedKits", name: "EcommerceVariantOption_Id", newName: "VariantOption_Id");
            this.RenameIndex(table: "dbo.EcommerceVariantOptionLocalizedKits", name: "IX_EcommerceVariantOption_Id", newName: "IX_VariantOption_Id");
            this.CreateTable(
                "dbo.BlogCategoryLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    BlogCategory_Id = c.Guid(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BlogCategories", t => t.BlogCategory_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.BlogCategory_Id)
                .Index(t => t.Country_Id);

            this.AddColumn("dbo.EcommerceVariants", "UrlToken", c => c.String());
            this.AddColumn("dbo.EcommerceVariantOptions", "UrlToken", c => c.String());
            this.AddColumn("dbo.EcommerceShippingBoxLocalizedKits", "InternalDescription", c => c.String());
            this.RenameColumn(table: "dbo.EcommerceCategoryLocalizedKits", name: "Name", newName: "Title");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.BlogCategoryLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.BlogCategoryLocalizedKits", "BlogCategory_Id", "dbo.BlogCategories");
            this.DropIndex("dbo.BlogCategoryLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.BlogCategoryLocalizedKits", new[] { "BlogCategory_Id" });
            this.DropColumn("dbo.EcommerceShippingBoxLocalizedKits", "InternalDescription");
            this.DropColumn("dbo.EcommerceVariantOptions", "UrlToken");
            this.DropColumn("dbo.EcommerceVariants", "UrlToken");
            this.DropTable("dbo.BlogCategoryLocalizedKits");
            this.RenameIndex(table: "dbo.EcommerceVariantOptionLocalizedKits", name: "IX_VariantOption_Id", newName: "IX_EcommerceVariantOption_Id");
            this.RenameColumn(table: "dbo.EcommerceVariantOptionLocalizedKits", name: "VariantOption_Id", newName: "EcommerceVariantOption_Id");
            this.RenameColumn(table: "dbo.EcommerceCategoryLocalizedKits", name: "Title", newName: "Name");
        }
    }
}
