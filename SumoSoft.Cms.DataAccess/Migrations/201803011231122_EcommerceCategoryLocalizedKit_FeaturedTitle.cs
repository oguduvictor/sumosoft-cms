namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceCategoryLocalizedKit_FeaturedTitle : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategoryLocalizedKits", "FeaturedTitle", c => c.String());
            this.AddColumn("dbo.EcommerceCategoryLocalizedKits", "FeaturedDescription", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCategoryLocalizedKits", "FeaturedDescription");
            this.DropColumn("dbo.EcommerceCategoryLocalizedKits", "FeaturedTitle");
        }
    }
}
