namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_204 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.Users", "Country_Id", c => c.Guid());
            this.AddColumn("dbo.BlogPosts", "FeaturedImageHorizontalCrop", c => c.String());
            this.AddColumn("dbo.BlogPosts", "FeaturedImageVerticalCrop", c => c.String());
            this.CreateIndex("dbo.Users", "Country_Id");
            this.AddForeignKey("dbo.Users", "Country_Id", "dbo.Countries", "Id");

            this.Sql("UPDATE [dbo].[Users] SET [dbo].[Users].[Country_Id] = [dbo].[Countries].[Id] FROM [dbo].[Users] INNER JOIN [dbo].[EcommerceOrders] ON [dbo].[EcommerceOrders].[UserId] = [dbo].[Users].[Id] INNER JOIN [dbo].[Countries] ON [dbo].[Countries].[Name] = [dbo].[EcommerceOrders].[CountryName] WHERE [dbo].[Users].[Id] = [dbo].[EcommerceOrders].[UserId]");

            this.Sql("UPDATE [dbo].[Users] SET [dbo].[Users].[Country_Id] = [dbo].[Countries].[Id] FROM [dbo].[Countries] WHERE [dbo].[Users].[Country_Id] IS NULL AND [dbo].[Countries].[IsDefault] = 1");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.Users", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.Users", new[] { "Country_Id" });
            this.DropColumn("dbo.BlogPosts", "FeaturedImageVerticalCrop");
            this.DropColumn("dbo.BlogPosts", "FeaturedImageHorizontalCrop");
            this.DropColumn("dbo.Users", "Country_Id");
        }
    }
}
