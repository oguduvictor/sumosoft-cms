namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_208 : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceLocalizedReferralCredits", newName: "EcommerceLocalizedCredits");
            this.AddColumn("dbo.EcommerceCoupons", "Recipient", c => c.String());
            this.RenameColumn(table: "dbo.EcommerceLocalizedCredits", name: "Credit", newName: "Amount");
        }

        public override void Down()
        {
            this.RenameColumn(table: "dbo.EcommerceLocalizedCredits", name: "Amount", newName: "Credit");
            this.DropColumn("dbo.EcommerceCoupons", "Recipient");
            this.RenameTable(name: "dbo.EcommerceLocalizedCredits", newName: "EcommerceLocalizedReferralCredits");
        }
    }
}
