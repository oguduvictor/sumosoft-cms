namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications2_126 : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceTagDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceTagDescriptions", "Tag_Id", "dbo.EcommerceTags");
            this.DropIndex("dbo.EcommerceTagDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceTagDescriptions", new[] { "Tag_Id" });
            this.AddColumn("dbo.EcommerceProducts", "Price", c => c.Single(nullable: false));
            this.AddColumn("dbo.EcommerceTags", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceTags", "Country_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceTags", "Product_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceTags", "Country_Id");
            this.CreateIndex("dbo.EcommerceTags", "Product_Id");
            this.AddForeignKey("dbo.EcommerceTags", "Country_Id", "dbo.Countries", "Id");
            this.AddForeignKey("dbo.EcommerceTags", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.DropColumn("dbo.EcommerceAttributes", "SortOrder");
            this.DropColumn("dbo.EcommerceAttributeGroups", "SortOrder");
            this.DropColumn("dbo.EcommerceCategories", "SortOrder");
            this.DropColumn("dbo.EcommerceProducts", "SortOrder");
            this.DropColumn("dbo.EcommerceManufacturers", "SortOrder");
            this.DropColumn("dbo.EcommerceProductDescriptions", "MetaKeyboard");
            this.DropColumn("dbo.EcommerceOptions", "SortOrder");
            this.DropColumn("dbo.EcommerceOptionValues", "SortOrder");
            this.DropColumn("dbo.EcommerceProductImages", "SortOrder");
            this.DropColumn("dbo.EcommerceTags", "SortOrder");
            this.DropTable("dbo.EcommerceTagDescriptions");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceTagDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Tag_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceTags", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductImages", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOptionValues", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOptions", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductDescriptions", "MetaKeyboard", c => c.String());
            this.AddColumn("dbo.EcommerceManufacturers", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceCategories", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeGroups", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceAttributes", "SortOrder", c => c.Int(nullable: false));
            this.DropForeignKey("dbo.EcommerceTags", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceTags", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceTags", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceTags", new[] { "Country_Id" });
            this.DropColumn("dbo.EcommerceTags", "Product_Id");
            this.DropColumn("dbo.EcommerceTags", "Country_Id");
            this.DropColumn("dbo.EcommerceTags", "Name");
            this.DropColumn("dbo.EcommerceProducts", "Price");
            this.CreateIndex("dbo.EcommerceTagDescriptions", "Tag_Id");
            this.CreateIndex("dbo.EcommerceTagDescriptions", "Country_Id");
            this.AddForeignKey("dbo.EcommerceTagDescriptions", "Tag_Id", "dbo.EcommerceTags", "Id");
            this.AddForeignKey("dbo.EcommerceTagDescriptions", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
