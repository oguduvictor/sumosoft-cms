namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Tax : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceCartItems", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropForeignKey("dbo.EcommerceOrderItems", "OrderCoupon_Id", "dbo.EcommerceOrderCoupons");
            this.DropIndex("dbo.EcommerceCartItems", new[] { "Coupon_Id" });
            this.DropIndex("dbo.EcommerceOrderItems", new[] { "OrderCoupon_Id" });
            this.CreateTable(
                "dbo.EcommerceTaxes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Code = c.String(),
                    Percentage = c.Double(nullable: false),
                    ApplyToShippingPrice = c.Boolean(nullable: false),
                    ApplyToProductPrice = c.Boolean(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    CartItem_Id = c.Guid(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceCartItems", t => t.CartItem_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.CartItem_Id)
                .Index(t => t.Country_Id);

            this.CreateTable(
                "dbo.EcommerceOrderItemTaxes",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    Code = c.String(),
                    Amount = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    OrderItem_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOrderItems", t => t.OrderItem_Id)
                .Index(t => t.OrderItem_Id);

            this.AddColumn("dbo.Addresses", "Country_Id", c => c.Guid());
            this.AddColumn("dbo.Countries", "Localize", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.Countries", "AfterTaxRoundingDigits", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "TotalBeforeTax", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "TotalAfterTax", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderShippingBoxes", "ShippingPriceBeforeTax", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderShippingBoxes", "ShippingPriceAfterTax", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrders", "TotalBeforeTax", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrders", "TotalAfterTax", c => c.Double(nullable: false));
            this.CreateIndex("dbo.Addresses", "Country_Id");
            this.AddForeignKey("dbo.Addresses", "Country_Id", "dbo.Countries", "Id");
            this.DropColumn("dbo.EcommerceCartItems", "Coupon_Id");
            this.DropColumn("dbo.EcommerceOrderItems", "OrderCoupon_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "OrderCoupon_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCartItems", "Coupon_Id", c => c.Guid());
            this.DropForeignKey("dbo.EcommerceOrderItemTaxes", "OrderItem_Id", "dbo.EcommerceOrderItems");
            this.DropForeignKey("dbo.Addresses", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceTaxes", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceTaxes", "CartItem_Id", "dbo.EcommerceCartItems");
            this.DropIndex("dbo.EcommerceOrderItemTaxes", new[] { "OrderItem_Id" });
            this.DropIndex("dbo.EcommerceTaxes", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceTaxes", new[] { "CartItem_Id" });
            this.DropIndex("dbo.Addresses", new[] { "Country_Id" });
            this.DropColumn("dbo.EcommerceOrders", "TotalAfterTax");
            this.DropColumn("dbo.EcommerceOrders", "TotalBeforeTax");
            this.DropColumn("dbo.EcommerceOrderShippingBoxes", "ShippingPriceAfterTax");
            this.DropColumn("dbo.EcommerceOrderShippingBoxes", "ShippingPriceBeforeTax");
            this.DropColumn("dbo.EcommerceOrderItems", "TotalAfterTax");
            this.DropColumn("dbo.EcommerceOrderItems", "TotalBeforeTax");
            this.DropColumn("dbo.Countries", "AfterTaxRoundingDigits");
            this.DropColumn("dbo.Countries", "Localize");
            this.DropColumn("dbo.Addresses", "Country_Id");
            this.DropTable("dbo.EcommerceOrderItemTaxes");
            this.DropTable("dbo.EcommerceTaxes");
            this.CreateIndex("dbo.EcommerceOrderItems", "OrderCoupon_Id");
            this.CreateIndex("dbo.EcommerceCartItems", "Coupon_Id");
            this.AddForeignKey("dbo.EcommerceOrderItems", "OrderCoupon_Id", "dbo.EcommerceOrderCoupons", "Id");
            this.AddForeignKey("dbo.EcommerceCartItems", "Coupon_Id", "dbo.EcommerceCoupons", "Id");
        }
    }
}
