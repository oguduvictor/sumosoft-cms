namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Cmssettings_Update : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.CmsSettings", "SmtpHost", c => c.String());
            this.AddColumn("dbo.CmsSettings", "SmtpPort", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.CmsSettings", "SmtpPort");
            this.DropColumn("dbo.CmsSettings", "SmtpHost");
        }
    }
}
