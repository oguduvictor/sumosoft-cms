namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_142b : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceProductVariants", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceProductVariants", new[] { "Country_Id" });
            this.CreateTable(
                "dbo.EcommerceProductVariantLocalizedStocks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ProductVariant_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceProductVariants", t => t.ProductVariant_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ProductVariant_Id);

            this.CreateTable(
                "dbo.EcommerceProductVariantOptionLocalizedStocks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ProductVariantOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceProductVariantOptions", t => t.ProductVariantOption_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ProductVariantOption_Id);

            this.AddColumn("dbo.EcommerceProducts", "GlobalStock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "GlobalStock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariantOptions", "GlobalStock", c => c.Int(nullable: false));
            this.DropColumn("dbo.EcommerceProducts", "Stock");
            this.DropColumn("dbo.EcommerceProductVariants", "Stock");
            this.DropColumn("dbo.EcommerceProductVariants", "Country_Id");
            this.DropColumn("dbo.EcommerceProductVariantOptions", "Stock");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProductVariantOptions", "Stock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "Country_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProductVariants", "Stock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "Stock", c => c.Int(nullable: false));
            this.DropForeignKey("dbo.EcommerceProductVariantOptionLocalizedStocks", "ProductVariantOption_Id", "dbo.EcommerceProductVariantOptions");
            this.DropForeignKey("dbo.EcommerceProductVariantOptionLocalizedStocks", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductVariantLocalizedStocks", "ProductVariant_Id", "dbo.EcommerceProductVariants");
            this.DropForeignKey("dbo.EcommerceProductVariantLocalizedStocks", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceProductVariantOptionLocalizedStocks", new[] { "ProductVariantOption_Id" });
            this.DropIndex("dbo.EcommerceProductVariantOptionLocalizedStocks", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProductVariantLocalizedStocks", new[] { "ProductVariant_Id" });
            this.DropIndex("dbo.EcommerceProductVariantLocalizedStocks", new[] { "Country_Id" });
            this.DropColumn("dbo.EcommerceProductVariantOptions", "GlobalStock");
            this.DropColumn("dbo.EcommerceProductVariants", "GlobalStock");
            this.DropColumn("dbo.EcommerceProducts", "GlobalStock");
            this.DropTable("dbo.EcommerceProductVariantOptionLocalizedStocks");
            this.DropTable("dbo.EcommerceProductVariantLocalizedStocks");
            this.CreateIndex("dbo.EcommerceProductVariants", "Country_Id");
            this.AddForeignKey("dbo.EcommerceProductVariants", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
