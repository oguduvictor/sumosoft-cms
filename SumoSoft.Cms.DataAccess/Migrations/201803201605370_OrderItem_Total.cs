namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class OrderItem_Total : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "TotalBeforeTax", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "TotalAfterTax", c => c.Double(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "TotalAfterTax");
            this.DropColumn("dbo.EcommerceOrderItems", "TotalBeforeTax");
        }
    }
}
