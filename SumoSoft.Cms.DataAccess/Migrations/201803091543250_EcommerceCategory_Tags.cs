namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceCategory_Tags : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategories", "Tags", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCategories", "Tags");
        }
    }
}
