namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ProductStockUnit_ShipsIn : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductStockUnits", "ShipsIn", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitShipsIn", c => c.Int(nullable: false));
            this.DropColumn("dbo.EcommerceProductStockUnits", "ShipsOn");
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitShipsOn");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitShipsOn", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductStockUnits", "ShipsOn", c => c.DateTime());
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitShipsIn");
            this.DropColumn("dbo.EcommerceProductStockUnits", "ShipsIn");
        }
    }
}
