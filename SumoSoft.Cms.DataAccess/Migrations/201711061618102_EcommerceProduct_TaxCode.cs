namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceProduct_TaxCode : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProducts", "TaxCode", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceProducts", "TaxCode");
        }
    }
}
