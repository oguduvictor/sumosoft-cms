namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_146a : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "VariantName", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "VariantLocalizedLabel", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "VariantType", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "VariantPriceModifier", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "VariantOptionLocalizedText", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "VariantOptionPriceModifier", c => c.Double(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "VariantOptionPriceModifier");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "VariantOptionLocalizedText");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "VariantPriceModifier");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "VariantType");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "VariantLocalizedLabel");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "VariantName");
        }
    }
}
