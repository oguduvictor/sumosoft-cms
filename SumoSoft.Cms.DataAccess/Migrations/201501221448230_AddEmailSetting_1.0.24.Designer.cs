// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class AddEmailSetting_1024 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddEmailSetting_1024));
        
        string IMigrationMetadata.Id
        {
            get { return "201501221448230_AddEmailSetting_1.0.24"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
