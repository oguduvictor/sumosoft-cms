namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_125 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.Addresses", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.Addresses", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.Addresses", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.Addresses", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogCategories", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.BlogCategories", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.BlogCategories", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogCategories", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogPosts", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.BlogPosts", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogPosts", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.Users", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.Users", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.Users", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.UserRoles", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.UserRoles", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.UserPermissions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserPermissions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.UserPermissions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.UserPermissions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogComments", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.BlogComments", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.BlogComments", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogComments", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogTags", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.BlogTags", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.BlogTags", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogTags", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.CmsSettings", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.CmsSettings", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.CmsSettings", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.CmsSettings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.ContentSections", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.ContentSections", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.ContentSections", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.Countries", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.Countries", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeDescriptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeDescriptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeDescriptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeDescriptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributes", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceAttributes", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceAttributes", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributes", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeGroups", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeGroups", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeGroups", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeGroups", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeGroupDescriptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeGroupDescriptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeGroupDescriptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeGroupDescriptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategories", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceCategories", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceCategories", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategories", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategoryDescriptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceCategoryDescriptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceCategoryDescriptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategoryDescriptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProducts", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProducts", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceLengthClasses", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceLengthClasses", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceLengthClasses", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceLengthClasses", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceLengthClasses", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceManufacturers", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceManufacturers", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceManufacturers", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceManufacturers", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceWeightClasses", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceWeightClasses", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceWeightClasses", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceWeightClasses", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceWeightClasses", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCountryPrices", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceCountryPrices", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceCountryPrices", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCountryPrices", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptionDescriptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceOptionDescriptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceOptionDescriptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptionDescriptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceOptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceOptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptionValueDescriptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceOptionValueDescriptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceOptionValueDescriptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptionValueDescriptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptionValues", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceOptionValues", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceOptionValues", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOptionValues", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductAttributes", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductAttributes", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProductAttributes", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductAttributes", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductDescriptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductDescriptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProductDescriptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductDescriptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductGroups", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductGroups", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProductGroups", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductGroups", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductImages", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductImages", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProductImages", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductImages", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductOptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductOptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProductOptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductOptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductOptionValues", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductOptionValues", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProductOptionValues", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductOptionValues", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductRelateds", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductRelateds", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceProductRelateds", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductRelateds", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceTagDescriptions", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceTagDescriptions", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceTagDescriptions", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceTagDescriptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceTags", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceTags", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceTags", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceTags", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.MenuLinks", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.MenuLinks", "CreatedDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.MenuLinks", "DeletedDate", c => c.DateTime());
            this.AddColumn("dbo.MenuLinks", "ModificationDate", c => c.DateTime());
            this.DropColumn("dbo.BlogPosts", "Date");
            this.DropColumn("dbo.BlogPosts", "LastChangeDate");
            this.DropColumn("dbo.ContentSections", "LastChangeDate");
            this.DropColumn("dbo.EcommerceCategories", "DateCreated");
            this.DropColumn("dbo.EcommerceCategories", "DateModified");
            this.DropColumn("dbo.EcommerceProducts", "DateCreated");
            this.DropColumn("dbo.EcommerceProducts", "DateModified");
            this.DropColumn("dbo.EcommerceLengthClasses", "Title");
            this.DropColumn("dbo.EcommerceWeightClasses", "Title");
            this.DropColumn("dbo.EcommerceTags", "DateCreated");
            this.DropColumn("dbo.EcommerceTags", "DateModified");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceTags", "DateModified", c => c.DateTime());
            this.AddColumn("dbo.EcommerceTags", "DateCreated", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceWeightClasses", "Title", c => c.String());
            this.AddColumn("dbo.EcommerceLengthClasses", "Title", c => c.String());
            this.AddColumn("dbo.EcommerceProducts", "DateModified", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProducts", "DateCreated", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceCategories", "DateModified", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategories", "DateCreated", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.ContentSections", "LastChangeDate", c => c.DateTime());
            this.AddColumn("dbo.BlogPosts", "LastChangeDate", c => c.DateTime());
            this.AddColumn("dbo.BlogPosts", "Date", c => c.DateTime(nullable: false));
            this.DropColumn("dbo.MenuLinks", "ModificationDate");
            this.DropColumn("dbo.MenuLinks", "DeletedDate");
            this.DropColumn("dbo.MenuLinks", "CreatedDate");
            this.DropColumn("dbo.MenuLinks", "IsDeleted");
            this.DropColumn("dbo.EcommerceTags", "ModificationDate");
            this.DropColumn("dbo.EcommerceTags", "DeletedDate");
            this.DropColumn("dbo.EcommerceTags", "CreatedDate");
            this.DropColumn("dbo.EcommerceTags", "IsDeleted");
            this.DropColumn("dbo.EcommerceTagDescriptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceTagDescriptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceTagDescriptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceTagDescriptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceProductRelateds", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductRelateds", "DeletedDate");
            this.DropColumn("dbo.EcommerceProductRelateds", "CreatedDate");
            this.DropColumn("dbo.EcommerceProductRelateds", "IsDeleted");
            this.DropColumn("dbo.EcommerceProductOptionValues", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductOptionValues", "DeletedDate");
            this.DropColumn("dbo.EcommerceProductOptionValues", "CreatedDate");
            this.DropColumn("dbo.EcommerceProductOptionValues", "IsDeleted");
            this.DropColumn("dbo.EcommerceProductOptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductOptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceProductOptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceProductOptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceProductImages", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductImages", "DeletedDate");
            this.DropColumn("dbo.EcommerceProductImages", "CreatedDate");
            this.DropColumn("dbo.EcommerceProductImages", "IsDeleted");
            this.DropColumn("dbo.EcommerceProductGroups", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductGroups", "DeletedDate");
            this.DropColumn("dbo.EcommerceProductGroups", "CreatedDate");
            this.DropColumn("dbo.EcommerceProductGroups", "IsDeleted");
            this.DropColumn("dbo.EcommerceProductDescriptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductDescriptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceProductDescriptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceProductDescriptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceProductAttributes", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductAttributes", "DeletedDate");
            this.DropColumn("dbo.EcommerceProductAttributes", "CreatedDate");
            this.DropColumn("dbo.EcommerceProductAttributes", "IsDeleted");
            this.DropColumn("dbo.EcommerceOptionValues", "ModificationDate");
            this.DropColumn("dbo.EcommerceOptionValues", "DeletedDate");
            this.DropColumn("dbo.EcommerceOptionValues", "CreatedDate");
            this.DropColumn("dbo.EcommerceOptionValues", "IsDeleted");
            this.DropColumn("dbo.EcommerceOptionValueDescriptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceOptionValueDescriptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceOptionValueDescriptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceOptionValueDescriptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceOptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceOptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceOptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceOptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceOptionDescriptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceOptionDescriptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceOptionDescriptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceOptionDescriptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceCountryPrices", "ModificationDate");
            this.DropColumn("dbo.EcommerceCountryPrices", "DeletedDate");
            this.DropColumn("dbo.EcommerceCountryPrices", "CreatedDate");
            this.DropColumn("dbo.EcommerceCountryPrices", "IsDeleted");
            this.DropColumn("dbo.EcommerceWeightClasses", "ModificationDate");
            this.DropColumn("dbo.EcommerceWeightClasses", "DeletedDate");
            this.DropColumn("dbo.EcommerceWeightClasses", "CreatedDate");
            this.DropColumn("dbo.EcommerceWeightClasses", "IsDeleted");
            this.DropColumn("dbo.EcommerceWeightClasses", "Name");
            this.DropColumn("dbo.EcommerceManufacturers", "ModificationDate");
            this.DropColumn("dbo.EcommerceManufacturers", "DeletedDate");
            this.DropColumn("dbo.EcommerceManufacturers", "CreatedDate");
            this.DropColumn("dbo.EcommerceManufacturers", "IsDeleted");
            this.DropColumn("dbo.EcommerceLengthClasses", "ModificationDate");
            this.DropColumn("dbo.EcommerceLengthClasses", "DeletedDate");
            this.DropColumn("dbo.EcommerceLengthClasses", "CreatedDate");
            this.DropColumn("dbo.EcommerceLengthClasses", "IsDeleted");
            this.DropColumn("dbo.EcommerceLengthClasses", "Name");
            this.DropColumn("dbo.EcommerceProducts", "ModificationDate");
            this.DropColumn("dbo.EcommerceProducts", "DeletedDate");
            this.DropColumn("dbo.EcommerceProducts", "CreatedDate");
            this.DropColumn("dbo.EcommerceCategoryDescriptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceCategoryDescriptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceCategoryDescriptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceCategoryDescriptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceCategories", "ModificationDate");
            this.DropColumn("dbo.EcommerceCategories", "DeletedDate");
            this.DropColumn("dbo.EcommerceCategories", "CreatedDate");
            this.DropColumn("dbo.EcommerceCategories", "IsDeleted");
            this.DropColumn("dbo.EcommerceAttributeGroupDescriptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributeGroupDescriptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceAttributeGroupDescriptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceAttributeGroupDescriptions", "IsDeleted");
            this.DropColumn("dbo.EcommerceAttributeGroups", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributeGroups", "DeletedDate");
            this.DropColumn("dbo.EcommerceAttributeGroups", "CreatedDate");
            this.DropColumn("dbo.EcommerceAttributeGroups", "IsDeleted");
            this.DropColumn("dbo.EcommerceAttributes", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributes", "DeletedDate");
            this.DropColumn("dbo.EcommerceAttributes", "CreatedDate");
            this.DropColumn("dbo.EcommerceAttributes", "IsDeleted");
            this.DropColumn("dbo.EcommerceAttributeDescriptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributeDescriptions", "DeletedDate");
            this.DropColumn("dbo.EcommerceAttributeDescriptions", "CreatedDate");
            this.DropColumn("dbo.EcommerceAttributeDescriptions", "IsDeleted");
            this.DropColumn("dbo.Countries", "ModificationDate");
            this.DropColumn("dbo.Countries", "DeletedDate");
            this.DropColumn("dbo.ContentSections", "ModificationDate");
            this.DropColumn("dbo.ContentSections", "DeletedDate");
            this.DropColumn("dbo.ContentSections", "CreatedDate");
            this.DropColumn("dbo.CmsSettings", "ModificationDate");
            this.DropColumn("dbo.CmsSettings", "DeletedDate");
            this.DropColumn("dbo.CmsSettings", "CreatedDate");
            this.DropColumn("dbo.CmsSettings", "IsDeleted");
            this.DropColumn("dbo.BlogTags", "ModificationDate");
            this.DropColumn("dbo.BlogTags", "DeletedDate");
            this.DropColumn("dbo.BlogTags", "CreatedDate");
            this.DropColumn("dbo.BlogTags", "IsDeleted");
            this.DropColumn("dbo.BlogComments", "ModificationDate");
            this.DropColumn("dbo.BlogComments", "DeletedDate");
            this.DropColumn("dbo.BlogComments", "CreatedDate");
            this.DropColumn("dbo.BlogComments", "IsDeleted");
            this.DropColumn("dbo.UserPermissions", "ModificationDate");
            this.DropColumn("dbo.UserPermissions", "DeletedDate");
            this.DropColumn("dbo.UserPermissions", "CreatedDate");
            this.DropColumn("dbo.UserPermissions", "IsDeleted");
            this.DropColumn("dbo.UserRoles", "ModificationDate");
            this.DropColumn("dbo.UserRoles", "DeletedDate");
            this.DropColumn("dbo.UserRoles", "CreatedDate");
            this.DropColumn("dbo.UserRoles", "IsDeleted");
            this.DropColumn("dbo.Users", "ModificationDate");
            this.DropColumn("dbo.Users", "DeletedDate");
            this.DropColumn("dbo.Users", "CreatedDate");
            this.DropColumn("dbo.BlogPosts", "ModificationDate");
            this.DropColumn("dbo.BlogPosts", "DeletedDate");
            this.DropColumn("dbo.BlogPosts", "CreatedDate");
            this.DropColumn("dbo.BlogCategories", "ModificationDate");
            this.DropColumn("dbo.BlogCategories", "DeletedDate");
            this.DropColumn("dbo.BlogCategories", "CreatedDate");
            this.DropColumn("dbo.BlogCategories", "IsDeleted");
            this.DropColumn("dbo.Addresses", "ModificationDate");
            this.DropColumn("dbo.Addresses", "DeletedDate");
            this.DropColumn("dbo.Addresses", "CreatedDate");
            this.DropColumn("dbo.Addresses", "IsDeleted");
        }
    }
}
