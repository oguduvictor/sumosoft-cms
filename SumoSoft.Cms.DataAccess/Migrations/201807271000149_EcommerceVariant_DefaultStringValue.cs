namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceVariant_DefaultStringValue : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceVariants", "DefaultStringValue", c => c.String());
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultStringValue", c => c.String());
            this.AddColumn("dbo.EcommerceCartItemVariants", "StringValue", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemVariants", "StringValue", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItemVariants", "StringValue");
            this.DropColumn("dbo.EcommerceCartItemVariants", "StringValue");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultStringValue");
            this.DropColumn("dbo.EcommerceVariants", "DefaultStringValue");
        }
    }
}
