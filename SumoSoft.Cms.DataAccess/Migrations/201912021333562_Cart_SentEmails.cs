namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cart_SentEmails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SentEmails", "Cart_Id", c => c.Guid());
            CreateIndex("dbo.SentEmails", "Cart_Id");
            AddForeignKey("dbo.SentEmails", "Cart_Id", "dbo.Carts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SentEmails", "Cart_Id", "dbo.Carts");
            DropIndex("dbo.SentEmails", new[] { "Cart_Id" });
            DropColumn("dbo.SentEmails", "Cart_Id");
        }
    }
}
