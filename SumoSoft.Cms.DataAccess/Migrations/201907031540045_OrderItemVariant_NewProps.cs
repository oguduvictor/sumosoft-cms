namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class OrderItemVariant_NewProps : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrderItemVariants", "VariantUrlToken", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemVariants", "VariantLocalizedDescription", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItemVariants", "VariantLocalizedDescription");
            this.DropColumn("dbo.EcommerceOrderItemVariants", "VariantUrlToken");
        }
    }
}
