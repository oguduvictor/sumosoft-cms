namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_139a : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceVariantOptionPriceModifiers",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    PriceModifier = c.Double(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    EcommerceVariantOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceVariantTypeOptions", t => t.EcommerceVariantOption_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.EcommerceVariantOption_Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceVariantOptionPriceModifiers", "EcommerceVariantOption_Id", "dbo.EcommerceVariantTypeOptions");
            this.DropForeignKey("dbo.EcommerceVariantOptionPriceModifiers", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceVariantOptionPriceModifiers", new[] { "EcommerceVariantOption_Id" });
            this.DropIndex("dbo.EcommerceVariantOptionPriceModifiers", new[] { "Country_Id" });
            this.DropTable("dbo.EcommerceVariantOptionPriceModifiers");
        }
    }
}
