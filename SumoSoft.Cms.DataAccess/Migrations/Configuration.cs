namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.Domain;

    public sealed class Configuration : DbMigrationsConfiguration<CmsDbContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CmsDbContext context)
        {
            if (context.UserRoles.Any()) { return; }

            #region Roles

            var systemAdministrator = new UserRole
            {
                Name = SharedConstants.DefaultRoleSystemAdmin,
                AccessAdmin = true
                //Permissions = Enum.GetValues(typeof(UserRolePermissionsEnum)).Cast<UserRolePermissionsEnum>().ToList()
            };

            context.UserRoles.AddOrUpdate(x => new { x.Name }, systemAdministrator);

            var administrator = new UserRole
            {
                Name = SharedConstants.DefaultRoleAdmin,
                AccessAdmin = true

            };

            context.UserRoles.AddOrUpdate(x => new { x.Name }, administrator);

            var roleUser = new UserRole
            {
                Name = SharedConstants.DefaultUserRole
            };

            context.UserRoles.AddOrUpdate(x => new { x.Name }, roleUser);

            #endregion

            #region Country

            var defaultCountry = new Country
            {
                IsDisabled = false,
                Url = string.Empty,
                Name = "United Kingdom",
                LanguageCode = "en-GB",
                IsDefault = true,
                FlagIcon = "flag-icon-gb"
            };

            context.Countries.AddOrUpdate(x => new { x.Name }, defaultCountry);

            #endregion

            #region Seo

            var seoHome = new SeoSection
            {
                IsDisabled = false,
                Page = "home/index",
                SeoSectionLocalizedKits = new List<SeoSectionLocalizedKit>
                {
                    new SeoSectionLocalizedKit
                    {
                        IsDisabled = false,
                        Country = defaultCountry,
                        Title = "Home",
                        Description = string.Empty
                    }
                }
            };

            context.SeoSections.AddOrUpdate(seoHome);

            var seoAccount = new SeoSection
            {
                IsDisabled = false,
                Page = "account/login",
                SeoSectionLocalizedKits = new List<SeoSectionLocalizedKit>
                {
                    new SeoSectionLocalizedKit
                    {
                        IsDisabled = false,
                        Country = defaultCountry,
                        Title = "Login",
                        Description = string.Empty
                    }
                }
            };

            context.SeoSections.AddOrUpdate(seoAccount);

            #endregion

            #region Setting

            var dbSetting = new CmsSettings
            {
                IsSeeded = true
            };

            context.CmsSettings.AddOrUpdate(dbSetting);

            #endregion

            context.SaveChanges();
        }
    }
}