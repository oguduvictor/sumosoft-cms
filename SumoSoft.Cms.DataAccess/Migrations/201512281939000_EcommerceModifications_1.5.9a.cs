namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_159a : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrders", "IsTrial", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceOrderStatus", "SortOrder", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderStatus", "SortOrder");
            this.DropColumn("dbo.EcommerceOrders", "IsTrial");
        }
    }
}
