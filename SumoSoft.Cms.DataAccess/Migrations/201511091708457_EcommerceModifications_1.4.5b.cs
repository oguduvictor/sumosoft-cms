namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_145b : DbMigration
    {
        public override void Up()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceOptions_dbo.EcommerceOrderLines_EcommerceOrderLine_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceVariants] DROP [FK_dbo.EcommerceOptions_dbo.EcommerceOrderLines_EcommerceOrderLine_Id]");
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceOrderItems_dbo.EcommerceOrders_Order_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceOrderItems] DROP [FK_dbo.EcommerceOrderItems_dbo.EcommerceOrders_Order_Id]");

            this.DropForeignKey("dbo.EcommerceOrders", "CartReference_Id", "dbo.EcommerceCarts");
            this.DropForeignKey("dbo.EcommerceOrders", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropForeignKey("dbo.EcommerceOrderLines", "Order_Id", "dbo.EcommerceOrders");
            this.DropForeignKey("dbo.EcommerceOrderLines", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceVariants", "EcommerceOrderLine_Id", "dbo.EcommerceOrderLines");
            this.DropIndex("dbo.EcommerceVariants", new[] { "EcommerceOrderLine_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "CartReference_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "Coupon_Id" });
            this.DropIndex("dbo.EcommerceOrderLines", new[] { "Order_Id" });
            this.DropIndex("dbo.EcommerceOrderLines", new[] { "Product_Id" });
            this.DropPrimaryKey("dbo.EcommerceOrders");
            this.CreateTable(
                "dbo.EcommerceOrderItems",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    ProductUrl = c.String(),
                    ProductName = c.String(),
                    ProductPrice = c.Double(nullable: false),
                    ProductQuantity = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Order_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOrders", t => t.Order_Id)
                .Index(t => t.Order_Id);

            this.CreateTable(
                "dbo.EcommerceOrderItemProductVariants",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    CheckboxValue = c.Boolean(nullable: false),
                    DoubleValue = c.Double(nullable: false),
                    VariantOptionSelectValue = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    EcommerceOrderItem_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOrderItems", t => t.EcommerceOrderItem_Id)
                .Index(t => t.EcommerceOrderItem_Id);

            this.CreateTable(
                "dbo.EcommerceOrderShippingAddresses",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    AddressFirstName = c.String(),
                    AddressLastName = c.String(),
                    AddressLine1 = c.String(),
                    AddressLine2 = c.String(),
                    CountryName = c.String(),
                    City = c.String(),
                    Postcode = c.String(),
                    PhoneNumber = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceOrders", "CountryLanguageCode", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "UserFirstName", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "UserLastName", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "UserEmail", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "TransactionId", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "ShippingAddress_Id", c => c.Guid());
            this.AlterColumn("dbo.EcommerceOrders", "Id", c => c.Guid(nullable: false, identity: true));
            this.AddPrimaryKey("dbo.EcommerceOrders", "Id");
            this.CreateIndex("dbo.EcommerceOrders", "ShippingAddress_Id");
            this.AddForeignKey("dbo.EcommerceOrders", "ShippingAddress_Id", "dbo.EcommerceOrderShippingAddresses", "Id");
            this.DropColumn("dbo.EcommerceVariants", "EcommerceOrderLine_Id");
            this.DropColumn("dbo.EcommerceOrders", "FirstName");
            this.DropColumn("dbo.EcommerceOrders", "LastName");
            this.DropColumn("dbo.EcommerceOrders", "Email");
            this.DropColumn("dbo.EcommerceOrders", "AddressFirstName");
            this.DropColumn("dbo.EcommerceOrders", "AddressLastName");
            this.DropColumn("dbo.EcommerceOrders", "AddressLine1");
            this.DropColumn("dbo.EcommerceOrders", "AddressLine2");
            this.DropColumn("dbo.EcommerceOrders", "City");
            this.DropColumn("dbo.EcommerceOrders", "Postcode");
            this.DropColumn("dbo.EcommerceOrders", "PhoneNumber");
            this.DropColumn("dbo.EcommerceOrders", "PayPalTransactionId");
            this.DropColumn("dbo.EcommerceOrders", "PayPalReceiptId");
            this.DropColumn("dbo.EcommerceOrders", "PayPalPaymentStatus");
            this.DropColumn("dbo.EcommerceOrders", "PayPalPaymentStatusDate");
            this.DropColumn("dbo.EcommerceOrders", "CartReference_Id");
            this.DropColumn("dbo.EcommerceOrders", "Coupon_Id");
            this.DropTable("dbo.EcommerceCoupons");
            this.DropTable("dbo.EcommerceOrderLines");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceOrderLines",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ProductQuantity = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Order_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceCoupons",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    DiscountAmount = c.Int(nullable: false),
                    Type = c.Int(nullable: false),
                    Code = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceOrders", "Coupon_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrders", "CartReference_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrders", "PayPalPaymentStatusDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrders", "PayPalPaymentStatus", c => c.Int());
            this.AddColumn("dbo.EcommerceOrders", "PayPalReceiptId", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "PayPalTransactionId", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "PhoneNumber", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "Postcode", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "City", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "AddressLine2", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "AddressLine1", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "AddressLastName", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "AddressFirstName", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "Email", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "LastName", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "FirstName", c => c.String());
            this.AddColumn("dbo.EcommerceVariants", "EcommerceOrderLine_Id", c => c.Guid());
            this.DropForeignKey("dbo.EcommerceOrders", "ShippingAddress_Id", "dbo.EcommerceOrderShippingAddresses");
            this.DropForeignKey("dbo.EcommerceOrderItemProductVariants", "EcommerceOrderItem_Id", "dbo.EcommerceOrderItems");
            this.DropForeignKey("dbo.EcommerceOrderItems", "Order_Id", "dbo.EcommerceOrders");
            this.DropIndex("dbo.EcommerceOrderItemProductVariants", new[] { "EcommerceOrderItem_Id" });
            this.DropIndex("dbo.EcommerceOrderItems", new[] { "Order_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "ShippingAddress_Id" });
            this.DropPrimaryKey("dbo.EcommerceOrders");
            this.AlterColumn("dbo.EcommerceOrders", "Id", c => c.Guid(nullable: false));
            this.DropColumn("dbo.EcommerceOrders", "ShippingAddress_Id");
            this.DropColumn("dbo.EcommerceOrders", "TransactionId");
            this.DropColumn("dbo.EcommerceOrders", "UserEmail");
            this.DropColumn("dbo.EcommerceOrders", "UserLastName");
            this.DropColumn("dbo.EcommerceOrders", "UserFirstName");
            this.DropColumn("dbo.EcommerceOrders", "CountryLanguageCode");
            this.DropTable("dbo.EcommerceOrderShippingAddresses");
            this.DropTable("dbo.EcommerceOrderItemProductVariants");
            this.DropTable("dbo.EcommerceOrderItems");
            this.AddPrimaryKey("dbo.EcommerceOrders", "Id");
            this.CreateIndex("dbo.EcommerceOrderLines", "Product_Id");
            this.CreateIndex("dbo.EcommerceOrderLines", "Order_Id");
            this.CreateIndex("dbo.EcommerceOrders", "Coupon_Id");
            this.CreateIndex("dbo.EcommerceOrders", "CartReference_Id");
            this.CreateIndex("dbo.EcommerceVariants", "EcommerceOrderLine_Id");
            this.AddForeignKey("dbo.EcommerceVariants", "EcommerceOrderLine_Id", "dbo.EcommerceOrderLines", "Id");
            this.AddForeignKey("dbo.EcommerceOrderLines", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceOrderLines", "Order_Id", "dbo.EcommerceOrders", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "Coupon_Id", "dbo.EcommerceCoupons", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "CartReference_Id", "dbo.EcommerceCarts", "Id");
        }
    }
}
