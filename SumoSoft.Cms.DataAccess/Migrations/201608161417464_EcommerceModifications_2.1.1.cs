namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_211 : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.ContentSectionLocalizedStrings", newName: "ContentSectionLocalizedKits");
            this.RenameTable(name: "dbo.EcommerceAttributeLocalizedStrings", newName: "EcommerceAttributeLocalizedKits");
            this.RenameTable(name: "dbo.EcommerceAttributeOptionLocalizedStrings", newName: "EcommerceAttributeOptionLocalizedKits");
            this.RenameTable(name: "dbo.EcommerceCategoryLocalizedStrings", newName: "EcommerceCategoryLocalizedKits");
            this.RenameTable(name: "dbo.EcommerceOrderStatusLocalizedStrings", newName: "EcommerceOrderStatusLocalizedKits");
            this.RenameTable(name: "dbo.EcommerceProductVariantLocalizedStocks", newName: "EcommerceProductVariantLocalizedKits");
            this.RenameTable(name: "dbo.EcommerceProductVariantOptionLocalizedStocks", newName: "EcommerceProductVariantOptionLocalizedKits");
            this.RenameTable(name: "dbo.EmailLocalizedStrings", newName: "EmailLocalizedKits");
            this.RenameTable(name: "dbo.SeoSectionLocalizedStrings", newName: "SeoSectionLocalizedKits");

            this.RenameTable(name: "dbo.EcommerceProductLocalizedStrings", newName: "EcommerceProductLocalizedKits");
            this.AddColumn("dbo.EcommerceProductLocalizedKits", "BasePrice", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProductLocalizedKits", "Stock", c => c.Int(nullable: false));

            this.Sql("UPDATE [dbo].[EcommerceProductLocalizedKits]" +
                "SET [dbo].[EcommerceProductLocalizedKits].[BasePrice] = [dbo].[EcommerceProductLocalizedPrices].[BasePrice]" +
                "FROM [dbo].[EcommerceProductLocalizedKits]" +
                "INNER JOIN [dbo].[EcommerceProductLocalizedPrices]" +
                "ON [dbo].[EcommerceProductLocalizedKits].[Country_Id] = [dbo].[EcommerceProductLocalizedPrices].[Country_Id]" +
                "AND [dbo].[EcommerceProductLocalizedKits].[Product_Id] = [dbo].[EcommerceProductLocalizedPrices].[Product_Id]");

            this.Sql("UPDATE [dbo].[EcommerceProductLocalizedKits]" +
                "SET [dbo].[EcommerceProductLocalizedKits].[Stock] = [dbo].[EcommerceProductLocalizedStocks].[Stock]" +
                "FROM [dbo].[EcommerceProductLocalizedKits]" +
                "INNER JOIN [dbo].[EcommerceProductLocalizedStocks]" +
                "ON [dbo].[EcommerceProductLocalizedKits].[Country_Id] = [dbo].[EcommerceProductLocalizedStocks].[Country_Id]" +
                "AND [dbo].[EcommerceProductLocalizedKits].[Product_Id] = [dbo].[EcommerceProductLocalizedStocks].[Product_Id]");

            this.RenameTable(name: "dbo.EcommerceVariantLocalizedStrings", newName: "EcommerceVariantLocalizedKits");
            this.AddColumn("dbo.EcommerceVariantLocalizedKits", "PriceModifier", c => c.Double(nullable: false));

            this.Sql("UPDATE [dbo].[EcommerceVariantLocalizedKits]" +
                "SET [dbo].[EcommerceVariantLocalizedKits].[PriceModifier] = [dbo].[EcommerceVariantPriceModifiers].[PriceModifier]" +
                "FROM [dbo].[EcommerceVariantLocalizedKits]" +
                "INNER JOIN [dbo].[EcommerceVariantPriceModifiers]" +
                "ON [dbo].[EcommerceVariantLocalizedKits].[Country_Id] = [dbo].[EcommerceVariantPriceModifiers].[Country_Id]" +
                "AND [dbo].[EcommerceVariantLocalizedKits].[Variant_Id] = [dbo].[EcommerceVariantPriceModifiers].[Variant_Id]");

            this.RenameTable(name: "dbo.EcommerceVariantOptionLocalizedStrings", newName: "EcommerceVariantOptionLocalizedKits");
            this.AddColumn("dbo.EcommerceVariantOptionLocalizedKits", "PriceModifier", c => c.Double(nullable: false));

            this.Sql("UPDATE [dbo].[EcommerceVariantOptionLocalizedKits]" +
                "SET [dbo].[EcommerceVariantOptionLocalizedKits].[PriceModifier] = [dbo].[EcommerceVariantOptionPriceModifiers].[PriceModifier]" +
                "FROM [dbo].[EcommerceVariantOptionLocalizedKits]" +
                "INNER JOIN [dbo].[EcommerceVariantOptionPriceModifiers]" +
                "ON [dbo].[EcommerceVariantOptionLocalizedKits].[Country_Id] = [dbo].[EcommerceVariantOptionPriceModifiers].[Country_Id]" +
                "AND [dbo].[EcommerceVariantOptionLocalizedKits].[EcommerceVariantOption_Id] = [dbo].[EcommerceVariantOptionPriceModifiers].[EcommerceVariantOption_Id]");

            this.CreateTable(
                "dbo.UserLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.User_Id);

            this.CreateTable(
                "dbo.EcommerceUserCredits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Amount = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    LocalizedKit_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserLocalizedKits", t => t.LocalizedKit_Id)
                .Index(t => t.LocalizedKit_Id);

            this.DropTable("dbo.EcommerceProductLocalizedPrices");
            this.DropTable("dbo.EcommerceProductLocalizedStocks");
            this.DropTable("dbo.EcommerceVariantPriceModifiers");
            this.DropTable("dbo.EcommerceVariantOptionPriceModifiers");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceProductLocalizedPrices",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    BasePrice = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateIndex("dbo.EcommerceProductLocalizedPrices", "Product_Id");
            this.CreateIndex("dbo.EcommerceProductLocalizedPrices", "Country_Id");
            this.AddForeignKey("dbo.EcommerceProductLocalizedPrices", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceProductLocalizedPrices", "Country_Id", "dbo.Countries", "Id");

            this.CreateTable(
                "dbo.EcommerceProductLocalizedStocks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateIndex("dbo.EcommerceProductLocalizedStocks", "Product_Id");
            this.CreateIndex("dbo.EcommerceProductLocalizedStocks", "Country_Id");
            this.AddForeignKey("dbo.EcommerceProductLocalizedStocks", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceProductLocalizedStocks", "Country_Id", "dbo.Countries", "Id");

            this.CreateTable(
                "dbo.EcommerceVariantPriceModifiers",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    PriceModifier = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Variant_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateIndex("dbo.EcommerceVariantPriceModifiers", "Variant_Id");
            this.CreateIndex("dbo.EcommerceVariantPriceModifiers", "Country_Id");
            this.AddForeignKey("dbo.EcommerceVariantPriceModifiers", "Variant_Id", "dbo.EcommerceVariants", "Id");
            this.AddForeignKey("dbo.EcommerceVariantPriceModifiers", "Country_Id", "dbo.Countries", "Id");

            this.CreateTable(
                "dbo.EcommerceVariantOptionPriceModifiers",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    PriceModifier = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    EcommerceVariantOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateIndex("dbo.EcommerceVariantOptionPriceModifiers", "EcommerceVariantOption_Id");
            this.CreateIndex("dbo.EcommerceVariantOptionPriceModifiers", "Country_Id");
            this.AddForeignKey("dbo.EcommerceVariantOptionPriceModifiers", "EcommerceVariantOption_Id", "dbo.EcommerceVariantOptions", "Id");
            this.AddForeignKey("dbo.EcommerceVariantOptionPriceModifiers", "Country_Id", "dbo.Countries", "Id");

            this.RenameTable(name: "dbo.ContentSectionLocalizedKits", newName: "ContentSectionLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceAttributeLocalizedKits", newName: "EcommerceAttributeLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceAttributeOptionLocalizedKits", newName: "EcommerceAttributeOptionLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceCategoryLocalizedKits", newName: "EcommerceCategoryLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceOrderStatusLocalizedKits", newName: "EcommerceOrderStatusLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceProductVariantLocalizedKits", newName: "EcommerceProductVariantLocalizedStocks");
            this.RenameTable(name: "dbo.EcommerceProductVariantOptionLocalizedKits", newName: "EcommerceProductVariantOptionLocalizedStocks");
            this.RenameTable(name: "dbo.EmailLocalizedKits", newName: "EmailLocalizedStrings");
            this.RenameTable(name: "dbo.SeoSectionLocalizedKits", newName: "SeoSectionLocalizedStrings");

            this.DropColumn("dbo.EcommerceProductLocalizedKits", "BasePrice");
            this.DropColumn("dbo.EcommerceProductLocalizedKits", "Stock");
            this.RenameTable(name: "dbo.EcommerceProductLocalizedKits", newName: "EcommerceProductLocalizedStrings");

            this.DropColumn("dbo.EcommerceVariantLocalizedKits", "PriceModifier");
            this.RenameTable(name: "dbo.EcommerceVariantLocalizedKits", newName: "EcommerceVariantLocalizedStrings");

            this.DropColumn("dbo.EcommerceVariantOptionLocalizedKits", "PriceModifier");
            this.RenameTable(name: "dbo.EcommerceVariantOptionLocalizedKits", newName: "EcommerceVariantOptionLocalizedStrings");

            this.DropForeignKey("dbo.EcommerceUserCredits", "LocalizedKit_Id", "dbo.UserLocalizedKits");
            this.DropIndex("dbo.EcommerceUserCredits", new[] { "LocalizedKit_Id" });
            this.DropTable("dbo.EcommerceUserCredits");

            this.DropForeignKey("dbo.EcommerceUserCredits", "LocalizedKit_Id", "dbo.UserLocalizedKits");
            this.DropForeignKey("dbo.UserLocalizedKits", "User_Id", "dbo.Users");
            this.DropForeignKey("dbo.UserLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.UserLocalizedKits", new[] { "User_Id" });
            this.DropIndex("dbo.UserLocalizedKits", new[] { "Country_Id" });
            this.DropTable("dbo.UserLocalizedKits");
        }
    }
}
