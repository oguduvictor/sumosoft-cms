namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceCategory_Image : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategories", "Image", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCategories", "Image");
        }
    }
}
