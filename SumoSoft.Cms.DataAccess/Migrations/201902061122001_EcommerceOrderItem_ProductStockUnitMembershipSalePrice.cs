namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class EcommerceOrderItem_ProductStockUnitMembershipSalePrice : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitMembershipSalePrice", c => c.Double(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitMembershipSalePrice");
        }
    }
}
