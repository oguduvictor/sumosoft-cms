namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_147a : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCartItemProductVariants", "IntegerValue", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "IntegerValue", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "IntegerValue");
            this.DropColumn("dbo.EcommerceCartItemProductVariants", "IntegerValue");
        }
    }
}
