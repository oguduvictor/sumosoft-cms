namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class VariantSelector : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceVariants", "Selector", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceVariants", "Selector");
        }
    }
}
