namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_138b : DbMigration
    {
        public override void Up()
        {
            this.Sql("ALTER TABLE [EcommerceVariantGroupLocalizedStrings] DROP [FK_dbo.EcommerceOptionGroupLocalizedStrings_dbo.EcommerceOptionGroups_Group_Id]");
            this.Sql("ALTER TABLE [EcommerceVariants] DROP [FK_dbo.EcommerceOptions_dbo.EcommerceOptionGroups_Group_Id]");


            this.CreateTable(
                "dbo.EcommerceVariantTypeOptionLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Text = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    EcommerceVariantTypeOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceVariantTypeOptions", t => t.EcommerceVariantTypeOption_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.EcommerceVariantTypeOption_Id);

            this.CreateTable(
                "dbo.EcommerceVariantTypeOptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Variant_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceVariants", t => t.Variant_Id)
                .Index(t => t.Variant_Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceVariantTypeOptions", "Variant_Id", "dbo.EcommerceVariants");
            this.DropForeignKey("dbo.EcommerceVariantTypeOptionLocalizedStrings", "EcommerceVariantTypeOption_Id", "dbo.EcommerceVariantTypeOptions");
            this.DropForeignKey("dbo.EcommerceVariantTypeOptionLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceVariantTypeOptions", new[] { "Variant_Id" });
            this.DropIndex("dbo.EcommerceVariantTypeOptionLocalizedStrings", new[] { "EcommerceVariantTypeOption_Id" });
            this.DropIndex("dbo.EcommerceVariantTypeOptionLocalizedStrings", new[] { "Country_Id" });
            this.DropTable("dbo.EcommerceVariantTypeOptions");
            this.DropTable("dbo.EcommerceVariantTypeOptionLocalizedStrings");
        }
    }
}
