namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceInitialCreate : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceAttributeDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.Int(nullable: false),
                    Attribute_Id = c.Guid(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceAttributes", t => t.Attribute_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Attribute_Id)
                .Index(t => t.Country_Id);

            this.CreateTable(
                "dbo.EcommerceAttributes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    SortOrder = c.Int(nullable: false),
                    AttributeGroup_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceAttributeGroups", t => t.AttributeGroup_Id)
                .Index(t => t.AttributeGroup_Id);

            this.CreateTable(
                "dbo.EcommerceAttributeGroups",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    SortOrder = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceAttributeGroupDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    AttributeGroup_Id = c.Guid(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceAttributeGroups", t => t.AttributeGroup_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.AttributeGroup_Id)
                .Index(t => t.Country_Id);

            this.CreateTable(
                "dbo.EcommerceCategories",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Image = c.String(),
                    SortOrder = c.Int(nullable: false),
                    DateCreated = c.DateTime(nullable: false),
                    DateModified = c.DateTime(),
                    Parent_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceCategories", t => t.Parent_Id)
                .Index(t => t.Parent_Id);

            this.CreateTable(
                "dbo.EcommerceCategoryDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Category_Id = c.Guid(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceCategories", t => t.Category_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.Country_Id);

            this.CreateTable(
                "dbo.EcommerceProducts",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Model = c.String(),
                    Stock = c.Int(nullable: false),
                    Image = c.String(),
                    Vat = c.Single(nullable: false),
                    Weight = c.Double(nullable: false),
                    Length = c.Double(nullable: false),
                    Width = c.Double(nullable: false),
                    Height = c.Double(nullable: false),
                    SortOrder = c.Int(nullable: false),
                    DateCreated = c.DateTime(nullable: false),
                    DateModified = c.DateTime(),
                    IsDeleted = c.Boolean(nullable: false),
                    LengthClass_Id = c.Guid(),
                    Manufacturer_Id = c.Guid(),
                    WeightClass_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceLengthClasses", t => t.LengthClass_Id)
                .ForeignKey("dbo.EcommerceManufacturers", t => t.Manufacturer_Id)
                .ForeignKey("dbo.EcommerceWeightClasses", t => t.WeightClass_Id)
                .Index(t => t.LengthClass_Id)
                .Index(t => t.Manufacturer_Id)
                .Index(t => t.WeightClass_Id);

            this.CreateTable(
                "dbo.EcommerceLengthClasses",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Unit = c.String(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Country_Id);

            this.CreateTable(
                "dbo.EcommerceManufacturers",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Image = c.String(),
                    SortOrder = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceWeightClasses",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Unit = c.String(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Country_Id);

            this.CreateTable(
                "dbo.EcommerceCountryPrices",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    BasePrice = c.Double(nullable: false),
                    Country_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceOptionDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Country_Id = c.Guid(),
                    Option_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceOptions", t => t.Option_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Option_Id);

            this.CreateTable(
                "dbo.EcommerceOptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Type = c.Int(nullable: false),
                    SortOrder = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOptionValueDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Country_Id = c.Guid(),
                    Option_Id = c.Guid(),
                    OptionValue_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceOptions", t => t.Option_Id)
                .ForeignKey("dbo.EcommerceOptionValues", t => t.OptionValue_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Option_Id)
                .Index(t => t.OptionValue_Id);

            this.CreateTable(
                "dbo.EcommerceOptionValues",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Image = c.String(),
                    SortOrder = c.Int(nullable: false),
                    Option_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOptions", t => t.Option_Id)
                .Index(t => t.Option_Id);

            this.CreateTable(
                "dbo.EcommerceProductAttributes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Text = c.String(),
                    Attribute_Id = c.Guid(),
                    Country_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Attribute_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Attribute_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceProductDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Description = c.String(),
                    MetaTitle = c.String(),
                    MetaDescription = c.String(),
                    MetaKeyboard = c.String(),
                    Country_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceProductGroups",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Product_Id = c.Guid(),
                    ProductSameGroup_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.ProductSameGroup_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.ProductSameGroup_Id);

            this.CreateTable(
                "dbo.EcommerceProductImages",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Image = c.String(),
                    SortOrder = c.Int(nullable: false),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceProductOptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.String(),
                    IsRequired = c.Boolean(nullable: false),
                    Option_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOptions", t => t.Option_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Option_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceProductOptionValues",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.String(),
                    IsRequired = c.Boolean(nullable: false),
                    Option_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOptions", t => t.Option_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Option_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceProductRelateds",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Product_Id = c.Guid(),
                    ProductRelated_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.ProductRelated_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.ProductRelated_Id);

            this.CreateTable(
                "dbo.EcommerceTagDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Country_Id = c.Guid(),
                    Tag_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceTags", t => t.Tag_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Tag_Id);

            this.CreateTable(
                "dbo.EcommerceTags",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    SortOrder = c.Int(nullable: false),
                    DateCreated = c.DateTime(nullable: false),
                    DateModified = c.DateTime(),
                    Parent_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceTags", t => t.Parent_Id)
                .Index(t => t.Parent_Id);

            this.CreateTable(
                "dbo.EcommerceProductEcommerceCategories",
                c => new
                {
                    EcommerceProduct_Id = c.Guid(nullable: false),
                    EcommerceCategory_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceProduct_Id, t.EcommerceCategory_Id })
                .ForeignKey("dbo.EcommerceProducts", t => t.EcommerceProduct_Id, cascadeDelete: true)
                .ForeignKey("dbo.EcommerceCategories", t => t.EcommerceCategory_Id, cascadeDelete: true)
                .Index(t => t.EcommerceProduct_Id)
                .Index(t => t.EcommerceCategory_Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceTagDescriptions", "Tag_Id", "dbo.EcommerceTags");
            this.DropForeignKey("dbo.EcommerceTags", "Parent_Id", "dbo.EcommerceTags");
            this.DropForeignKey("dbo.EcommerceTagDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductRelateds", "ProductRelated_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductRelateds", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductOptionValues", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductOptionValues", "Option_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceProductOptions", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductOptions", "Option_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceProductImages", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductGroups", "ProductSameGroup_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductGroups", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductDescriptions", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductAttributes", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductAttributes", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductAttributes", "Attribute_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceOptionValueDescriptions", "OptionValue_Id", "dbo.EcommerceOptionValues");
            this.DropForeignKey("dbo.EcommerceOptionValues", "Option_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceOptionValueDescriptions", "Option_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceOptionValueDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceOptionDescriptions", "Option_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceOptionDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceCountryPrices", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceCountryPrices", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProducts", "WeightClass_Id", "dbo.EcommerceWeightClasses");
            this.DropForeignKey("dbo.EcommerceWeightClasses", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProducts", "Manufacturer_Id", "dbo.EcommerceManufacturers");
            this.DropForeignKey("dbo.EcommerceProducts", "LengthClass_Id", "dbo.EcommerceLengthClasses");
            this.DropForeignKey("dbo.EcommerceLengthClasses", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductEcommerceCategories", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceProductEcommerceCategories", "EcommerceProduct_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceCategories", "Parent_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCategoryDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceCategoryDescriptions", "Category_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceAttributeDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceAttributes", "AttributeGroup_Id", "dbo.EcommerceAttributeGroups");
            this.DropForeignKey("dbo.EcommerceAttributeGroupDescriptions", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceAttributeGroupDescriptions", "AttributeGroup_Id", "dbo.EcommerceAttributeGroups");
            this.DropForeignKey("dbo.EcommerceAttributeDescriptions", "Attribute_Id", "dbo.EcommerceAttributes");
            this.DropIndex("dbo.EcommerceProductEcommerceCategories", new[] { "EcommerceCategory_Id" });
            this.DropIndex("dbo.EcommerceProductEcommerceCategories", new[] { "EcommerceProduct_Id" });
            this.DropIndex("dbo.EcommerceTags", new[] { "Parent_Id" });
            this.DropIndex("dbo.EcommerceTagDescriptions", new[] { "Tag_Id" });
            this.DropIndex("dbo.EcommerceTagDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProductRelateds", new[] { "ProductRelated_Id" });
            this.DropIndex("dbo.EcommerceProductRelateds", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductOptionValues", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductOptionValues", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceProductOptions", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductOptions", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceProductImages", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductGroups", new[] { "ProductSameGroup_Id" });
            this.DropIndex("dbo.EcommerceProductGroups", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductDescriptions", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProductAttributes", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductAttributes", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProductAttributes", new[] { "Attribute_Id" });
            this.DropIndex("dbo.EcommerceOptionValues", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceOptionValueDescriptions", new[] { "OptionValue_Id" });
            this.DropIndex("dbo.EcommerceOptionValueDescriptions", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceOptionValueDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceOptionDescriptions", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceOptionDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceCountryPrices", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceCountryPrices", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceWeightClasses", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceLengthClasses", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProducts", new[] { "WeightClass_Id" });
            this.DropIndex("dbo.EcommerceProducts", new[] { "Manufacturer_Id" });
            this.DropIndex("dbo.EcommerceProducts", new[] { "LengthClass_Id" });
            this.DropIndex("dbo.EcommerceCategoryDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceCategoryDescriptions", new[] { "Category_Id" });
            this.DropIndex("dbo.EcommerceCategories", new[] { "Parent_Id" });
            this.DropIndex("dbo.EcommerceAttributeGroupDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceAttributeGroupDescriptions", new[] { "AttributeGroup_Id" });
            this.DropIndex("dbo.EcommerceAttributes", new[] { "AttributeGroup_Id" });
            this.DropIndex("dbo.EcommerceAttributeDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceAttributeDescriptions", new[] { "Attribute_Id" });
            this.DropTable("dbo.EcommerceProductEcommerceCategories");
            this.DropTable("dbo.EcommerceTags");
            this.DropTable("dbo.EcommerceTagDescriptions");
            this.DropTable("dbo.EcommerceProductRelateds");
            this.DropTable("dbo.EcommerceProductOptionValues");
            this.DropTable("dbo.EcommerceProductOptions");
            this.DropTable("dbo.EcommerceProductImages");
            this.DropTable("dbo.EcommerceProductGroups");
            this.DropTable("dbo.EcommerceProductDescriptions");
            this.DropTable("dbo.EcommerceProductAttributes");
            this.DropTable("dbo.EcommerceOptionValues");
            this.DropTable("dbo.EcommerceOptionValueDescriptions");
            this.DropTable("dbo.EcommerceOptions");
            this.DropTable("dbo.EcommerceOptionDescriptions");
            this.DropTable("dbo.EcommerceCountryPrices");
            this.DropTable("dbo.EcommerceWeightClasses");
            this.DropTable("dbo.EcommerceManufacturers");
            this.DropTable("dbo.EcommerceLengthClasses");
            this.DropTable("dbo.EcommerceProducts");
            this.DropTable("dbo.EcommerceCategoryDescriptions");
            this.DropTable("dbo.EcommerceCategories");
            this.DropTable("dbo.EcommerceAttributeGroupDescriptions");
            this.DropTable("dbo.EcommerceAttributeGroups");
            this.DropTable("dbo.EcommerceAttributes");
            this.DropTable("dbo.EcommerceAttributeDescriptions");
        }
    }
}
