// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Attribute_AttributeOption_DoubleValue_UrlToken : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Attribute_AttributeOption_DoubleValue_UrlToken));
        
        string IMigrationMetadata.Id
        {
            get { return "201807171648534_Attribute_AttributeOption_DoubleValue_UrlToken"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
