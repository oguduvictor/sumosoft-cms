namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CategoryItemsSortOrders : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategories", "ProductsSortOrder", c => c.String());
            this.AddColumn("dbo.EcommerceCategories", "ProductStockUnitsSortOrder", c => c.String());
            this.AddColumn("dbo.EcommerceCategories", "ProductImageKitsSortOrder", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCategories", "ProductImageKitsSortOrder");
            this.DropColumn("dbo.EcommerceCategories", "ProductStockUnitsSortOrder");
            this.DropColumn("dbo.EcommerceCategories", "ProductsSortOrder");
        }
    }
}
