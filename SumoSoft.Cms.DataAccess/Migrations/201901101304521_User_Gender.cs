namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class User_Gender : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"UPDATE Users  
                SET     Gender =  CASE  
                        WHEN Gender = 'male' THEN '10' 
                        WHEN Gender = 'female' THEN '20' 
                        ELSE '0'
                END;");
            this.AlterColumn("dbo.Users", "Gender", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            this.AlterColumn("dbo.Users", "Gender", c => c.String());
        }
    }
}
