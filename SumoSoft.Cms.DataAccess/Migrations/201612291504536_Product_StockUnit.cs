namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Product_StockUnit : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"ALTER TABLE [dbo].[EcommerceAttributes] Drop Constraint [FK_dbo.EcommerceAttributes_dbo.EcommerceAttributeGroups_AttributeGroup_Id];
                  ALTER TABLE [dbo].[EcommerceProductVariantOptions] Drop Constraint [FK_dbo.EcommerceProductVariantOptions_dbo.EcommerceProductVariants_ProductVariant_Id];
                  ALTER TABLE [dbo].[EcommerceProductVariantOptions] Drop Constraint [FK_dbo.EcommerceProductVariantOptions_dbo.EcommerceVariantOptions_VariantOption_Id];
                  ALTER TABLE [dbo].[EcommerceCartItemProductVariants] Drop Constraint [FK_dbo.EcommerceCartItemProductVariants_dbo.EcommerceProductVariantOptions_SelectValue_Id];
                  ALTER TABLE [dbo].[EcommerceProductVariantOptionLocalizedKits] Drop Constraint [FK_dbo.EcommerceProductVariantOptionLocalizedStocks_dbo.EcommerceProductVariantOptions_ProductVariantOption_Id];");

            this.RenameTable(name: "dbo.EcommerceOrderItemProductVariants", newName: "EcommerceOrderItemVariants");
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "CartItem_Id", "dbo.EcommerceCartItems");
            this.DropForeignKey("dbo.EcommerceAttributes", "Group_Id", "dbo.EcommerceAttributeGroups");
            this.DropForeignKey("dbo.EcommerceProductVariantLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductVariantLocalizedKits", "ProductVariant_Id", "dbo.EcommerceProductVariants");
            this.DropForeignKey("dbo.EcommerceProductVariantOptions", "ProductVariant_Id", "dbo.EcommerceProductVariants");
            this.DropForeignKey("dbo.EcommerceProductVariantOptionLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductVariantOptionLocalizedKits", "ProductVariantOption_Id", "dbo.EcommerceProductVariantOptions");
            this.DropForeignKey("dbo.EcommerceProductVariantOptions", "VariantOption_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "ProductVariant_Id", "dbo.EcommerceProductVariants");
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", "dbo.EcommerceProductVariantOptions");
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "CartItem_Id" });
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "ProductVariant_Id" });
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "SelectValue_Id" });
            this.DropIndex("dbo.EcommerceAttributes", new[] { "Group_Id" });
            this.DropIndex("dbo.EcommerceProductVariantLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProductVariantLocalizedKits", new[] { "ProductVariant_Id" });
            this.DropIndex("dbo.EcommerceProductVariantOptions", new[] { "ProductVariant_Id" });
            this.DropIndex("dbo.EcommerceProductVariantOptions", new[] { "VariantOption_Id" });
            this.DropIndex("dbo.EcommerceProductVariantOptionLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProductVariantOptionLocalizedKits", new[] { "ProductVariantOption_Id" });
            this.CreateTable(
                "dbo.EcommerceCartItemVariants",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    BooleanValue = c.Boolean(nullable: false),
                    DoubleValue = c.Double(nullable: false),
                    IntegerValue = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    CartItem_Id = c.Guid(),
                    Variant_Id = c.Guid(),
                    VariantOptionValue_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceCartItems", t => t.CartItem_Id)
                .ForeignKey("dbo.EcommerceVariants", t => t.Variant_Id)
                .ForeignKey("dbo.EcommerceVariantOptions", t => t.VariantOptionValue_Id)
                .Index(t => t.CartItem_Id)
                .Index(t => t.Variant_Id)
                .Index(t => t.VariantOptionValue_Id);

            this.CreateTable(
                "dbo.EcommerceProductStockUnits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceProductStockUnitEcommerceVariantOptions",
                c => new
                {
                    EcommerceProductStockUnit_Id = c.Guid(nullable: false),
                    EcommerceVariantOption_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceProductStockUnit_Id, t.EcommerceVariantOption_Id })
                .ForeignKey("dbo.EcommerceProductStockUnits", t => t.EcommerceProductStockUnit_Id)
                .ForeignKey("dbo.EcommerceVariantOptions", t => t.EcommerceVariantOption_Id)
                .Index(t => t.EcommerceProductStockUnit_Id)
                .Index(t => t.EcommerceVariantOption_Id);

            this.AddColumn("dbo.EcommerceProductVariants", "DefaultBooleanValue", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultIntegerValue", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultVariantOptionValue_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceVariants", "DefaultBooleanValue", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceVariants", "DefaultDoubleValue", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceVariants", "DefaultIntegerValue", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceVariants", "CreateProductStockUnits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceVariants", "DefaultVariantOptionValue_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceVariantLocalizedKits", "Description", c => c.String());
            //AddColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedText", c => c.String());
            //AddColumn("dbo.EcommerceOrderItemVariants", "BooleanValue", c => c.Boolean(nullable: false));
            //AddColumn("dbo.EcommerceOrderItemVariants", "VariantOptionName", c => c.String());
            //AddColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedPriceModifier", c => c.Double(nullable: false));
            this.CreateIndex("dbo.EcommerceVariants", "DefaultVariantOptionValue_Id");
            this.CreateIndex("dbo.EcommerceProductVariants", "DefaultVariantOptionValue_Id");
            this.AddForeignKey("dbo.EcommerceVariants", "DefaultVariantOptionValue_Id", "dbo.EcommerceVariantOptions", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariants", "DefaultVariantOptionValue_Id", "dbo.EcommerceVariantOptions", "Id");
            this.DropColumn("dbo.EcommerceProductVariants", "GlobalStock");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultCheckboxValue");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultIntValue");
            this.DropColumn("dbo.EcommerceAttributes", "Group_Id");
            this.DropColumn("dbo.EcommerceVariantLocalizedKits", "PriceModifier");
            //DropColumn("dbo.EcommerceOrderItemVariants", "SelectValueVariantOptionText");
            //DropColumn("dbo.EcommerceOrderItemVariants", "CheckboxValue");
            //DropColumn("dbo.EcommerceOrderItemVariants", "SelectValueVatiantOptionName");
            //DropColumn("dbo.EcommerceOrderItemVariants", "SelectValueVatiantOptionPriceModifier");

            // Custom directives -----------------------------------------------------------------------------
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "SelectValueVariantOptionText", "VariantOptionLocalizedText");
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "CheckboxValue", "BooleanValue");
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "SelectValueVatiantOptionName", "VariantOptionName");
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "SelectValueVatiantOptionPriceModifier", "VariantOptionLocalizedPriceModifier");
            // -----------------------------------------------------------------------------------------------

            this.DropTable("dbo.EcommerceCartItemProductVariants");
            this.DropTable("dbo.EcommerceAttributeGroups");
            this.DropTable("dbo.EcommerceProductVariantLocalizedKits");
            this.DropTable("dbo.EcommerceProductVariantOptions");
            this.DropTable("dbo.EcommerceProductVariantOptionLocalizedKits");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceProductVariantOptionLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ProductVariantOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceProductVariantOptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    GlobalStock = c.Int(nullable: false),
                    IsDefault = c.Boolean(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    ProductVariant_Id = c.Guid(),
                    VariantOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceProductVariantLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ProductVariant_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceAttributeGroups",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceCartItemProductVariants",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    CheckboxValue = c.Boolean(nullable: false),
                    DoubleValue = c.Double(nullable: false),
                    IntegerValue = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    CartItem_Id = c.Guid(),
                    ProductVariant_Id = c.Guid(),
                    SelectValue_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            //AddColumn("dbo.EcommerceOrderItemVariants", "SelectValueVatiantOptionPriceModifier", c => c.Double(nullable: false));
            //AddColumn("dbo.EcommerceOrderItemVariants", "SelectValueVatiantOptionName", c => c.String());
            //AddColumn("dbo.EcommerceOrderItemVariants", "CheckboxValue", c => c.Boolean(nullable: false));
            //AddColumn("dbo.EcommerceOrderItemVariants", "SelectValueVariantOptionText", c => c.String());
            this.AddColumn("dbo.EcommerceVariantLocalizedKits", "PriceModifier", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceAttributes", "Group_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultIntValue", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultCheckboxValue", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "GlobalStock", c => c.Int(nullable: false));
            this.DropForeignKey("dbo.EcommerceProductVariants", "DefaultVariantOptionValue_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceProductStockUnitEcommerceVariantOptions", "EcommerceVariantOption_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceProductStockUnitEcommerceVariantOptions", "EcommerceProductStockUnit_Id", "dbo.EcommerceProductStockUnits");
            this.DropForeignKey("dbo.EcommerceProductStockUnits", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceCartItemVariants", "VariantOptionValue_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceCartItemVariants", "Variant_Id", "dbo.EcommerceVariants");
            this.DropForeignKey("dbo.EcommerceVariants", "DefaultVariantOptionValue_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceCartItemVariants", "CartItem_Id", "dbo.EcommerceCartItems");
            this.DropIndex("dbo.EcommerceProductStockUnitEcommerceVariantOptions", new[] { "EcommerceVariantOption_Id" });
            this.DropIndex("dbo.EcommerceProductStockUnitEcommerceVariantOptions", new[] { "EcommerceProductStockUnit_Id" });
            this.DropIndex("dbo.EcommerceProductVariants", new[] { "DefaultVariantOptionValue_Id" });
            this.DropIndex("dbo.EcommerceProductStockUnits", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceVariants", new[] { "DefaultVariantOptionValue_Id" });
            this.DropIndex("dbo.EcommerceCartItemVariants", new[] { "VariantOptionValue_Id" });
            this.DropIndex("dbo.EcommerceCartItemVariants", new[] { "Variant_Id" });
            this.DropIndex("dbo.EcommerceCartItemVariants", new[] { "CartItem_Id" });
            //DropColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedPriceModifier");
            //DropColumn("dbo.EcommerceOrderItemVariants", "VariantOptionName");
            //DropColumn("dbo.EcommerceOrderItemVariants", "BooleanValue");
            //DropColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedText");
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedText", "SelectValueVariantOptionText");
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "BooleanValue", "CheckboxValue");
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "VariantOptionName", "SelectValueVatiantOptionName");
            this.RenameColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedPriceModifier", "SelectValueVatiantOptionPriceModifier");
            this.DropColumn("dbo.EcommerceVariantLocalizedKits", "Description");
            this.DropColumn("dbo.EcommerceVariants", "DefaultVariantOptionValue_Id");
            this.DropColumn("dbo.EcommerceVariants", "CreateProductStockUnits");
            this.DropColumn("dbo.EcommerceVariants", "DefaultIntegerValue");
            this.DropColumn("dbo.EcommerceVariants", "DefaultDoubleValue");
            this.DropColumn("dbo.EcommerceVariants", "DefaultBooleanValue");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultVariantOptionValue_Id");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultIntegerValue");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultBooleanValue");
            this.DropTable("dbo.EcommerceProductStockUnitEcommerceVariantOptions");
            this.DropTable("dbo.EcommerceProductStockUnits");
            this.DropTable("dbo.EcommerceCartItemVariants");
            this.CreateIndex("dbo.EcommerceProductVariantOptionLocalizedKits", "ProductVariantOption_Id");
            this.CreateIndex("dbo.EcommerceProductVariantOptionLocalizedKits", "Country_Id");
            this.CreateIndex("dbo.EcommerceProductVariantOptions", "VariantOption_Id");
            this.CreateIndex("dbo.EcommerceProductVariantOptions", "ProductVariant_Id");
            this.CreateIndex("dbo.EcommerceProductVariantLocalizedKits", "ProductVariant_Id");
            this.CreateIndex("dbo.EcommerceProductVariantLocalizedKits", "Country_Id");
            this.CreateIndex("dbo.EcommerceAttributes", "Group_Id");
            this.CreateIndex("dbo.EcommerceCartItemProductVariants", "SelectValue_Id");
            this.CreateIndex("dbo.EcommerceCartItemProductVariants", "ProductVariant_Id");
            this.CreateIndex("dbo.EcommerceCartItemProductVariants", "CartItem_Id");
            this.AddForeignKey("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", "dbo.EcommerceProductVariantOptions", "Id");
            this.AddForeignKey("dbo.EcommerceCartItemProductVariants", "ProductVariant_Id", "dbo.EcommerceProductVariants", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariantOptions", "VariantOption_Id", "dbo.EcommerceVariantOptions", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariantOptionLocalizedKits", "ProductVariantOption_Id", "dbo.EcommerceProductVariantOptions", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariantOptionLocalizedKits", "Country_Id", "dbo.Countries", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariantOptions", "ProductVariant_Id", "dbo.EcommerceProductVariants", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariantLocalizedKits", "ProductVariant_Id", "dbo.EcommerceProductVariants", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariantLocalizedKits", "Country_Id", "dbo.Countries", "Id");
            this.AddForeignKey("dbo.EcommerceAttributes", "Group_Id", "dbo.EcommerceAttributeGroups", "Id");
            this.AddForeignKey("dbo.EcommerceCartItemProductVariants", "CartItem_Id", "dbo.EcommerceCartItems", "Id");
            this.RenameTable(name: "dbo.EcommerceOrderItemVariants", newName: "EcommerceOrderItemProductVariants");
        }
    }
}
