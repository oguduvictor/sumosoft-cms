namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RemovedVariantSelectorAddedOrderTotalPaid : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrders", "TotalPaid", c => c.Double(nullable: false));
            this.DropColumn("dbo.EcommerceVariants", "Selector");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceVariants", "Selector", c => c.String());
            this.DropColumn("dbo.EcommerceOrders", "TotalPaid");
        }
    }
}
