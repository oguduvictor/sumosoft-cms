namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.Addresses",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Description = c.String(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);

            this.CreateTable(
                "dbo.BlogCategories",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    BlogPost_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BlogPosts", t => t.BlogPost_Id)
                .Index(t => t.BlogPost_Id);

            this.CreateTable(
                "dbo.BlogComments",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Email = c.String(),
                    Url = c.String(),
                    Body = c.String(),
                    Published = c.Boolean(nullable: false),
                    Author_Id = c.Guid(),
                    Post_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Author_Id)
                .ForeignKey("dbo.BlogPosts", t => t.Post_Id)
                .Index(t => t.Author_Id)
                .Index(t => t.Post_Id);

            this.CreateTable(
                "dbo.Users",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    FirstName = c.String(),
                    LastName = c.String(),
                    Email = c.String(),
                    Password = c.String(),
                    RegistrationDate = c.DateTime(nullable: false),
                    LastLogin = c.DateTime(),
                    IsDeleted = c.Boolean(nullable: false),
                    Role_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRoles", t => t.Role_Id)
                .Index(t => t.Role_Id);

            this.CreateTable(
                "dbo.UserRoles",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.UserPermissions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.Int(nullable: false),
                    UserRole_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRoles", t => t.UserRole_Id)
                .Index(t => t.UserRole_Id);

            this.CreateTable(
                "dbo.BlogPosts",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Content = c.String(),
                    IsPublished = c.Boolean(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    Date = c.DateTime(nullable: false),
                    LastChangeDate = c.DateTime(),
                    FeaturedImage = c.String(),
                    Url = c.String(),
                    MetaTitle = c.String(),
                    MetaDescription = c.String(),
                    Excerpt = c.String(),
                    Author_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Author_Id)
                .Index(t => t.Author_Id);

            this.CreateTable(
                "dbo.BlogTags",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Frequency = c.Int(nullable: false),
                    Post_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BlogPosts", t => t.Post_Id)
                .Index(t => t.Post_Id);

            this.CreateTable(
                "dbo.CmsSettings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsSeeded = c.Boolean(nullable: false),
                    IsMultiLanguage = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.Countries",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    FlagImageRootUrl = c.String(),
                    Url = c.String(),
                    Name = c.String(),
                    CultureInfoName = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    IsDefault = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.MenuLinks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Label = c.String(),
                    ActiveLink = c.String(),
                    Position = c.Int(nullable: false),
                    Controller = c.String(),
                    Action = c.String(),
                    Icon = c.String(),
                    Permission = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.BlogTags", "Post_Id", "dbo.BlogPosts");
            this.DropForeignKey("dbo.BlogComments", "Post_Id", "dbo.BlogPosts");
            this.DropForeignKey("dbo.BlogCategories", "BlogPost_Id", "dbo.BlogPosts");
            this.DropForeignKey("dbo.BlogPosts", "Author_Id", "dbo.Users");
            this.DropForeignKey("dbo.BlogComments", "Author_Id", "dbo.Users");
            this.DropForeignKey("dbo.Users", "Role_Id", "dbo.UserRoles");
            this.DropForeignKey("dbo.UserPermissions", "UserRole_Id", "dbo.UserRoles");
            this.DropForeignKey("dbo.Addresses", "User_Id", "dbo.Users");
            this.DropIndex("dbo.BlogTags", new[] { "Post_Id" });
            this.DropIndex("dbo.BlogPosts", new[] { "Author_Id" });
            this.DropIndex("dbo.UserPermissions", new[] { "UserRole_Id" });
            this.DropIndex("dbo.Users", new[] { "Role_Id" });
            this.DropIndex("dbo.BlogComments", new[] { "Post_Id" });
            this.DropIndex("dbo.BlogComments", new[] { "Author_Id" });
            this.DropIndex("dbo.BlogCategories", new[] { "BlogPost_Id" });
            this.DropIndex("dbo.Addresses", new[] { "User_Id" });
            this.DropTable("dbo.MenuLinks");
            this.DropTable("dbo.Countries");
            this.DropTable("dbo.CmsSettings");
            this.DropTable("dbo.BlogTags");
            this.DropTable("dbo.BlogPosts");
            this.DropTable("dbo.UserPermissions");
            this.DropTable("dbo.UserRoles");
            this.DropTable("dbo.Users");
            this.DropTable("dbo.BlogComments");
            this.DropTable("dbo.BlogCategories");
            this.DropTable("dbo.Addresses");
        }
    }
}
