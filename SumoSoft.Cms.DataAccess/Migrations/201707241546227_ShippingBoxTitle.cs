namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ShippingBoxTitle : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceShippingBoxLocalizedKits", "Title", c => c.String());
            this.AddColumn("dbo.EcommerceOrderShippingBoxes", "Title", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderShippingBoxes", "Title");
            this.DropColumn("dbo.EcommerceShippingBoxLocalizedKits", "Title");
        }
    }
}
