namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_181 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.CmsSettings", "SmtpDisplayName", c => c.String());
            this.DropColumn("dbo.CmsSettings", "IsMultiLanguage");
        }

        public override void Down()
        {
            this.AddColumn("dbo.CmsSettings", "IsMultiLanguage", c => c.Boolean(nullable: false));
            this.DropColumn("dbo.CmsSettings", "SmtpDisplayName");
        }
    }
}
