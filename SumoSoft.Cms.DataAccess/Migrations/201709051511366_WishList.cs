namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class WishList : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceWishLists",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);

            this.AddColumn("dbo.EcommerceCartItems", "WishList_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCartItems", "WishList_Id");
            this.AddForeignKey("dbo.EcommerceCartItems", "WishList_Id", "dbo.EcommerceWishLists", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceWishLists", "User_Id", "dbo.Users");
            this.DropForeignKey("dbo.EcommerceCartItems", "WishList_Id", "dbo.EcommerceWishLists");
            this.DropIndex("dbo.EcommerceWishLists", new[] { "User_Id" });
            this.DropIndex("dbo.EcommerceCartItems", new[] { "WishList_Id" });
            this.DropColumn("dbo.EcommerceCartItems", "WishList_Id");
            this.DropTable("dbo.EcommerceWishLists");
        }
    }
}
