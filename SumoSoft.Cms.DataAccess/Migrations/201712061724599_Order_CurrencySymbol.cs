namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Order_CurrencySymbol : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrders", "CurrencySymbol", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "ISO4217CurrencySymbol", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrders", "ISO4217CurrencySymbol");
            this.DropColumn("dbo.EcommerceOrders", "CurrencySymbol");
        }
    }
}
