// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class EcommerceModifications_190 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EcommerceModifications_190));
        
        string IMigrationMetadata.Id
        {
            get { return "201603011542346_EcommerceModifications_1.9.0"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
