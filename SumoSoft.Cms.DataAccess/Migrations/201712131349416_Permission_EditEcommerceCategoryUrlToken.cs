namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Permission_EditEcommerceCategoryUrlToken : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.UserRoles", "EditEcommerceCategoryUrlToken", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.UserRoles", "EditEcommerceCategoryUrlToken");
        }
    }
}
