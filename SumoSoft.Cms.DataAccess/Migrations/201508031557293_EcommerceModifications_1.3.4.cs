namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_134 : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceProductRelateds", newName: "EcommerceProductRelatedProducts");
            this.DropForeignKey("dbo.EcommerceAttributeGroupLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceAttributeGroupLocalizedStrings", "Group_Id", "dbo.EcommerceAttributeGroups");
            this.DropForeignKey("dbo.EcommerceProductImages", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProducts", "Manufacturer_Id", "dbo.EcommerceManufacturers");
            this.DropIndex("dbo.EcommerceProducts", new[] { "Manufacturer_Id" });
            this.DropIndex("dbo.EcommerceAttributeGroupLocalizedStrings", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceAttributeGroupLocalizedStrings", new[] { "Group_Id" });
            this.DropIndex("dbo.EcommerceProductImages", new[] { "Product_Id" });
            this.CreateTable(
                "dbo.EcommerceAttributeTypeOptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Attribute_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceAttributes", t => t.Attribute_Id)
                .Index(t => t.Attribute_Id);

            this.CreateTable(
                "dbo.EcommerceAttributeTypeOptionLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Text = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    EcommerceAttributeTypeOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceAttributeTypeOptions", t => t.EcommerceAttributeTypeOption_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.EcommerceAttributeTypeOption_Id);

            this.CreateTable(
                "dbo.RelatedProducts",
                c => new
                {
                    ProductID = c.Guid(nullable: false),
                    RelatedProductID = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.ProductID, t.RelatedProductID })
                .ForeignKey("dbo.EcommerceProducts", t => t.ProductID)
                .ForeignKey("dbo.EcommerceProducts", t => t.RelatedProductID)
                .Index(t => t.ProductID)
                .Index(t => t.RelatedProductID);

            this.AddColumn("dbo.EcommerceProductAttributes", "TextValue", c => c.String());
            this.AddColumn("dbo.EcommerceProductAttributes", "TextAreaValue", c => c.String());
            this.AddColumn("dbo.EcommerceProductAttributes", "CheckboxValue", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductAttributes", "DateTimeValue", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductAttributes", "ImageValue", c => c.String());
            this.AddColumn("dbo.EcommerceProductAttributes", "SelectValue_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceAttributes", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceAttributes", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceAttributes", "Type", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceAttributeGroups", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceAttributeLocalizedStrings", "Label", c => c.String());
            this.AddColumn("dbo.CmsSettings", "UseAzureStorage", c => c.Boolean(nullable: false));
            this.CreateIndex("dbo.EcommerceProductAttributes", "SelectValue_Id");
            this.AddForeignKey("dbo.EcommerceProductAttributes", "SelectValue_Id", "dbo.EcommerceAttributeTypeOptions", "Id");
            this.DropColumn("dbo.EcommerceProducts", "Manufacturer_Id");
            this.DropColumn("dbo.EcommerceProductAttributes", "Value");
            this.DropColumn("dbo.EcommerceAttributeLocalizedStrings", "Name");
            this.DropTable("dbo.EcommerceAttributeGroupLocalizedStrings");
            this.DropTable("dbo.EcommerceProductImages");
            this.DropTable("dbo.EcommerceManufacturers");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceManufacturers",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Image = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceProductImages",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Url = c.String(),
                    Tag = c.Int(nullable: false),
                    SortOrder = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceAttributeGroupLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Group_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceAttributeLocalizedStrings", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceProductAttributes", "Value", c => c.String());
            this.AddColumn("dbo.EcommerceProducts", "Manufacturer_Id", c => c.Guid());
            this.DropForeignKey("dbo.RelatedProducts", "RelatedProductID", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.RelatedProducts", "ProductID", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductAttributes", "SelectValue_Id", "dbo.EcommerceAttributeTypeOptions");
            this.DropForeignKey("dbo.EcommerceAttributeTypeOptionLocalizedStrings", "EcommerceAttributeTypeOption_Id", "dbo.EcommerceAttributeTypeOptions");
            this.DropForeignKey("dbo.EcommerceAttributeTypeOptionLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceAttributeTypeOptions", "Attribute_Id", "dbo.EcommerceAttributes");
            this.DropIndex("dbo.RelatedProducts", new[] { "RelatedProductID" });
            this.DropIndex("dbo.RelatedProducts", new[] { "ProductID" });
            this.DropIndex("dbo.EcommerceAttributeTypeOptionLocalizedStrings", new[] { "EcommerceAttributeTypeOption_Id" });
            this.DropIndex("dbo.EcommerceAttributeTypeOptionLocalizedStrings", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceAttributeTypeOptions", new[] { "Attribute_Id" });
            this.DropIndex("dbo.EcommerceProductAttributes", new[] { "SelectValue_Id" });
            this.DropColumn("dbo.CmsSettings", "UseAzureStorage");
            this.DropColumn("dbo.EcommerceAttributeLocalizedStrings", "Label");
            this.DropColumn("dbo.EcommerceAttributeGroups", "Name");
            this.DropColumn("dbo.EcommerceAttributes", "Type");
            this.DropColumn("dbo.EcommerceAttributes", "SortOrder");
            this.DropColumn("dbo.EcommerceAttributes", "Name");
            this.DropColumn("dbo.EcommerceProductAttributes", "SelectValue_Id");
            this.DropColumn("dbo.EcommerceProductAttributes", "ImageValue");
            this.DropColumn("dbo.EcommerceProductAttributes", "DateTimeValue");
            this.DropColumn("dbo.EcommerceProductAttributes", "CheckboxValue");
            this.DropColumn("dbo.EcommerceProductAttributes", "TextAreaValue");
            this.DropColumn("dbo.EcommerceProductAttributes", "TextValue");
            this.DropTable("dbo.RelatedProducts");
            this.DropTable("dbo.EcommerceAttributeTypeOptionLocalizedStrings");
            this.DropTable("dbo.EcommerceAttributeTypeOptions");
            this.CreateIndex("dbo.EcommerceProductImages", "Product_Id");
            this.CreateIndex("dbo.EcommerceAttributeGroupLocalizedStrings", "Group_Id");
            this.CreateIndex("dbo.EcommerceAttributeGroupLocalizedStrings", "Country_Id");
            this.CreateIndex("dbo.EcommerceProducts", "Manufacturer_Id");
            this.AddForeignKey("dbo.EcommerceProducts", "Manufacturer_Id", "dbo.EcommerceManufacturers", "Id");
            this.AddForeignKey("dbo.EcommerceProductImages", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceAttributeGroupLocalizedStrings", "Group_Id", "dbo.EcommerceAttributeGroups", "Id");
            this.AddForeignKey("dbo.EcommerceAttributeGroupLocalizedStrings", "Country_Id", "dbo.Countries", "Id");
            this.RenameTable(name: "dbo.EcommerceProductRelatedProducts", newName: "EcommerceProductRelateds");
        }
    }
}
