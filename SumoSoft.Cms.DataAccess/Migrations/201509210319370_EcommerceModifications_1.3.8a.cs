namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_138a : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceProductOptions", newName: "EcommerceProductVariants");
            this.RenameTable(name: "dbo.EcommerceOptions", newName: "EcommerceVariants");
            this.RenameTable(name: "dbo.EcommerceOptionGroups", newName: "EcommerceVariantGroups");
            this.RenameTable(name: "dbo.EcommerceOptionGroupLocalizedStrings", newName: "EcommerceVariantGroupLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceOptionPriceModifiers", newName: "EcommerceVariantPriceModifiers");
            this.RenameTable(name: "dbo.EcommerceOptionLocalizedStrings", newName: "EcommerceVariantLocalizedStrings");
            this.RenameColumn(table: "dbo.EcommerceVariantLocalizedStrings", name: "Option_Id", newName: "Variant_Id");
            this.RenameColumn(table: "dbo.EcommerceVariantPriceModifiers", name: "EcommerceOption_Id", newName: "EcommerceVariant_Id");
            this.RenameColumn(table: "dbo.EcommerceProductVariants", name: "Option_Id", newName: "Variant_Id");
            this.RenameColumn(table: "dbo.EcommerceProducts", name: "OptionThatOverridesTheStock_Id", newName: "VariantThatOverridesTheStock_Id");
            this.RenameIndex(table: "dbo.EcommerceProducts", name: "IX_OptionThatOverridesTheStock_Id", newName: "IX_VariantThatOverridesTheStock_Id");
            this.RenameIndex(table: "dbo.EcommerceProductVariants", name: "IX_Option_Id", newName: "IX_Variant_Id");
            this.RenameIndex(table: "dbo.EcommerceVariantLocalizedStrings", name: "IX_Option_Id", newName: "IX_Variant_Id");
            this.RenameIndex(table: "dbo.EcommerceVariantPriceModifiers", name: "IX_EcommerceOption_Id", newName: "IX_EcommerceVariant_Id");
            this.DropPrimaryKey("dbo.EcommerceVariantPriceModifiers");
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalVariantStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseLocalVariantStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalVariantStockIfLocalVariantStockIsEmpty", c => c.Boolean(nullable: false));
            this.AlterColumn("dbo.EcommerceVariantPriceModifiers", "Id", c => c.Guid(nullable: false, identity: true));
            this.AddPrimaryKey("dbo.EcommerceVariantPriceModifiers", "Id");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalOptionStock");
            this.DropColumn("dbo.EcommerceProducts", "UseLocalOptionStock");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalOptionStockIfLocalOptionStockIsEmpty");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalOptionStockIfLocalOptionStockIsEmpty", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseLocalOptionStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalOptionStock", c => c.Boolean(nullable: false));
            this.DropPrimaryKey("dbo.EcommerceVariantPriceModifiers");
            this.AlterColumn("dbo.EcommerceVariantPriceModifiers", "Id", c => c.Guid(nullable: false));
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalVariantStockIfLocalVariantStockIsEmpty");
            this.DropColumn("dbo.EcommerceProducts", "UseLocalVariantStock");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalVariantStock");
            this.AddPrimaryKey("dbo.EcommerceVariantPriceModifiers", "Id");
            this.RenameIndex(table: "dbo.EcommerceVariantPriceModifiers", name: "IX_EcommerceVariant_Id", newName: "IX_EcommerceOption_Id");
            this.RenameIndex(table: "dbo.EcommerceVariantLocalizedStrings", name: "IX_Variant_Id", newName: "IX_Option_Id");
            this.RenameIndex(table: "dbo.EcommerceProductVariants", name: "IX_Variant_Id", newName: "IX_Option_Id");
            this.RenameIndex(table: "dbo.EcommerceProducts", name: "IX_VariantThatOverridesTheStock_Id", newName: "IX_OptionThatOverridesTheStock_Id");
            this.RenameColumn(table: "dbo.EcommerceProducts", name: "VariantThatOverridesTheStock_Id", newName: "OptionThatOverridesTheStock_Id");
            this.RenameColumn(table: "dbo.EcommerceProductVariants", name: "Variant_Id", newName: "Option_Id");
            this.RenameColumn(table: "dbo.EcommerceVariantPriceModifiers", name: "EcommerceVariant_Id", newName: "EcommerceOption_Id");
            this.RenameColumn(table: "dbo.EcommerceVariantLocalizedStrings", name: "Variant_Id", newName: "Option_Id");
            this.RenameTable(name: "dbo.EcommerceVariantLocalizedStrings", newName: "EcommerceOptionLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceVariantPriceModifiers", newName: "EcommerceOptionPriceModifiers");
            this.RenameTable(name: "dbo.EcommerceVariantGroupLocalizedStrings", newName: "EcommerceOptionGroupLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceVariantGroups", newName: "EcommerceOptionGroups");
            this.RenameTable(name: "dbo.EcommerceVariants", newName: "EcommerceOptions");
            this.RenameTable(name: "dbo.EcommerceProductVariants", newName: "EcommerceProductOptions");
        }
    }
}
