namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_171a : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceOrderItemProductVariants", "Order_Id", "dbo.EcommerceOrders");
            this.DropIndex("dbo.EcommerceOrderItemProductVariants", new[] { "Order_Id" });
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "EcommerceOrderItem_Id", newName: "OrderItem_Id");
            this.RenameIndex(table: "dbo.EcommerceOrderItemProductVariants", name: "IX_EcommerceOrderItem_Id", newName: "IX_OrderItem_Id");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "Order_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "Order_Id", c => c.Guid());
            this.RenameIndex(table: "dbo.EcommerceOrderItemProductVariants", name: "IX_OrderItem_Id", newName: "IX_EcommerceOrderItem_Id");
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "OrderItem_Id", newName: "EcommerceOrderItem_Id");
            this.CreateIndex("dbo.EcommerceOrderItemProductVariants", "Order_Id");
            this.AddForeignKey("dbo.EcommerceOrderItemProductVariants", "Order_Id", "dbo.EcommerceOrders", "Id");
        }
    }
}
