namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Email_ContentSection : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.Emails", "ContentSection_Id", c => c.Guid());
            this.CreateIndex("dbo.Emails", "ContentSection_Id");
            this.AddForeignKey("dbo.Emails", "ContentSection_Id", "dbo.ContentSections", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.Emails", "ContentSection_Id", "dbo.ContentSections");
            this.DropIndex("dbo.Emails", new[] { "ContentSection_Id" });
            this.DropColumn("dbo.Emails", "ContentSection_Id");
        }
    }
}
