namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RemoveNewsletter : DbMigration
    {
        public override void Up()
        {
            this.DropColumn("dbo.Users", "Newsletter");
        }

        public override void Down()
        {
            this.AddColumn("dbo.Users", "Newsletter", c => c.Boolean(nullable: false));
        }
    }
}
