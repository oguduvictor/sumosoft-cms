namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class BaseEntity_ModifiedDate_NotNull : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"  DECLARE @sql VARCHAR(MAX)
                    DECLARE sqlQueryCursor CURSOR LOCAL FAST_FORWARD For 
                        select 'update ' + c.table_name + ' set ModifiedDate = GetDate() where ModifiedDate IS  NULL' as my_update_query 
                            from (select table_name, COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'ModifiedDate') c;
                
                    OPEN sqlQueryCursor;
                    FETCH Next From sqlQueryCursor into @sql;

                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            EXEC(@sql);
                            FETCH Next From sqlQueryCursor into @sql;
                        END
                    CLOSE sqlQueryCursor ");

            this.AlterColumn("dbo.Addresses", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.Countries", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceShippingBoxes", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProducts", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceCategories", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceCategoryLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductImageKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductImages", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceVariantOptions", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceVariants", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceVariantLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceVariantOptionLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductStockUnits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductStockUnitLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductAttributes", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceAttributes", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceAttributeLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceAttributeOptions", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceAttributeOptionLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.ContentSections", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.ContentSectionLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.ContentVersions", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductVariants", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceShippingBoxLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceTaxes", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceCartItems", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceCartItemVariants", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceCartShippingBoxes", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceCarts", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceCoupons", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.Users", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.UserRoles", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.UserLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceUserCredits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceWishLists", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.BlogCategories", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.BlogCategoryLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.BlogPosts", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.BlogComments", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.CmsSettings", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrderAddresses", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrderCoupons", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrderCredits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrderItemVariants", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrderItems", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrderShippingBoxes", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrders", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.SentEmails", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.Emails", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EmailLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceOrderItemTaxes", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.EcommerceProductRelatedProducts", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.Logs", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.ScheduledTasks", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.SeoSectionLocalizedKits", "ModifiedDate", c => c.DateTime(nullable: false));
            this.AlterColumn("dbo.SeoSections", "ModifiedDate", c => c.DateTime(nullable: false));
        }

        public override void Down()
        {
            this.AlterColumn("dbo.SeoSections", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.SeoSectionLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.ScheduledTasks", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.Logs", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductRelatedProducts", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrderItemTaxes", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EmailLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.Emails", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.SentEmails", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrders", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrderShippingBoxes", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrderItems", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrderItemVariants", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrderCredits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrderCoupons", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceOrderAddresses", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.CmsSettings", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.BlogComments", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.BlogPosts", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.BlogCategoryLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.BlogCategories", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceWishLists", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceUserCredits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.UserLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.UserRoles", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.Users", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceCoupons", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceCarts", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceCartShippingBoxes", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceCartItemVariants", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceCartItems", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceTaxes", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceShippingBoxLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductVariants", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.ContentVersions", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.ContentSectionLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.ContentSections", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceAttributeOptionLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceAttributeOptions", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceAttributeLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceAttributes", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductAttributes", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductStockUnitLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductStockUnits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceVariantOptionLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceVariantLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceVariants", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceVariantOptions", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductImages", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProductImageKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceCategoryLocalizedKits", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceCategories", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceProducts", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.EcommerceShippingBoxes", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.Countries", "ModifiedDate", c => c.DateTime());
            this.AlterColumn("dbo.Addresses", "ModifiedDate", c => c.DateTime());
        }
    }
}
