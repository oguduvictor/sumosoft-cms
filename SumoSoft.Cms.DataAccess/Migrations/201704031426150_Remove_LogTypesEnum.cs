namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Remove_LogTypesEnum : DbMigration
    {
        public override void Up()
        {
            this.DropColumn("dbo.Logs", "Type");
        }

        public override void Down()
        {
            this.AddColumn("dbo.Logs", "Type", c => c.Int(nullable: false));
        }
    }
}
