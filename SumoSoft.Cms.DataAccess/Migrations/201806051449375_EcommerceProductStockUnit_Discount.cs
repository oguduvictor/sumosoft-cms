namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceProductStockUnit_Discount : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceProductStockUnitLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    BasePrice = c.Double(nullable: false),
                    SaleAtPrice = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ProductStockUnit_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceProductStockUnits", t => t.ProductStockUnit_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ProductStockUnit_Id);

            this.Sql(@"Insert into EcommerceProductStockUnitLocalizedKits(Id, BasePrice, SaleAtPrice, Country_Id, ProductStockUnit_Id, CreatedDate, IsDisabled)
                Select NEWID(), productsLocalizedKits.BasePrice, productsLocalizedKits.SaleAtPrice, productsLocalizedKits.Country_Id, productStockUnits.Id, GETDATE(), 0
                From EcommerceProductLocalizedKits as productsLocalizedKits inner join EcommerceProductStockUnits as productStockUnits on 
                productsLocalizedKits.Product_Id = productStockUnits.Product_Id");

            this.DropColumn("dbo.EcommerceProductLocalizedKits", "BasePrice");
            this.DropColumn("dbo.EcommerceProductLocalizedKits", "SaleAtPrice");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProductLocalizedKits", "SaleAtPrice", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProductLocalizedKits", "BasePrice", c => c.Double(nullable: false));
            this.DropForeignKey("dbo.EcommerceProductStockUnitLocalizedKits", "ProductStockUnit_Id", "dbo.EcommerceProductStockUnits");
            this.DropForeignKey("dbo.EcommerceProductStockUnitLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceProductStockUnitLocalizedKits", new[] { "ProductStockUnit_Id" });
            this.DropIndex("dbo.EcommerceProductStockUnitLocalizedKits", new[] { "Country_Id" });
            this.DropTable("dbo.EcommerceProductStockUnitLocalizedKits");
        }
    }
}
