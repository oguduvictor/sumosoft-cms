namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class StatusEnums : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceShippingBoxStatus", "ShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropForeignKey("dbo.EcommerceShippingBoxStatusLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceShippingBoxStatusLocalizedKits", "ShippingBoxStatus_Id", "dbo.EcommerceShippingBoxStatus");
            this.DropIndex("dbo.EcommerceShippingBoxStatus", new[] { "ShippingBox_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxStatusLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxStatusLocalizedKits", new[] { "ShippingBoxStatus_Id" });
            this.AddColumn("dbo.EcommerceOrderItems", "Status", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderShippingBoxes", "Status", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrders", "Status", c => c.Int(nullable: false));
            this.DropTable("dbo.EcommerceShippingBoxStatus");
            this.DropTable("dbo.EcommerceShippingBoxStatusLocalizedKits");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceShippingBoxStatusLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ShippingBoxStatus_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceShippingBoxStatus",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    SortOrder = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    ShippingBox_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.DropColumn("dbo.EcommerceOrders", "Status");
            this.DropColumn("dbo.EcommerceOrderShippingBoxes", "Status");
            this.DropColumn("dbo.EcommerceOrderItems", "Status");
            this.CreateIndex("dbo.EcommerceShippingBoxStatusLocalizedKits", "ShippingBoxStatus_Id");
            this.CreateIndex("dbo.EcommerceShippingBoxStatusLocalizedKits", "Country_Id");
            this.CreateIndex("dbo.EcommerceShippingBoxStatus", "ShippingBox_Id");
            this.AddForeignKey("dbo.EcommerceShippingBoxStatusLocalizedKits", "ShippingBoxStatus_Id", "dbo.EcommerceShippingBoxStatus", "Id");
            this.AddForeignKey("dbo.EcommerceShippingBoxStatusLocalizedKits", "Country_Id", "dbo.Countries", "Id");
            this.AddForeignKey("dbo.EcommerceShippingBoxStatus", "ShippingBox_Id", "dbo.EcommerceShippingBoxes", "Id");
        }
    }
}
