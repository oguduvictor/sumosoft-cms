namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class BillingAddress : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceOrderShippingAddresses", newName: "EcommerceOrderAddresses");
            this.AddColumn("dbo.EcommerceCarts", "BillingAddress_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCategories", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductStockUnits", "Code", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitCode", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "BillingAddress_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCarts", "BillingAddress_Id");
            this.CreateIndex("dbo.EcommerceOrders", "BillingAddress_Id");
            this.AddForeignKey("dbo.EcommerceCarts", "BillingAddress_Id", "dbo.Addresses", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "BillingAddress_Id", "dbo.EcommerceOrderAddresses", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceOrders", "BillingAddress_Id", "dbo.EcommerceOrderAddresses");
            this.DropForeignKey("dbo.EcommerceCarts", "BillingAddress_Id", "dbo.Addresses");
            this.DropIndex("dbo.EcommerceOrders", new[] { "BillingAddress_Id" });
            this.DropIndex("dbo.EcommerceCarts", new[] { "BillingAddress_Id" });
            this.DropColumn("dbo.EcommerceOrders", "BillingAddress_Id");
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitCode");
            this.DropColumn("dbo.EcommerceProductStockUnits", "Code");
            this.DropColumn("dbo.EcommerceCategories", "SortOrder");
            this.DropColumn("dbo.EcommerceCarts", "BillingAddress_Id");
            this.RenameTable(name: "dbo.EcommerceOrderAddresses", newName: "EcommerceOrderShippingAddresses");
        }
    }
}
