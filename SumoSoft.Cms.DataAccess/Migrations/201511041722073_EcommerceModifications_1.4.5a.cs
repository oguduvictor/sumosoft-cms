namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_145a : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceProductPrices", newName: "EcommerceProductLocalizedPrices");
            this.RenameTable(name: "dbo.EcommerceProductStocks", newName: "EcommerceProductLocalizedStocks");
            this.AddColumn("dbo.EcommerceCartItemProductVariants", "DoubleValue", c => c.Double(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCartItemProductVariants", "DoubleValue");
            this.RenameTable(name: "dbo.EcommerceProductLocalizedStocks", newName: "EcommerceProductStocks");
            this.RenameTable(name: "dbo.EcommerceProductLocalizedPrices", newName: "EcommerceProductPrices");
        }
    }
}
