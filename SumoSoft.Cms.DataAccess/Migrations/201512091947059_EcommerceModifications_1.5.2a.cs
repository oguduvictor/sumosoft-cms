namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_152a : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.Logs",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Message = c.String(),
                    Exception = c.String(),
                    Type = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);

            this.AddColumn("dbo.Addresses", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.Countries", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.Users", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCarts", "IsTrial", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceCarts", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCartItems", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProducts", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductAttributes", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributes", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeGroups", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeOptions", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeOptionLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategories", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategoryLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductLocalizedPrices", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductLocalizedStocks", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductTags", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariants", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantOptions", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantOptionLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantOptionPriceModifiers", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariants", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantPriceModifiers", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariantLocalizedStocks", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariantOptions", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariantOptionLocalizedStocks", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCartItemProductVariants", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrders", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrderItems", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrderShippingAddresses", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.UserRoles", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.UserPermissions", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogCategories", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogPosts", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogComments", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.BlogTags", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.CmsSettings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.ContentSectionLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.ContentSections", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductRelatedProducts", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.SeoSectionLocalizedStrings", "ModifiedDate", c => c.DateTime());
            this.AddColumn("dbo.SeoSections", "ModifiedDate", c => c.DateTime());
            this.DropColumn("dbo.Addresses", "ModificationDate");
            this.DropColumn("dbo.Countries", "ModificationDate");
            this.DropColumn("dbo.Users", "ModificationDate");
            this.DropColumn("dbo.EcommerceCarts", "ModificationDate");
            this.DropColumn("dbo.EcommerceCartItems", "ModificationDate");
            this.DropColumn("dbo.EcommerceProducts", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductAttributes", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributes", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributeGroups", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributeLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributeOptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceAttributeOptionLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.EcommerceCategories", "ModificationDate");
            this.DropColumn("dbo.EcommerceCategoryLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductLocalizedPrices", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductLocalizedStocks", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductTags", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductVariants", "ModificationDate");
            this.DropColumn("dbo.EcommerceVariantOptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceVariantOptionLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.EcommerceVariantOptionPriceModifiers", "ModificationDate");
            this.DropColumn("dbo.EcommerceVariants", "ModificationDate");
            this.DropColumn("dbo.EcommerceVariantLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.EcommerceVariantPriceModifiers", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductVariantLocalizedStocks", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductVariantOptions", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductVariantOptionLocalizedStocks", "ModificationDate");
            this.DropColumn("dbo.EcommerceCartItemProductVariants", "ModificationDate");
            this.DropColumn("dbo.EcommerceOrders", "ModificationDate");
            this.DropColumn("dbo.EcommerceOrderItems", "ModificationDate");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "ModificationDate");
            this.DropColumn("dbo.EcommerceOrderShippingAddresses", "ModificationDate");
            this.DropColumn("dbo.UserRoles", "ModificationDate");
            this.DropColumn("dbo.UserPermissions", "ModificationDate");
            this.DropColumn("dbo.BlogCategories", "ModificationDate");
            this.DropColumn("dbo.BlogPosts", "ModificationDate");
            this.DropColumn("dbo.BlogComments", "ModificationDate");
            this.DropColumn("dbo.BlogTags", "ModificationDate");
            this.DropColumn("dbo.CmsSettings", "ModificationDate");
            this.DropColumn("dbo.ContentSectionLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.ContentSections", "ModificationDate");
            this.DropColumn("dbo.EcommerceProductRelatedProducts", "ModificationDate");
            this.DropColumn("dbo.SeoSectionLocalizedStrings", "ModificationDate");
            this.DropColumn("dbo.SeoSections", "ModificationDate");
        }

        public override void Down()
        {
            this.AddColumn("dbo.SeoSections", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.SeoSectionLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductRelatedProducts", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.ContentSections", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.ContentSectionLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.CmsSettings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogTags", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogComments", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogPosts", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.BlogCategories", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.UserPermissions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.UserRoles", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrderShippingAddresses", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrderItemProductVariants", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrderItems", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceOrders", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCartItemProductVariants", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariantOptionLocalizedStocks", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariantOptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariantLocalizedStocks", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantPriceModifiers", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariants", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantOptionPriceModifiers", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantOptionLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceVariantOptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductVariants", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductTags", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductLocalizedStocks", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductLocalizedPrices", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategoryLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCategories", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeOptionLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeOptions", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeLocalizedStrings", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributeGroups", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceAttributes", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProductAttributes", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProducts", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCartItems", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.EcommerceCarts", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.Users", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.Countries", "ModificationDate", c => c.DateTime());
            this.AddColumn("dbo.Addresses", "ModificationDate", c => c.DateTime());
            this.DropForeignKey("dbo.Logs", "User_Id", "dbo.Users");
            this.DropIndex("dbo.Logs", new[] { "User_Id" });
            this.DropColumn("dbo.SeoSections", "ModifiedDate");
            this.DropColumn("dbo.SeoSectionLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductRelatedProducts", "ModifiedDate");
            this.DropColumn("dbo.ContentSections", "ModifiedDate");
            this.DropColumn("dbo.ContentSectionLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.CmsSettings", "ModifiedDate");
            this.DropColumn("dbo.BlogTags", "ModifiedDate");
            this.DropColumn("dbo.BlogComments", "ModifiedDate");
            this.DropColumn("dbo.BlogPosts", "ModifiedDate");
            this.DropColumn("dbo.BlogCategories", "ModifiedDate");
            this.DropColumn("dbo.UserPermissions", "ModifiedDate");
            this.DropColumn("dbo.UserRoles", "ModifiedDate");
            this.DropColumn("dbo.EcommerceOrderShippingAddresses", "ModifiedDate");
            this.DropColumn("dbo.EcommerceOrderItemProductVariants", "ModifiedDate");
            this.DropColumn("dbo.EcommerceOrderItems", "ModifiedDate");
            this.DropColumn("dbo.EcommerceOrders", "ModifiedDate");
            this.DropColumn("dbo.EcommerceCartItemProductVariants", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductVariantOptionLocalizedStocks", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductVariantOptions", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductVariantLocalizedStocks", "ModifiedDate");
            this.DropColumn("dbo.EcommerceVariantPriceModifiers", "ModifiedDate");
            this.DropColumn("dbo.EcommerceVariantLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.EcommerceVariants", "ModifiedDate");
            this.DropColumn("dbo.EcommerceVariantOptionPriceModifiers", "ModifiedDate");
            this.DropColumn("dbo.EcommerceVariantOptionLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.EcommerceVariantOptions", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductVariants", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductTags", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductLocalizedStocks", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductLocalizedPrices", "ModifiedDate");
            this.DropColumn("dbo.EcommerceCategoryLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.EcommerceCategories", "ModifiedDate");
            this.DropColumn("dbo.EcommerceAttributeOptionLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.EcommerceAttributeOptions", "ModifiedDate");
            this.DropColumn("dbo.EcommerceAttributeLocalizedStrings", "ModifiedDate");
            this.DropColumn("dbo.EcommerceAttributeGroups", "ModifiedDate");
            this.DropColumn("dbo.EcommerceAttributes", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProductAttributes", "ModifiedDate");
            this.DropColumn("dbo.EcommerceProducts", "ModifiedDate");
            this.DropColumn("dbo.EcommerceCartItems", "ModifiedDate");
            this.DropColumn("dbo.EcommerceCarts", "ModifiedDate");
            this.DropColumn("dbo.EcommerceCarts", "IsTrial");
            this.DropColumn("dbo.Users", "ModifiedDate");
            this.DropColumn("dbo.Countries", "ModifiedDate");
            this.DropColumn("dbo.Addresses", "ModifiedDate");
            this.DropTable("dbo.Logs");
        }
    }
}
