namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_142c : DbMigration
    {
        public override void Up()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceOptions_dbo.EcommerceShoppingCartLines_EcommerceShoppingCartLine_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceVariants] DROP [FK_dbo.EcommerceOptions_dbo.EcommerceShoppingCartLines_EcommerceShoppingCartLine_Id]");

            this.DropForeignKey("dbo.EcommerceShoppingCarts", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceShoppingCarts", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropForeignKey("dbo.EcommerceShoppingCarts", "ShippingAddress_Id", "dbo.Addresses");
            this.DropForeignKey("dbo.EcommerceShoppingCartLines", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceVariants", "EcommerceShoppingCartLine_Id", "dbo.EcommerceShoppingCartLines");
            this.DropForeignKey("dbo.EcommerceShoppingCartLines", "ShoppingCart_Id", "dbo.EcommerceShoppingCarts");
            this.DropForeignKey("dbo.Users", "Cart_Id", "dbo.EcommerceShoppingCarts");
            this.DropForeignKey("dbo.EcommerceOrders", "ShoppingCartReference_Id", "dbo.EcommerceShoppingCarts");
            this.DropIndex("dbo.Users", new[] { "Cart_Id" });
            this.DropIndex("dbo.EcommerceShoppingCarts", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceShoppingCarts", new[] { "Coupon_Id" });
            this.DropIndex("dbo.EcommerceShoppingCarts", new[] { "ShippingAddress_Id" });
            this.DropIndex("dbo.EcommerceShoppingCartLines", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceShoppingCartLines", new[] { "ShoppingCart_Id" });
            this.DropIndex("dbo.EcommerceVariants", new[] { "EcommerceShoppingCartLine_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "ShoppingCartReference_Id" });
            this.DropColumn("dbo.Users", "Cart_Id");
            this.DropColumn("dbo.EcommerceVariants", "EcommerceShoppingCartLine_Id");
            this.DropColumn("dbo.EcommerceOrders", "ShoppingCartReference_Id");
            this.DropTable("dbo.EcommerceShoppingCarts");
            this.DropTable("dbo.EcommerceShoppingCartLines");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceShoppingCartLines",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ProductQuantity = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Product_Id = c.Guid(),
                    ShoppingCart_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceShoppingCarts",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    SessionId = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Coupon_Id = c.Guid(),
                    ShippingAddress_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceOrders", "ShoppingCartReference_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceVariants", "EcommerceShoppingCartLine_Id", c => c.Guid());
            this.AddColumn("dbo.Users", "Cart_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceOrders", "ShoppingCartReference_Id");
            this.CreateIndex("dbo.EcommerceVariants", "EcommerceShoppingCartLine_Id");
            this.CreateIndex("dbo.EcommerceShoppingCartLines", "ShoppingCart_Id");
            this.CreateIndex("dbo.EcommerceShoppingCartLines", "Product_Id");
            this.CreateIndex("dbo.EcommerceShoppingCarts", "ShippingAddress_Id");
            this.CreateIndex("dbo.EcommerceShoppingCarts", "Coupon_Id");
            this.CreateIndex("dbo.EcommerceShoppingCarts", "Country_Id");
            this.CreateIndex("dbo.Users", "Cart_Id");
            this.AddForeignKey("dbo.EcommerceOrders", "ShoppingCartReference_Id", "dbo.EcommerceShoppingCarts", "Id");
            this.AddForeignKey("dbo.Users", "Cart_Id", "dbo.EcommerceShoppingCarts", "Id");
            this.AddForeignKey("dbo.EcommerceShoppingCartLines", "ShoppingCart_Id", "dbo.EcommerceShoppingCarts", "Id");
            this.AddForeignKey("dbo.EcommerceVariants", "EcommerceShoppingCartLine_Id", "dbo.EcommerceShoppingCartLines", "Id");
            this.AddForeignKey("dbo.EcommerceShoppingCartLines", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceShoppingCarts", "ShippingAddress_Id", "dbo.Addresses", "Id");
            this.AddForeignKey("dbo.EcommerceShoppingCarts", "Coupon_Id", "dbo.EcommerceCoupons", "Id");
            this.AddForeignKey("dbo.EcommerceShoppingCarts", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
