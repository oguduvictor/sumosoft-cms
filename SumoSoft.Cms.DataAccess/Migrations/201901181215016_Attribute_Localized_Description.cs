namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Attribute_Localized_Description : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.AttributeLocalizedKits", "Description", c => c.String());
            this.AddColumn("dbo.AttributeOptionLocalizedKits", "Description", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.AttributeOptionLocalizedKits", "Description");
            this.DropColumn("dbo.AttributeLocalizedKits", "Description");
        }
    }
}
