namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Coupon_Categories : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceCouponEcommerceCategories",
                c => new
                {
                    EcommerceCoupon_Id = c.Guid(nullable: false),
                    EcommerceCategory_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceCoupon_Id, t.EcommerceCategory_Id })
                .ForeignKey("dbo.EcommerceCoupons", t => t.EcommerceCoupon_Id)
                .ForeignKey("dbo.EcommerceCategories", t => t.EcommerceCategory_Id)
                .Index(t => t.EcommerceCoupon_Id)
                .Index(t => t.EcommerceCategory_Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceCouponEcommerceCategories", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCouponEcommerceCategories", "EcommerceCoupon_Id", "dbo.EcommerceCoupons");
            this.DropIndex("dbo.EcommerceCouponEcommerceCategories", new[] { "EcommerceCategory_Id" });
            this.DropIndex("dbo.EcommerceCouponEcommerceCategories", new[] { "EcommerceCoupon_Id" });
            this.DropTable("dbo.EcommerceCouponEcommerceCategories");
        }
    }
}
