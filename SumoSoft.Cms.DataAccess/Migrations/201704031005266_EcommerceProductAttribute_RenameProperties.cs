namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceProductAttribute_RenameProperties : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn(table: "dbo.EcommerceProductAttributes", name: "SelectValue_Id", newName: "AttributeOptionValue_Id");
            this.RenameColumn(table: "dbo.EcommerceProductAttributes", name: "TextValue", newName: "StringValue");
            this.RenameColumn(table: "dbo.EcommerceProductAttributes", name: "CheckboxValue", newName: "BooleanValue");
            this.RenameIndex(table: "dbo.EcommerceProductAttributes", name: "IX_SelectValue_Id", newName: "IX_AttributeOptionValue_Id");

        }

        public override void Down()
        {
            this.RenameIndex(table: "dbo.EcommerceProductAttributes", name: "IX_AttributeOptionValue_Id", newName: "IX_SelectValue_Id");
            this.RenameColumn(table: "dbo.EcommerceProductAttributes", name: "AttributeOptionValue_Id", newName: "SelectValue_Id");
            this.RenameColumn(table: "dbo.EcommerceProductAttributes", name: " StringValue", newName: "TextValue");
            this.RenameColumn(table: "dbo.EcommerceProductAttributes", name: "BooleanValue", newName: "CheckboxValue");
        }
    }
}
