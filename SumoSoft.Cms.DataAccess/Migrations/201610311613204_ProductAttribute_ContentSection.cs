namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ProductAttribute_ContentSection : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductAttributes", "ContentSectionValue_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceProductAttributes", "ContentSectionValue_Id");
            this.AddForeignKey("dbo.EcommerceProductAttributes", "ContentSectionValue_Id", "dbo.ContentSections", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceProductAttributes", "ContentSectionValue_Id", "dbo.ContentSections");
            this.DropIndex("dbo.EcommerceProductAttributes", new[] { "ContentSectionValue_Id" });
            this.DropColumn("dbo.EcommerceProductAttributes", "ContentSectionValue_Id");
        }
    }
}
