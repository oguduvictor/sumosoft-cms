namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_145c : DbMigration
    {
        public override void Up()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceProducts_dbo.EcommerceOptions_OptionThatOverridesTheStock_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceProducts] DROP [FK_dbo.EcommerceProducts_dbo.EcommerceOptions_OptionThatOverridesTheStock_Id]");

            this.DropForeignKey("dbo.EcommerceProducts", "VariantThatOverridesTheStock_Id", "dbo.EcommerceVariants");
            this.DropIndex("dbo.EcommerceProducts", new[] { "VariantThatOverridesTheStock_Id" });
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalProductStock");
            this.DropColumn("dbo.EcommerceProducts", "UseLocalProductStock");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalProductStockIfLocalProductStockIsEmpty");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalVariantStock");
            this.DropColumn("dbo.EcommerceProducts", "UseLocalVariantStock");
            this.DropColumn("dbo.EcommerceProducts", "UseGlobalVariantStockIfLocalVariantStockIsEmpty");
            this.DropColumn("dbo.EcommerceProducts", "VariantThatOverridesTheStock_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProducts", "VariantThatOverridesTheStock_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalVariantStockIfLocalVariantStockIsEmpty", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseLocalVariantStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalVariantStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalProductStockIfLocalProductStockIsEmpty", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseLocalProductStock", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "UseGlobalProductStock", c => c.Boolean(nullable: false));
            this.CreateIndex("dbo.EcommerceProducts", "VariantThatOverridesTheStock_Id");
            this.AddForeignKey("dbo.EcommerceProducts", "VariantThatOverridesTheStock_Id", "dbo.EcommerceVariants", "Id");
        }
    }
}
