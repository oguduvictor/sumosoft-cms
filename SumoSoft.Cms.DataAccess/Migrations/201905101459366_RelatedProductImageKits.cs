namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RelatedProductImageKits : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductImageKits", "EcommerceProductImageKit_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceProductImageKits", "EcommerceProductImageKit_Id");
            this.AddForeignKey("dbo.EcommerceProductImageKits", "EcommerceProductImageKit_Id", "dbo.EcommerceProductImageKits", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceProductImageKits", "EcommerceProductImageKit_Id", "dbo.EcommerceProductImageKits");
            this.DropIndex("dbo.EcommerceProductImageKits", new[] { "EcommerceProductImageKit_Id" });
            this.DropColumn("dbo.EcommerceProductImageKits", "EcommerceProductImageKit_Id");
        }
    }
}
