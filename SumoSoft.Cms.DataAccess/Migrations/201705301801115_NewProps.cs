namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class NewProps : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductStockUnits", "ReleaseDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceOrderShippingBoxes", "InternalDescription", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderShippingBoxes", "InternalDescription");
            this.DropColumn("dbo.EcommerceProductStockUnits", "ReleaseDate");
        }
    }
}
