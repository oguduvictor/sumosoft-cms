namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Order_TotalToBePaid : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn(table: "dbo.EcommerceOrders", name: "TotalPaid", newName: "TotalToBePaid");
        }

        public override void Down()
        {
            this.RenameColumn(table: "dbo.EcommerceOrders", name: "TotalToBePaid", newName: "TotalPaid");
        }
    }
}
