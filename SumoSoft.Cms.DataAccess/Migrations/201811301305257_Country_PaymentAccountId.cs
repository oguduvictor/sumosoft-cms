namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Country_PaymentAccountId : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.Countries", "PaymentAccountId", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.Countries", "PaymentAccountId");
        }
    }
}
