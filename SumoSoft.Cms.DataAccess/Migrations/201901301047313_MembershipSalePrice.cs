namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class MembershipSalePrice : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductStockUnitLocalizedKits", "MembershipSalePrice", c => c.Double(nullable: false));
            this.AddColumn("dbo.Users", "ShowMembershipSalePrice", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.Users", "ShowMembershipSalePrice");
            this.DropColumn("dbo.EcommerceProductStockUnitLocalizedKits", "MembershipSalePrice");
        }
    }
}
