namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CountryFlagIcon_126 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.Countries", "FlagIcon", c => c.String());
            this.DropColumn("dbo.Countries", "FlagImageRootUrl");
        }

        public override void Down()
        {
            this.AddColumn("dbo.Countries", "FlagImageRootUrl", c => c.String());
            this.DropColumn("dbo.Countries", "FlagIcon");
        }
    }
}
