// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Remove_CartItem_UserCredit : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Remove_CartItem_UserCredit));
        
        string IMigrationMetadata.Id
        {
            get { return "201702241834178_Remove_CartItem_UserCredit"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
