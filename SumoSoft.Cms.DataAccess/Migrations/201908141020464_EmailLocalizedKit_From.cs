namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class EmailLocalizedKit_From : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EmailLocalizedKits", "From", c => c.String());
            this.AddColumn("dbo.EmailLocalizedKits", "ReplyTo", c => c.String());
            this.AddColumn("dbo.EmailLocalizedKits", "DisplayName", c => c.String());
            this.DropColumn("dbo.EmailLocalizedKits", "Subject");
            this.DropColumn("dbo.EmailLocalizedKits", "Content");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EmailLocalizedKits", "Content", c => c.String());
            this.AddColumn("dbo.EmailLocalizedKits", "Subject", c => c.String());
            this.DropColumn("dbo.EmailLocalizedKits", "DisplayName");
            this.DropColumn("dbo.EmailLocalizedKits", "ReplyTo");
            this.DropColumn("dbo.EmailLocalizedKits", "From");
        }
    }
}
