namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_190 : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceProductAttributes", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceProductAttributes", new[] { "Country_Id" });
            this.DropColumn("dbo.EcommerceProductAttributes", "Country_Id");

            this.Sql("WITH ProdAttr AS (SELECT ROW_NUMBER() OVER(PARTITION BY [Product_Id], [Attribute_Id] ORDER BY [Product_Id], [Attribute_Id]) AS row FROM [dbo].[EcommerceProductAttributes]) DELETE FROM ProdAttr WHERE row > 1");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProductAttributes", "Country_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceProductAttributes", "Country_Id");
            this.AddForeignKey("dbo.EcommerceProductAttributes", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
