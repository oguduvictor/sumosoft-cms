namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class RenameProps : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceVariantOptions", "Image", c => c.String());
            this.AddColumn("dbo.EcommerceVariantOptionLocalizedKits", "Description", c => c.String());

            this.RenameColumn(table: "dbo.EcommerceVariantOptionLocalizedKits", name: "Text", newName: "Title");
            this.RenameColumn(table: "dbo.EcommerceVariantLocalizedKits", name: "Label", newName: "Title");
            this.RenameColumn(table: "dbo.EcommerceShippingBoxStatus", name: "Value", newName: "Name");
            this.RenameColumn(table: "dbo.EcommerceAttributeLocalizedKits", name: "Label", newName: "Title");
            this.RenameColumn(table: "dbo.EcommerceAttributeOptionLocalizedKits", name: "Text", newName: "Title");
            this.RenameColumn(table: "dbo.EcommerceProductLocalizedKits", name: "Name", newName: "Title");
            this.RenameColumn(table: "dbo.BlogTags", name: "Title", newName: "Name");
            this.RenameColumn(table: "dbo.EcommerceOrderStatus", name: "Value", newName: "Name");
            this.RenameColumn(table: "dbo.EcommerceOrderShippingBoxStatus", name: "Value", newName: "Name");
            this.RenameColumn(table: "dbo.EcommerceOrderItemVariants", name: "VariantLocalizedLabel", newName: "VariantLocalizedTitle");
            this.RenameColumn(table: "dbo.EcommerceOrderItemVariants", name: "VariantOptionLocalizedText", newName: "VariantOptionLocalizedTitle");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductName", newName: "ProductTitle");
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceVariantOptions", "Image");
            this.DropColumn("dbo.EcommerceVariantOptionLocalizedKits", "Description");

            this.RenameColumn(table: "dbo.EcommerceVariantOptionLocalizedKits", name: "Title", newName: "Text");
            this.RenameColumn(table: "dbo.EcommerceVariantLocalizedKits", name: "Title", newName: "Label");
            this.RenameColumn(table: "dbo.EcommerceShippingBoxStatus", name: "Name", newName: "Value");
            this.RenameColumn(table: "dbo.EcommerceAttributeLocalizedKits", name: "Title", newName: "Label");
            this.RenameColumn(table: "dbo.EcommerceAttributeOptionLocalizedKits", name: "Title", newName: "Text");
            this.RenameColumn(table: "dbo.EcommerceProductLocalizedKits", name: "Title", newName: "Name");
            this.RenameColumn(table: "dbo.BlogTags", name: "Name", newName: "Title");
            this.RenameColumn(table: "dbo.EcommerceOrderStatus", name: "Name", newName: "Value");
            this.RenameColumn(table: "dbo.EcommerceOrderShippingBoxStatus", name: "Name", newName: "Value");
            this.RenameColumn(table: "dbo.EcommerceOrderItemVariants", name: "VariantLocalizedTitle", newName: "VariantLocalizedLabel");
            this.RenameColumn(table: "dbo.EcommerceOrderItemVariants", name: "VariantOptionLocalizedTitle", newName: "VariantOptionLocalizedText");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductTitle", newName: "ProductName");
        }
    }
}
