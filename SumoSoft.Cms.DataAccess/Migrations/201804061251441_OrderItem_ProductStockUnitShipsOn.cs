namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class OrderItem_ProductStockUnitShipsOn : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitShipsOn", c => c.DateTime());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitShipsOn");
        }
    }
}
