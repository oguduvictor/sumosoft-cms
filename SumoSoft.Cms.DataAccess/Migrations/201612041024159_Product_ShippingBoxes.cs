namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Product_ShippingBoxes : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.BlogPostBlogCategories", "BlogPost_Id", "dbo.BlogPosts");
            this.DropForeignKey("dbo.BlogPostBlogCategories", "BlogCategory_Id", "dbo.BlogCategories");
            this.CreateTable(
                "dbo.EcommerceShippingBoxEcommerceProducts",
                c => new
                {
                    EcommerceShippingBox_Id = c.Guid(nullable: false),
                    EcommerceProduct_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceShippingBox_Id, t.EcommerceProduct_Id })
                .ForeignKey("dbo.EcommerceShippingBoxes", t => t.EcommerceShippingBox_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.EcommerceProduct_Id)
                .Index(t => t.EcommerceShippingBox_Id)
                .Index(t => t.EcommerceProduct_Id);

            this.AddColumn("dbo.EcommerceShippingBoxLocalizedKits", "Carrier", c => c.String());
            this.AddColumn("dbo.EcommerceShippingBoxLocalizedKits", "FreeShippingMinimumPrice", c => c.Double(nullable: false));
            this.AddForeignKey("dbo.BlogPostBlogCategories", "BlogPost_Id", "dbo.BlogPosts", "Id");
            this.AddForeignKey("dbo.BlogPostBlogCategories", "BlogCategory_Id", "dbo.BlogCategories", "Id");
            this.DropColumn("dbo.EcommerceCarts", "IsTrial");
            this.DropColumn("dbo.EcommerceOrders", "IsTrial");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrders", "IsTrial", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceCarts", "IsTrial", c => c.Boolean(nullable: false));
            this.DropForeignKey("dbo.BlogPostBlogCategories", "BlogCategory_Id", "dbo.BlogCategories");
            this.DropForeignKey("dbo.BlogPostBlogCategories", "BlogPost_Id", "dbo.BlogPosts");
            this.DropForeignKey("dbo.EcommerceShippingBoxEcommerceProducts", "EcommerceProduct_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceShippingBoxEcommerceProducts", "EcommerceShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropIndex("dbo.EcommerceShippingBoxEcommerceProducts", new[] { "EcommerceProduct_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxEcommerceProducts", new[] { "EcommerceShippingBox_Id" });
            this.DropColumn("dbo.EcommerceShippingBoxLocalizedKits", "FreeShippingMinimumPrice");
            this.DropColumn("dbo.EcommerceShippingBoxLocalizedKits", "Carrier");
            this.DropTable("dbo.EcommerceShippingBoxEcommerceProducts");
            this.AddForeignKey("dbo.BlogPostBlogCategories", "BlogCategory_Id", "dbo.BlogCategories", "Id", cascadeDelete: true);
            this.AddForeignKey("dbo.BlogPostBlogCategories", "BlogPost_Id", "dbo.BlogPosts", "Id", cascadeDelete: true);
        }
    }
}
