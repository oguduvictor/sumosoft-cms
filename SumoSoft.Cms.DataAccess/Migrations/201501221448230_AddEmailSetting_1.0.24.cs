namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddEmailSetting_1024 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.CmsSettings", "SmtpEmail", c => c.String());
            this.AddColumn("dbo.CmsSettings", "SmtpPassword", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.CmsSettings", "SmtpPassword");
            this.DropColumn("dbo.CmsSettings", "SmtpEmail");
        }
    }
}
