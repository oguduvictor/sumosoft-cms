namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_136 : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"ALTER TABLE [dbo].[EcommerceProductAttributes] DROP CONSTRAINT [FK_dbo.EcommerceProductAttributes_dbo.Countries_Attribute_Id]
                ALTER TABLE [dbo].[EcommerceProductAttributes] ADD CONSTRAINT [FK_dbo.EcommerceProductAttributes_dbo.Attributes_Attribute_Id]
                FOREIGN KEY (Attribute_Id) REFERENCES EcommerceAttributes(Id)");

            this.AddColumn("dbo.EcommerceProducts", "Url", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceProducts", "Url");
        }
    }
}
