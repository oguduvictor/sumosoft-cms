namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_178 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.Addresses", "StateCountyProvince", c => c.String());
            this.AddColumn("dbo.EcommerceOrderShippingAddresses", "StateCountyProvince", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderShippingAddresses", "StateCountyProvince");
            this.DropColumn("dbo.Addresses", "StateCountyProvince");
        }
    }
}
