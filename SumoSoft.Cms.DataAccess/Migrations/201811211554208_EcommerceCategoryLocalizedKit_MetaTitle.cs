namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class EcommerceCategoryLocalizedKit_MetaTitle : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategoryLocalizedKits", "MetaTitle", c => c.String());
            this.AddColumn("dbo.EcommerceCategoryLocalizedKits", "MetaDescription", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCategoryLocalizedKits", "MetaDescription");
            this.DropColumn("dbo.EcommerceCategoryLocalizedKits", "MetaTitle");
        }
    }
}
