﻿namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Removed_Explicit_Names_In_IndexAnnotation : DbMigration
    {
        public override void Up()
        {
            RenameIndex(table: "dbo.Countries", name: "IX_Country_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.Countries", name: "IX_Country_LanguageCode", newName: "IX_LanguageCode");
            RenameIndex(table: "dbo.ShippingBoxes", name: "IX_ShippingBox_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.Products", name: "IX_Product_Url", newName: "IX_Url");
            RenameIndex(table: "dbo.Categories", name: "IX_Category_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.Categories", name: "IX_Category_Url", newName: "IX_Url");
            RenameIndex(table: "dbo.VariantOptions", name: "IX_VariantOption_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.VariantOptions", name: "IX_VariantOption_Url", newName: "IX_Url");
            RenameIndex(table: "dbo.Variants", name: "IX_Variant_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.Variants", name: "IX_Variant_Url", newName: "IX_Url");
            RenameIndex(table: "dbo.Attributes", name: "IX_Attribute_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.Attributes", name: "IX_Attribute_Url", newName: "IX_Url");
            RenameIndex(table: "dbo.AttributeOptions", name: "IX_AttributeOption_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.AttributeOptions", name: "IX_AttributeOption_Url", newName: "IX_Url");
            RenameIndex(table: "dbo.Coupons", name: "IX_Coupon_Code", newName: "IX_Code");
            RenameIndex(table: "dbo.UserRoles", name: "IX_UserRole_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.Emails", name: "IX_Email_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.OrderShippingBoxes", name: "IX_OrderShippingBox_Name", newName: "IX_Name");
            RenameIndex(table: "dbo.BlogPosts", name: "IX_BlogPost_Url", newName: "IX_Url");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.BlogPosts", name: "IX_Url", newName: "IX_BlogPost_Url");
            RenameIndex(table: "dbo.OrderShippingBoxes", name: "IX_Name", newName: "IX_OrderShippingBox_Name");
            RenameIndex(table: "dbo.Emails", name: "IX_Name", newName: "IX_Email_Name");
            RenameIndex(table: "dbo.UserRoles", name: "IX_Name", newName: "IX_UserRole_Name");
            RenameIndex(table: "dbo.Coupons", name: "IX_Code", newName: "IX_Coupon_Code");
            RenameIndex(table: "dbo.AttributeOptions", name: "IX_Url", newName: "IX_AttributeOption_Url");
            RenameIndex(table: "dbo.AttributeOptions", name: "IX_Name", newName: "IX_AttributeOption_Name");
            RenameIndex(table: "dbo.Attributes", name: "IX_Url", newName: "IX_Attribute_Url");
            RenameIndex(table: "dbo.Attributes", name: "IX_Name", newName: "IX_Attribute_Name");
            RenameIndex(table: "dbo.Variants", name: "IX_Url", newName: "IX_Variant_Url");
            RenameIndex(table: "dbo.Variants", name: "IX_Name", newName: "IX_Variant_Name");
            RenameIndex(table: "dbo.VariantOptions", name: "IX_Url", newName: "IX_VariantOption_Url");
            RenameIndex(table: "dbo.VariantOptions", name: "IX_Name", newName: "IX_VariantOption_Name");
            RenameIndex(table: "dbo.Categories", name: "IX_Url", newName: "IX_Category_Url");
            RenameIndex(table: "dbo.Categories", name: "IX_Name", newName: "IX_Category_Name");
            RenameIndex(table: "dbo.Products", name: "IX_Url", newName: "IX_Product_Url");
            RenameIndex(table: "dbo.ShippingBoxes", name: "IX_Name", newName: "IX_ShippingBox_Name");
            RenameIndex(table: "dbo.Countries", name: "IX_LanguageCode", newName: "IX_Country_LanguageCode");
            RenameIndex(table: "dbo.Countries", name: "IX_Name", newName: "IX_Country_Name");
        }
    }
}
