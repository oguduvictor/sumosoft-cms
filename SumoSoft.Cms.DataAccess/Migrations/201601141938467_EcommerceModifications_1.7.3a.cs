namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_173a : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.Coupons",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Code = c.String(),
                    Amount = c.Double(nullable: false),
                    Percentage = c.Double(nullable: false),
                    Published = c.Boolean(nullable: false),
                    ExpirationDate = c.DateTime(nullable: false),
                    ValidityTimes = c.Int(nullable: false),
                    Note = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Country_Id);

            this.AddColumn("dbo.EcommerceCarts", "Coupon_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrders", "Coupon_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCarts", "Coupon_Id");
            this.CreateIndex("dbo.EcommerceOrders", "Coupon_Id");
            this.AddForeignKey("dbo.EcommerceCarts", "Coupon_Id", "dbo.Coupons", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "Coupon_Id", "dbo.Coupons", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceOrders", "Coupon_Id", "dbo.Coupons");
            this.DropForeignKey("dbo.EcommerceCarts", "Coupon_Id", "dbo.Coupons");
            this.DropForeignKey("dbo.Coupons", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceOrders", new[] { "Coupon_Id" });
            this.DropIndex("dbo.Coupons", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceCarts", new[] { "Coupon_Id" });
            this.DropColumn("dbo.EcommerceOrders", "Coupon_Id");
            this.DropColumn("dbo.EcommerceCarts", "Coupon_Id");
            this.DropTable("dbo.Coupons");
        }
    }
}
