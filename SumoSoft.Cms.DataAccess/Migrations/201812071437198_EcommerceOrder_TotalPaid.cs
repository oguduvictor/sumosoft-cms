namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class EcommerceOrder_TotalPaid : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrders", "TotalPaid", c => c.Double(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrders", "TotalPaid");
        }
    }
}
