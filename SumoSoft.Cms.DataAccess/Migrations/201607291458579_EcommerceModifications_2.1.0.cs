namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_210 : DbMigration
    {
        public override void Up()
        {
            this.Sql("UPDATE [dbo].[Users] SET [dbo].[Users].[Cart_Id] = NULL FROM [dbo].[Users]");
            this.Sql("DELETE FROM [dbo].[EcommerceCartItemProductVariants]");
            this.Sql("DELETE FROM [dbo].[EcommerceCartItems]");
            this.Sql("DELETE FROM [dbo].[EcommerceCarts]");

            this.DropForeignKey("dbo.EcommerceCartItems", "Cart_Id", "dbo.EcommerceCarts");
            this.DropForeignKey("dbo.EcommerceOrderItems", "Order_Id", "dbo.EcommerceOrders");

            this.DropIndex("dbo.EcommerceCartItems", new[] { "Cart_Id" });
            this.DropIndex("dbo.EcommerceOrderItems", new[] { "Order_Id" });

            this.CreateTable(
                "dbo.EcommerceCartShippingBoxes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Cart_Id = c.Guid(),
                    ShippingBox_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceCarts", t => t.Cart_Id)
                .ForeignKey("dbo.EcommerceShippingBoxes", t => t.ShippingBox_Id)
                .Index(t => t.Cart_Id)
                .Index(t => t.ShippingBox_Id);

            this.CreateTable(
                "dbo.EcommerceShippingBoxes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceShippingBoxLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Description = c.String(),
                    MinDays = c.Int(nullable: false),
                    MaxDays = c.Int(nullable: false),
                    CountWeekends = c.Boolean(nullable: false),
                    Price = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ShippingBox_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceShippingBoxes", t => t.ShippingBox_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ShippingBox_Id);

            this.CreateTable(
                "dbo.EcommerceOrderShippingBoxes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Description = c.String(),
                    MinDays = c.Int(nullable: false),
                    MaxDays = c.Int(nullable: false),
                    CountWeekends = c.Boolean(nullable: false),
                    Price = c.Double(nullable: false),
                    TrackingCode = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Order_Id = c.Guid(),
                    OrderShippingBoxStatus_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOrders", t => t.Order_Id)
                .ForeignKey("dbo.EcommerceOrderShippingBoxStatus", t => t.OrderShippingBoxStatus_Id)
                .Index(t => t.Order_Id)
                .Index(t => t.OrderShippingBoxStatus_Id);

            this.CreateTable(
                "dbo.EcommerceOrderShippingBoxStatus",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.String(),
                    SortOrder = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOrderShippingBoxStatusLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    OrderShippingBoxStatus_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceOrderShippingBoxStatus", t => t.OrderShippingBoxStatus_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.OrderShippingBoxStatus_Id);

            this.AddColumn("dbo.EcommerceCartItems", "OrderItemStockStatus", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceCartItems", "CartShippingBox_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderItems", "OrderItemStockStatus", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "OrderShippingBox_Id", c => c.Guid());

            this.CreateIndex("dbo.EcommerceCartItems", "CartShippingBox_Id");
            this.CreateIndex("dbo.EcommerceOrderItems", "OrderShippingBox_Id");

            this.AddForeignKey("dbo.EcommerceCartItems", "CartShippingBox_Id", "dbo.EcommerceCartShippingBoxes", "Id");
            this.AddForeignKey("dbo.EcommerceOrderItems", "OrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes", "Id");

            this.Sql("INSERT INTO [dbo].[EcommerceOrderShippingBoxes] (Order_Id, TrackingCode, MinDays, MaxDays, Price, CountWeekends, IsDisabled, CreatedDate) SELECT [dbo].[EcommerceOrders].[Id], [dbo].[EcommerceOrders].[TrackingCode], 0, 0, 0, 0, 0, getdate() FROM [dbo].[EcommerceOrders]");
            this.Sql("UPDATE [dbo].[EcommerceOrderItems] SET [dbo].[EcommerceOrderItems].[OrderShippingBox_Id] = [dbo].[EcommerceOrderShippingBoxes].[Id] FROM [dbo].[EcommerceOrderShippingBoxes] INNER JOIN [dbo].[EcommerceOrderItems] ON [dbo].[EcommerceOrderShippingBoxes].[Order_Id] = [dbo].[EcommerceOrderItems].[Order_Id]");
            this.Sql("UPDATE [dbo].[EcommerceOrderItems] SET [dbo].[EcommerceOrderItems].[OrderItemStockStatus] = [dbo].[EcommerceOrderItems].[OrderStockStatus] FROM [dbo].[EcommerceOrderItems]");

            this.DropColumn("dbo.Users", "RegistrationDate");
            this.DropColumn("dbo.EcommerceCartItems", "OrderStockStatus");
            this.DropColumn("dbo.EcommerceCartItems", "ShippingPrice");
            this.DropColumn("dbo.EcommerceCartItems", "Cart_Id");
            this.DropColumn("dbo.EcommerceOrderItems", "OrderStockStatus");
            this.DropColumn("dbo.EcommerceOrderItems", "ShippingPrice");
            this.DropColumn("dbo.EcommerceOrderItems", "Order_Id");
            this.DropColumn("dbo.EcommerceOrders", "TrackingCode");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrders", "TrackingCode", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItems", "Order_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderItems", "ShippingPrice", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "OrderStockStatus", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceCartItems", "Cart_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCartItems", "ShippingPrice", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceCartItems", "OrderStockStatus", c => c.Int(nullable: false));
            this.AddColumn("dbo.Users", "RegistrationDate", c => c.DateTime(nullable: false));

            this.DropForeignKey("dbo.EcommerceOrderShippingBoxes", "OrderShippingBoxStatus_Id", "dbo.EcommerceOrderShippingBoxStatus");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "OrderShippingBoxStatus_Id", "dbo.EcommerceOrderShippingBoxStatus");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceOrderItems", "OrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxes", "Order_Id", "dbo.EcommerceOrders");
            this.DropForeignKey("dbo.EcommerceCartShippingBoxes", "ShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropForeignKey("dbo.EcommerceShippingBoxLocalizedKits", "ShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropForeignKey("dbo.EcommerceShippingBoxLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceCartItems", "CartShippingBox_Id", "dbo.EcommerceCartShippingBoxes");
            this.DropForeignKey("dbo.EcommerceCartShippingBoxes", "Cart_Id", "dbo.EcommerceCarts");

            this.DropIndex("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", new[] { "OrderShippingBoxStatus_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxes", new[] { "OrderShippingBoxStatus_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxes", new[] { "Order_Id" });
            this.DropIndex("dbo.EcommerceOrderItems", new[] { "OrderShippingBox_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxLocalizedKits", new[] { "ShippingBox_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceCartItems", new[] { "CartShippingBox_Id" });
            this.DropIndex("dbo.EcommerceCartShippingBoxes", new[] { "ShippingBox_Id" });
            this.DropIndex("dbo.EcommerceCartShippingBoxes", new[] { "Cart_Id" });

            this.DropColumn("dbo.EcommerceOrderItems", "OrderShippingBox_Id");
            this.DropColumn("dbo.EcommerceOrderItems", "OrderItemStockStatus");
            this.DropColumn("dbo.EcommerceCartItems", "CartShippingBox_Id");
            this.DropColumn("dbo.EcommerceCartItems", "OrderItemStockStatus");

            this.DropTable("dbo.EcommerceOrderShippingBoxStatusLocalizedKits");
            this.DropTable("dbo.EcommerceOrderShippingBoxStatus");
            this.DropTable("dbo.EcommerceOrderShippingBoxes");
            this.DropTable("dbo.EcommerceShippingBoxLocalizedKits");
            this.DropTable("dbo.EcommerceShippingBoxes");
            this.DropTable("dbo.EcommerceCartShippingBoxes");

            this.CreateIndex("dbo.EcommerceOrderItems", "Order_Id");
            this.CreateIndex("dbo.EcommerceCartItems", "Cart_Id");

            this.AddForeignKey("dbo.EcommerceOrderItems", "Order_Id", "dbo.EcommerceOrders", "Id");
            this.AddForeignKey("dbo.EcommerceCartItems", "Cart_Id", "dbo.EcommerceCarts", "Id");
        }
    }
}
