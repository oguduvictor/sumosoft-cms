namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_141 : DbMigration
    {
        public override void Up()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceProductVariants_dbo.EcommerceVariantTypeOptions_SelectValue_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceProductVariants] DROP [FK_dbo.EcommerceProductVariants_dbo.EcommerceVariantTypeOptions_SelectValue_Id]");

            this.DropForeignKey("dbo.EcommerceProductVariants", "SelectValue_Id", "dbo.EcommerceVariantOptions");
            this.DropIndex("dbo.EcommerceProductVariants", new[] { "SelectValue_Id" });
            this.CreateTable(
                "dbo.EcommerceProductVariantOptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    ProductVariant_Id = c.Guid(),
                    VariantOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProductVariants", t => t.ProductVariant_Id)
                .ForeignKey("dbo.EcommerceVariantOptions", t => t.VariantOption_Id)
                .Index(t => t.ProductVariant_Id)
                .Index(t => t.VariantOption_Id);

            this.DropColumn("dbo.EcommerceProductVariants", "CheckboxValue");
            this.DropColumn("dbo.EcommerceProductVariants", "SelectValue_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProductVariants", "SelectValue_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProductVariants", "CheckboxValue", c => c.Boolean(nullable: false));
            this.DropForeignKey("dbo.EcommerceProductVariantOptions", "VariantOption_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceProductVariantOptions", "ProductVariant_Id", "dbo.EcommerceProductVariants");
            this.DropIndex("dbo.EcommerceProductVariantOptions", new[] { "VariantOption_Id" });
            this.DropIndex("dbo.EcommerceProductVariantOptions", new[] { "ProductVariant_Id" });
            this.DropTable("dbo.EcommerceProductVariantOptions");
            this.CreateIndex("dbo.EcommerceProductVariants", "SelectValue_Id");
            this.AddForeignKey("dbo.EcommerceProductVariants", "SelectValue_Id", "dbo.EcommerceVariantOptions", "Id");
        }
    }
}
