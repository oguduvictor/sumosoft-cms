namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Tax_ShippingAddress : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceTaxes", "ShippingAddress_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceTaxes", "ShippingAddress_Id");
            this.AddForeignKey("dbo.EcommerceTaxes", "ShippingAddress_Id", "dbo.Addresses", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceTaxes", "ShippingAddress_Id", "dbo.Addresses");
            this.DropIndex("dbo.EcommerceTaxes", new[] { "ShippingAddress_Id" });
            this.DropColumn("dbo.EcommerceTaxes", "ShippingAddress_Id");
        }
    }
}
