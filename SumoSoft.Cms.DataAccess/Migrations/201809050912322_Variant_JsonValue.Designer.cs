// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Variant_JsonValue : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Variant_JsonValue));
        
        string IMigrationMetadata.Id
        {
            get { return "201809050912322_Variant_JsonValue"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
