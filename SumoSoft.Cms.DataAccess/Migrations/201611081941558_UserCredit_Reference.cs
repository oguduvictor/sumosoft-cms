namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class UserCredit_Reference : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceUserCredits", "Reference", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceUserCredits", "Reference");
        }
    }
}
