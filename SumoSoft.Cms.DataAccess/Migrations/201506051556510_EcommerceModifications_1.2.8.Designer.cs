// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class EcommerceModifications_128 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EcommerceModifications_128));
        
        string IMigrationMetadata.Id
        {
            get { return "201506051556510_EcommerceModifications_1.2.8"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
