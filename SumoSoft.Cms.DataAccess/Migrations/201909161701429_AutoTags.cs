namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AutoTags : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProducts", "AutoTags", c => c.String());
            this.AddColumn("dbo.EcommerceCategories", "AutoTags", c => c.String());
            this.AddColumn("dbo.EcommerceVariantOptions", "AutoTags", c => c.String());
            this.AddColumn("dbo.AttributeOptions", "AutoTags", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "Tags", c => c.String());
            this.AddColumn("dbo.EcommerceOrders", "AutoTags", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrders", "AutoTags");
            this.DropColumn("dbo.EcommerceOrders", "Tags");
            this.DropColumn("dbo.AttributeOptions", "AutoTags");
            this.DropColumn("dbo.EcommerceVariantOptions", "AutoTags");
            this.DropColumn("dbo.EcommerceCategories", "AutoTags");
            this.DropColumn("dbo.EcommerceProducts", "AutoTags");
        }
    }
}
