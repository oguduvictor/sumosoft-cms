namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class OrderItem_MinMaxEta : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "MinEta", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "MaxEta", c => c.DateTime(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "MaxEta");
            this.DropColumn("dbo.EcommerceOrderItems", "MinEta");
        }
    }
}
