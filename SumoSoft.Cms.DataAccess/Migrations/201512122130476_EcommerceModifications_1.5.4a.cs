namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_154a : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceProductVariants", "DefaultSelectValue_Id", "dbo.EcommerceVariantOptions");
            this.DropIndex("dbo.EcommerceProductVariants", new[] { "DefaultSelectValue_Id" });
            this.AddColumn("dbo.EcommerceProductVariantOptions", "IsDefault", c => c.Boolean(nullable: false));
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultSelectValue_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultSelectValue_Id", c => c.Guid());
            this.DropColumn("dbo.EcommerceProductVariantOptions", "IsDefault");
            this.CreateIndex("dbo.EcommerceProductVariants", "DefaultSelectValue_Id");
            this.AddForeignKey("dbo.EcommerceProductVariants", "DefaultSelectValue_Id", "dbo.EcommerceVariantOptions", "Id");
        }
    }
}
