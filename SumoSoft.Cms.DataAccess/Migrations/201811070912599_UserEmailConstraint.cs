namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class UserEmailConstraint : DbMigration
    {
        public override void Up()
        {
            this.AlterColumn("dbo.Users", "Email", c => c.String(maxLength: 255));
            this.Sql(@"CREATE UNIQUE NONCLUSTERED INDEX IX_User_Email
                    ON Users(Email)
                    WHERE Email IS NOT NULL;");
        }

        public override void Down()
        {
            this.DropIndex("dbo.Users", new[] { "Email" });
            this.AlterColumn("dbo.Users", "Email", c => c.String());
        }
    }
}
