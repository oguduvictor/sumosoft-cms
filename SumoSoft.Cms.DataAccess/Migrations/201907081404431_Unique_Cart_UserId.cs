namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Unique_Cart_UserId : DbMigration
    {
        public override void Up()
        {
            this.DropIndex("dbo.EcommerceCarts", new[] { "User_Id" });

            this.Sql(@"CREATE UNIQUE NONCLUSTERED INDEX IX_EcommerceCarts_User_Id
                    ON EcommerceCarts(User_Id)
                    WHERE User_Id IS NOT NULL;");
        }

        public override void Down()
        {
            this.DropIndex("dbo.EcommerceCarts", new[] { "User_Id" });

            this.Sql(@"CREATE NONCLUSTERED INDEX IX_User_Id
                    ON EcommerceCarts(User_Id)
                    WHERE User_Id IS NOT NULL;");
        }
    }
}
