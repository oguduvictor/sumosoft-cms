namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DefaultShippingBox : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceShippingBoxEcommerceProducts", newName: "EcommerceProductEcommerceShippingBoxes");
            this.DropPrimaryKey("dbo.EcommerceProductEcommerceShippingBoxes");
            this.AddColumn("dbo.EcommerceProducts", "DefaultShippingBox_Id", c => c.Guid());
            this.AddPrimaryKey("dbo.EcommerceProductEcommerceShippingBoxes", new[] { "EcommerceProduct_Id", "EcommerceShippingBox_Id" });
            this.CreateIndex("dbo.EcommerceProducts", "DefaultShippingBox_Id");
            this.AddForeignKey("dbo.EcommerceProducts", "DefaultShippingBox_Id", "dbo.EcommerceShippingBoxes", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceProducts", "DefaultShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropIndex("dbo.EcommerceProducts", new[] { "DefaultShippingBox_Id" });
            this.DropPrimaryKey("dbo.EcommerceProductEcommerceShippingBoxes");
            this.DropColumn("dbo.EcommerceProducts", "DefaultShippingBox_Id");
            this.AddPrimaryKey("dbo.EcommerceProductEcommerceShippingBoxes", new[] { "EcommerceShippingBox_Id", "EcommerceProduct_Id" });
            this.RenameTable(name: "dbo.EcommerceProductEcommerceShippingBoxes", newName: "EcommerceShippingBoxEcommerceProducts");
        }
    }
}
