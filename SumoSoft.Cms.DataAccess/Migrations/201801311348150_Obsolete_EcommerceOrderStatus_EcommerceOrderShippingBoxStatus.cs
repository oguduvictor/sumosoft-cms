namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Obsolete_EcommerceOrderStatus_EcommerceOrderShippingBoxStatus : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceOrderStatusLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceOrderStatusLocalizedKits", "OrderStatus_Id", "dbo.EcommerceOrderStatus");
            this.DropForeignKey("dbo.EcommerceOrders", "OrderStatus_Id", "dbo.EcommerceOrderStatus");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "OrderShippingBoxStatus_Id", "dbo.EcommerceOrderShippingBoxStatus");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxes", "OrderShippingBoxStatus_Id", "dbo.EcommerceOrderShippingBoxStatus");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes");
            this.DropIndex("dbo.EcommerceOrderShippingBoxes", new[] { "OrderShippingBoxStatus_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "OrderStatus_Id" });
            this.DropIndex("dbo.EcommerceOrderStatusLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceOrderStatusLocalizedKits", new[] { "OrderStatus_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxStatus", new[] { "OrderShippingBox_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxStatus", new[] { "EcommerceOrderShippingBox_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", new[] { "OrderShippingBoxStatus_Id" });
            this.DropColumn("dbo.Addresses", "CountryName");
            this.DropColumn("dbo.EcommerceOrderShippingBoxes", "Price");
            this.DropColumn("dbo.EcommerceOrderShippingBoxes", "OrderShippingBoxStatus_Id");
            this.DropColumn("dbo.EcommerceOrders", "Vat");
            this.DropColumn("dbo.EcommerceOrders", "OrderStatus_Id");
            this.DropTable("dbo.EcommerceOrderStatusLocalizedKits");
            this.DropTable("dbo.EcommerceOrderStatus");
            this.DropTable("dbo.EcommerceOrderShippingBoxStatusLocalizedKits");
            this.DropTable("dbo.EcommerceOrderShippingBoxStatus");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceOrderShippingBoxStatusLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    OrderShippingBoxStatus_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOrderShippingBoxStatus",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    SortOrder = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    OrderShippingBox_Id = c.Guid(),
                    EcommerceOrderShippingBox_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOrderStatusLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    OrderStatus_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOrderStatus",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    SortOrder = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceOrders", "OrderStatus_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrders", "Vat", c => c.Single(nullable: false));
            this.AddColumn("dbo.EcommerceOrderShippingBoxes", "OrderShippingBoxStatus_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderShippingBoxes", "Price", c => c.Double(nullable: false));
            this.AddColumn("dbo.Addresses", "CountryName", c => c.String());
            this.CreateIndex("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "OrderShippingBoxStatus_Id");
            this.CreateIndex("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "Country_Id");
            this.CreateIndex("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id");
            this.CreateIndex("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id");
            this.CreateIndex("dbo.EcommerceOrderStatusLocalizedKits", "OrderStatus_Id");
            this.CreateIndex("dbo.EcommerceOrderStatusLocalizedKits", "Country_Id");
            this.CreateIndex("dbo.EcommerceOrders", "OrderStatus_Id");
            this.CreateIndex("dbo.EcommerceOrderShippingBoxes", "OrderShippingBoxStatus_Id");
            this.AddForeignKey("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes", "Id");
            this.AddForeignKey("dbo.EcommerceOrderShippingBoxes", "OrderShippingBoxStatus_Id", "dbo.EcommerceOrderShippingBoxStatus", "Id");
            this.AddForeignKey("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "OrderShippingBoxStatus_Id", "dbo.EcommerceOrderShippingBoxStatus", "Id");
            this.AddForeignKey("dbo.EcommerceOrderShippingBoxStatusLocalizedKits", "Country_Id", "dbo.Countries", "Id");
            this.AddForeignKey("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "OrderStatus_Id", "dbo.EcommerceOrderStatus", "Id");
            this.AddForeignKey("dbo.EcommerceOrderStatusLocalizedKits", "OrderStatus_Id", "dbo.EcommerceOrderStatus", "Id");
            this.AddForeignKey("dbo.EcommerceOrderStatusLocalizedKits", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
