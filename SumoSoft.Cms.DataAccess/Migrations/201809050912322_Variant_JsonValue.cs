namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Variant_JsonValue : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceVariants", "DefaultJsonValue", c => c.String(maxLength: 4000));
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultJsonValue", c => c.String(maxLength: 4000));
            this.AddColumn("dbo.EcommerceCartItemVariants", "JsonValue", c => c.String(maxLength: 4000));
            this.AddColumn("dbo.EcommerceOrderItemVariants", "JsonValue", c => c.String(maxLength: 4000));
            this.AlterColumn("dbo.EcommerceVariants", "DefaultStringValue", c => c.String(maxLength: 500));
            this.AlterColumn("dbo.EcommerceProductVariants", "DefaultStringValue", c => c.String(maxLength: 500));
            this.AlterColumn("dbo.EcommerceCartItemVariants", "StringValue", c => c.String(maxLength: 500));
            this.AlterColumn("dbo.EcommerceOrderItemVariants", "StringValue", c => c.String(maxLength: 500));
        }

        public override void Down()
        {
            this.AlterColumn("dbo.EcommerceOrderItemVariants", "StringValue", c => c.String());
            this.AlterColumn("dbo.EcommerceCartItemVariants", "StringValue", c => c.String());
            this.AlterColumn("dbo.EcommerceProductVariants", "DefaultStringValue", c => c.String());
            this.AlterColumn("dbo.EcommerceVariants", "DefaultStringValue", c => c.String());
            this.DropColumn("dbo.EcommerceOrderItemVariants", "JsonValue");
            this.DropColumn("dbo.EcommerceCartItemVariants", "JsonValue");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultJsonValue");
            this.DropColumn("dbo.EcommerceVariants", "DefaultJsonValue");
        }
    }
}
