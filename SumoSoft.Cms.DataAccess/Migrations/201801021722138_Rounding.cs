namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Rounding : DbMigration
    {
        public override void Up()
        {
            this.DropColumn("dbo.Countries", "AfterTaxRoundingDigits");
        }

        public override void Down()
        {
            this.AddColumn("dbo.Countries", "AfterTaxRoundingDigits", c => c.Int(nullable: false));
        }
    }
}
