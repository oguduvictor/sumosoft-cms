namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DeleteEcommerceProductTag : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceProductTags", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceProductTags", "Parent_Id", "dbo.EcommerceProductTags");
            this.DropForeignKey("dbo.EcommerceProductTags", "Product_Id", "dbo.EcommerceProducts");
            this.DropIndex("dbo.EcommerceProductTags", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceProductTags", new[] { "Parent_Id" });
            this.DropIndex("dbo.EcommerceProductTags", new[] { "Product_Id" });
            this.DropTable("dbo.EcommerceProductTags");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceProductTags",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Parent_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateIndex("dbo.EcommerceProductTags", "Product_Id");
            this.CreateIndex("dbo.EcommerceProductTags", "Parent_Id");
            this.CreateIndex("dbo.EcommerceProductTags", "Country_Id");
            this.AddForeignKey("dbo.EcommerceProductTags", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceProductTags", "Parent_Id", "dbo.EcommerceProductTags", "Id");
            this.AddForeignKey("dbo.EcommerceProductTags", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
