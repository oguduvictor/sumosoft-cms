namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_209 : DbMigration
    {
        public override void Up()
        {
            this.Sql("WITH EcommerceProductsUrlDups AS (SELECT *, ROW_NUMBER() OVER(PARTITION BY[Url] ORDER BY[Id]) AS RowNumber FROM [dbo].[EcommerceProducts]) UPDATE EcommerceProductsUrlDups SET[Url] = [Url] + '-' + CAST((RowNumber - 1) AS VARCHAR(255)) WHERE RowNumber > 1;");

            this.AlterColumn("dbo.EcommerceProducts", "Url", c => c.String(maxLength: 250));

            this.CreateIndex("dbo.EcommerceProducts", "Url", true, "IX_EcommerceProducts_Url");
        }

        public override void Down()
        {
            this.DropIndex("dbo.EcommerceProducts", "IX_EcommerceProducts_Url");

            this.AlterColumn("dbo.EcommerceProducts", "Url", c => c.String());
        }
    }
}
