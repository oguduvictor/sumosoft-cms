namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceProductStockUnit_EnablePreorder : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"ALTER TABLE [dbo].[EcommerceOrders] Drop Constraint [FK_dbo.EcommerceOrders_dbo.Coupons_Coupon_Id];");

            this.DropForeignKey("dbo.EcommerceOrders", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropIndex("dbo.EcommerceOrders", new[] { "Coupon_Id" });
            this.AddColumn("dbo.EcommerceProductStockUnits", "EnablePreorder", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductStockUnits", "ShipsOn", c => c.DateTime());
            this.DropColumn("dbo.EcommerceProducts", "EnablePreorder");
            this.DropColumn("dbo.EcommerceProducts", "ShipsOn");
            this.DropColumn("dbo.EcommerceProducts", "GlobalStock");
            this.DropColumn("dbo.EcommerceProductAttributes", "TextAreaValue");
            this.DropColumn("dbo.EcommerceProductLocalizedKits", "Stock");
            this.DropColumn("dbo.EcommerceOrderItemVariants", "VariantPriceModifier");
            this.DropColumn("dbo.EcommerceOrders", "Coupon_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrders", "Coupon_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderItemVariants", "VariantPriceModifier", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProductLocalizedKits", "Stock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductAttributes", "TextAreaValue", c => c.String());
            this.AddColumn("dbo.EcommerceProducts", "GlobalStock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "ShipsOn", c => c.DateTime());
            this.AddColumn("dbo.EcommerceProducts", "EnablePreorder", c => c.Boolean(nullable: false));
            this.DropColumn("dbo.EcommerceProductStockUnits", "ShipsOn");
            this.DropColumn("dbo.EcommerceProductStockUnits", "EnablePreorder");
            this.CreateIndex("dbo.EcommerceOrders", "Coupon_Id");
            this.AddForeignKey("dbo.EcommerceOrders", "Coupon_Id", "dbo.EcommerceCoupons", "Id");
        }
    }
}
