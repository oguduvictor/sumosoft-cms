namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_215 : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceShippingBoxStatus",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.String(),
                    SortOrder = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    ShippingBox_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceShippingBoxes", t => t.ShippingBox_Id)
                .Index(t => t.ShippingBox_Id);

            this.CreateTable(
                "dbo.EcommerceShippingBoxStatusLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    ShippingBoxStatus_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceShippingBoxStatus", t => t.ShippingBoxStatus_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.ShippingBoxStatus_Id);

            this.AddColumn("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id");
            this.CreateIndex("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id");
            this.AddForeignKey("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes", "Id");
            this.AddForeignKey("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes");
            this.DropForeignKey("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id", "dbo.EcommerceOrderShippingBoxes");
            this.DropForeignKey("dbo.EcommerceShippingBoxStatusLocalizedKits", "ShippingBoxStatus_Id", "dbo.EcommerceShippingBoxStatus");
            this.DropForeignKey("dbo.EcommerceShippingBoxStatusLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceShippingBoxStatus", "ShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropIndex("dbo.EcommerceOrderShippingBoxStatus", new[] { "EcommerceOrderShippingBox_Id" });
            this.DropIndex("dbo.EcommerceOrderShippingBoxStatus", new[] { "OrderShippingBox_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxStatusLocalizedKits", new[] { "ShippingBoxStatus_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxStatusLocalizedKits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxStatus", new[] { "ShippingBox_Id" });
            this.DropColumn("dbo.EcommerceOrderShippingBoxStatus", "EcommerceOrderShippingBox_Id");
            this.DropColumn("dbo.EcommerceOrderShippingBoxStatus", "OrderShippingBox_Id");
            this.DropTable("dbo.EcommerceShippingBoxStatusLocalizedKits");
            this.DropTable("dbo.EcommerceShippingBoxStatus");
        }
    }
}
