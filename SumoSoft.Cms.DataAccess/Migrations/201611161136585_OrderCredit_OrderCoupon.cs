namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class OrderCredit_OrderCoupon : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceOrderCoupons",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Code = c.String(),
                    Amount = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOrderCredits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Amount = c.Double(nullable: false),
                    Reference = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceCarts", "UserCredit_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCartItems", "Coupon_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCartItems", "UserCredit_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderItems", "OrderCoupon_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrderItems", "OrderCredit_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrders", "OrderCoupon_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrders", "OrderCredit_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCarts", "UserCredit_Id");
            this.CreateIndex("dbo.EcommerceCartItems", "Coupon_Id");
            this.CreateIndex("dbo.EcommerceCartItems", "UserCredit_Id");
            this.CreateIndex("dbo.EcommerceOrderItems", "OrderCoupon_Id");
            this.CreateIndex("dbo.EcommerceOrderItems", "OrderCredit_Id");
            this.CreateIndex("dbo.EcommerceOrders", "OrderCoupon_Id");
            this.CreateIndex("dbo.EcommerceOrders", "OrderCredit_Id");
            this.AddForeignKey("dbo.EcommerceCartItems", "Coupon_Id", "dbo.EcommerceCoupons", "Id");
            this.AddForeignKey("dbo.EcommerceCartItems", "UserCredit_Id", "dbo.EcommerceUserCredits", "Id");
            this.AddForeignKey("dbo.EcommerceCarts", "UserCredit_Id", "dbo.EcommerceUserCredits", "Id");
            this.AddForeignKey("dbo.EcommerceOrderItems", "OrderCoupon_Id", "dbo.EcommerceOrderCoupons", "Id");
            this.AddForeignKey("dbo.EcommerceOrderItems", "OrderCredit_Id", "dbo.EcommerceOrderCredits", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "OrderCoupon_Id", "dbo.EcommerceOrderCoupons", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "OrderCredit_Id", "dbo.EcommerceOrderCredits", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceOrders", "OrderCredit_Id", "dbo.EcommerceOrderCredits");
            this.DropForeignKey("dbo.EcommerceOrders", "OrderCoupon_Id", "dbo.EcommerceOrderCoupons");
            this.DropForeignKey("dbo.EcommerceOrderItems", "OrderCredit_Id", "dbo.EcommerceOrderCredits");
            this.DropForeignKey("dbo.EcommerceOrderItems", "OrderCoupon_Id", "dbo.EcommerceOrderCoupons");
            this.DropForeignKey("dbo.EcommerceCarts", "UserCredit_Id", "dbo.EcommerceUserCredits");
            this.DropForeignKey("dbo.EcommerceCartItems", "UserCredit_Id", "dbo.EcommerceUserCredits");
            this.DropForeignKey("dbo.EcommerceCartItems", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropIndex("dbo.EcommerceOrders", new[] { "OrderCredit_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "OrderCoupon_Id" });
            this.DropIndex("dbo.EcommerceOrderItems", new[] { "OrderCredit_Id" });
            this.DropIndex("dbo.EcommerceOrderItems", new[] { "OrderCoupon_Id" });
            this.DropIndex("dbo.EcommerceCartItems", new[] { "UserCredit_Id" });
            this.DropIndex("dbo.EcommerceCartItems", new[] { "Coupon_Id" });
            this.DropIndex("dbo.EcommerceCarts", new[] { "UserCredit_Id" });
            this.DropColumn("dbo.EcommerceOrders", "OrderCredit_Id");
            this.DropColumn("dbo.EcommerceOrders", "OrderCoupon_Id");
            this.DropColumn("dbo.EcommerceOrderItems", "OrderCredit_Id");
            this.DropColumn("dbo.EcommerceOrderItems", "OrderCoupon_Id");
            this.DropColumn("dbo.EcommerceCartItems", "UserCredit_Id");
            this.DropColumn("dbo.EcommerceCartItems", "Coupon_Id");
            this.DropColumn("dbo.EcommerceCarts", "UserCredit_Id");
            this.DropTable("dbo.EcommerceOrderCredits");
            this.DropTable("dbo.EcommerceOrderCoupons");
        }
    }
}
