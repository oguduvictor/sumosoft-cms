namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_217 : DbMigration
    {
        public override void Up()
        {
            this.AlterColumn("dbo.BlogPosts", "UrlToken", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.Countries", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.Countries", "LanguageCode", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceProducts", "UrlToken", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceCategories", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceAttributes", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceAttributeOptions", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceVariantOptions", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceVariants", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceShippingBoxes", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceCoupons", "Code", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.UserRoles", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.EcommerceOrderShippingBoxes", "Name", c => c.String(maxLength: 250));
            this.AlterColumn("dbo.Emails", "Name", c => c.String(maxLength: 250));

            this.CreateIndex("dbo.Countries", "Name", name: "IX_Country_Name");
            this.CreateIndex("dbo.Countries", "LanguageCode", name: "IX_Country_LanguageCode");
            this.CreateIndex("dbo.EcommerceProducts", "UrlToken", name: "IX_Product_UrlToken");
            this.CreateIndex("dbo.EcommerceCategories", "Name", name: "IX_Category_Name");
            this.CreateIndex("dbo.EcommerceAttributes", "Name", name: "IX_Attribute_Name");
            this.CreateIndex("dbo.EcommerceAttributeOptions", "Name", name: "IX_AttributeOption_Name");
            this.CreateIndex("dbo.EcommerceVariantOptions", "Name", name: "IX_VariantOption_Name");
            this.CreateIndex("dbo.EcommerceVariants", "Name", name: "IX_Variant_Name");
            this.CreateIndex("dbo.EcommerceShippingBoxes", "Name", name: "IX_ShippingBox_Name");
            this.CreateIndex("dbo.EcommerceCoupons", "Code", name: "IX_Coupon_Code");
            this.CreateIndex("dbo.UserRoles", "Name", name: "IX_UserRole_Name");
            this.CreateIndex("dbo.BlogPosts", "UrlToken", name: "IX_BlogPost_UrlToken");
            this.CreateIndex("dbo.EcommerceOrderShippingBoxes", "Name", name: "IX_OrderShippingBox_Name");
            this.CreateIndex("dbo.Emails", "Name", name: "IX_Email_Name");

            this.DropIndex("dbo.EcommerceProducts", "IX_EcommerceProducts_Url");
        }

        public override void Down()
        {
            this.DropIndex("dbo.Emails", "IX_Email_Name");
            this.DropIndex("dbo.EcommerceOrderShippingBoxes", "IX_OrderShippingBox_Name");
            this.DropIndex("dbo.BlogPosts", "IX_BlogPost_UrlToken");
            this.DropIndex("dbo.UserRoles", "IX_UserRole_Name");
            this.DropIndex("dbo.EcommerceCoupons", "IX_Coupon_Code");
            this.DropIndex("dbo.EcommerceShippingBoxes", "IX_ShippingBox_Name");
            this.DropIndex("dbo.EcommerceVariants", "IX_Variant_Name");
            this.DropIndex("dbo.EcommerceVariantOptions", "IX_VariantOption_Name");
            this.DropIndex("dbo.EcommerceAttributeOptions", "IX_AttributeOption_Name");
            this.DropIndex("dbo.EcommerceAttributes", "IX_Attribute_Name");
            this.DropIndex("dbo.EcommerceCategories", "IX_Category_Name");
            this.DropIndex("dbo.EcommerceProducts", "IX_Product_UrlToken");
            this.DropIndex("dbo.Countries", "IX_Country_LanguageCode");
            this.DropIndex("dbo.Countries", "IX_Country_Name");

            this.AlterColumn("dbo.Emails", "Name", c => c.String());
            this.AlterColumn("dbo.EcommerceOrderShippingBoxes", "Name", c => c.String());
            this.AlterColumn("dbo.UserRoles", "Name", c => c.String());
            this.AlterColumn("dbo.EcommerceCoupons", "Code", c => c.String());
            this.AlterColumn("dbo.EcommerceShippingBoxes", "Name", c => c.String());
            this.AlterColumn("dbo.EcommerceVariants", "Name", c => c.String());
            this.AlterColumn("dbo.EcommerceVariantOptions", "Name", c => c.String());
            this.AlterColumn("dbo.EcommerceAttributeOptions", "Name", c => c.String());
            this.AlterColumn("dbo.EcommerceAttributes", "Name", c => c.String());
            this.AlterColumn("dbo.EcommerceCategories", "Name", c => c.String());
            this.AlterColumn("dbo.Countries", "LanguageCode", c => c.String());
            this.AlterColumn("dbo.Countries", "Name", c => c.String());
            this.AlterColumn("dbo.BlogPosts", "UrlToken", c => c.String());

            this.CreateIndex("dbo.EcommerceProducts", "UrlToken", name: "IX_EcommerceProducts_Url");
        }
    }
}
