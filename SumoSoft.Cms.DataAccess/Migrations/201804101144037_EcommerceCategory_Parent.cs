namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceCategory_Parent : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategories", "Parent_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCategories", "Parent_Id");
            this.AddForeignKey("dbo.EcommerceCategories", "Parent_Id", "dbo.EcommerceCategories", "Id");

            this.Sql(@"update EcommerceCategories set Parent_Id = (Select EcommerceCategory_Id from EcommerceCategoryEcommerceCategories where Id = EcommerceCategory_Id1)");

            this.DropForeignKey("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id1", "dbo.EcommerceCategories");
            this.DropIndex("dbo.EcommerceCategoryEcommerceCategories", new[] { "EcommerceCategory_Id" });
            this.DropIndex("dbo.EcommerceCategoryEcommerceCategories", new[] { "EcommerceCategory_Id1" });
            this.DropTable("dbo.EcommerceCategoryEcommerceCategories");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceCategoryEcommerceCategories",
                c => new
                {
                    EcommerceCategory_Id = c.Guid(nullable: false),
                    EcommerceCategory_Id1 = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceCategory_Id, t.EcommerceCategory_Id1 });

            this.DropForeignKey("dbo.EcommerceCategories", "Parent_Id", "dbo.EcommerceCategories");
            this.DropIndex("dbo.EcommerceCategories", new[] { "Parent_Id" });
            this.DropColumn("dbo.EcommerceCategories", "Parent_Id");
            this.CreateIndex("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id1");
            this.CreateIndex("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id");
            this.AddForeignKey("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id1", "dbo.EcommerceCategories", "Id");
            this.AddForeignKey("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id", "dbo.EcommerceCategories", "Id");
        }
    }
}
