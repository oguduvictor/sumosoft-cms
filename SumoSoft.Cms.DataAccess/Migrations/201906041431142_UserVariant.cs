namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class UserVariant : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.UserVariants",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    BooleanValue = c.Boolean(nullable: false),
                    DoubleValue = c.Double(nullable: false),
                    IntegerValue = c.Int(nullable: false),
                    StringValue = c.String(maxLength: 500),
                    JsonValue = c.String(maxLength: 4000),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(nullable: false),
                    User_Id = c.Guid(),
                    Variant_Id = c.Guid(),
                    VariantOptionValue_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .ForeignKey("dbo.EcommerceVariants", t => t.Variant_Id)
                .ForeignKey("dbo.EcommerceVariantOptions", t => t.VariantOptionValue_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Variant_Id)
                .Index(t => t.VariantOptionValue_Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.UserVariants", "VariantOptionValue_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.UserVariants", "Variant_Id", "dbo.EcommerceVariants");
            this.DropForeignKey("dbo.UserVariants", "User_Id", "dbo.Users");
            this.DropIndex("dbo.UserVariants", new[] { "VariantOptionValue_Id" });
            this.DropIndex("dbo.UserVariants", new[] { "Variant_Id" });
            this.DropIndex("dbo.UserVariants", new[] { "User_Id" });
            this.DropTable("dbo.UserVariants");
        }
    }
}
