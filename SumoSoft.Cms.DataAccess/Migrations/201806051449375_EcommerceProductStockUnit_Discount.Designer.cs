// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class EcommerceProductStockUnit_Discount : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EcommerceProductStockUnit_Discount));
        
        string IMigrationMetadata.Id
        {
            get { return "201806051449375_EcommerceProductStockUnit_Discount"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
