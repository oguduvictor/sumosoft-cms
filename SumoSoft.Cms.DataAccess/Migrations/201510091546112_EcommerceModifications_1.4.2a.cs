namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_142a : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceProductStocks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Stock = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Product_Id);

            this.AddColumn("dbo.EcommerceVariantOptions", "SortOrder", c => c.Int(nullable: false));

            this.RenameColumn("dbo.Addresses", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.Countries", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.BlogCategories", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.BlogPosts", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.Users", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceShoppingCarts", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceCoupons", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceShoppingCartLines", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProducts", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProductAttributes", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceAttributes", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceAttributeGroups", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceAttributeLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceAttributeOptions", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceAttributeOptionLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceCategories", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceCategoryLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProductLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProductPrices", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProductTags", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProductVariants", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceVariants", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceVariantLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceVariantOptions", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceVariantOptionLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceVariantOptionPriceModifiers", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceVariantPriceModifiers", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProductVariantOptions", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceOrders", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceOrderLines", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.UserRoles", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.UserPermissions", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.BlogComments", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.BlogTags", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.CmsSettings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.ContentSectionLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.ContentSections", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.EcommerceProductRelatedProducts", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.SeoSectionLocalizedStrings", "IsDeleted", "IsDisabled");
            this.RenameColumn("dbo.SeoSections", "IsDeleted", "IsDisabled");

            this.DropColumn("dbo.EcommerceProductLocalizedStrings", "Stock");
        }

        public override void Down()
        {
            this.RenameColumn("dbo.Addresses", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.Countries", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.BlogCategories", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.BlogPosts", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.Users", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceShoppingCarts", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceCoupons", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceShoppingCartLines", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProducts", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProductAttributes", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceAttributes", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceAttributeGroups", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceAttributeLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceAttributeOptions", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceAttributeOptionLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceCategories", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceCategoryLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProductLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProductPrices", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProductTags", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProductVariants", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceVariants", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceVariantLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceVariantOptions", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceVariantOptionLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceVariantOptionPriceModifiers", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceVariantPriceModifiers", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProductVariantOptions", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceOrders", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceOrderLines", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.UserRoles", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.UserPermissions", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.BlogComments", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.BlogTags", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.CmsSettings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.ContentSectionLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.ContentSections", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.EcommerceProductRelatedProducts", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.SeoSectionLocalizedStrings", "IsDisabled", "IsDeleted");
            this.RenameColumn("dbo.SeoSections", "IsDisabled", "IsDeleted");

            this.AddColumn("dbo.EcommerceProductLocalizedStrings", "Stock", c => c.Int(nullable: false));
            this.DropForeignKey("dbo.EcommerceProductStocks", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductStocks", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceProductStocks", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductStocks", new[] { "Country_Id" });

            this.DropColumn("dbo.EcommerceVariantOptions", "SortOrder");
            this.DropTable("dbo.EcommerceProductStocks");
        }
    }
}
