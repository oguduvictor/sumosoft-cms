// <auto-generated />
namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Obsolete_EcommerceOrderStatus_EcommerceOrderShippingBoxStatus : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Obsolete_EcommerceOrderStatus_EcommerceOrderShippingBoxStatus));
        
        string IMigrationMetadata.Id
        {
            get { return "201801311348150_Obsolete_EcommerceOrderStatus_EcommerceOrderShippingBoxStatus"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
