namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceProductEcommerceCategory : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceProductEcommerceCategories", newName: "EcommerceCategoryEcommerceProducts");
            this.DropPrimaryKey("dbo.EcommerceCategoryEcommerceProducts");
            this.AddPrimaryKey("dbo.EcommerceCategoryEcommerceProducts", new[] { "EcommerceCategory_Id", "EcommerceProduct_Id" });
        }

        public override void Down()
        {
            this.DropPrimaryKey("dbo.EcommerceCategoryEcommerceProducts");
            this.AddPrimaryKey("dbo.EcommerceCategoryEcommerceProducts", new[] { "EcommerceProduct_Id", "EcommerceCategory_Id" });
            this.RenameTable(name: "dbo.EcommerceCategoryEcommerceProducts", newName: "EcommerceProductEcommerceCategories");
        }
    }
}
