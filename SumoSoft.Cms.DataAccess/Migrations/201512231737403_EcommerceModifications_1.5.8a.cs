namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_158a : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrders", "OrderStatus_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceOrders", "OrderStatus_Id");
            this.AddForeignKey("dbo.EcommerceOrders", "OrderStatus_Id", "dbo.EcommerceOrderStatus", "Id");
            this.DropColumn("dbo.EcommerceOrders", "Status");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceOrders", "Status", c => c.Int(nullable: false));
            this.DropForeignKey("dbo.EcommerceOrders", "OrderStatus_Id", "dbo.EcommerceOrderStatus");
            this.DropIndex("dbo.EcommerceOrders", new[] { "OrderStatus_Id" });
            this.DropColumn("dbo.EcommerceOrders", "OrderStatus_Id");
        }
    }
}
