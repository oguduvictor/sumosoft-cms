namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_216 : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn(table: "dbo.EcommerceProducts", name: "Url", newName: "UrlToken");
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "VariantOptionLocalizedText", newName: "SelectValueVariantOptionText");
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "VariantOptionSelectValue", newName: "SelectValueVatiantOptionName");
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "VariantOptionPriceModifier", newName: "SelectValueVatiantOptionPriceModifier");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductUrl", newName: "ProductUrlToken");
            this.RenameColumn(table: "dbo.BlogPosts", name: "Url", newName: "UrlToken");
        }

        public override void Down()
        {
            this.RenameColumn(table: "dbo.EcommerceProducts", name: "UrlToken", newName: "Url");
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "SelectValueVariantOptionText", newName: "VariantOptionLocalizedText");
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "SelectValueVatiantOptionName", newName: "VariantOptionSelectValue");
            this.RenameColumn(table: "dbo.EcommerceOrderItemProductVariants", name: "SelectValueVatiantOptionPriceModifier", newName: "VariantOptionPriceModifier");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductUrlToken", newName: "ProductUrl");
            this.RenameColumn(table: "dbo.BlogPosts", name: "UrlToken", newName: "Url");
        }
    }
}
