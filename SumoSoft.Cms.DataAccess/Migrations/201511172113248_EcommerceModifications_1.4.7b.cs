namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_147b : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultCheckboxValue", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultDoubleValue", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultIntValue", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "DefaultSelectValue_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceProductVariants", "DefaultSelectValue_Id");
            this.AddForeignKey("dbo.EcommerceProductVariants", "DefaultSelectValue_Id", "dbo.EcommerceVariantOptions", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceProductVariants", "DefaultSelectValue_Id", "dbo.EcommerceVariantOptions");
            this.DropIndex("dbo.EcommerceProductVariants", new[] { "DefaultSelectValue_Id" });
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultSelectValue_Id");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultIntValue");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultDoubleValue");
            this.DropColumn("dbo.EcommerceProductVariants", "DefaultCheckboxValue");
        }
    }
}
