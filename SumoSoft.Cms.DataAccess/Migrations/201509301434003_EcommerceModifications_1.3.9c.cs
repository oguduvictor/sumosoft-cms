namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_139c : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceVariantTypeOptions", newName: "EcommerceVariantOptions");
            this.RenameTable(name: "dbo.EcommerceVariantTypeOptionLocalizedStrings", newName: "EcommerceVariantOptionLocalizedStrings");
            this.RenameColumn(table: "dbo.EcommerceVariantOptionLocalizedStrings", name: "EcommerceVariantTypeOption_Id", newName: "EcommerceVariantOption_Id");
            this.RenameIndex(table: "dbo.EcommerceVariantOptionLocalizedStrings", name: "IX_EcommerceVariantTypeOption_Id", newName: "IX_EcommerceVariantOption_Id");
        }

        public override void Down()
        {
            this.RenameIndex(table: "dbo.EcommerceVariantOptionLocalizedStrings", name: "IX_EcommerceVariantOption_Id", newName: "IX_EcommerceVariantTypeOption_Id");
            this.RenameColumn(table: "dbo.EcommerceVariantOptionLocalizedStrings", name: "EcommerceVariantOption_Id", newName: "EcommerceVariantTypeOption_Id");
            this.RenameTable(name: "dbo.EcommerceVariantOptionLocalizedStrings", newName: "EcommerceVariantTypeOptionLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceVariantOptions", newName: "EcommerceVariantTypeOptions");
        }
    }
}
