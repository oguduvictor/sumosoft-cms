namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Ecommerce_Updates : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceProductImageKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceProductImages",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Url = c.String(),
                    AltText = c.String(),
                    SortOrder = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    ProductImageKit_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProductImageKits", t => t.ProductImageKit_Id)
                .Index(t => t.ProductImageKit_Id);

            this.CreateTable(
                "dbo.EcommerceProductImageKitEcommerceVariantOptions",
                c => new
                {
                    EcommerceProductImageKit_Id = c.Guid(nullable: false),
                    EcommerceVariantOption_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceProductImageKit_Id, t.EcommerceVariantOption_Id })
                .ForeignKey("dbo.EcommerceProductImageKits", t => t.EcommerceProductImageKit_Id)
                .ForeignKey("dbo.EcommerceVariantOptions", t => t.EcommerceVariantOption_Id)
                .Index(t => t.EcommerceProductImageKit_Id)
                .Index(t => t.EcommerceVariantOption_Id);

            this.CreateTable(
                "dbo.EcommerceProductVariantEcommerceVariantOptions",
                c => new
                {
                    EcommerceProductVariant_Id = c.Guid(nullable: false),
                    EcommerceVariantOption_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceProductVariant_Id, t.EcommerceVariantOption_Id })
                .ForeignKey("dbo.EcommerceProductVariants", t => t.EcommerceProductVariant_Id)
                .ForeignKey("dbo.EcommerceVariantOptions", t => t.EcommerceVariantOption_Id)
                .Index(t => t.EcommerceProductVariant_Id)
                .Index(t => t.EcommerceVariantOption_Id);

            this.AddColumn("dbo.EcommerceVariants", "CreateProductImageKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "EnablePreorder", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "ShipsOn", c => c.DateTime());
            this.AddColumn("dbo.EcommerceShippingBoxes", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitId", c => c.Guid(nullable: false));
            this.DropColumn("dbo.EcommerceCartItems", "OrderItemStockStatus");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceCartItems", "OrderItemStockStatus", c => c.Int(nullable: false));
            this.DropForeignKey("dbo.EcommerceProductVariantEcommerceVariantOptions", "EcommerceVariantOption_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceProductVariantEcommerceVariantOptions", "EcommerceProductVariant_Id", "dbo.EcommerceProductVariants");
            this.DropForeignKey("dbo.EcommerceProductImageKitEcommerceVariantOptions", "EcommerceVariantOption_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceProductImageKitEcommerceVariantOptions", "EcommerceProductImageKit_Id", "dbo.EcommerceProductImageKits");
            this.DropForeignKey("dbo.EcommerceProductImages", "ProductImageKit_Id", "dbo.EcommerceProductImageKits");
            this.DropForeignKey("dbo.EcommerceProductImageKits", "Product_Id", "dbo.EcommerceProducts");
            this.DropIndex("dbo.EcommerceProductVariantEcommerceVariantOptions", new[] { "EcommerceVariantOption_Id" });
            this.DropIndex("dbo.EcommerceProductVariantEcommerceVariantOptions", new[] { "EcommerceProductVariant_Id" });
            this.DropIndex("dbo.EcommerceProductImageKitEcommerceVariantOptions", new[] { "EcommerceVariantOption_Id" });
            this.DropIndex("dbo.EcommerceProductImageKitEcommerceVariantOptions", new[] { "EcommerceProductImageKit_Id" });
            this.DropIndex("dbo.EcommerceProductImages", new[] { "ProductImageKit_Id" });
            this.DropIndex("dbo.EcommerceProductImageKits", new[] { "Product_Id" });
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitId");
            this.DropColumn("dbo.EcommerceShippingBoxes", "SortOrder");
            this.DropColumn("dbo.EcommerceProducts", "ShipsOn");
            this.DropColumn("dbo.EcommerceProducts", "EnablePreorder");
            this.DropColumn("dbo.EcommerceVariants", "CreateProductImageKits");
            this.DropTable("dbo.EcommerceProductVariantEcommerceVariantOptions");
            this.DropTable("dbo.EcommerceProductImageKitEcommerceVariantOptions");
            this.DropTable("dbo.EcommerceProductImages");
            this.DropTable("dbo.EcommerceProductImageKits");
        }
    }
}
