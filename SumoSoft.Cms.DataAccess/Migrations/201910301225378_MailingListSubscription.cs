namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MailingListSubscription : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("MailingListUsers", "Status", c => c.Int(nullable: false));

            this.RenameTable("MailingListUsers", "MailingListSubscriptions");
        }

        public override void Down()
        {
            this.DropColumn("MailingListSubscriptions", "Status");

            this.RenameTable("MailingListSubscriptions", "MailingListUsers");
        }
    }
}
