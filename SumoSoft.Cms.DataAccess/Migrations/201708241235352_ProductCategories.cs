namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ProductCategories : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceProducts", "Category_Id", "dbo.EcommerceCategories");
            this.DropIndex("dbo.EcommerceProducts", new[] { "Category_Id" });
            this.CreateTable(
                "dbo.EcommerceProductEcommerceCategories",
                c => new
                {
                    EcommerceProduct_Id = c.Guid(nullable: false),
                    EcommerceCategory_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceProduct_Id, t.EcommerceCategory_Id })
                .ForeignKey("dbo.EcommerceProducts", t => t.EcommerceProduct_Id)
                .ForeignKey("dbo.EcommerceCategories", t => t.EcommerceCategory_Id)
                .Index(t => t.EcommerceProduct_Id)
                .Index(t => t.EcommerceCategory_Id);

            this.AddColumn("dbo.EcommerceCategories", "UrlToken", c => c.String(maxLength: 250));
            this.CreateIndex("dbo.EcommerceCategories", "UrlToken", name: "IX_Category_UrlToken");
            this.DropColumn("dbo.EcommerceProducts", "Category_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProducts", "Category_Id", c => c.Guid());
            this.DropForeignKey("dbo.EcommerceProductEcommerceCategories", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceProductEcommerceCategories", "EcommerceProduct_Id", "dbo.EcommerceProducts");
            this.DropIndex("dbo.EcommerceProductEcommerceCategories", new[] { "EcommerceCategory_Id" });
            this.DropIndex("dbo.EcommerceProductEcommerceCategories", new[] { "EcommerceProduct_Id" });
            this.DropIndex("dbo.EcommerceCategories", "IX_Category_UrlToken");
            this.DropColumn("dbo.EcommerceCategories", "UrlToken");
            this.DropTable("dbo.EcommerceProductEcommerceCategories");
            this.CreateIndex("dbo.EcommerceProducts", "Category_Id");
            this.AddForeignKey("dbo.EcommerceProducts", "Category_Id", "dbo.EcommerceCategories", "Id");
        }
    }
}
