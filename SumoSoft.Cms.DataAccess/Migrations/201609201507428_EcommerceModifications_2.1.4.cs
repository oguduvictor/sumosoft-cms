namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_214 : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceLocalizedCredits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceLocalizedCredits", "User_Id", "dbo.Users");
            this.DropIndex("dbo.EcommerceLocalizedCredits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceLocalizedCredits", new[] { "User_Id" });
            this.RenameColumn(table: "dbo.EcommerceUserCredits", name: "LocalizedKit_Id", newName: "UserLocalizedKit_Id");
            this.RenameIndex(table: "dbo.EcommerceUserCredits", name: "IX_LocalizedKit_Id", newName: "IX_UserLocalizedKit_Id");
            this.DropTable("dbo.EcommerceLocalizedCredits");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceLocalizedCredits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Amount = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.RenameIndex(table: "dbo.EcommerceUserCredits", name: "IX_UserLocalizedKit_Id", newName: "IX_LocalizedKit_Id");
            this.RenameColumn(table: "dbo.EcommerceUserCredits", name: "UserLocalizedKit_Id", newName: "LocalizedKit_Id");
            this.CreateIndex("dbo.EcommerceLocalizedCredits", "User_Id");
            this.CreateIndex("dbo.EcommerceLocalizedCredits", "Country_Id");
            this.AddForeignKey("dbo.EcommerceLocalizedCredits", "User_Id", "dbo.Users", "Id");
            this.AddForeignKey("dbo.EcommerceLocalizedCredits", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
