namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SaleAtPrice : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductLocalizedKits", "SaleAtPrice", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "ProductSaleAtPrice", c => c.Double(nullable: false));
            this.RenameColumn("dbo.EcommerceOrderItems", name: "ProductPrice", newName: "ProductBasePrice");
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "ProductSaleAtPrice");
            this.DropColumn("dbo.EcommerceProductLocalizedKits", "SaleAtPrice");
            this.RenameColumn("dbo.EcommerceOrderItems", name: "ProductPrice", newName: "ProductBasePrice");
        }
    }
}
