namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class UserRole_Coupon : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.UserRoles", "CreateCoupon", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditCoupon", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteCoupon", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.UserRoles", "DeleteCoupon");
            this.DropColumn("dbo.UserRoles", "EditCoupon");
            this.DropColumn("dbo.UserRoles", "CreateCoupon");
        }
    }
}
