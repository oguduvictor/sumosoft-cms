namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceAttributeOption_Tags : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceAttributeOptions", "Tags", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceAttributeOptions", "Tags");
        }
    }
}
