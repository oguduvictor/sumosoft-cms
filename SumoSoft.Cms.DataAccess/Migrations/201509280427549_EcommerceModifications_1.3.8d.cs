namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_138d : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn(table: "dbo.EcommerceVariantPriceModifiers", name: "EcommerceVariant_Id", newName: "Variant_Id");
            this.RenameIndex(table: "dbo.EcommerceVariantPriceModifiers", name: "IX_EcommerceVariant_Id", newName: "IX_Variant_Id");
            this.AddColumn("dbo.EcommerceProductVariants", "CheckboxValue", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "Stock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductVariants", "Country_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProductVariants", "SelectValue_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceProductVariants", "Country_Id");
            this.CreateIndex("dbo.EcommerceProductVariants", "SelectValue_Id");
            this.AddForeignKey("dbo.EcommerceProductVariants", "Country_Id", "dbo.Countries", "Id");
            this.AddForeignKey("dbo.EcommerceProductVariants", "SelectValue_Id", "dbo.EcommerceVariantTypeOptions", "Id");
            this.DropColumn("dbo.EcommerceVariants", "Stock");
            this.DropColumn("dbo.EcommerceVariantLocalizedStrings", "Stock");

            this.AlterColumn("dbo.EcommerceVariantPriceModifiers", "Id", c => c.Guid(nullable: false, defaultValueSql: "NEWID()"));
            this.AlterColumn("dbo.EcommerceShoppingCarts", "Id", c => c.Guid(nullable: false, defaultValueSql: "NEWID()"));
            this.AlterColumn("dbo.EcommerceShoppingCartLines", "Id", c => c.Guid(nullable: false, defaultValueSql: "NEWID()"));
            this.AlterColumn("dbo.EcommerceOrders", "Id", c => c.Guid(nullable: false, defaultValueSql: "NEWID()"));
            this.AlterColumn("dbo.EcommerceOrderLines", "Id", c => c.Guid(nullable: false, defaultValueSql: "NEWID()"));
            this.AlterColumn("dbo.EcommerceCoupons", "Id", c => c.Guid(nullable: false, defaultValueSql: "NEWID()"));

        }

        public override void Down()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceProductVariants_dbo.EcommerceVariantOptions_SelectValue_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceProductVariants] DROP [FK_dbo.EcommerceProductVariants_dbo.EcommerceVariantOptions_SelectValue_Id]");

            this.AddColumn("dbo.EcommerceVariantLocalizedStrings", "Stock", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceVariants", "Stock", c => c.Int(nullable: false));
            this.DropForeignKey("dbo.EcommerceProductVariants", "SelectValue_Id", "dbo.EcommerceVariantTypeOptions");
            this.DropForeignKey("dbo.EcommerceProductVariants", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.EcommerceProductVariants", new[] { "SelectValue_Id" });
            this.DropIndex("dbo.EcommerceProductVariants", new[] { "Country_Id" });
            this.DropColumn("dbo.EcommerceProductVariants", "SelectValue_Id");
            this.DropColumn("dbo.EcommerceProductVariants", "Country_Id");
            this.DropColumn("dbo.EcommerceProductVariants", "Stock");
            this.DropColumn("dbo.EcommerceProductVariants", "CheckboxValue");
            this.RenameIndex(table: "dbo.EcommerceVariantPriceModifiers", name: "IX_Variant_Id", newName: "IX_EcommerceVariant_Id");
            this.RenameColumn(table: "dbo.EcommerceVariantPriceModifiers", name: "Variant_Id", newName: "EcommerceVariant_Id");
        }
    }
}
