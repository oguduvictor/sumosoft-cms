namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddBlogCategory_1023 : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.BlogCategories", "BlogPost_Id", "dbo.BlogPosts");
            this.DropIndex("dbo.BlogCategories", new[] { "BlogPost_Id" });
            this.CreateTable(
                "dbo.BlogPostBlogCategories",
                c => new
                {
                    BlogPost_Id = c.Guid(nullable: false),
                    BlogCategory_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.BlogPost_Id, t.BlogCategory_Id })
                .ForeignKey("dbo.BlogPosts", t => t.BlogPost_Id, cascadeDelete: true)
                .ForeignKey("dbo.BlogCategories", t => t.BlogCategory_Id, cascadeDelete: true)
                .Index(t => t.BlogPost_Id)
                .Index(t => t.BlogCategory_Id);

            this.DropColumn("dbo.BlogCategories", "BlogPost_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.BlogCategories", "BlogPost_Id", c => c.Guid());
            this.DropForeignKey("dbo.BlogPostBlogCategories", "BlogCategory_Id", "dbo.BlogCategories");
            this.DropForeignKey("dbo.BlogPostBlogCategories", "BlogPost_Id", "dbo.BlogPosts");
            this.DropIndex("dbo.BlogPostBlogCategories", new[] { "BlogCategory_Id" });
            this.DropIndex("dbo.BlogPostBlogCategories", new[] { "BlogPost_Id" });
            this.DropTable("dbo.BlogPostBlogCategories");
            this.CreateIndex("dbo.BlogCategories", "BlogPost_Id");
            this.AddForeignKey("dbo.BlogCategories", "BlogPost_Id", "dbo.BlogPosts", "Id");
        }
    }
}
