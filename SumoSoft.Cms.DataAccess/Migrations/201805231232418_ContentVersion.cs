namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ContentVersion : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.ContentVersions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Content = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    ContentSectionLocalizedKit_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentSectionLocalizedKits", t => t.ContentSectionLocalizedKit_Id)
                .Index(t => t.ContentSectionLocalizedKit_Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.ContentVersions", "ContentSectionLocalizedKit_Id", "dbo.ContentSectionLocalizedKits");
            this.DropIndex("dbo.ContentVersions", new[] { "ContentSectionLocalizedKit_Id" });
            this.DropTable("dbo.ContentVersions");
        }
    }
}
