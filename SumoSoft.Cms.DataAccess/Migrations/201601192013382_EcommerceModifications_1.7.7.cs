namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_177 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCartItems", "ShippingPrice", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "ShippingPrice", c => c.Double(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "ShippingPrice");
            this.DropColumn("dbo.EcommerceCartItems", "ShippingPrice");
        }
    }
}
