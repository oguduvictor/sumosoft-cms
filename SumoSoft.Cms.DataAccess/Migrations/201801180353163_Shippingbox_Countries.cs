namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Shippingbox_Countries : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceProducts", "DefaultShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropIndex("dbo.EcommerceProducts", new[] { "DefaultShippingBox_Id" });
            this.CreateTable(
                "dbo.EcommerceShippingBoxCountries",
                c => new
                {
                    EcommerceShippingBox_Id = c.Guid(nullable: false),
                    Country_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceShippingBox_Id, t.Country_Id })
                .ForeignKey("dbo.EcommerceShippingBoxes", t => t.EcommerceShippingBox_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.EcommerceShippingBox_Id)
                .Index(t => t.Country_Id);

            this.DropColumn("dbo.EcommerceProducts", "DefaultShippingBox_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProducts", "DefaultShippingBox_Id", c => c.Guid());
            this.DropForeignKey("dbo.EcommerceShippingBoxCountries", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceShippingBoxCountries", "EcommerceShippingBox_Id", "dbo.EcommerceShippingBoxes");
            this.DropIndex("dbo.EcommerceShippingBoxCountries", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceShippingBoxCountries", new[] { "EcommerceShippingBox_Id" });
            this.DropTable("dbo.EcommerceShippingBoxCountries");
            this.CreateIndex("dbo.EcommerceProducts", "DefaultShippingBox_Id");
            this.AddForeignKey("dbo.EcommerceProducts", "DefaultShippingBox_Id", "dbo.EcommerceShippingBoxes", "Id");
        }
    }
}
