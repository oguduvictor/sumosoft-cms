namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_142d : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceCarts",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    ShippingAddress_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.ShippingAddress_Id)
                .Index(t => t.ShippingAddress_Id);

            this.CreateTable(
                "dbo.EcommerceCartItems",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Quantity = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Cart_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceCarts", t => t.Cart_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Cart_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.EcommerceCartItemProductVariants",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    CheckboxValue = c.Boolean(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    ProductVariant_Id = c.Guid(),
                    SelectValue_Id = c.Guid(),
                    EcommerceCartItem_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProductVariants", t => t.ProductVariant_Id)
                .ForeignKey("dbo.EcommerceVariantOptions", t => t.SelectValue_Id)
                .ForeignKey("dbo.EcommerceCartItems", t => t.EcommerceCartItem_Id)
                .Index(t => t.ProductVariant_Id)
                .Index(t => t.SelectValue_Id)
                .Index(t => t.EcommerceCartItem_Id);

            this.AddColumn("dbo.Users", "Cart_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOrders", "CartReference_Id", c => c.Guid());
            this.CreateIndex("dbo.Users", "Cart_Id");
            this.CreateIndex("dbo.EcommerceOrders", "CartReference_Id");
            this.AddForeignKey("dbo.Users", "Cart_Id", "dbo.EcommerceCarts", "Id");
            this.AddForeignKey("dbo.EcommerceOrders", "CartReference_Id", "dbo.EcommerceCarts", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceOrders", "CartReference_Id", "dbo.EcommerceCarts");
            this.DropForeignKey("dbo.Users", "Cart_Id", "dbo.EcommerceCarts");
            this.DropForeignKey("dbo.EcommerceCarts", "ShippingAddress_Id", "dbo.Addresses");
            this.DropForeignKey("dbo.EcommerceCartItems", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "EcommerceCartItem_Id", "dbo.EcommerceCartItems");
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", "dbo.EcommerceVariantOptions");
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "ProductVariant_Id", "dbo.EcommerceProductVariants");
            this.DropForeignKey("dbo.EcommerceCartItems", "Cart_Id", "dbo.EcommerceCarts");
            this.DropIndex("dbo.EcommerceOrders", new[] { "CartReference_Id" });
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "EcommerceCartItem_Id" });
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "SelectValue_Id" });
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "ProductVariant_Id" });
            this.DropIndex("dbo.EcommerceCartItems", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceCartItems", new[] { "Cart_Id" });
            this.DropIndex("dbo.EcommerceCarts", new[] { "ShippingAddress_Id" });
            this.DropIndex("dbo.Users", new[] { "Cart_Id" });
            this.DropColumn("dbo.EcommerceOrders", "CartReference_Id");
            this.DropColumn("dbo.Users", "Cart_Id");
            this.DropTable("dbo.EcommerceCartItemProductVariants");
            this.DropTable("dbo.EcommerceCartItems");
            this.DropTable("dbo.EcommerceCarts");
        }
    }
}
