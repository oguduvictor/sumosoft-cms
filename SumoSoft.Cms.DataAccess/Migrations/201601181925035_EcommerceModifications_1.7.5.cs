namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_175 : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.Coupons", newName: "EcommerceCoupons");
        }

        public override void Down()
        {
            this.RenameTable(name: "dbo.EcommerceCoupons", newName: "Coupons");
        }
    }
}
