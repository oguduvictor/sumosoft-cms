namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_202 : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn(table: "dbo.EcommerceCartItemProductVariants", name: "EcommerceCartItem_Id", newName: "CartItem_Id");
            this.RenameIndex(table: "dbo.EcommerceCartItemProductVariants", name: "IX_EcommerceCartItem_Id", newName: "IX_CartItem_Id");
            this.CreateTable(
                "dbo.EcommerceLocalizedReferralCredits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Credit = c.Double(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.User_Id);

            this.AddColumn("dbo.EcommerceCoupons", "RefereeReward", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceCoupons", "Referee_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCoupons", "Referee_Id");
            this.AddForeignKey("dbo.EcommerceCoupons", "Referee_Id", "dbo.Users", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceLocalizedReferralCredits", "User_Id", "dbo.Users");
            this.DropForeignKey("dbo.EcommerceLocalizedReferralCredits", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceCoupons", "Referee_Id", "dbo.Users");
            this.DropIndex("dbo.EcommerceLocalizedReferralCredits", new[] { "User_Id" });
            this.DropIndex("dbo.EcommerceLocalizedReferralCredits", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceCoupons", new[] { "Referee_Id" });
            this.DropColumn("dbo.EcommerceCoupons", "Referee_Id");
            this.DropColumn("dbo.EcommerceCoupons", "RefereeReward");
            this.DropTable("dbo.EcommerceLocalizedReferralCredits");
            this.RenameIndex(table: "dbo.EcommerceCartItemProductVariants", name: "IX_CartItem_Id", newName: "IX_EcommerceCartItem_Id");
            this.RenameColumn(table: "dbo.EcommerceCartItemProductVariants", name: "CartItem_Id", newName: "EcommerceCartItem_Id");
        }
    }
}
