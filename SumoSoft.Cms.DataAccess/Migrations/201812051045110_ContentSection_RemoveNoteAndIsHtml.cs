namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ContentSection_RemoveNoteAndIsHtml : DbMigration
    {
        public override void Up()
        {
            this.DropColumn("dbo.ContentSections", "Note");
            this.DropColumn("dbo.ContentSections", "IsHtml");
        }

        public override void Down()
        {
            this.AddColumn("dbo.ContentSections", "IsHtml", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.ContentSections", "Note", c => c.String());
        }
    }
}
