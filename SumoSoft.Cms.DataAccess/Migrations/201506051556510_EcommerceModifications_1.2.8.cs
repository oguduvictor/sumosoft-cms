namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_128 : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceAttributeDescriptions", newName: "EcommerceAttributeLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceProductDescriptions", newName: "EcommerceProductLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceCountryPrices", newName: "EcommerceProductPrices");
            this.RenameTable(name: "dbo.EcommerceProductEcommerceCategories", newName: "EcommerceCategoryEcommerceProducts");
            this.RenameTable(name: "dbo.EcommerceTags", newName: "EcommerceProductTags");
            this.RenameTable(name: "dbo.EcommerceAttributeGroupDescriptions", newName: "EcommerceAttributeGroupLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceCategoryDescriptions", newName: "EcommerceCategoryLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceLengthClasses", newName: "EcommerceOptionGroupLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceWeightClasses", newName: "EcommerceOptionLocalizedStrings");
            this.DropForeignKey("dbo.EcommerceProducts", "LengthClass_Id", "dbo.EcommerceLengthClasses");
            this.DropForeignKey("dbo.EcommerceProducts", "WeightClass_Id", "dbo.EcommerceWeightClasses");
            this.DropForeignKey("dbo.EcommerceOptionValues", "Option_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceOptionValueDescriptions", "OptionValue_Id", "dbo.EcommerceOptionValues");
            this.DropForeignKey("dbo.EcommerceProductGroups", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductGroups", "ProductSameGroup_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceProductOptionValues", "Option_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceProductOptionValues", "Product_Id", "dbo.EcommerceProducts");
            this.DropIndex("dbo.EcommerceProducts", new[] { "LengthClass_Id" });
            this.DropIndex("dbo.EcommerceProducts", new[] { "WeightClass_Id" });
            this.DropIndex("dbo.EcommerceOptionDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceOptionDescriptions", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceOptionValueDescriptions", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceOptionValueDescriptions", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceOptionValueDescriptions", new[] { "OptionValue_Id" });
            this.DropIndex("dbo.EcommerceOptionValues", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceProductGroups", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceProductGroups", new[] { "ProductSameGroup_Id" });
            this.DropIndex("dbo.EcommerceProductOptionValues", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceProductOptionValues", new[] { "Product_Id" });
            this.RenameColumn(table: "dbo.EcommerceAttributeGroupLocalizedStrings", name: "AttributeGroup_Id", newName: "Group_Id");
            this.RenameColumn(table: "dbo.EcommerceAttributes", name: "AttributeGroup_Id", newName: "Group_Id");
            this.RenameColumn(table: "dbo.Countries", name: "CultureInfoName", newName: "LanguageCode");
            this.RenameIndex(table: "dbo.EcommerceAttributes", name: "IX_AttributeGroup_Id", newName: "IX_Group_Id");
            this.RenameIndex(table: "dbo.EcommerceAttributeGroupLocalizedStrings", name: "IX_AttributeGroup_Id", newName: "IX_Group_Id");
            this.DropPrimaryKey("dbo.EcommerceCategoryEcommerceProducts");
            this.CreateTable(
                "dbo.EcommerceShoppingCarts",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    SessionId = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Coupon_Id = c.Guid(),
                    ShippingAddress_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceCoupons", t => t.Coupon_Id)
                .ForeignKey("dbo.Addresses", t => t.ShippingAddress_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.Coupon_Id)
                .Index(t => t.ShippingAddress_Id);

            this.CreateTable(
                "dbo.EcommerceCoupons",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    DiscountAmount = c.Int(nullable: false),
                    Type = c.Int(nullable: false),
                    Code = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceShoppingCartLines",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ProductQuantity = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Product_Id = c.Guid(),
                    ShoppingCart_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .ForeignKey("dbo.EcommerceShoppingCarts", t => t.ShoppingCart_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.ShoppingCart_Id);

            this.CreateTable(
                "dbo.EcommerceOptionGroups",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOptionPriceModifiers",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    PriceModifier = c.Double(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    EcommerceOption_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.EcommerceOptions", t => t.EcommerceOption_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.EcommerceOption_Id);

            this.CreateTable(
                "dbo.EcommerceOrders",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    OrderNumber = c.Int(),
                    Status = c.Int(nullable: false),
                    FirstName = c.String(),
                    LastName = c.String(),
                    Email = c.String(),
                    AddressFirstName = c.String(),
                    AddressLastName = c.String(),
                    AddressLine1 = c.String(),
                    AddressLine2 = c.String(),
                    CountryName = c.String(),
                    City = c.String(),
                    Postcode = c.String(),
                    PhoneNumber = c.String(),
                    PayPalTransactionId = c.String(),
                    PayPalReceiptId = c.String(),
                    PayPalPaymentStatus = c.Int(),
                    PayPalPaymentStatusDate = c.DateTime(),
                    Comments = c.String(),
                    Vat = c.Single(nullable: false),
                    TrackingCode = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Coupon_Id = c.Guid(),
                    ShoppingCartReference_Id = c.Guid(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceCoupons", t => t.Coupon_Id)
                .ForeignKey("dbo.EcommerceShoppingCarts", t => t.ShoppingCartReference_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Coupon_Id)
                .Index(t => t.ShoppingCartReference_Id)
                .Index(t => t.User_Id);

            this.CreateTable(
                "dbo.EcommerceOrderLines",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ProductQuantity = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Order_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EcommerceOrders", t => t.Order_Id)
                .ForeignKey("dbo.EcommerceProducts", t => t.Product_Id)
                .Index(t => t.Order_Id)
                .Index(t => t.Product_Id);

            this.CreateTable(
                "dbo.ContentSectionLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Content = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    ContentSection_Id = c.Guid(),
                    Country_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentSections", t => t.ContentSection_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.ContentSection_Id)
                .Index(t => t.Country_Id);



            this.Sql(@"UPDATE ContentSections
                SET CreatedDate = GETDATE(), ModificationDate = GETDATE()

                INSERT INTO ContentSectionLocalizedStrings (Id,Content,CreatedDate,IsDeleted,ContentSection_Id,Country_Id)
                SELECT NEWID(), cs.Content, cs.CreatedDate, 0, cs.Id, c.Id
                FROM ContentSections as cs, Countries as c
                where c.Url = 'uk'

                UPDATE CmsSettings
                SET CreatedDate = GETDATE(), ModificationDate = GETDATE()

                UPDATE UserPermissions
                SET CreatedDate = GETDATE(), ModificationDate = GETDATE()

                UPDATE UserRoles
                SET CreatedDate = GETDATE(), ModificationDate = GETDATE()

                UPDATE Users
                SET CreatedDate = GETDATE(), ModificationDate = GETDATE()

                UPDATE ContentSectionLocalizedStrings
                SET CreatedDate = GETDATE(), ModificationDate = GETDATE()");


            this.CreateTable(
                "dbo.SeoSectionLocalizedStrings",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Description = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    SeoSection_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.SeoSections", t => t.SeoSection_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.SeoSection_Id);

            this.CreateTable(
                "dbo.SeoSections",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Page = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.Addresses", "FirstName", c => c.String());
            this.AddColumn("dbo.Addresses", "LastName", c => c.String());
            this.AddColumn("dbo.Addresses", "AddressLine1", c => c.String());
            this.AddColumn("dbo.Addresses", "AddressLine2", c => c.String());
            this.AddColumn("dbo.Addresses", "City", c => c.String());
            this.AddColumn("dbo.Addresses", "Postcode", c => c.String());
            this.AddColumn("dbo.Addresses", "PhoneNumber", c => c.String());
            this.AddColumn("dbo.Addresses", "Country_Id", c => c.Guid());
            this.AddColumn("dbo.Users", "Gender", c => c.String());
            this.AddColumn("dbo.Users", "LoginProvider", c => c.String());
            this.AddColumn("dbo.Users", "Newsletter", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.Users", "Cart_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOptions", "Group_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOptions", "EcommerceShoppingCartLine_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOptions", "EcommerceOrderLine_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProductAttributes", "Value", c => c.String());
            this.AddColumn("dbo.EcommerceProductImages", "Url", c => c.String());
            this.AddColumn("dbo.EcommerceProductImages", "Tag", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceProductImages", "SortOrder", c => c.Int(nullable: false));
            this.AddColumn("dbo.EcommerceOptionGroupLocalizedStrings", "Group_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceOptionLocalizedStrings", "Option_Id", c => c.Guid());
            this.AlterColumn("dbo.EcommerceAttributeLocalizedStrings", "Name", c => c.String());
            this.AddPrimaryKey("dbo.EcommerceCategoryEcommerceProducts", new[] { "EcommerceCategory_Id", "EcommerceProduct_Id" });
            this.CreateIndex("dbo.Addresses", "Country_Id");
            this.CreateIndex("dbo.Users", "Cart_Id");
            this.CreateIndex("dbo.EcommerceOptions", "Group_Id");
            this.CreateIndex("dbo.EcommerceOptions", "EcommerceShoppingCartLine_Id");
            this.CreateIndex("dbo.EcommerceOptions", "EcommerceOrderLine_Id");
            this.CreateIndex("dbo.EcommerceOptionGroupLocalizedStrings", "Group_Id");
            this.CreateIndex("dbo.EcommerceOptionLocalizedStrings", "Option_Id");
            this.AddForeignKey("dbo.Addresses", "Country_Id", "dbo.Countries", "Id");
            this.AddForeignKey("dbo.EcommerceOptionGroupLocalizedStrings", "Group_Id", "dbo.EcommerceOptionGroups", "Id");
            this.AddForeignKey("dbo.EcommerceOptions", "Group_Id", "dbo.EcommerceOptionGroups", "Id");
            this.AddForeignKey("dbo.EcommerceOptions", "EcommerceShoppingCartLine_Id", "dbo.EcommerceShoppingCartLines", "Id");
            this.AddForeignKey("dbo.Users", "Cart_Id", "dbo.EcommerceShoppingCarts", "Id");
            this.AddForeignKey("dbo.EcommerceOptions", "EcommerceOrderLine_Id", "dbo.EcommerceOrderLines", "Id");
            this.DropColumn("dbo.Addresses", "Name");
            this.DropColumn("dbo.Addresses", "Description");
            this.DropColumn("dbo.ContentSections", "Content");
            this.DropColumn("dbo.EcommerceProducts", "Model");
            this.DropColumn("dbo.EcommerceProducts", "Image");
            this.DropColumn("dbo.EcommerceProducts", "Vat");
            this.DropColumn("dbo.EcommerceProducts", "Price");
            this.DropColumn("dbo.EcommerceProducts", "Weight");
            this.DropColumn("dbo.EcommerceProducts", "Length");
            this.DropColumn("dbo.EcommerceProducts", "Width");
            this.DropColumn("dbo.EcommerceProducts", "Height");
            this.DropColumn("dbo.EcommerceProducts", "LengthClass_Id");
            this.DropColumn("dbo.EcommerceProducts", "WeightClass_Id");
            this.DropColumn("dbo.EcommerceProductAttributes", "Text");
            this.DropColumn("dbo.EcommerceProductImages", "Image");
            this.DropColumn("dbo.EcommerceProductOptions", "Value");
            this.DropColumn("dbo.EcommerceProductOptions", "IsRequired");
            this.DropColumn("dbo.EcommerceOptionGroupLocalizedStrings", "Unit");
            this.DropColumn("dbo.EcommerceOptionLocalizedStrings", "Unit");
            this.DropTable("dbo.EcommerceOptionDescriptions");
            this.DropTable("dbo.EcommerceOptionValueDescriptions");
            this.DropTable("dbo.EcommerceOptionValues");
            this.DropTable("dbo.EcommerceProductGroups");
            this.DropTable("dbo.EcommerceProductOptionValues");
            this.DropTable("dbo.MenuLinks");

        }

        public override void Down()
        {
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceVariantGroupLocalizedStrings_dbo.EcommerceVariantGroups_Group_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceOptionGroupLocalizedStrings] DROP [FK_dbo.EcommerceVariantGroupLocalizedStrings_dbo.EcommerceVariantGroups_Group_Id]");
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceVariants_dbo.EcommerceOrderLines_EcommerceOrderLine_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceOptions] DROP [FK_dbo.EcommerceVariants_dbo.EcommerceOrderLines_EcommerceOrderLine_Id]");
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceVariants_dbo.EcommerceShoppingCartLines_EcommerceShoppingCartLine_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceOptions] DROP [FK_dbo.EcommerceVariants_dbo.EcommerceShoppingCartLines_EcommerceShoppingCartLine_Id]");
            this.Sql("IF (OBJECT_ID('dbo.[FK_dbo.EcommerceVariants_dbo.EcommerceVariantGroups_Group_Id]', 'F') IS NOT NULL) ALTER TABLE [EcommerceOptions] DROP [FK_dbo.EcommerceVariants_dbo.EcommerceVariantGroups_Group_Id]");



            this.CreateTable(
                "dbo.MenuLinks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Label = c.String(),
                    ActiveLink = c.String(),
                    Position = c.Int(nullable: false),
                    Controller = c.String(),
                    Action = c.String(),
                    Icon = c.String(),
                    Permission = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceProductOptionValues",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.String(),
                    IsRequired = c.Boolean(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Option_Id = c.Guid(),
                    Product_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceProductGroups",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Product_Id = c.Guid(),
                    ProductSameGroup_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOptionValues",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Image = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Option_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOptionValueDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Option_Id = c.Guid(),
                    OptionValue_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.EcommerceOptionDescriptions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModificationDate = c.DateTime(),
                    Country_Id = c.Guid(),
                    Option_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.AddColumn("dbo.EcommerceOptionLocalizedStrings", "Unit", c => c.String());
            this.AddColumn("dbo.EcommerceOptionGroupLocalizedStrings", "Unit", c => c.String());
            this.AddColumn("dbo.EcommerceProductOptions", "IsRequired", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.EcommerceProductOptions", "Value", c => c.String());
            this.AddColumn("dbo.EcommerceProductImages", "Image", c => c.String());
            this.AddColumn("dbo.EcommerceProductAttributes", "Text", c => c.String());
            this.AddColumn("dbo.EcommerceProducts", "WeightClass_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProducts", "LengthClass_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceProducts", "Height", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "Width", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "Length", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "Weight", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "Price", c => c.Single(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "Vat", c => c.Single(nullable: false));
            this.AddColumn("dbo.EcommerceProducts", "Image", c => c.String());
            this.AddColumn("dbo.EcommerceProducts", "Model", c => c.String());
            this.AddColumn("dbo.ContentSections", "Content", c => c.String());
            this.AddColumn("dbo.Addresses", "Description", c => c.String());
            this.AddColumn("dbo.Addresses", "Name", c => c.String());
            this.DropForeignKey("dbo.SeoSectionLocalizedStrings", "SeoSection_Id", "dbo.SeoSections");
            this.DropForeignKey("dbo.SeoSectionLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.ContentSectionLocalizedStrings", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.ContentSectionLocalizedStrings", "ContentSection_Id", "dbo.ContentSections");
            this.DropForeignKey("dbo.EcommerceOrders", "User_Id", "dbo.Users");
            this.DropForeignKey("dbo.EcommerceOrders", "ShoppingCartReference_Id", "dbo.EcommerceShoppingCarts");
            this.DropForeignKey("dbo.EcommerceOptions", "EcommerceOrderLine_Id", "dbo.EcommerceOrderLines");
            this.DropForeignKey("dbo.EcommerceOrderLines", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceOrderLines", "Order_Id", "dbo.EcommerceOrders");
            this.DropForeignKey("dbo.EcommerceOrders", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropForeignKey("dbo.Users", "Cart_Id", "dbo.EcommerceShoppingCarts");
            this.DropForeignKey("dbo.EcommerceShoppingCartLines", "ShoppingCart_Id", "dbo.EcommerceShoppingCarts");
            this.DropForeignKey("dbo.EcommerceOptions", "EcommerceShoppingCartLine_Id", "dbo.EcommerceShoppingCartLines");
            this.DropForeignKey("dbo.EcommerceShoppingCartLines", "Product_Id", "dbo.EcommerceProducts");
            this.DropForeignKey("dbo.EcommerceOptionPriceModifiers", "EcommerceOption_Id", "dbo.EcommerceOptions");
            this.DropForeignKey("dbo.EcommerceOptionPriceModifiers", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.EcommerceOptions", "Group_Id", "dbo.EcommerceOptionGroups");
            this.DropForeignKey("dbo.EcommerceOptionGroupLocalizedStrings", "Group_Id", "dbo.EcommerceOptionGroups");
            this.DropForeignKey("dbo.EcommerceShoppingCarts", "ShippingAddress_Id", "dbo.Addresses");
            this.DropForeignKey("dbo.EcommerceShoppingCarts", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropForeignKey("dbo.EcommerceShoppingCarts", "Country_Id", "dbo.Countries");
            this.DropForeignKey("dbo.Addresses", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.SeoSectionLocalizedStrings", new[] { "SeoSection_Id" });
            this.DropIndex("dbo.SeoSectionLocalizedStrings", new[] { "Country_Id" });
            this.DropIndex("dbo.ContentSectionLocalizedStrings", new[] { "Country_Id" });
            this.DropIndex("dbo.ContentSectionLocalizedStrings", new[] { "ContentSection_Id" });
            this.DropIndex("dbo.EcommerceOrderLines", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceOrderLines", new[] { "Order_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "User_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "ShoppingCartReference_Id" });
            this.DropIndex("dbo.EcommerceOrders", new[] { "Coupon_Id" });
            this.DropIndex("dbo.EcommerceOptionPriceModifiers", new[] { "EcommerceOption_Id" });
            this.DropIndex("dbo.EcommerceOptionPriceModifiers", new[] { "Country_Id" });
            this.DropIndex("dbo.EcommerceOptionLocalizedStrings", new[] { "Option_Id" });
            this.DropIndex("dbo.EcommerceOptionGroupLocalizedStrings", new[] { "Group_Id" });
            this.DropIndex("dbo.EcommerceOptions", new[] { "EcommerceOrderLine_Id" });
            this.DropIndex("dbo.EcommerceOptions", new[] { "EcommerceShoppingCartLine_Id" });
            this.DropIndex("dbo.EcommerceOptions", new[] { "Group_Id" });
            this.DropIndex("dbo.EcommerceShoppingCartLines", new[] { "ShoppingCart_Id" });
            this.DropIndex("dbo.EcommerceShoppingCartLines", new[] { "Product_Id" });
            this.DropIndex("dbo.EcommerceShoppingCarts", new[] { "ShippingAddress_Id" });
            this.DropIndex("dbo.EcommerceShoppingCarts", new[] { "Coupon_Id" });
            this.DropIndex("dbo.EcommerceShoppingCarts", new[] { "Country_Id" });
            this.DropIndex("dbo.Users", new[] { "Cart_Id" });
            this.DropIndex("dbo.Addresses", new[] { "Country_Id" });
            this.DropPrimaryKey("dbo.EcommerceCategoryEcommerceProducts");
            this.AlterColumn("dbo.EcommerceAttributeLocalizedStrings", "Name", c => c.Int(nullable: false));
            this.DropColumn("dbo.EcommerceOptionLocalizedStrings", "Option_Id");
            this.DropColumn("dbo.EcommerceOptionGroupLocalizedStrings", "Group_Id");
            this.DropColumn("dbo.EcommerceProductImages", "SortOrder");
            this.DropColumn("dbo.EcommerceProductImages", "Tag");
            this.DropColumn("dbo.EcommerceProductImages", "Url");
            this.DropColumn("dbo.EcommerceProductAttributes", "Value");
            this.DropColumn("dbo.EcommerceOptions", "EcommerceOrderLine_Id");
            this.DropColumn("dbo.EcommerceOptions", "EcommerceShoppingCartLine_Id");
            this.DropColumn("dbo.EcommerceOptions", "Group_Id");
            this.DropColumn("dbo.Users", "Cart_Id");
            this.DropColumn("dbo.Users", "Newsletter");
            this.DropColumn("dbo.Users", "LoginProvider");
            this.DropColumn("dbo.Users", "Gender");
            this.DropColumn("dbo.Addresses", "Country_Id");
            this.DropColumn("dbo.Addresses", "PhoneNumber");
            this.DropColumn("dbo.Addresses", "Postcode");
            this.DropColumn("dbo.Addresses", "City");
            this.DropColumn("dbo.Addresses", "AddressLine2");
            this.DropColumn("dbo.Addresses", "AddressLine1");
            this.DropColumn("dbo.Addresses", "LastName");
            this.DropColumn("dbo.Addresses", "FirstName");
            this.DropTable("dbo.SeoSections");
            this.DropTable("dbo.SeoSectionLocalizedStrings");
            this.DropTable("dbo.ContentSectionLocalizedStrings");
            this.DropTable("dbo.EcommerceOrderLines");
            this.DropTable("dbo.EcommerceOrders");
            this.DropTable("dbo.EcommerceOptionPriceModifiers");
            this.DropTable("dbo.EcommerceOptionGroups");
            this.DropTable("dbo.EcommerceShoppingCartLines");
            this.DropTable("dbo.EcommerceCoupons");
            this.DropTable("dbo.EcommerceShoppingCarts");
            this.AddPrimaryKey("dbo.EcommerceCategoryEcommerceProducts", new[] { "EcommerceProduct_Id", "EcommerceCategory_Id" });
            this.RenameIndex(table: "dbo.EcommerceAttributeGroupLocalizedStrings", name: "IX_Group_Id", newName: "IX_AttributeGroup_Id");
            this.RenameIndex(table: "dbo.EcommerceAttributes", name: "IX_Group_Id", newName: "IX_AttributeGroup_Id");
            this.RenameColumn(table: "dbo.EcommerceAttributes", name: "Group_Id", newName: "AttributeGroup_Id");
            this.RenameColumn(table: "dbo.EcommerceAttributeGroupLocalizedStrings", name: "Group_Id", newName: "AttributeGroup_Id");
            this.RenameColumn(table: "dbo.Countries", name: "LanguageCode", newName: "CultureInfoName");
            this.CreateIndex("dbo.EcommerceProductOptionValues", "Product_Id");
            this.CreateIndex("dbo.EcommerceProductOptionValues", "Option_Id");
            this.CreateIndex("dbo.EcommerceProductGroups", "ProductSameGroup_Id");
            this.CreateIndex("dbo.EcommerceProductGroups", "Product_Id");
            this.CreateIndex("dbo.EcommerceOptionValues", "Option_Id");
            this.CreateIndex("dbo.EcommerceOptionValueDescriptions", "OptionValue_Id");
            this.CreateIndex("dbo.EcommerceOptionValueDescriptions", "Option_Id");
            this.CreateIndex("dbo.EcommerceOptionValueDescriptions", "Country_Id");
            this.CreateIndex("dbo.EcommerceOptionDescriptions", "Option_Id");
            this.CreateIndex("dbo.EcommerceOptionDescriptions", "Country_Id");
            this.CreateIndex("dbo.EcommerceProducts", "WeightClass_Id");
            this.CreateIndex("dbo.EcommerceProducts", "LengthClass_Id");
            this.RenameTable(name: "dbo.EcommerceOptionLocalizedStrings", newName: "EcommerceWeightClasses");
            this.RenameTable(name: "dbo.EcommerceOptionGroupLocalizedStrings", newName: "EcommerceLengthClasses");
            this.AddForeignKey("dbo.EcommerceProductOptionValues", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceProductOptionValues", "Option_Id", "dbo.EcommerceOptions", "Id");
            this.AddForeignKey("dbo.EcommerceProductGroups", "ProductSameGroup_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceProductGroups", "Product_Id", "dbo.EcommerceProducts", "Id");
            this.AddForeignKey("dbo.EcommerceOptionValueDescriptions", "OptionValue_Id", "dbo.EcommerceOptionValues", "Id");
            this.AddForeignKey("dbo.EcommerceOptionValues", "Option_Id", "dbo.EcommerceOptions", "Id");
            this.AddForeignKey("dbo.EcommerceProducts", "WeightClass_Id", "dbo.EcommerceWeightClasses", "Id");
            this.AddForeignKey("dbo.EcommerceProducts", "LengthClass_Id", "dbo.EcommerceLengthClasses", "Id");
            this.RenameTable(name: "dbo.EcommerceCategoryLocalizedStrings", newName: "EcommerceCategoryDescriptions");
            this.RenameTable(name: "dbo.EcommerceAttributeGroupLocalizedStrings", newName: "EcommerceAttributeGroupDescriptions");
            this.RenameTable(name: "dbo.EcommerceProductTags", newName: "EcommerceTags");
            this.RenameTable(name: "dbo.EcommerceCategoryEcommerceProducts", newName: "EcommerceProductEcommerceCategories");
            this.RenameTable(name: "dbo.EcommerceProductPrices", newName: "EcommerceCountryPrices");
            this.RenameTable(name: "dbo.EcommerceProductLocalizedStrings", newName: "EcommerceProductDescriptions");
            this.RenameTable(name: "dbo.EcommerceAttributeLocalizedStrings", newName: "EcommerceAttributeDescriptions");
        }
    }
}
