namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ScheduleTask : DbMigration
    {
        public override void Up()
        {
            this.DropTable("dbo.ScheduledTasks");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.ScheduledTasks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    ExecutionDate = c.DateTime(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

        }
    }
}
