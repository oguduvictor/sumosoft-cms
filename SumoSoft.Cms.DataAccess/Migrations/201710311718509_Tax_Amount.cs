namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Tax_Amount : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceTaxes", "Amount", c => c.Double());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceTaxes", "Amount");
        }
    }
}
