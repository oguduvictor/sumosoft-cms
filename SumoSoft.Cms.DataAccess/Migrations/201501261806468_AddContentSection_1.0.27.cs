namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddContentSection_1027 : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.ContentSections",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    Content = c.String(),
                    Note = c.String(),
                    LastChangeDate = c.DateTime(),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            this.DropTable("dbo.ContentSections");
        }
    }
}
