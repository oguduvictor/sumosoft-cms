namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_207 : DbMigration
    {
        public override void Up()
        {
            this.Sql("UPDATE [dbo].[ContentSections] SET [IsHtml] = 1");
        }

        public override void Down()
        {
            this.Sql("UPDATE [dbo].[ContentSections] SET [IsHtml] = 0");
        }
    }
}
