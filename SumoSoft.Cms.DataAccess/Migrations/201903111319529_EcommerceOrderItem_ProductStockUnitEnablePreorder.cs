namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class EcommerceOrderItem_ProductStockUnitEnablePreorder : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitReleaseDate", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.EcommerceOrderItems", "ProductStockUnitEnablePreorder", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitEnablePreorder");
            this.DropColumn("dbo.EcommerceOrderItems", "ProductStockUnitReleaseDate");
        }
    }
}
