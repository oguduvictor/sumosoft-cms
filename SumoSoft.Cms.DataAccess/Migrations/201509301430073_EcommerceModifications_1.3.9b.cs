namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_139b : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceAttributeTypeOptions", newName: "EcommerceAttributeOptions");
            this.RenameTable(name: "dbo.EcommerceAttributeTypeOptionLocalizedStrings", newName: "EcommerceAttributeOptionLocalizedStrings");
            this.RenameColumn(table: "dbo.EcommerceAttributeOptionLocalizedStrings", name: "EcommerceAttributeTypeOption_Id", newName: "EcommerceAttributeOption_Id");
            this.RenameIndex(table: "dbo.EcommerceAttributeOptionLocalizedStrings", name: "IX_EcommerceAttributeTypeOption_Id", newName: "IX_EcommerceAttributeOption_Id");
        }

        public override void Down()
        {
            this.RenameIndex(table: "dbo.EcommerceAttributeOptionLocalizedStrings", name: "IX_EcommerceAttributeOption_Id", newName: "IX_EcommerceAttributeTypeOption_Id");
            this.RenameColumn(table: "dbo.EcommerceAttributeOptionLocalizedStrings", name: "EcommerceAttributeOption_Id", newName: "EcommerceAttributeTypeOption_Id");
            this.RenameTable(name: "dbo.EcommerceAttributeOptionLocalizedStrings", newName: "EcommerceAttributeTypeOptionLocalizedStrings");
            this.RenameTable(name: "dbo.EcommerceAttributeOptions", newName: "EcommerceAttributeTypeOptions");
        }
    }
}
