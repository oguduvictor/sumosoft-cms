namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_186 : DbMigration
    {
        public override void Up()
        {
            this.AlterColumn("dbo.EcommerceOrderStatus", "Value", c => c.String());
        }

        public override void Down()
        {
            this.AlterColumn("dbo.EcommerceOrderStatus", "Value", c => c.Int(nullable: false));
        }
    }
}
