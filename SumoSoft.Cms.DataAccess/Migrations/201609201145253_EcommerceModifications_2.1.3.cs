namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_213 : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceCarts", "Id", "dbo.Users");
            this.DropIndex("dbo.EcommerceCarts", new[] { "Id" });
            this.AddColumn("dbo.EcommerceCarts", "User_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCarts", "User_Id");
            this.AddForeignKey("dbo.EcommerceCarts", "User_Id", "dbo.Users", "Id");

            this.Sql("UPDATE [dbo].[EcommerceCarts]" +
                "SET [dbo].[EcommerceCarts].[User_Id] = [dbo].[Users].[Id]" +
                "FROM [dbo].[EcommerceCarts]" +
                "INNER JOIN [dbo].[Users]" +
                "ON [dbo].[EcommerceCarts].[Id] = [dbo].[Users].[Cart_Id]");

            this.Sql(@"ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_dbo.Users_dbo.EcommerceCarts_Cart_Id]");

            this.DropIndex("dbo.Users", new[] { "Cart_Id" });
            this.DropColumn("dbo.Users", "Cart_Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceCarts", "User_Id", "dbo.Users");
            this.DropIndex("dbo.EcommerceCarts", new[] { "User_Id" });
            this.DropColumn("dbo.EcommerceCarts", "User_Id");

            this.AddColumn("dbo.Users", "Cart_Id", c => c.Guid());

            this.Sql(@"ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.EcommerceCarts_Cart_Id] FOREIGN KEY (Cart_Id) REFERENCES EcommerceCarts(Id)");

            this.CreateIndex("dbo.Users", new[] { "Cart_Id" });
        }
    }
}
