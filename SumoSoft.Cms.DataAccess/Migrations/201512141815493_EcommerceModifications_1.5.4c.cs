namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_154c : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCartItemProductVariants", "SelectValue_Id");
            this.AddForeignKey("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", "dbo.EcommerceProductVariantOptions", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", "dbo.EcommerceProductVariantOptions");
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "SelectValue_Id" });
            this.DropColumn("dbo.EcommerceCartItemProductVariants", "SelectValue_Id");
        }
    }
}
