namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceLocalizedKits : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCategoryLocalizedKits", "Image", c => c.String());

            this.Sql(@"IF EXISTS (
                    SELECT * 
                    FROM INFORMATION_SCHEMA.COLUMNS 
                    WHERE table_name = 'EcommerceCategories'
                    AND column_name = 'Image'
                )
                BEGIN
	                UPDATE [dbo].[EcommerceCategoryLocalizedKits]
	                SET [dbo].[EcommerceCategoryLocalizedKits].[Image] = [dbo].[EcommerceCategories].[Image] 
	                FROM [dbo].[EcommerceCategories]
	                WHERE [dbo].[EcommerceCategoryLocalizedKits].[Category_Id] = [dbo].[EcommerceCategories].[Id]
                END
            ");

            this.DropColumn("dbo.EcommerceCategories", "Image");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceCategories", "Image", c => c.String());
            this.DropColumn("dbo.EcommerceCategoryLocalizedKits", "Image");
        }
    }
}
