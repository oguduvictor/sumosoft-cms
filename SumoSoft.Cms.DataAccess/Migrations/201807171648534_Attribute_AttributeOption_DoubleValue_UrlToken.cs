namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Attribute_AttributeOption_DoubleValue_UrlToken : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProductAttributes", "DoubleValue", c => c.Double(nullable: false));
            this.AddColumn("dbo.EcommerceAttributes", "UrlToken", c => c.String(maxLength: 250));
            this.AddColumn("dbo.EcommerceAttributeOptions", "UrlToken", c => c.String(maxLength: 250));
            this.CreateIndex("dbo.EcommerceAttributes", "UrlToken", name: "IX_Attribute_UrlToken");
            this.CreateIndex("dbo.EcommerceAttributeOptions", "UrlToken", name: "IX_AttributeOption_UrlToken");
            this.Sql(@"UPDATE EcommerceProductAttributes SET DoubleValue = IntegerValue");
            this.DropColumn("dbo.EcommerceProductAttributes", "IntegerValue");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceProductAttributes", "IntegerValue", c => c.Int(nullable: false));
            this.DropIndex("dbo.EcommerceAttributeOptions", "IX_AttributeOption_UrlToken");
            this.DropIndex("dbo.EcommerceAttributes", "IX_Attribute_UrlToken");
            this.DropColumn("dbo.EcommerceAttributeOptions", "UrlToken");
            this.DropColumn("dbo.EcommerceAttributes", "UrlToken");
            this.DropColumn("dbo.EcommerceProductAttributes", "DoubleValue");
        }
    }
}
