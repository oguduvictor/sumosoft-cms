namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class MailingList : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.MailingLists",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    SortOrder = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.MailingListLocalizedKits",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(),
                    Description = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(nullable: false),
                    Country_Id = c.Guid(),
                    MailingList_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .ForeignKey("dbo.MailingLists", t => t.MailingList_Id)
                .Index(t => t.Country_Id)
                .Index(t => t.MailingList_Id);

            this.CreateTable(
                "dbo.MailingListUsers",
                c => new
                {
                    MailingList_Id = c.Guid(nullable: false),
                    User_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.MailingList_Id, t.User_Id })
                .ForeignKey("dbo.MailingLists", t => t.MailingList_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.MailingList_Id)
                .Index(t => t.User_Id);

        }

        public override void Down()
        {
            this.DropForeignKey("dbo.MailingListUsers", "User_Id", "dbo.Users");
            this.DropForeignKey("dbo.MailingListUsers", "MailingList_Id", "dbo.MailingLists");
            this.DropForeignKey("dbo.MailingListLocalizedKits", "MailingList_Id", "dbo.MailingLists");
            this.DropForeignKey("dbo.MailingListLocalizedKits", "Country_Id", "dbo.Countries");
            this.DropIndex("dbo.MailingListUsers", new[] { "User_Id" });
            this.DropIndex("dbo.MailingListUsers", new[] { "MailingList_Id" });
            this.DropIndex("dbo.MailingListLocalizedKits", new[] { "MailingList_Id" });
            this.DropIndex("dbo.MailingListLocalizedKits", new[] { "Country_Id" });
            this.DropTable("dbo.MailingListUsers");
            this.DropTable("dbo.MailingListLocalizedKits");
            this.DropTable("dbo.MailingLists");
        }
    }
}
