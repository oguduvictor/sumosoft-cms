namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_203 : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.ContentSections", "IsHtml", c => c.Boolean(nullable: false));

            this.AlterColumn("dbo.ContentSections", "Name", c => c.String(maxLength: 250));

            this.CreateIndex("dbo.ContentSections", "Name", true, "IX_ContentSections_Name");
        }

        public override void Down()
        {
            this.DropColumn("dbo.ContentSections", "IsHtml");

            this.DropIndex("dbo.ContentSections", "IX_ContentSections_Name");

            this.AlterColumn("dbo.ContentSections", "Name", c => c.String());
        }
    }
}
