namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ScheduledTask : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.ScheduledTasks",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Name = c.String(),
                    ExecutionDate = c.DateTime(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            this.DropTable("dbo.ScheduledTasks");
        }
    }
}
