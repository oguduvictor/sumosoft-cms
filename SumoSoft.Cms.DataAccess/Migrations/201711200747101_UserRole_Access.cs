namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class UserRole_Access : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.UserPermissions", "UserRole_Id", "dbo.UserRoles");
            this.DropIndex("dbo.UserPermissions", new[] { "UserRole_Id" });
            this.AddColumn("dbo.UserRoles", "AccessAdmin", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminEcommerce", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminOrders", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminBlog", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminMedia", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminContent", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminEmails", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminSeo", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminUsers", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminCountries", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "AccessAdminSettings", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditOrder", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateProduct", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditProduct", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditProductAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteProduct", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateAttribute", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditAttribute", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditAttributeName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditAttributeAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteAttribute", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateAttributeOption", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditAttributeOption", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditAttributeOptionName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditAttributeOptionAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteAttributeOption", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateVariant", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditVariant", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditVariantName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditVariantAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteVariant", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateVariantOption", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditVariantOption", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditVariantOptionName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditVariantOptionAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteVariantOption", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateShippingBox", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditShippingBox", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditShippingBoxName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditShippingBoxAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteShippingBox", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateEcommerceCategory", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditEcommerceCategory", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditEcommerceCategoryName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditEcommerceCategoryAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteEcommerceCategory", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateBlogPost", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditBlogPost", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteBlogPost", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateContentSection", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditContentSection", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditContentSectionName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditContentSectionAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteContentSection", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateEmail", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditEmail", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditEmailName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditEmailAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteEmail", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateSeoSection", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditSeoSection", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditSeoSectionPage", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditSeoSectionAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteSeoSection", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateUser", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditUser", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteUser", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateUserRole", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditUserRole", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteUserRole", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "CreateCountry", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditCountry", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteCountry", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "ViewNotificationsExceptions", c => c.Boolean(nullable: false));
            this.DropTable("dbo.UserPermissions");
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.UserPermissions",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Value = c.Int(nullable: false),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(),
                    UserRole_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id);

            this.DropColumn("dbo.UserRoles", "ViewNotificationsExceptions");
            this.DropColumn("dbo.UserRoles", "DeleteCountry");
            this.DropColumn("dbo.UserRoles", "EditCountry");
            this.DropColumn("dbo.UserRoles", "CreateCountry");
            this.DropColumn("dbo.UserRoles", "DeleteUserRole");
            this.DropColumn("dbo.UserRoles", "EditUserRole");
            this.DropColumn("dbo.UserRoles", "CreateUserRole");
            this.DropColumn("dbo.UserRoles", "DeleteUser");
            this.DropColumn("dbo.UserRoles", "EditUser");
            this.DropColumn("dbo.UserRoles", "CreateUser");
            this.DropColumn("dbo.UserRoles", "DeleteSeoSection");
            this.DropColumn("dbo.UserRoles", "EditSeoSectionAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditSeoSectionPage");
            this.DropColumn("dbo.UserRoles", "EditSeoSection");
            this.DropColumn("dbo.UserRoles", "CreateSeoSection");
            this.DropColumn("dbo.UserRoles", "DeleteEmail");
            this.DropColumn("dbo.UserRoles", "EditEmailAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditEmailName");
            this.DropColumn("dbo.UserRoles", "EditEmail");
            this.DropColumn("dbo.UserRoles", "CreateEmail");
            this.DropColumn("dbo.UserRoles", "DeleteContentSection");
            this.DropColumn("dbo.UserRoles", "EditContentSectionAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditContentSectionName");
            this.DropColumn("dbo.UserRoles", "EditContentSection");
            this.DropColumn("dbo.UserRoles", "CreateContentSection");
            this.DropColumn("dbo.UserRoles", "DeleteBlogPost");
            this.DropColumn("dbo.UserRoles", "EditBlogPost");
            this.DropColumn("dbo.UserRoles", "CreateBlogPost");
            this.DropColumn("dbo.UserRoles", "DeleteEcommerceCategory");
            this.DropColumn("dbo.UserRoles", "EditEcommerceCategoryAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditEcommerceCategoryName");
            this.DropColumn("dbo.UserRoles", "EditEcommerceCategory");
            this.DropColumn("dbo.UserRoles", "CreateEcommerceCategory");
            this.DropColumn("dbo.UserRoles", "DeleteShippingBox");
            this.DropColumn("dbo.UserRoles", "EditShippingBoxAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditShippingBoxName");
            this.DropColumn("dbo.UserRoles", "EditShippingBox");
            this.DropColumn("dbo.UserRoles", "CreateShippingBox");
            this.DropColumn("dbo.UserRoles", "DeleteVariantOption");
            this.DropColumn("dbo.UserRoles", "EditVariantOptionAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditVariantOptionName");
            this.DropColumn("dbo.UserRoles", "EditVariantOption");
            this.DropColumn("dbo.UserRoles", "CreateVariantOption");
            this.DropColumn("dbo.UserRoles", "DeleteVariant");
            this.DropColumn("dbo.UserRoles", "EditVariantAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditVariantName");
            this.DropColumn("dbo.UserRoles", "EditVariant");
            this.DropColumn("dbo.UserRoles", "CreateVariant");
            this.DropColumn("dbo.UserRoles", "DeleteAttributeOption");
            this.DropColumn("dbo.UserRoles", "EditAttributeOptionAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditAttributeOptionName");
            this.DropColumn("dbo.UserRoles", "EditAttributeOption");
            this.DropColumn("dbo.UserRoles", "CreateAttributeOption");
            this.DropColumn("dbo.UserRoles", "DeleteAttribute");
            this.DropColumn("dbo.UserRoles", "EditAttributeAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditAttributeName");
            this.DropColumn("dbo.UserRoles", "EditAttribute");
            this.DropColumn("dbo.UserRoles", "CreateAttribute");
            this.DropColumn("dbo.UserRoles", "DeleteProduct");
            this.DropColumn("dbo.UserRoles", "EditProductAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditProduct");
            this.DropColumn("dbo.UserRoles", "CreateProduct");
            this.DropColumn("dbo.UserRoles", "EditOrder");
            this.DropColumn("dbo.UserRoles", "AccessAdminSettings");
            this.DropColumn("dbo.UserRoles", "AccessAdminCountries");
            this.DropColumn("dbo.UserRoles", "AccessAdminUsers");
            this.DropColumn("dbo.UserRoles", "AccessAdminSeo");
            this.DropColumn("dbo.UserRoles", "AccessAdminEmails");
            this.DropColumn("dbo.UserRoles", "AccessAdminContent");
            this.DropColumn("dbo.UserRoles", "AccessAdminMedia");
            this.DropColumn("dbo.UserRoles", "AccessAdminBlog");
            this.DropColumn("dbo.UserRoles", "AccessAdminOrders");
            this.DropColumn("dbo.UserRoles", "AccessAdminEcommerce");
            this.DropColumn("dbo.UserRoles", "AccessAdmin");
            this.CreateIndex("dbo.UserPermissions", "UserRole_Id");
            this.AddForeignKey("dbo.UserPermissions", "UserRole_Id", "dbo.UserRoles", "Id");
        }
    }
}
