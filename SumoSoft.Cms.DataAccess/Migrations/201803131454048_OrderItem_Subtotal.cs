namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class OrderItem_Subtotal : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn("dbo.EcommerceOrderItems", "TotalBeforeTax", "SubtotalBeforeTax");
            this.RenameColumn("dbo.EcommerceOrderItems", "TotalAfterTax", "SubtotalAfterTax");
        }

        public override void Down()
        {
            this.RenameColumn("dbo.EcommerceOrderItems", "SubtotalBeforeTax", "TotalBeforeTax");
            this.RenameColumn("dbo.EcommerceOrderItems", "SubtotalAfterTax", "TotalAfterTax");
        }
    }
}
