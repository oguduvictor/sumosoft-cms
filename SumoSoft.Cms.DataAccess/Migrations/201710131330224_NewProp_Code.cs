namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class NewProp_Code : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceVariantOptions", "Code", c => c.String());
            this.AddColumn("dbo.EcommerceProducts", "Name", c => c.String());
            this.AddColumn("dbo.EcommerceProducts", "Code", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemVariants", "VariantOptionUrlToken", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemVariants", "VariantOptionCode", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedDescription", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItems", "ProductName", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItems", "ProductCode", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceOrderItems", "ProductCode");
            this.DropColumn("dbo.EcommerceOrderItems", "ProductName");
            this.DropColumn("dbo.EcommerceOrderItemVariants", "VariantOptionLocalizedDescription");
            this.DropColumn("dbo.EcommerceOrderItemVariants", "VariantOptionCode");
            this.DropColumn("dbo.EcommerceOrderItemVariants", "VariantOptionUrlToken");
            this.DropColumn("dbo.EcommerceProducts", "Code");
            this.DropColumn("dbo.EcommerceProducts", "Name");
            this.DropColumn("dbo.EcommerceVariantOptions", "Code");
        }
    }
}
