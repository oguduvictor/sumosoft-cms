namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Renaming : DbMigration
    {
        public override void Up()
        {
            this.RenameColumn(table: "dbo.EcommerceProductStockUnitLocalizedKits", name: "SaleAtPrice", newName: "SalePrice");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductBasePrice", newName: "ProductStockUnitBasePrice");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductSaleAtPrice", newName: "ProductStockUnitSalePrice");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "OrderItemStockStatus", newName: "StockStatus");
        }

        public override void Down()
        {
            this.RenameColumn(table: "dbo.EcommerceProductStockUnitLocalizedKits", name: "SalePrice", newName: "SaleAtPrice");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductStockUnitBasePrice", newName: "ProductBasePrice");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "ProductStockUnitSalePrice", newName: "ProductSaleAtPrice");
            this.RenameColumn(table: "dbo.EcommerceOrderItems", name: "StockStatus", newName: "OrderItemStockStatus");
        }
    }
}
