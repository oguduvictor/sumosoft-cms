namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Product_Comments : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceProducts", "Comments", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceProducts", "Comments");
        }
    }
}
