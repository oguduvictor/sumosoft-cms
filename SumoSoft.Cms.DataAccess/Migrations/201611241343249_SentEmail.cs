namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SentEmail : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.SentEmails", "Reference", c => c.String());
            this.AddColumn("dbo.SentEmails", "Coupon_Id", c => c.Guid());
            this.CreateIndex("dbo.SentEmails", "Coupon_Id");
            this.AddForeignKey("dbo.SentEmails", "Coupon_Id", "dbo.EcommerceCoupons", "Id");
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.SentEmails", "Coupon_Id", "dbo.EcommerceCoupons");
            this.DropIndex("dbo.SentEmails", new[] { "Coupon_Id" });
            this.DropColumn("dbo.SentEmails", "Coupon_Id");
            this.DropColumn("dbo.SentEmails", "Reference");
        }
    }
}
