namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class LogType : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.UserRoles", "ViewExceptionLogs", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "ViewEcommerceLogs", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "ViewContentLogs", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.Logs", "Type", c => c.Int(nullable: false));
            this.AddColumn("dbo.Logs", "Details", c => c.String());
            this.DropColumn("dbo.UserRoles", "ViewNotificationsExceptions");
            this.DropColumn("dbo.Logs", "Exception");
        }

        public override void Down()
        {
            this.AddColumn("dbo.Logs", "Exception", c => c.String());
            this.AddColumn("dbo.UserRoles", "ViewNotificationsExceptions", c => c.Boolean(nullable: false));
            this.DropColumn("dbo.Logs", "Details");
            this.DropColumn("dbo.Logs", "Type");
            this.DropColumn("dbo.UserRoles", "ViewContentLogs");
            this.DropColumn("dbo.UserRoles", "ViewEcommerceLogs");
            this.DropColumn("dbo.UserRoles", "ViewExceptionLogs");
        }
    }
}
