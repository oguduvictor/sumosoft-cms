namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CategoryRefactoring : DbMigration
    {
        public override void Up()
        {
            this.CreateTable(
                "dbo.EcommerceCategoryEcommerceProductImageKits",
                c => new
                {
                    EcommerceCategory_Id = c.Guid(nullable: false),
                    EcommerceProductImageKit_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceCategory_Id, t.EcommerceProductImageKit_Id })
                .ForeignKey("dbo.EcommerceCategories", t => t.EcommerceCategory_Id)
                .ForeignKey("dbo.EcommerceProductImageKits", t => t.EcommerceProductImageKit_Id)
                .Index(t => t.EcommerceCategory_Id)
                .Index(t => t.EcommerceProductImageKit_Id);

            this.CreateTable(
                "dbo.EcommerceCategoryEcommerceProductStockUnits",
                c => new
                {
                    EcommerceCategory_Id = c.Guid(nullable: false),
                    EcommerceProductStockUnit_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceCategory_Id, t.EcommerceProductStockUnit_Id })
                .ForeignKey("dbo.EcommerceCategories", t => t.EcommerceCategory_Id)
                .ForeignKey("dbo.EcommerceProductStockUnits", t => t.EcommerceProductStockUnit_Id)
                .Index(t => t.EcommerceCategory_Id)
                .Index(t => t.EcommerceProductStockUnit_Id);

            this.AddColumn("dbo.EcommerceCategories", "Type", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceProductStockUnits", "EcommerceProductStockUnit_Id", "dbo.EcommerceProductStockUnits");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceProductStockUnits", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceProductImageKits", "EcommerceProductImageKit_Id", "dbo.EcommerceProductImageKits");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceProductImageKits", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropIndex("dbo.EcommerceCategoryEcommerceProductStockUnits", new[] { "EcommerceProductStockUnit_Id" });
            this.DropIndex("dbo.EcommerceCategoryEcommerceProductStockUnits", new[] { "EcommerceCategory_Id" });
            this.DropIndex("dbo.EcommerceCategoryEcommerceProductImageKits", new[] { "EcommerceProductImageKit_Id" });
            this.DropIndex("dbo.EcommerceCategoryEcommerceProductImageKits", new[] { "EcommerceCategory_Id" });
            this.DropColumn("dbo.EcommerceCategories", "Type");
            this.DropTable("dbo.EcommerceCategoryEcommerceProductStockUnits");
            this.DropTable("dbo.EcommerceCategoryEcommerceProductImageKits");
        }
    }
}
