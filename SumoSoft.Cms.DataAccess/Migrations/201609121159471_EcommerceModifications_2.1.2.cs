namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_212 : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceCategories", "Parent_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceProducts", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceProducts", "EcommerceProduct_Id", "dbo.EcommerceProducts");
            this.DropIndex("dbo.EcommerceCategories", new[] { "Parent_Id" });
            this.DropIndex("dbo.EcommerceCategoryEcommerceProducts", new[] { "EcommerceCategory_Id" });
            this.DropIndex("dbo.EcommerceCategoryEcommerceProducts", new[] { "EcommerceProduct_Id" });

            this.CreateTable(
                "dbo.EcommerceCategoryEcommerceCategories",
                c => new
                {
                    EcommerceCategory_Id = c.Guid(nullable: false),
                    EcommerceCategory_Id1 = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceCategory_Id, t.EcommerceCategory_Id1 })
                .ForeignKey("dbo.EcommerceCategories", t => t.EcommerceCategory_Id)
                .ForeignKey("dbo.EcommerceCategories", t => t.EcommerceCategory_Id1)
                .Index(t => t.EcommerceCategory_Id)
                .Index(t => t.EcommerceCategory_Id1);


            this.AddColumn("dbo.EcommerceProducts", "Category_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCategories", "Name", c => c.String());
            this.CreateIndex("dbo.EcommerceProducts", "Category_Id");
            this.AddForeignKey("dbo.EcommerceProducts", "Category_Id", "dbo.EcommerceCategories", "Id");
            this.DropColumn("dbo.EcommerceCategories", "Image");
            this.DropColumn("dbo.EcommerceCategories", "Parent_Id");
            this.DropTable("dbo.EcommerceCategoryEcommerceProducts");

            this.RenameColumn(table: "dbo.EcommerceAttributeOptions", name: "Value", newName: "Name");
            this.RenameColumn(table: "dbo.EcommerceVariantOptions", name: "Value", newName: "Name");

            this.AddColumn("dbo.EcommerceCartItems", "Image", c => c.String());
            this.AddColumn("dbo.EcommerceOrderItems", "Image", c => c.String());
        }

        public override void Down()
        {
            this.CreateTable(
                "dbo.EcommerceCategoryEcommerceProducts",
                c => new
                {
                    EcommerceCategory_Id = c.Guid(nullable: false),
                    EcommerceProduct_Id = c.Guid(nullable: false),
                })
                .PrimaryKey(t => new { t.EcommerceCategory_Id, t.EcommerceProduct_Id });

            this.AddColumn("dbo.EcommerceCategories", "Parent_Id", c => c.Guid());
            this.AddColumn("dbo.EcommerceCategories", "Image", c => c.String());
            this.DropForeignKey("dbo.EcommerceProducts", "Category_Id", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id1", "dbo.EcommerceCategories");
            this.DropForeignKey("dbo.EcommerceCategoryEcommerceCategories", "EcommerceCategory_Id", "dbo.EcommerceCategories");
            this.DropIndex("dbo.EcommerceCategoryEcommerceCategories", new[] { "EcommerceCategory_Id1" });
            this.DropIndex("dbo.EcommerceCategoryEcommerceCategories", new[] { "EcommerceCategory_Id" });
            this.DropIndex("dbo.EcommerceProducts", new[] { "Category_Id" });
            this.DropColumn("dbo.EcommerceCategories", "Name");
            this.DropColumn("dbo.EcommerceProducts", "Category_Id");
            this.DropTable("dbo.EcommerceCategoryEcommerceCategories");
            this.CreateIndex("dbo.EcommerceCategoryEcommerceProducts", "EcommerceProduct_Id");
            this.CreateIndex("dbo.EcommerceCategoryEcommerceProducts", "EcommerceCategory_Id");
            this.CreateIndex("dbo.EcommerceCategories", "Parent_Id");
            this.AddForeignKey("dbo.EcommerceCategoryEcommerceProducts", "EcommerceProduct_Id", "dbo.EcommerceProducts", "Id", cascadeDelete: true);
            this.AddForeignKey("dbo.EcommerceCategoryEcommerceProducts", "EcommerceCategory_Id", "dbo.EcommerceCategories", "Id", cascadeDelete: true);
            this.AddForeignKey("dbo.EcommerceCategories", "Parent_Id", "dbo.EcommerceCategories", "Id");

            this.RenameColumn(table: "dbo.EcommerceAttributeOptions", name: "Name", newName: "Value");
            this.RenameColumn(table: "dbo.EcommerceVariantOptions", name: "Name", newName: "Value");

            this.DropColumn("dbo.EcommerceOrderItems", "Image");
            this.DropColumn("dbo.EcommerceCartItems", "Image");
        }
    }
}
