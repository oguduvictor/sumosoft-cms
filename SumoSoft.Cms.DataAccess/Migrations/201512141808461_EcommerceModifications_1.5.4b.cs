namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class EcommerceModifications_154b : DbMigration
    {
        public override void Up()
        {
            this.DropForeignKey("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", "dbo.EcommerceVariantOptions");
            this.DropIndex("dbo.EcommerceCartItemProductVariants", new[] { "SelectValue_Id" });
            this.DropColumn("dbo.EcommerceCartItemProductVariants", "SelectValue_Id");
        }

        public override void Down()
        {
            this.AddColumn("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", c => c.Guid());
            this.CreateIndex("dbo.EcommerceCartItemProductVariants", "SelectValue_Id");
            this.AddForeignKey("dbo.EcommerceCartItemProductVariants", "SelectValue_Id", "dbo.EcommerceVariantOptions", "Id");
        }
    }
}
