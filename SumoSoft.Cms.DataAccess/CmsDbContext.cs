﻿#pragma warning disable 1591

namespace SumoSoft.Cms.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Configurations;

    public class CmsDbContext : DbContext
    {
        public CmsDbContext() : base(SharedConstants.DbConnectionString)
        {
        }

        public delegate void SavingChangesEventHandler(
            IEnumerable<DbEntityEntry<BaseEntity>> added,
            IEnumerable<DbEntityEntry<BaseEntity>> modified,
            IEnumerable<DbEntityEntry<BaseEntity>> deleted);

        /// <summary>
        /// Invoked right before calling SaveChanges() or SaveChangesAsync()
        /// </summary>
        public static event SavingChangesEventHandler SavingChanges;

        public override int SaveChanges()
        {
            this.OnSavingChanges();

            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync()
        {
            this.OnSavingChanges();

            return base.SaveChangesAsync();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            this.OnSavingChanges();

            return base.SaveChangesAsync(cancellationToken);
        }

        protected virtual void OnSavingChanges()
        {
            this.ChangeTracker.DetectChanges();

            var added = this.ChangeTracker
                .Entries<BaseEntity>()
                .Where(t => t.State == EntityState.Added)
                .ToList();

            foreach (var entityEntry in added)
            {
                entityEntry.Entity.CreatedDate = DateTime.Now;
                entityEntry.Entity.ModifiedDate = DateTime.Now;
            }

            var modified = this.ChangeTracker
                .Entries<BaseEntity>()
                .Where(t => t.State == EntityState.Modified)
                .ToList();

            foreach (var entityEntry in modified)
            {
                entityEntry.Entity.ModifiedDate = DateTime.Now;
                entityEntry.Entity.CreatedDate = entityEntry.OriginalValues.GetValue<DateTime>(nameof(BaseEntity.CreatedDate));
            }

            var deleted = this.ChangeTracker
                .Entries<BaseEntity>()
                .Where(t => t.State == EntityState.Deleted)
                .ToList();

            SavingChanges?.Invoke(added, modified, deleted);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserAttributeConfiguration());
            modelBuilder.Configurations.Add(new UserRoleConfiguration());
            modelBuilder.Configurations.Add(new UserLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new UserVariantConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
            modelBuilder.Configurations.Add(new CountryConfiguration());
            modelBuilder.Configurations.Add(new CmsSettingConfiguration());
            modelBuilder.Configurations.Add(new ContentSectionConfiguration());
            modelBuilder.Configurations.Add(new ContentSectionLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new SeoSectionConfiguration());
            modelBuilder.Configurations.Add(new SeoSectionLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new LogConfiguration());
            modelBuilder.Configurations.Add(new SentEmailConfiguration());
            modelBuilder.Configurations.Add(new EmailConfiguration());
            modelBuilder.Configurations.Add(new EmailLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new BlogCategoryConfiguration());
            modelBuilder.Configurations.Add(new BlogCategoryLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new BlogCommentConfiguration());
            modelBuilder.Configurations.Add(new BlogPostConfiguration());
            modelBuilder.Configurations.Add(new ContentVersionConfiguration());
            modelBuilder.Configurations.Add(new AttributeConfiguration());
            modelBuilder.Configurations.Add(new AttributeLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new AttributeOptionConfiguration());
            modelBuilder.Configurations.Add(new AttributeOptionLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new CartConfiguration());
            modelBuilder.Configurations.Add(new CartItemConfiguration());
            modelBuilder.Configurations.Add(new CartItemVariantConfiguration());
            modelBuilder.Configurations.Add(new CartShippingBoxConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new CategoryLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new CouponConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new OrderItemConfiguration());
            modelBuilder.Configurations.Add(new OrderItemVariantConfiguration());
            modelBuilder.Configurations.Add(new OrderAddressConfiguration());
            modelBuilder.Configurations.Add(new OrderShippingBoxConfiguration());
            modelBuilder.Configurations.Add(new OrderCouponConfiguration());
            modelBuilder.Configurations.Add(new OrderCreditConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new ProductAttributeConfiguration());
            modelBuilder.Configurations.Add(new ProductLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new ProductRelatedConfiguration());
            modelBuilder.Configurations.Add(new ProductVariantConfiguration());
            modelBuilder.Configurations.Add(new ProductImageConfiguration());
            modelBuilder.Configurations.Add(new ProductImageKitConfiguration());
            modelBuilder.Configurations.Add(new ShippingBoxConfiguration());
            modelBuilder.Configurations.Add(new ShippingBoxLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new UserCreditConfiguration());
            modelBuilder.Configurations.Add(new VariantConfiguration());
            modelBuilder.Configurations.Add(new VariantLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new VariantOptionConfiguration());
            modelBuilder.Configurations.Add(new VariantOptionLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new ProductStockUnitConfiguration());
            modelBuilder.Configurations.Add(new ProductStockUnitLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new WishListConfiguration());
            modelBuilder.Configurations.Add(new TaxConfiguration());
            modelBuilder.Configurations.Add(new MailingListConfiguration());
            modelBuilder.Configurations.Add(new MailingListLocalizedKitConfiguration());
            modelBuilder.Configurations.Add(new MailingListSubscriptionConfiguration());
        }

        public virtual DbSet<Domain.Attribute> Attributes { get; set; }

        public virtual DbSet<AttributeLocalizedKit> AttributeLocalizedKits { get; set; }

        public virtual DbSet<AttributeOption> AttributeOptions { get; set; }

        public virtual DbSet<AttributeOptionLocalizedKit> AttributeOptionLocalizedKits { get; set; }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<UserAttribute> UserAttributes { get; set; }

        public virtual DbSet<UserRole> UserRoles { get; set; }

        public virtual DbSet<UserLocalizedKit> UserLocalizedKits { get; set; }

        public virtual DbSet<UserVariant> UserVariants { get; set; }

        public virtual DbSet<Address> Addresses { get; set; }

        public virtual DbSet<Country> Countries { get; set; }

        public virtual DbSet<CmsSettings> CmsSettings { get; set; }

        public virtual DbSet<ContentVersion> ContentVersions { get; set; }

        public virtual DbSet<ContentSection> ContentSections { get; set; }

        public virtual DbSet<ContentSectionLocalizedKit> ContentSectionLocalizedKits { get; set; }

        public virtual DbSet<SeoSection> SeoSections { get; set; }

        public virtual DbSet<SeoSectionLocalizedKit> SeoSectionLocalizedKits { get; set; }

        public virtual DbSet<Log> Logs { get; set; }

        public virtual DbSet<SentEmail> SentEmails { get; set; }

        public virtual DbSet<Email> Emails { get; set; }

        public virtual DbSet<EmailLocalizedKit> EmailLocalizedKits { get; set; }

        public virtual DbSet<Coupon> Coupons { get; set; }

        public virtual DbSet<BlogCategory> BlogCategories { get; set; }

        public virtual DbSet<BlogCategoryLocalizedKit> BlogCategoryLocalizedKits { get; set; }

        public virtual DbSet<BlogComment> BlogComments { get; set; }

        public virtual DbSet<BlogPost> BlogPosts { get; set; }

        public virtual DbSet<Category> Categories { get; set; }

        public virtual DbSet<CategoryLocalizedKit> CategoryLocalizedKits { get; set; }

        public virtual DbSet<Cart> Carts { get; set; }

        public virtual DbSet<CartItem> CartItems { get; set; }

        public virtual DbSet<CartItemVariant> CartItemVariants { get; set; }

        public virtual DbSet<CartShippingBox> CartShippingBoxes { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<OrderItem> OrderItems { get; set; }

        public virtual DbSet<OrderItemVariant> OrderItemVariants { get; set; }

        public virtual DbSet<OrderAddress> OrderAddresses { get; set; }

        public virtual DbSet<OrderShippingBox> OrderShippingBoxes { get; set; }

        public virtual DbSet<OrderCoupon> OrderCoupons { get; set; }

        public virtual DbSet<OrderCredit> OrderCredits { get; set; }

        public virtual DbSet<ProductLocalizedKit> ProductLocalizedKits { get; set; }

        public virtual DbSet<UserCredit> UserCredits { get; set; }

        public virtual DbSet<Variant> Variants { get; set; }

        public virtual DbSet<VariantLocalizedKit> VariantLocalizedKits { get; set; }

        public virtual DbSet<VariantOption> VariantOptions { get; set; }

        public virtual DbSet<VariantOptionLocalizedKit> VariantOptionLocalizedKits { get; set; }

        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<ProductAttribute> ProductAttributes { get; set; }

        public virtual DbSet<ProductVariant> ProductVariants { get; set; }

        public virtual DbSet<ProductRelatedProducts> ProductRelated { get; set; }

        public virtual DbSet<ShippingBox> ShippingBoxes { get; set; }

        public virtual DbSet<ShippingBoxLocalizedKit> ShippingBoxLocalizedKits { get; set; }

        public virtual DbSet<ProductStockUnit> ProductStockUnits { get; set; }

        public virtual DbSet<ProductStockUnitLocalizedKit> ProductStockUnitLocalizedKits { get; set; }

        public virtual DbSet<ProductImage> ProductImages { get; set; }

        public virtual DbSet<ProductImageKit> ProductImageKits { get; set; }

        public virtual DbSet<WishList> WishLists { get; set; }

        public virtual DbSet<Tax> Taxes { get; set; }

        public virtual DbSet<MailingList> MailingLists { get; set; }

        public virtual DbSet<MailingListLocalizedKit> MailingListLocalizedKits { get; set; }

        public virtual DbSet<MailingListSubscription> MailingListSubscriptions { get; set; }
    }
}