﻿namespace SumoSoft.Cms.Extensions
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Gets whether a string contains any substring in a given array.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="substrings"></param>
        /// <returns></returns>
        public static bool ContainsAny(this string text, params string[] substrings)
        {
            return substrings.Any(text.Contains);
        }

        /// <summary>
        /// Parse a json string as an object of a given type.
        /// </summary>
        /// <param name="json"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T ParseJsonAs<T>(this string json) where T : class
        {
            return Json.Decode(json, typeof(T)) as T;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <param name="fallbackValue"></param>
        /// <returns></returns>
        [CanBeNull]
        public static T TryParseJsonAs<T>(this string json, T fallbackValue = null) where T : class
        {
            try
            {
                return ParseJsonAs<T>(json);
            }
            catch (Exception)
            {
                return fallbackValue;
            }
        }

        /// <summary>
        /// Parse a json string as an object.
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static dynamic ParseJson(this string json)
        {
            return Json.Decode(json);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool ContainsHtml(this string text)
        {
            var regex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");

            return regex.IsMatch(text);
        }

        /// <summary>
        /// Damereau-Levenshein Distance algorithm. It calculates the number of letter additions, subtractions, substitutions
        /// and transpositions (swaps) necessary to convert one string to another. The lower the score, the more similar they are.
        /// </summary>
        public static int LevenshteinDistanceFrom(this string source, string target)
        {
            if (string.IsNullOrEmpty(source))
            {
                if (string.IsNullOrEmpty(target))
                {
                    return 0;
                }

                return target.Length;
            }

            if (string.IsNullOrEmpty(target))
            {
                return source.Length;
            }

            var n = source.Length;
            var m = target.Length;
            var d = new int[n + 1, m + 1];

            for (var i = 0; i <= n; d[i, 0] = i++)
            {
                ;
            }

            for (var j = 1; j <= m; d[0, j] = j++)
            {
                ;
            }

            for (var i = 1; i <= n; i++)
            {
                for (var j = 1; j <= m; j++)
                {
                    var cost = (target[j - 1] == source[i - 1]) ? 0 : 1;
                    var min1 = d[i - 1, j] + 1;
                    var min2 = d[i, j - 1] + 1;
                    var min3 = d[i - 1, j - 1] + cost;
                    d[i, j] = Math.Min(Math.Min(min1, min2), min3);
                }
            }
            return d[n, m];
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="tolerance">Levenshtein Distance.</param>
        /// <param name="ignoreCase"></param>
        /// <returns></returns>
        public static bool AproximatelyEqualsTo(this string source, string target, int tolerance = 1, bool ignoreCase = true)
        {
            if (ignoreCase)
            {
                source = source?.ToLower();
                target = target?.ToLower();
            }

            return source.LevenshteinDistanceFrom(target) <= tolerance;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="text"></param>
        /// <param name="search"></param>
        /// <param name="replace"></param>
        /// <returns></returns>
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            var pos = text.IndexOf(search, StringComparison.Ordinal);

            if (pos < 0)
            {
                return text;
            }

            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="text"></param>
        /// <param name="search"></param>
        /// <param name="replace"></param>
        /// <returns></returns>
        public static string ReplaceLast(this string text, string search, string replace)
        {
            var pos = text.LastIndexOf(search, StringComparison.Ordinal);

            if (pos < 0)
            {
                return text;
            }

            return text.Remove(pos, search.Length).Insert(pos, replace);
        }

        /// <summary>
        /// Capitalize the first letter of each word.
        /// </summary>
        [CanBeNull]
        public static string ToTitleCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        /// <summary>
        /// Capitalize the first letter.
        /// </summary>
        [CanBeNull]
        public static string ToUpperFirstLetter(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            if (str.Length > 1)
            {
                return char.ToUpper(str[0]) + str.Substring(1);
            }

            return str.ToUpper();
        }

        /// <summary>
        /// Make the first letter lowercase.
        /// </summary>
        [CanBeNull]
        public static string ToLowerFirstLetter(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            if (str.Length > 1)
            {
                return char.ToLower(str[0]) + str.Substring(1);
            }

            return str.ToLower();
        }

        /// <summary>
        /// Return string.Empty if null.
        /// </summary>
        [NotNull]
        public static string AsNotNull(this string text)
        {
            return text ?? string.Empty;
        }

        /// <summary>
        /// Convert string.Empty or null to another value.
        /// </summary>
        [NotNull]
        public static string IfNullOrEmptyConvertTo(this string text, string fallbackValue)
        {
            fallbackValue = fallbackValue ?? string.Empty;

            return string.IsNullOrEmpty(text) ? fallbackValue : text;
        }

        /// <summary>
        ///
        /// </summary>
        public static string SubstringFromFirst(this string text, string delimiter, bool includeDelimiter = false)
        {
            var indexOfDelimiter = text.IndexOf(delimiter, StringComparison.Ordinal);

            if (indexOfDelimiter == -1)
            {
                return text;
            }

            var lastIndex = includeDelimiter ? indexOfDelimiter : indexOfDelimiter + delimiter.Length;

            return text.Substring(lastIndex);
        }

        /// <summary>
        ///
        /// </summary>
        public static string SubstringUpToFirst(this string text, string delimiter, bool includeDelimiter = false)
        {
            var indexOfDelimiter = text.IndexOf(delimiter, StringComparison.Ordinal);

            if (indexOfDelimiter == -1)
            {
                return text;
            }

            var lastIndex = includeDelimiter ? indexOfDelimiter + delimiter.Length : indexOfDelimiter;

            return text.Substring(0, lastIndex);
        }

        /// <summary>
        ///
        /// </summary>
        public static bool ContainsCaseInsensitive(this string text, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }

            if (string.IsNullOrEmpty(text))
            {
                return false;
            }

            return text.IndexOf(value, StringComparison.CurrentCultureIgnoreCase) >= 0;
        }

        /// <summary>
        ///
        /// </summary>
        public static string ToValidCsvString(this string data)
        {
            if (data == null)
            {
                return string.Empty;
            }

            data = EscapeQuotes(data);
            if (data.Contains(","))
            {
                data = "\"" + data + "\"";
            }

            data = Regex.Replace(data, @"[\s]+", " ", RegexOptions.Compiled);
            data = data.Trim();

            return data;
        }

        /// <summary>
        ///
        /// </summary>
        public static string Shorten(this string text, int length, bool ellipsis = false)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

            if (text.Length > length)
            {
                text = text.Substring(0, Math.Min(text.Length, length));

                if (ellipsis)
                {
                    return text + "...";
                }
            }
            return text;
        }

        /// <summary>
        /// Encodes a string into a valid URL using HttpUtility.UrlPathEncode().
        /// </summary>
        public static string ToUrl(this string value)
        {
            return HttpUtility.UrlPathEncode(value);
        }

        /// <summary>
        /// Removes all the non aphanumeric characters and replaces the spaces with a dash to create a valid and readable URL.
        /// </summary>
        public static string ToPrettyUrl(this string value)
        {
            return
                Regex.Replace(value, @"[^a-zA-Z0-9 -]", "")
                .Replace(" ", "-")
                .Replace("--", "-")
                .ToLower().ToUrl();
        }

        /// <summary>
        ///
        /// </summary>
        public static string ToPrettyImageUrl(this string value)
        {
            return value
                    .Replace(" ", "-")
                    .Replace("--", "-")
                    .ToLower().ToUrl();
        }

        /// <summary>
        /// Removes spaces and changes capitalization to lower.
        /// </summary>
        public static string ToPrettyEmailAddress(this string value)
        {
            return value.Replace(" ", "").ToLower();
        }

        /// <summary>
        /// Encodes a string into an MvcHtmlString.
        /// </summary>
        public static MvcHtmlString ToMvcHtmlString(this string value)
        {
            return new MvcHtmlString(value);
        }

        /// <summary>
        ///
        /// </summary>
        public static int ToInt(this string value, int fallbackValue = 0)
        {
            return int.TryParse(value, out int result) ? result : fallbackValue;
        }

        /// <summary>
        ///
        /// </summary>
        public static double ToDouble(this string value, double fallbackValue = 0)
        {
            return double.TryParse(value, out double result) ? result : fallbackValue;
        }

        /// <summary>
        ///
        /// </summary>
        public static decimal ToDecimal(this string value, decimal fallbackValue = 0)
        {
            return decimal.TryParse(value, out decimal result) ? result : fallbackValue;
        }

        /// <summary>
        ///
        /// </summary>
        public static bool ToBoolean(this string value, bool fallbackValue = false)
        {
            return bool.TryParse(value, out bool result) ? result : fallbackValue;
        }

        /// <summary>
        ///
        /// </summary>
        public static string RegexReplace(this string value, string pattern, string replacement, RegexOptions regexOptions)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            var regex = new Regex(pattern, regexOptions);

            return regex.Replace(value, replacement);
        }

        /// <summary>
        ///
        /// </summary>
        public static string RegexReplace(this string value, string pattern, string replacement)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            var regex = new Regex(pattern, RegexOptions.Compiled);

            return regex.Replace(value, replacement);
        }

        /// <summary>
        ///
        /// </summary>
        public static string RenderMarkdown(this string markdown)
        {
            if (string.IsNullOrEmpty(markdown))
            {
                return string.Empty;
            }

            markdown = ReplaceMarkdownVideo(markdown);
            markdown = ReplaceMarkdownImage(markdown);
            markdown = ReplaceSmartCountryUrlInMarkdownAncors(markdown);
            markdown = ReplaceMarkdownAnchors(markdown);
            markdown = ReplaceMarkdownHeaders(markdown);
            markdown = ReplaceMarkdownHorizontalRule(markdown);
            markdown = ReplaceMarkdownTables(markdown);
            markdown = CreateParagraphs(markdown);
            markdown = RemoveEmptyParagraphs(markdown);
            markdown = ReplaceMarkdownBold(markdown);
            markdown = ReplaceMarkdownItalic(markdown);

            return markdown;
        }

        private static string ReplaceMarkdownBold(string text)
        {
            // Example: **bold**

            var regex = new Regex(@"(\*\*) (?=\S) (.+?[*_]*) (?<=\S) \1", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);

            return regex.Replace(text, @"<b>$2</b>");
        }

        private static string ReplaceMarkdownItalic(string text)
        {
            // Example: *italic*

            var regex = new Regex(@"(\*) (?=\S) (.+?) (?<=\S) \1", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);

            return regex.Replace(text, @"<i>$2</i>");
        }

        private static string ReplaceMarkdownVideo(string text)
        {
            // Example: !![attributes](video.mp4)

            var regex = new Regex(@"\!\!\[([^]]*)\]\(([^\s^\)]*)[\s\)]", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);

            return regex.Replace(text, @"<video src='$2' $1></video>");
        }

        private static string ReplaceMarkdownImage(string text)
        {
            // Example: ![alt text](image.jpg)

            var regex = new Regex(@"\!\[([^]]*)\]\(([^\s^\)]*)[\s\)]", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);

            return regex.Replace(text, @"<img src='$2' alt='$1'/>");
        }

        private static string ReplaceMarkdownAnchors(string text)
        {
            // Example: [link text](/page.html)

            var regex = new Regex(@"\[([^]]*)\]\(([^\s^\)]*)[\s\)]", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);

            return regex.Replace(text, @"<a href='$2'>$1</a>");
        }

        private static string ReplaceSmartCountryUrlInMarkdownAncors(string text)
        {
            // Example: [link text](/**/page.html)
            // Or: [link text](www.domain.com/**/page.html)

            var currentCountryUrl = HttpContext.Current?.Request?.RequestContext?.RouteData?.GetCountry();

            var regex = new Regex(@"(?<=\]\()(.*?)(\/\*\*\/)(.*?)(?=\))", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);

            var matchEvaluator = new MatchEvaluator(match =>
            {
                var countryParameter = match.Groups[2].Value
                    .Replace("/**/", $"/{currentCountryUrl}/")
                    .Replace("//", "");

                return $"{match.Groups[1].Value}{(string.IsNullOrEmpty(countryParameter) ? "/" : countryParameter)}{match.Groups[3].Value}";
            });

            return regex.Replace(text, matchEvaluator);
        }

        private static string ReplaceMarkdownHeaders(string text)
        {
            // Example: #This is a title

            var regex = new Regex(@"
                ^(\#{1,6})  # $1 = string of #'s
                [ ]*
                (.+?)       # $2 = Header text
                [ ]*
                (\n+|$)", // capture new line or end of string
                RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

            var matchEvaluator = new MatchEvaluator(match =>
            {
                var header = match.Groups[2].Value;
                var level = match.Groups[1].Value.Length;
                return string.Format("<h{1}>{0}</h{1}>\n", header, level);
            });

            return regex.Replace(text, matchEvaluator);
        }

        private static string ReplaceMarkdownHorizontalRule(string text)
        {
            // Example: ---

            text = text.Replace("---", "<hr>");

            return text;
        }

        private static string ReplaceMarkdownTables(string text)
        {
            // var regex = new Regex(@"^\|([\s\S]*?)\|(?:\s*?)(?:\z|[\r\n][^\|])", RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

            var regex = new Regex(@"^\|([\s\S]*?)\|(?:\s*?)(?:$(?![r\n])|[\r\n](?!\|))", RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

            var matchEvaluator = new MatchEvaluator(match =>
            {
                var table = match.Groups[1].Value;
                var rows = table.Split('\n').Select(x => x.Trim('|'));
                var cells = rows.Select(x => x.Split('|'));

                return $"<table>{string.Join("", cells.Select(tr => $"<tr>{string.Join("", tr.Select(td => $"<td>{td}</td>"))}</tr>"))}</table>";
            });

            return regex.Replace(text, matchEvaluator);
        }

        private static string CreateParagraphs(string text)
        {
            // Match all lines that don't start with "<", are not empty and are not the slice separator "==="

            var regex = new Regex(@"^(?!\s*<|\s*$|===)(.*)", RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

            return regex.Replace(text, @"<p>$1</p>");
        }

        private static string RemoveEmptyParagraphs(string text)
        {
            var regex = new Regex(@"<p>[\s]{0,}<\/p>", RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

            return regex.Replace(text, "");
        }

        private static string EscapeQuotes(string str)
        {
            var containsQuotedString = str.Contains("\"");

            if (!containsQuotedString)
            {
                return str;
            }

            var sb = new StringBuilder();

            foreach (var nextChar in str)
            {
                sb.Append(nextChar);

                if (nextChar == '"')
                {
                    sb.Append("\"");
                }
            }

            return sb.ToString();
        }
    }
}