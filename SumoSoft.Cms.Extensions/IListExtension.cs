﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public static class IListExtension
    {
        public static bool SequenceEqualIgnoreOrder<T>(this IList<T> list1, IList<T> list2)
        {
            return list1.Count == list2.Count && list1.All(list2.Contains);
        }

        public static bool ContainsAll<T>(this IList<T> list, IList<T> sublist)
        {
            return sublist.Intersect(list).Count() == sublist.Count;
        }

        public static bool ContainsAny<T>(this IList<T> list, params T[] args)
        {
            return list.Intersect(args).Any();
        }

        public static bool ContainsDuplicates<T>(this IList<T> e)
        {
            var set = new HashSet<T>();

            return e.Any(item => !set.Add(item));
        }

        /// <summary>
        /// Sorts an IList randomly.
        /// </summary>
        public static IList<T> OrderByRandom<T>(this IList<T> list)
        {
            return list.OrderBy(x => Guid.NewGuid().ToString()).ToList();
        }

        /// <summary>
        /// Returns the List if not null. If it is, initializes a new empty List.
        /// </summary>
        [NotNull]
        public static IList<T> AsNotNull<T>(this IList<T> list)
        {
            return list ?? new List<T>();
        }

        public static void AddIfNotNull<T>(this IList<T> list, T item)
        {
            if (item != null)
            {
                list.Add(item);
            }
        }

        public static void AddIfNotNullOrEmpty(this IList<string> list, string item)
        {
            if (!string.IsNullOrEmpty(item))
            {
                list.Add(item);
            }
        }

        public static List<T> DistinctBy<T, T1>(this IList<T> items, Func<T, T1> keySelector)
        {
            return items.GroupBy(keySelector).Select(x => x.FirstOrDefault()).ToList();
        }
    }
}