﻿namespace SumoSoft.Cms.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    ///
    /// </summary>
    public static class EnumExtension
    {
        /// <summary>
        /// Gets the Description attribute of an Enum item.
        /// </summary>
        public static string GetDescription(this Enum value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var field = value.GetType().GetField(value.ToString());

            if (field == null)
            {
                return string.Empty;
            }

            var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }

        /// <summary>
        ///
        /// </summary>
        public static List<string> GetDescriptions(this Type type)
        {
            var descriptions = new List<string>();
            var names = Enum.GetNames(type);
            foreach (var name in names)
            {
                var field = type.GetField(name);
                var customAttributes = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                descriptions.AddRange(from DescriptionAttribute description in customAttributes select description.Description);
            }
            return descriptions;
        }
    }
}
