﻿namespace SumoSoft.Cms.Extensions
{
    /// <summary>
    ///
    /// </summary>
    public static class BoolExtension
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="nullableBool"></param>
        /// <returns></returns>
        public static bool IsNullOrFalse(this bool? nullableBool)
        {
            return !nullableBool ?? true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="nullableBool"></param>
        /// <returns></returns>
        public static bool IsTrue(this bool? nullableBool)
        {
            return nullableBool ?? false;
        }
    }
}
