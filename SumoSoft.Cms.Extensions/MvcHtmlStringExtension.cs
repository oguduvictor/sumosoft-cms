﻿namespace SumoSoft.Cms.Extensions
{
    using System.Web.Mvc;

    /// <summary>
    ///
    /// </summary>
    public static class MvcHtmlStringExtension
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="mvcHtmlString"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderMarkdown(this MvcHtmlString mvcHtmlString)
        {
            return mvcHtmlString.ToString().RenderMarkdown().ToMvcHtmlString();
        }
    }
}