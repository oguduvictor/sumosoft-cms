﻿namespace SumoSoft.Cms.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web.Script.Serialization;
    using JetBrains.Annotations;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    /// <summary>
    ///
    /// </summary>
    public static class ObjectExtension
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T TryCast<T>(this object obj)
        {
            try
            {
                // --------------------------------------------------------
                // Convert.ChangeType() doesn't work with nullable Types,
                // so we have to convert them into the underlying Type.
                // --------------------------------------------------------

                var underlyingType = Nullable.GetUnderlyingType(typeof(T));

                if (underlyingType == null)
                {
                    return (T)Convert.ChangeType(obj, typeof(T));
                }
                else
                {
                    return (T)Convert.ChangeType(obj, underlyingType);
                }
            }
            catch
            {
                return default;
            }
        }

        /// <summary>
        /// Create a List containing the current object.
        /// </summary>
        public static List<T> InList<T>(this T obj)
        {
            return new List<T> { obj };
        }

        /// <summary>
        /// Returns true if the object matches any of the items used as parameters.
        /// </summary>
        public static bool EqualsToAny<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        /// <summary>
        /// Returns true if the object matches any of the items used as parameters.
        /// </summary>
        public static bool EqualsToAny<T>(this T obj, List<T> args)
        {
            return args.Contains(obj);
        }

        /// <summary>
        /// Returns the Id of the object.
        /// </summary>
        public static Guid GetId<T>(this T obj)
        {
            var property = obj.GetType().GetProperty("Id");

            if (property == null)
            {
                throw new NullReferenceException();
            }

            return new Guid(property.GetValue(obj, null).ToString());
        }

        /// <summary>
        /// Returns the Object if not null. If it is, initializes a new Object of the same type.
        /// </summary>
        [NotNull]
        public static T AsNotNull<T>(this T obj) where T : new()
        {
            return obj == null ? new T() : obj;
        }

        /// <summary>
        /// Creates a Shallow Copy of the Object.
        /// </summary>
        public static T Clone<T>(this T obj)
        {
            var inst = obj.GetType().GetMethod("MemberwiseClone", BindingFlags.Instance | BindingFlags.NonPublic);

            return (T)inst?.Invoke(obj, null);
        }

        /// <summary>
        ///
        /// </summary>
        public static string ToJson<T>(this T obj)
        {
            return new JavaScriptSerializer().Serialize(obj);
        }

        /// <summary>
        ///
        /// </summary>
        public static string ToCamelCaseJson<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}