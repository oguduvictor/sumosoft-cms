﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Extensions
{
    using FluentValidation;

    public static class IRuleBuilderExtension
    {
        public static IRuleBuilderOptions<T, string> MustNotContainScript<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Must(x => string.IsNullOrEmpty(x) || !x.ContainsCaseInsensitive("<script"))
                .WithMessage("{PropertyName} cannot contain <script> tags!");
        }
    }
}
