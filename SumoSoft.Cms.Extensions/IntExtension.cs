﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Extensions
{
    public static class IntExtension
    {
        public static int ToMinimum(this int originalValue, int minimumValue)
        {
            return originalValue > minimumValue ? originalValue : minimumValue;
        }

        /// <summary>
        /// Round to the closest interval number.
        /// </summary>
        public static int RoundOff(this int number, int interval)
        {
            var remainder = number % interval;
            number += remainder < interval / 2 ? -remainder : interval - remainder;
            return number;
        }

        /// <summary>
        /// Round down to the interval number.
        /// </summary>
        public static int RoundDown(this int number, int interval)
        {
            var remainder = number % interval;
            return number - remainder;
        }

        /// <summary>
        /// Round up to the interval number.
        /// </summary>
        public static int RoundUp(this int number, int interval)
        {
            var remainder = number % interval;
            return number == 0 ? 0 : number - remainder + interval;
        }
    }
}