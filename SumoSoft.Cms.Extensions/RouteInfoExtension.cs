﻿namespace SumoSoft.Cms.Extensions
{
    using System;
    using System.Web;
    using System.Web.Routing;

    /// <summary>
    ///
    /// </summary>
    public class RouteInfoExtension
    {
        /// <summary>
        ///
        /// </summary>
        public RouteInfoExtension(Uri uri, string applicaionPath)
        {
            this.RouteData = RouteTable.Routes.GetRouteData(new InternalHttpContext(uri, applicaionPath));
        }

        /// <summary>
        ///
        /// </summary>
        public RouteData RouteData { get; private set; }

        private class InternalHttpContext : HttpContextBase
        {
            public InternalHttpContext(Uri uri, string applicationPath)
            {
                this.Request = new InternalRequestContext(uri, applicationPath);
            }

            public override HttpRequestBase Request { get; }
        }

        private class InternalRequestContext : HttpRequestBase
        {
            private readonly string appRelativePath;

            public InternalRequestContext(Uri uri, string applicationPath)
            {
                this.PathInfo = "";
                if (string.IsNullOrEmpty(applicationPath) || !uri.AbsolutePath.StartsWith(applicationPath, StringComparison.OrdinalIgnoreCase))
                {
                    if (applicationPath != null)
                    {
                        this.appRelativePath = uri.AbsolutePath.Substring(applicationPath.Length);
                    }
                }
                else
                {
                    this.appRelativePath = uri.AbsolutePath;
                }
            }

            public override string AppRelativeCurrentExecutionFilePath => string.Concat("~", this.appRelativePath);

            public override string PathInfo { get; }
        }
    }
}