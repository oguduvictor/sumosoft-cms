﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Extensions
{
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    /// <summary>
    ///
    /// </summary>
    public static class HtmlHelperExtension
    {
        /// <summary>
        /// Tries to return a Partial View. If the View doesn't exist, returns an empty string.
        /// </summary>
        public static MvcHtmlString TryPartial(this HtmlHelper html, string name, object model = null)
        {
            var controllerContext = html.ViewContext.Controller.ControllerContext;

            var partialView = ViewEngines.Engines.FindPartialView(controllerContext, name).View;

            return partialView == null ? new MvcHtmlString(string.Empty) : html.Partial(name, model);
        }
    }
}