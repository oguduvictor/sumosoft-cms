﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Extensions
{
    using System;
    using System.Globalization;
    using System.Threading;

    public static class DoubleExtension
    {
        public static double CalculatePercentage(this double value, double percentage)
        {
            return value * percentage / 100;
        }

        /// <summary>
        /// Compares two double values with a tolerance of 0.01.
        /// </summary>
        public static bool ApproximatelyEqualsTo(this double firstValue, double secondValue)
        {
            return Math.Abs(firstValue - secondValue) < 0.01;
        }

        public static double ToMinimum(this double originalValue, double minimumValue)
        {
            return originalValue > minimumValue ? originalValue : minimumValue;
        }

        public static double ToMaximum(this double originalValue, double maximumValue)
        {
            return originalValue < maximumValue ? originalValue : maximumValue;
        }

        /// <summary>
        ///
        /// </summary>
        public static decimal ToDecimal(this double value, decimal fallbackValue = 0)
        {
            return (decimal)value;
        }

        /// <summary>
        /// Formats a double value into a currency, based on a custom languageCode.
        /// </summary>
        public static string ToPrice(this double value, string languageCode = null)
        {
            var culture = CultureInfo.GetCultureInfo(languageCode ?? Thread.CurrentThread.CurrentCulture.Name);

            return string.Format(culture, "{0:C}", value);
        }

        /// <summary>
        /// Round the decimal numbers.
        /// </summary>
        public static double Round(this double value, int decimals)
        {
            return Math.Round(value, decimals);
        }
    }
}