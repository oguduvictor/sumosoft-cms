﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using JetBrains.Annotations;

    public static class IEnumerableExtension
    {
        public static bool ContainsAll<T>(this IEnumerable<T> list, IEnumerable<T> sublist)
        {
            var enumerable = sublist as IList<T> ?? sublist.ToList();
            return enumerable.Intersect(list).Count() == enumerable.Count;
        }

        public static bool ContainsAny<T>(this IEnumerable<T> list, params T[] args)
        {
            return list.Intersect(args).Any();
        }

        public static bool ContainsDuplicates<T>(this IEnumerable<T> e)
        {
            var set = new HashSet<T>();

            return e.Any(item => !set.Add(item));
        }

        /// <summary>
        /// Sorts an IEnumerable randomly.
        /// </summary>
        public static IEnumerable<T> OrderByRandom<T>(this IEnumerable<T> list)
        {
            return list.OrderBy(x => Guid.NewGuid().ToString());
        }

        public static string ToCsv<T>(this IEnumerable<T> list, string delimiter, params Func<T, string>[] properties)
        {
            var columns = properties.Select(func => list.Select(func).ToList()).ToList();

            var stringBuilder = new StringBuilder();

            var rowsCount = columns.First().Count;

            for (var i = 0; i < rowsCount; i++)
            {
                var rowCells = columns.Select(column => column[i].ToValidCsvString());

                stringBuilder.AppendLine(string.Join(delimiter, rowCells));
            }

            return stringBuilder.ToString();
        }

        public static string ToCsv<T>(this IEnumerable<T> list, params Func<T, string>[] properties)
        {
            return list.ToCsv(",", properties);
        }

        public static bool ContainsCaseInsensitive(this IEnumerable<string> list, string item)
        {
            return list.Any(x => string.Equals(x, item, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Returns the List if not null. If it is, initializes a new empty List.
        /// </summary>
        [NotNull]
        public static IEnumerable<T> AsNotNull<T>(this IEnumerable<T> list)
        {
            return list ?? new List<T>();
        }
    }
}
