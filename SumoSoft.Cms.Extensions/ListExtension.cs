﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    public static class ListExtension
    {
        public static void AddRangeIfNotNull<T>(this List<T> list, IEnumerable<T> items)
        {
            if (items == null)
            {
                return;
            }

            list.AddRange(items.Where(x => x != null));
        }
    }
}
