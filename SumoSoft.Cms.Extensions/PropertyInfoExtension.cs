﻿namespace SumoSoft.Cms.Extensions
{
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public static class PropertyInfoExtension
    {
        /// <summary>
        /// Get the Display Attribute of a PropertyInfo.
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        [CanBeNull]
        public static DisplayAttribute GetDisplayAttribute(this PropertyInfo propertyInfo)
        {
            return propertyInfo.GetCustomAttribute(typeof(DisplayAttribute), false) as DisplayAttribute;
        }
    }
}
