﻿namespace SumoSoft.Cms.Extensions
{
    using System.Web.Routing;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public static class Route
    {
        /// <summary>
        /// Return the country url to lowercase or string.Empty.
        /// </summary>
        [NotNull]
        public static string GetCountry(this RouteData routeData)
        {
            return routeData.Values["country"]?.ToString().ToLowerInvariant() ?? string.Empty;
        }

        /// <summary>
        /// Return the controller name to lowercase or string.Empty.
        /// </summary>
        [NotNull]
        public static string GetController(this RouteData routeData)
        {
            return routeData.Values["controller"]?.ToString().ToLowerInvariant() ?? string.Empty;
        }

        /// <summary>
        /// Return the action name to lowercase or string.Empty.
        /// </summary>
        [NotNull]
        public static string GetAction(this RouteData routeData)
        {
            return routeData.Values["action"]?.ToString().ToLowerInvariant() ?? string.Empty;
        }

        /// <summary>
        /// Return the url name to lowercase or string.Empty.
        /// </summary>
        [NotNull]
        public static string GetUrl(this RouteData routeData)
        {
            return routeData.Values["url"]?.ToString().ToLowerInvariant() ?? string.Empty;
        }

        /// <summary>
        /// Return the area name to lowercase or string.Empty.
        /// </summary>
        /// <param name="routeData"></param>
        /// <returns>string.</returns>
        [NotNull]
        public static string GetArea(this RouteData routeData)
        {
            return routeData.DataTokens["area"]?.ToString().ToLowerInvariant() ?? string.Empty;
        }
    }
}