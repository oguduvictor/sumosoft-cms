﻿namespace SumoSoft.Cms.RequestDbContextServices
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    /// <inheritdoc />
    public class RequestDbContextService : IRequestDbContextService
    {
        /// <inheritdoc />

        [CanBeNull]
        public virtual CmsDbContext Get()
        {
            return HttpContext.Current?.Items["RequestDbContext"] as CmsDbContext;
        }

        /// <inheritdoc />
        public virtual void Set()
        {
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));

            if (routeData?.RouteHandler is MvcRouteHandler)
            {
                HttpContext.Current.Items["RequestDbContext"] = new CmsDbContext();
            }
        }

        /// <inheritdoc />
        public virtual void Dispose()
        {
            this.Get()?.Dispose();
        }

        /// <inheritdoc />
        public virtual void SaveChanges()
        {
            this.Get()?.SaveChanges();
        }
    }
}
