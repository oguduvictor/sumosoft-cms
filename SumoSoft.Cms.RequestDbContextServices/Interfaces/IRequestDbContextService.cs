﻿namespace SumoSoft.Cms.RequestDbContextServices.Interfaces
{
    using SumoSoft.Cms.DataAccess;

    /// <summary>
    ///
    /// </summary>
    public interface IRequestDbContextService
    {
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        CmsDbContext Get();

        /// <summary>
        ///
        /// </summary>
        void Set();

        /// <summary>
        ///
        /// </summary>
        void Dispose();

        /// <summary>
        ///
        /// </summary>
        void SaveChanges();
    }
}