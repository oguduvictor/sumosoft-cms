﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Dto
{
    public class FormError
    {
        public FormError()
        {
        }

        public FormError(string message)
        {
            this.Message = message;
        }

        public FormError(string propertyName, string message)
        {
            this.PropertyName = propertyName;

            this.Message = message;
        }

        public string PropertyName { get; set; }

        public string Message { get; set; }
    }
}
