﻿
#pragma warning disable 1591

namespace SumoSoft.Cms.Dto
{
    using JetBrains.Annotations;

    public class AuthenticateUserDto
    {
        [CanBeNull]
        public string Email { get; set; }

        [CanBeNull]
        public string Password { get; set; }
    }
}