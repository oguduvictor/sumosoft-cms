﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Dto
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ResizeFileDto
    {
        public IEnumerable<string> LocalPaths { get; set; }

        public int Width { get; set; }

        public int Compression { get; set; }

        [CanBeNull]
        public string Destination { get; set; }

        public bool KeepOriginal { get; set; }
    }
}