﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Dto
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public class FormResponse
    {
        public bool IsValid => !this.Errors.Any();

        [CanBeNull]
        public string SuccessMessage { get; set; }

        [NotNull]
        public List<FormError> Errors { get; set; } = new List<FormError>();

        [CanBeNull]
        public string GetErrorMessageFor(string propertyName)
        {
            return this.Errors.FirstOrDefault(x => x.PropertyName == propertyName)?.Message;
        }

        [CanBeNull]
        public string RedirectUrl { get; set; }

        public void AddError(string message)
        {
            this.Errors.Add(new FormError(message));
        }
    }

    public class FormResponse<T> : FormResponse
    {
        [CanBeNull]
        public T Target { get; set; }
    }
}