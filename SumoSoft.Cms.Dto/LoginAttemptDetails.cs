﻿namespace SumoSoft.Cms.Dto
{
    using System;

    public class LoginAttemptDetails
    {
        public DateTime? LockOutTime { get; set; }

        public int Count { get; set; }

        public bool IsLocked()
        {
            return this.LockOutTime != null && DateTime.Now < this.LockOutTime;
        }
    }
}