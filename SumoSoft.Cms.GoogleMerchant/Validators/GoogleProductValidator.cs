﻿#pragma warning disable 1591

namespace SumoSoft.Cms.GoogleMerchant.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.GoogleMerchant.Dtos;

    public class GoogleProductValidator : AbstractValidator<GoogleProduct>
    {
        public GoogleProductValidator()
        {
            this.RuleFor(x => x.Id).NotEmpty();
            this.RuleFor(x => x.Id.Length).LessThanOrEqualTo(13);

            this.RuleFor(x => x.OfferId).NotEmpty();
            this.RuleFor(x => x.OfferId.Length).LessThanOrEqualTo(13);

            this.RuleFor(x => x.ItemGroupId).NotEmpty();
            this.RuleFor(x => x.ItemGroupId.Length).LessThanOrEqualTo(13);

            this.RuleFor(x => x.Title).NotEmpty();
            this.RuleFor(x => x.Title.Length).LessThanOrEqualTo(150);

            this.RuleFor(x => x.Description).NotEmpty();
            this.RuleFor(x => x.Description.Length).LessThanOrEqualTo(5000);

            this.RuleFor(x => x.GoogleProductCategory).NotEmpty();

            this.RuleFor(x => x.Link).NotEmpty();

            this.RuleFor(x => x.ImageLink).NotEmpty();

            this.RuleFor(x => x.Condition).NotEmpty();
            this.RuleFor(x => x.Condition)
                .Must(x => x.EqualsToAny("new", "refurbished", "used"))
                .WithMessage("Possible values: new | refurbished | used");

            this.RuleFor(x => x.Availability).NotEmpty();
            this.RuleFor(x => x.Availability)
                .Must(x => x.EqualsToAny("in stock", "out of stock", "preorder"))
                .WithMessage("Possible values: in stock | out of stock | preorder");

            this.RuleFor(x => x.Price).NotEmpty();

            this.RuleFor(x => x.SalePrice).NotEmpty();

            this.RuleFor(x => x.Brand).NotEmpty();

            this.RuleFor(x => x.Mpn).NotEmpty();

            this.RuleFor(x => x.AgeGroup).NotEmpty();

            this.RuleFor(x => x.Gender).NotEmpty();
            this.RuleFor(x => x.Gender)
                .Must(x => x.EqualsToAny("male", "female", "unisex"))
                .WithMessage("Possible values: male | female | unisex");
        }
    }
}