﻿namespace SumoSoft.Cms.GoogleMerchant
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.GoogleMerchant.Dtos;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class CmsGoogleMerchantService
    {
        private readonly string googleMerchantCategoryAttributeName;
        private readonly string googleMerchantColorVariantName;
        private readonly string googleMerchantSizeVariantName;
        private readonly string googleMerchantImageName;

        /// <summary>
        ///
        /// </summary>
        public CmsGoogleMerchantService(
            IUtilityService utilityService)
        {
            this.googleMerchantCategoryAttributeName = utilityService.GetAppSetting("GoogleMerchantCategoryAttributeName");
            this.googleMerchantColorVariantName = utilityService.GetAppSetting("GoogleMerchantColorVariantName");
            this.googleMerchantSizeVariantName = utilityService.GetAppSetting("GoogleMerchantSizeVariantName");
            this.googleMerchantImageName = utilityService.GetAppSetting("GoogleMerchantImageName");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="productStockUnit"></param>
        /// <param name="currentCountry"></param>
        /// <param name="additionalVariantOptions"></param>
        /// <returns></returns>
        public GoogleProduct MapGoogleProduct(CmsDbContext dbContext, ProductStockUnit productStockUnit, Country currentCountry, IEnumerable<VariantOption> additionalVariantOptions = null)
        {
            var variantOptions = new List<VariantOption>();

            variantOptions.AddRange(productStockUnit.VariantOptions);
            variantOptions.AddRange(additionalVariantOptions.AsNotNull());

            variantOptions = variantOptions.DistinctBy(x => x.Id);

            var product = productStockUnit.Product.AsNotNull();
            var countryTaxes = currentCountry.Taxes;
            var languageCode = currentCountry.LanguageCode;

            return new GoogleProduct
            {
                Id = CreateId(productStockUnit.Id),
                OfferId = CreateId(productStockUnit.Id),
                Mpn = CreateId(productStockUnit.Id),
                ItemGroupId = CreateId(product.Id),
                GoogleProductCategory = product.GetProductAttributeByName(this.googleMerchantCategoryAttributeName)?.ContentSectionValue?.GetLocalizedContent(languageCode),
                Title = product.GetLocalizedTitle(languageCode),
                Description = product.GetLocalizedDescription(languageCode),
                Price = product.GetTotalAfterTaxWithoutSalePrice(dbContext, countryTaxes, variantOptions, languageCode),
                SalePrice = product.GetTotalAfterTax(dbContext, countryTaxes, variantOptions, languageCode),
                Availability = productStockUnit.EnablePreorder ? "preorder" : productStockUnit.Stock > 0 ? "in stock" : "out of stock",
                Color = variantOptions.FirstOrDefault(x => x.Variant?.Name == this.googleMerchantColorVariantName)?.GetLocalizedTitle(languageCode),
                Size = variantOptions.FirstOrDefault(x => x.Variant?.Name == this.googleMerchantSizeVariantName)?.GetLocalizedTitle(languageCode),
                ImageLink = product.GetProductImageKit(variantOptions)?.GetProductImageByName(this.googleMerchantImageName)?.Url,
                Condition = "new",
                AgeGroup = "adult"
            };
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="currentCountry"></param>
        /// <param name="googleProducts"></param>
        /// <returns></returns>
        public MemoryStream CreateFeed(CmsDbContext dbContext, Country currentCountry, List<GoogleProduct> googleProducts)
        {
            var rows = new List<string>();

            var csvHeadersList = new List<string>
            {
                "id",
                "offer id",
                "item group id",
                "title",
                "description",
                "google product category",
                "link",
                "image link",
                "condition",
                "availability",
                "price",
                "sale price",
                "brand",
                "mpn",
                "age group",
                "gender",
                "color",
                "size",
                "shipping",
                "custom label 0"
            };

            // ----------------------------------------------------------------------
            // Add Custom label 1 if needed
            // ----------------------------------------------------------------------

            if (googleProducts.Any(x => !string.IsNullOrWhiteSpace(x.CustomLabel1)))
            {
                csvHeadersList.Add("custom label 1");
            }

            // ----------------------------------------------------------------------
            // Add Custom label 2 if needed
            // ----------------------------------------------------------------------

            if (googleProducts.Any(x => !string.IsNullOrWhiteSpace(x.CustomLabel2)))
            {
                csvHeadersList.Add("custom label 2");
            }

            // ----------------------------------------------------------------------
            // Add Custom label 3 if needed
            // ----------------------------------------------------------------------

            if (googleProducts.Any(x => !string.IsNullOrWhiteSpace(x.CustomLabel3)))
            {
                csvHeadersList.Add("custom label 3");
            }

            rows.Add(string.Join("\t", csvHeadersList) + Environment.NewLine);

            var currencySymbol = currentCountry.GetIso4217CurrencySymbol();
            var regionName = currentCountry.GetRegionInfo()?.TwoLetterISORegionName;

            googleProducts = googleProducts.DistinctBy(x => x.Id);

            foreach (var googleProduct in googleProducts)
            {
                var row = string.Join("\t", new List<string>
                {
                    googleProduct.Id.ToValidCsvString(),
                    googleProduct.OfferId.ToValidCsvString(),
                    googleProduct.ItemGroupId.ToValidCsvString(),
                    googleProduct.Title.ToValidCsvString(),
                    googleProduct.Description.ToValidCsvString(),
                    googleProduct.GoogleProductCategory.ToValidCsvString(),
                    googleProduct.Link.ToValidCsvString(),
                    googleProduct.ImageLink.ToValidCsvString(),
                    googleProduct.Condition.ToValidCsvString(),
                    googleProduct.Availability.ToValidCsvString(),
                    GetFormattedPrice(currencySymbol, googleProduct.Price),
                    GetFormattedPrice(currencySymbol, googleProduct.SalePrice),
                    googleProduct.Brand.ToValidCsvString(),
                    googleProduct.Mpn.ToValidCsvString(),
                    googleProduct.AgeGroup.ToValidCsvString(),
                    googleProduct.Gender.ToValidCsvString(),
                    googleProduct.Color?.Replace(",", "").ToValidCsvString(),
                    googleProduct.Size.ToValidCsvString(),
                    $"{regionName}:::{GetFormattedPrice(currencySymbol, googleProduct.ShippingPrice)}",
                    googleProduct.CustomLabel0.ToValidCsvString()
                });

                // ----------------------------------------------------------------------
                // Add Custom label 1 if needed
                // ----------------------------------------------------------------------

                if (!string.IsNullOrWhiteSpace(googleProduct.CustomLabel1))
                {
                    row = $"{row}\t{googleProduct.CustomLabel1.ToValidCsvString()}";
                }

                // ----------------------------------------------------------------------
                // Add Custom label 2 if needed
                // ----------------------------------------------------------------------

                if (!string.IsNullOrWhiteSpace(googleProduct.CustomLabel2))
                {
                    row = $"{row}\t{googleProduct.CustomLabel2.ToValidCsvString()}";
                }
                // ----------------------------------------------------------------------
                // Add Custom label 3 if needed
                // ----------------------------------------------------------------------

                if (!string.IsNullOrWhiteSpace(googleProduct.CustomLabel3))
                {
                    row = $"{row}\t{googleProduct.CustomLabel3.ToValidCsvString()}";
                }

                rows.Add(row);
            }

            var csvString = string.Join(Environment.NewLine, rows);

            var csvBytes = new UTF8Encoding().GetBytes(csvString);

            return new MemoryStream(csvBytes);

        }

        private static string CreateId(Guid id)
        {
            return id.ToString().Replace("-", "").Shorten(10);
        }

        /// <summary>
        /// Format: 100.00 USD.
        /// </summary>
        private static string GetFormattedPrice(string currencySymbol, double price)
        {
            return $"{price.Round(0):0.00} {currencySymbol}";
        }
    }
}
