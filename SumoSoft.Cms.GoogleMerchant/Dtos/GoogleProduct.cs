﻿namespace SumoSoft.Cms.GoogleMerchant.Dtos
{
    using JetBrains.Annotations;

    /// <summary>
    /// Data specs: https://support.google.com/merchants/answer/7052112?hl=en-GB.
    /// </summary>
    public class GoogleProduct
    {
        /// <summary>
        /// Max 13 chars.
        /// </summary>
        [CanBeNull]
        public string Id { get; set; }

        /// <summary>
        /// Max 13 chars.
        /// </summary>
        [CanBeNull]
        public string OfferId { get; set; }

        /// <summary>
        /// Max 13 chars.
        /// </summary>
        [CanBeNull]
        public string ItemGroupId { get; set; }

        /// <summary>
        /// Max 150 characters
        /// Format: Classic Suit Fine PlainWeave.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Max 5,000 characters.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Required, include only 1 and most relevant category
        /// Example: Clothing Accessories > Clothing > Outerwear > Coats  Jackets.
        /// </summary>
        [CanBeNull]
        public string GoogleProductCategory { get; set; }

        /// <summary>
        /// Product landing page.
        /// </summary>
        [CanBeNull]
        public string Link { get; set; }

        /// <summary>
        /// URL of the product's main image.
        /// </summary>
        [CanBeNull]
        public string ImageLink { get; set; }

        /// <summary>
        /// Possible values: new | refurbished | used.
        /// </summary>
        [CanBeNull]
        public string Condition { get; set; }

        /// <summary>
        /// Possible values: in stock | out of stock | preorder.
        /// </summary>
        [CanBeNull]
        public string Availability { get; set; }

        /// <summary>
        /// Products original price (ISO 4217)
        /// Format: 2000.00 GBP.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Products sale price if on sale (ISO 4217)
        /// Format: 1500.12 USD.
        /// </summary>
        public double SalePrice { get; set; }

        /// <summary>
        /// Product brand name
        /// Format: Archibald.
        /// </summary>
        [CanBeNull]
        public string Brand { get; set; }

        /// <summary>
        /// Manufacturers Part Number.
        /// </summary>
        [CanBeNull]
        public string Mpn { get; set; }

        /// <summary>
        /// Possible values: adult | ?.
        /// </summary>
        [CanBeNull]
        public string AgeGroup { get; set; }

        /// <summary>
        /// Possible values: male | female | unisex.
        /// </summary>
        [CanBeNull]
        public string Gender { get; set; }

        /// <summary>
        /// Product color
        /// Sample values: Green | Red | Black.
        /// </summary>
        [CanBeNull]
        public string Color { get; set; }

        /// <summary>
        /// Required for all clothing
        /// Sample values: S | L | XL | XXL.
        /// </summary>
        [CanBeNull]
        public string Size { get; set; }

        /// <summary>
        /// Required: Uk, US, Australia, Czechia, France, Germany, Israel, Italy, Netherlands, Spain and Switzerland.
        /// </summary>
        public double ShippingPrice { get; set; }

        /// <summary>
        /// Label assigned to product to help shopping campaigns and bidding
        /// Sample values: Sale | Clearance | Seasonal.
        /// </summary>
        [CanBeNull]
        public string CustomLabel0 { get; set; }

        /// <summary>
        /// "For" Product Attribute
        /// Sample values: Him | Her | Everyone.
        /// </summary>
        [CanBeNull]
        public string CustomLabel1 { get; set; }

        /// <summary>
        /// "Made In" Attribute for product.
        /// </summary>
        [CanBeNull]
        public string CustomLabel2 { get; set; }

        /// <summary>
        /// "Town" Attribute for product
        /// Sample values: Sale | Clearance | Seasonal.
        /// </summary>
        [CanBeNull]
        public string CustomLabel3 { get; set; }
    }
}
