﻿export namespace Constants {

    export const string_requiredFieldPlaceholder = "* required";

    export const tooltip_name = "The Name is the unique identifier of the entity. It should be written in camelCase, using only numbers and letters.";
    export const tooltip_url = "The Url is used to represent an entity in the URL. ";
    export const tooltip_code = "The Code is used uniquely identify a Product from an external source, like a third party Stock Management Software.";
    export const tooltip_taxCode = "The Tax Code is sed to identify a Product type for tax purposes.";

    export const alert_empty_child = "Please select a child";
    export const alert_empty_parent = "Please select a parent";
    export const alert_multiple_editing_productStockUnit = "Warning: Functionality is limited as another user is currently editing these Stock Units.";
    export const alert_saveSuccess = "Your data has been successfully updated";
    export const alert_unsaved_changes = "You have some unsaved changes. If you leave the page now without saving, all your changes will be lost. Are you sure you wish to proceed?";

    export const flag_icon_class = "flag-icon-";

    export const hub_Country = "countryHub";
    export const hub_Product = "productHub";

    export const hubMethod_Country_BatchUpdateAllPrices = "batchUpdateAllPrices";
    export const hubMethod_Country_BatchUpdateAllPricesUndo = "batchUpdateAllPricesUndo";
    export const hubMethod_Country_BatchUpdateProductStockUnitSalePrices = "batchUpdateProductStockUnitSalePrices";
    export const hubMethod_Country_BatchUpdateProductStockUnitSalePricesUndo = "batchUpdateProductStockUnitSalePricesUndo";
    export const hubMethod_Country_BatchVoidProductStockUnitSalePrices = "batchVoidProductStockUnitSalePrices";
    export const hubMethod_Country_BatchUpdateProductStockUnitMembershipSalePrices = "batchUpdateProductStockUnitMembershipSalePrices";
    export const hubMethod_Country_BatchUpdateProductStockUnitMembershipSalePricesUndo = "batchUpdateProductStockUnitMembershipSalePricesUndo";
    export const hubMethod_Country_BatchVoidProductStockUnitMembershipSalePrices = "batchVoidProductStockUnitMembershipSalePrices";
    export const hubMethod_Product_BeginEditingProductStockUnit = "beginEditingProductStockUnit";
    export const hubMethod_Product_OnMultipleEditingProductStockUnit = "onMultipleEditingProductStockUnit";
    export const hubMethod_Product_SaveEditingProductStockUnit = "saveEditingProductStockUnit";

    export const hubEvent_Country_FinishBatchUpdateAllPrices = "finishBatchUpdateAllPrices";
    export const hubEvent_Country_FinishBatchUpdateAllPricesUndo = "finishBatchUpdateAllPricesUndo";
    export const hubEvent_Country_FinishBatchUpdateProductStockUnitSalePrices = "finishBatchUpdateProductStockUnitSalePrices";
    export const hubEvent_Country_FinishBatchUpdateProductStockUnitSalePricesUndo = "finishBatchUpdateProductStockUnitSalePricesUndo";
    export const hubEvent_Country_FinishBatchUpdateProductStockUnitMembershipSalePrices = "finishBatchUpdateProductStockUnitMembershipSalePrices";
    export const hubEvent_Country_FinishBatchUpdateProductStockUnitMembershipSalePricesUndo = "finishBatchUpdateProductStockUnitMembershipSalePricesUndo";
    export const hubEvent_Country_FinishBatchVoidProductStockUnitSalePrices = "finishBatchVoidProductStockUnitSalePrices";
    export const hubEvent_Country_FinishBatchVoidProductStockUnitMembershipSalePrices = "finishBatchVoidProductStockUnitMembershipSalePrices";
    export const hubEvent_Product_QueryIfEditingProductStockUnit = "queryIfEditingProductStockUnit";
    export const hubEvent_Product_ShowMultipleEditingProductStockUnitWarning = "showMultipleEditingProductStockUnitWarning";
    export const hubEvent_Product_RefreshEditingProductStockUnitData = "refreshEditingProductStockUnitData";

    export const mail_To_Type_Mailing_List = "mailing-list";
    export const mail_To_Type_Single_Users = "single-users";

    export const media_backups_folderName = "_backups";

    export const url_Content_Img_Loading = "/Areas/Admin/Content/img/loading.gif";

    export const url_ContentSection_Index = "/Admin/ContentSection";
    export const url_ContentSection_IndexJson = "/Admin/ContentSection/IndexJson";
    export const url_ContentSection_Delete = "/Admin/ContentSection/Delete";
    export const url_ContentSection_GetContentVersions = "/Admin/ContentSection/GetContentVersions";
    export const url_ContentSection_Restore = "/Admin/ContentSection/Restore";
    export const url_ContentSection_Save = "/Admin/ContentSection/Save";
    export const url_ContentSection_SimilarityMatch = "/Admin/ContentSection/SimilarityMatch";
    export const url_ContentSection_ExportCsv = "/Admin/ContentSection/ExportCsv";

    export const url_Country_Index = "/Admin/Country/Index";
    export const url_Country_IndexJson = "/Admin/Country/IndexJson";
    export const url_Country_Edit = "/Admin/Country/Edit?countryId=";
    export const url_Country_EditJson = "/Admin/Country/EditJson";
    export const url_Country_AddOrUpdate = "/Admin/Country/AddOrUpdate";
    export const url_Country_Delete = "/Admin/Country/Delete";
    export const url_Country_SetDefaultCountry = "/Admin/Country/SetDefaultCountry";

    export const url_Attribute = "/Admin/Attribute";
    export const url_Attribute_Index = "/Admin/Attribute/Index";
    export const url_Attribute_IndexJson = "/Admin/Attribute/IndexJson";
    export const url_Attribute_Edit = "/Admin/Attribute/Edit?attributeId=";
    export const url_Attribute_EditJson = "/Admin/Attribute/EditJson";
    export const url_Attribute_Delete = "/Admin/Attribute/Delete";
    export const url_Attribute_AddOrUpdate = "/Admin/Attribute/AddOrUpdate";
    export const url_Attribute_UpdateSortOrder = "/Admin/Attribute/UpdateSortOrder";

    export const url_Cart_Index = "/Admin/Cart/Index";
    export const url_Cart_IndexJson = "/Admin/Cart/IndexJson";
    export const url_Cart_Edit = "/Admin/Cart/Edit?cartId=";
    export const url_Cart_EditJson = "/Admin/Cart/EditJson";
    export const url_Cart_CreateOrder = "/Admin/Cart/CreateOrder";
    export const url_Cart_SendEmail = "/Admin/Cart/SendEmail";

    export const url_Category_Index = "/Admin/Category/Index";
    export const url_Category_IndexJson = "/Admin/Category/IndexJson";
    export const url_Category_Edit = "/Admin/Category/Edit?categoryId=";
    export const url_Category_EditJson = "/Admin/Category/EditJson";
    export const url_Category_GetAllProductsJson = "/Admin/Category/GetAllProductsJson";
    export const url_Category_GetAllProductImageKitsJson = "/Admin/Category/GetAllProductImageKitsJson";
    export const url_Category_GetAllProductStockUnitsJson = "/Admin/Category/GetAllProductStockUnitsJson";
    export const url_Category_AddOrUpdate = "/Admin/Category/AddOrUpdate";
    export const url_Category_Delete = "/Admin/Category/Delete";
    export const url_Category_UpdateHierarchy = "/Admin/Category/UpdateHierarchy";

    export const url_Coupon_Index = "/Admin/Coupon/Index";
    export const url_Coupon_IndexJson = "/Admin/Coupon/IndexJson";
    export const url_Coupon_Edit = "/Admin/Coupon/Edit?couponId=";
    export const url_Coupon_EditJson = "/Admin/Coupon/EditJson";
    export const url_Coupon_DeleteJson = "/Admin/Coupon/DeleteJson";
    export const url_Coupon_CleanDatabase = "/Admin/Coupon/CleanDatabase";
    export const url_Coupon_AddOrUpdate = "/Admin/Coupon/AddOrUpdate";

    export const url_Order_Index = "/Admin/Order/Index";
    export const url_Order_IndexJson = "/Admin/Order/IndexJson";
    export const url_Order_Edit = "/Admin/Order/Edit?orderId=";
    export const url_Order_EditJson = "/Admin/Order/EditJson";
    export const url_Order_RestoreStock = "/Admin/Order/RestoreStock";
    export const url_Order_SendEmail = "/Admin/Order/SendEmail";
    export const url_Order_Update = "/Admin/Order/Update";

    export const url_Product_Index = "/Admin/Product/Index";
    export const url_Product_IndexJson = "/Admin/Product/IndexJson";
    export const url_Product_Edit = "/Admin/Product/Edit?productId=";
    export const url_Product_EditJson = "/Admin/Product/EditJson";
    export const url_Product_AddOrUpdateProduct = "/Admin/Product/AddOrUpdateProduct";
    export const url_Product_Delete = "/Admin/Product/Delete";
    export const url_Product_Duplicate = "/Admin/Product/Duplicate";
    export const url_Product_ImportProducts = "/Admin/Product/ImportProducts";

    export const url_Product_EditProductVariants = "/Admin/Product/EditProductVariants?productId=";
    export const url_Product_EditProductVariantsJson = "/Admin/Product/EditProductVariantsJson";
    export const url_Product_AddOrupdateProductVariants = "/Admin/Product/AddOrupdateProductVariants";

    export const url_Product_EditProductAttributes = "/Admin/Product/EditProductAttributes?productId=";
    export const url_Product_EditProductAttributesJson = "/Admin/Product/EditProductAttributesJson";
    export const url_Product_AddOrUpdateProductAttributes = "/Admin/Product/AddOrUpdateProductAttributes";
    export const url_Product_AddContentSection = "/Admin/Product/AddContentSection";

    export const url_Product_EditProductImageKits = "/Admin/Product/EditProductImageKits?productId=";
    export const url_Product_EditProductImageKitsJson = "/Admin/Product/EditProductImageKitsJson";
    export const url_Product_AddorUpdateProductImageKits = "/Admin/Product/AddOrUpdateProductImageKits";

    export const url_Product_EditProductStockUnits = "/Admin/Product/EditProductStockUnits?productId=";
    export const url_Product_EditProductStockUnitsJson = "/Admin/Product/EditProductStockUnitsJson";
    export const url_Product_AddOrUpdateProductStockUnits = "/Admin/Product/AddOrUpdateProductStockUnits";


    export const url_ShippingBox_Index = "/Admin/ShippingBox/Index";
    export const url_ShippingBox_IndexJson = "/Admin/ShippingBox/IndexJson";
    export const url_ShippingBox_Edit = "/Admin/ShippingBox/Edit?shippingBoxId=";
    export const url_ShippingBox_Duplicate = "/Admin/ShippingBox/Duplicate";
    export const url_ShippingBox_EditJson = "/Admin/ShippingBox/EditJson";
    export const url_ShippingBox_GetAllProductsJson = "/Admin/ShippingBox/GetAllProductsJson";
    export const url_ShippingBox_AddOrUpdate = "/Admin/ShippingBox/AddOrUpdate";
    export const url_ShippingBox_Delete = "/Admin/ShippingBox/Delete";
    export const url_ShippingBox_UpdateSortOrder = "/Admin/ShippingBox/UpdateSortOrder";

    export const url_Variant = "/Admin/Variant";
    export const url_Variant_IndexJson = "/Admin/Variant/IndexJson";
    export const url_Variant_Edit = "/Admin/Variant/Edit?variantId=";
    export const url_Variant_EditJson = "/Admin/Variant/EditJson";
    export const url_Variant_AddOrUpdate = "/Admin/Variant/AddOrUpdate";
    export const url_Variant_Delete = "/Admin/Variant/Delete";
    export const url_Variant_UpdateSortOrder = "/Admin/Variant/UpdateSortOrder";

    export const url_Variant_EditVariantOption = "/Admin/Variant/EditVariantOption?variantId=";
    export const url_Variant_EditVariantOptionJson = "/Admin/Variant/EditVariantOptionJson";
    export const url_Variant_AddOrUpdateVariantOption = "/Admin/Variant/AddOrUpdateVariantOption";
    export const url_Variant_DeleteVariantOption = "/Admin/Variant/DeleteVariantOption";

    export const url_Email_Index = "/Admin/Email";
    export const url_Email_IndexJson = "/Admin/Email/IndexJson";
    export const url_Email_Edit = "/Admin/Email/Edit?id=";
    export const url_Email_EditJson = "/Admin/Email/EditJson";
    export const url_Email_AddOrUpdate = "/Admin/Email/AddOrUpdate";
    export const url_Email_AddContentSection = "/Admin/Email/AddContentSection";
    export const url_Email_Delete = "/Admin/Email/Delete";
    export const url_Email_SendEmailToUsers = "/Admin/Email/SendEmailToUsers";
    export const url_Email_SendEmailToMailingList = "/Admin/Email/SendEmailToMailingList";
    export const url_Email_ViewInBrowser = "/Admin/Email/ViewInBrowser?id=";
    export const url_Email_AllCountries = "/Admin/Email/GetAllCountriesJson";
    export const url_Email_AllMailingLists = "/Admin/Email/GetAllMailingListsJson";
    export const url_Email_AllUsers = "/Admin/Email/GetAllUsersJson";

    export const url_MailingList_AddOrUpdate = "/Admin/MailingList/AddOrUpdate";
    export const url_MailingList_Delete = "/Admin/MailingList/Delete";
    export const url_MailingList_Duplicate = "/Admin/MailingList/Duplicate";
    export const url_MailingList_Edit = "/Admin/MailingList/Edit?id=";
    export const url_MailingList_EditJson = "/Admin/MailingList/EditJson";
    export const url_MailingList_Export = "/Admin/MailingList/Export";
    export const url_MailingList_Index = "/Admin/MailingList";
    export const url_MailingList_IndexJson = "/Admin/MailingList/IndexJson";
    export const url_MailingList_UploadImportEmails = "/Admin/MailingList/ImportEmails";
    export const url_MailingList_UpdateSortOrder = "/Admin/MailingList/UpdateSortOrder";
    
    export const url_Media_IndexJson = "/Admin/Media/IndexJson";
    export const url_Media_Index = "/Admin/Media/Index?localPath=";
    export const url_Media_NewDirectoryForm = "/Admin/Media/NewDirectoryForm";
    export const url_Media = "/Admin/Media?localPath=";
    export const url_Media_Backup = "/Admin/Media/Backup";
    export const url_Media_DeleteDirectoryForm = "/Admin/Media/DeleteDirectoryForm";
    export const url_Media_RenameFileForm = "/Admin/Media/RenameFileForm";
    export const url_Media_BatchRenameFiles = "/Admin/Media/BatchRenameFiles";
    export const url_Media_ResizeFilesForm = "/Admin/Media/ResizeFilesForm";
    export const url_Media_DeleteFilesForm = "/Admin/Media/DeleteFilesForm";
    export const url_Media_UploadFileForm = "/Admin/Media/UploadFileForm";

    export const url_Posts_Index = "/Admin/Posts/Index";
    export const url_Posts_IndexJson = "/Admin/Posts/IndexJson";
    export const url_Posts_Edit = "/Admin/Posts/Edit?postId=";
    export const url_Posts_EditJson = "/Admin/Posts/EditJson";
    export const url_Posts_AddOrUpdate = "/Admin/Posts/AddOrUpdate";
    export const url_Posts_Delete = "/Admin/Posts/Delete";

    export const url_Setting_Index = "/Admin/Setting/Index";
    export const url_Setting_IndexJson = "/Admin/Setting/IndexJson";
    export const url_Setting_Edit = "/Admin/Setting/Edit";
    export const url_Setting_EditJson = "/Admin/Setting/EditJson";
    export const url_Setting_AddOrUpdate = "/Admin/Setting/AddOrUpdate";
    export const url_Setting_FlushCache = "/Admin/Setting/FlushCache";

    export const url_Seo_Index = "/Admin/Seo/Index";
    export const url_Seo_IndexJson = "/Admin/Seo/IndexJson";
    export const url_Seo_Edit = "/Admin/Seo/Edit?id=";
    export const url_Seo_EditJson = "/Admin/Seo/EditJson";
    export const url_Seo_AddOrUpdate = "/Admin/Seo/AddOrUpdate";
    export const url_Seo_Delete = "/Admin/Seo/Delete";
    export const url_Seo_Create = "/Admin/Seo/Delete";

    export const url_User_Index = "/Admin/User/Index";
    export const url_User_IndexJson = "/Admin/User/IndexJson";
    export const url_User_Edit = "/Admin/User/Edit?userId=";
    export const url_User_EditJson = "/Admin/User/EditJson";
    export const url_User_AddOrUpdate = "/Admin/User/AddOrUpdate";
    export const url_User_Delete = "/Admin/User/Delete";
    export const url_User_Export = "/Admin/User/Export";
    export const url_User_ChangePassword = "/Admin/User/ChangePassword";
    export const url_User_Roles = "/Admin/User/Roles";
    export const url_User_RolesJson = "/Admin/User/RolesJson";
    export const url_User_EditUserRole = "/Admin/User/EditUserRole?userRoleId=";
    export const url_User_EditUserRoleJson = "/Admin/User/EditUserRoleJson";
    export const url_User_AddOrUpdateUserRole = "/Admin/User/AddOrUpdateUserRole";
    export const url_User_DeleteUserRole = "/Admin/User/DeleteUserRole";
    export const url_User_AddOrUpdateUserCredit = "/Admin/User/AddOrUpdateUserCredit";
    export const url_User_AddOrUpdateUserCredits = "/Admin/User/AddOrUpdateUserCredits";
    export const url_User_DeleteUserCredit = "/Admin/User/DeleteUserCredit";

    export const url_BlogCategories_Index = "/Admin/BlogCategory/Index";
    export const url_BlogCategories_IndexJson = "/Admin/BlogCategory/IndexJson";
    export const url_BlogCategories_Edit = "/Admin/BlogCategory/Edit?id=";
    export const url_BlogCategories_EditJson = "/Admin/BlogCategory/EditJson";
    export const url_BlogCategories_AddOrUpdate = "/Admin/BlogCategory/AddOrUpdate";
    export const url_BlogCategories_Delete = "/Admin/BlogCategory/Delete";

    export const url_Notification_Index = "/Admin/Notification/Index";
    export const url_Notification_IndexJson = "/Admin/Notification/IndexJson";
    export const url_Notification_Details = "/Admin/Notification/Details?notificationId=";
    export const url_Notification_DetailsJson = "/Admin/Notification/DetailsJson";
    export const url_Notification_Delete = "/Admin/Notification/Delete";
    export const url_Notification_DeleteAll = "/Admin/Notification/DeleteAll";

    export const url_Account_Login = "/Admin";
    export const url_Account_LoginForm = "/Admin/Account/LoginForm";
    export const url_Account_RegisterForm = "/Admin/Account/RegisterForm";
}