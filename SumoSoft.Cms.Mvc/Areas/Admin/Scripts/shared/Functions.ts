﻿import { IFormResponse } from "../../../../Scripts/interfaces/IFormResponse";
import { IBaseEntityDto } from "../../../../Scripts/interfaces/IBaseEntityDto";

export namespace Functions {

    export function getEnumDescription(enumObj: any, descriptions: Array<string>, value: any) {
        value = value || 0;
        const values = Object.keys(enumObj).filter(x => typeof enumObj[x] !== "number");
        const index = values.indexOf(value.toString());
        return descriptions[index];
    }

    // -------------------------------------------------------------------------------------------------
    /** Generates a new Guid */

    export function newGuid(): string {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, c => {
            const r = Math.random() * 16 | 0;
            const v = c === "x" ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    // -------------------------------------------------------------------------------------------------
    /** Uploads files with ajax. Returns an IFormResponse 
    */
    export function ajaxUploadFile(formData: any, url: string, success?: (formResponse: IFormResponse) => void) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            contentType: false,
            processData: false,
            cache: false,
            data: formData,
            success(response: string) {
                success && success(JSON.parse(response));
            }
        });
    }

    // -------------------------------------------------------------------------------------------------
    /** Returns a url formatted string 
    */
    export function convertToUrl(string: string) {
        return string.toLowerCase()
            .replace(/[^a-zA-Z0-9]+/g, "-") // convert invalid characters into dashes
            .replace(/^-+/g, "") // remove dashes at the beginning
            .replace(/-+$/g, ""); // remove dashes at the end
    }

    // -------------------------------------------------------------------------------------------------
    /** Converts a String to Title Case 
    */
    export function convertToTitleCase(str: string) {
        const splitStr = str.toLowerCase().split(" ");
        for (let i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(" ");
    }

    // -------------------------------------------------------------------------------------------------
    /** Set Form elements to ReadOnly 
    */
    export function toggleFormElementsDisabledState(disabled: boolean, parent?: JQuery) {
        if (parent) {
            parent.find("input,textarea").prop("readOnly", disabled);
            parent.find('input[type="checkbox"],input[type="radio"],select,button:not(.button-delete-component, .button-remove),.editor')
                .prop("disabled", disabled);
        } else {
            $("input,textarea").prop("readOnly", disabled);
            $('input[type="checkbox"],input[type="radio"],select,button:not(.button-delete-component, .button-remove),.editor')
                .prop("disabled", disabled);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /** Use this to disable all the form elements in the page according to the user create and edit permissions 
    */
    export function setCreateAndEditPermissions<T extends IBaseEntityDto>(entity: T, createPermission: boolean, editPermission: boolean, parent?: JQuery) {
        if (entity.isNew) {
            if (!createPermission) {
                toggleFormElementsDisabledState(true, parent);
            }
        } else {
            if (!editPermission) {
                toggleFormElementsDisabledState(true, parent);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------
    /** Use this to disable the Required fields like Name and Url fields.
     * The method returns the value of the permission unless the ID of the entity is an empty Guid.
     * In that case, it means that the entity is new and the field can be edited.
    */
    export function getReadOnlyStateForRequiredField<T extends IBaseEntityDto>(entity: T, permission: boolean): boolean {
        if (entity.isNew) {
            return false;
        } else {
            return !permission;
        }
    }

}