﻿// ------------------------------------------------------
// Elastic text-area
// ------------------------------------------------------

$(document).on("input change click", ".elastic", (e) => {
    var otherFields = $(e.target).closest("tr").find("textarea");
    const maxHeight = Math.max.apply(Math, otherFields.map((i, element) => element.scrollHeight));
    const paddingTop = parseInt($(e.target).css('padding-top'), 10);
    const paddingBottom = parseInt($(e.target).css('padding-bottom'), 10);
    otherFields.height(maxHeight - (paddingTop + paddingBottom));
    $(e.target).css("cursor", "text");
});

// ------------------------------------------------------
/* CKEDITOR */
// ------------------------------------------------------

interface JQuery {
    ckeditor();
    ckeditor(p: { readOnly: boolean });
}

$(".htmlEditor").ckeditor();

$(".htmlEditorReadOnly").ckeditor({
    readOnly: true
});