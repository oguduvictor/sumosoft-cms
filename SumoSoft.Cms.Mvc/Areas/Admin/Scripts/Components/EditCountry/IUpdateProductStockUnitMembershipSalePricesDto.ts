﻿import { Category } from "../../../../../Scripts/classes/Category";

export interface IUpdateProductStockUnitMembershipSalePricesDto {
    salePercentage: number;
    roundMethod: string;
    roundInterval: number;
    roundAfterTax: boolean;
    category: Category;
}