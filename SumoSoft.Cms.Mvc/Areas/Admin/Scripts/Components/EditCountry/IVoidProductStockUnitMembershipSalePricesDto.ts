﻿import { Category } from "../../../../../Scripts/classes/Category";

export interface IVoidProductStockUnitMembershipSalePricesDto {
    category: Category;
}