﻿import { Category } from "../../../../../Scripts/classes/Category";

export interface IVoidProductStockUnitSalePricesDto {
    category: Category;
}