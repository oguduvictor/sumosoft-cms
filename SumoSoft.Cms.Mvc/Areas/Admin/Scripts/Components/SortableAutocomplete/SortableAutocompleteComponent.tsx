﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { SortableComponent, ISortableData, ISortableProps, ISortableRow } from "../Sortable/SortableComponent";
import { Autocomplete, AutocompleteOption } from "../../../../../Scripts/sumoReact/Autocomplete/Autocomplete";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

interface ISortableAutocompleteBaseEntityDto {
    id: string;
}

export interface ISortableAutocompleteState {
    selectedOptionIds: Array<string>;
}

export interface ISortableAutocompleteProps<T extends ISortableAutocompleteBaseEntityDto> {
    disabled?: boolean;
    label?: string;
    className?: string;
    placeholder?: string;
    options: Array<SortableAutocompleteOption<T>>;
    selectedOptionIds?: Array<string>;
    neutralOption?: SortableAutocompleteOption<T>;
    handleChangeValue(newValue: Array<T>): void;
    error?: IFormError;
}

export class SortableAutocompleteOption<T extends ISortableAutocompleteBaseEntityDto> {
    value: T;
    displayTitle: string;
    displayDescription?: string;

    constructor(value: T, displayTitle: string, displayDescription?: string) {
        this.value = value;
        this.displayTitle = displayTitle;
        this.displayDescription = displayDescription;
    }
}

interface ITAutocomplete<T> {
    new (): Autocomplete<T>;
}

export class SortableAutocompleteComponent<T extends ISortableAutocompleteBaseEntityDto> extends React.Component<ISortableAutocompleteProps<T>, ISortableAutocompleteState> {

    constructor(props) {
        super(props);

        this.state = {
            selectedOptionIds: new Array<string>(),
        } as ISortableAutocompleteState;
    }

    componentWillReceiveProps(nextProps: ISortableAutocompleteProps<T>) {
        this.setState({
            selectedOptionIds: nextProps.selectedOptionIds
        });
    }

    handleSelectOption(value: T) {
        const selectedOption = this.props.options.filter(x => x.value === value)[0];
        const selectedOptionIds = sumoJS.deepClone(this.state.selectedOptionIds);
        selectedOptionIds.push(selectedOption.value.id);

        const selectedOptions = selectedOptionIds.map(x => this.props.options.filter(option => option.value.id === x)[0]);

        this.setState({
            selectedOptionIds
        }, () => this.props.handleChangeValue(selectedOptions.map(x => x.value)));
    }

    handleDeleteOption(value: T) {
        const selectedOption = this.props.options.filter(x => x.value === value)[0];
        const selectedOptionIds = sumoJS.deepClone(this.state.selectedOptionIds);
        const selectedOptionIndex = selectedOptionIds.indexOf(selectedOption.value.id);
        selectedOptionIds.splice(selectedOptionIndex, 1);

        const filteredOptions = selectedOptionIds.map(x => this.props.options.filter(option => option.value.id === x)[0]);

        this.setState({
            selectedOptionIds
        }, () => this.props.handleChangeValue(filteredOptions.map(x => x.value)));
    }

    getSortableRows() {
        const selectedOptions = this.state.selectedOptionIds.map(x => this.props.options.filter(option => option.value.id === x)[0]);
        return selectedOptions.map(option => {
            return {
                id: option.value.id,
                contentItems: [
                    { item: `${option.displayTitle}`},
                    {
                        item: <button className="delete-option" onClick={() => this.handleDeleteOption(option.value)}>
                                  ✕
                              </button>
                    } 
                ]
            } as ISortableRow;
        });
    }

    handleChangeSorting(data: Array<ISortableData>) {
        const selectedOptions = data.map(x => this.props.options.filter(y => y.value.id === x.id)[0]);
        this.setState({
            selectedOptionIds: selectedOptions.map(x => x.value.id)
        }, () => this.props.handleChangeValue(selectedOptions.map(x => x.value)));
    }

    render() {
        const TAutocomplete = Autocomplete as ITAutocomplete<T>;

        return (
            <div className={`select-component sortable-autocomplete-component ${this.props.className ? this.props.className : ""} ${this.props.disabled ? "disabled" : ""}`}>
                <label>{this.props.label}</label>
                {
                    this.state.selectedOptionIds.length > 0 &&
                    <SortableComponent {...{
                        rows: this.getSortableRows(),
                        className: "options",
                        handleChangeSorting: (data) => this.handleChangeSorting(data),
                    } as ISortableProps} />
                }
                <TAutocomplete
                    placeholder={this.props.placeholder}
                    className={this.props.className}
                    options={this.props.options.filter(option => !this.state.selectedOptionIds.contains(option.value.id)).map(x => new AutocompleteOption<T>(x.value, x.displayTitle))}
                    selectedOption={this.props.neutralOption ? new AutocompleteOption<T>(this.props.neutralOption.value, this.props.neutralOption.displayTitle) : new AutocompleteOption<T>(null, null)}
                    handleChangeValue={x => this.handleSelectOption(x)}
                />
            </div>
        );
    }
}