﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IMailingListLocalizedKitDto } from '../../../../../Scripts/interfaces/IMailingListLocalizedKitDto';

export interface IMailingListDto {
    id: string;
    name: string;
    sortOrder: number;
    users: Array<IUserDto>;
    localizedTitle: string;
    localizedDescription: string;
    mailingListLocalizedKits: Array<IMailingListLocalizedKitDto>;
}