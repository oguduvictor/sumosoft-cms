﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { MailingList } from "../../../../../Scripts/classes/MailingList";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { IMailingListState } from "./IMailingListState";
import { SortableComponent, ISortableProps, ISortableData, ISortableRow, ISortableRowCellStyle } from '../Sortable/SortableComponent';
import { ButtonSubmitComponent, IButtonSubmitProps } from '../ButtonSubmit/ButtonSubmitComponent';
import { IFormResponse } from '../../../../../Scripts/interfaces/IFormResponse';
import { AlertComponent, IAlertProps } from '../Alert/AlertComponent';

export default class MailingListComponent extends ReactPageComponent<{}, IMailingListState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            mailingList: new Array<MailingList>(),
            sortedMailingList: new Array<MailingList>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IMailingListState;
    }

    componentDidMount() {
        sumoJS.ajaxPost<Array<MailingList>>(Constants.url_MailingList_IndexJson, null, (mailingListDto) => {
            this.setState({
                pageIsLoading: false,
                mailingList: mailingListDto,
            });
        });
    }

    getSortableRows(): ISortableRow[] {
        const mailingLists = this.state.mailingList.orderBy(x => x.sortOrder) as Array<MailingList>;

        return mailingLists.map(item => {
            return {
                id: item.id,
                contentItems: [
                    { item: item.sortOrder },
                    { item: item.name },
                    { item: item.totalSubscribers },
                    { item: item.totalUsers },
                    {
                        item:
                            <div className="dropdown">
                                <button className="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                    Options <span className="caret"></span>
                                </button>
                                <ul className="dropdown-menu text-right">
                                    <li>
                                        <a href={Constants.url_MailingList_Edit + item.id}>
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onClick={e => this.handleDuplicate(e, item.id)}>
                                            Duplicate
                                        </a>
                                    </li>
                                </ul>
                            </div>
                    }
                ],
                children: null,
                
                cellStyles: [
                    { cellIndices: [0, 1, 2, 3, 4], style: { paddingRight: "5px", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" } } as ISortableRowCellStyle,
                    { cellIndices: [0], style: { width: "15rem" } } as ISortableRowCellStyle,
                    { cellIndices: [1], style: { width: "45rem" } } as ISortableRowCellStyle,
                    { cellIndices: [4], style: { width: "10rem" } } as ISortableRowCellStyle
                ]
            } as ISortableRow;
        });
    }

    handleChangeSorting(data: Array<ISortableData>) {
        const sortedMailingList = data as Array<MailingList>;

        this.setState({
            sortedMailingList,
            isModified: true
        });
    }

    handleDuplicate(e, mailingListId: string) {
        sumoJS.ajaxPost<IFormResponse>(Constants.url_MailingList_Duplicate, { id: mailingListId }, response => {
            if (response.isValid) {
                window.location.href = response.redirectUrl;
            } else {
                this.setState({
                    isSubmitting: false
                });
            }
        });
    }

    handleSubmit() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse<Array<MailingList>>>(Constants.url_MailingList_UpdateSortOrder, { mailingListDtos: this.state.sortedMailingList }, response => {
            if (response.isValid) {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess,
                    mailingList: response.target,
                    sortedMailingList: []
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false,
                    sortedMailingList: []
                });
            }
        });

    }

    render() {
        return (
            <div className="mailing-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Mailing Lists
                    </div>
                    <div className="admin-top-bar-controls">
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            className: this.state.isModified ? "" : "disabled",
                            handleSubmit: () => this.handleSubmit()
                        } as IButtonSubmitProps} />
                        <a href={Constants.url_MailingList_Edit} className={`btn btn-primary`}>
                            <i className="fa fa-plus"></i>
                            Create new Mailing List
                        </a>
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">

                        <SortableComponent {...{
                            headerRowItems: ["Sort Order", "Name", "Subscribers", "Users", ""],
                            rows: this.getSortableRows(),
                            className: "sortable-attribute-options",
                            handleChangeSorting: (data) => this.handleChangeSorting(data),
                            isFixedTableLayout: true
                        } as ISortableProps} />
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}