﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { MailingList } from '../../../../../Scripts/classes/MailingList';

export interface IMailingListState extends IReactPageState {
    mailingList: Array<MailingList>;
    sortedMailingList: Array<MailingList>;
    pageIsLoading: boolean;
}