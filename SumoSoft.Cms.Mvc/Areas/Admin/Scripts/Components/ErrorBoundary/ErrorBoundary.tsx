﻿import * as React from "react";
import { LogTypesEnum } from "../../../../../Scripts/enums/LogTypesEnum";

interface IWithErrorBoundaryState {
    error?: Error;
    errorInfo?: any;
}

const withErrorBoundary = <P extends object>(Component: React.ComponentType<P>) => class WithErrorBoundary extends React.Component<P, IWithErrorBoundaryState> {
    constructor(props: Readonly<P>) {
        super(props);
        this.state = {} as IWithErrorBoundaryState;
    }

    componentDidCatch(error: Error, errorInfo: any): void {
        this.setState({
            error, errorInfo
        }, () => {
            const logMessage = `An error occured ${this.state.errorInfo.componentStack} - ${this.state.error.message}.`;
            this.log(LogTypesEnum.exception, logMessage.replace(/\n|\s+/g, " "), this.state.error.stack);
        });

        console.log("Render Error: ", error);
        console.log("Render Error Info: ", errorInfo);
    }

    log(type: LogTypesEnum, message: string, details?: string): void {
        $.ajax({ type: "POST", url: "/CmsApi/Log",
            data: JSON.stringify({ type: type, message: message, details: details }),
            contentType: "application/json"
        });
    }

    renderError(): JSX.Element {
        return (
            <div className="error-boundary-component">
                <h1 className="title">
                    Oops! Something went wrong :'(
                </h1>
                <div className="subtitle">
                    {
                        `An error occured ${this.state.errorInfo.componentStack} - ${this.state.error.message}. See stack trace below. More details available in Console.`
                    }
                </div>
                <div className="error-stack-trace">
                    {
                        this.state.error.stack
                    }
                </div>
            </div>
        );
    }

    render() {
        return (
            !!this.state.error ? this.renderError() : <Component {...this.props} />
        );
    }

}

export default withErrorBoundary;