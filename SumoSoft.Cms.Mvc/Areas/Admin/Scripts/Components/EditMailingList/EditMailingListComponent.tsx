﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { Constants } from '../../shared/Constants';
import { ButtonDeleteComponent, IButtonDeleteProps } from '../ButtonDelete/ButtonDeleteComponent';
import { ButtonSubmitComponent, IButtonSubmitProps } from '../ButtonSubmit/ButtonSubmitComponent';
import { AlertComponent, IAlertProps } from '../Alert/AlertComponent';
import { SpinnerComponent } from '../Spinner/SpinnerComponent';
import { ReactPageComponent } from '../ReactPage/ReactPageComponent';
import { IEditMailingListState } from './IEditMailingListState';
import { User } from '../../../../../Scripts/classes/User';
import { MailingList } from '../../../../../Scripts/classes/MailingList';
import { TextboxComponent, ITextboxProps } from '../Textbox/TextboxComponent';
import { TableComponent, ITableProps } from '../Table/TableComponent';
import { Functions } from '../../shared/Functions';
import { MailingListLocalizedKit } from '../../../../../Scripts/classes/MailingListLocalizedKit';
import { ILocalizedKitsProps, ILocalizedKitUI, LocalizedKitsComponent } from '../LocalizedKits/LocalizedKitsComponent';
import { Country } from '../../../../../Scripts/classes/Country';
import { IEditMailingListDto } from './IEditMailingListDto';
import { IFormResponse } from '../../../../../Scripts/interfaces/IFormResponse';
import { UserRole } from '../../../../../Scripts/classes/UserRole';
import { MailingListSubscriptionStatusEnum } from '../../../../../Scripts/enums/MailingListSubscriptionStatusEnum';
import { CheckboxComponent, ICheckboxProps } from '../Checkbox/CheckboxComponent';
import { IFormError } from '../../../../../Scripts/interfaces/IFormError';

export default class EditMailingListComponent extends ReactPageComponent<{}, IEditMailingListState> {

    constructor(props) {
        super(props);

        this.state = {
            mailingList: new MailingList(),
            authenticatedUser: new User({ role: new UserRole() }),
            pageIsLoading: true,
        } as IEditMailingListState
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    setStateFromApi = () => {
        sumoJS.ajaxPost<IEditMailingListDto>(Constants.url_MailingList_EditJson,
            {
                id: sumoJS.getQueryStringParameter("id")
            },
            (editMailingListDto) => {

                this.setState({
                    authenticatedUser: new User(editMailingListDto.authenticatedUser),
                    mailingList: new MailingList(editMailingListDto.mailingList),
                    pageIsLoading: false
                }, () => {
                    Functions.setCreateAndEditPermissions(
                        this.state.mailingList,
                        this.state.authenticatedUser.role.createMailingList,
                        this.state.authenticatedUser.role.editMailingList, $(".non-localized"));

                    const defaultLocalizedKitCountry =
                        sumoJS.get(() => this.state.mailingList.mailingListLocalizedKits.filter(x => x.country.isDefault)[0].country, new Country());

                    Functions.setCreateAndEditPermissions(
                        this.state.mailingList,
                        this.state.authenticatedUser.role.createMailingList,
                        this.state.authenticatedUser.role.editMailingList, $(`#${defaultLocalizedKitCountry.id}${defaultLocalizedKitCountry.id}`));
                });
            });

    }

    handleChangeMailingListName = e => {
        this.state.mailingList.name = e.target.value;
        this.setState({
            mailingList: this.state.mailingList
        });
    }

    handleChangeIsDisabled(e) {
        this.state.mailingList.isDisabled = e.target.checked;
        this.setState({ mailingList: this.state.mailingList });
    }

    handleChangeMailingListLocalizedTitle(e, mailingListLocalizedKit: MailingListLocalizedKit) {
        mailingListLocalizedKit.title = e.target.value;
        this.setState({
            mailingList: this.state.mailingList
        });
    }

    handleChangeMailingListLocalizedDescription(e, mailingListLocalizedKit: MailingListLocalizedKit) {
        mailingListLocalizedKit.description = e.target.value;
        this.setState({
            mailingList: this.state.mailingList
        });
    }

    handleChangeMailingListLocalizedIsDisabled(e, mailingListLocalizedKit: MailingListLocalizedKit) {
        mailingListLocalizedKit.isDisabled = e.target.checked;

        this.setState({
            mailingList: this.state.mailingList
        });
    }

    handleChangeMailingListSortOrder(e) {
        this.state.mailingList.sortOrder = e.target.value;
        this.setState({
            mailingList: this.state.mailingList
        });
    }

    handleLocalizedKitCopyValuesFrom(source, target, completed) {
        target.subject = source.subject;
        const sourceContent = $(`#${source.id}`).val();
        $(`#${target.id}`).val(sourceContent);
        this.setState({
            mailingList: this.state.mailingList
        }, () => completed());
    }

    handleMailingListDelete() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_MailingList_Delete, { id: this.state.mailingList.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_MailingList_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleUploadFiles(e) {
        const formData = new FormData();
        const files = e.target.files;

        let totalSize = 0;

        for (let i = 0; i < files.length; i++) {
            totalSize = totalSize + files[i].size;
        }

        if (totalSize / 1024 / 1024 > 100) {
            this.setState({
                formErrors: [{ message: "The files you're trying to upload exceed the maximum upload size (100 MB)" } as IFormError]
            });
        } else {

            formData.append("id", this.state.mailingList.id);

            for (let i = 0; i < files.length; i++) {
                formData.append(files[i].name, files[i]);
            }

            this.setState({
                isUploadingFile: true
            });

            Functions.ajaxUploadFile(formData, Constants.url_MailingList_UploadImportEmails, response => {
                this.setStateFromApi();

                if (response.isValid) {
                    this.setState({
                        formSuccessMessage: Constants.alert_saveSuccess,
                        isUploadingFile: false
                    });
                } else {
                    this.setState({
                        formErrors: response.errors,
                        isUploadingFile: false
                    });
                }
            });
        }
    }


    submit = () => {
        this.setState({
            isSubmitting: true
        });
        const mailingList = sumoJS.deepClone(this.state.mailingList);
        mailingList.mailingListSubscriptions = [];
        sumoJS.ajaxPost<IFormResponse>(Constants.url_MailingList_AddOrUpdate, mailingList, formResponse => {
            if (formResponse.isValid) {
                if (formResponse.redirectUrl) history.pushState({}, "", formResponse.redirectUrl);
                this.setStateFromApi();
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    isModified: false,
                    isSubmitting: false
                });
            } else {
                this.setState({
                    formErrors: formResponse.errors,
                    isModified: false,
                    isSubmitting: false
                });
            }
        });
    }

    render() {
        return (
            <div className="edit-mailing-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.mailingList.isNew ? "New Mailing List" : "Edit Mailing List"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_MailingList_Index} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            !this.state.mailingList.isNew && <a href={`${Constants.url_MailingList_Export}?id=${this.state.mailingList.id}`} className="btn btn-default">
                                <i className="fa fa-table" aria-hidden="true"></i>
                                Export users
                            </a>
                        }
                        {
                            !this.state.mailingList.isNew &&
                            <ButtonDeleteComponent {...{
                                handleDelete: () => this.handleMailingListDelete(),
                                className: this.state.authenticatedUser.role.deleteUser ? "" : "hidden",
                                canDelete: this.state.authenticatedUser.role.deleteMailingList,
                                isDeleting: this.state.isDeleting
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            handleSubmit: () => this.submit(),
                            isSubmitting: this.state.isSubmitting
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading && <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Global properties
                                </div>

                                <div className="table-wrapper">
                                    <TextboxComponent {...{
                                        label: "Name",
                                        value: this.state.mailingList.name,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: (e) => this.handleChangeMailingListName(e)
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Sort order",
                                        value: this.state.mailingList.sortOrder,
                                        className: "form-group",
                                        type: "number",
                                        handleChangeValue: (e) => this.handleChangeMailingListSortOrder(e)
                                    } as ITextboxProps} />
                                    <CheckboxComponent {...{
                                        value: this.state.mailingList.isDisabled || false,
                                        className: "form-group",
                                        text: "Disabled",
                                        handleChangeValue: (e) => this.handleChangeIsDisabled(e)
                                    } as ICheckboxProps} />
                                </div>
                            </div>

                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Localized properties
                                </div>
                                <div className="table-wrapper">
                                    <LocalizedKitsComponent {...{
                                        handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source, target, completed),
                                        editAllLocalizedKits: this.state.authenticatedUser.role.editMailingListAllLocalizedKits,
                                        localizedKitUIs: this.state.mailingList.mailingListLocalizedKits.map((mailingListLocalizedKit, i) => {
                                            return {
                                                localizedKit: mailingListLocalizedKit,
                                                html: <div>
                                                    <TextboxComponent {...{
                                                        label: "Title",
                                                        value: mailingListLocalizedKit.title,
                                                        placeholder: this.state.mailingList.mailingListLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                        className: "form-group",
                                                        type: "text",
                                                        handleChangeValue: (e) => this.handleChangeMailingListLocalizedTitle(e, mailingListLocalizedKit),
                                                    } as ITextboxProps} />
                                                    <TextboxComponent {...{
                                                        label: "Description",
                                                        value: mailingListLocalizedKit.description,
                                                        placeholder: this.state.mailingList.mailingListLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                        className: "form-group",
                                                        type: "text",
                                                        handleChangeValue: (e) => this.handleChangeMailingListLocalizedDescription(e, mailingListLocalizedKit),
                                                    } as ITextboxProps} />
                                                    <CheckboxComponent {...{
                                                        value: mailingListLocalizedKit.isDisabled || false,
                                                        className: "form-group",
                                                        text: "Disabled",
                                                        handleChangeValue: (e) => this.handleChangeMailingListLocalizedIsDisabled(e, mailingListLocalizedKit)
                                                    } as ICheckboxProps} />
                                                </div>
                                            } as ILocalizedKitUI;
                                        })
                                    } as ILocalizedKitsProps} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Users
                                </div>
                                {
                                    !this.state.mailingList.isNew && !this.state.mailingList.mailingListSubscriptions.length &&
                                    <form method="post" encType="multipart/form-data" className="file-upload-form">
                                        <input type="file" ref="file" value="" name="files" accept="text/csv" className="btn btn-default" id="file-upload" onChange={e => this.handleUploadFiles(e)} />
                                        {
                                            this.state.isUploadingFile && <span className="file-upload-spinner"><i className="fa fa-spinner fa-pulse fa-fw"></i></span>
                                        }
                                    </form>
                                }
                                <br />
                                <div className="table-wrapper">
                                    <TableComponent {...{
                                        thList: ["#", "Name", "Email", "Status"],
                                        trList: this.state.mailingList.mailingListSubscriptions
                                            .map((subscription, index) => [
                                                index + 1,
                                                subscription.user && subscription.user.fullName,
                                                subscription.user && subscription.user.email,
                                                Functions.getEnumDescription(MailingListSubscriptionStatusEnum, subscription.statusDescriptions, subscription.status)
                                            ])
                                    } as ITableProps} />
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        )
    }
}