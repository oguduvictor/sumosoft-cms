﻿import { MailingList } from '../../../../../Scripts/classes/MailingList';
import { IReactPageState } from '../ReactPage/ReactPageComponent';

export interface IEditMailingListState extends IReactPageState {
    mailingList: MailingList;
    isUploadingFile: boolean;
}