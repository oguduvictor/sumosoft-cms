﻿import { IMailingListDto } from '../../../../../Scripts/interfaces/IMailingListDto';
import { IUserDto } from '../../../../../Scripts/interfaces/IUserDto';

export interface IEditMailingListDto {
    authenticatedUser: IUserDto;
    mailingList: IMailingListDto;
}