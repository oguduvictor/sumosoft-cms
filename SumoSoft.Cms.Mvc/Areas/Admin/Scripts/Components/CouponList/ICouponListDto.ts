﻿import { ICouponDto } from "../../../../../Scripts/interfaces/ICouponDto";
import { IPaginatedListDto } from "../../interfaces/IPaginatedListDto";

export interface ICouponListDto extends IPaginatedListDto {
    coupons: Array<ICouponDto>;
}