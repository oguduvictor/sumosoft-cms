﻿export interface IPaginatedListDto {
    page: number;
    pageSize: number;
    totalItems: number;
    totalPages: number;
}