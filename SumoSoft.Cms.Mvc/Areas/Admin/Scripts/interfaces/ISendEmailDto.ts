﻿import { Country } from '../../../../Scripts/classes/Country';
import { MailingList } from '../../../../Scripts/classes/MailingList';
import { User } from '../../../../Scripts/classes/User';
import { IUserDto } from '../../../../Scripts/interfaces/IUserDto';

export interface ISendEmailDto {
    authenticatedUser: IUserDto;
    allCountries: Array<Country>;
    allMailingLists: Array<MailingList>;
    allUsers: Array<User>;
}