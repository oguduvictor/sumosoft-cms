﻿import { IContentSectionDto } from "../../../../Scripts/interfaces/IContentSectionDto";
import { IFormResponse } from "../../../../Scripts/interfaces/IFormResponse";

export interface ICreateContentSectionDto {
    formResponse: IFormResponse;
    similarContentSection: IContentSectionDto;
    savedContentSection: IContentSectionDto;
}