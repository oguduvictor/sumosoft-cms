CKEDITOR.editorConfig = function (config) {

    config.toolbarGroups = [
        { name: "clipboard", groups: ["clipboard", "undo"] },
        { name: "editing", groups: ["find", "selection", "spellchecker"] },
        { name: "links" },
        { name: "insert" },
        { name: "forms" },
        { name: "tools" },
        { name: "document", groups: ["mode", "document", "doctools"] },
        { name: "others" },
        "/",
        { name: "basicstyles", groups: ["basicstyles", "cleanup"] },
        { name: "paragraph", groups: ["list", "indent", "blocks", "align", "bidi"] },
        { name: "styles" },
        { name: "colors" },
        { name: "about" }
    ];

    config.height = "500";

    config.removeButtons = "Underline,Subscript,Superscript,About,Anchor,PasteFromWord,Styles";

    config.removePlugins = "elementspath,resize";

    config.extraPlugins = "htmlwriter";

    config.forcePasteAsPlainText = true;

    config.format_tags = "p;h1;h2;h3;h4;h5";

    config.removeDialogTabs = "image:advanced;link:advanced";

    // ---------------------------------------------------------------------------------
    // ALLOW CUSTOM HTML WITH THE EXCEPTION OF IMG WIDTH AND HEIGHT
    // ---------------------------------------------------------------------------------

    config.allowedContent = {
        $1: {
            elements: CKEDITOR.dtd,
            attributes: true,
            styles: true,
            classes: true
        }
    };

    config.disallowedContent = "img{width,height};img[width,height];";
};

CKEDITOR.on("instanceReady", function (ev) {

    var tags1 = ["div", "p", "h1", "h2", "h3", "h4", "h5", "h6", "ul", "li", "span", "section", "hr", "tr", "td"];

    for (var key1 in tags1) {
        if (tags1.hasOwnProperty(key1)) {
            ev.editor.dataProcessor.writer.setRules(tags1[key1], {
                indent: true,
                breakBeforeOpen: true,
                breakAfterOpen: true,
                breakBeforeClose: true,
                breakAfterClose: false
            });
        }
    }

    var tags2 = ["code", "pre"];

    for (var key2 in tags2) {
        if (tags2.hasOwnProperty(key2)) {
            ev.editor.dataProcessor.writer.setRules(tags2[key2], {
                indent: false,
                breakBeforeOpen: true,
                breakAfterOpen: true,
                breakBeforeClose: true,
                breakAfterClose: false
            });
        }
    }

    ev.editor.dataProcessor.writer.setRules("img", {
        indent: true,
        breakBeforeOpen: false,
        breakAfterOpen: true
    });

    ev.editor.dataProcessor.writer.setRules("a", {
        indent: false,
        breakBeforeOpen: false,
        breakAfterOpen: false
    });
});