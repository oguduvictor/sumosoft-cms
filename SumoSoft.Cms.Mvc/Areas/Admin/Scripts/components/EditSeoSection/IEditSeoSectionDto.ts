﻿import { ISeoSectionDto } from "../../../../../Scripts/interfaces/ISeoSectionDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditSeoSectionDto {
    authenticatedUser: IUserDto;
    seoSection: ISeoSectionDto;
}