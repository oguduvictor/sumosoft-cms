﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Constants } from "../../shared/Constants";
import { SeoSection } from "../../../../../Scripts/classes/SeoSection";
import { IEditSeoSectionState } from "./IEditSeoSectionState";
import { SeoSectionLocalizedKit } from "../../../../../Scripts/classes/SeoSectionLocalizedKit";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { IEditSeoSectionDto } from "./IEditSeoSectionDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";
import { IFilePickerProps, FilePickerComponent } from "../FilePicker/FilePickerComponent";

export default class EditSeoSectionComponent extends ReactPageComponent<{}, IEditSeoSectionState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            formErrors: new Array<IFormError>(),
            pageIsLoading: true,
            seoSection: new SeoSection(),
        } as IEditSeoSectionState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleChangeSeoPage(e) {
        this.state.seoSection.page = e.target.value;
        this.setState({ isModified: true, seoSection: this.state.seoSection });
    };

    handleChangeSeoLocalizedKitTitle(e, seoSectionLocalizedKit: SeoSectionLocalizedKit) {
        seoSectionLocalizedKit.title = e.target.value;
        this.setState({
            isModified: true,
            seoSection: this.state.seoSection
        });
    };

    handleChangeSeoLocalizedKitDescription(e, seoSectionLocalizedKit: SeoSectionLocalizedKit) {
        seoSectionLocalizedKit.description = e.target.value;
        this.setState({
            isModified: true,
            seoSection: this.state.seoSection
        });
    };

    handleChangeSeoLocalizedKitImage(absoluteUrl: string, seoSectionLocalizedKit: SeoSectionLocalizedKit) {
        seoSectionLocalizedKit.image = absoluteUrl;
        this.setState({
            isModified: true,
            seoSection: this.state.seoSection
        });
    }

    handleSeoDelete() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Seo_Delete, { id: this.state.seoSection.id }, response => {

            if (response.isValid) {
                window.location.href = Constants.url_Seo_Index;
            } else {
                this.setState({
                    formErrors: response.errors,
                    isDeleting: false
                });
            }
        });
    };

    handleLocalizedKitCopyValuesFrom(source, target, completed) {
        target.title = source.title;
        target.description = source.description;

        this.setState({
            isModified: true,
            seoSection: this.state.seoSection
        }, () => completed());
    }

    submit() {
        const seoSection = this.state.seoSection;
        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Seo_AddOrUpdate, seoSection, response => {

            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditSeoSectionDto>(Constants.url_Seo_EditJson,
            {
                id: sumoJS.getQueryStringParameter("id")
            },
            (dto) => {
                this.setState({
                    authenticatedUser: new User(dto.authenticatedUser),
                    pageIsLoading: false,
                    seoSection: new SeoSection(dto.seoSection)
                }, () => Functions.setCreateAndEditPermissions(
                    this.state.seoSection,
                    this.state.authenticatedUser.role.createSeoSection,
                    this.state.authenticatedUser.role.editSeoSection));
            });
    }

    validateSeoPage(e) {
        $.ajax({
            url: `${e.target.value}`,
            type: "HEAD",
            success: () => this.setState({
                pageTextboxAddon: <i className="fa fa-check-circle fa-lg valid" title="Url is valid"></i>
            }),
            error: () => this.setState({
                pageTextboxAddon: <i className="fa fa-times-circle fa-lg invalid" title="Url is invalid"></i>
            })
        });
    }

    render() {
        return (
            <div className="edit-seo-section-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.seoSection.isNew ? "Create SEO content" : "Edit SEO content"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Seo_Index} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            !this.state.seoSection.isNew &&
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                canDelete: this.state.authenticatedUser.role.deleteSeoSection,
                                handleDelete: () => this.handleSeoDelete()
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div >
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <TextboxComponent {...{
                            label: "Page",
                            type: "text",
                            className: "form-group page-textbox",
                            value: this.state.seoSection.page,
                            handleChangeValue: (e) => this.handleChangeSeoPage(e),
                            readOnly: Functions.getReadOnlyStateForRequiredField(
                                this.state.seoSection,
                                this.state.authenticatedUser.role.editSeoSectionPage),
                            handleBlur: (e) => this.validateSeoPage(e),
                            addon: this.state.pageTextboxAddon
                        } as ITextboxProps} />
                        <LocalizedKitsComponent {...{
                            handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source, target, completed),
                            editAllLocalizedKits: this.state.authenticatedUser.role.editSeoSectionAllLocalizedKits,
                            localizedKitUIs: this.state.seoSection.seoSectionLocalizedKits.map((seoLocalizedKit, i) => {
                                return {
                                    localizedKit: seoLocalizedKit,
                                    html:
                                        <div>
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <TextboxComponent {...{
                                                        label: "Title",
                                                        type: "text",
                                                        className: "form-group",
                                                        value: seoLocalizedKit.title,
                                                        placeholder: this.state.seoSection.seoSectionLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                        handleChangeValue: (e) => this.handleChangeSeoLocalizedKitTitle(e, seoLocalizedKit),
                                                        addon: <span className="title-counter"> {seoLocalizedKit.title && seoLocalizedKit.title.length} </span>
                                                    } as ITextboxProps} />
                                                </div>
                                            </div>
                                            <TextboxComponent {...{
                                                label: "Description",
                                                type: "text",
                                                className: "form-group",
                                                value: seoLocalizedKit.description,
                                                placeholder: this.state.seoSection.seoSectionLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                handleChangeValue: (e) => this.handleChangeSeoLocalizedKitDescription(e, seoLocalizedKit),
                                                addon: <span className="description-counter">{seoLocalizedKit.description && seoLocalizedKit.description.length} </span>
                                            } as ITextboxProps} />
                                            <FilePickerComponent {...{
                                                label: "Image",
                                                type: "text",
                                                value: seoLocalizedKit.image,
                                                className: "form-group",
                                                handleChangeValue: (absoluteUrl) => this.handleChangeSeoLocalizedKitImage(absoluteUrl, seoLocalizedKit)
                                            } as IFilePickerProps} />
                                        </div>
                                } as ILocalizedKitUI;
                            })
                        } as ILocalizedKitsProps} />
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}
