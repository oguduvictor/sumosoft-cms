﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { SeoSection } from "../../../../../Scripts/classes/SeoSection";
import { User } from "../../../../../Scripts/classes/User";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export interface IEditSeoSectionState extends IReactPageState {
    authenticatedUser?: User;
    formErrors?: Array<IFormError>;
    pageIsLoading?: boolean;
    seoSection?: SeoSection;
    pageTextboxAddon?: JSX.Element;
}