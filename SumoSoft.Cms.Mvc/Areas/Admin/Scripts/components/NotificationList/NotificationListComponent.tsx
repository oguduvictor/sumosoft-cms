﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { TableComponent, ITableProps } from "../Table/TableComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { INotificationListState } from "./INotificationListState";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { Constants } from "../../shared/Constants";
import { Log } from "../../../../../Scripts/classes/Log";
import { ILogDto } from "../../../../../Scripts/interfaces/ILogDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";

export default class NotificationListComponent extends React.Component<{}, INotificationListState> {

    constructor(props) {
        super(props);

        this.state = {
            isDeleting: false,
            pageIsLoading: true,
            logs: new Array<Log>()
        } as INotificationListState;

        this.handleDeleteAll = this.handleDeleteAll.bind(this);
    }

    componentDidMount() {
        sumoJS.ajaxPost<Array<ILogDto>>(Constants.url_Notification_IndexJson, null, (logList) => {
            this.setState({
                pageIsLoading: false,
                logs: logList.map(x => new Log(x))
            });
        });
    }

    componentDidCatch(error: Error, errorInfo: Object): void {
        console.error({ error });
        console.error({ errorInfo });
    }

    handleDelete(id: string) {
        this.setState({ isDeleting: true });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Notification_Delete, { id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Notification_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleDeleteAll() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Notification_DeleteAll, null, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Notification_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    render() {
        return (
            <div className="notification-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Notifications
                    </div>
                    <div className="admin-top-bar-controls">
                        <ButtonDeleteComponent {...{
                            text: "Delete All",
                            isDeleting: this.state.isDeleting,
                            handleDelete: () => this.handleDeleteAll()
                        } as IButtonDeleteProps} />
                    </div>
                </div>
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <div className="table-wrapper">
                            <TableComponent {...{
                                thList: ["#", "Date", "Message", ""],
                                trList: this.state.logs
                                    .map((log, i) => [this.state.logs.length - i,
                                    dayjs(log.createdDate).format("DD/MM/YYYY"),
                                    log.message,
                                    <span>
                                        <a className="btn btn-primary btn-xs" href={Constants.url_Notification_Details + log.id}>
                                            open
                                        </a>
                                        &nbsp;
                                        <a className="btn btn-danger btn-xs" onClick={() => this.handleDelete(log.id)}>
                                            delete
                                        </a>
                                    </span>])

                            } as ITableProps} />
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}