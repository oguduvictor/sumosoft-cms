﻿import {IReactPageState} from "../ReactPage/ReactPageComponent";
import {Log} from "../../../../../Scripts/classes/Log";

export interface INotificationListState extends IReactPageState {
    logs?: Array<Log>;
}