﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { BlogCategory } from "../../../../../Scripts/classes/BlogCategory";

export interface IEditBlogCategoryState extends IReactPageState {
    blogCategory?: BlogCategory;
}