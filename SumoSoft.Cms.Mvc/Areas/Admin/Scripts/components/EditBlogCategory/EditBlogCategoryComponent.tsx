﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { Constants } from "../../shared/Constants";
import { BlogCategory } from "../../../../../Scripts/classes/BlogCategory";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { BlogCategoryLocalizedKit } from "../../../../../Scripts/classes/BlogCategoryLocalizedKit";
import { IEditBlogCategoryState } from "./IEditBlogCategoryState";
import { IBlogCategoryDto } from "../../../../../Scripts/interfaces/IBlogCategoryDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditBlogCategoryComponent extends ReactPageComponent<{}, IEditBlogCategoryState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            blogCategory: new BlogCategory(),
            isDeleting: false,
            isSubmitting: false
        } as IEditBlogCategoryState;

        this.handleChangeCategoryName = this.handleChangeCategoryName.bind(this);
        this.handleChangeUrl = this.handleChangeUrl.bind(this);
        this.handleChangeCategoryLocalizedKitTitle = this.handleChangeCategoryLocalizedKitTitle.bind(this);
        this.handleCategoryDelete = this.handleCategoryDelete.bind(this);
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleUpdateUrlOnBlur(source: string) {
        const blogCategory = this.state.blogCategory;
        if (!blogCategory.url || blogCategory.url === "") {
            blogCategory.url = Functions.convertToUrl(source);
            this.setState({ blogCategory });
        }
    }

    handleChangeCategoryName(e) {
        this.state.blogCategory.name = e.target.value;
        this.setState({
            isModified: true,
            blogCategory: this.state.blogCategory
        });
    }

    handleChangeUrl(e) {
        this.state.blogCategory.url = e.target.value;
        this.setState({
            isModified: true,
            blogCategory: this.state.blogCategory
        });
    }

    handleCategoryDelete() {
        this.setState({ isDeleting: true });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_BlogCategories_Delete, { id: this.state.blogCategory.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_BlogCategories_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleChangeCategoryLocalizedKitTitle(e, categoryLocalizedKit: BlogCategoryLocalizedKit) {
        categoryLocalizedKit.title = e.target.value;
        this.setState({
            isModified: true,
            blogCategory: this.state.blogCategory
        });
    }

    handleChangeCategoryLocalizedKitDescription(e, categoryLocalizedKit: BlogCategoryLocalizedKit) {
        categoryLocalizedKit.description = e.target.value;
        this.setState({
            isModified: true,
            blogCategory: this.state.blogCategory
        });
    }

    handleLocalizedKitCopyValuesFrom(source, target, completed) {
        target.title = source.title;
        target.description = source.description;
        this.setState({
            isModified: true,
            blogCategory: this.state.blogCategory
        }, () => completed());
    }

    submit() {
        const blogCategory = this.state.blogCategory;

        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_BlogCategories_AddOrUpdate, blogCategory, response => {
            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);
                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IBlogCategoryDto>(Constants.url_BlogCategories_EditJson,
            {
                id: sumoJS.getQueryStringParameter("id")
            },
            (dto) => {
                this.setState({
                    blogCategory: new BlogCategory(dto)
                });
            });
    }

    render() {
        return (
            <div className="edit-blog-category-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.blogCategory.isNew ? "Create category" : "Edit category"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_BlogCategories_Index} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            !this.state.blogCategory.isNew &&
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                handleDelete: () => this.handleCategoryDelete()
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div >
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                <div className="admin-subsection">
                    <TextboxComponent {...{
                        label: "Category Name",
                        value: this.state.blogCategory.name,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeCategoryName(e),
                        handleBlur: (e) => this.handleUpdateUrlOnBlur(e.target.value)
                    } as ITextboxProps}
                    />
                    <TextboxComponent {...{
                        label: "Url",
                        readOnly: false,
                        value: this.state.blogCategory.url,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeUrl(e)
                    } as ITextboxProps}
                    />
                    <LocalizedKitsComponent {...{
                        editAllLocalizedKits: true,
                        handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source, target, completed),
                        localizedKitUIs: this.state.blogCategory.blogCategoryLocalizedKits.map((categoryLocalizedKit, i) => {
                            return {
                                localizedKit: categoryLocalizedKit,
                                html:
                                    <div>
                                        <NestedInputComponent {...{
                                            input: <TextboxComponent {...{
                                                label: "Title",
                                                type: "text",
                                                value: categoryLocalizedKit.title,
                                                placeholder: this.state.blogCategory.blogCategoryLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitTitle(e, categoryLocalizedKit),
                                                handleBlur: (e) => this.handleUpdateUrlOnBlur(e.target.value)
                                            } as ITextboxProps} />
                                        } as INestedInputProps} />
                                        <NestedInputComponent {...{
                                            input: <TextboxComponent {...{
                                                label: "Description",
                                                type: "text",
                                                value: categoryLocalizedKit.description,
                                                placeholder: this.state.blogCategory.blogCategoryLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitDescription(e, categoryLocalizedKit)
                                            } as ITextboxProps} />
                                        } as INestedInputProps} />
                                    </div>
                            } as ILocalizedKitUI;
                        })
                    } as ILocalizedKitsProps} />
                </div>
            </div>
        );
    }
}