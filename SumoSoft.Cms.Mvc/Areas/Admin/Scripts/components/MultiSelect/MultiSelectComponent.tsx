﻿import * as React from "react";

import { Functions } from "../../shared/Functions";
import { TopLabel } from "../TopLabel/TopLabel";

export interface IMultiSelectProps {
    disabled?: boolean;
    value?: Array<string>;
    label?: string;
    className?: string;
    options: Array<SmartOption>;
    maxItems?: number;
    neutralOption?: SmartOption;
    placeholder?: string;
    addon?: JSX.Element;
    handleChangeValue?(newValue: Array<string>): void;
    allowCreateOptions?: boolean;
    forceLowerCase?: boolean;
    enableSorting?: boolean;
}

export class SmartOption {
    value?: string;
    title?: string;
    subtitle?: string;

    constructor(value: string, title: string, subtitle = null) {
        this.value = value;
        this.title = title;
        this.subtitle = subtitle;
    }
}

export class MultiSelectComponent extends React.Component<IMultiSelectProps, undefined> {

    selectId = Functions.newGuid();

    initializeSelectize() {
        if (this.props.allowCreateOptions) {
            $(`#${this.selectId}`).selectize(({
                delimiter: ",",
                persist: true,
                create(input) {
                    return {
                        value: input,
                        text: input,
                        title: input
                    }
                },
                sortField: "title",
                valueField: "value",
                searchField: ["title", "subtitle"],
                plugins: this.props.enableSorting ? ["drag_drop"] : [],
                onChange: (newValue: Array<string>) => this.props.handleChangeValue(newValue || new Array<string>()),
                maxItems: this.props.maxItems,
                options: this.props.options,
                items: this.props.value,
                render: {
                    item: (item: SmartOption) => {
                        return `<div title="${item.subtitle}">${this.props.forceLowerCase
                            ? item.title.toLowerCase()
                            : item.title}</div>`;
                    },
                    option: (item: SmartOption) => {
                        var title = `<div class="option-title">${item.title}</div>`;
                        var subtitle = `<div class="option-subtitle">${item.subtitle}</div>`;
                        if (item.subtitle) return `<div>${title + subtitle}</div>`;
                        return title;
                    }
                }
            }) as Selectize.IOptions<any, any>);
        }
        else {
            $(`#${this.selectId}`).selectize(({
                sortField: "title",
                valueField: "value",
                searchField: ["title", "subtitle"],
                placeholder: this.props.placeholder,
                plugins: this.props.enableSorting ? ["drag_drop"] : [],
                onChange: (newValue: Array<string>) => this.props.handleChangeValue(newValue || new Array<string>()),
                maxItems: this.props.maxItems,
                options: this.props.options,
                items: this.props.value,
                render: {
                    item: (item: SmartOption) => {
                        return `<div title="${item.subtitle}">${item.title}</div>`;
                    },
                    option: (item: SmartOption) => {
                        var title = `<div class="option-title">${item.title}</div>`;
                        var subtitle = `<div class="option-subtitle">${item.subtitle}</div>`;
                        if (item.subtitle) return `<div>${title + subtitle}</div>`;
                        return title;
                    }
                }
            }) as Selectize.IOptions<any, any>);
        }
    }

    componentDidMount() {
        this.initializeSelectize();
    }

    componentDidUpdate() {
        var selectize = $(`#${this.selectId}`)[0].selectize;
        selectize.load((callback) => {
            callback(this.props.options);
        });
        if (this.props.disabled) {
            selectize.disable();
        } else {
            selectize.enable();
        }
    }

    render() {
        return (
            <div className={`select-component multi-select-component input-sub-parent ${this.props.className ? this.props.className : ""}`}>
                <TopLabel label={this.props.label} />
                <select id={this.selectId} value={this.props.value} multiple disabled={this.props.disabled}>
                </select>
                {
                    this.props.addon
                }
            </div>
        );
    }
}