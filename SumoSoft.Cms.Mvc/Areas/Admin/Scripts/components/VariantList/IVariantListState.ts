﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";
import { User } from "../../../../../Scripts/classes/User";

export interface IVariantListState extends IReactPageState {
    authenticatedUser: User;
    variants?: Array<Variant>;
    sortedVariants?: Array<Variant>;
    pageIsLoading?: boolean;
}