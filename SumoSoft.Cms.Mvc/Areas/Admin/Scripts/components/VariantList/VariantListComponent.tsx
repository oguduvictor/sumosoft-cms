﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { VariantTypesEnum } from "../../../../../Scripts/enums/VariantTypesEnum";
import { IVariantListState } from "./IVariantListState";
import { IVariantListDto } from "./IVariantListDto";
import { Functions } from "../../shared/Functions";
import { SortableComponent, ISortableProps, ISortableRow, ISortableRowCellStyle, ISortableData } from "../Sortable/SortableComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";

export default class VariantListComponent extends ReactPageComponent<{}, IVariantListState> {

    constructor(props) {
        super(props);

        this.state = {
            variants: new Array<Variant>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IVariantListState;
    }

    componentDidMount() {
        this.setState({ pageIsLoading: true });

        sumoJS.ajaxPost<IVariantListDto>(Constants.url_Variant_IndexJson, null, (variantListDto) => {

            this.setState({
                authenticatedUser: new User(variantListDto.authenticatedUser),
                variants: variantListDto.variants.map(x => new Variant(x)),
                pageIsLoading: false
            });
        });
    }

    getSortableRows(): ISortableRow[] {
        const variants = this.state.variants.orderBy(x => x.sortOrder) as Array<Variant>;

        return variants.map(variant => {
            return {
                id: variant.id,
                contentItems: [
                    { item: variant.sortOrder },
                    { item: variant.name },
                    { item: variant.localizedTitle },
                    { item: Functions.getEnumDescription(VariantTypesEnum, variant.typeDescriptions, variant.type) },
                    {
                        item: <div
                            className="text-right"
                            style={{ paddingRight: "5px" }}>
                            <a
                                className="btn btn-primary btn-xs btn-open"
                                style={{ margin: "0 10px 0 5px" }}
                                href={Constants.url_Variant_Edit + variant.id}>
                                Edit
                                  </a>
                        </div>
                    }
                ],
                children: null,
                cellStyles: [
                    { cellIndices: [0, 1, 2, 3, 4], style: { paddingRight: "5px", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" } } as ISortableRowCellStyle,
                    { cellIndices: [0], style: { width: "90px" } } as ISortableRowCellStyle,
                    { cellIndices: [4], style: { width: "110px" } } as ISortableRowCellStyle
                ]
            } as ISortableRow;
        });
    }

    handleChangeSorting(data: Array<ISortableData>) {
        const sortedVariants = data as Array<Variant>;

        this.setState({
            sortedVariants,
            isModified: true
        });
    }

    handleSubmit() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse<Array<Variant>>>(Constants.url_Variant_UpdateSortOrder, { variantDtos: this.state.sortedVariants }, response => {
            if (response.isValid) {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess,
                    variants: response.target,
                    sortedVariants: []
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false,
                    sortedVariants: []
                });
            }
        });
    }

    render() {

        return (
            <div className="variant-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Variants
                    </div>
                    <div className="admin-top-bar-controls">
                        <a className={`btn btn-primary ${this.state.authenticatedUser.role.createVariant ? "" : "disabled"}`} href={Constants.url_Variant_Edit}>
                            <i className="fa fa-plus"> </i>
                            Create new variant
                        </a>
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            className: this.state.isModified ? "" : "disabled",
                            handleSubmit: () => this.handleSubmit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    this.state.pageIsLoading ||
                    <div className="admin-subsection">
                        <SortableComponent {...{
                            headerRowItems: ["Sort Order", "Name", "Title", "Type", ""],
                            rows: this.getSortableRows(),
                            className: "sortable-variant-options",
                            handleChangeSorting: (data) => this.handleChangeSorting(data),
                            isFixedTableLayout: true
                        } as ISortableProps} />
                    </div>
                }
                {this.state.pageIsLoading && <SpinnerComponent />}
            </div>
        );
    }
}