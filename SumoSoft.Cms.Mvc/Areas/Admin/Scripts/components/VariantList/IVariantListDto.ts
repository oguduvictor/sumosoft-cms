﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IVariantDto } from "../../../../../Scripts/interfaces/IVariantDto";

export interface IVariantListDto {
    authenticatedUser: IUserDto;
    variants?: Array<IVariantDto>;
}