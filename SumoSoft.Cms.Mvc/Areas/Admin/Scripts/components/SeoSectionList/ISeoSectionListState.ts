﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import {SeoSection} from "../../../../../Scripts/classes/SeoSection";

export interface ISeoSectionListState extends IReactPageState {
    pageIsLoading?: boolean;
    seoSections?: Array<SeoSection>;
}