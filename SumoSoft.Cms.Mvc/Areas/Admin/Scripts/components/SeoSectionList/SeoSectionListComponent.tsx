﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { TableComponent, ITableProps } from "../Table/TableComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Constants } from "../../shared/Constants";
import { ISeoSectionListState } from "./ISeoSectionListState";
import { SeoSection } from "../../../../../Scripts/classes/SeoSection";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { ISeoSectionListDto } from "./ISeoSectionListDto";

export default class SeoSectionListComponent extends React.Component<{}, ISeoSectionListState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            pageIsLoading: true,
            seoSections: new Array<SeoSection>()
        } as ISeoSectionListState;
    }

    componentDidMount() {
        sumoJS.ajaxPost<ISeoSectionListDto>(Constants.url_Seo_IndexJson, null, (seoSectionListDto) => {
            this.setState({
                authenticatedUser: new User(seoSectionListDto.authenticatedUser),
                seoSections: seoSectionListDto.seoSections.map(x => new SeoSection(x)),
                pageIsLoading: false
            });
        });
    }

    render() {
        return (
            <div className="seo-section-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Seo
                    </div>
                    <div className="admin-top-bar-controls" >
                        <a href={Constants.url_Seo_Edit} className={`btn btn-primary ${this.state.authenticatedUser.role.createSeoSection ? "" : "disabled"}`}>
                            <i className="fa fa-plus"> </i>
                            Create new seo content
                        </a>
                    </div>
                </div>
                <div className="admin-subsection">
                    <div className="table-wrapper">
                        <TableComponent {...{
                            thList: ["Page", "Meta Title", "Meta Description", ""],
                            trList: this.state.seoSections.map((x) => [x.page, x.localizedTitle, x.localizedDescription,
                            <a className="btn btn-primary btn-xs" href={Constants.url_Seo_Edit + x.id}> edit </a>])
                        } as ITableProps} />
                        {
                            this.state.pageIsLoading && <SpinnerComponent />
                        }
                    </div>
                </div>
            </div>
        );
    }
}

