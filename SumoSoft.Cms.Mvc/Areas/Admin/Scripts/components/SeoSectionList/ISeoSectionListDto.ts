﻿import { ISeoSectionDto } from "../../../../../Scripts/interfaces/ISeoSectionDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface ISeoSectionListDto {
    authenticatedUser: IUserDto;
    seoSections: Array<ISeoSectionDto>;
}