﻿import * as React from "react";
import * as dayjs from "dayjs";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { ProductAttribute } from "../../../../../Scripts/classes/ProductAttribute";
import { Attribute } from "../../../../../Scripts/classes/Attribute";
import { Product } from "../../../../../Scripts/classes/Product";
import { Constants } from "../../shared/Constants";
import { AttributeTypesEnum } from "../../../../../Scripts/enums/AttributeTypesEnum";
import { ContentSection } from "../../../../../Scripts/classes/ContentSection";
import { ContentSectionLocalizedKit } from "../../../../../Scripts/classes/ContentSectionLocalizedKit";
import { IEditProductAttributesState } from "./IEditProductAttributesState";
import { IEditProductAttributesDto } from "./IEditProductAttributesDto";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { FilePickerComponent, IFilePickerProps } from "../FilePicker/FilePickerComponent";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { ICreateContentSectionDto } from "../../interfaces/ICreateContentSectionDto";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { Autocomplete, AutocompleteOption } from "../../../../../Scripts/sumoReact/Autocomplete/Autocomplete";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";
import { ContentSectionLocalizedKitEditor } from '../EditContentSections/ContentSectionLocalizedKitEditor/ContentSectionLocalizedKitEditor';

// https://github.com/Microsoft/TypeScript/issues/3960#issuecomment-165330151
interface IContentSectionAutocomplete {
    new(): Autocomplete<ContentSection>;
}

export default class EditProductAttributesComponent extends ReactPageComponent<{}, IEditProductAttributesState> {
    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            newContentSectionErrors: new Array<IFormError>(),
            productAttributes: new Array<ProductAttribute>(),
            allAttributes: new Array<Attribute>(),
            allContentSections: new Array<ContentSection>(),
            newContentSection: new ContentSection({
                contentSectionLocalizedKits: [new ContentSectionLocalizedKit()]
            }),
            matchedContentSection: new ContentSection(),
            productAttributeOnEdit: new ProductAttribute(),
            contentSectionsSchemaNames: new Array<string>()
        } as IEditProductAttributesState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleAddProductAttribute(attribute: Attribute) {
        const productAttribute = new ProductAttribute();
        productAttribute.attribute = attribute;
        productAttribute.attributeOptionValue = null;
        productAttribute.contentSectionValue = null;
        productAttribute.product = new Product({ id: this.state.productId });

        this.state.productAttributes.unshift(productAttribute);
        this.setState({ isModified: true, productAttributes: this.state.productAttributes });
    }

    handleRemoveProductAttribute(productAttribute: ProductAttribute) {
        const index = this.state.productAttributes.indexOf(productAttribute);
        this.state.productAttributes.splice(index, 1);
        this.setState({
            isModified: true,
            productAttributes: this.state.productAttributes
        });
    }

    handleChangeSelectValue(e, productAttribute: ProductAttribute) {
        productAttribute.attributeOptionValue = productAttribute.attribute.attributeOptions.filter(x => x.id === e.target.value)[0];
        this.setState({ isModified: true, productAttributes: this.state.productAttributes });
    }

    handleChangeCheckboxValue(e, productAttribute: ProductAttribute) {
        productAttribute.booleanValue = e.target.checked;
        this.setState({ isModified: true, productAttributes: this.state.productAttributes });
    }

    handleChangeTextValue(e, productAttribute: ProductAttribute) {
        productAttribute.stringValue = e.target.value;
        this.setState({ isModified: true, productAttributes: this.state.productAttributes });
    }

    handleChangeImageValue(productAttribute: ProductAttribute, absoluteUrl: string) {
        productAttribute.imageValue = absoluteUrl;
        this.setState({ isModified: true, productAttributes: this.state.productAttributes });
    }

    handleChangeDoubleValue(e, productAttribute: ProductAttribute) {
        productAttribute.doubleValue = e.target.value;
        this.setState({ isModified: true, productAttributes: this.state.productAttributes });
    }

    handleChangeDateTimeValue(e, productAttribute: ProductAttribute) {
        productAttribute.dateTimeValue = e.target.value;
        this.setState({ isModified: true, productAttributes: this.state.productAttributes });
    }

    handleChangeContentSectionValue(productAttribute: ProductAttribute, contentSection: ContentSection) {
        const productAttributes = this.state.productAttributes.map(x => {
            if (x.id === productAttribute.id) {
                x.contentSectionValue = contentSection;
            }
            return x;
        });

        this.setState({ isModified: true, productAttributes });
    }

    handleOpenNewContentSectionModal(productAttribute: ProductAttribute) {
        const attributeTitle = Functions.convertToTitleCase(productAttribute.attribute.localizedTitle).replace(/[^a-zA-Z0-9]/g, "");
        const productTitle = Functions.convertToTitleCase(this.state.productTitle).replace(/[^a-zA-Z0-9]/g, "");
        const prefilledTitle = `Product_${attributeTitle}_${productTitle}`;

        const newContentSection = new ContentSection();
        newContentSection.name = prefilledTitle;
        newContentSection.contentSectionLocalizedKits = [
            new ContentSectionLocalizedKit({
                content: "",
                contentSection: new ContentSection({ id: newContentSection.id })
            })
        ];

        this.setState(
            {
                newContentSection,
                matchedContentSection: new ContentSection(),
                productAttributeOnEdit: productAttribute,
                newContentSectionErrors: new Array<IFormError>()
            },
            () => $("#new-content-section-add").modal("show")
        );
    }

    handleChangeNewContentSectionName(value) {
        const newContentSection = this.state.newContentSection;
        newContentSection.name = value;

        this.setState({ newContentSection });
    }

    handleChangeNewContentSectionContent(value) {
        const newContentSection = this.state.newContentSection;
        newContentSection.contentSectionLocalizedKits = newContentSection.contentSectionLocalizedKits.map(x => {
            x.content = value;
            return x;
        });

        this.setState({ newContentSection });
    }

    handleChangeNewContentSectionContentSchema(value) {
        const newContentSection = this.state.newContentSection;

        newContentSection.schema = value;

        this.setState({ newContentSection });
    }

    handleSaveContentSection(eventTarget, checkForSimilarContent: boolean) {
        $(eventTarget).prepend("<i class='fa fa-spinner fa-pulse fa-fw'></i>");

        sumoJS.ajaxPost<ICreateContentSectionDto>(
            Constants.url_Product_AddContentSection,
            {
                contentSectionDto: this.state.newContentSection,
                checkForSimilarContent
            },
            createContentSectionDto => {
                $(eventTarget)
                    .find("i")
                    .remove();

                if (createContentSectionDto.similarContentSection) {
                    this.setState({
                        isModified: true,
                        matchedContentSection: new ContentSection(createContentSectionDto.similarContentSection),
                        newContentSectionErrors: new Array<IFormError>()
                    });
                } else if (createContentSectionDto.formResponse.isValid) {
                    const savedContentSection = new ContentSection(createContentSectionDto.savedContentSection);
                    const productAttribute = this.state.productAttributes.filter(x => x.id === this.state.productAttributeOnEdit.id)[0];
                    productAttribute.contentSectionValue = savedContentSection;

                    this.setState(
                        {
                            isModified: true,
                            productAttributes: this.state.productAttributes,
                            newContentSectionErrors: new Array<IFormError>()
                        },
                        () => this.submit()
                    );

                    $("#new-content-section-add").modal("hide");
                } else {
                    this.setState({
                        isModified: false,
                        isSubmitting: false,
                        newContentSectionErrors: createContentSectionDto.formResponse.errors
                    });
                }
            }
        );
    }

    handleUseExistingContentSection() {
        const productAttributes = this.state.productAttributes.map(productAttribute => {
            if (productAttribute.id === this.state.productAttributeOnEdit.id) {
                productAttribute.contentSectionValue = this.state.matchedContentSection;
            }
            return productAttribute;
        });

        this.setState({
            isModified: true,
            productAttributes: productAttributes.map(x => new ProductAttribute(x)),
            newContentSectionErrors: new Array<IFormError>()
        });

        $("#new-content-section-add").modal("hide");
    }

    submit() {
        const model = {
            productId: this.state.productId,
            productAttributes: this.state.productAttributes
        } as IEditProductAttributesDto;

        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Product_AddOrUpdateProductAttributes, model, response => {
            if (response.isValid) {
                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditProductAttributesDto>(
            Constants.url_Product_EditProductAttributesJson,
            { productId: sumoJS.getQueryStringParameter("productId") },
            editProductAttributesDto => {
                this.setState(
                    {
                        authenticatedUser: new User(editProductAttributesDto.authenticatedUser),
                        productId: editProductAttributesDto.productId,
                        productTitle: editProductAttributesDto.productTitle,
                        productAttributes: editProductAttributesDto.productAttributes.map(x => new ProductAttribute(x)),
                        allAttributes: editProductAttributesDto.allAttributes.map(x => new Attribute(x)),
                        allContentSections: editProductAttributesDto.allContentSections.map(x => new ContentSection(x)),
                        pageIsLoading: false,
                        contentSectionsSchemaNames: editProductAttributesDto.contentSectionsSchemaNames
                    },
                    () =>
                        Functions.setCreateAndEditPermissions(
                            new Product(),
                            this.state.authenticatedUser.role.createProduct,
                            this.state.authenticatedUser.role.editProduct
                        )
                );
            }
        );
    }

    renderProductAttributesSection() {
        const ContentSectionAutocomplete = Autocomplete as IContentSectionAutocomplete;

        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">Product Attributes</div>
                <div>
                    {this.state.productAttributes.map(productAttribute => {
                        var template: JSX.Element;

                        if (productAttribute.attribute.type === AttributeTypesEnum.Options) {
                            //---------------------------------------------------------
                            // TYPE: OPTIONS
                            //---------------------------------------------------------

                            template = (
                                <SelectComponent
                                    {...{
                                        key: productAttribute.id,
                                        options: productAttribute.attribute.attributeOptions
                                            .orderBy(x => x.name)
                                            .map(x => (
                                                <option key={x.id} value={x.id}>
                                                    {x.name}
                                                </option>
                                            )),
                                        value: productAttribute.attributeOptionValue ? productAttribute.attributeOptionValue.id : "",
                                        label: productAttribute.attribute.localizedTitle,
                                        neutralOption: <option />,
                                        handleChangeValue: e => this.handleChangeSelectValue(e, productAttribute)
                                    } as ISelectProps}
                                />
                            );
                        } else if (productAttribute.attribute.type === AttributeTypesEnum.Boolean) {
                            //---------------------------------------------------------
                            // TYPE: BOOLEAN
                            //---------------------------------------------------------

                            template = (
                                <CheckboxComponent
                                    {...{
                                        label: "Boolean Attribute",
                                        text: productAttribute.attribute.localizedTitle,
                                        value: productAttribute.booleanValue,
                                        handleChangeValue: e => this.handleChangeCheckboxValue(e, productAttribute)
                                    } as ICheckboxProps}
                                />
                            );
                        } else if (productAttribute.attribute.type === AttributeTypesEnum.String) {
                            //---------------------------------------------------------
                            // TYPE: TEXT
                            //---------------------------------------------------------

                            template = (
                                <TextboxComponent
                                    {...{
                                        label: productAttribute.attribute.localizedTitle,
                                        value: productAttribute.stringValue,
                                        type: "text",
                                        handleChangeValue: e => this.handleChangeTextValue(e, productAttribute)
                                    } as ITextboxProps}
                                />
                            );
                        } else if (productAttribute.attribute.type === AttributeTypesEnum.Image) {
                            //---------------------------------------------------------
                            // TYPE: IMAGE
                            //---------------------------------------------------------

                            template = (
                                <FilePickerComponent
                                    {...{
                                        label: productAttribute.attribute.localizedTitle,
                                        value: productAttribute.imageValue,
                                        handleChangeValue: absoluteUrl => this.handleChangeImageValue(productAttribute, absoluteUrl)
                                    } as IFilePickerProps}
                                />
                            );
                        } else if (productAttribute.attribute.type === AttributeTypesEnum.Double) {
                            //---------------------------------------------------------
                            // TYPE: INTEGER
                            //---------------------------------------------------------

                            template = (
                                <TextboxComponent
                                    {...{
                                        label: productAttribute.attribute.localizedTitle,
                                        value: productAttribute.doubleValue,
                                        type: "number",
                                        handleChangeValue: e => this.handleChangeDoubleValue(e, productAttribute)
                                    } as ITextboxProps}
                                />
                            );
                        } else if (productAttribute.attribute.type === AttributeTypesEnum.DateTime) {
                            //---------------------------------------------------------
                            // TYPE: DATETIME
                            //---------------------------------------------------------

                            template = (
                                <TextboxComponent
                                    {...{
                                        label: productAttribute.attribute.localizedTitle,
                                        type: "date",
                                        value: dayjs(productAttribute.dateTimeValue).format("YYYY-MM-DD"),
                                        handleChangeValue: e => this.handleChangeDateTimeValue(e, productAttribute)
                                    } as ITextboxProps}
                                />
                            );
                        } else if (productAttribute.attribute.type === AttributeTypesEnum.ContentSection) {
                            //---------------------------------------------------------
                            // TYPE: CONTENT SECTION
                            //---------------------------------------------------------

                            template = (
                                <ContentSectionAutocomplete
                                    label={productAttribute.attribute.localizedTitle}
                                    options={this.state.allContentSections.map(x => new AutocompleteOption(x, x.name))}
                                    selectedOption={
                                        new AutocompleteOption(
                                            productAttribute.contentSectionValue,
                                            productAttribute.contentSectionValue && productAttribute.contentSectionValue.name
                                        )
                                    }
                                    handleChangeValue={(contentSection: ContentSection) => this.handleChangeContentSectionValue(productAttribute, contentSection)}
                                />
                            );
                        } else return null;

                        return (
                            <div className="row form-group" key={productAttribute.id}>
                                <div className="col-md-9">
                                    {template}
                                </div>
                                <div className="col-md-3">
                                    {productAttribute.attribute.type === AttributeTypesEnum.ContentSection && productAttribute.contentSectionValue && (
                                        <a
                                            href={`${Constants.url_ContentSection_Index}?filter=${productAttribute.contentSectionValue.name}`}
                                            target="_blank"
                                            title="Edit"
                                            className="btn btn-default">
                                            <i className="fa fa-edit" />
                                        </a>
                                    )}
                                    {productAttribute.attribute.type === AttributeTypesEnum.ContentSection && (
                                        <button type="button" title="Create" className="btn btn-default" onClick={() => this.handleOpenNewContentSectionModal(productAttribute)}>
                                            <i className="fa fa-plus" />
                                        </button>
                                    )}
                                    <button type="button" title="Delete" className="btn btn-default" onClick={() => this.handleRemoveProductAttribute(productAttribute)}>
                                        <i className="fa fa-trash-o" />
                                    </button>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }

    renderAttributesSection() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">Available Attributes</div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th />
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.allAttributes.map(attribute => {
                            return (
                                <tr key={attribute.id}>
                                    <td>{attribute.localizedTitle}</td>
                                    <td>{AttributeTypesEnum[attribute.type]}</td>
                                    <td>
                                        {!this.state.productAttributes.some(x => x.attribute.id === attribute.id) && (
                                            <button type="submit" className="btn btn-primary btn-xs" onClick={() => this.handleAddProductAttribute(attribute)}>
                                                <i className="fa fa-plus-circle" /> Add
                                            </button>
                                        )}
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

    render() {
        return (
            <div className="edit-product-attributes-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">Edit Product Attributes for {this.state.productTitle}</div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Product_Index} className="btn btn-link">
                            Back to list
                        </a>
                        <div className="dropdown">
                            <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                Edit... &nbsp;
                                <span className="caret" />
                            </button>
                            <ul className="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href={Constants.url_Product_Edit + this.state.productId}>Product</a>
                                </li>
                                <li className="disabled">
                                    <a>Product Attributes</a>
                                </li>
                                <li>
                                    <a href={Constants.url_Product_EditProductVariants + this.state.productId}>Product Variants</a>
                                </li>
                                <li>
                                    <a href={Constants.url_Product_EditProductImageKits + this.state.productId}>Product Images</a>
                                </li>
                                <li>
                                    <a href={Constants.url_Product_EditProductStockUnits + this.state.productId}>Product Stock</a>
                                </li>
                            </ul>
                        </div>
                        <ButtonSubmitComponent
                            {...{
                                isSubmitting: this.state.isSubmitting,
                                handleSubmit: () => this.submit()
                            } as IButtonSubmitProps}
                        />
                    </div>
                </div>
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                {!this.state.pageIsLoading && (
                    <div className="row">
                        <div className="col-md-7">{this.renderProductAttributesSection()}</div>
                        <div className="col-md-5">{this.renderAttributesSection()}</div>
                    </div>
                )}
                {this.state.pageIsLoading && <SpinnerComponent />}
                <ModalComponent
                    {...{
                        id: "new-content-section-add",
                        title: "New Content Section",
                        body: (
                            <div>
                                <div className="modal-errors">
                                    {this.state.newContentSectionErrors.map((x, i) => (
                                        <p key={i}>{x.message}</p>
                                    ))}
                                </div>
                                {this.state.matchedContentSection.name && (
                                    <p>
                                        {`A Content Section with name "${this.state.matchedContentSection.name}" exists with similar content "${
                                            this.state.matchedContentSection.contentSectionLocalizedKits[0].content
                                            }". Do you want to associate this content to the productattribute "${this.state.productAttributeOnEdit.attribute.localizedTitle}" ?`}
                                    </p>
                                )}
                                {!this.state.matchedContentSection.name &&
                                    <ContentSectionLocalizedKitEditor
                                        content={this.state.newContentSection.contentSectionLocalizedKits[0].content}
                                        contentSectionName={this.state.newContentSection.name}
                                        contentSectionSchemaNames={this.state.contentSectionsSchemaNames}
                                        contentSectionSchema={this.state.newContentSection.schema}
                                        canEditContentSectionName={!Functions.getReadOnlyStateForRequiredField(this.state.newContentSection, this.state.authenticatedUser.role.editContentSectionName)}
                                        canEditContentSectionContent={!Functions.getReadOnlyStateForRequiredField(this.state.newContentSection, this.state.authenticatedUser.role.editContentSectionAllLocalizedKits)}
                                        handleContentSectionNameChange={value => this.handleChangeNewContentSectionName(value) /*this.handleUpdateContentSectionName(value)*/}
                                        handleContentSectionSchemaChange={value => this.handleChangeNewContentSectionContentSchema(value) /*this.handleUpdateContentSectionSchema(value)*/}
                                        handleContentChange={value => this.handleChangeNewContentSectionContent(value) /*this.handleUpdateContentSectionLocalizedKit(value)*/}
                                    />}
                            </div>
                        ),
                        footer: (
                            <div>
                                {this.state.matchedContentSection.name && (
                                    <div>
                                        <button className="btn btn-primary" type="button" onClick={() => this.handleUseExistingContentSection()}>
                                            Use Existing
                                        </button>
                                        <button type="button" className="btn btn-default" onClick={e => this.handleSaveContentSection(e.target, false)}>
                                            Create New
                                        </button>
                                    </div>
                                )}
                                {!this.state.matchedContentSection.name && (
                                    <div>
                                        <button className="btn btn-primary" type="button" onClick={e => this.handleSaveContentSection(e.target, true)}>
                                            Create
                                        </button>
                                        <button type="button" className="btn btn-default" data-dismiss="modal">
                                            Cancel
                                        </button>
                                    </div>
                                )}
                            </div>
                        )
                    } as IModalProps}
                />
            </div>
        );
    }
}
