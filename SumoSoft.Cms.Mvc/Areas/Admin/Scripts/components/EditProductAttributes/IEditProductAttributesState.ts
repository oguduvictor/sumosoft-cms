﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { ProductAttribute } from "../../../../../Scripts/classes/ProductAttribute";
import { Attribute } from "../../../../../Scripts/classes/Attribute";
import { ContentSection } from "../../../../../Scripts/classes/ContentSection";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export interface IEditProductAttributesState extends IReactPageState {
    productId?: string;
    productTitle?: string;
    pageIsLoading?: boolean;
    productAttributes?: Array<ProductAttribute>;
    allAttributes?: Array<Attribute>;
    allContentSections?: Array<ContentSection>;
    newContentSection?: ContentSection;
    matchedContentSection?: ContentSection;
    productAttributeOnEdit?: ProductAttribute;
    newContentSectionErrors?: Array<IFormError>;
    contentSectionsSchemaNames: Array<string>;
}
