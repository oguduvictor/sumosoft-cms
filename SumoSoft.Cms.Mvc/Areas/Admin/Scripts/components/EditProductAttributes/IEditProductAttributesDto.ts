﻿import { IAttributeDto } from "../../../../../Scripts/interfaces/IAttributeDto";
import { IContentSectionDto } from "../../../../../Scripts/interfaces/IContentSectionDto";
import { IProductAttributeDto } from "../../../../../Scripts/interfaces/IProductAttributeDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditProductAttributesDto {
    authenticatedUser?: IUserDto;
    productId?: string;
    productTitle?: string;
    productAttributes?: Array<IProductAttributeDto>;
    allAttributes?: Array<IAttributeDto>;
    allContentSections?: Array<IContentSectionDto>;
    contentSectionsSchemaNames?: Array<string>;
}
