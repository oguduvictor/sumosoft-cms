﻿import * as React from "react";
import { User } from "../../../../../Scripts/classes/User";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export interface IReactPageState {
    authenticatedUser?: User;
    formSuccessMessage?: string;
    formErrors?: Array<IFormError>;
    isModified?: boolean;
    isSubmitting?: boolean;
    isDeleting?: boolean;
    pageIsLoading?: boolean;
}

export class ReactPageComponent<P, S extends IReactPageState> extends React.Component<P, S>  {
    constructor(props: Readonly<P>) {
        super(props);

        this.handleHideAlert = this.handleHideAlert.bind(this);

        window.onbeforeunload = () => this.state.isModified ? "Changes that you made may not be saved" : null;
    }

    getFormErrorMessages(): Array<string> {
        const formErrors = this.state.formErrors || new Array<IFormError>();
        return formErrors.map(x => x.message);
    }

    handleHideAlert() {
        $(".alert").slideUp(1000, () => this.setState({ formErrors: new Array<IFormError>(), formSuccessMessage: null }));
    }
}