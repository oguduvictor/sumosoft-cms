﻿import { IAttributeDto } from "../../../../../Scripts/interfaces/IAttributeDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IAttributeListDto {
    authenticatedUser: IUserDto;
    attributes?: Array<IAttributeDto>;
}
