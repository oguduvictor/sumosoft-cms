﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Constants } from "../../shared/Constants";
import { IAttributeListState } from "./IAttributeListState";
import { Attribute } from "../../../../../Scripts/classes/Attribute";
import { IAttributeListDto } from "./IAttributeListDto";
import { AttributeTypesEnum } from "../../../../../Scripts/enums/AttributeTypesEnum";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Functions } from "../../shared/Functions";
import { SortableComponent, ISortableData, ISortableProps, ISortableRow, ISortableRowCellStyle } from "../Sortable/SortableComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";

export default class AttributeListComponent extends ReactPageComponent<{}, IAttributeListState> {
    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            attributes: new Array<Attribute>(),
            sortedAttributes: new Array<Attribute>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IAttributeListState;
    }

    componentDidMount() {
        sumoJS.ajaxPost<IAttributeListDto>(Constants.url_Attribute_IndexJson, null, attributeList => {
            this.setState({
                authenticatedUser: new User(attributeList.authenticatedUser),
                attributes: attributeList.attributes.map(x => new Attribute(x)),
                pageIsLoading: false
            });
        });
    }
    
    getSortableRows(): ISortableRow[] {
        const attributes = this.state.attributes.orderBy(x => x.sortOrder) as Array<Attribute>;

        return attributes.map(attribute => {
            return {
                id: attribute.id,
                contentItems: [
                    { item: attribute.sortOrder},
                    { item: attribute.name},
                    { item: attribute.localizedTitle} ,
                    { item: Functions.getEnumDescription(AttributeTypesEnum, attribute.typeDescriptions, attribute.type)} ,
                    {
                        item: <div
                                  className="text-right"
                                  style={{ paddingRight: "5px" }}>
                                  <a
                                      className="btn btn-primary btn-xs btn-open"
                                      style={{ margin: "0 10px 0 5px" }}
                                      href={Constants.url_Attribute_Edit + attribute.id}>
                                      Edit
                                  </a>
                              </div>
                    } 
                ],
                children: null,
                cellStyles: [
                    { cellIndices: [0, 1, 2, 3, 4], style: { paddingRight: "5px", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" } } as ISortableRowCellStyle,
                    { cellIndices: [0], style: { width: "90px" } } as ISortableRowCellStyle,
                    { cellIndices: [4], style: { width: "110px" } } as ISortableRowCellStyle
                ]
            } as ISortableRow;
        });
    }

    handleChangeSorting(data: Array<ISortableData>) {
        const sortedAttributes = data as Array<Attribute>;
        
        this.setState({
            sortedAttributes,
            isModified: true
        });
    }

    handleSubmit() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse<Array<Attribute>>>(Constants.url_Attribute_UpdateSortOrder, { attributeDtos: this.state.sortedAttributes }, response => {
            if (response.isValid) {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess,
                    attributes: response.target,
                    sortedAttributes: []
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false,
                    sortedAttributes: []
                });
            }
        });
    }

    render() {
        return (
            <div className="attribute-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">Attributes</div>
                    <div className="admin-top-bar-controls">
                        <a className={`btn btn-primary ${this.state.authenticatedUser.role.createAttribute ? "" : "disabled"}`} href={Constants.url_Attribute_Edit}>
                            <i className="fa fa-plus"> </i>
                            Create new attribute
                        </a>
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            className: this.state.isModified ? "" : "disabled",
                            handleSubmit: () => this.handleSubmit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-12">
                            <div className="admin-subsection">
                                <SortableComponent {...{
                                    headerRowItems: ["Sort Order", "Name", "Title", "Type", ""],
                                    rows: this.getSortableRows(),
                                    className: "sortable-attribute-options",
                                    handleChangeSorting: (data) => this.handleChangeSorting(data),
                                    isFixedTableLayout: true
                                } as ISortableProps} />
                            </div>
                        </div>
                    </div>
                }
                { (this.state.pageIsLoading || this.state.isSubmitting) && <SpinnerComponent /> }
            </div>
        );
    }
}
