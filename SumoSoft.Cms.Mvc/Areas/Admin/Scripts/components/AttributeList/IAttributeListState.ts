﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Attribute } from "../../../../../Scripts/classes/Attribute";

export interface IAttributeListState extends IReactPageState {
    attributes?: Array<Attribute>;
    sortedAttributes?: Array<Attribute>;
}
