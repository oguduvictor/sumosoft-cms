﻿import * as React from "react";

import { VariantOption } from "../../../../../Scripts/classes/VariantOption";
import { Constants } from "../../shared/Constants";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { SortableComponent, ISortableProps, ISortableRow, ISortableRowCellStyle } from "../Sortable/SortableComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";

export interface IVariantOptionsProps {
    variant: Variant;
    variantOptions: Array<VariantOption>;
    role: UserRole;
    handleDeleteVariantOption(variantOption: VariantOption): void;
    shouldComponentUpdate: boolean;
}

export class VariantOptionsComponent extends React.Component<IVariantOptionsProps> {

    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps: IVariantOptionsProps, nextState) {
        return nextProps.shouldComponentUpdate;
    }

    getSortableRows(variantOptions: Array<VariantOption>) {
        variantOptions = variantOptions.orderBy(x => x.sortOrder);
        return variantOptions.map(variantOption => {
            return {
                id: variantOption.id,
                contentItems: [
                    { item: variantOption.sortOrder },
                    { item: variantOption.name },
                    { item: variantOption.url },
                    { item: variantOption.code },
                    {
                        item: <div
                            className="text-right"
                            style={{ paddingRight: "5px" }}>
                            <a
                                className="btn btn-primary btn-xs btn-open"
                                style={{ margin: "0 10px 0 5px" }}
                                href={`${Constants.url_Variant_EditVariantOption}${this.props.variant.id}&variantOptionId=${variantOption.id}`}>
                                Edit
                                  </a>
                        </div>
                    }
                ],
                children: null,
                cellStyles: [
                    { cellIndices: [0, 1, 2, 3, 4], style: { paddingRight: "5px", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" } } as ISortableRowCellStyle,
                    { cellIndices: [0], style: { width: "90px" } } as ISortableRowCellStyle,
                    { cellIndices: [4], style: { width: "110px" } } as ISortableRowCellStyle
                ]
            } as ISortableRow;
        });
    }

    render() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Variant Options
                </div>
                <div className="admin-subsection-controls">
                    {
                        this.props.variant.isNew &&
                        <div className="text-info">Click "Save" to add VariantOptions</div>
                    }
                    {
                        !this.props.variant.isNew &&
                        <a className={`btn btn-link ${this.props.role.createVariantOption ? "" : "disabled"}`}
                            href={Constants.url_Variant_EditVariantOption + this.props.variant.id}>
                            <span className="glyphicon glyphicon-plus"></span>
                            &nbsp; Add new option
                        </a>
                    }
                </div>
                {
                    <SortableComponent {...{
                        headerRowItems: ["Sort Order", "Name", "Url", "Code", ""],
                        rows: this.getSortableRows(this.props.variantOptions),
                        className: "sortable-variant-options",
                        isFixedTableLayout: true
                    } as ISortableProps} />
                }
            </div>
        );
    }
}