﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";

export interface IEditVariantState extends IReactPageState {
    variant?: Variant;
    variantOptionComponentShouldUpdate?: boolean;
}