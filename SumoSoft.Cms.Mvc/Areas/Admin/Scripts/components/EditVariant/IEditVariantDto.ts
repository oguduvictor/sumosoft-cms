﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IVariantDto } from "../../../../../Scripts/interfaces/IVariantDto";

export interface IEditVariantDto {
    allVariantSelectors: Array<string>;
    authenticatedUser: IUserDto;
    variant: IVariantDto;
}