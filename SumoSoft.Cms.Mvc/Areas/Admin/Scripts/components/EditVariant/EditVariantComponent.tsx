﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";
import { VariantOption } from "../../../../../Scripts/classes/VariantOption";
import { VariantLocalizedKit } from "../../../../../Scripts/classes/VariantLocalizedKit";
import { Constants } from "../../shared/Constants";
import { VariantTypesEnum } from "../../../../../Scripts/enums/VariantTypesEnum";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { IEditVariantState } from "./IEditVariantState";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IEditVariantDto } from "./IEditVariantDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { VariantOptionsComponent, IVariantOptionsProps } from "./VariantOptionsComponent";
import { Functions } from "../../shared/Functions";
import { ITextareaProps, TextareaComponent } from "../Textarea/TextareaComponent";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditVariantComponent extends ReactPageComponent<{}, IEditVariantState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            variant: new Variant(),
            variantOptions: new Array<VariantOption>(),
            authenticatedUser: new User({ role: new UserRole() }),
            variantOptionComponentShouldUpdate: false
        } as IEditVariantState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleChangeVariantName(e) {
        this.state.variant.name = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleBlurVariantName(e) {
        if (e.target.value !== "" && (!this.state.variant.url || this.state.variant.url === "")) {
            this.state.variant.url = Functions.convertToUrl(this.state.variant.name);
            this.setState({ isModified: true, variant: this.state.variant });
        }
    }

    handleChangeVariantUrl(e) {
        this.state.variant.url = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantType(e) {
        this.state.variant.type = Number(e.target.value);
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantDefaultDoubleValue(e) {
        this.state.variant.defaultDoubleValue = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantDefaultIntegerValue(e) {
        this.state.variant.defaultIntegerValue = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantDefaultStringValue(e) {
        this.state.variant.defaultStringValue = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantDefaultJsonValue(e) {
        this.state.variant.defaultJsonValue = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantDefaultBooleanValue(e) {
        this.state.variant.defaultBooleanValue = e.target.checked;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantDefaultOptionValue(e) {
        this.state.variant.defaultVariantOptionValue = this.state.variant.variantOptions.filter(x => x.name === e.target.value)[0];
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantSortOrder(e) {
        this.state.variant.sortOrder = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantCreateProductImageKits(e) {
        this.state.variant.createProductImageKits = e.target.checked;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantCreateProductStockUnits(e) {
        this.state.variant.createProductStockUnits = e.target.checked;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantLocalizedKitTitle(e, variantLocalizedKit: VariantLocalizedKit) {
        variantLocalizedKit.title = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleChangeVariantLocalizedKitDescription(e, variantLocalizedKit: VariantLocalizedKit) {
        variantLocalizedKit.description = e.target.value;
        this.setState({ isModified: true, variant: this.state.variant });
    }

    handleDeleteVariant() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Variant_Delete, { id: this.state.variant.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Variant;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleDeleteVariantOption(variantOption: VariantOption) {
        this.state.variant.variantOptions = this.state.variant.variantOptions.filter(x => x.id !== variantOption.id);
        this.setState({
            variant: this.state.variant,
            variantOptionComponentShouldUpdate: true
        });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Variant_DeleteVariantOption, { id: variantOption.id }, response => {
            if (response.isValid) {
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    variantOptionComponentShouldUpdate: false
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    variantOptionComponentShouldUpdate: false
                })
            }
        });
    }

    handleLocalizedKitCopyValuesFrom(source, target, completed) {
        target.title = source.title;

        this.setState({
            isModified: true,
            variant: this.state.variant
        }, () => completed());
    }

    submit() {
        this.setState({ isSubmitting: true });

        const variant = sumoJS.deepClone(this.state.variant);

        if (!this.state.variant.isNew) {
            const sortedVariantOptions = $(".sortable-variant-options .dd").nestable("serialize") as Array<VariantOption>;
            variant.variantOptions = sortedVariantOptions;
        }

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Variant_AddOrUpdate, variant, response => {

            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi(true);
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi(variantOptionComponentShouldUpdate?: boolean) {
        sumoJS.ajaxPost<IEditVariantDto>(Constants.url_Variant_EditJson,
            {
                variantId: sumoJS.getQueryStringParameter("variantId")
            },
            (editVariantDto) => {
                const newState = {
                    allVariantSelectors: editVariantDto.allVariantSelectors,
                    authenticatedUser: new User(editVariantDto.authenticatedUser),
                    variant: new Variant(editVariantDto.variant),
                    pageIsLoading: false
                } as IEditVariantState;

                if (variantOptionComponentShouldUpdate) {
                    newState.variantOptionComponentShouldUpdate = true;
                }

                this.setState(newState, () => {
                    Functions.setCreateAndEditPermissions(
                        this.state.variant,
                        this.state.authenticatedUser.role.createVariant,
                        this.state.authenticatedUser.role.editVariant);
                    if (this.state.variantOptionComponentShouldUpdate) {
                        this.setState({
                            variantOptionComponentShouldUpdate: false
                        });
                    }
                });
            });
    }

    renderGlobalProperties() {

        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Global properties
                </div>
                <TextboxComponent {...{
                    label: "Name",
                    type: "text",
                    value: this.state.variant.name,
                    tooltip: Constants.tooltip_name,
                    className: "form-group",
                    readOnly: Functions.getReadOnlyStateForRequiredField(
                        this.state.variant,
                        this.state.authenticatedUser.role.editVariantName),
                    handleChangeValue: (e) => this.handleChangeVariantName(e),
                    handleBlur: (e) => this.handleBlurVariantName(e)
                } as ITextboxProps} />
                <TextboxComponent {...{
                    label: "Url",
                    type: "text",
                    value: this.state.variant.url,
                    tooltip: Constants.tooltip_url,
                    className: "form-group",
                    handleChangeValue: (e) => this.handleChangeVariantUrl(e)
                } as ITextboxProps} />
                <div className="form-group">
                    <SelectComponent {...{
                        options: Object.keys(VariantTypesEnum).filter(x => typeof VariantTypesEnum[x] !== "number").map((value, i) =>
                            <option key={value} value={value}>{this.state.variant.typeDescriptions[i]}</option>),
                        value: this.state.variant.type ? this.state.variant.type.toString() : "",
                        label: "Type",
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeVariantType(e)
                    } as ISelectProps} />
                </div>
                {
                    this.state.variant &&
                    this.state.variant.type === VariantTypesEnum.Double &&
                    <TextboxComponent {...{
                        label: "Default Double Value",
                        type: "number",
                        value: this.state.variant.defaultDoubleValue || 0,
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeVariantDefaultDoubleValue(e)
                    } as ITextboxProps} />
                }
                {
                    this.state.variant &&
                    this.state.variant.type === VariantTypesEnum.Integer &&
                    <TextboxComponent {...{
                        label: "Default Integer Value",
                        type: "number",
                        value: this.state.variant.defaultIntegerValue || 0,
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeVariantDefaultIntegerValue(e)
                    } as ITextboxProps} />
                }
                {
                    this.state.variant && this.state.variant.type === VariantTypesEnum.String &&
                    <TextboxComponent {...{
                        label: "Default String Value",
                        type: "text",
                        value: this.state.variant.defaultStringValue || "",
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeVariantDefaultStringValue(e)
                    } as ITextboxProps} />
                }
                {
                    this.state.variant &&
                    this.state.variant.type === VariantTypesEnum.Json &&
                    <TextareaComponent {...{
                        label: "Default Json Value",
                        type: "text",
                        value: this.state.variant.defaultJsonValue || "",
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeVariantDefaultJsonValue(e)
                    } as ITextareaProps} />
                }
                {
                    this.state.variant &&
                    this.state.variant.type === VariantTypesEnum.Boolean &&
                    <CheckboxComponent {...{
                        text: "Default Boolean Value",
                        value: this.state.variant.defaultBooleanValue || false,
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeVariantDefaultBooleanValue(e)
                    } as ICheckboxProps} />
                }
                {
                    // --------------------------------------------------------------
                    //  Name is used as the value instead of id because all new VariantOptions added will have
                    //  the same new Guid as id thereby making it impossible to select a defaultVariantOption
                    // --------------------------------------------------------------
                    this.state.variant && this.state.variant.type === VariantTypesEnum.Options &&
                    <div className="form-group">
                        <SelectComponent {...{
                            options: this.state.variant.variantOptions.map(variantOption =>
                                <option key={variantOption.id} value={variantOption.name}>{variantOption.localizedTitle || variantOption.name}</option>),
                            value: this.state.variant.defaultVariantOptionValue ? this.state.variant.defaultVariantOptionValue.name : "",
                            neutralOption: (!this.state.variant.defaultVariantOptionValue || !this.state.variant.variantOptions.length) && <option value="">Select default option</option>,
                            label: "Default Option",
                            className: "form-group",
                            handleChangeValue: (e) => this.handleChangeVariantDefaultOptionValue(e)
                        } as ISelectProps} />
                    </div>
                }
                <TextboxComponent {...{
                    label: "Sort order",
                    type: "number",
                    value: this.state.variant.sortOrder || 0,
                    className: "form-group",
                    handleChangeValue: (e) => this.handleChangeVariantSortOrder(e)
                } as ITextboxProps} />
                <CheckboxComponent {...{
                    text: "Create ProductImageKits",
                    value: this.state.variant.createProductImageKits || false,
                    className: "form-group",
                    handleChangeValue: (e) => this.handleChangeVariantCreateProductImageKits(e)
                } as ICheckboxProps} />
                <CheckboxComponent {...{
                    text: "Create ProductStockUnits",
                    value: this.state.variant.createProductStockUnits || false,
                    className: "form-group",
                    handleChangeValue: (e) => this.handleChangeVariantCreateProductStockUnits(e)
                } as ICheckboxProps} />
            </div>
        );
    }

    renderLocalizedProperties() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Localized properties
                </div>
                <LocalizedKitsComponent {...{
                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source, target, completed),
                    editAllLocalizedKits: this.state.authenticatedUser.role.editVariantAllLocalizedKits,
                    localizedKitUIs: this.state.variant.variantLocalizedKits.map((variantLocalizedKit, i) => {
                        return {
                            localizedKit: variantLocalizedKit,
                            html:
                                <div>
                                    <NestedInputComponent {...{
                                        input: <TextboxComponent {...{
                                            label: "Title",
                                            type: "text",
                                            value: variantLocalizedKit.title,
                                            placeholder: this.state.variant.variantLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                            handleChangeValue: (e) => this.handleChangeVariantLocalizedKitTitle(e, variantLocalizedKit)
                                        } as ITextboxProps} />
                                    } as INestedInputProps} />
                                    <NestedInputComponent {...{
                                        input: <TextboxComponent {...{
                                            label: "Description",
                                            type: "text",
                                            value: variantLocalizedKit.description,
                                            placeholder: this.state.variant.variantLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                            handleChangeValue: (e) => this.handleChangeVariantLocalizedKitDescription(e, variantLocalizedKit)
                                        } as ITextboxProps} />
                                    } as INestedInputProps} />
                                </div>
                        } as ILocalizedKitUI;
                    })
                } as ILocalizedKitsProps} />
            </div>
        );
    }

    render() {
        return (
            <div className="edit-variant-component">
                {
                    this.state.variant.isNew &&
                    <div className="admin-top-bar">
                        <div className="admin-top-bar-title">
                            Create Variant
                        </div>
                        <div className="admin-top-bar-controls">
                            <a href={Constants.url_Variant} className="btn btn-link">
                                Back to list
                            </a>
                            <ButtonSubmitComponent {...{
                                isSubmitting: this.state.isSubmitting,
                                handleSubmit: () => this.submit()
                            } as IButtonSubmitProps} />
                        </div>
                    </div>
                }
                {
                    !this.state.variant.isNew &&
                    <div className="admin-top-bar">
                        <div className="admin-top-bar-title">
                            Edit Variant
                        </div>
                        <div className="admin-top-bar-controls">
                            <a href={Constants.url_Variant} className="btn btn-link">
                                Back to list
                            </a>
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                canDelete: this.state.authenticatedUser.role.deleteVariant,
                                handleDelete: () => this.handleDeleteVariant()
                            } as IButtonDeleteProps} />
                            <ButtonSubmitComponent {...{
                                isSubmitting: this.state.isSubmitting,
                                canSubmit: this.state.authenticatedUser.role.editVariant,
                                handleSubmit: () => this.submit()
                            } as IButtonSubmitProps} />
                        </div>
                    </div>
                }
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    this.state.pageIsLoading ||
                    <div className="row">
                        <div className="col-md-4">
                            {
                                this.renderGlobalProperties()
                            }
                            {
                                this.renderLocalizedProperties()
                            }
                        </div>
                        <div className="col-md-8">
                            <VariantOptionsComponent {...{
                                variant: this.state.variant,
                                variantOptions: this.state.variant.variantOptions,
                                role: this.state.authenticatedUser.role,
                                handleDeleteVariantOption: (variantOption) => this.handleDeleteVariantOption(variantOption),
                                shouldComponentUpdate: this.state.variantOptionComponentShouldUpdate
                            } as IVariantOptionsProps} />
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}