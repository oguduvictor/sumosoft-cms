﻿import { IProductImageKitDto } from "../../../../../Scripts/interfaces/IProductImageKitDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditProductImageKitsDto {
    authenticatedUser?: IUserDto;
    productId: string;
    productTitle: string;
    productImageKits: Array<IProductImageKitDto>;
}
