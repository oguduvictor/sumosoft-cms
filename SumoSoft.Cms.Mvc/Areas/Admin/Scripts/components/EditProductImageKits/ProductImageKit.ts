﻿import { ProductImageKit as BaseEcommerceProductImageKit } from "../../../../../Scripts/classes/ProductImageKit";
import { ProductImage } from "./ProductImage";
import { IProductImageKitDto } from "../../../../../Scripts/interfaces/IProductImageKitDto";

export class ProductImageKit extends BaseEcommerceProductImageKit {
    productImages = new Array<ProductImage>();

    constructor(dto?: IProductImageKitDto | any) {
        super(dto);

        dto = dto || {} as IProductImageKitDto;
        this.productImages = dto.productImages ? dto.productImages.map(x => new ProductImage(x)) : new Array<ProductImage>();
    }
}