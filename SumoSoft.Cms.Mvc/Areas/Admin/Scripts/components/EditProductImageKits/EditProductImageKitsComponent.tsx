﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { FilePickerComponent, IFilePickerProps } from "../FilePicker/FilePickerComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { Constants } from "../../shared/Constants";
import { IEditProductImagesState } from "./IEditProductImagesState";
import { IEditProductImageKitsDto } from "./IEditProductImageKitsDto";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { Product } from "../../../../../Scripts/classes/Product";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";
import { SortableComponent, ISortableProps, ISortableRow, ISortableData, ISortableRowCellStyle, ISortableContentItem } from "../Sortable/SortableComponent";
import { ProductImageKit } from "./ProductImageKit";
import { ProductImage } from "./ProductImage";

export default class EditProductImageKitsComponent extends ReactPageComponent<{}, IEditProductImagesState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            productImageKits: new Array<ProductImageKit>(),
        } as IEditProductImagesState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleAddProductImage(productImageKit: ProductImageKit) {
        const productImage = new ProductImage();
        productImage.isNew = true;
        productImage.tempId = Functions.newGuid();
        productImage.productImageKit = new ProductImageKit();
        productImage.productImageKit.id = productImageKit.id;

        const productImageKits = this.state.productImageKits.map(x => {
            if (x.id === productImageKit.id) {
                productImageKit.isModified = true;
                x.productImages = [productImage].concat(x.productImages);
            }
            return x;
        });

        this.setState({
            productImageKits
        });
    }

    handleRemoveProductImage(productImageKit: ProductImageKit, productImage: ProductImage) {
        if (productImage.isNew) {
            productImageKit.productImages = productImageKit.productImages.filter(x => x.id !== productImage.id);
        } else {
            productImage.isDeleted = true;
        }

        productImageKit.isModified = true;

        this.setState({
            isModified: true,
            productImageKits: this.state.productImageKits
        });
    }

    handleChangeProductImageName(e, productImage: ProductImage, productImageKit: ProductImageKit) {
        productImage.name = e.target.value;
        productImage.isModified = true;

        productImageKit.isModified = true;

        this.setState({ isModified: true, productImageKits: this.state.productImageKits });
    }

    handleChangeProductImageUrl(productImage: ProductImage, absoluteUrl: string, productImageKit: ProductImageKit) {
        productImage.url = absoluteUrl;
        productImage.isModified = true;

        productImageKit.isModified = true;

        this.setState({ isModified: true, productImageKits: this.state.productImageKits });
    }

    handleDeleteProductImageKit(productImageKit: ProductImageKit) {
        productImageKit.isDeleted = true;
        this.setState({
            isModified: true,
            productImageKits: this.state.productImageKits
        });
    }

    getSortableRows(productImageKit: ProductImageKit) {
        const noDragClass = "dd-nodrag";
        const productImages = productImageKit.productImages.filter(x => !x.isDeleted);

        if (productImages.length === 0) {
            return [{
                id: Functions.newGuid(),
                contentItems: new Array<ISortableContentItem>({
                    item: <div className="no-images">
                        No images available. Please click on "Add Image" above to add images.
                         </div>,
                    className: noDragClass
                })
            } as ISortableRow];
        }

        return productImages.map(productImage => {
            return {
                id: productImage.id,
                contentItems: new Array<ISortableContentItem>(
                    {
                        item: <div className="bg-img" style={{
                            backgroundImage: `url(/imagik?url=${productImage.url}&width=200&save=false)`,
                            paddingBottom: "100%",
                            backgroundSize: "contain",
                            backgroundRepeat: "no-repeat",
                            backgroundColor: "transparent",
                            backgroundPosition: "center"
                        }}></div>
                    },
                    {
                        item: <TextboxComponent {...{
                            type: "text",
                            value: productImage.name,
                            handleChangeValue: (e) => this.handleChangeProductImageName(e, productImage, productImageKit)
                        } as ITextboxProps} />,
                        className: noDragClass
                    },
                    {
                        item: <React.Fragment>
                            {productImageKit.isDisabled &&
                                <input defaultValue={productImage.url} className="form-control" />
                            }
                            {!productImageKit.isDisabled &&
                                <FilePickerComponent {...{
                                    value: productImage.url,
                                    handleChangeValue: (absoluteUrl) => this.handleChangeProductImageUrl(productImage, absoluteUrl, productImageKit)
                                } as IFilePickerProps} />
                            }
                        </React.Fragment>,
                        className: noDragClass
                    },
                    {
                        item: <button type="button" className="btn btn-block btn-default" onClick={() => this.handleRemoveProductImage(productImageKit, productImage)}>
                            <i className="fa fa-trash-o"></i>
                        </button>,
                        className: noDragClass
                    }
                ),
                children: null,
                cellStyles: [
                    { cellIndices: [0, 1, 2, 3], style: { verticalAlign: "middle" } } as ISortableRowCellStyle,
                    { cellIndices: [0], style: { width: "100px" } } as ISortableRowCellStyle,
                    { cellIndices: [1], style: { width: "180px", paddingLeft: "12px" } } as ISortableRowCellStyle,
                    { cellIndices: [2], style: { paddingLeft: "12px" } } as ISortableRowCellStyle,
                    { cellIndices: [3], style: { width: "5em", paddingLeft: "12px" } } as ISortableRowCellStyle
                ]
            } as ISortableRow;
        });
    }

    handleChangeSorting(data: Array<ISortableData>, productImageKit: ProductImageKit) {
        const productImageKits = this.state.productImageKits.map(x => {
            if (x.id === productImageKit.id) {
                x.isModified = true;
                x.productImages = (data as Array<ProductImage>).map(y => x.productImages.filter(z => z.id === y.id)[0]);
            }
            return x;
        });

        this.setState({
            productImageKits,
            isModified: true
        });
    }

    submit() {
        const productImageKits = sumoJS.deepClone(this.state.productImageKits);

        const modifiedProductImageKits = productImageKits.filter(x => x.isModified);

        const model = {
            productId: this.state.productId,
            productTitle: this.state.productTitle,
            productImageKits: this.state.productImageKits.filter(x => x.isDeleted).concat(modifiedProductImageKits)
        } as IEditProductImageKitsDto;

        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Product_AddorUpdateProductImageKits, model, response => {

            if (response.isValid) {
                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditProductImageKitsDto>(Constants.url_Product_EditProductImageKitsJson,
            {
                productId: sumoJS.getQueryStringParameter("productId")
            },
            (editProductImageKitsDto) => {
                this.setState({
                    authenticatedUser: new User(editProductImageKitsDto.authenticatedUser),
                    productId: editProductImageKitsDto.productId,
                    productTitle: editProductImageKitsDto.productTitle,
                    productImageKits: editProductImageKitsDto.productImageKits.map(x => {
                        x.productImages = x.productImages.orderBy(x => x.sortOrder);
                        return new ProductImageKit(x);
                    }),
                    pageIsLoading: false
                }, () => Functions.setCreateAndEditPermissions(
                    new Product(),
                    this.state.authenticatedUser.role.createProduct,
                    this.state.authenticatedUser.role.editProduct));

                $(".disabled table *").attr("disabled", "disabled");
            });
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">
                    Edit Product Images for {this.state.productTitle}
                </div>
                <div className="admin-top-bar-controls">
                    <a href={Constants.url_Product_Index} className="btn btn-link">
                        Back to list
                    </a>
                    <div className="dropdown">
                        <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                            Edit...
                            &nbsp;
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href={Constants.url_Product_Edit + this.state.productId}>
                                    Product
                                </a>
                            </li>
                            <li>
                                <a href={Constants.url_Product_EditProductAttributes + this.state.productId}>
                                    Product Attributes
                                </a>
                            </li>
                            <li>
                                <a href={Constants.url_Product_EditProductVariants + this.state.productId}>
                                    Product Variants
                                </a>
                            </li>
                            <li className="disabled">
                                <a>
                                    Product Images
                                </a>
                            </li>
                            <li>
                                <a href={Constants.url_Product_EditProductStockUnits + this.state.productId}>
                                    Product Stock
                                </a>
                            </li>
                        </ul>
                    </div>
                    <ButtonSubmitComponent {...{
                        isSubmitting: this.state.isSubmitting,
                        handleSubmit: () => this.submit()
                    } as IButtonSubmitProps} />
                </div >
            </div>
        );
    }

    render() {

        return (
            <div className="edit-product-image-kits-component">
                {
                    this.renderTopBar()
                }
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                <div>
                    {!this.state.pageIsLoading && this.state.productImageKits.filter(x => !x.isDeleted).map((productImageKit) =>
                        <div className="admin-subsection" key={productImageKit.id}>
                            <div className="admin-subsection-title">
                                {productImageKit.variantOptions.map((variantOption) => <span key={variantOption.id} className="product-image-kit-variant-option">{`${variantOption.variant.localizedTitle}: ${variantOption.localizedTitle}`}</span>)}
                                {productImageKit.isDisabled ? " (disabled because obsolete)" : ""}
                            </div>
                            <div className="admin-subsection-controls ">
                                {!productImageKit.isDisabled &&
                                    <button type="button" className="btn btn-link" onClick={() => this.handleAddProductImage(productImageKit)}>
                                        <i className="fa fa-plus"></i>
                                        Add Image
                                    </button>
                                }
                                <button type="button" className="btn btn-link product-image-kit-delete" onClick={() => this.handleDeleteProductImageKit(productImageKit)}>
                                    <i className="fa fa-trash-o"></i>
                                    Delete all
                                </button>
                            </div>
                            <SortableComponent {...{
                                headerRowItems: ["Preview", "Name", "Url", ""],
                                rows: this.getSortableRows(productImageKit),
                                className: `image-sortable ${productImageKit.isDisabled ? "disabled" : ""}`,
                                handleChangeSorting: (data) => this.handleChangeSorting(data, productImageKit)
                            } as ISortableProps} />
                        </div>
                    )
                    }
                    {this.state.pageIsLoading && <SpinnerComponent />}
                </div>
            </div>
        );
    }
}