﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { ProductImageKit } from "./ProductImageKit";

export interface IEditProductImagesState extends IReactPageState {
    productId?: string; 
    productTitle?: string;
    productImageKits?: Array<ProductImageKit>;
}