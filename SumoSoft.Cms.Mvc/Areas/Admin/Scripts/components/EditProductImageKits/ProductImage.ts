﻿import { ProductImage as BaseEcommerceProductImage } from "../../../../../Scripts/classes/ProductImage";
import { IProductImageDto } from "../../../../../Scripts/interfaces/IProductImageDto";

export class ProductImage extends BaseEcommerceProductImage {
    tempId: string;

    constructor(dto?: IProductImageDto | any) {
        super(dto);
    }
}