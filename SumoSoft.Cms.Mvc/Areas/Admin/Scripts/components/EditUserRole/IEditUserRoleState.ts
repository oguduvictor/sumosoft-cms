﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { UserRole } from "../../../../../Scripts/classes/UserRole";

export interface IEditUserRoleState extends IReactPageState {
    userRole?: UserRole;
}