﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IUserRoleDto } from "../../../../../Scripts/interfaces/IUserRoleDto";

export interface IEditUserRoleDto {
    authenticatedUser?: IUserDto;
    userRole?: IUserRoleDto;
}