﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { Constants } from "../../shared/Constants";
import { IEditUserRoleState } from "./IEditUserRoleState";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { IEditUserRoleDto } from "./IEditUserRoleDto";
import { User } from "../../../../../Scripts/classes/User";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditUserRoleComponent extends ReactPageComponent<{}, IEditUserRoleState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            userRole: new UserRole()
        } as IEditUserRoleState;

        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleDeleteUserRole() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_User_DeleteUserRole, { userRoleId: this.state.userRole.id }, response => {

            if (response.isValid) {

                window.location.href = Constants.url_User_Roles;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleChangeRoleDescription(e) {
        this.state.userRole.name = e.target.value;
        this.setState({ userRole: this.state.userRole });
    }

    handleChangeUserPermission(e, property: string) {
        Object.defineProperty(this.state.userRole, property, {
            value: e.target.checked,
            writable: true,
            enumerable: true
        });

        this.setState({ userRole: this.state.userRole });
    }

    submit() {
        this.setState({ isSubmitting: true });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_User_AddOrUpdateUserRole, { dto: this.state.userRole }, response => {

            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditUserRoleDto>(Constants.url_User_EditUserRoleJson,
            {
                userRoleId: sumoJS.getQueryStringParameter("userRoleId")
            },
            (editUserRoleDto) => {
                this.setState({
                    authenticatedUser: new User(editUserRoleDto.authenticatedUser),
                    userRole: new UserRole(editUserRoleDto.userRole),
                    pageIsLoading: false
                }, () => Functions.setCreateAndEditPermissions(
                    this.state.userRole,
                    this.state.authenticatedUser.role.createUserRole,
                    this.state.authenticatedUser.role.editUserRole));
            });
    }

    renderPermissionTableRow(key: string, description: string) {
        return (
            [<CheckboxComponent {...{
                text: description,
                value: this.state.userRole[key],
                handleChangeValue: (e) => this.handleChangeUserPermission(e, key)
            } as ICheckboxProps} />]
        );
    }

    render() {

        return (
            <div className="edit-user-role-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.userRole.isNew ? "Create User Role" : "Edit User Role"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_User_Roles} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                canDelete: this.state.authenticatedUser.role.deleteUserRole,
                                handleDelete: () => this.handleDeleteUserRole()
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div>
                        <div className="admin-subsection">
                            <TextboxComponent {...{
                                label: "Role description",
                                value: this.state.userRole.name,
                                type: "text",
                                handleChangeValue: (e) => this.handleChangeRoleDescription(e)
                            } as ITextboxProps} />
                        </div>
                        <div className="admin-subsection">
                            <div className="admin-subsection-title">
                                Permissions
                            </div>
                            <TableComponent {...{
                                trList:
                                    [
                                        [<b>Access to specific areas of the Administration panel</b>],
                                        this.renderPermissionTableRow("accessAdmin", "Access the Administration panel"),
                                        this.renderPermissionTableRow("accessAdminEcommerce", "Access any of the Ecommerce sections"),
                                        this.renderPermissionTableRow("accessAdminOrders", "Access the Orders section"),
                                        this.renderPermissionTableRow("accessAdminBlog", "Access the Blog section"),
                                        this.renderPermissionTableRow("accessAdminMedia", "Access the Media section"),
                                        this.renderPermissionTableRow("accessAdminContent", "Access the Content section"),
                                        this.renderPermissionTableRow("accessAdminEmails", "Access the Emails section"),
                                        this.renderPermissionTableRow("accessAdminSeo", "Access the Seo section"),
                                        this.renderPermissionTableRow("accessAdminUsers", "Access the Users section"),
                                        this.renderPermissionTableRow("accessAdminCountries", "Access the Countries section"),
                                        this.renderPermissionTableRow("accessAdminSettings", "Access the Settings section"),

                                        [<b>Orders</b>],
                                        this.renderPermissionTableRow("editOrder", "Edit an existing Order"),

                                        [<b>Products</b>],
                                        this.renderPermissionTableRow("createProduct", "Create a new Product"),
                                        this.renderPermissionTableRow("editProduct", "Edit an existing Product"),
                                        this.renderPermissionTableRow("editProductAllLocalizedKits", "Edit all the Product's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteProduct", "Delete a Product"),

                                        [<b>Attributes</b>],
                                        this.renderPermissionTableRow("createAttribute", "Create a new Attribute"),
                                        this.renderPermissionTableRow("editAttribute", "Edit an existing Attribute"),
                                        this.renderPermissionTableRow("editAttributeName", "Edit the Name of an existing Attribute"),
                                        this.renderPermissionTableRow("editAttributeAllLocalizedKits", "Edit all the Attribute's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteAttribute", "Delete an Attribute"),
                                        this.renderPermissionTableRow("createAttributeOption", "Create a new AttributeOption"),
                                        this.renderPermissionTableRow("editAttributeOption", "Edit an existing AttributeOption"),
                                        this.renderPermissionTableRow("editAttributeOptionName", "Edit the Name of an existing AttributeOption"),
                                        this.renderPermissionTableRow("editAttributeOptionAllLocalizedKits", "Edit all the AttributeOption's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteAttributeOption", "Delete an AttributeOption"),

                                        [<b>Variants</b>],
                                        this.renderPermissionTableRow("createVariant", "Create a new Variant"),
                                        this.renderPermissionTableRow("editVariant", "Edit an existing Variant"),
                                        this.renderPermissionTableRow("editVariantName", "Edit the Name of an existing Variant"),
                                        this.renderPermissionTableRow("editVariantSelector", "Edit the Option Selector of an existing Variant"),
                                        this.renderPermissionTableRow("editVariantAllLocalizedKits", "Edit all the Variant's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteVariant", "Delete a Variant"),
                                        this.renderPermissionTableRow("createVariantOption", "Create a new VariantOption"),
                                        this.renderPermissionTableRow("editVariantOption", "Edit an existing VariantOption"),
                                        this.renderPermissionTableRow("editVariantOptionName", "Edit the Name of an existing VariantOption"),
                                        this.renderPermissionTableRow("editVariantOptionAllLocalizedKits", "Edit all the VariantOption's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteVariantOption", "Delete a VariantOption"),

                                        [<b>ShippingBoxes</b>],
                                        this.renderPermissionTableRow("createShippingBox", "Create a new ShippingBox"),
                                        this.renderPermissionTableRow("editShippingBox", "Edit an existing ShippingBox"),
                                        this.renderPermissionTableRow("editShippingBoxName", "Edit the Name of an existing ShippingBox"),
                                        this.renderPermissionTableRow("editShippingBoxAllLocalizedKits", "Edit all the ShippingBox's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteShippingBox", "Delete a ShippingBox"),

                                        [<b>Product Categories</b>],
                                        this.renderPermissionTableRow("createCategory", "Create a new Category"),
                                        this.renderPermissionTableRow("editCategory", "Edit an existing Category"),
                                        this.renderPermissionTableRow("editCategoryName", "Edit the Name of an existing Category"),
                                        this.renderPermissionTableRow("editCategoryUrl", "Edit the Url of an existing Category"),
                                        this.renderPermissionTableRow("editCategoryAllLocalizedKits", "Edit all the Category's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteCategory", "Delete a Category"),

                                        [<b>Blog Posts</b>],
                                        this.renderPermissionTableRow("createBlogPost", "Create a new BlogPost"),
                                        this.renderPermissionTableRow("editBlogPost", "Edit an existing BlogPost"),
                                        this.renderPermissionTableRow("deleteBlogPost", "Delete a BlogPost"),

                                        [<b>ContentSections</b>],
                                        this.renderPermissionTableRow("createContentSection", "Create a new ContentSection"),
                                        this.renderPermissionTableRow("editContentSection", "Edit an existing ContentSection"),
                                        this.renderPermissionTableRow("editContentSectionName", "Edit the Name of an existing ContentSection"),
                                        this.renderPermissionTableRow("editContentSectionAllLocalizedKits", "Edit all the ContentSection's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteContentSection", "Delete a ContentSection"),

                                        [<b>Emails</b>],
                                        this.renderPermissionTableRow("createEmail", "Create a new Email"),
                                        this.renderPermissionTableRow("createMailingList", "Create a new Mailing list"),
                                        this.renderPermissionTableRow("editEmail", "Edit an existing Email"),
                                        this.renderPermissionTableRow("editMailingListName", "Edit an existing Mailing list Name"),
                                        this.renderPermissionTableRow("editMailingList", "Edit an existing Mailing list"),
                                        this.renderPermissionTableRow("editEmailName", "Edit the Name of an existing Email"),
                                        this.renderPermissionTableRow("editEmailAllLocalizedKits", "Edit all the Email's LocalizedKits"),
                                        this.renderPermissionTableRow("ediMailingListAllLocalizedKits", "Edit all the Mailing List's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteEmail", "Delete an Email"),
                                        this.renderPermissionTableRow("deleteMailingList", "Delete a Mailing list"),

                                        [<b>SeoSections</b>],
                                        this.renderPermissionTableRow("createSeoSection", "Create a new SeoSection"),
                                        this.renderPermissionTableRow("editSeoSection", "Edit an existing SeoSection"),
                                        this.renderPermissionTableRow("editSeoSectionPage", "Edit the Page of an existing SeoSection"),
                                        this.renderPermissionTableRow("editSeoSectionAllLocalizedKits", "Edit all the SeoSection's LocalizedKits"),
                                        this.renderPermissionTableRow("deleteSeoSection", "Delete a SeoSection"),

                                        [<b>Users</b>],
                                        this.renderPermissionTableRow("createUser", "Create a new User"),
                                        this.renderPermissionTableRow("editUser", "Edit an existing User"),
                                        this.renderPermissionTableRow("deleteUser", "Delete a User"),

                                        this.renderPermissionTableRow("createUserRole", "Create a new UserRole"),
                                        this.renderPermissionTableRow("editUserRole", "Edit an existing UserRole"),
                                        this.renderPermissionTableRow("deleteUserRole", "Delete a UserRole"),

                                        [<b>Countries</b>],
                                        this.renderPermissionTableRow("createCountry", "Create a new Country"),
                                        this.renderPermissionTableRow("editCountry", "Edit an existing Country"),
                                        this.renderPermissionTableRow("deleteCountry", "Delete a Country"),

                                        [<b>Coupons</b>],
                                        this.renderPermissionTableRow("createCoupon", "Create a new Coupon"),
                                        this.renderPermissionTableRow("editCoupon", "Edit an existing Coupon"),
                                        this.renderPermissionTableRow("deleteCoupon", "Delete a Coupon"),

                                        [<b>Notifications</b>],
                                        this.renderPermissionTableRow("viewExceptionLogs", "View code exception logs"),
                                        this.renderPermissionTableRow("viewEcommerceLogs", "View ecommerce related logs"),
                                        this.renderPermissionTableRow("viewContentLogs", "View content related logs")
                                    ]
                            } as ITableProps} />
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}