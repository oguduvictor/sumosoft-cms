﻿import { IProductDto } from "../../../../../Scripts/interfaces/IProductDto";
import { IShippingBoxDto } from "../../../../../Scripts/interfaces/IShippingBoxDto";
import { ICategoryDto } from "../../../../../Scripts/interfaces/ICategoryDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IProductImageKitDto } from "../../../../../Scripts/interfaces/IProductImageKitDto";

export interface IEditProductDto {
    authenticatedUser: IUserDto;
    product: IProductDto;
    allTags: Array<string>;
    allShippingBoxes: Array<IShippingBoxDto>;
    allProducts: Array<IProductDto>;
    allCategories: Array<ICategoryDto>;
    allProductImageKits: Array<IProductImageKitDto>;
}
