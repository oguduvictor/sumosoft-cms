﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { MultiSelectComponent, IMultiSelectProps, SmartOption } from "../MultiSelect/MultiSelectComponent";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { Product } from "../../../../../Scripts/classes/Product";
import { Category } from "../../../../../Scripts/classes/Category";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";
import { ProductLocalizedKit } from "../../../../../Scripts/classes/ProductLocalizedKit";
import { Constants } from "../../shared/Constants";
import { IEditProductState } from "./IEditProductState";
import { IEditProductDto } from "./IEditProductDto";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { TagsComponent, ITagsProps } from "../Tags/TagsComponent";
import { IProductLocalizedKitDto } from "../../../../../Scripts/interfaces/IProductLocalizedKitDto";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";
import { ProductImageKit } from "../../../../../Scripts/classes/ProductImageKit";

export default class EditProductComponent extends ReactPageComponent<{}, IEditProductState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            formErrors: new Array<IFormError>(),
            product: new Product(),
            allShippingBoxes: new Array<ShippingBox>(),
            allProducts: new Array<Product>(),
            allTags: new Array<string>(),
            allCategories: new Array<Category>(),
            isSaved: false
        } as IEditProductState;

        this.handleChangeProductLocalizedKitTitle = this.handleChangeProductLocalizedKitTitle.bind(this);
        this.handleUpdateProductLocalizedKitDescription = this.handleUpdateProductLocalizedKitDescription.bind(this);
        this.handleUpdateProductLocalizedKitMetaTitle = this.handleUpdateProductLocalizedKitMetaTitle.bind(this);
        this.handleUpdateProductLocalizedKitMetaDescription = this.handleUpdateProductLocalizedKitMetaDescription.bind(this);
        this.handleSelectCategories = this.handleSelectCategories.bind(this);
        this.handleDeleteProduct = this.handleDeleteProduct.bind(this);
        this.handleSelectShippingOptions = this.handleSelectShippingOptions.bind(this);
        this.handleSelectRelatedProducts = this.handleSelectRelatedProducts.bind(this);
        this.handleChangeProductTags = this.handleChangeProductTags.bind(this);
        this.handleLocalizedKitCopyValuesFrom = this.handleLocalizedKitCopyValuesFrom.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.setState({ pageIsLoading: true });
        this.setStateFromApi();
    }

    handleUpdateUrlOnBlur(source: string) {
        const product = this.state.product;
        if (!product.url || product.url === "") {
            product.url = Functions.convertToUrl(source);
            this.setState({ product });
        }
    }

    handleChangeProductLocalizedKitTitle(e, productLocalizedKit: ProductLocalizedKit) {
        productLocalizedKit.title = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleUpdateProductLocalizedKitDescription(e, productLocalizedKit: ProductLocalizedKit) {
        productLocalizedKit.description = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleUpdateProductLocalizedKitMetaTitle(e, productLocalizedKit: ProductLocalizedKit) {
        productLocalizedKit.metaTitle = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleUpdateProductLocalizedKitMetaDescription(e, productLocalizedKit: ProductLocalizedKit) {
        productLocalizedKit.metaDescription = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleChangeProductLocalizedKitIsDisabled(e, productLocalizedKit: ProductLocalizedKit) {
        productLocalizedKit.isDisabled = e.target.checked;
        this.setState({ product: this.state.product });
    }

    handleChangeName(e) {
        this.state.product.name = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleChangeUrl(e) {
        this.state.product.url = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleChangeProductTags(newValue) {
        this.state.product.tags = newValue;
        this.setState({
            isModified: true,
            product: this.state.product
        });
    }

    handleChangeCode(e) {
        this.state.product.code = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleChangeTaxCode(e) {
        this.state.product.taxCode = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleChangeComments(e) {
        this.state.product.comments = e.target.value;
        this.setState({ product: this.state.product });
    }

    handleChangeIsDisabled(e) {
        this.state.product.isDisabled = e.target.checked;
        this.setState({ product: this.state.product });
    }

    handleSelectCategories(newValue: Array<string>) {
        this.state.product.categories = newValue.map(categoryId => this.state.allCategories.filter(x => x.id === categoryId)[0]);
        this.setState({
            isModified: true,
            product: this.state.product
        });
    }

    handleSelectShippingOptions(newValue: Array<string>) {
        this.state.product.shippingBoxes = newValue.map(shippingBoxId => this.state.allShippingBoxes.filter(x => x.id === shippingBoxId)[0]);

        this.setState({
            isModified: true,
            product: this.state.product
        });
    }

    handleSelectRelatedProducts(newValue: Array<string>) {
        this.state.product.relatedProducts = newValue.map(productId => this.state.allProducts.filter(x => x.id === productId)[0]);
        this.setState({
            isModified: true,
            product: this.state.product
        });
    }

    handleSelectRelatedProductImageKits(productImageKit: ProductImageKit, newValue: Array<string>) {
        const product = this.state.product;

        product.productImageKits.filter(x => x.id === productImageKit.id)[0].relatedProductImageKits = newValue.mapMany(id => this.state.allProductImageKits.filter(y => y.id === id));

        this.setState({
            isModified: true,
            product
        });
    }

    handleDeleteProduct() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Product_Delete, { id: this.state.product.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Product_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleLocalizedKitCopyValuesFrom(source: IProductLocalizedKitDto, target: IProductLocalizedKitDto, completed) {
        target.title = source.title;
        target.description = source.description;
        target.metaTitle = source.metaTitle;
        target.metaDescription = source.metaDescription;
        target.isDisabled = source.isDisabled;

        this.setState({
            product: this.state.product
        }, () => completed());
    }

    submit() {
        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Product_AddOrUpdateProduct, this.state.product, response => {
            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {

        sumoJS.ajaxPost<IEditProductDto>(Constants.url_Product_EditJson,
            {
                productId: sumoJS.getQueryStringParameter("productId")
            },
            (editProductDto) => this.setState({
                authenticatedUser: new User(editProductDto.authenticatedUser),
                product: new Product(editProductDto.product),
                allCategories: editProductDto.allCategories.map(x => new Category(x)),
                allTags: editProductDto.allTags,
                allProducts: editProductDto.allProducts.map(x => new Product(x)),
                allShippingBoxes: editProductDto.allShippingBoxes.map(x => new ShippingBox(x)),
                allProductImageKits: editProductDto.allProductImageKits.map(x => new ProductImageKit(x)),
                pageIsLoading: false
            }, () => Functions.setCreateAndEditPermissions(
                this.state.product,
                this.state.authenticatedUser.role.createProduct,
                this.state.authenticatedUser.role.editProduct))
        );
    }

    render() {
        return (
            <div className="edit-product-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.product.isNew ? "Create Product" : "Edit Product"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Product_Index} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            !this.state.product.isNew &&
                            <div className="dropdown">
                                <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                    Edit...
                                    &nbsp;
                                <span className="caret"></span>
                                </button>
                                <ul className="dropdown-menu dropdown-menu-right">
                                    <li className="disabled">
                                        <a>
                                            Product
                                        </a>
                                    </li>
                                    <li>
                                        <a href={Constants.url_Product_EditProductAttributes + this.state.product.id}>
                                            Product Attributes
                                        </a>
                                    </li>
                                    <li>
                                        <a href={Constants.url_Product_EditProductVariants + this.state.product.id}>
                                            Product Variants
                                        </a>
                                    </li>
                                    <li>
                                        <a href={Constants.url_Product_EditProductImageKits + this.state.product.id}>
                                            Product Images
                                        </a>
                                    </li>
                                    <li>
                                        <a href={Constants.url_Product_EditProductStockUnits + this.state.product.id}>
                                            Product Stock
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        }
                        {
                            !this.state.product.isNew &&
                            <ButtonDeleteComponent {...{
                                canDelete: this.state.authenticatedUser.role.deleteProduct,
                                isDeleting: this.state.isDeleting,
                                handleDelete: this.handleDeleteProduct
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: this.submit
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-5">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Global properties
                                </div>
                                <TextboxComponent {...{
                                    label: "Name",
                                    value: this.state.product.name,
                                    readOnly: false,
                                    tooltip: Constants.tooltip_name,
                                    className: "form-group",
                                    type: "text",
                                    handleChangeValue: (e) => this.handleChangeName(e),
                                    handleBlur: (e) => this.handleUpdateUrlOnBlur(e.target.value)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Url",
                                    value: this.state.product.url,
                                    readOnly: false,
                                    tooltip: (() => {
                                        var exampleUrl = this.state.product.url || "my-amazing-product";
                                        return Constants.tooltip_url + `For example, if you set it to '${exampleUrl}', the URL of the product page will look like this: .../product/${exampleUrl}`;
                                    })(),
                                    className: "form-group",
                                    type: "text",
                                    handleChangeValue: (e) => this.handleChangeUrl(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Code",
                                    value: this.state.product.code,
                                    readOnly: false,
                                    tooltip: Constants.tooltip_code,
                                    className: "form-group",
                                    type: "text",
                                    handleChangeValue: (e) => this.handleChangeCode(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Tax Code",
                                    value: this.state.product.taxCode,
                                    readOnly: false,
                                    tooltip: Constants.tooltip_taxCode,
                                    className: "form-group",
                                    type: "text",
                                    handleChangeValue: (e) => this.handleChangeTaxCode(e)
                                } as ITextboxProps} />
                                <MultiSelectComponent {...{
                                    options: this.state.allCategories.map(x => new SmartOption(x.id, x.localizedTitle)),
                                    value: this.state.product.categories.map(category => category.id),
                                    label: "Categories",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectCategories(newValue)
                                } as IMultiSelectProps} />
                                <TagsComponent {...{
                                    value: this.state.product.tags,
                                    allTags: this.state.allTags,
                                    handleChangeValue: (newValue) => this.handleChangeProductTags(newValue),
                                    className: "form-group"
                                } as ITagsProps} />
                                <TextareaComponent {...{
                                    label: "Comments",
                                    type: "text",
                                    className: "form-group",
                                    value: this.state.product.comments,
                                    handleChangeValue: (e) => this.handleChangeComments(e)
                                } as ITextareaProps} />
                                <CheckboxComponent {...{
                                    value: this.state.product.isDisabled || false,
                                    className: "form-group",
                                    text: "Disabled",
                                    handleChangeValue: (e) => this.handleChangeIsDisabled(e)
                                } as ICheckboxProps} />
                            </div>
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Shipping Options
                                </div>
                                <MultiSelectComponent {...{
                                    options: this.state.allShippingBoxes.map(x => new SmartOption(x.id, x.localizedInternalDescription, `Name: ${x.name}`)),
                                    value: this.state.product.shippingBoxes.map(shippingBox => shippingBox.id),
                                    label: "Available Shipping Boxes",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectShippingOptions(newValue)
                                } as IMultiSelectProps} />
                            </div>
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Related Products
                                </div>
                                <MultiSelectComponent {...{
                                    options: this.state.allProducts.map(x => (x.id !== this.state.product.id) && new SmartOption(x.id, x.localizedTitle)),
                                    value: this.state.product.relatedProducts.map(product => product.id),
                                    label: "Select one or more products",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectRelatedProducts(newValue)
                                } as IMultiSelectProps} />
                            </div>
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Related Product Image Kits
                                </div>
                                {
                                    this.state.product.productImageKits.map(productImageKit => <MultiSelectComponent
                                        key={productImageKit.id}
                                        options={this.state.allProductImageKits.map(x => new SmartOption(x.id, `${x.product.localizedTitle} - ` + x.variantOptions.map(vo => `${vo.variant.localizedTitle}: ${vo.localizedTitle}`).join(" & ")))}
                                        value={productImageKit.relatedProductImageKits.map(pik => pik.id)}
                                        label={productImageKit.variantOptions.map(vo => `${vo.variant.localizedTitle}: ${vo.localizedTitle}`).join(" & ")}
                                        className="form-group"
                                        handleChangeValue={(newValue) => this.handleSelectRelatedProductImageKits(productImageKit, newValue)}
                                    />)
                                }
                                {
                                    this.state.product.productImageKits.length === 0 &&
                                    <span className="text-muted">No Product Image Kits available</span>
                                }
                            </div>
                        </div>
                        <div className="col-md-7">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Localized properties
                                </div>
                                <LocalizedKitsComponent {...{
                                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source as IProductLocalizedKitDto, target as IProductLocalizedKitDto, completed),
                                    editAllLocalizedKits: this.state.authenticatedUser.role.editProductAllLocalizedKits,
                                    localizedKitUIs: this.state.product.productLocalizedKits.map((productLocalizedKit) => {
                                        return {
                                            localizedKit: productLocalizedKit,
                                            html:
                                                <div>
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Title",
                                                            type: "text",
                                                            value: productLocalizedKit.title,
                                                            placeholder: this.state.product.productLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                            handleChangeValue: (e) => this.handleChangeProductLocalizedKitTitle(e, productLocalizedKit),
                                                            handleBlur: (e) => this.handleUpdateUrlOnBlur(e.target.value)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "Description",
                                                            type: "text",
                                                            value: productLocalizedKit.description,
                                                            placeholder: this.state.product.productLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                            handleChangeValue: (e) => this.handleUpdateProductLocalizedKitDescription(e, productLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "MetaTitle",
                                                            type: "text",
                                                            value: productLocalizedKit.metaTitle,
                                                            placeholder: this.state.product.productLocalizedKits.filter(x => x.country.isDefault)[0].metaTitle,
                                                            className: "form-group",
                                                            handleChangeValue: (e) => this.handleUpdateProductLocalizedKitMetaTitle(e, productLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "MetaDescription",
                                                            type: "text",
                                                            value: productLocalizedKit.metaDescription,
                                                            placeholder: this.state.product.productLocalizedKits.filter(x => x.country.isDefault)[0].metaDescription,
                                                            handleChangeValue: (e) => this.handleUpdateProductLocalizedKitMetaDescription(e, productLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <CheckboxComponent {...{
                                                            text: "Disabled",
                                                            value: productLocalizedKit.isDisabled,
                                                            className: "form-group",
                                                            handleChangeValue: (e) => this.handleChangeProductLocalizedKitIsDisabled(e, productLocalizedKit)
                                                        } as ICheckboxProps} />
                                                    } as INestedInputProps} />
                                                </div>
                                        } as ILocalizedKitUI;
                                    })
                                } as ILocalizedKitsProps} />
                            </div>
                        </div>
                    </div>
                }
                {this.state.pageIsLoading && <SpinnerComponent />}
            </div>
        );
    }

}
