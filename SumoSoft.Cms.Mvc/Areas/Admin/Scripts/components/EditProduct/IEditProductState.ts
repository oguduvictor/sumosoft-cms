﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Category } from "../../../../../Scripts/classes/Category";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";
import { Product } from "../../../../../Scripts/classes/Product";
import { ProductImageKit } from "../../../../../Scripts/classes/ProductImageKit";

export interface IEditProductState extends IReactPageState {
    product?: Product;
    allShippingBoxes?: Array<ShippingBox>;
    allProducts?: Array<Product>;
    allTags?: Array<string>;
    allCategories?: Array<Category>;
    allProductImageKits?: Array<ProductImageKit>;
}