﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IVariantOptionDto } from "../../../../../Scripts/interfaces/IVariantOptionDto";

export interface IEditVariantOptionDto {
    authenticatedUser: IUserDto;
    allTags: Array<string>;
    variantOption: IVariantOptionDto;
}