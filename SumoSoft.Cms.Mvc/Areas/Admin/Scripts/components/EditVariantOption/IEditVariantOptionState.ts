﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { VariantOption } from "../../../../../Scripts/classes/VariantOption";

export interface IEditVariantOptionState extends IReactPageState {
    variantOption?: VariantOption;
    allTags?: Array<string>;

}