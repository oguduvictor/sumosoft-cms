﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";
import { VariantOption } from "../../../../../Scripts/classes/VariantOption";
import { VariantOptionLocalizedKit } from "../../../../../Scripts/classes/VariantOptionLocalizedKit";
import { Constants } from "../../shared/Constants";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { IEditVariantOptionState } from "./IEditVariantOptionState";
import { FilePickerComponent, IFilePickerProps } from "../FilePicker/FilePickerComponent";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IEditVariantOptionDto } from "./IEditVariantOptionDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { TagsComponent, ITagsProps } from "../Tags/TagsComponent";
import { IVariantOptionLocalizedKitDto } from
    "../../../../../Scripts/interfaces/IVariantOptionLocalizedKitDto";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditVariantOptionComponent extends ReactPageComponent<{}, IEditVariantOptionState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            variantOption: new VariantOption({ variant: new Variant() }),
            authenticatedUser: new User({ role: new UserRole() }),
            allTags: new Array<string>()
        } as IEditVariantOptionState;

        this.handleChangeVariantOptionName = this.handleChangeVariantOptionName.bind(this);
        this.handleBlurVariantOptionName = this.handleBlurVariantOptionName.bind(this);
        this.handleChangeVariantOptionSortOrder = this.handleChangeVariantOptionSortOrder.bind(this);
        this.handleChangeVariantOptionUrl = this.handleChangeVariantOptionUrl.bind(this);
        this.handleChangeVariantOptionCode = this.handleChangeVariantOptionCode.bind(this);
        this.handleChangeVariantOptionLocalizedKitTitle = this.handleChangeVariantOptionLocalizedKitTitle.bind(this);
        this.handleChangeVariantOptionLocalizedKitPriceModifier = this.handleChangeVariantOptionLocalizedKitPriceModifier.bind(this);
        this.handleDeleteVariantOption = this.handleDeleteVariantOption.bind(this);
        this.handleChangeVariantOptionIsDisabled = this.handleChangeVariantOptionIsDisabled.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleChangeVariantOptionName(e) {
        this.state.variantOption.name = e.target.value;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleBlurVariantOptionName() {
        if (this.state.variantOption.name && !this.state.variantOption.url || this.state.variantOption.url === "") {
            this.state.variantOption.url = Functions.convertToUrl(this.state.variantOption.name);

            this.setState({ isModified: true, variantOption: this.state.variantOption });
        }
    }

    handleChangeVariantOptionUrl(e) {
        this.state.variantOption.url = e.target.value;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionCode(e) {
        this.state.variantOption.code = e.target.value;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionImage(imageUrl) {
        this.state.variantOption.image = imageUrl;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionIsDisabled(e) {
        this.state.variantOption.isDisabled = e.target.checked;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionSortOrder(e) {
        this.state.variantOption.sortOrder = e.target.value;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionTags(newValue) {
        this.state.variantOption.tags = newValue;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionLocalizedKitTitle(e, variantOptionLocalizedKit: VariantOptionLocalizedKit) {
        variantOptionLocalizedKit.title = e.target.value;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionLocalizedKitDescription(e, variantOptionLocalizedKit: VariantOptionLocalizedKit) {
        variantOptionLocalizedKit.description = e.target.value;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionLocalizedKitPriceModifier(e, variantOptionLocalizedKit: VariantOptionLocalizedKit) {
        variantOptionLocalizedKit.priceModifier = e.target.value;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleChangeVariantOptionLocalizedKitIsDisabled(e, variantOptionLocalizedKit: VariantOptionLocalizedKit) {
        variantOptionLocalizedKit.isDisabled = e.target.checked;
        this.setState({ isModified: true, variantOption: this.state.variantOption });
    }

    handleDeleteVariantOption() {
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Variant_DeleteVariantOption, { id: this.state.variantOption.id },
            response => {

                if (response.isValid) {
                    window.location.href = Constants.url_Variant_Edit + this.state.variantOption.variant.id;
                } else {
                    this.setState({
                        formErrors: response.errors
                    });
                }
            });
    }

    handleLocalizedKitCopyValuesFrom(source: IVariantOptionLocalizedKitDto, target: IVariantOptionLocalizedKitDto, completed) {
        target.title = source.title;
        target.description = source.description;
        target.priceModifier = source.priceModifier;
        target.isDisabled = source.isDisabled;

        this.setState({
            isModified: true,
            variantOption: this.state.variantOption
        }, () => completed());
    }

    submit() {
        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(
            Constants.url_Variant_AddOrUpdateVariantOption,
            this.state.variantOption,
            response => {

                if (response.isValid) {
                    if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                    this.setStateFromApi();
                    this.setState({
                        isModified: false,
                        isSubmitting: false,
                        formSuccessMessage: Constants.alert_saveSuccess
                    });
                } else {
                    this.setState({
                        isModified: false,
                        isSubmitting: false,
                        formErrors: response.errors
                    });
                }
            });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditVariantOptionDto>(Constants.url_Variant_EditVariantOptionJson,
            {
                variantId: sumoJS.getQueryStringParameter("variantId"),
                variantOptionId: sumoJS.getQueryStringParameter("variantOptionId")
            },
            (editVariantOptionDto) => {
                this.setState({
                    authenticatedUser: new User(editVariantOptionDto.authenticatedUser),
                    variantOption: new VariantOption(editVariantOptionDto.variantOption),
                    allTags: editVariantOptionDto.allTags,
                    pageIsLoading: false
                }, () => Functions.setCreateAndEditPermissions(
                    this.state.variantOption,
                    this.state.authenticatedUser.role.createVariantOption,
                    this.state.authenticatedUser.role.editVariantOption)
                );
            });
    }

    render() {

        return (
            <div className="edit-variant-option-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            `${this.state.variantOption.isNew ? "Create" : "Edit"} Variant Option for ${this.state.variantOption.variant.name}`
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Variant_Edit + this.state.variantOption.variant.id} className="btn btn-link">
                            Back to Variant
                        </a>
                        {
                            !this.state.variantOption.isNew &&
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                canDelete: this.state.authenticatedUser.role.deleteVariantOption,
                                handleDelete: () => this.handleDeleteVariantOption()
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    this.state.pageIsLoading ||
                    <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Global properties
                                </div>
                                <div className="variant-option">
                                    <TextboxComponent {...{
                                        label: "Name",
                                        type: "text",
                                        value: this.state.variantOption.name,
                                        tooltip: Constants.tooltip_name,
                                        className: "form-group",
                                        readOnly: Functions.getReadOnlyStateForRequiredField(
                                            this.state.variantOption,
                                            this.state.authenticatedUser.role.editVariantOptionName),
                                        handleChangeValue: this.handleChangeVariantOptionName,
                                        handleBlur: this.handleBlurVariantOptionName
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Url",
                                        type: "text",
                                        value: this.state.variantOption.url,
                                        tooltip: Constants.tooltip_url,
                                        className: "form-group",
                                        handleChangeValue: this.handleChangeVariantOptionUrl
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Code",
                                        type: "text",
                                        value: this.state.variantOption.code,
                                        className: "form-group",
                                        handleChangeValue: this.handleChangeVariantOptionCode
                                    } as ITextboxProps} />
                                    <FilePickerComponent {...{
                                        label: "Image",
                                        type: "text",
                                        value: this.state.variantOption.image,
                                        className: "form-group",
                                        handleChangeValue: (imageUrl) => this.handleChangeVariantOptionImage(imageUrl)
                                    } as IFilePickerProps} />
                                    <TagsComponent {...{
                                        value: this.state.variantOption.tags,
                                        allTags: this.state.allTags,
                                        handleChangeValue: (newValue) => this.handleChangeVariantOptionTags(newValue),
                                        className: "form-group"
                                    } as ITagsProps} />
                                    <TextboxComponent {...{
                                        label: "Sort order",
                                        type: "number",
                                        value: this.state.variantOption.sortOrder,
                                        className: "form-group",
                                        handleChangeValue: (e) => this.handleChangeVariantOptionSortOrder(e)
                                    } as ITextboxProps} />
                                    <CheckboxComponent{...{
                                        text: "Disabled",
                                        value: this.state.variantOption.isDisabled || false,
                                        className: "form-group",
                                        handleChangeValue: (e) => this.handleChangeVariantOptionIsDisabled(e)
                                    } as ICheckboxProps} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Localized properties
                                </div>
                                <LocalizedKitsComponent {...{
                                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source as IVariantOptionLocalizedKitDto, target as IVariantOptionLocalizedKitDto, completed),
                                    editAllLocalizedKits: this.state.authenticatedUser.role.editVariantOptionAllLocalizedKits,
                                    localizedKitUIs: this.state.variantOption.variantOptionLocalizedKits.map((variantOptionLocalizedKit, i) => {
                                        return {
                                            localizedKit: variantOptionLocalizedKit,
                                            html:
                                                <div>
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Title",
                                                            type: "text",
                                                            value: variantOptionLocalizedKit.title,
                                                            placeholder: this.state.variantOption.variantOptionLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                            handleChangeValue: (e) => this.handleChangeVariantOptionLocalizedKitTitle(e, variantOptionLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Description",
                                                            type: "text",
                                                            value: variantOptionLocalizedKit.description,
                                                            placeholder: this.state.variantOption.variantOptionLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                            handleChangeValue: (e) => this.handleChangeVariantOptionLocalizedKitDescription(e, variantOptionLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: `Price (${variantOptionLocalizedKit.country.currencySymbol})`,
                                                            type: "number",
                                                            value: variantOptionLocalizedKit.priceModifier || 0,
                                                            handleChangeValue: (e) => this.handleChangeVariantOptionLocalizedKitPriceModifier(e, variantOptionLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <CheckboxComponent{...{
                                                            text: "Disabled",
                                                            value: variantOptionLocalizedKit.isDisabled || false,
                                                            className: "form-group",
                                                            handleChangeValue: (e) => this.handleChangeVariantOptionLocalizedKitIsDisabled(e, variantOptionLocalizedKit)
                                                        } as ICheckboxProps} />
                                                    } as INestedInputProps} />
                                                </div>
                                        } as ILocalizedKitUI;
                                    })
                                } as ILocalizedKitsProps} />
                            </div>
                        </div>
                    </div>
                }
                {this.state.pageIsLoading && <SpinnerComponent />}
            </div>
        );
    }
}