﻿import { ICouponDto } from "../../../../../Scripts/interfaces/ICouponDto";
import { ICountryDto } from "../../../../../Scripts/interfaces/ICountryDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { ICategoryDto } from "../../../../../Scripts/interfaces/ICategoryDto";

export interface IEditCouponDto {
    authenticatedUser: IUserDto;
    coupon: ICouponDto;
    quantity: number;
    allCategories: Array<ICategoryDto>;
    allCountries: Array<ICountryDto>;
    allUsers: Array<IUserDto>;
}
