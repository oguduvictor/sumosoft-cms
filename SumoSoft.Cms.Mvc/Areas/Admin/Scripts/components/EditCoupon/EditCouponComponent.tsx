﻿import * as React from "react";
import * as dayjs from "dayjs";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { MultiSelectComponent, IMultiSelectProps, SmartOption } from "../MultiSelect/MultiSelectComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { Coupon } from "../../../../../Scripts/classes/Coupon";
import { Constants } from "../../shared/Constants";
import { IEditCouponState } from "./IEditCouponState";
import { Category } from "../../../../../Scripts/classes/Category";
import { Country } from "../../../../../Scripts/classes/Country";
import { User } from "../../../../../Scripts/classes/User";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { IEditCouponDto } from "./IEditCouponDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { CouponTypeEnum } from "./CouponTypeEnum";
import { Functions } from "../../shared/Functions";
import { Autocomplete, AutocompleteOption } from "../../../../../Scripts/sumoReact/Autocomplete/Autocomplete";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

interface IUserAutocomplete { new(): Autocomplete<User> };

export default class EditCouponComponent extends ReactPageComponent<{}, IEditCouponState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            authenticatedUser: new User({ role: new UserRole() }),
            formErrors: new Array<IFormError>(),
            coupon: new Coupon({ referee: new User() }),
            couponType: CouponTypeEnum.Percentage,
            allCategories: new Array<Category>(),
            allCountries: new Array<Country>(),
            allUsers: new Array<User>(),
            isDeleting: false
        } as IEditCouponState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    getCouponType() {
        const coupon = this.state.coupon;
        let couponType = CouponTypeEnum.Percentage;
        if (coupon.amount > 0 && coupon.percentage === 0) {
            couponType = CouponTypeEnum.Amount;
        }

        this.setState({
            couponType
        });
    }

    handleChangeQuantity(e) {
        this.setState({ quantity: e.target.value });
    }

    handleChangeCouponCode(e) {
        this.state.coupon.code = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponType(value: string) {
        const couponType = parseInt(value);
        const coupon = this.state.coupon;
        if (couponType === parseInt(CouponTypeEnum.Percentage.toString())) {
            coupon.amount = 0;
        } else {
            coupon.percentage = 0;
        }
        this.setState({
            coupon,
            couponType
        });
    }

    handleChangeCouponAmount(e) {
        this.state.coupon.amount = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponPercentage(e) {
        this.state.coupon.percentage = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleSelectCouponCategories(newValue: Array<string>) {
        this.state.coupon.categories = newValue.map(categoryId => this.state.allCategories.filter(x => x.id === categoryId)[0]);
        this.setState({
            isModified: true,
            coupon: this.state.coupon
        });
    }

    handleChangeCouponRefereeReward(e) {
        this.state.coupon.refereeReward = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponReferee(referee: User) {
        const coupon = sumoJS.deepClone(this.state.coupon);
        coupon.referee = referee;
        this.setState({ coupon });
    }

    handleChangeCouponRecipient(e) {
        this.state.coupon.recipient = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponCountry(e) {
        const id = e.target.value;
        if (!id.length) {
            this.handleChangeCouponType(CouponTypeEnum.Percentage.toString());
            this.state.coupon.country = null;
        } else {
            const country = new Country();
            country.id = id;
            this.state.coupon.country = country;
        }

        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponExpirationDate(e) {
        this.state.coupon.expirationDate = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponValidityTimes(e) {
        this.state.coupon.validityTimes = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponAllowOnSale(e) {
        this.state.coupon.allowOnSale = e.target.checked;
        this.setState({ coupon: this.state.coupon });
    }

    handleChangeCouponPublished(e) {
        this.state.coupon.published = e.target.checked;
        this.setState({ coupon: this.state.coupon });
    }

    handleUpdateCouponNote(e) {
        this.state.coupon.note = e.target.value;
        this.setState({ coupon: this.state.coupon });
    }

    handleDeleteCoupon() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Coupon_DeleteJson, { couponId: this.state.coupon.id },
            response => {
                if (response.isValid) {
                    window.location.href = Constants.url_Coupon_Index;
                } else {
                    this.setState({
                        formErrors: response.errors,
                        isDeleting: false
                    });
                }
            });
    }

    submit() {
        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Coupon_AddOrUpdate, { coupon: this.state.coupon, quantity: this.state.quantity }, response => {

            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditCouponDto>(Constants.url_Coupon_EditJson,
            {
                couponId: sumoJS.getQueryStringParameter("couponId")
            },
            (editCouponDto) => {
                this.setState({
                    coupon: new Coupon(editCouponDto.coupon),
                    quantity: editCouponDto.quantity,
                    allCategories: editCouponDto.allCategories.map(x => new Category(x)),
                    allCountries: editCouponDto.allCountries.map(x => new Country(x)),
                    allUsers: editCouponDto.allUsers.map(x => new User(x)),
                    pageIsLoading: false,
                    authenticatedUser: new User(editCouponDto.authenticatedUser)
                }, () => this.getCouponType());
            });
    }

    render() {
        const UserAutocomplete = Autocomplete as IUserAutocomplete;

        return (
            <div className="edit-coupon-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.coupon.isNew ? "Create Coupon" : "Edit Coupon"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Coupon_Index} className="btn btn-link">
                            Back to list
                        </a>
                        <ButtonDeleteComponent {...{
                            canDelete: this.state.authenticatedUser.role.deleteCoupon,
                            isDeleting: this.state.isDeleting,
                            handleDelete: () => this.handleDeleteCoupon()
                        } as IButtonDeleteProps} />
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {!this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Coupon values
				                </div>
                                {
                                    this.state.coupon.isNew &&
                                    <TextboxComponent {...{
                                        label: "Quantity",
                                        type: "number",
                                        value: this.state.quantity || 0,
                                        className: "form-group",
                                        handleChangeValue: (e) => this.handleChangeQuantity(e)
                                    } as ITextboxProps} />
                                }
                                {
                                    !this.state.coupon.isNew &&
                                    <TextboxComponent {...{
                                        label: "Code",
                                        type: "text",
                                        value: this.state.coupon.code,
                                        className: "form-group",
                                        handleChangeValue: (e) => this.handleChangeCouponCode(e)
                                    } as ITextboxProps} />
                                }
                                <SelectComponent {...{
                                    options: this.state.allCountries.orderBy(c => c.name)
                                        .map(x => <option key={x.id} value={x.id}>{x.name}</option>),
                                    value: this.state.coupon.country ? this.state.coupon.country.id : "",
                                    className: "form-group",
                                    neutralOption: <option value="">All Countries (Percentage only)</option>,
                                    label: "Country",
                                    handleChangeValue: (e) => this.handleChangeCouponCountry(e)
                                } as ISelectProps} />
                                <SelectComponent {...{
                                    options: [
                                        <option key={CouponTypeEnum.Percentage} value={CouponTypeEnum.Percentage}>{CouponTypeEnum[CouponTypeEnum.Percentage]}</option>,
                                        <option key={CouponTypeEnum.Amount} value={CouponTypeEnum.Amount}>{CouponTypeEnum[CouponTypeEnum.Amount]}</option>
                                    ],
                                    value: this.state.couponType.toString(),
                                    className: `form-group${this.state.coupon.country ? "" : " disabled"}`,
                                    label: "Type",
                                    handleChangeValue: (e) => this.handleChangeCouponType(e.target.value)
                                } as ISelectProps} />
                                {
                                    this.state.couponType === CouponTypeEnum.Amount &&
                                    <TextboxComponent {...{
                                        label: "Amount",
                                        type: "number",
                                        value: this.state.coupon.amount || 0,
                                        className: "form-group",
                                        handleChangeValue: (e) => this.handleChangeCouponAmount(e)
                                    } as ITextboxProps} />
                                }
                                {
                                    this.state.couponType === CouponTypeEnum.Percentage &&
                                    <TextboxComponent {...{
                                        label: "Percentage",
                                        type: "number",
                                        value: this.state.coupon.percentage || 0,
                                        className: "form-group",
                                        handleChangeValue: (e) => this.handleChangeCouponPercentage(e)
                                    } as ITextboxProps} />
                                }
                                <TextboxComponent {...{
                                    label: "Referee Reward",
                                    type: "number",
                                    value: this.state.coupon.refereeReward || 0,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCouponRefereeReward(e)
                                } as ITextboxProps} />
                                <UserAutocomplete
                                    label="Referee"
                                    className="form-group"
                                    options={this.state.allUsers.filter(x => !!x.email).map(x => new AutocompleteOption(x, x.email))}
                                    selectedOption={new AutocompleteOption(this.state.coupon.referee, this.state.coupon.referee && this.state.coupon.referee.email)}
                                    handleChangeValue={(referee: User) => this.handleChangeCouponReferee(referee)} />
                                <TextboxComponent {...{
                                    label: "Recipient",
                                    type: "text",
                                    value: this.state.coupon.recipient,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCouponRecipient(e)
                                } as ITextboxProps} />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Validity conditions
				                </div>
                                <TextboxComponent {...{
                                    label: "Expiration Date",
                                    type: "date",
                                    value: dayjs(this.state.coupon.expirationDate).format("YYYY-MM-DD"),
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCouponExpirationDate(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Validity Times",
                                    type: "number",
                                    value: this.state.coupon.validityTimes || 0,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCouponValidityTimes(e)
                                } as ITextboxProps} />
                                <MultiSelectComponent {...{
                                    options: this.state.allCategories.map(x => new SmartOption(x.id, x.localizedTitle)),
                                    value: this.state.coupon.categories.map(category => category.id),
                                    label: "Categories",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectCouponCategories(newValue)
                                } as IMultiSelectProps} />
                                <TextareaComponent {...{
                                    label: "Note",
                                    type: "text",
                                    value: this.state.coupon.note,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleUpdateCouponNote(e)
                                } as ITextareaProps} />
                                <CheckboxComponent {...{
                                    value: this.state.coupon.allowOnSale,
                                    text: "Apply to products on sale",
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCouponAllowOnSale(e)
                                } as ICheckboxProps} />
                                <CheckboxComponent {...{
                                    value: this.state.coupon.published,
                                    text: "Published",
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCouponPublished(e)
                                } as ICheckboxProps} />
                            </div>
                        </div>
                    </div>
                }
                {this.state.pageIsLoading && <SpinnerComponent />}
            </div>
        );
    }
}