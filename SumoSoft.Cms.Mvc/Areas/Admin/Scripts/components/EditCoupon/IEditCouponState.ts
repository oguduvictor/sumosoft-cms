﻿import { IReactPageState } from '../ReactPage/ReactPageComponent';
import { Coupon } from '../../../../../Scripts/classes/Coupon';
import { Category } from '../../../../../Scripts/classes/Category';
import { Country } from '../../../../../Scripts/classes/Country';
import { User } from '../../../../../Scripts/classes/User';
import { CouponTypeEnum } from './CouponTypeEnum';

export interface IEditCouponState extends IReactPageState {
	coupon?: Coupon;
	couponType?: CouponTypeEnum;
	authenticatedUser?: User;
	quantity?: number;
	allCategories?: Array<Category>;
	allCountries?: Array<Country>;
	allUsers?: Array<User>;
	isDeleting?: boolean;
}
