﻿export enum CouponTypeEnum {
    "Percentage" = 0,
    "Amount" = 1
}