﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ICmsSettingDto } from "./ICmsSettingDto";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { RadioComponent, IRadioProps } from "../Radio/RadioComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Constants } from "../../shared/Constants";
import { ISettingState } from "./ISettingState";
import { CmsSetting } from "./CmsSetting";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class SettingComponent extends ReactPageComponent<{}, ISettingState> {

    constructor(props) {
        super(props);

        this.state = {
            formErrors: new Array<IFormError>(),
            cmsSetting: new CmsSetting(),
            pageIsLoading: true,
            flushCacheKey: "",
            flushCacheSubmitting: false,
        } as ISettingState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleChangeSmtpHost(e) {
        this.state.cmsSetting.smtpHost = e.target.value;
        this.setState({
            cmsSetting: this.state.cmsSetting,
            isModified: true
        });
    }

    handleChangeSmtpPort(e) {
        this.state.cmsSetting.smtpPort = e.target.value;
        this.setState({
            cmsSetting: this.state.cmsSetting,
            isModified: true
        });
    }

    handleChangeSmtpDisplayName(e) {
        this.state.cmsSetting.smtpDisplayName = e.target.value;
        this.setState({
            cmsSetting: this.state.cmsSetting,
            isModified: true
        });
    }

    handleChangeSmtpEmail(e) {
        this.state.cmsSetting.smtpEmail = e.target.value;
        this.setState({
            cmsSetting: this.state.cmsSetting,
            isModified: true
        });
    }

    handleChangeSmtpPassword(e) {
        this.state.cmsSetting.smtpPassword = e.target.value;
        this.setState({
            cmsSetting: this.state.cmsSetting,
            isModified: true
        });
    }

    handleChangeUseAzureStorage(value: boolean) {
        this.state.cmsSetting.useAzureStorage = value;
        this.setState({
            cmsSetting: this.state.cmsSetting,
            isModified: true
        });
    }

    handleChangeMemoryCacheKey(flushCacheKey: string) {
        this.setState({
            flushCacheKey
        });
    }

    handleFlushCache() {
        this.setState({
            flushCacheSubmitting: true
        });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Setting_FlushCache, { key: this.state.flushCacheKey }, response => {
            this.setState({
                flushCacheSubmitting: false,
                formSuccessMessage: response.successMessage
            });
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<ICmsSettingDto>(Constants.url_Setting_IndexJson,
            "",
            (cmsSettingDto) => {
                this.setState({
                    cmsSetting: cmsSettingDto,
                    pageIsLoading: false
                });
            });
    }

    submit() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Setting_AddOrUpdate, this.state.cmsSetting,
            response => {
                if (response.isValid) {
                    if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                    this.setStateFromApi();
                    this.setState({
                        formSuccessMessage: Constants.alert_saveSuccess,
                        isModified: false,
                        isSubmitting: false
                    });
                } else {
                    this.setState({
                        formErrors: response.errors,
                        isModified: false,
                        isSubmitting: false
                    });
                }
            });
    }

    render() {
        return (
            <div className="setting-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Settings
                    </div>
                    <div className="admin-top-bar-controls">
                        <ButtonSubmitComponent {...{
                            handleSubmit: () => this.submit(),
                            isSubmitting: this.state.isSubmitting
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Email SMTP account
                                </div>
                                <TextboxComponent {...{
                                    label: "Host",
                                    type: "text",
                                    value: this.state.cmsSetting.smtpHost,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeSmtpHost(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Port",
                                    type: "number",
                                    value: this.state.cmsSetting.smtpPort,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeSmtpPort(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Display name",
                                    type: "text",
                                    value: this.state.cmsSetting.smtpDisplayName,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeSmtpDisplayName(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Address",
                                    type: "text",
                                    value: this.state.cmsSetting.smtpEmail,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeSmtpEmail(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Password",
                                    type: "password",
                                    value: this.state.cmsSetting.smtpPassword,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeSmtpPassword(e)
                                } as ITextboxProps} />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Media
                                </div>
                                <RadioComponent {...{
                                    value: !this.state.cmsSetting.useAzureStorage,
                                    className: "form-group",
                                    addon: "Save files in the local FTP folder",
                                    handleChangeValue: () => this.handleChangeUseAzureStorage(false)
                                } as IRadioProps}
                                />
                                <RadioComponent {...{
                                    value: this.state.cmsSetting.useAzureStorage,
                                    className: "form-group",
                                    addon: "Use a Windows Azure Storage Account",
                                    handleChangeValue: () => this.handleChangeUseAzureStorage(true)
                                } as IRadioProps}
                                />
                                <small>Note: The Storage Account connection string must be defined in your Web.Config</small>
                            </div>
                            <div className="admin-subsection cache">
                                <div className="admin-subsection-title">
                                    Cache
                                </div>
                                <div className="row">
                                    <div className="col-md-8">
                                        <TextboxComponent {...{
                                            label: "Flush memory cache entries whose key contains",
                                            type: "text",
                                            value: this.state.flushCacheKey,
                                            className: "form-group",
                                            handleChangeValue: (e) => this.handleChangeMemoryCacheKey(e.target.value)
                                        } as ITextboxProps} />
                                    </div>
                                    <div className="col-md-4">
                                        <button
                                            type="button"
                                            className="btn btn-default"
                                            onClick={() => this.handleFlushCache()}>
                                            Flush
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}