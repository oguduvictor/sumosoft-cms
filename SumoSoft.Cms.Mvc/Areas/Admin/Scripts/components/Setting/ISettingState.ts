﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import {CmsSetting} from "./CmsSetting";

export interface ISettingState extends IReactPageState {
    cmsSetting?: CmsSetting;
    flushCacheKey?: string,
    flushCacheSubmitting?: boolean,
}