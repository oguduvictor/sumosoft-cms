﻿export interface ICmsSettingDto {
    isSeeded: boolean;
    smtpHost: string;
    smtpPort: number;
    smtpDisplayName: string;
    smtpEmail: string;
    smtpPassword: string;
    useAzureStorage: boolean;
}