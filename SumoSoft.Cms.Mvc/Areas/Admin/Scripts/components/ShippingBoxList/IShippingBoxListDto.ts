﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IShippingBoxDto } from "../../../../../Scripts/interfaces/IShippingBoxDto";

export interface IShippingBoxListDto {
    authenticatedUser: IUserDto;
    shippingBoxes?: Array<IShippingBoxDto>;
}