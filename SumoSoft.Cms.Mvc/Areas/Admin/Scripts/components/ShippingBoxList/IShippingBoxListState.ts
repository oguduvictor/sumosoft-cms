﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";

export interface IShippingBoxListState extends IReactPageState {
    shippingBoxes?: Array<ShippingBox>;
    sortedShippingBoxes?: Array<ShippingBox>;
}