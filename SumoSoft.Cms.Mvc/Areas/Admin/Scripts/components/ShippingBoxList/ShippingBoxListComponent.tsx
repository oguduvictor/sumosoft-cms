﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { IShippingBoxListState } from "./IShippingBoxListState";
import { IShippingBoxListDto } from "./IShippingBoxListDto";
import { ISortableData, ISortableRow, ISortableRowCellStyle, ISortableProps, SortableComponent } from '../Sortable/SortableComponent';
import { ButtonSubmitComponent, IButtonSubmitProps } from '../ButtonSubmit/ButtonSubmitComponent';
import { IFormResponse } from '../../../../../Scripts/interfaces/IFormResponse';
import { AlertComponent, IAlertProps } from '../Alert/AlertComponent';

export default class ShippingBoxListComponent extends ReactPageComponent<{}, IShippingBoxListState> {

    constructor(props) {
        super(props);

        this.state = {
            shippingBoxes: new Array<ShippingBox>(),
            sortedShippingBoxes: new Array<ShippingBox>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IShippingBoxListState;
    }

    componentDidMount() {
        this.setState({
            pageIsLoading: true
        });

        sumoJS.ajaxPost<IShippingBoxListDto>(Constants.url_ShippingBox_IndexJson, null, (shippingBoxListDto) => {
            this.setState({
                pageIsLoading: false,
                shippingBoxes: shippingBoxListDto.shippingBoxes.map(x => new ShippingBox(x)),
                authenticatedUser: new User(shippingBoxListDto.authenticatedUser)
            });
        });
    }

    getSortableRows(): ISortableRow[] {
        const shippingBoxes = this.state.shippingBoxes.orderBy(x => x.sortOrder) as Array<ShippingBox>;

        return shippingBoxes.map(shippingBox => {
            return {
                id: shippingBox.id,
                contentItems: [
                    { item: shippingBox.name },
                    { item: shippingBox.localizedTitle },
                    {
                        item:
                            <div className="dropdown">
                                <button className="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                    Options
                                <span className="caret"></span>
                                </button>
                                <ul className="dropdown-menu">
                                    <li>
                                        <a href={Constants.url_ShippingBox_Edit + shippingBox.id}>
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onClick={e => this.handleDuplicate(e, shippingBox.id)}>
                                            Duplicate
                                        </a>
                                    </li>
                                </ul>
                            </div>
                    }
                ],
                children: null,
                cellStyles: [
                    { cellIndices: [0, 1, 2], style: { paddingRight: "5px", overflow: "hidden", whiteSpace: "nowrap", textOverflow: "ellipsis" } } as ISortableRowCellStyle,
                    { cellIndices: [0], style: { width: "35rem" } } as ISortableRowCellStyle,
                    { cellIndices: [2], style: { width: "10rem" } } as ISortableRowCellStyle,
                ]
            } as ISortableRow;
        });
    }

    handleChangeSorting(data: Array<ISortableData>) {
        const sortedShippingBoxes = data as Array<ShippingBox>;

        this.setState({
            sortedShippingBoxes,
            isModified: true
        });
    }

    handleDuplicate(e, shippingBoxId: string) {
        sumoJS.ajaxPost<IFormResponse>(Constants.url_ShippingBox_Duplicate, { id: shippingBoxId }, response => {
            if (response.isValid) {
                window.location.href = response.redirectUrl;
            } else {
                this.setState({
                    isSubmitting: false
                });
            }
        });
    }

    handleSubmit() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse<Array<ShippingBox>>>(Constants.url_ShippingBox_UpdateSortOrder, { shippingBoxDtos: this.state.sortedShippingBoxes }, response => {
            if (response.isValid) {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess,
                    shippingBoxes: response.target,
                    sortedShippingBoxes: []
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false,
                    sortedShippingBoxes: []
                });
            }
        });
    }

    render() {
        return (
            <div className="shipping-box-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Shipping Boxes
                    </div>
                    <div className="admin-top-bar-controls">
                        <a className={`btn btn-primary ${this.state.authenticatedUser.role.createShippingBox ? "" : "disabled"}`} href={Constants.url_ShippingBox_Edit}>
                            <i className="fa fa-plus"> </i>
                            Create new shipping Box
                        </a>
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            className: this.state.isModified ? "" : "disabled",
                            handleSubmit: () => this.handleSubmit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <SortableComponent {...{
                            headerRowItems: ["Name", "Title", "", ""],
                            rows: this.getSortableRows(),
                            className: "sortable-attribute-options",
                            handleChangeSorting: (data) => this.handleChangeSorting(data),
                            isFixedTableLayout: true
                        } as ISortableProps} />
                    </div>
                }
            </div>
        );
    }
}