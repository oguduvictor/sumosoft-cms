﻿import { IAttributeDto } from "../../../../../Scripts/interfaces/IAttributeDto";
import { IAttributeOptionDto } from "../../../../../Scripts/interfaces/IAttributeOptionDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditAttributeDto {
    authenticatedUser: IUserDto;
    attribute: IAttributeDto;
    allTags: Array<string>;
    newAttributeOption: IAttributeOptionDto;
}
