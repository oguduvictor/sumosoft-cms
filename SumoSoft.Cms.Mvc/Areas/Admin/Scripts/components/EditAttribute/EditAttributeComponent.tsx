﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { Constants } from "../../shared/Constants";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { AttributeTypesEnum } from "../../../../../Scripts/enums/AttributeTypesEnum";
import { IEditAttributeState } from "./IEditAttributeState";
import { Attribute } from "../../../../../Scripts/classes/Attribute";
import { AttributeOption } from "../../../../../Scripts/classes/AttributeOption";
import { AttributeLocalizedKit } from "../../../../../Scripts/classes/AttributeLocalizedKit";
import { AttributeOptionLocalizedKit } from "../../../../../Scripts/classes/AttributeOptionLocalizedKit";
import { User } from "../../../../../Scripts/classes/User";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { IEditAttributeDto } from "./IEditAttributeDto";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { TagsComponent, ITagsProps } from "../Tags/TagsComponent";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditAttributeComponent extends ReactPageComponent<{}, IEditAttributeState> {
    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            attribute: new Attribute(),
            allTags: new Array<string>(),
            newAttributeOption: new AttributeOption(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IEditAttributeState;

        this.handleChangeAttributeName = this.handleChangeAttributeName.bind(this);
        this.handleChangeAttributeType = this.handleChangeAttributeType.bind(this);
        this.handleChangeAttributeSortOrder = this.handleChangeAttributeSortOrder.bind(this);
        this.handleChangeAttributeLocalizedKitTitle = this.handleChangeAttributeLocalizedKitTitle.bind(this);
        this.handleChangeAttributeOptionName = this.handleChangeAttributeOptionName.bind(this);
        this.handleChangeAttributeOptionLocalizedKitTitle = this.handleChangeAttributeOptionLocalizedKitTitle.bind(this);
        this.handleAddAttributeOption = this.handleAddAttributeOption.bind(this);
        this.handleDeleteAttribute = this.handleDeleteAttribute.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleUpdateUrlOnBlur(source: string) {
        const attribute = this.state.attribute;
        if (!attribute.url || attribute.url === "") {
            attribute.url = Functions.convertToUrl(source);
            this.setState({ attribute });
        }
    }

    handleChangeAttributeName(e) {
        this.state.attribute.name = e.target.value;
        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeUrl(e) {
        this.state.attribute.url = e.target.value;
        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeType(e) {
        this.state.attribute.type = Number(e.target.value);
        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeSortOrder(e) {
        this.state.attribute.sortOrder = e.target.value;
        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeLocalizedKitTitle(e, attributeLocalizedKit: AttributeLocalizedKit) {
        attributeLocalizedKit.title = e.target.value;
        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeLocalizedKitDescription(e, attributeLocalizedKit: AttributeLocalizedKit) {
        attributeLocalizedKit.description = e.target.value;
        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeOptionName(e, attributeOption: AttributeOption) {
        attributeOption.name = e.target.value;
        attributeOption.isModified = true;

        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleUpdateAttributeOptionUrlOnBlur(source: string, attributeOption: AttributeOption) {
        if (!attributeOption.url || attributeOption.url === "") {
            attributeOption.url = Functions.convertToUrl(source);
            this.setState({
                attribute: this.state.attribute
            });
        }
    }

    handleChangeAttributeOptionUrl(e, attributeOption: AttributeOption) {
        attributeOption.url = e.target.value;
        attributeOption.isModified = true;

        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeOptionTags(newValue, attributeOption: AttributeOption) {
        attributeOption.tags = newValue;
        attributeOption.isModified = true;

        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeOptionLocalizedKitTitle(e, attributeOptionLocalizedKit: AttributeOptionLocalizedKit, attributeOption: AttributeOption) {
        attributeOptionLocalizedKit.title = e.target.value;
        attributeOption.isModified = true;

        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleChangeAttributeOptionLocalizedKitDescription(e, attributeOptionLocalizedKit: AttributeOptionLocalizedKit, attributeOption: AttributeOption) {
        attributeOptionLocalizedKit.description = e.target.value;
        attributeOption.isModified = true;

        this.setState({ isModified: true, attribute: this.state.attribute });
    }

    handleAddAttributeOption() {
        const newAttributeOption = new AttributeOption(this.state.newAttributeOption);

        // --------------------------------------------------------------------------------
        // Set new ids for every new attributeOption and attributeOptionLocalizedKits added
        // So that new attribute options and their localizedkits dont share ids
        // --------------------------------------------------------------------------------

        newAttributeOption.id = Functions.newGuid();
        newAttributeOption.attributeOptionLocalizedKits.forEach(x => {
            x.id = Functions.newGuid();
            x.attributeOption.id = newAttributeOption.id;
        });

        this.state.attribute.attributeOptions.unshift(newAttributeOption);
        this.setState({ attribute: this.state.attribute });
    }

    handleRemoveAttributeOption(attributeOption: AttributeOption) {
        if (!attributeOption.isNew && !this.state.authenticatedUser.role.deleteAttributeOption) {
            return;
        }

        if (attributeOption.isNew) {
            const index = this.state.attribute.attributeOptions.indexOf(attributeOption);
            this.state.attribute.attributeOptions.splice(index, 1);
        } else {
            attributeOption.isDeleted = true;
        }

        this.setState({
            isModified: true,
            attribute: this.state.attribute
        });
    }

    handleDeleteAttribute() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Attribute_Delete, { id: this.state.attribute.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Attribute;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleLocalizedKitCopyValuesFrom(source, target, completed) {
        target.title = source.title;
        this.setState(
            {
                isModified: true,
                attribute: this.state.attribute
            },
            () => completed()
        );
    }

    submit() {
        this.setState({ isSubmitting: true });

        const attribute = sumoJS.deepClone(this.state.attribute);
        attribute.attributeOptions = attribute.attributeOptions.filter(x => x.isModified || x.isDeleted);

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Attribute_AddOrUpdate, attribute, response => {
            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        var apiParam = {
            attributeId: sumoJS.getQueryStringParameter("attributeId")
        };
        sumoJS.ajaxPost<IEditAttributeDto>(Constants.url_Attribute_EditJson, apiParam, editAttributeDto => {
            this.setState(
                {
                    authenticatedUser: new User(editAttributeDto.authenticatedUser),
                    attribute: new Attribute(editAttributeDto.attribute),
                    newAttributeOption: new AttributeOption(editAttributeDto.newAttributeOption),
                    allTags: editAttributeDto.allTags,
                    pageIsLoading: false
                },
                () => {
                    Functions.setCreateAndEditPermissions(
                        this.state.attribute,
                        this.state.authenticatedUser.role.createAttribute,
                        this.state.authenticatedUser.role.editAttribute,
                        $(".attribute")
                    );

                    Functions.setCreateAndEditPermissions(
                        this.state.attribute,
                        this.state.authenticatedUser.role.createAttributeOption,
                        this.state.authenticatedUser.role.editAttributeOption,
                        $(".attribute-options")
                    );
                }
            );
        });
    }

    render() {
        return (
            <div className="edit-attribute-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">{this.state.attribute.isNew ? " Create Attribute" : " Edit Attribute"}</div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Attribute} className="btn btn-link">
                            Back to List
                        </a>
                        {!this.state.attribute.isNew && (
                            <ButtonDeleteComponent
                                {...{
                                    isDeleting: this.state.isDeleting,
                                    canDelete: this.state.authenticatedUser.role.deleteAttribute,
                                    handleDelete: () => this.handleDeleteAttribute()
                                } as IButtonDeleteProps}
                            />
                        )}
                        <a className={`btn btn-primary ${this.state.authenticatedUser.role.createAttributeOption ? "" : "disabled"}`} onClick={this.handleAddAttributeOption}>
                            <i className="fa fa-plus" />
                            Add new option
                        </a>
                        <ButtonSubmitComponent
                            {...{
                                isSubmitting: this.state.isSubmitting,
                                handleSubmit: () => this.submit()
                            } as IButtonSubmitProps}
                        />
                    </div>
                </div>
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                <div className="row">
                    <div className="col-md-6 attribute">
                        <div className="admin-subsection">
                            <div className="admin-subsection-title">Global properties</div>
                            <TextboxComponent
                                {...{
                                    label: "Name",
                                    type: "text",
                                    value: this.state.attribute.name,
                                    tooltip: Constants.tooltip_name,
                                    className: "form-group",
                                    readOnly: Functions.getReadOnlyStateForRequiredField(this.state.attribute, this.state.authenticatedUser.role.editAttributeName),
                                    handleChangeValue: e => this.handleChangeAttributeName(e),
                                    handleBlur: e => this.handleUpdateUrlOnBlur(e.target.value)
                                } as ITextboxProps}
                            />
                            <TextboxComponent
                                {...{
                                    label: "Url",
                                    type: "text",
                                    value: this.state.attribute.url,
                                    tooltip: Constants.tooltip_url,
                                    className: "form-group",
                                    handleChangeValue: e => this.handleChangeAttributeUrl(e)
                                } as ITextboxProps}
                            />
                            <div className="form-group">
                                <SelectComponent
                                    {...{
                                        options: Object.keys(AttributeTypesEnum)
                                            .filter(x => typeof AttributeTypesEnum[x] !== "number")
                                            .map((value, i) => (
                                                <option key={value} value={value}>
                                                    {this.state.attribute.typeDescriptions[i]}
                                                </option>
                                            )),
                                        value: this.state.attribute.type ? this.state.attribute.type.toString() : "",
                                        label: "Type",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeAttributeType(e)
                                    } as ISelectProps}
                                />
                            </div>
                            <TextboxComponent
                                {...{
                                    label: "Sort order",
                                    type: "number",
                                    value: this.state.attribute.sortOrder || 0,
                                    className: "form-group",
                                    handleChangeValue: e => this.handleChangeAttributeSortOrder(e),
                                } as ITextboxProps}
                            />
                        </div>
                        <div className="admin-subsection">
                            <div className="admin-subsection-title">Localized properties</div>
                            <LocalizedKitsComponent
                                {...{
                                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source, target, completed),
                                    editAllLocalizedKits: this.state.authenticatedUser.role.editAttributeAllLocalizedKits,
                                    localizedKitUIs: this.state.attribute.attributeLocalizedKits.map((localizedKit, i) => {
                                        return {
                                            localizedKit: localizedKit,
                                            html:
                                            <div>
                                                <NestedInputComponent
                                                    {...{
                                                        input: (
                                                            <TextboxComponent
                                                                {...{
                                                                    label: "Title",
                                                                    type: "text",
                                                                    value: localizedKit.title,
                                                                    placeholder: this.state.attribute.attributeLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                                    handleChangeValue: e => this.handleChangeAttributeLocalizedKitTitle(e, localizedKit),
                                                                    handleBlur: e => this.handleUpdateUrlOnBlur(e.target.value)
                                                                } as ITextboxProps}
                                                            />
                                                        )
                                                    } as INestedInputProps} />
                                                <NestedInputComponent
                                                    {...{
                                                        input: (
                                                            <TextboxComponent
                                                                {...{
                                                                    label: "Description",
                                                                    type: "text",
                                                                    value: localizedKit.description,
                                                                    placeholder: this.state.attribute.attributeLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                                    handleChangeValue: e => this.handleChangeAttributeLocalizedKitDescription(e, localizedKit),
                                                                    handleBlur: e => this.handleUpdateUrlOnBlur(e.target.value)
                                                                } as ITextboxProps}
                                                            />
                                                        )
                                                    } as INestedInputProps} />
                                            </div>
                                        } as ILocalizedKitUI;
                                    })
                                } as ILocalizedKitsProps}
                            />
                        </div>
                    </div>
                    <div className="col-md-6 attribute-options">
                        {this.state.attribute.attributeOptions && !this.state.attribute.attributeOptions.length && (
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">Attribute Option</div>
                            </div>
                        )}
                        {this.state.attribute.attributeOptions && this.state.attribute.attributeOptions.length
                            ? this.state.attribute.attributeOptions
                                  .filter(x => !x.isDeleted)
                                  .map((attributeOption, i) => (
                                      <div className="admin-subsection" key={attributeOption.id}>
                                          <div className="admin-subsection-title">Attribute Option</div>
                                          <div className="admin-subsection-controls">
                                              <button
                                                  type="button"
                                                  className={`btn-link button-remove ${
                                                      attributeOption.isNew || this.state.authenticatedUser.role.deleteAttributeOption ? "" : "disabled"
                                                  }`}
                                                  onClick={() => this.handleRemoveAttributeOption(attributeOption)}
                                              >
                                                  <i className="fa fa-trash-o" />
                                                  &nbsp; Remove
                                              </button>
                                          </div>
                                          <div className="attribute-option">
                                              <TextboxComponent
                                                  {...{
                                                      label: "Name",
                                                      type: "text",
                                                      value: attributeOption.name,
                                                      tooltip: Constants.tooltip_name,
                                                      className: "form-group",
                                                      readOnly: Functions.getReadOnlyStateForRequiredField(
                                                          attributeOption,
                                                          this.state.authenticatedUser.role.editAttributeOptionName
                                                      ),
                                                      handleBlur: e => this.handleUpdateAttributeOptionUrlOnBlur(e.target.value, attributeOption),
                                                      handleChangeValue: e => this.handleChangeAttributeOptionName(e, attributeOption)
                                          } as ITextboxProps}
                                      />
                                              <TextboxComponent
                                                  {...{
                                                      label: "Url",
                                                      type: "text",
                                                      value: attributeOption.url,
                                                      tooltip: Constants.tooltip_url,
                                                      className: "form-group",
                                                      handleChangeValue: e => this.handleChangeAttributeOptionUrl(e, attributeOption)
                                                  } as ITextboxProps}
                                              />
                                              <TagsComponent
                                                  {...{
                                                      value: attributeOption.tags,
                                                      allTags: this.state.allTags,
                                                      handleChangeValue: newValue => this.handleChangeAttributeOptionTags(newValue, attributeOption),
                                                      className: "form-group"
                                                  } as ITagsProps}
                                              />
                                              <LocalizedKitsComponent
                                                  {...{
                                                      handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source, target, completed),
                                                      editAllLocalizedKits: this.state.authenticatedUser.role.editAttributeOptionAllLocalizedKits,
                                                      localizedKitUIs: attributeOption.attributeOptionLocalizedKits.map((attributeOptionLocalizedKit, j) => {
                                                          return {
                                                              localizedKit: attributeOptionLocalizedKit,
                                                              html: 
                                                              <div>
                                                                  <NestedInputComponent
                                                                      {...{
                                                                          input: (
                                                                              <TextboxComponent
                                                                                  {...{
                                                                                      label: "Title",
                                                                                      type: "text",
                                                                                      value: attributeOptionLocalizedKit.title,
                                                                                      placeholder: attributeOption.attributeOptionLocalizedKits.filter(x => x.country.isDefault)[0]
                                                                                          .title,
                                                                                      handleChangeValue: e =>
                                                                                          this.handleChangeAttributeOptionLocalizedKitTitle(
                                                                                              e,
                                                                                              attributeOptionLocalizedKit,
                                                                                              attributeOption
                                                                                          )
                                                                                  } as ITextboxProps}
                                                                              />
                                                                          )
                                                                      } as INestedInputProps} />
                                                                  <NestedInputComponent
                                                                      {...{
                                                                          input: (
                                                                              <TextboxComponent
                                                                                  {...{
                                                                                      label: "Description",
                                                                                      type: "text",
                                                                                      value: attributeOptionLocalizedKit.description,
                                                                                      placeholder: attributeOption.attributeOptionLocalizedKits.filter(x => x.country.isDefault)[0]
                                                                                          .description,
                                                                                      handleChangeValue: e =>
                                                                                          this.handleChangeAttributeOptionLocalizedKitDescription(
                                                                                              e,
                                                                                              attributeOptionLocalizedKit,
                                                                                              attributeOption
                                                                                          )
                                                                                  } as ITextboxProps}
                                                                              />
                                                                          )
                                                                      } as INestedInputProps} />
                                                              </div>
                                                          } as ILocalizedKitUI;
                                                      })
                                                  } as ILocalizedKitsProps}
                                              />
                                          </div>
                                      </div>
                                  ))
                            : ""}
                    </div>
                </div>
            </div>
        );
    }
}
