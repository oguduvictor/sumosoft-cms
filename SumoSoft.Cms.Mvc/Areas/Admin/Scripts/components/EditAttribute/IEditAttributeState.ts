﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Attribute } from "../../../../../Scripts/classes/Attribute";
import { AttributeOption } from "../../../../../Scripts/classes/AttributeOption";

export interface IEditAttributeState extends IReactPageState {
    attribute?: Attribute;
    allTags?: Array<string>;
    newAttributeOption?: AttributeOption;
    pageIsLoading?: boolean;
}
