﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { ICountryDto } from "../../../../../Scripts/interfaces/ICountryDto";

export interface ICountryListDto {
    authenticatedUser?: IUserDto;
    countries?: Array<ICountryDto>;
}