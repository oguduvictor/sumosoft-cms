﻿import { Country } from "../../../../../Scripts/classes/Country";
import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export interface ICountryListState extends IReactPageState {
    formErrors?: Array<IFormError>;
    countries?: Array<Country>;
    isSettingDefault?: boolean;
    pageIsLoading?: boolean;
} 