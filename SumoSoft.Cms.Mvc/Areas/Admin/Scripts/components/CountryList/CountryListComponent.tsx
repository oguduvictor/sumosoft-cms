﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { Country } from "../../../../../Scripts/classes/Country";
import { Constants } from "../../shared/Constants";
import { ICountryListState } from "./ICountryListState";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { ICountryListDto } from "./ICountryListDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class CountryListComponent extends ReactPageComponent<{}, ICountryListState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            formErrors: new Array<IFormError>(),
            countries: new Array<Country>(),
            isSettingDefault: false
        } as ICountryListState;
    }



    componentDidMount() {
        this.setState({
            pageIsLoading: true
        });

        this.setStateFromApi();
    }

    handleSetDefaultCountry(e, country) {
        this.setState({ isSettingDefault: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Country_SetDefaultCountry, { countryId: country.id }, response => {

            if (response.isValid) {
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    isSettingDefault: false
                });
                this.setStateFromApi();
            } else {
                this.setState({
                    formErrors: response.errors,
                    isSettingDefault: false
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<ICountryListDto>(Constants.url_Country_IndexJson, null, countryListDto => {
            this.setState({
                authenticatedUser: new User(countryListDto.authenticatedUser),
                countries: countryListDto.countries.map(x => new Country(x)),
                pageIsLoading: false
            });
        });
    }

    render() {
        return (
            <div className="country-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Countries
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Country_Edit} className={`btn btn-primary ${this.state.authenticatedUser.role.createCountry ? "" : "disabled"}`}>
                            <i className="fa fa-plus"></i>
                            Add new country
                        </a>
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {this.state.pageIsLoading && <SpinnerComponent />}
                {!this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <TableComponent {...{
                            thList: ["Icon", "Name", "Url", "Language Code", "Payment Account", "Localization", "Status", "Default", ""],
                            trList: this.state.countries
                                .map((country) => [
                                    <span className={`flag-icon ${country.flagIcon}`}></span>,
                                    country.name,
                                    `/${country.url || ""}`,
                                    country.languageCode,
                                    country.paymentAccountId,
                                    country.localize ? "Enabled" : "Disabled",
                                    country.isDisabled ? "Disabled" : "Enabled",
                                    country.isDefault ?
                                        <i className="fa fa-check"></i> :
                                        <button type="button" className={`btn btn-default btn-xs ${this.state.isSettingDefault ? "disabled" : ""}`} onClick={(e) => this.handleSetDefaultCountry(e, country)}>
                                            Set default
                                        </button>,
                                    <a className="btn btn-primary btn-xs" href={Constants.url_Country_Edit + country.id}>edit</a>
                                ])
                        } as ITableProps} />
                    </div>
                }
            </div>
        );
    }
}