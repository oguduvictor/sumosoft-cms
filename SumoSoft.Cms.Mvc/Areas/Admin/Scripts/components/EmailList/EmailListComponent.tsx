﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { Email } from "../../../../../Scripts/classes/Email";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { IEmailListState } from "./IEmailListState";
import { IEmailListDto } from "./IEmailListDto";

export default class EmailListComponent extends ReactPageComponent<{}, IEmailListState> {

    constructor(props) {
        super(props);

        this.state = {
            emails: new Array<Email>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IEmailListState;
    }

    componentDidMount() {
        this.setState({
            pageIsLoading: true
        });

        sumoJS.ajaxPost<IEmailListDto>(Constants.url_Email_IndexJson, null, (emailListDto) => {
            this.setState({
                pageIsLoading: false,
                emails: emailListDto.emails.map(x => new Email(x)),
                authenticatedUser: new User(emailListDto.authenticatedUser)
            });
        });
    }

    render() {
        return (
            <div className="email-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Emails
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Email_Edit} className={`btn btn-primary ${this.state.authenticatedUser.role.createEmail ? "" : "disabled"}`}>
                            <i className="fa fa-plus"></i>
                            Create new email
                        </a>
                    </div>
                </div>
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <TableComponent {...{
                            thList: ["Name", "Template", ""],
                            trList: this.state.emails.map((email) => [email.name, email.viewName,
                                <div>
                                    <a href={Constants.url_Email_Edit + email.id} className="btn btn-primary btn-xs">
                                        edit
                                    </a>
                                </div>
                            ])
                        } as ITableProps} />
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}