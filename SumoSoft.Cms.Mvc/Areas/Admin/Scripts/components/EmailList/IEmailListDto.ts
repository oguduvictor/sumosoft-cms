﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IEmailDto } from "../../../../../Scripts/interfaces/IEmailDto";

export interface IEmailListDto {
    authenticatedUser: IUserDto;
    emails: Array<IEmailDto>;
}