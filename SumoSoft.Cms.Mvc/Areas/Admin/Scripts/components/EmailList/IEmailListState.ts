﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Email } from "../../../../../Scripts/classes/Email";

export interface IEmailListState extends IReactPageState {
    emails?: Array<Email>;
}