﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { Product } from "../../../../../Scripts/classes/Product";
import { VariantOption } from "../../../../../Scripts/classes/VariantOption";
import { VariantTypesEnum } from "../../../../../Scripts/enums/VariantTypesEnum";
import { Constants } from "../../shared/Constants";
import { ProductVariant } from "../../../../../Scripts/classes/ProductVariant";
import { Variant } from "../../../../../Scripts/classes/Variant";
import { IEditProductVariantsState } from "./IEditProductVariantsState";
import { IEditProductVariantsDto } from "./IEditProductVariantsDto";
import { MultiSelectComponent, IMultiSelectProps, SmartOption } from "../MultiSelect/MultiSelectComponent";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditProductVariantsComponent extends ReactPageComponent<{}, IEditProductVariantsState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            productVariants: new Array<ProductVariant>(),
            allVariants: new Array<Variant>()
        } as IEditProductVariantsState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleAddProductVariant(variant: Variant) {
        const deletedProductVariant = this.state.productVariants.filter(x => x.variant.id === variant.id && x.isDeleted)[0];

        if (deletedProductVariant) {
            deletedProductVariant.isDeleted = false;
        } else {
            const productVariant = new ProductVariant();
            productVariant.isNew = true;
            productVariant.variant = variant;
            productVariant.product = new Product();
            productVariant.product.id = this.state.productId;
            productVariant.defaultBooleanValue = variant.defaultBooleanValue;
            productVariant.defaultDoubleValue = variant.defaultDoubleValue;
            productVariant.defaultIntegerValue = variant.defaultIntegerValue;
            productVariant.defaultStringValue = variant.defaultStringValue;
            productVariant.defaultVariantOptionValue = variant.defaultVariantOptionValue || variant.variantOptions[0];

            this.state.productVariants.unshift(productVariant);
        }

        this.setState({ productVariants: this.state.productVariants });
    }

    handleRemoveProductVariant(productVariant: ProductVariant) {
        if (productVariant.isNew) {
            const index = this.state.productVariants.indexOf(productVariant);
            this.state.productVariants.splice(index, 1);
        } else {
            productVariant.isDeleted = true;
        }

        this.setState({
            isModified: true,
            productVariants: this.state.productVariants
        });
    }

    handleSelectVariantOptions(newValue: Array<string>, productVariant: ProductVariant) {
        productVariant.isModified = true;
        productVariant.variantOptions = newValue.map(variantOptionId => productVariant.variant.variantOptions.filter(x => x.id === variantOptionId)[0]);
        if (!productVariant.defaultVariantOptionValue || productVariant.variantOptions.every(x => x.id !== productVariant.defaultVariantOptionValue.id)) {
            // If the current defaultVariantOptionValue is null, or it's not a value included
            // in the available variant options, change it to the first available variant option:
            productVariant.defaultVariantOptionValue = productVariant.variantOptions[0];
        }
        this.setState({
            isModified: true,
            productVariants: this.state.productVariants
        });
    }

    handleChangeDefaultVariantOptionValue(e, productVariant: ProductVariant) {
        productVariant.isModified = true;
        productVariant.defaultVariantOptionValue = productVariant.variantOptions.filter(x => x.id === e.target.value)[0];
        this.setState({ isModified: true, productVariants: this.state.productVariants });
    }

    handleChangeDefaultDoubleValue(e, productVariant: ProductVariant) {
        productVariant.isModified = true;
        productVariant.defaultDoubleValue = e.target.value;
        this.setState({ isModified: true, productVariants: this.state.productVariants });
    }

    handleChangeDefaultIntegerValue(e, productVariant: ProductVariant) {
        productVariant.isModified = true;
        productVariant.defaultIntegerValue = e.target.value;
        this.setState({ isModified: true, productVariants: this.state.productVariants });
    }

    handleChangeDefaultStringValue(e, productVariant: ProductVariant) {
        productVariant.isModified = true;
        productVariant.defaultStringValue = e.target.value;
        this.setState({ isModified: true, productVariants: this.state.productVariants });
    }

    handleChangeDefaultBooleanValue(e, productVariant: ProductVariant) {
        productVariant.isModified = true;
        productVariant.defaultBooleanValue = e.target.checked;
        this.setState({ isModified: true, productVariants: this.state.productVariants });
    }

    submit() {

        this.setState({ isSubmitting: true });

        // simplify objects before turning them into json
        const changedProductVariants = this.state.productVariants.filter(x => x.isNew || x.isModified || x.isDeleted);
        const productVariants = sumoJS.deepClone(changedProductVariants);
        productVariants.forEach(x => x.product = new Product({ id: x.product.id }));
        productVariants.forEach(x => x.variant = new Variant({ id: x.variant.id, type: x.variant.type }));
        productVariants.forEach(x => x.defaultVariantOptionValue = new VariantOption({ id: x.defaultVariantOptionValue && x.defaultVariantOptionValue.id }));
        productVariants.forEach(x => x.variantOptions = x.variantOptions && x.variantOptions.map(variantOption => new VariantOption({ id: variantOption.id })));

        const model = {
            productId: this.state.productId,
            productVariants
        } as IEditProductVariantsDto;

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Product_AddOrupdateProductVariants, model, response => {

            if (response.isValid) {
                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditProductVariantsDto>(Constants.url_Product_EditProductVariantsJson,
            {
                productId: sumoJS.getQueryStringParameter("productId")
            },
            (editProductVariantsDto) => {
                this.setState({
                    authenticatedUser: new User(editProductVariantsDto.authenticatedUser),
                    productId: editProductVariantsDto.productId,
                    productLocalizedName: editProductVariantsDto.productLocalizedName,
                    productVariants: editProductVariantsDto.productVariants.map(x => new ProductVariant(x)),
                    allVariants: editProductVariantsDto.allVariants.map(x => new Variant(x)),
                    pageIsLoading: false
                }, () => Functions.setCreateAndEditPermissions(
                    new Product(),
                    this.state.authenticatedUser.role.createProduct,
                    this.state.authenticatedUser.role.editProduct));
            });
    }

    renderProductVariantsSection() {
        return (
            <div>
                {this.state.productVariants.filter(x => !x.isDeleted).map((productVariant, i) => {

                    var template: JSX.Element;

                    if (productVariant.variant.type === VariantTypesEnum.Options) {

                        // --------------------------------------------------------------
                        //  Template for Variant Type "Options"
                        // --------------------------------------------------------------

                        template =
                            <div>
                                <MultiSelectComponent {...{
                                    options: productVariant.variant.variantOptions.map(x => (new SmartOption(x.id, x.localizedTitle, `Name: ${x.name}`))),
                                    value: productVariant.variantOptions.map(variantOption => variantOption.id),
                                    label: "Available Variant Options",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectVariantOptions(newValue, productVariant)
                                } as IMultiSelectProps} />
                                <SelectComponent {...{
                                    key: productVariant.variant.id,
                                    options: productVariant.variantOptions.orderBy(x => x.localizedTitle).map(x => <option key={x.id} value={x.id}>{x.localizedTitle}</option>),
                                    value: productVariant.defaultVariantOptionValue ? productVariant.defaultVariantOptionValue.id : "",
                                    neutralOption: (!productVariant.defaultVariantOptionValue || !productVariant.variantOptions.length) && <option value="">Select default option</option>,
                                    label: "Default Variant Option",
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeDefaultVariantOptionValue(e, productVariant)
                                } as ISelectProps} />
                            </div>;
                    }
                    else if (productVariant.variant.type === VariantTypesEnum.Double) {

                        // --------------------------------------------------------------
                        //  Template for Variant Type "Double"
                        // --------------------------------------------------------------

                        template =
                            <TextboxComponent {...{
                                label: "Default value",
                                value: productVariant.defaultDoubleValue,
                                type: "number",
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeDefaultDoubleValue(e, productVariant)
                            } as ITextboxProps} />;
                    }
                    else if (productVariant.variant.type === VariantTypesEnum.Integer) {

                        // --------------------------------------------------------------
                        //  Template for Variant Type "Integer"
                        // --------------------------------------------------------------

                        template =
                            <TextboxComponent {...{
                                label: "Default value",
                                value: productVariant.defaultIntegerValue,
                                type: "number",
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeDefaultIntegerValue(e, productVariant)
                            } as ITextboxProps} />;
                    }
                    else if (productVariant.variant.type === VariantTypesEnum.String) {

                        // --------------------------------------------------------------
                        //  Template for Variant Type "String"
                        // --------------------------------------------------------------

                        template =
                            <TextboxComponent {...{
                                label: "Default value",
                                value: productVariant.defaultStringValue,
                                type: "text",
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeDefaultStringValue(e, productVariant)
                            } as ITextboxProps} />;
                    }
                    else if (productVariant.variant.type === VariantTypesEnum.Json) {

                        // --------------------------------------------------------------
                        //  Template for Variant Type "Json"
                        // --------------------------------------------------------------

                        template =
                            <TextareaComponent {...{
                                label: "Default value",
                                value: productVariant.defaultStringValue,
                                type: "text",
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeDefaultStringValue(e, productVariant)
                            } as ITextareaProps} />;
                    }
                    else if (productVariant.variant.type === VariantTypesEnum.Boolean) {

                        // --------------------------------------------------------------
                        //  Template for Variant Type "Boolean"
                        // --------------------------------------------------------------

                        template =
                            <CheckboxComponent {...{
                                value: productVariant.defaultBooleanValue,
                                text: "Default value",
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeDefaultBooleanValue(e, productVariant)
                            } as ICheckboxProps} />;
                    }
                    else return null;

                    return <div className="admin-subsection" key={productVariant.id}>
                        <div className="admin-subsection-title">
                            {productVariant.variant.localizedTitle}
                        </div>
                        <div className="admin-subsection-controls">
                            <button type="button" className="btn btn-link" onClick={() => this.handleRemoveProductVariant(productVariant)}>
                                <i className="fa fa-trash-o"></i>
                                Remove
                            </button>
                        </div>
                        {template}
                    </div>;
                }
                )}
            </div>
        );
    }

    renderVariantsSection() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Available Variants
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.allVariants.map(variant => {
                            var variantIsAdded = this.state.productVariants.some(x => x.variant.id === variant.id && !x.isDeleted);
                            return (
                                <tr key={variant.id}>
                                    <td>
                                        {variant.name || "-"}
                                    </td>
                                    <td>
                                        {VariantTypesEnum[variant.type]}
                                    </td>
                                    <td>
                                        {
                                            !variantIsAdded &&
                                            <button
                                                type="submit"
                                                className="btn btn-primary btn-xs"
                                                onClick={() => this.handleAddProductVariant(variant)}>
                                                <i className="fa fa-plus-circle"></i> Add
                                            </button>
                                        }
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

    render() {
        return (
            <div className="edit-product-variants-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Edit Product Variants for {this.state.productLocalizedName}
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Product_Index} className="btn btn-link">
                            Back to list
                        </a>
                        <div className="dropdown">
                            <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                Edit...
                                &nbsp;
                                <span className="caret"></span>
                            </button>
                            <ul className="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href={Constants.url_Product_Edit + this.state.productId}>
                                        Product
                                    </a>
                                </li>
                                <li>
                                    <a href={Constants.url_Product_EditProductAttributes + this.state.productId}>
                                        Product Attributes
                                    </a>
                                </li>
                                <li className="disabled">
                                    <a>
                                        Product Variants
                                    </a>
                                </li>
                                <li>
                                    <a href={Constants.url_Product_EditProductImageKits + this.state.productId}>
                                        Product Images
                                    </a>
                                </li>
                                <li>
                                    <a href={Constants.url_Product_EditProductStockUnits + this.state.productId}>
                                        Product Stock
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6">
                            {
                                this.renderProductVariantsSection()
                            }
                        </div>
                        <div className="col-md-6">
                            {
                                this.renderVariantsSection()
                            }
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}