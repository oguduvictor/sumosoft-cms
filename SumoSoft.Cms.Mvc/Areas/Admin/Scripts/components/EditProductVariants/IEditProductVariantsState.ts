﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { ProductVariant } from "../../../../../Scripts/classes/ProductVariant";
import { Variant } from "../../../../../Scripts/classes/Variant";

export interface IEditProductVariantsState extends IReactPageState {
    productId?: string;
    productLocalizedName?: string;
    pageIsLoading?: boolean;
    productVariants?: Array<ProductVariant>;
    allVariants?: Array<Variant>;
}