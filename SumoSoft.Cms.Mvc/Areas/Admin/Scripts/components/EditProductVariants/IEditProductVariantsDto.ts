﻿import { IProductVariantDto } from "../../../../../Scripts/interfaces/IProductVariantDto";
import { IVariantDto } from "../../../../../Scripts/interfaces/IVariantDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditProductVariantsDto {
    authenticatedUser?: IUserDto;
    productId?: string;
    productLocalizedName?: string;
    productVariants?: Array<IProductVariantDto>;
    allVariants?: Array<IVariantDto>;
}
