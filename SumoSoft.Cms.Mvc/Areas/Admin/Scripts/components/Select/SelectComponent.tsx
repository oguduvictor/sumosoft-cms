﻿import * as React from "react";

import { TopLabel } from "../TopLabel/TopLabel";
import { Functions } from "../../shared/Functions";

export interface ISelectProps {
    value?: string;
    label?: string;
    className?: string;
    options?: Array<JSX.Element>;
    neutralOption?: JSX.Element;
    handleChangeValue?(e): void;
}

export class SelectComponent extends React.Component<ISelectProps, undefined> {

    selectId = Functions.newGuid();

    render() {
        return (
            <div className={`select-component input-sub-parent ${this.props.className ? this.props.className : ""}`}>
                <TopLabel label={this.props.label} />
                <i className="fa fa-caret-down"></i>
                <select id={this.selectId} value={this.props.value} className={this.props.className} onChange={this.props.handleChangeValue}>
                    {this.props.neutralOption}
                    {this.props.options}
                </select>
            </div>
        );
    }
}