﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Product } from "../../../../../Scripts/classes/Product";
import { Category } from "../../../../../Scripts/classes/Category";

export interface IProductListState extends IReactPageState {
    products?: Array<Product>;
    allCategories: Array<Category>;
    categoryFilter: string;
    keywordFilter?: string;
    isImportingProducts?: boolean;
    productsFile?: any;
    sampleProductRows: Array<Array<string>>;
    page?: number;
    totalPages?: number;
}