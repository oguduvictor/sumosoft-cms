﻿import { IProductDto } from "../../../../../Scripts/interfaces/IProductDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { ICategoryDto } from "../../../../../Scripts/interfaces/ICategoryDto";
import { IPaginatedListDto } from "../../interfaces/IPaginatedListDto";

export interface IProductListDto extends IPaginatedListDto {
    authenticatedUser?: IUserDto;
    allCategories: Array<ICategoryDto>;
    products?: Array<IProductDto>;
    localizedCountries?: Array<string>;
}