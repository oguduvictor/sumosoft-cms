﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { Product } from "../../../../../Scripts/classes/Product";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Constants } from "../../shared/Constants";
import { IProductListState } from "./IProductListState";
import { IProductListDto } from "./IProductListDto";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { Category } from "../../../../../Scripts/classes/Category";
import { Functions } from "../../shared/Functions";
import { IFormResponse } from '../../../../../Scripts/interfaces/IFormResponse';

export default class ProductListComponent extends ReactPageComponent<{}, IProductListState> {

    constructor(props) {
        super(props);

        this.state = {
            products: new Array<Product>(),
            allCategories: new Array<Category>(),
            categoryFilter: "",
            keywordFilter: "",
            authenticatedUser: new User({ role: new UserRole() }),
            sampleProductRows: [[""], [""]],
            page: 1,
            totalPages: 1
        } as IProductListState;
    }

    componentDidMount() {
        this.setState({
            pageIsLoading: true
        });

        this.setStateFromApi();
    }

    setStateFromApi(page = 1) {
        sumoJS.ajaxPost<IProductListDto>(
            Constants.url_Product_IndexJson,
            {
                categoryFilter: this.state.categoryFilter,
                keywordFilter: this.state.keywordFilter,
                page: page
            }, (productListDto) => {

                const sampleProductRowHeader = new Array<string>("Name", "Url", "Code", "Tax Code");
                const sampleProductFields = new Array<string>("myProduct", "my-product", "prod-000", "TX000");

                productListDto.localizedCountries.forEach(country =>
                    ["Localized Title", "Localized Description", "Localized Price"].forEach(x => {
                        sampleProductRowHeader.push(`${x} ${country}`);
                        sampleProductFields.push(x.indexOf("Price") === -1 ? `${x} ${country}` : this.randomNumber().toString());
                    })
                );

                this.setState({
                    authenticatedUser: new User(productListDto.authenticatedUser),
                    allCategories: productListDto.allCategories.map(x => new Category(x)),
                    products: productListDto.products.map(x => new Product(x)),
                    sampleProductRows: [sampleProductRowHeader, sampleProductFields],
                    page,
                    totalPages: productListDto.totalPages,
                    pageIsLoading: false
                });
            });
    }

    handleChangeCategoryFilter(e) {
        const categoryFilter = e.target.value;

        this.setState({
            categoryFilter
        }, () => this.setStateFromApi(1));
    }

    handleChangeKeywordFilter(e) {

        const value = e.target.value;
        this.setState({ keywordFilter: e.target.value }, () => {
            if (!value) {
                this.setStateFromApi(1);
            }
        });
    }

    handleKeyPress(e) {
        if (e.key === "Enter") {
            this.setStateFromApi(1);
        }
    }

    handleUpdateProductFile(e) {
        this.setState({
            productsFile: e.target.files[0]
        });
    }

    handleUploadProductsFile() {

        const formData = new FormData();

        formData.append("products file", this.state.productsFile);

        this.setState({
            isImportingProducts: true
        });

        Functions.ajaxUploadFile(formData, Constants.url_Product_ImportProducts, response => {

            $("#import-products").modal("hide");
            $("#file-upload").val("");

            this.setStateFromApi();

            if (response.isValid) {
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    isImportingProducts: false,
                    productsFile: null
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isImportingProducts: false,
                    productsFile: null
                });
            }
        });
    }

    handleDuplicate(e, productId: string) {
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Product_Duplicate, { id: productId }, response => {
            if (response.isValid) {
                window.location.href = response.redirectUrl;
            } else {
                this.setState({
                    isSubmitting: false
                });
            }
        });
    }

    randomNumber(min = 200, max = 1000) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">
                    Products
                </div>
                <div className="admin-top-bar-controls">
                    <div id="keep-this-wrapper">
                        <div className="input-group">
                            <input type="text" className="form-control" placeholder="Search..."
                                value={this.state.keywordFilter || ""}
                                onChange={(e) => this.handleChangeKeywordFilter(e)}
                                onKeyPress={(e) => this.handleKeyPress(e)} />
                            <span className="input-group-btn">
                                <button className="btn btn-default" type="button" onClick={() => this.setStateFromApi(1)}>
                                    <i className="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div className="btn-group" role="group">
                        <a className={`btn btn-default ${(this.state.page <= 1 || this.state.totalPages === 1) ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page - 1))}>
                            <i className="fa fa-angle-left" aria-hidden="true"></i>
                        </a>
                        <span className="btn btn-default">
                            {`Page ${this.state.page} / ${this.state.totalPages}`}
                        </span>
                        <a className={`btn btn-default ${this.state.page === this.state.totalPages ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page + 1))}>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                    <SelectComponent {...{
                        neutralOption: <option value="">All categories</option>,
                        options: this.state.allCategories.map(
                            x => <option key={x.id} value={x.name}>{x.localizedTitle}</option>),
                        value: this.state.categoryFilter,
                        className: "no-bottom-margin category-filter",
                        handleChangeValue: (e) => this.handleChangeCategoryFilter(e)
                    } as ISelectProps} />
                    {
                        //<div className="dropdown">
                        //    <a className="dropdown-toggle btn btn-default" data-toggle="dropdown">
                        //        Batch
                        //    <span className="caret"></span>
                        //    </a>
                        //    <ul className="dropdown-menu">
                        //        <li>
                        //            <a className={`btn btn-primary ${this.state.authenticatedUser.role.createProduct
                        //                ? ""
                        //                : "disabled"}`} href="#" data-toggle="modal" data-target="#import-products">
                        //                Import from CSV
                        //        </a>
                        //        </li>
                        //    </ul>
                        //</div>
                    }
                    <a className={`btn btn-primary ${this.state.authenticatedUser.role.createProduct
                        ? ""
                        : "disabled"}`} href={Constants.url_Product_Edit}>
                        <i className="fa fa-plus"> </i>
                        Create new product
                    </a>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="product-list-component">
                {
                    this.renderTopBar()
                }
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Localized Title</th>
                                    <th>Url</th>
                                    <th>Code</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.products.length < 1 &&
                                    <tr className="empty-list-message">
                                        <td colSpan={10}>
                                            No items
					                    </td>
                                    </tr>
                                }
                                {
                                    this.state.products.map((product, i) =>
                                        <tr key={product.id}>
                                            <td>
                                                {
                                                    i + 1
                                                }
                                            </td>
                                            <td>
                                                {
                                                    product.localizedTitle
                                                }
                                            </td>
                                            <td>
                                                {
                                                    product.url
                                                }
                                            </td>
                                            <td>
                                                {
                                                    product.code
                                                }
                                            </td>
                                            <td>
                                                {
                                                    product.isDisabled &&
                                                    <label className="label label-warning">Disabled</label>
                                                }
                                                {
                                                    !product.isDisabled &&
                                                    <label className="label label-success">Enabled</label>
                                                }
                                            </td>
                                            <td>
                                                <div className="dropdown">
                                                    <button className="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                                        Options
                                                        <span className="caret"></span>
                                                    </button>
                                                    <ul className="dropdown-menu">
                                                        <li>
                                                            <a href={Constants.url_Product_Edit + product.id}>
                                                                Edit
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href={Constants.url_Product_EditProductAttributes + product.id}>
                                                                Edit Attributes
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href={Constants.url_Product_EditProductVariants + product.id}>
                                                                Edit Variants
                                                        </a>
                                                        </li>
                                                        <li>
                                                            <a href={Constants.url_Product_EditProductStockUnits + product.id}>
                                                                Edit Stock
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href={Constants.url_Product_EditProductImageKits + product.id}>
                                                                Edit Images
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick={e => this.handleDuplicate(e, product.id)}>
                                                                Duplicate
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                }
                <ModalComponent {...{
                    id: "import-products",
                    title: "Import Products",
                    body:
                    <span>
                        <div className="file-input">
                            <div className="textbox-component form-group">
                                <label>
                                    Select CSV file
                                </label>
                                <input type="file" ref="file" id="file-upload" name="file" accept=".csv" onChange={(e) => this.handleUpdateProductFile(e)} />
                            </div>
                        </div>
                        <div className="file-format-guide">
                            <div className="title">
                                CSV File Format Guide
                            </div>
                            <TableComponent {...{
                                thList: this.state.sampleProductRows[0],
                                trList: [this.state.sampleProductRows[1]]
                            } as ITableProps} />
                            <div className="note">Note: The first row should be the header as shown in this guide</div>
                        </div>
                    </span>,
                    footer:
                    <span>
                        <ButtonSubmitComponent {...{
                            text: "Import",
                            isSubmitting: this.state.isImportingProducts,
                            handleSubmit: () => this.handleUploadProductsFile()
                        } as IButtonSubmitProps} />
                        <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                    </span>
                } as IModalProps} />
            </div>
        );
    }
}