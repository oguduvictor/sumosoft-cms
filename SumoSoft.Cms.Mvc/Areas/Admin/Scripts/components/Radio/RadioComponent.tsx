﻿import * as React from "react";

export interface IRadioProps {
    value: boolean;
    label?: string;
    addon?: string;
    className?: string;
    handleChangeValue(e): void;
}

export var RadioComponent: (props: IRadioProps) => JSX.Element = props => {
    return props.addon ?
        (
            <div className={`radio-component input-sub-parent ${props.className}`}>
                <span className="pre-input">{props.label}</span>
                <label>
                    <input type="radio" checked={props.value} onChange={(e) => props.handleChangeValue(e)} /><span className="post-input">{props.addon}</span>
                </label>
            </div>
        ) :
        (
            <div className={`radio-component input-sub-parent ${props.className ? props.className : ""}`}>
                <label>
                    <input type="radio" checked={props.value} onChange={(e) => props.handleChangeValue(e)} /><span className="post-input">{props.label}</span>
                </label>
            </div>
        );
}