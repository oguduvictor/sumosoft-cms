﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Cart } from "../../../../../Scripts/classes/Cart";
import { Country } from "../../../../../Scripts/classes/Country";
import { Email } from '../../../../../Scripts/classes/Email';
import { SendEmailConfirmationModalDto } from '../EditOrder/SendEmailConfirmationModalDto';

export interface IEditCartState extends IReactPageState {
    email?: Email;
    cart?: Cart;
    allEmails?: Array<Email>;
    shippingCountryOrDefault?: Country;
    isCreatingOrder?: boolean;
    sendEmailConfirmationModalDetails?: SendEmailConfirmationModalDto;
}