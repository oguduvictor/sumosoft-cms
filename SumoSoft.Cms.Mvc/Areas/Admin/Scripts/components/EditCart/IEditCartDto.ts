﻿import { ICartDto } from "../../../../../Scripts/interfaces/ICartDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { Email } from '../../../../../Scripts/classes/Email';

export interface IEditCartDto {
    authenticatedUser: IUserDto;
    cart: ICartDto;
    allEmails?: Array<Email>;
    defaultReplyToEmail: string;
}