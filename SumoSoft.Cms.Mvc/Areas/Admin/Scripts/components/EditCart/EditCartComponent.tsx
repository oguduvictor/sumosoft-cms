﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { Constants } from "../../shared/Constants";
import { Cart } from "../../../../../Scripts/classes/Cart";
import { CartItemVariant } from "../../../../../Scripts/classes/CartItemVariant";
import { IEditCartState } from "./IEditCartState";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { VariantTypesEnum } from "../../../../../Scripts/enums/VariantTypesEnum";
import { IEditCartDto } from "./IEditCartDto";
import { Country } from "../../../../../Scripts/classes/Country";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { Email } from '../../../../../Scripts/classes/Email';
import { SendEmailConfirmationModalDto } from '../EditOrder/SendEmailConfirmationModalDto';

export default class EditCartComponent extends ReactPageComponent<{}, IEditCartState> {

    constructor(props) {
        super(props);

        this.state = {
            cart: new Cart(),
            shippingCountryOrDefault: new Country(),
            pageIsLoading: true,
            email: new Email(),
            sendEmailConfirmationModalDetails: new SendEmailConfirmationModalDto()
        } as IEditCartState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    getCartItemVariantPrice(cartItemVariant: CartItemVariant) {
        if (cartItemVariant.priceModifier < 0) {
            return `-${this.state.cart.user.country.currencySymbol + Math.abs(cartItemVariant.priceModifier)}`;
        } else {
            return this.state.cart.user.country.currencySymbol + cartItemVariant.priceModifier;
        }
    }

    getCartItemVariantValue(cartItemVariant: CartItemVariant) {
        switch (cartItemVariant.variant.type) {
            case VariantTypesEnum.Options:
                return (cartItemVariant.variantOptionValue ? cartItemVariant.variantOptionValue.name : ``);
            case VariantTypesEnum.Boolean:
                return cartItemVariant.booleanValue;
            case VariantTypesEnum.Double:
                return cartItemVariant.doubleValue;
            case VariantTypesEnum.Integer:
                return cartItemVariant.integerValue;
            case VariantTypesEnum.String:
                return cartItemVariant.stringValue;
            default:
                return "";
        }
    }

    handleConfirmCreateOrder() {
        $("#confirm-create-order").modal("show");
    }

    handleOpenSendEmailConfirmationModal(e, email: Email) {
        const { sendEmailConfirmationModalDetails } = this.state;

        sendEmailConfirmationModalDetails.entityId = this.state.cart.id;
        sendEmailConfirmationModalDetails.emailName = email.name;
        sendEmailConfirmationModalDetails.sendTo = this.state.cart.user.email;

        this.setState({
            sendEmailConfirmationModalDetails
        });
    }

    handleChangeSendEmailConfirmationModalDetailsSendTo(e) {
        this.state.sendEmailConfirmationModalDetails.sendTo = e.target.value;
        this.setState({
            sendEmailConfirmationModalDetails: this.state.sendEmailConfirmationModalDetails
        });
    }

    sendConfirmationEmail() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Cart_SendEmail, this.state.sendEmailConfirmationModalDetails, response => {
            if (response.isValid) {
                window.location.href = response.redirectUrl;
            } else {
                this.setState({
                    isSubmitting: false
                });
                $(".modal-errors").html(response.errors.map(x => x.message).toString());
            }
        });
    }

    setStateFromApi() {
        this.setState({ pageIsLoading: true });

        var cartId = sumoJS.getQueryStringParameter("cartId");
        if (!cartId) {
            window.location.href = Constants.url_Cart_Index
        }

        sumoJS.ajaxPost<IEditCartDto>(Constants.url_Cart_EditJson,
            {
                cartId
            },
            (editCartDto) => {
                const shippingCountryOrDefault = editCartDto.cart &&
                    ((editCartDto.cart.shippingAddress && editCartDto.cart.shippingAddress.country) ||
                        (editCartDto.cart.user && editCartDto.cart.user.country));

                this.setState({
                    cart: new Cart(editCartDto.cart),
                    allEmails: editCartDto.allEmails.map(x => new Email(x)),
                    shippingCountryOrDefault: new Country(shippingCountryOrDefault),
                    pageIsLoading: false
                });
            }
        );
    }

    submitCreateOrder() {
        this.setState({ isCreatingOrder: true });

        const countryUrl = this.state.cart.user && this.state.cart.user.country && this.state.cart.user.country.url;

        sumoJS.ajaxPost<IFormResponse>(sumoJS.getLocalizedUrl(Constants.url_Cart_CreateOrder, countryUrl),
            {
                cartId: this.state.cart.id
            },
            response => {
                if (response.isValid) {
                    window.location.href = Constants.url_Cart_Index;
                } else {
                    this.setState({
                        isCreatingOrder: false,
                        formErrors: response.errors
                    });
                }
            });
    }

    // ---------------------------------------------------------------------
    // Render methods
    // ---------------------------------------------------------------------

    renderSendEmails() {
        return (
            <TableComponent {...{
                thList: ["Email", "Status", ""],
                trList: this.state.allEmails
                    .map((email) => [
                        email.name,
                        (this.state.cart.sentEmails.filter(x => x.email.id === email.id).length > 0) &&
                        <label className="label label-success">
                            <i className="fa fa-check-circle"></i>
                            &nbsp;Sent
                        </label>,
                        <button data-toggle="modal" data-target="#confirm-send-email-modal" className="btn btn-primary btn-xs" onClick={(e) => this.handleOpenSendEmailConfirmationModal(e, email)}>
                            send
                        </button>])
            } as ITableProps} />
        );
    }

    render() {
        return (
            <div className="edit-cart-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {!this.state.pageIsLoading &&
                            (this.state.cart.user.firstName && this.state.cart.user.lastName
                                ? `${this.state.cart.user.fullName}'s Cart`
                                : `User does not have a name`)
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Cart_Index} className="btn btn-link">
                            Back to list
                        </a>
                        <ButtonSubmitComponent {...{
                            text: "Create Order",
                            isSubmitting: null,
                            handleSubmit: () => this.handleConfirmCreateOrder()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {this.state.pageIsLoading && <SpinnerComponent />}
                {!this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Summary
                                </div>
                                <TextboxComponent {...{
                                    label: "Date",
                                    value: this.state.cart.createdDate ? dayjs(this.state.cart.createdDate).format("DD/MM/YYYY") : "",
                                    className: "form-group",
                                    type: "text",
                                    readOnly: true
                                } as ITextboxProps} />
                                <TableComponent {...{
                                    trList: [
                                        [
                                            "Items total:",
                                            this.state.cart.user.country.currencySymbol + this.state.cart.cartShippingBoxes.sum(x => x.totalAfterTax).toFixed(2)
                                        ],
                                        [
                                            "Coupon:",
                                            (this.state.cart.coupon ? (this.state.cart.user.country.currencySymbol + this.state.cart.couponValue) : "-")
                                        ],
                                        [
                                            "Credit:",
                                            (this.state.cart.userCredit ? (this.state.cart.user.country.currencySymbol + this.state.cart.userCredit.amount) : "-")
                                        ],
                                        [
                                            "Total:",
                                            this.state.cart.user.country.currencySymbol + this.state.cart.totalToBePaid.toFixed(2)
                                        ]
                                    ],
                                    className: "items-summary-table"
                                } as ITableProps} />
                            </div>
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    User details
                                </div>
                                <TextboxComponent {...{
                                    label: "First Name",
                                    value: this.state.cart.user.firstName,
                                    className: "form-group",
                                    type: "text",
                                    readOnly: true
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Last Name",
                                    value: this.state.cart.user.lastName,
                                    className: "form-group",
                                    type: "text",
                                    readOnly: true
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Email",
                                    value: this.state.cart.user.email,
                                    className: "form-group",
                                    type: "email",
                                    readOnly: true
                                } as ITextboxProps} />
                            
                                {this.renderSendEmails()}
                        </div>
                        </div>
                        <div className="col-md-6">
                            {this.state.cart.cartShippingBoxes.map(cartShippingBox =>
                                <div className="admin-subsection" key={cartShippingBox.id}>
                                    <div className="admin-subsection-title">
                                        Shipping Box
                                    </div>
                                    <TextboxComponent {...{
                                        label: "Name",
                                        value: cartShippingBox.shippingBox.name,
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Shipping Price",
                                        value: this.state.cart.user.country.currencySymbol + cartShippingBox.shippingPriceAfterTax.toFixed(2),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <div className="panel-group" id="accordion">
                                        {cartShippingBox.cartItems.map(cartItem =>
                                            <div className="panel panel-default" key={cartItem.id}>
                                                <div className="panel-heading">
                                                    <h4 className="panel-title">
                                                        <a role="button" data-toggle="collapse" className="btn-block" data-parent="#accordion" href={`#${cartItem.id}`}>
                                                            {cartItem.product.localizedTitle}
                                                            <span className="pull-right">
                                                                {this.state.cart.user.country.currencySymbol + cartItem.totalAfterTax.toFixed(2)}
                                                            </span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id={cartItem.id} className="panel-collapse collapse">
                                                    <div className="panel-body">
                                                        <TableComponent {...{
                                                            thList: ["Variant", "Value", "Price"],
                                                            trList: cartItem.cartItemVariants
                                                                .orderBy(x => x.variant.name)
                                                                .map((itemProductVariant) => [
                                                                    itemProductVariant.variant.localizedTitle,
                                                                    this.getCartItemVariantValue(itemProductVariant),
                                                                    this.getCartItemVariantPrice(itemProductVariant)
                                                                ])
                                                        } as ITableProps} />
                                                    </div>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            )}
                            {this.state.cart.shippingAddress &&
                                <div className="admin-subsection">
                                    <div className="admin-subsection-title">
                                        Shipping details
                                    </div>
                                    <TextboxComponent {...{
                                        label: "First Name",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.firstName),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Last Name",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.lastName),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Address line 1",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.addressLine1),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Address line 2",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.addressLine2),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "City",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.city),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Post Code",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.postcode),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Country",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.country && this.state.cart.shippingAddress.country.name),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                    <TextboxComponent {...{
                                        label: "Phone Number",
                                        value: (this.state.cart.shippingAddress && this.state.cart.shippingAddress.phoneNumber),
                                        className: "form-group",
                                        type: "text",
                                        readOnly: true
                                    } as ITextboxProps} />
                                </div>
                            }
                        </div>
                    </div>
                }

                <ModalComponent {...{
                    id: "confirm-send-email-modal",
                    title: "Send Email",
                    body:
                        <span>
                            <p className="modal-errors"></p>
                            <TextboxComponent {...{
                                label: "Recipient",
                                value: this.state.sendEmailConfirmationModalDetails.sendTo,
                                className: "form-group",
                                type: "text",
                                handleChangeValue: (e) => this.handleChangeSendEmailConfirmationModalDetailsSendTo(e)
                            } as ITextboxProps} />
                        </span>,
                    footer:
                        <span>
                            <button className={`btn btn-primary ${this.state.isSubmitting ? "disabled" : ""}`} onClick={() => this.sendConfirmationEmail()}>
                                {this.state.isSubmitting &&
                                    <i className="fa fa-spinner fa-pulse fa-fw"></i>
                                } &nbsp;Confirm
                            </button>
                            <button type="button" className="btn btn-default" data-dismiss="modal">
                                Back to list
                            </button>
                        </span>
                } as IModalProps} />

                <ModalComponent {...{
                    id: `confirm-create-order`,
                    title: "Confirmation",
                    body:
                        <span>
                            Are you sure you want to proceed?
                    </span>,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                text: "Confirm",
                                isSubmitting: this.state.isCreatingOrder,
                                handleSubmit: () => this.submitCreateOrder()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">
                                Cancel
                        </button>
                        </span>
                } as IModalProps} />
            </div>
        );
    }
}