﻿import * as React from "react";
import * as dayjs from "dayjs";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { FilePickerComponent, IFilePickerProps } from "../FilePicker/FilePickerComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { MultiSelectComponent, IMultiSelectProps, SmartOption } from "../MultiSelect/MultiSelectComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { BlogPost } from "../../../../../Scripts/classes/BlogPost";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { BlogCategory } from "../../../../../Scripts/classes/BlogCategory";
import { IEditBlogPostState } from "./IEditBlogPostState";
import { IEditBlogPostDto } from "./IEditBlogPostDto";
import {IFormResponse} from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditBlogPostComponent extends ReactPageComponent<{}, IEditBlogPostState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: false,
            formErrors: new Array<IFormError>(),
            blogPost: new BlogPost(),
            users: new Array<User>(),
            allBlogCategories: new Array<BlogCategory>(),
            postPreviewLink: "",
            authenticatedUser: new User({ role: new UserRole() })
        } as IEditBlogPostState;
    }

    setupCKEDITOR() {
        setTimeout(() => {
            var createEditPost = this.state.authenticatedUser.role.createBlogPost ||
                this.state.authenticatedUser.role.editBlogPost;
            $(".editPost").ckeditor({ readOnly: !createEditPost});
            
        }, 100);

        var ckeditorInstance = CKEDITOR.instances["editor"];

        ckeditorInstance && ckeditorInstance.on("blur", () => {
            var data = ckeditorInstance.getData();
            this.handleChangePostContent(data);
        });
    }

    componentDidMount() {
        this.setState({ pageIsLoading: true });
        this.setStateFromApi();
        this.setupCKEDITOR();
    }

    componentDidUpdate(){
        this.setupCKEDITOR();
    }

    handleChangePostTitle(e) {
        this.state.blogPost.title = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    };

    handleBlurPostTitle(e) {
        if (e.target.value !== "" && (!this.state.blogPost.url || this.state.blogPost.url === "")) {
            this.state.blogPost.url = Functions.convertToUrl(this.state.blogPost.title);
            this.setState({ isModified: true, blogPost: this.state.blogPost });
        }
    };

    handleChangePostUrl(e) {
        this.state.blogPost.url = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    };

    handleChangeFeaturedImageUrl(absoluteUrl) {
        this.state.blogPost.featuredImage = absoluteUrl;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    };

    handleChangePostContent(content) {
        this.state.blogPost.content = content;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleFeaturedImageHorizontalCrop(e) {
        this.state.blogPost.featuredImageHorizontalCrop = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleFeaturedImageVerticalCrop(e) {
        this.state.blogPost.featuredImageVerticalCrop = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleChangePostIsPublished(e) {
        this.state.blogPost.isPublished = e.target.checked;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleChangePostPublicationDate(e) {
        this.state.blogPost.publicationDate = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleChangePostExcerpt(e) {
        this.state.blogPost.excerpt = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleChangePostMetaTitle(e) {
        this.state.blogPost.metaTitle = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleChangePostMetaDescription(e) {
        this.state.blogPost.metaDescription = e.target.value;
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleChangePostAuthor(e) {
        this.state.blogPost.author = this.state.users.filter(x => x.id === e.target.value)[0];
        this.setState({ isModified: true, blogPost: this.state.blogPost });
    }

    handleSelectBlogCategories(newValue: Array<string>) {
        this.state.blogPost.blogCategories = newValue.map(categoryId => this.state.allBlogCategories.filter(x => x.id === categoryId)[0]);

        this.setState({
            isModified: true,
            blogPost: this.state.blogPost
        });
    }

    handlePostDelete() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Posts_Delete, { postId: this.state.blogPost.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Posts_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    submit() {
        const blogPost = this.state.blogPost;
        blogPost.content = $(".editPost").val().toString();
        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Posts_AddOrUpdate, blogPost, response => {

            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditBlogPostDto>(Constants.url_Posts_EditJson,
            {
                postId: sumoJS.getQueryStringParameter("postId")
            },
            (editBlogPostDto) => {
                this.setState({
                    authenticatedUser: new User(editBlogPostDto.authenticatedUser),
                    pageIsLoading: false,
                    blogPost: new BlogPost(editBlogPostDto.blogPost),
                    users: editBlogPostDto.users.map(x => new User(x)),
                    allBlogCategories: editBlogPostDto.allBlogCategories.map(x => new BlogCategory(x)),
                    postPreviewLink: editBlogPostDto.postPreviewLink
                }, () => Functions.setCreateAndEditPermissions(
                    this.state.blogPost,
                    this.state.authenticatedUser.role.createBlogPost,
                    this.state.authenticatedUser.role.editBlogPost));
            });
    }

    render() {
        return (
            <div className="edit-blog-post-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.blogPost.isNew ? "Create Post" : "Edit Post"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Posts_Index} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            !this.state.blogPost.isNew &&
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                canDelete: this.state.authenticatedUser.role.deleteBlogPost,
                                handleDelete: () => this.handlePostDelete()
                            } as IButtonDeleteProps} />
                        }
                        {
                            !this.state.blogPost.isNew &&
                            <a href={this.state.postPreviewLink} className="btn btn-primary" target="_blank">
                                Show preview
                            </a>
                        }
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div >
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-8">
                            <div className="admin-subsection">
                                <TextboxComponent {...{
                                    label: "Post Title",
                                    value: this.state.blogPost.title,
                                    className: "form-group",
                                    type: "text",
                                    handleChangeValue: (e) => this.handleChangePostTitle(e),
                                    handleBlur: (e) => this.handleBlurPostTitle(e)
                                } as ITextboxProps}
                                />
                                <TextboxComponent {...{
                                    label: "Custom Url",
                                    value: this.state.blogPost.url,
                                    className: "form-group",
                                    type: "text",
                                    handleChangeValue: (e) => this.handleChangePostUrl(e)
                                } as ITextboxProps}
                                />
                                <div className="form-group" >
                                    <FilePickerComponent {...{
                                        label: 'Featured Image',
                                        value: this.state.blogPost.featuredImage,
                                        className: "form-group",
                                        handleChangeValue: (absoluteUrl) => this.handleChangeFeaturedImageUrl(absoluteUrl)
                                    } as IFilePickerProps}
                                    />
                                </div>
                                <div className="row" >
                                    <div className="col-sm-6" >
                                        <SelectComponent {...{
                                            options: [
                                                <option key={"0"} value={"0%"}> Top </option>,
                                                <option key={"25"} value={"25%"}> Middle - top </option>,
                                                <option key={"50"} value={"50%"}> Middle </option>,
                                                <option key={"75"} value={"75%"}> Middle - bottom </option>,
                                                <option key={"100"} value={"100%"}> Bottom </option>
                                            ],
                                            value: this.state.blogPost.featuredImageVerticalCrop || "50%",
                                            label: "Vertical Crop",
                                            className: "form-group",
                                            handleChangeValue: (e) => this.handleFeaturedImageVerticalCrop(e)
                                        } as ISelectProps}
                                        />
                                    </div>
                                    <div className="col-sm-6" >
                                        <SelectComponent {...{
                                            options: [
                                                <option key={"0"} value={"0%"}> Left </option>,
                                                <option key={"25"} value={"25%"}> Middle - left </option>,
                                                <option key={"50"} value={"50%"}> Middle </option>,
                                                <option key={"75"} value={"75%"}> Middle - right </option>,
                                                <option key={"100"} value={"100%"}> Right </option>
                                            ],
                                            value: this.state.blogPost.featuredImageHorizontalCrop || "50%",
                                            label: "Horizontal Crop",
                                            className: "form-group",
                                            handleChangeValue: (e) => this.handleFeaturedImageHorizontalCrop(e)
                                        } as ISelectProps}
                                        />
                                    </div>
                                </div>
                                <textarea defaultValue={this.state.blogPost.content || ""} name="editor" className="editor editPost" />
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="admin-subsection">
                                <CheckboxComponent{...{
                                    text: "Published",
                                    value: this.state.blogPost.isPublished || false,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangePostIsPublished(e)
                                } as ICheckboxProps}
                                />
                                <TextboxComponent {...{
                                    label: "Date",
                                    type: "date",
                                    value: dayjs(this.state.blogPost.publicationDate).format("YYYY-MM-DD"),
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangePostPublicationDate(e)
                                } as ITextboxProps}
                                />
                                <SelectComponent {...{
                                    options: this.state.users.map(x => <option key={x.id} value={x.id} > {x.firstName + " " + x.lastName} </option>),
                                    value: this.state.blogPost.author ? this.state.blogPost.author.id : "",
                                    label: "Author",
                                    neutralOption: <option value="">Select Author</option>,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangePostAuthor(e)
                                } as ISelectProps}
                                />
                                <TextareaComponent {...{
                                    label: "Excerpt",
                                    type: "text",
                                    value: this.state.blogPost.excerpt,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangePostExcerpt(e)
                                } as ITextareaProps} />
                                <TextareaComponent {...{
                                    label: "SEO Title",
                                    type: "text",
                                    value: this.state.blogPost.metaTitle,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangePostMetaTitle(e)
                                } as ITextareaProps} />
                                <TextareaComponent {...{
                                    label: "SEO Description",
                                    type: "text",
                                    value: this.state.blogPost.metaDescription,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangePostMetaDescription(e)
                                } as ITextareaProps} />
                            </div>
                            <div className="admin-subsection" >
                                <div className="admin-subsection-title" >
                                    Categories
                            </div>
                                <MultiSelectComponent {...{
                                    options: this.state.allBlogCategories.map(x => new SmartOption(x.id, x.localizedTitle)),
                                    value: this.state.blogPost.blogCategories.map(blogCategory => blogCategory.id),
                                    label: "Available Categories",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectBlogCategories(newValue)
                                } as IMultiSelectProps} />
                            </div>
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}
