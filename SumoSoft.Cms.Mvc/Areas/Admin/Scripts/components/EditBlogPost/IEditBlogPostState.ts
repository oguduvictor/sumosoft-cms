﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { BlogPost } from "../../../../../Scripts/classes/BlogPost";
import { User } from "../../../../../Scripts/classes/User";
import { BlogCategory } from "../../../../../Scripts/classes/BlogCategory";

export interface IEditBlogPostState extends IReactPageState {
    blogPost?: BlogPost;
    users?: Array<User>;
    allBlogCategories?: Array<BlogCategory>;
    postPreviewLink?: string;
}