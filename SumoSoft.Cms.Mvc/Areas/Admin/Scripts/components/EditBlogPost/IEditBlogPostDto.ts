﻿import { IBlogPostDto } from "../../../../../Scripts/interfaces/IBlogPostDto";
import { BlogCategory } from "../../../../../Scripts/classes/BlogCategory";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditBlogPostDto {
    authenticatedUser: IUserDto;
    blogPost: IBlogPostDto;
    users: Array<IUserDto>;
    allBlogCategories: Array<BlogCategory>;
    postPreviewLink: string;
}