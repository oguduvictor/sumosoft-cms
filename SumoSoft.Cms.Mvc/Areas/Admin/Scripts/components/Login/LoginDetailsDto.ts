﻿import {ILoginDetailsDto} from "./ILoginDetailsDto";

export class LoginDetailsDto implements ILoginDetailsDto {
    email: string;
    password: string;
}