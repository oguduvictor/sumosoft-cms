﻿export interface ILoginDetailsDto {
    email: string;
    password: string;
}