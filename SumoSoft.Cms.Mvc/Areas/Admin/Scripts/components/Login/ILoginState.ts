﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";

export interface ILoginState extends IReactPageState {
    formResponse: IFormResponse;
    isSubmitting: boolean;
}
