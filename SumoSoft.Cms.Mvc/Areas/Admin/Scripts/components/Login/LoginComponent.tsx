﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { Constants } from "../../shared/Constants";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { ILoginState } from "./ILoginState";
import { LoginForm, ILoginDetails } from "../../../../../Scripts/sumoReact/LoginForm/LoginForm";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";

export default class LoginComponent extends ReactPageComponent<{}, ILoginState> {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitting: false,
            formResponse: {} as IFormResponse
        } as ILoginState;
    }


    getReturnUrl = () => {
        const registerUrl = "/admin/account/register";
        const returnUrl = sumoJS.getQueryStringParameter("returnUrl");

        if (returnUrl && returnUrl.length > 0) {
            return `${registerUrl}?returnUrl=${returnUrl}`;
        }

        return registerUrl;
    };

    handleSubmit = (loginDetails: ILoginDetails) => {
        this.setState({ isSubmitting: true, formResponse: {} as IFormResponse });
        const returnUrl = sumoJS.getQueryStringParameter("returnUrl");

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Account_LoginForm, { loginDetails, returnUrl }, formResponse => {
            if (formResponse.isValid) {
                window.location.href = formResponse.redirectUrl;
            } else {
                this.setState({
                    formResponse,
                    isSubmitting: false
                });
            }
        });
    };

    render() {
        const { formResponse, isSubmitting } = this.state;
        return (
            <div className="login-component">
                <div className="form-container">
                    <h1>Log in</h1>
                    <LoginForm
                        loginButtonClassName="form__button"
                        enableFacebookLogin={false}
                        enableGoogleLogin={false}
                        formResponse={formResponse}
                        isSubmitting={isSubmitting}
                        showForgotPassword={false}
                        secondaryActionText="Don't have an account yet?"
                        registerUrlTitle="Click here"
                        onSubmit={this.handleSubmit}
                        registerUrl={this.getReturnUrl()}
                    />
                </div>
            </div>
        );
    }
}
