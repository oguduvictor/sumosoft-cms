﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";
import { Product } from "../../../../../Scripts/classes/Product";
import { ShippingBoxLocalizedKit } from "../../../../../Scripts/classes/ShippingBoxLocalizedKit";
import { Constants } from "../../shared/Constants";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { IEditShippingBoxState } from "./IEditShippingBoxState";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IEditShipppingBoxDto } from "./IEditShipppingBoxDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Country } from "../../../../../Scripts/classes/Country";
import { MultiSelectComponent, IMultiSelectProps, SmartOption } from "../MultiSelect/MultiSelectComponent";
import { IShippingBoxLocalizedKitDto } from "../../../../../Scripts/interfaces/IShippingBoxLocalizedKitDto";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditShippingBoxComponent extends ReactPageComponent<{}, IEditShippingBoxState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            allCountries: new Array<Country>(),
            allProducts: new Array<Product>(),
            shippingBox: new ShippingBox(),
            shippingBoxProducts: new Array<Product>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IEditShippingBoxState;
    }

    componentDidMount() {
        this.setStateFromApi(true).done(() => {
            sumoJS.ajaxPost<Array<Product>>(Constants.url_ShippingBox_GetAllProductsJson,
                null,
                (allProducts) => {
                    this.setState({
                        allProducts,
                    });
                });
        });
    }

    handleChangeShippingBoxName(e) {
        this.state.shippingBox.name = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeIsDisabled(e) {
        this.state.shippingBox.isDisabled = e.target.checked;
        this.setState({ shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxSortOrder(e) {
        this.state.shippingBox.sortOrder = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxRequiresShippingAddress(e) {
        this.state.shippingBox.requiresShippingAddress = e.target.checked;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleSelectCountries(newValue: Array<string>) {
        this.state.shippingBox.countries = newValue.map(countryId => this.state.allCountries.filter(x => x.id === countryId)[0]);
        this.setState({
            isModified: true,
            shippingBox: this.state.shippingBox
        });
    }

    handleSelectProduct(newValue: Array<string>) {
        this.state.shippingBox.products = newValue.map(productId => this.state.allProducts.filter(x => x.id === productId)[0]);
        this.setState({
            isModified: true,
            shippingBox: this.state.shippingBox
        });
    }

    handleChangeShippingBoxLocalizedKitTitle(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.title = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitDescription(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.description = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitInternalDescription(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.internalDescription = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitCarrier(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.carrier = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitPrice(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.price = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitFreeShippingMinimumPrice(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.freeShippingMinimumPrice = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitMinDays(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.minDays = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitMaxDays(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.maxDays = e.target.value;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleChangeShippingBoxLocalizedKitCountWeekends(e, shippingBoxLocalizedKit: ShippingBoxLocalizedKit) {
        shippingBoxLocalizedKit.countWeekends = e.target.checked;
        this.setState({ isModified: true, shippingBox: this.state.shippingBox });
    }

    handleDeleteShippingBox() {
        this.setState({ isDeleting: true });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_ShippingBox_Delete, { shippingBoxId: this.state.shippingBox.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_ShippingBox_Index;
            } else {
                alert("Error");
            }
        });
    }

    handleLocalizedKitCopyValuesFrom(source: IShippingBoxLocalizedKitDto, target: IShippingBoxLocalizedKitDto, completed) {
        target.title = source.title;
        target.description = source.description;
        target.internalDescription = source.internalDescription;
        target.carrier = source.carrier;
        target.price = source.price;
        target.freeShippingMinimumPrice = source.freeShippingMinimumPrice;
        target.minDays = source.minDays;
        target.maxDays = source.maxDays;
        target.countWeekends = source.countWeekends;

        this.setState({
            isModified: true,
            shippingBox: this.state.shippingBox
        }, () => completed());
    }

    submit() {
        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_ShippingBox_AddOrUpdate, this.state.shippingBox, response => {
            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi(false);
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi(isFirstLoad: boolean) {
        return sumoJS.ajaxPost<IEditShipppingBoxDto>(Constants.url_ShippingBox_EditJson,
            {
                shippingBoxId: sumoJS.getQueryStringParameter("shippingBoxId")
            },
            (editShippingBoxDto) => {
                this.setState({
                    authenticatedUser: new User(editShippingBoxDto.authenticatedUser),
                    allCountries: editShippingBoxDto.allCountries.map(x => new Country(x)),
                    allProducts: isFirstLoad ? editShippingBoxDto.shippingBox.products.map(x => new Product(x)) : this.state.allProducts,
                    shippingBox: new ShippingBox(editShippingBoxDto.shippingBox),
                    pageIsLoading: false
                }, () => Functions.setCreateAndEditPermissions(
                    this.state.shippingBox,
                    this.state.authenticatedUser.role.createShippingBox,
                    this.state.authenticatedUser.role.editShippingBox));
            });
    }

    render() {
        return (
            <div className="edit-shipping-box-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.shippingBox.isNew ? "Create Shipping Box" : "Edit Shipping Box"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_ShippingBox_Index} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            !this.state.shippingBox.isNew &&
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                canDelete: this.state.authenticatedUser.role.deleteShippingBox,
                                handleDelete: () => this.handleDeleteShippingBox()
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Global properties
                                </div>
                                <TextboxComponent {...{
                                    label: "Name",
                                    type: "text",
                                    value: this.state.shippingBox.name,
                                    className: "form-group",
                                    placeholder: Constants.string_requiredFieldPlaceholder,
                                    readOnly: Functions.getReadOnlyStateForRequiredField(
                                        this.state.shippingBox,
                                        this.state.authenticatedUser.role.editShippingBoxName),
                                    handleChangeValue: (e) => this.handleChangeShippingBoxName(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Sort order",
                                    type: "number",
                                    value: this.state.shippingBox.sortOrder || 0,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeShippingBoxSortOrder(e)
                                } as ITextboxProps} />
                                <CheckboxComponent {...{
                                    text: "Require Shipping Address",
                                    value: this.state.shippingBox.requiresShippingAddress || false,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeShippingBoxRequiresShippingAddress(e)
                                } as ICheckboxProps} />
                                <MultiSelectComponent {...{
                                    options: this.state.allCountries.map(x => new SmartOption(x.id, x.name)),
                                    value: this.state.shippingBox.countries.map(country => country.id),
                                    label: "Shipping To",
                                    placeholder: "Select the countries of shipping",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectCountries(newValue)
                                } as IMultiSelectProps} />
                                <MultiSelectComponent{...{
                                    options: this.state.allProducts.map(x => new SmartOption(x.id, x.localizedTitle)),
                                    value: this.state.shippingBox.products.map(product => product.id),
                                    label: `Products (${this.state.allProducts.length})`,
                                    placeholder: "Select some products",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectProduct(newValue)
                                } as IMultiSelectProps} />
                                <CheckboxComponent {...{
                                    value: this.state.shippingBox.isDisabled || false,
                                    className: "form-group",
                                    text: "Disabled",
                                    handleChangeValue: (e) => this.handleChangeIsDisabled(e)
                                } as ICheckboxProps} />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Localized properties
                                </div>
                                <LocalizedKitsComponent {...{
                                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source as IShippingBoxLocalizedKitDto, target as IShippingBoxLocalizedKitDto, completed),
                                    editAllLocalizedKits: this.state.authenticatedUser.role.editShippingBoxAllLocalizedKits,
                                    localizedKitUIs: this.state.shippingBox.shippingBoxLocalizedKits.map((shippingBoxLocalizedKit, i) => {
                                        return {
                                            localizedKit: shippingBoxLocalizedKit,
                                            country: shippingBoxLocalizedKit.country,
                                            html:
                                                <div>
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Title",
                                                            type: "text",
                                                            value: shippingBoxLocalizedKit.title,
                                                            placeholder: this.state.shippingBox.shippingBoxLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitTitle(e, shippingBoxLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "Description",
                                                            type: "text",
                                                            value: shippingBoxLocalizedKit.description,
                                                            placeholder: this.state.shippingBox.shippingBoxLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitDescription(e, shippingBoxLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "Internal Description",
                                                            type: "text", value: shippingBoxLocalizedKit.internalDescription,
                                                            placeholder: this.state.shippingBox.shippingBoxLocalizedKits.filter(x => x.country.isDefault)[0].internalDescription,
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitInternalDescription(e, shippingBoxLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Carrier",
                                                            type: "text",
                                                            value: shippingBoxLocalizedKit.carrier,
                                                            placeholder: this.state.shippingBox.shippingBoxLocalizedKits.filter(x => x.country.isDefault)[0].carrier,
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitCarrier(e, shippingBoxLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: `Price (${shippingBoxLocalizedKit.country.currencySymbol})`,
                                                            type: "number",
                                                            value: shippingBoxLocalizedKit.price || 0,
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitPrice(e, shippingBoxLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Free delivery over",
                                                            type: "number",
                                                            value: shippingBoxLocalizedKit.freeShippingMinimumPrice || 0,
                                                            className: "form-group",
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitFreeShippingMinimumPrice(e, shippingBoxLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Min days",
                                                            type: "number",
                                                            value: shippingBoxLocalizedKit.minDays || 0,
                                                            placeholder: this.state.shippingBox.shippingBoxLocalizedKits.filter(x => x.country.isDefault)[0].minDays.toString(),
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitMinDays(e, shippingBoxLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Max days",
                                                            type: "number",
                                                            value: shippingBoxLocalizedKit.maxDays || 0,
                                                            placeholder: this.state.shippingBox.shippingBoxLocalizedKits.filter(x => x.country.isDefault)[0].maxDays.toString(),
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitMaxDays(e, shippingBoxLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <CheckboxComponent {...{
                                                            text: "Count weekends (ETA evaluation)",
                                                            value: shippingBoxLocalizedKit.countWeekends || false,
                                                            handleChangeValue: (e) => this.handleChangeShippingBoxLocalizedKitCountWeekends(e, shippingBoxLocalizedKit)
                                                        } as ICheckboxProps} />
                                                    } as INestedInputProps} />
                                                </div>
                                        } as ILocalizedKitUI;
                                    })
                                } as ILocalizedKitsProps} />
                            </div>
                        </div>
                    </div>
                }
                {this.state.pageIsLoading && <SpinnerComponent />}
            </div>
        );
    }
}