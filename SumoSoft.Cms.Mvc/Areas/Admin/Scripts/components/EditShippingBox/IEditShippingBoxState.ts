﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";
import { Product } from "../../../../../Scripts/classes/Product";
import { Country } from "../../../../../Scripts/classes/Country";

export interface IEditShippingBoxState extends IReactPageState
{
    allCountries?: Array<Country>;
    allProducts?: Array<Product>;
    shippingBoxProducts?: Array<Product>;
    shippingBox?: ShippingBox;
    pageIsLoading?: boolean;
}