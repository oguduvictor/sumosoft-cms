﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IShippingBoxDto } from "../../../../../Scripts/interfaces/IShippingBoxDto";
import { IProductDto } from "../../../../../Scripts/interfaces/IProductDto";
import { ICountryDto } from "../../../../../Scripts/interfaces/ICountryDto";

export interface IEditShipppingBoxDto
{
    authenticatedUser: IUserDto;
    allCountries: Array<ICountryDto>;
    allProducts: Array<IProductDto>,
    shippingBox: IShippingBoxDto;
}