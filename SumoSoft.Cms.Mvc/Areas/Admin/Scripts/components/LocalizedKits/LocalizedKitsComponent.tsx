﻿import * as React from "react";

import { ILocalizedKitDto } from "../../../../../Scripts/interfaces/ILocalizedKitDto";

export interface ILocalizedKitUI {
    localizedKit: ILocalizedKitDto;
    html: JSX.Element;
}

export interface ILocalizedKitsProps {
    className?: string;
    localizedKitUIs: Array<ILocalizedKitUI>;
    editAllLocalizedKits: boolean;
    localizedKitIdInCurrentlyCopying?: string;
    handleCopyValuesFrom?(source: ILocalizedKitDto, target: ILocalizedKitDto, completed: () => void);
}

export class LocalizedKitsComponent extends React.Component<ILocalizedKitsProps, undefined> {

    constructor(props) {
        super(props);
    }

    handleToggleCollapse(targetPanelId: string) {
        $(`#${targetPanelId}`).collapse("toggle");
    }

    handleCopyValuesFrom(source: ILocalizedKitDto, target: ILocalizedKitDto, targetPanelId: string) {
        $(`#panel-${targetPanelId} .alert-copied`).show();
        setTimeout(() => $(`#panel-${targetPanelId} .alert-copied`).hide(), 3000);
        this.props.handleCopyValuesFrom(source, target, () => $(`#${targetPanelId}`).collapse('show'));
    }

    render() {
        return (
            <div className={`localized-kits-component ${this.props.className ? this.props.className : ""}`}>
                <div className="panel-group" id="accordion">
                    {
                        this.props.localizedKitUIs.map((thisLocalizedKitUI, i) =>
                            <div className="panel panel-default" key={i} id={`panel-${thisLocalizedKitUI.localizedKit.id}-${i}`}>
                                <div
                                    className="panel-heading"
                                    data-toggle="collapse"
                                    data-parent="#accordion"
                                    onClick={() => this.handleToggleCollapse(`${thisLocalizedKitUI.localizedKit.id}-${i}`)}>
                                    {
                                        this.props.localizedKitIdInCurrentlyCopying === thisLocalizedKitUI.localizedKit.id
                                            ? <i className="fa fa-spinner fa-pulse fa-fw copying-icon"></i>
                                            : null
                                    }
                                    <span className="panel-title">
                                        {thisLocalizedKitUI.localizedKit.country.name}
                                        {thisLocalizedKitUI.localizedKit.country.isDefault && <span className="text-muted">&nbsp;(default)</span>}
                                    </span>
                                    <span className="alert-copied">
                                        Copied!
                                    </span>
                                </div>
                                <div className="dropdown options">
                                    <button className="dropdown-toggle" type="button" data-toggle="dropdown">
                                        <i className="fa fa-ellipsis-v"></i>
                                    </button>
                                    <ul className="dropdown-menu dropdown-menu-right">
                                        <li className="dropdown-header">
                                            Copy values from
                                        </li>
                                        {
                                            this.props.localizedKitUIs
                                                .filter(x => x.localizedKit.country.id !== thisLocalizedKitUI.localizedKit.country.id)
                                                .map(x => x.localizedKit)
                                                .map((localizedKit, ii) =>
                                                    <li key={"country-option" + ii}>
                                                        <a className="" onClick={() => this.handleCopyValuesFrom(localizedKit, thisLocalizedKitUI.localizedKit, `${thisLocalizedKitUI.localizedKit.id}-${i}`)}>
                                                            {localizedKit.country.name}
                                                        </a>
                                                    </li>)
                                        }
                                    </ul>
                                </div>
                                <div id={`${thisLocalizedKitUI.localizedKit.id}-${i}`} className={thisLocalizedKitUI.localizedKit.country.isDefault ? "panel-collapse collapse in" : "panel-collapse collapse"}>
                                    <div className="panel-body">
                                        {thisLocalizedKitUI.html}
                                    </div>
                                </div>
                            </div>)
                    }
                </div>
            </div>
        );
    }
}