﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { IEditUserState } from "./IEditUserState";
import { Country } from "../../../../../Scripts/classes/Country";
import { IEditUserDto } from "./IEditUserDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { Order } from "../../../../../Scripts/classes/Order";
import { OrderStatusEnum } from "../../../../../Scripts/enums/OrderStatusEnum";
import { Address } from "../../../../../Scripts/classes/Address";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { UserCredit } from "../../../../../Scripts/classes/UserCredit";
import { UserLocalizedKit } from "../../../../../Scripts/classes/UserLocalizedKit";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { IUserLocalizedKitDto } from "../../../../../Scripts/interfaces/IUserLocalizedKitDto";
import { IUserCreditDto } from "../../../../../Scripts/interfaces/IUserCreditDto";
import { Functions } from "../../shared/Functions";
import { GenderEnum } from "../../../../../Scripts/enums/GenderEnum";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditUserComponent extends ReactPageComponent<{}, IEditUserState> {

    constructor(props) {
        super(props);

        this.state = {
            formErrors: new Array<IFormError>(),
            countries: new Array<Country>(),
            pageIsLoading: true,
            user: new User(),
            userRoles: new Array<UserRole>(),
            authenticatedUser: new User({ role: new UserRole() }),
            orders: new Array<Order>()
        } as IEditUserState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    handleChangeShowMembershipSalePrice(e) {
        this.state.user.showMembershipSalePrice = e.target.checked;
        this.setState({
            user: this.state.user
        });
    }

    handleChangeIsDisabled(e) {
        this.state.user.isDisabled = e.target.checked;
        this.setState({
            user: this.state.user
        });
    }

    handleChangeUserCountry(e) {
        const country = new Country();
        country.id = e.target.value;
        this.state.user.country = country;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeUserEmail(e) {
        this.state.user.email = e.target.value;
        this.setState({
            user: this.state.user
        });
    }

    handleChangeUserFirstName(e) {
        this.state.user.firstName = e.target.value;
        this.setState({
            user: this.state.user
        });
    }

    handleChangeUserLastName(e) {
        this.state.user.lastName = e.target.value;
        this.setState({
            user: this.state.user
        });
    }

    handleChangeUserPassword(e) {
        this.state.user.password = e.target.value;
        this.setState({ user: this.state.user });
    }

    handleChangeUserGender(e) {
        this.state.user.gender = Number(e.target.value);
        this.setState({ user: this.state.user });
    }

    handleChangeUserRole(e) {
        const userRole = new UserRole();
        userRole.id = e.target.value;
        this.state.user.role = userRole;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressFirstName(e, address: Address) {
        address.firstName = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressLastName(e, address: Address) {
        address.lastName = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressLine1(e, address: Address) {
        address.addressLine1 = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressLine2(e, address: Address) {
        address.addressLine2 = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressCity(e, address: Address) {
        address.city = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressStateCountyProvince(e, address: Address) {
        address.stateCountyProvince = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressPostCode(e, address: Address) {
        address.postcode = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressCountry(e, address: Address) {
        const country = new Country();
        country.id = e.target.value;
        address.country = country;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleChangeAddressPhoneNumber(e, address: Address) {
        address.phoneNumber = e.target.value;
        address.isModified = true;

        this.setState({
            user: this.state.user
        });
    }

    handleOpenEditUserCreditModal(userLocalizedKit: UserLocalizedKit, userCredit?: UserCredit) {
        const editUserCredit = userCredit
            ? sumoJS.deepClone(userCredit)
            : new UserCredit({
                id: Functions.newGuid(),
                userLocalizedKit: userLocalizedKit
            });

        this.setState({
            editUserCredit
        }, () => $("#edit-user-credits-modal").modal("show"));
    }

    handleChangeEditUserCreditAmount(e) {
        const editUserCredit = this.state.editUserCredit;
        editUserCredit.amount = e.target.value;

        this.setState({
            editUserCredit
        });
    }

    handleChangeEditUserCreditReference(e) {
        const editUserCredit = this.state.editUserCredit;
        editUserCredit.reference = e.target.value;

        this.setState({
            editUserCredit
        });
    }

    handleSubmitUserCredit() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse>(
            Constants.url_User_AddOrUpdateUserCredit,
            this.state.editUserCredit,
            response => {
                if (response.isValid) {
                    this.setStateFromApi();
                    $("#edit-user-credits-modal").modal("hide");
                } else {
                    this.setState({
                        formErrors: response.errors
                    });
                }
                this.setState({
                    isSubmitting: false
                });
            });

    }

    handleDeleteUserCredit(userCredit: UserCredit) {
        this.setState({
            isDeleting: true
        });

        sumoJS.ajaxPost<IFormResponse>(
            Constants.url_User_DeleteUserCredit,
            { id: userCredit.id },
            response => {
                if (response.isValid) {
                    this.setStateFromApi();
                    $("#edit-user-credits-modal").modal("hide");
                }
                this.setState({
                    isDeleting: false
                });
            });

    }

    handleDeleteUser() {
        this.setState({
            isDeleting: true
        });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_User_Delete, { id: this.state.user.id },
            response => {
                if (response.isValid) {
                    window.location.href = Constants.url_User_Index;
                } else {
                    this.setState({
                        isDeleting: false,
                        formErrors: response.errors
                    });
                }
            });
    }

    handleLocalizedKitCopyValuesFrom(source: IUserLocalizedKitDto, target: IUserLocalizedKitDto, completed) {
        if (this.state.localizedKitIdInCurrentlyCopying) {
            return;
        }

        this.setState({
            localizedKitIdInCurrentlyCopying: target.id
        });

        target.userCredits = [] as Array<IUserCreditDto>;

        const userCredits = source.userCredits.map(x => {
            x.id = Functions.newGuid(),
                x.userLocalizedKit = target;
            return x;
        });
        sumoJS.ajaxPost<IFormResponse>(
            Constants.url_User_AddOrUpdateUserCredits,
            userCredits,
            response => {
                if (response.isValid) {
                    this.setStateFromApi();
                    completed();
                }
            });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditUserDto>(Constants.url_User_EditJson,
            {
                userId: sumoJS.getQueryStringParameter("userId")
            },
            (editUserDto) => {
                this.setState({
                    authenticatedUser: new User(editUserDto.authenticatedUser),
                    countries: editUserDto.countries.map(x => new Country(x)),
                    pageIsLoading: false,
                    userRoles: editUserDto.userRoles.map(x => new UserRole(x)),
                    user: new User(editUserDto.user),
                    orders: editUserDto.orders.map(x => new Order(x)),
                    localizedKitIdInCurrentlyCopying: null
                }, () => Functions.setCreateAndEditPermissions(
                    this.state.user,
                    this.state.authenticatedUser.role.createUser,
                    this.state.authenticatedUser.role.editUser));
            });
    }

    submit() {
        this.setState({ isSubmitting: true });

        const user = sumoJS.deepClone(this.state.user);
        user.addresses = user.addresses.filter(x => x.isModified);

        sumoJS.ajaxPost<IFormResponse>(Constants.url_User_AddOrUpdate, user, response => {

            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    isModified: false,
                    isSubmitting: false
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false
                });
            }
        });
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">
                    {
                        this.state.user.isNew ? "Create user" : " Edit user"
                    }
                </div>
                <div className="admin-top-bar-controls">
                    <a href={Constants.url_User_Index} className="btn btn-link">
                        Back to list
                    </a>
                    <ButtonDeleteComponent {...{
                        isDeleting: this.state.isDeleting,
                        className: this.state.authenticatedUser.role.deleteUser ? "" : "hidden",
                        canDelete: this.state.authenticatedUser.role.deleteUser,
                        handleDelete: () => this.handleDeleteUser()
                    } as IButtonDeleteProps} />
                    <ButtonSubmitComponent {...{
                        isSubmitting: this.state.isSubmitting,
                        handleSubmit: () => this.submit()
                    } as IButtonSubmitProps} />
                </div>
            </div>
        );
    }

    renderUserSection() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    User Details
                </div>
                <div className="table-wrapper">
                    <TextboxComponent {...{
                        label: "First Name",
                        value: this.state.user.firstName,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeUserFirstName(e)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "Last Name",
                        value: this.state.user.lastName,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeUserLastName(e)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "Email",
                        value: this.state.user.email,
                        className: "form-group",
                        type: "email",
                        handleChangeValue: (e) => this.handleChangeUserEmail(e)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "Password",
                        value: this.state.user.password,
                        className: "form-group",
                        type: "password",
                        placeholder: this.state.user.isNew ? "" : "••••••••",
                        handleChangeValue: (e) => this.handleChangeUserPassword(e)
                    } as ITextboxProps} />
                    <SelectComponent {...{
                        options: Object.keys(GenderEnum).filter(x => typeof GenderEnum[x] !== "number").map((value, i) =>
                            <option key={value} value={value}>{this.state.user.genderDescriptions[i]}</option>),
                        value: this.state.user.gender ? this.state.user.gender.toString() : "",
                        label: "Gender",
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeUserGender(e)
                    } as ISelectProps} />
                    {
                        this.state.authenticatedUser.role.editUser &&
                        <SelectComponent {...{
                            options: this.state.userRoles.map(
                                x => <option key={x.id} value={x.id} > {x.name} </option>),
                            value: this.state.user.role ? this.state.user.role.id : "",
                            label: "Role",
                            className: "form-group",
                            handleChangeValue: (e) => this.handleChangeUserRole(e)
                        } as ISelectProps} />
                    }
                    <SelectComponent {...{
                        options: this.state.countries.map(x => <option key={x.id} value={x.id} > {x.name} </option>),
                        value: this.state.user.country ? this.state.user.country.id : "",
                        label: "Country",
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeUserCountry(e)
                    } as ISelectProps} />
                    <CheckboxComponent {...{
                        value: this.state.user.showMembershipSalePrice || false,
                        className: "form-group",
                        text: "Show Membership Sale Price",
                        handleChangeValue: (e) => this.handleChangeShowMembershipSalePrice(e)
                    } as ICheckboxProps} />
                    <CheckboxComponent {...{
                        value: this.state.user.isDisabled || false,
                        className: "form-group",
                        text: "Disabled",
                        handleChangeValue: (e) => this.handleChangeIsDisabled(e)
                    } as ICheckboxProps} />
                </div>
            </div>
        );
    }

    renderAddress(address: Address, index: number) {
        return (
            <div key={address.id} className="admin-subsection">
                <div className="admin-subsection-title">
                    Address #{index + 1}
                </div>
                <div className="table-wrapper">
                    <TextboxComponent {...{
                        label: "First name",
                        value: address.firstName,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressFirstName(e, address)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "Last name",
                        value: address.lastName,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressLastName(e, address)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "Address line 1",
                        value: address.addressLine1,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressLine1(e, address)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "Address line 2",
                        value: address.addressLine2,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressLine2(e, address)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "City",
                        value: address.city,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressCity(e, address)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "State/County/Province",
                        value: address.stateCountyProvince,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressStateCountyProvince(e, address)
                    } as ITextboxProps} />
                    <TextboxComponent {...{
                        label: "Post Code",
                        value: address.postcode,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressPostCode(e, address)
                    } as ITextboxProps} />
                    <SelectComponent {...{
                        options: this.state.countries.map(x => <option key={x.id} value={x.id} > {x.name} </option>),
                        value: address.country ? address.country.id : "",
                        label: "Country",
                        className: "form-group",
                        handleChangeValue: (e) => this.handleChangeAddressCountry(e, address)
                    } as ISelectProps} />
                    <TextboxComponent {...{
                        label: "Phone Number",
                        value: address.phoneNumber,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: (e) => this.handleChangeAddressPhoneNumber(e, address)
                    } as ITextboxProps} />
                </div>
            </div>
        );
    }

    renderNoAddress() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Address
                </div>
                <p className="text-muted">
                    This user does not have any address
                </p>
            </div>
        );
    }

    renderOrders() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Orders
                </div>
                <div className="table-wrapper">
                    <TableComponent {...{
                        thList: ["#", "Date", "Country", "Status", "Revenue", ""],
                        trList: this.state.orders
                            .map(x => [
                                x.orderNumber.toString(),
                                dayjs(x.createdDate).format("DD/MM/YYYY"),
                                x.countryName,
                                Functions.getEnumDescription(OrderStatusEnum, x.statusDescriptions, x.status),
                                x.currencySymbol + " " + x.totalAfterTax.toFixed(2),
                                <a href={Constants.url_Order_Edit + x.id} className="btn btn-primary btn-xs">open</a>
                            ])
                    } as ITableProps} />
                </div>
            </div>
        );
    }

    renderUserCredits() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Localized properties
                </div>
                <LocalizedKitsComponent {...{
                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source as IUserLocalizedKitDto, target as IUserLocalizedKitDto, completed),
                    className: "user-localized-kits-component",
                    localizedKitUIs: this.state.user.userLocalizedKits.map(userLocalizedKit => {
                        return {
                            localizedKit: userLocalizedKit,
                            html:
                                <div>
                                    <TableComponent {...{
                                        thList: [
                                            <button className="btn btn-link btn-xs" onClick={() => this.handleOpenEditUserCreditModal(userLocalizedKit)}>
                                                <i className="fa fa-plus"></i> Add Credit
                                            </button>,
                                            "Reference",
                                            "Amount"
                                        ],
                                        trList: [...userLocalizedKit.userCredits.map(userCredit =>
                                            [
                                                <span>
                                                    <button className="btn btn-link" onClick={() => this.handleOpenEditUserCreditModal(userLocalizedKit, userCredit)}>
                                                        <i className="fa fa-pencil"></i>
                                                    </button>
                                                    <ButtonDeleteComponent {...{
                                                        isDeleting: this.state.isDeleting,
                                                        className: "btn-link",
                                                        icon: "fa fa-trash-o",
                                                        text: " ",
                                                        handleDelete: () => this.handleDeleteUserCredit(userCredit)
                                                    } as IButtonDeleteProps} />
                                                </span>,
                                                userCredit.reference,
                                                userLocalizedKit.country.currencySymbol + userCredit.amount
                                            ]),
                                        [
                                            "",
                                            "Total",
                                            userLocalizedKit.country.currencySymbol + userLocalizedKit.userCredits.sum(x => x.amount)
                                        ]
                                        ]
                                    } as ITableProps} />
                                </div>
                        } as ILocalizedKitUI;
                    }),
                    editAllLocalizedKits: true,
                    localizedKitIdInCurrentlyCopying: this.state.localizedKitIdInCurrentlyCopying
                } as ILocalizedKitsProps} />
            </div>
        );
    }

    renderNoUserCreditsInLocalizedKit() {
        return (
            <p className="text-muted">
                This user does not have any user credits in this localized kit.
            </p>
        );
    }

    render() {
        return (
            <div className="edit-user-component">
                {
                    this.renderTopBar()
                }
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                <ModalComponent {...{
                    id: `edit-user-credits-modal`,
                    title: "Edit User Credit",
                    body:
                        <span>
                            <TextboxComponent {...{
                                label: "Amount",
                                type: "number",
                                value: sumoJS.get(() => this.state.editUserCredit.amount, 0),
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeEditUserCreditAmount(e)
                            } as ITextboxProps} />
                            <TextboxComponent {...{
                                label: "Reference",
                                type: "text",
                                value: sumoJS.get(() => this.state.editUserCredit.reference),
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeEditUserCreditReference(e)
                            } as ITextboxProps} />
                        </span>,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                isSubmitting: this.state.isSubmitting,
                                handleSubmit: () => this.handleSubmitUserCredit()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">
                                Cancel
                        </button>
                        </span>
                } as IModalProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6" >
                            {
                                this.renderUserSection()
                            }
                            {
                                this.state.user.addresses.length > 0 ||
                                this.renderNoAddress()
                            }
                            {
                                this.state.user.addresses.length > 0 &&
                                this.state.user.addresses.map((x, i) => this.renderAddress(x, i))
                            }
                        </div>
                        <div className="col-md-6">
                            {
                                this.renderOrders()
                            }
                            {
                                this.renderUserCredits()
                            }
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}