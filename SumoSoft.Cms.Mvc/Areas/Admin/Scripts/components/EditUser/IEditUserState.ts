﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Country } from "../../../../../Scripts/classes/Country";
import { Order } from "../../../../../Scripts/classes/Order";
import { UserCredit } from "../../../../../Scripts/classes/UserCredit";

export interface IEditUserState extends IReactPageState {
    authenticatedUser?: User;
    countries?: Array<Country>;
    editUserCredit?: UserCredit,
    user?: User;
    userRoles?: Array<UserRole>;
    orders?: Array<Order>;
    isSubmitting?: boolean;
    isDeleting?: boolean;
    localizedKitIdInCurrentlyCopying?: string;
}