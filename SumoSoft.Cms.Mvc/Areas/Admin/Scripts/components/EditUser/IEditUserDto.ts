﻿import { Country } from "../../../../../Scripts/classes/Country";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Order } from "../../../../../Scripts/classes/Order";
import { UserLocalizedKit } from "../../../../../Scripts/classes/UserLocalizedKit";

export interface IEditUserDto {
    authenticatedUser?: IUserDto;
    countries: Array<Country>;
    user: IUserDto;
    userRoles: Array<UserRole>;
    orders: Array<Order>;
    userLocalizedKits: Array<UserLocalizedKit>;
}