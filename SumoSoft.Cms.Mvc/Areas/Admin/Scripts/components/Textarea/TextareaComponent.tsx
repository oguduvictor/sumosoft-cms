﻿import * as React from "react";
import { Functions } from "../../shared/Functions";
import Textarea from "react-textarea-autosize";
import { TopLabel } from "../TopLabel/TopLabel";

export interface ITextareaProps {
    label?: string;
    type?: string;
    value?: any;
    className?: string;
    placeholder?: string;
    addon?: JSX.Element;
    handleChangeValue?(e): void;
    handleBlur?(e): void;
    readOnly?: boolean;
}

export class TextareaComponent extends React.Component<ITextareaProps, undefined> {
    textareaId = Functions.newGuid();

    render() {
        return (
            <div className={`textarea-component input-sub-parent ${this.props.className ? this.props.className : ""}`}>
                <TopLabel label={this.props.label}/>
                <Textarea
                    id={this.textareaId}
                    ref="textarea"
                    value={this.props.value}
                    placeholder={this.props.placeholder}
                    minRows={1}
                    maxRows={36}
                    onChange={e => this.props.handleChangeValue && this.props.handleChangeValue(e)}
                    onBlur={this.props.handleBlur}
                    readOnly={this.props.readOnly}
                />
                {this.props.addon}
            </div>
        );
    }
}
