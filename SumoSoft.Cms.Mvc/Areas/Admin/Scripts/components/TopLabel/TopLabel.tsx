﻿import * as React from "react";

import { TooltipComponent } from "../Tooltip/TooltipComponent";

export interface ITopLabelProps {
    label?: string;
    tooltip?: string;
}

export var TopLabel: (props: ITopLabelProps) => JSX.Element = props => {
    return props.label ?
        <label className="top-label-component">
            {
                props.label
            }
            &nbsp;
            {
                props.tooltip && <TooltipComponent title={props.tooltip} placement="right" />
            }
        </label> : null;
}