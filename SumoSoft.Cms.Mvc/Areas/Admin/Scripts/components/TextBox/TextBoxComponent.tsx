﻿import * as React from "react";

import { TopLabel } from "../TopLabel/TopLabel";

export interface ITextboxProps {
    label?: string;
    type?: string;
    placeholder?: string;
    value?: any;
    tooltip?: string;
    className?: string;
    addon?: JSX.Element;
    handleChangeValue?(e): void;
    handleBlur?(e): void;
    readOnly?: boolean;
}

export var TextboxComponent: (props: ITextboxProps) => JSX.Element = props => {
    return (
        <div className={`textbox-component input-sub-parent ${props.className ? props.className : ""}`}>
            <TopLabel
                label={props.label}
                tooltip={props.tooltip}
            />
            <input type={props.type} value={props.value || ""} placeholder={props.placeholder} onChange={props.handleChangeValue} onBlur={props.handleBlur} readOnly={props.readOnly}/>
            {
                props.addon
            }
        </div>
    );
}