﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { DirectoryDto } from "../../../../../Scripts/classes/DirectoryDto";
import { IDirectoryDto } from "../../../../../Scripts/interfaces/IDirectoryDto";
import { Constants } from "../../shared/Constants";
import { Functions } from "../../shared/Functions";
import { MediaThumbnail } from "../MediaThumbnail/MediaThumbnail";

export interface IFilePickerState {
    modalId?: string;
    directory?: DirectoryDto;
    isLoading?: boolean;
}

export interface IFilePickerProps {
    value?: string;
    label?: string;
    className?: string;
    handleChangeValue(absoluteUrl: string): void;
}

export class FilePickerComponent extends React.Component<IFilePickerProps, IFilePickerState> {

    state = {
        modalId: Functions.newGuid(),
        directory: new DirectoryDto()
    } as IFilePickerState;

    constructor(props) {
        super(props);
    }

    handleGetDirectory(localPath: string) {

        $(`#${this.state.modalId}`).modal("show");

        this.setState({ isLoading: true });

        sumoJS.ajaxPost<IDirectoryDto>(Constants.url_Media_IndexJson, { localPath }, (directoryDto) => {

            this.setState({
                directory: new DirectoryDto(directoryDto),
                isLoading: false
            });
        });
    }

    handleSelectFile(absoluteUrl: string) {
        this.props.handleChangeValue(absoluteUrl);
        $(`#${this.state.modalId}`).modal("hide");
    }

    render() {
        return (
            <div className="file-picker-component">
                <TextboxComponent {...{
                    label: this.props.label,
                    type: "text",
                    value: this.props.value,
                    className: this.props.className,
                    addon: <button type="button" className="textbox-addon" data-toggle="modal" onClick={() => this.handleGetDirectory("")}>
                        <i className="fa fa-folder-open-o"></i>
                    </button>,
                    handleChangeValue: (e) => this.props.handleChangeValue(e.target.value)
                } as ITextboxProps} />
                <ModalComponent {...{
                    id: this.state.modalId,
                    title: "Choose an image",
                    body:
                        <span className="file-picker-modal-body">
                            {!this.state.isLoading &&
                                <span>
                                    {this.state.directory.isRoot != undefined && !this.state.directory.isRoot &&
                                        <a className="parent" onClick={() => this.handleGetDirectory(this.state.directory.parent.localPath)}>
                                            <p>
                                                Parent Folder
                                            </p>
                                        </a>
                                    }
                                    {this.state.directory.directories.map((x, i) =>
                                        <a key={i} title={x.name} className="directory" onClick={() => this.handleGetDirectory(x.localPath)}>
                                            <p>
                                                {x.name}
                                            </p>
                                        </a>
                                    )}
                                    {this.state.directory.files.filter(x => x.extension && x.extension.toLowerCase() !== ".pdf").map((x, i) =>
                                        <a key={i} title={x.name} className="file" onClick={() => this.handleSelectFile(x.absolutePath)}>
                                            <MediaThumbnail file={x} imageWidth={200} />
                                            <p>
                                                {x.name}
                                            </p>
                                        </a>
                                    )}
                                </span>
                            }
                            {this.state.isLoading && <SpinnerComponent />}
                        </span>
                } as IModalProps} />
            </div>
        );
    }
}