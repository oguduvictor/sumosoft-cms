﻿import * as React from "react";
import * as ReactDOMServer from "react-dom/server";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { Functions } from "../../shared/Functions";

export interface IButtonDeleteProps {
    text?: string;
    className?: string;
    icon?: string;
    isDeleting: boolean;
    canDelete?: boolean;
    handleDelete?(e): void;
}

export var ButtonDeleteComponent: (props: IButtonDeleteProps) => JSX.Element = props => {

    const modalId = Functions.newGuid();

    const modal =
        <ModalComponent {...{
            id: `confirm-delete-${modalId}`,
            title: "Confirmation",
            body:
            <span>
                Are you sure you want to proceed?
            </span>,
            footer:
            <span>
                <a className="btn btn-primary" data-dismiss="modal">
                    Confirm
                </a>
                <button type="button" className="btn btn-default" data-dismiss="modal">
                    Cancel
                </button>
            </span>
        } as IModalProps} />;

    var handleOpenModal = () => {
        $(`#confirm-delete-${modalId}`).modal("show");
    }

    $("body").append(ReactDOMServer.renderToString(modal));

    $(document).on("click", `#confirm-delete-${modalId} .btn-primary`, props.handleDelete);

    {/* If isDeleting === true, the button is disabled and the onClick event handler is removed */ }

    if (props.isDeleting)
        return (
            <button className={`button-delete-component btn disabled ${props.className ? props.className : "btn-danger"}`} type="button">
                <i className="fa fa-spinner fa-pulse fa-fw"></i>
                {
                    props.icon && <i className={props.icon}></i>
                }
                {props.text || "Delete"}
            </button>
        );

    {/* If isDeleting === false, enable the button and add the onClick event handler */ }

    return (
        props.canDelete === undefined
            ? <button className={`button-delete-component btn ${props.className ? props.className : "btn-danger"}`} type="button" data-toggle="modal" onClick={handleOpenModal}>
                  {
                    props.icon && <i className={props.icon}></i>
                  }
                  {props.text || "Delete"}
              </button>
            : <button className={`button-delete-component btn ${props.className ? props.className : "btn-danger"} ${props.canDelete ? "" : "disabled"}`} type="button" data-toggle="modal" onClick={props.canDelete ? handleOpenModal : null}>
                {
                    props.icon && <i className={props.icon}></i>
                }
                {props.text || "Delete"}
            </button> 
    );
}