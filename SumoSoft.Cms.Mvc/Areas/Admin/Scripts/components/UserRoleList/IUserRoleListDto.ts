﻿import { IUserRoleDto } from "../../../../../Scripts/interfaces/IUserRoleDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IUserRoleListDto {
    authenticatedUser: IUserDto;
    userRoles: Array<IUserRoleDto>;
}