﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { User } from "../../../../../Scripts/classes/User";

export interface IUserRoleListState extends IReactPageState {
    authenticatedUser: User;
    userRoles?: Array<UserRole>;
} 