﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { User } from "../../../../../Scripts/classes/User";
import { Constants } from "../../shared/Constants";
import { IUserRoleListState } from "./IUserRoleListState";
import { IUserRoleListDto } from "./IUserRoleListDto";
import { TableComponent, ITableProps } from "../Table/TableComponent";

export default class UserRoleListComponent extends ReactPageComponent<{}, IUserRoleListState> {

    constructor(props) {
        super(props);

        this.state = {
            userRoles: new Array<UserRole>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IUserRoleListState;
    }

    componentDidMount() {
        this.setState({ pageIsLoading: true });

        sumoJS.ajaxPost<IUserRoleListDto>(Constants.url_User_RolesJson, null, (userRoleListDto) => {

            this.setState({
                authenticatedUser: new User(userRoleListDto.authenticatedUser),
                userRoles: userRoleListDto.userRoles.map(x => new UserRole(x)),
                pageIsLoading: false
            });
        });
    }

    render() {
        return (
            <div className="user-role-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        User roles
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_User_EditUserRole} className={`btn btn-primary ${this.state.authenticatedUser.role.createUserRole ? "" : "disabled"}`}>
                            <i className="fa fa-plus"></i>
                            Create new role
                        </a>
                    </div>
                </div>
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">

                        <TableComponent {...{
                            className: "table table-striped table-responsive-sm",
                            thList: ["#", "Role", ""],
                            trList: this.state.userRoles
                                .map((userRole, i) => [
                                    i + 1,
                                    userRole.name,
                                    <a href={Constants.url_User_EditUserRole + userRole.id} className="btn btn-primary btn-xs">edit</a>])
                        } as ITableProps} />
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}