﻿import * as React from "react";

import { FileDto } from "../../../../../Scripts/classes/FileDto";

interface IMediaThumbnailProps {
    file: FileDto;
    imageWidth?: number;
}

export var MediaThumbnail: (props: IMediaThumbnailProps) => JSX.Element = props => {
    var imageUrl = "/Areas/Admin/Content/img/file.png";

    switch (props.file.extension) {
        case ".zip":
            imageUrl = "/Areas/Admin/Content/img/zip.png";
            break;
        case ".pdf":
            imageUrl = "/Areas/Admin/Content/img/pdf.png";
            break;
        case ".txt":
            imageUrl = "/Areas/Admin/Content/img/txt.png";
            break;
        case ".mp4":
            imageUrl = "/Areas/Admin/Content/img/mp4.png";
            break;
        case ".jpg":
        case ".jpeg":
        case ".png":
        case ".tif":
        case ".tiff":
        case ".gif":
            imageUrl = `/imagik?url=${props.file.absolutePath}&width=${props.imageWidth ? props.imageWidth : 100}&save=false`;
            break;
    }

    return (
        <div className="media-thumbnail">
            <div className="image" style={{ backgroundImage: `url(${imageUrl})` }}></div>
        </div>
    );
}