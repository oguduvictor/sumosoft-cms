﻿import { IOrderDto } from "../../../../../Scripts/interfaces/IOrderDto";
import { IEmailDto } from "../../../../../Scripts/interfaces/IEmailDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditOrderDto {
    authenticatedUser: IUserDto;
    order: IOrderDto;
    allEmails: Array<IEmailDto>;
    defaultReplyToEmail: string;
}