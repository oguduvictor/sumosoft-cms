﻿import { ISendEmailConfirmationModalDto } from "./ISendEmailConfirmationModalDto";

export class SendEmailConfirmationModalDto implements ISendEmailConfirmationModalDto {
    entityId: string;
    emailName: string;
    sendTo: string;
}