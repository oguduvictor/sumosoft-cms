﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Order } from "../../../../../Scripts/classes/Order";
import { Email } from "../../../../../Scripts/classes/Email";
import { SendEmailConfirmationModalDto } from "./SendEmailConfirmationModalDto";

export interface IEditOrderState extends IReactPageState {
    order?: Order;
    allEmails?: Array<Email>;
    sendEmailConfirmationModalDetails?: SendEmailConfirmationModalDto;
}