﻿export interface ISendEmailConfirmationModalDto {
    entityId: string;
    emailName: string;
    sendTo: string;
}