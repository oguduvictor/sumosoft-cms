﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { OrderSummary } from "../../../../../Scripts/sumoReact/OrderSummary/OrderSummary";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { Constants } from "../../shared/Constants";
import { Order } from "../../../../../Scripts/classes/Order";
import { OrderItemVariant } from "../../../../../Scripts/classes/OrderItemVariant";
import { OrderShippingBox } from "../../../../../Scripts/classes/OrderShippingBox";
import { Email } from "../../../../../Scripts/classes/Email";
import { IEditOrderState } from "./IEditOrderState";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { OrderItemStockStatusEnum } from "../../../../../Scripts/enums/OrderItemStockStatusEnum";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { VariantTypesEnum } from "../../../../../Scripts/enums/VariantTypesEnum";
import { IEditOrderDto } from "./IEditOrderDto";
import { SendEmailConfirmationModalDto } from "./SendEmailConfirmationModalDto";
import { OrderAddress } from "../../../../../Scripts/classes/OrderAddress";
import { OrderStatusEnum } from "../../../../../Scripts/enums/OrderStatusEnum";
import { OrderShippingBoxStatusEnum } from "../../../../../Scripts/enums/OrderShippingBoxStatusEnum";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { OrderItemStatusEnum } from "../../../../../Scripts/enums/OrderItemStatusEnum";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditOrderComponent extends ReactPageComponent<{}, IEditOrderState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            formErrors: new Array<IFormError>(),
            allEmails: new Array<Email>(),
            order: new Order(),
            pageIsLoading: false,
            sendEmailConfirmationModalDetails: new SendEmailConfirmationModalDto()
        } as IEditOrderState;
    }

    componentDidMount() {
        this.setStateFromApi();
    }

    getFormattedPrice(price: number) {
        price = price || 0;
        if (price >= 0) {
            return this.state.order.currencySymbol + " " + price.toFixed(2);
        }
        return "- " + this.state.order.currencySymbol + " " + Math.abs(price).toFixed(2);
    }

    getOrderItemVariantValue(orderItemVariant: OrderItemVariant) {
        switch (orderItemVariant.variantType) {
            case VariantTypesEnum.Options:
                return orderItemVariant.variantOptionName;
            case VariantTypesEnum.Boolean:
                return orderItemVariant.booleanValue;
            case VariantTypesEnum.Double:
                return orderItemVariant.doubleValue;
            case VariantTypesEnum.Integer:
                return orderItemVariant.integerValue;
            case VariantTypesEnum.String:
                return orderItemVariant.stringValue;
            default:
                return "";
        }
    }

    handleChangeOrderComments(e) {
        this.state.order.comments = e.target.value;
        this.setState({
            isModified: true,
            order: this.state.order
        });
    }

    handleChangeOrderShippingBoxStatus(e, orderShippingBox: OrderShippingBox) {
        this.state.order.orderShippingBoxes.filter(x => x.id === orderShippingBox.id)[0].status = e.target.value;
        this.setState({
            isModified: true,
            order: this.state.order
        });
    }

    handleChangeOrderStatus(e) {
        this.state.order.status = e.target.value;

        this.setState({
            isModified: true,
            order: this.state.order
        });
    }

    handleChangeOrderShippingBoxTrackingCode(e, orderShippingBox: OrderShippingBox) {
        orderShippingBox.trackingCode = e.target.value;
        this.setState({
            isModified: true,
            order: this.state.order
        });
    }

    handleOpenSendEmailConfirmationModal(e, email: Email) {
        const { sendEmailConfirmationModalDetails } = this.state;

        sendEmailConfirmationModalDetails.entityId = this.state.order.id;
        sendEmailConfirmationModalDetails.emailName = email.name;
        sendEmailConfirmationModalDetails.sendTo = this.state.order.userEmail;

        this.setState({
            sendEmailConfirmationModalDetails
        });
    }

    handleChangeSendEmailConfirmationModalDetailsSendTo(e) {
        this.state.sendEmailConfirmationModalDetails.sendTo = e.target.value;
        this.setState({
            sendEmailConfirmationModalDetails: this.state.sendEmailConfirmationModalDetails
        });
    }

    handleChangeOrderItemStatus(e, orderItemId: string) {
        this.state.order.orderShippingBoxes.map(shippingBox => shippingBox.orderItems
            .filter(orderItem => orderItem.id === orderItemId)).reduce(x => x)[0].status = e.target.value;

        this.setState({
            isModified: true,
            order: this.state.order
        });
    }

    handleRestock(e, orderItemId: string) {
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Order_RestoreStock, { orderItemId: orderItemId }, response => {
            if (response.isValid) {
                window.location.href = response.redirectUrl;
            } else {
                this.setState({
                    isSubmitting: false
                });
            }
        });
    }

    sendConfirmationEmail() {
        this.setState({
            isSubmitting: true
        });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Order_SendEmail, this.state.sendEmailConfirmationModalDetails, response => {
            if (response.isValid) {
                window.location.href = response.redirectUrl;
            } else {
                this.setState({
                    isSubmitting: false
                });
                $(".modal-errors").html(response.errors.map(x => x.message).toString());
            }
        });
    }

    setStateFromApi(done?: () => void) {
        this.setState({
            pageIsLoading: true
        });
        var orderId = sumoJS.getQueryStringParameter("orderId");
        if (!orderId) {
            window.location.href = Constants.url_Order_Index
        }
        sumoJS.ajaxPost<IEditOrderDto>(Constants.url_Order_EditJson, { orderId }, (editOrderDto) => {
            this.setState({
                authenticatedUser: new User(editOrderDto.authenticatedUser),
                order: new Order(editOrderDto.order),
                allEmails: editOrderDto.allEmails.map(x => new Email(x)),
                pageIsLoading: false
            }, () => {
                !!done && done();
                Functions.setCreateAndEditPermissions(this.state.order, false, this.state.authenticatedUser.role.editOrder);
                $(".order-summary-component table").addClass("table table-responsive-sm");
            });
        }
        );
    }

    submit() {
        this.setState({
            isSubmitting: true
        });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Order_Update, this.state.order, response => {
            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    // ---------------------------------------------------------------------
    // Render methods
    // ---------------------------------------------------------------------

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">
                    Order n. {this.state.order.orderNumber}
                </div>
                <div className="admin-top-bar-controls">
                    <a href={Constants.url_Order_Index} className="btn btn-link">
                        Back to list
                        </a>
                    <ButtonSubmitComponent {...{
                        isSubmitting: this.state.isSubmitting,
                        handleSubmit: () => this.submit()
                    } as IButtonSubmitProps} />
                </div>
            </div>
        );
    }

    renderReadOnlyTextBox(label: string, value: string) {
        return <TextboxComponent {...{
            label,
            value,
            className: "form-group",
            type: "text",
            readOnly: true
        } as ITextboxProps} />;
    }

    renderOrderDetailsSection() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Order details
                </div>
                {
                    this.renderReadOnlyTextBox("Date", dayjs(this.state.order.createdDate).format("DD/MM/YYYY"))
                }
                {
                    this.renderReadOnlyTextBox("Transaction ID", this.state.order.transactionId)
                }
                <SelectComponent {...{
                    options: Object.keys(OrderStatusEnum).filter(x => typeof OrderStatusEnum[x] !== "number").map((value, i) =>
                        <option key={value} value={value}>{this.state.order.statusDescriptions[i]}</option>),
                    value: this.state.order.status ? this.state.order.status.toString() : OrderStatusEnum.Placed.toString(),
                    label: "Status",
                    className: "form-group",
                    handleChangeValue: (e) => this.handleChangeOrderStatus(e)
                } as ISelectProps} />
                <TextareaComponent {...{
                    label: "Comments",
                    type: "text",
                    value: this.state.order.comments,
                    handleChangeValue: (e) => this.handleChangeOrderComments(e)
                } as ITextareaProps} />
            </div>
        );
    }

    renderShippingDetailsSection() {
        const shippingAddress = (this.state.order && this.state.order.shippingAddress) || new OrderAddress();
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Shipping details
                </div>
                {
                    this.renderReadOnlyTextBox("First Name", shippingAddress.addressFirstName)
                }
                {
                    this.renderReadOnlyTextBox("Last Name", shippingAddress.addressLastName)
                }
                {
                    this.renderReadOnlyTextBox("Address line 1", shippingAddress.addressLine1)
                }
                {
                    this.renderReadOnlyTextBox("Address line 2", shippingAddress.addressLine2)
                }
                {
                    this.renderReadOnlyTextBox("City", shippingAddress.city)
                }
                {
                    this.renderReadOnlyTextBox("State/Province", shippingAddress.stateCountyProvince)
                }
                {
                    this.renderReadOnlyTextBox("Post Code", shippingAddress.postcode)
                }
                {
                    this.renderReadOnlyTextBox("Country", shippingAddress.countryName)
                }
                {
                    this.renderReadOnlyTextBox("Phone Number", shippingAddress.phoneNumber)
                }
            </div>
        );
    }

    renderUserDetailsSection() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-controls">
                    <a href={`/Admin/User/Edit?userId=${this.state.order.userId}`} target="_blank" className="btn btn-link">
                        <i className="fa fa-search"></i>
                        Find user
                    </a>
                </div>
                <div className="admin-subsection-title">
                    User details
                </div>
                {
                    this.renderReadOnlyTextBox("First Name", this.state.order.userFirstName)
                }
                {
                    this.renderReadOnlyTextBox("Last Name", this.state.order.userLastName)
                }
                {
                    this.renderReadOnlyTextBox("Email", this.state.order.userEmail)
                }
                {
                    this.renderSendEmails()
                }
            </div>
        );
    }

    renderSendEmails() {
        return (
            <TableComponent {...{
                thList: ["Email", "Status", ""],
                trList: this.state.allEmails
                    .map((email) => [
                        email.name,
                        (this.state.order.sentEmails.filter(x => x.email.id === email.id).length > 0) &&
                        <label className="label label-success">
                            <i className="fa fa-check-circle"></i>
                            &nbsp;Sent
                        </label>,
                        <button data-toggle="modal" data-target="#confirm-send-email-modal" className="btn btn-primary btn-xs" onClick={(e) => this.handleOpenSendEmailConfirmationModal(e, email)}>
                            send
                        </button>])
            } as ITableProps} />
        );
    }

    renderShippingBoxSections() {
        return (
            <div>
                {
                    this.state.order.orderShippingBoxes.map((orderShippingBox, i) =>
                        <div className="admin-subsection" key={orderShippingBox.id}>
                            <div className="admin-subsection-title">
                                Shipping Box
                            </div>
                            {
                                this.renderReadOnlyTextBox("Title", orderShippingBox.title)
                            }
                            {
                                this.renderReadOnlyTextBox("Description", orderShippingBox.description)
                            }
                            {
                                this.renderReadOnlyTextBox("Internal Description", orderShippingBox.internalDescription)
                            }
                            <TextboxComponent {...{
                                label: "Tracking Code",
                                value: orderShippingBox.trackingCode,
                                className: "form-group",
                                type: "text",
                                handleChangeValue: (e) => this.handleChangeOrderShippingBoxTrackingCode(e, orderShippingBox)
                            } as ITextboxProps} />
                            {
                                this.renderReadOnlyTextBox("Shipping Price", this.state.order.currencySymbol + orderShippingBox.shippingPriceAfterTax)
                            }
                            <SelectComponent {...{
                                options: Object.keys(OrderShippingBoxStatusEnum)
                                    .filter(x => typeof OrderShippingBoxStatusEnum[x] !== "number").map((value, i) =>
                                        <option key={value} value={value}>{orderShippingBox.statusDescriptions[i]
                                        }</option>),
                                value: orderShippingBox.status
                                    ? orderShippingBox.status.toString()
                                    : OrderShippingBoxStatusEnum.Preparing.toString(),
                                label: "Status",
                                className: "form-group",
                                handleChangeValue: (e) => this.handleChangeOrderShippingBoxStatus(e, orderShippingBox)
                            } as ISelectProps} />
                            {
                                this.renderOrderItems(orderShippingBox, i)
                            }
                        </div>
                    )}
            </div>
        );
    }

    renderOrderItems(orderShippingBox: OrderShippingBox, index: number) {
        return (
            orderShippingBox.orderItems.map((orderItem, i) =>
                <div key={orderItem.id}>
                    <p className="item-index">
                        {
                            `Item ${i + 1}`
                        }
                    </p>
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "ETA",
                            value: `${dayjs(orderItem.minEta).format("DD/MM/YYYY")} - ${dayjs(orderItem.maxEta).format("DD/MM/YYYY")}`
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Quantity",
                            value: orderItem.productQuantity
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Name",
                            value: orderItem.productName
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Title",
                            value: orderItem.productTitle
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Code",
                            value: orderItem.productCode
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Base Price",
                            value: this.getFormattedPrice(orderItem.productStockUnitBasePrice)
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Sale at Price",
                            value: this.getFormattedPrice(orderItem.productStockUnitSalePrice)
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Membership Sale at Price",
                            value: this.getFormattedPrice(orderItem.productStockUnitMembershipSalePrice)
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Product Stock Unit Release Date",
                            type: "date",
                            value: dayjs(orderItem.productStockUnitReleaseDate).format("DD/MM/YYYY")
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <CheckboxComponent {...{
                            label: "Product Stock Unit Preorder",
                            text: "Enabled",
                            value: orderItem.productStockUnitEnablePreorder,
                            className: "form-group disabled"
                        } as ICheckboxProps} />
                    } as INestedInputProps} />
                    {
                        orderItem.orderItemVariants.map(v => {
                            {
                                // ----------------------------------------------------------------
                                // VariantOption Value
                                // ----------------------------------------------------------------
                            }
                            if (v.variantType === VariantTypesEnum.Options) {
                                return (
                                    <div key={v.id}>
                                        <NestedInputComponent {...{
                                            input: <TextboxComponent {...{
                                                readOnly: true,
                                                label: v.variantLocalizedTitle + " title",
                                                value: v.variantOptionLocalizedTitle
                                            } as ITextboxProps} />
                                        } as INestedInputProps} />
                                        <NestedInputComponent {...{
                                            input: <TextboxComponent {...{
                                                readOnly: true,
                                                label: v.variantLocalizedTitle + " code",
                                                value: v.variantOptionCode
                                            } as ITextboxProps} />
                                        } as INestedInputProps} />
                                        <NestedInputComponent {...{
                                            input: <TextboxComponent {...{
                                                readOnly: true,
                                                label: v.variantLocalizedTitle + " price modifier",
                                                value: this.getFormattedPrice(v.variantOptionLocalizedPriceModifier)
                                            } as ITextboxProps} />
                                        } as INestedInputProps} />
                                    </div>
                                );
                            }
                            {
                                // ----------------------------------------------------------------
                                // Json Value
                                // ----------------------------------------------------------------
                            }
                            if (v.variantType === VariantTypesEnum.Json) {
                                return (
                                    <div key={v.id}>
                                        <NestedInputComponent {...{
                                            input: <TextareaComponent  {...{
                                                label: v.variantLocalizedTitle,
                                                value: JSON.stringify(v.jsonValue.parseJsonAs<any>(), null, 2)
                                            } as ITextareaProps} />
                                        } as INestedInputProps} />
                                    </div>
                                );
                            }
                            {
                                // ----------------------------------------------------------------
                                // Double, Boolean, Integer or String values
                                // ----------------------------------------------------------------
                            }
                            var variantValue: string;
                            switch (v.variantType) {
                                case VariantTypesEnum.Double:
                                    variantValue = v.doubleValue.toString();
                                    break;
                                case VariantTypesEnum.Boolean:
                                    variantValue = v.booleanValue.toString();
                                    break;
                                case VariantTypesEnum.Integer:
                                    variantValue = v.integerValue.toString();
                                    break;
                                default:
                                    variantValue = v.stringValue;
                                    break;
                            }
                            return (
                                <div key={v.id}>
                                    <NestedInputComponent {...{
                                        input: <TextboxComponent {...{
                                            readOnly: true,
                                            label: v.variantLocalizedTitle,
                                            value: variantValue
                                        } as ITextboxProps} />
                                    } as INestedInputProps} />
                                </div>
                            );
                        })
                    }
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Item's subtotal before tax",
                            value: this.getFormattedPrice(orderItem.subtotalBeforeTax)
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Item's subtotal after tax",
                            value: this.getFormattedPrice(orderItem.subtotalAfterTax)
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Item's total before tax",
                            value: this.getFormattedPrice(orderItem.totalBeforeTax)
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <TextboxComponent {...{
                            readOnly: true,
                            label: "Item's total after tax",
                            value: this.getFormattedPrice(orderItem.totalAfterTax)
                        } as ITextboxProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input: <SelectComponent {...{
                            options: Object.keys(OrderItemStatusEnum).filter(x => typeof OrderItemStatusEnum[x] !== "number").map((value, index) =>
                                <option key={value} value={value}>{orderItem.statusDescriptions[index]}</option>),
                            value: orderItem.status ? orderItem.status.toString() : OrderItemStatusEnum.Default.toString(),
                            label: "Status",
                            className: "form-group",
                            handleChangeValue: (e) => this.handleChangeOrderItemStatus(e, orderItem.id)
                        } as ISelectProps} />
                    } as INestedInputProps} />
                    <NestedInputComponent {...{
                        input:
                            <TextboxComponent {...{
                                readOnly: true,
                                label: "Stock Status",
                                value: Functions.getEnumDescription(OrderItemStockStatusEnum, orderItem.stockStatusDescriptions, orderItem.stockStatus),
                                addon: orderItem.stockStatus === OrderItemStockStatusEnum.Decreased
                                    ? <a className="btn btn-xs btn-primary restore-stock-button" href="#" onClick={e => this.handleRestock(e, orderItem.id)}>
                                        Restore stock
                                    </a>
                                    : null
                            } as ITextboxProps} />
                    } as INestedInputProps} />
                </div>
            )
        );
    }

    renderItemsSummarySection() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">
                    Items summary
                </div>
                <OrderSummary
                    order={this.state.order}
                    showCouponCode={true}
                    rowDescriptionItemsSubtotalBeforeTax="Items subtotal before tax:"
                    rowDescriptionCoupon="Coupon:"
                    rowDescriptionTax="Tax:"
                    rowDescriptionShipping="Shipping:"
                    rowDescriptionTotalAfterTax="Total:"
                    rowDescriptionCredit="Credit applied:"
                    rowDescriptionTotalToBePaid="Total paid:"
                />
            </div>
        );
    }

    render() {
        return (
            <div className="edit-order-component">
                {this.renderTopBar()}
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                <ModalComponent {...{
                    id: "confirm-send-email-modal",
                    title: "Send Email",
                    body:
                        <span>
                            <p className="modal-errors"></p>
                            <TextboxComponent {...{
                                label: "Recipient",
                                value: this.state.sendEmailConfirmationModalDetails.sendTo,
                                className: "form-group",
                                type: "text",
                                handleChangeValue: (e) => this.handleChangeSendEmailConfirmationModalDetailsSendTo(e)
                            } as ITextboxProps} />
                        </span>,
                    footer:
                        <span>
                            <button className={`btn btn-primary ${this.state.isSubmitting ? "disabled" : ""}`} onClick={() => this.sendConfirmationEmail()}>
                                {this.state.isSubmitting &&
                                    <i className="fa fa-spinner fa-pulse fa-fw"></i>
                                } &nbsp;Confirm
                            </button>
                            <button type="button" className="btn btn-default" data-dismiss="modal">
                                Back to list
                            </button>
                        </span>
                } as IModalProps} />
                {this.state.pageIsLoading && <SpinnerComponent />}
                {!this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6">
                            {
                                this.renderOrderDetailsSection()
                            }
                            {
                                this.renderShippingDetailsSection()
                            }
                            {
                                this.renderUserDetailsSection()
                            }

                        </div>
                        <div className="col-md-6">
                            {
                                this.renderShippingBoxSections()
                            }
                            {
                                this.renderItemsSummarySection()
                            }
                        </div>
                    </div>
                }
            </div>
        );
    }
}