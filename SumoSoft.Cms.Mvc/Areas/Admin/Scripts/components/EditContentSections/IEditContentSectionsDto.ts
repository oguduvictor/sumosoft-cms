﻿import { IPaginatedListDto } from "./../../interfaces/IPaginatedListDto";
import { IContentSectionDto } from "../../../../../Scripts/interfaces/IContentSectionDto";
import { ICountryDto } from "../../../../../Scripts/interfaces/ICountryDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditContentSectionsDto extends IPaginatedListDto {
    authenticatedUser?: IUserDto;
    contentSections?: Array<IContentSectionDto>;
    countries?: Array<ICountryDto>;
    contentSectionsSchemaNames?: Array<string>;
}