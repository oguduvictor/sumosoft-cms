﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from 'dayjs';

import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { Constants } from "../../shared/Constants";
import { IEditContentSectionsState } from "./IEditContentSectionsState";
import { IEditContentSectionsDto } from "./IEditContentSectionsDto";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { ContentVersion } from "../../../../../Scripts/classes/ContentVersion";
import { IContentVersionDto } from "../../../../../Scripts/interfaces/IContentVersionDto";
import { Functions } from "../../shared/Functions";
import { ContentSection } from "../../../../../Scripts/classes/ContentSection";
import { ContentSectionLocalizedKit } from "../../../../../Scripts/classes/ContentSectionLocalizedKit";
import { Country } from "../../../../../Scripts/classes/Country";
import { ContentSectionLocalizedKitEditor } from "./ContentSectionLocalizedKitEditor/ContentSectionLocalizedKitEditor";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditContentSectionsComponent extends ReactPageComponent<{}, IEditContentSectionsState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            contentVersionsIsLoading: false,
            formErrors: new Array<IFormError>(),
            showContentSectionsName: true,
            showCountries: new Array<string>(),
            filter: "",
            contentSectionToDelete: new ContentSection(),
            contentSections: new Array<ContentSection>(),
            contentSectionsSchemaNames: new Array<string>(),
            countries: new Array<Country>(),
            authenticatedUser: new User({ role: new UserRole() }),
            page: 1,
            pageSize: 1,
            totalPages: 1,
            textareasWithNoWrap: new Array<string>()
        } as IEditContentSectionsState;

        this.handleRemoveContentSection = this.handleRemoveContentSection.bind(this);
        this.handleShowHistory = this.handleShowHistory.bind(this);
        this.handleRestoreContent = this.handleRestoreContent.bind(this);
        this.handleDeleteContentSection = this.handleDeleteContentSection.bind(this);
        this.handleUpdateContentSectionName = this.handleUpdateContentSectionName.bind(this);
        this.handleUpdateContentSectionLocalizedKit = this.handleUpdateContentSectionLocalizedKit.bind(this);
        this.handleCheckCountry = this.handleCheckCountry.bind(this);
        this.handleChangeFilter = this.handleChangeFilter.bind(this);
        this.handleShowContentSectionsName = this.handleShowContentSectionsName.bind(this);
        this.handleShowCheatsheet = this.handleShowCheatsheet.bind(this);
    }

    componentDidMount() {
        this.setState({
            showContentSectionsName: localStorage.getItem("showContentSectionsName") === "true",
            filter: sumoJS.getQueryStringParameter("filter")
        }, () => this.setStateFromApi());

        $(document).on("hidden.bs.modal", $("#edit-content"), e => {
            this.setState({ contentSectionLocalizedKitToEdit: null });
        });
    }

    componentDidUpdate() {
        if (this.state.authenticatedUser.role.editContentSection !== undefined && !this.state.authenticatedUser.role.editContentSection) {
            Functions.toggleFormElementsDisabledState(true);
        }
    }

    handleAddContentSection() {
        if (!this.state.authenticatedUser.role.createContentSection) {
            return;
        }

        const newcontentSection = new ContentSection();

        newcontentSection.name = "";
        newcontentSection.isNew = true;
        newcontentSection.isModified = true;

        newcontentSection.contentSectionLocalizedKits = this.state.countries.map(country => {
            const localizedKit = new ContentSectionLocalizedKit();
            localizedKit.contentSection = { id: newcontentSection.id, isNew: true } as ContentSection;
            localizedKit.country = country;
            //localizedKit.readOnly = !this.state.authenticatedUser.role.editContentSection &&
            //    (!this.state.authenticatedUser.role.editContentSectionAllLocalizedKits ||
            //        this.state.authenticatedUser.country &&
            //        this.state.authenticatedUser.country.id !== country.id);
            localizedKit.content = "";

            return localizedKit;
        });

        const contentSections = this.state.contentSections.slice(); // copy array

        contentSections.unshift(newcontentSection);

        this.setState({ contentSections }, () =>
            this.handleOpenContentSectionLocalizedKitEditor(newcontentSection.contentSectionLocalizedKits.filter(x => x.country.isDefault)[0])
        );
    }

    handleUpdateContentSectionName(value: string, contentSectionId: string) {
        const contentSection = this.state.contentSections.filter(x => x.id === contentSectionId)[0];
        contentSection.name = value;
        contentSection.isModified = true;
        this.setState({ isModified: true, contentSections: this.state.contentSections });
    }

    handleUpdateContentSectionSchema(value: string, contentSectionId: string) {
        const contentSection = this.state.contentSections.filter(x => x.id === contentSectionId)[0];
        contentSection.schema = value;
        contentSection.isModified = true;
        this.setState({ isModified: true, contentSections: this.state.contentSections });
    }

    handleUpdateContentSectionLocalizedKit(value: string, contentSectionLocalizedKit: ContentSectionLocalizedKit) {
        if (value !== contentSectionLocalizedKit.content) {
            const contentSection = this.state.contentSections.filter(x => x.id === contentSectionLocalizedKit.contentSection.id)[0];
            contentSection.isModified = true;

            const localizedKit = contentSection.contentSectionLocalizedKits.filter(x => x.id === contentSectionLocalizedKit.id)[0];
            localizedKit.content = value;

            this.setState({
                isModified: true,
                contentSections: this.state.contentSections
            });
        }
    }

    handleShowHistory(e, localizedKit: ContentSectionLocalizedKit) {
        e.preventDefault();
        this.setState({ contentVersionsIsLoading: true });
        $("#edit-content").modal("hide");
        $("#content-history-modal").modal("show");
        $("#content-history-modal").on('shown.bs.modal', e => {
            $("body").addClass("modal-open");
        });
        sumoJS.ajaxPost<Array<IContentVersionDto>>(Constants.url_ContentSection_GetContentVersions, { contentSectionLocalizedKitId: localizedKit.id }, contentVersions => {
            localizedKit.contentVersions = contentVersions.map(x => new ContentVersion(x));
            this.setState({
                contentSectionLocalizedKitToShowHistory: localizedKit,
                contentVersionsIsLoading: false
            });
        });
    }

    handleUseContentForAllCountries(e, localizedKit: ContentSectionLocalizedKit) {
        const contentSection = this.state.contentSections.filter(x => x.id === localizedKit.contentSection.id)[0];
        contentSection.isModified = true;

        for (let contentSectionLocalizedKit of contentSection.contentSectionLocalizedKits) {
            contentSectionLocalizedKit.content = localizedKit.content;
        }

        this.setState({
            isModified: true,
            contentSections: this.state.contentSections
        });
    }

    handleCopyContentFromCountry(country: Country) {
        const localizedContentSectionToEdit = this.state.contentSectionLocalizedKitToEdit;

        const contentSection = this.state.contentSections.filter(x => x.id === localizedContentSectionToEdit.contentSection.id)[0];
        const countryContentToUse = contentSection.contentSectionLocalizedKits.filter(x => x.country.id === country.id)[0].content;

        localizedContentSectionToEdit.content = countryContentToUse;

        this.setState({
            isModified: true,
            contentSections: this.state.contentSections
        });
    }

    handleClearContentOfNonDefaultCountries(e, localizedKit: ContentSectionLocalizedKit) {
        const contentSection = this.state.contentSections.filter(x => x.id === localizedKit.contentSection.id)[0];
        contentSection.isModified = true;

        const otherCountriesContentSectionLocalizedKits = contentSection.contentSectionLocalizedKits.filter(x => !x.country.isDefault);

        for (let contentSectionLocalizedKit of otherCountriesContentSectionLocalizedKits) {
            contentSectionLocalizedKit.content = "";
        }
        
        this.setState({
            isModified: true,
            contentSections: this.state.contentSections
        });
    }

    handleRestoreContent(e, contentVersion: ContentVersion) {
        this.handleUpdateContentSectionLocalizedKit(contentVersion.content, contentVersion.contentSectionLocalizedKit);
        this.handleSaveContentSectionChanges(contentVersion.contentSectionLocalizedKit);
        $("#content-history-modal").modal("hide");
    }

    handleRemoveContentSection(contentSection: ContentSection) {
        const index = this.state.contentSections.indexOf(contentSection);
        this.state.contentSections.splice(index, 1);

        this.setState({
            isModified: true,
            contentSectionLocalizedKitToEdit: null,
            contentSections: this.state.contentSections
        });
    }

    handleConfirmDeleteContentSection(contentSection: ContentSection) {
        if (!this.state.authenticatedUser.role.deleteContentSection) {
            return;
        }

        this.setState({ contentSectionToDelete: contentSection });
        $("#confirm-delete").modal("show");
    }

    handleShowCheatsheet() {
        $("#markdown-cheatsheet-modal").modal("show");
    }

    handleDeleteContentSection(contentSection: ContentSection) {
        sumoJS.ajaxPost<IFormResponse>(Constants.url_ContentSection_Delete, { id: contentSection.id }, response => {
            if (response.isValid) {
                this.setState({ contentSections: this.state.contentSections.filter(x => x.id !== contentSection.id) });
            } else {
                this.setState({ formErrors: response.errors });
            }
        });
    }

    handleExportContentSections() {
        window.location.href = "/admin/contentsection/exportcsv";
    }

    handleCheckCountry(country: Country) {
        let showCountries = this.state.showCountries;
        if (showCountries.contains(country.languageCode)) {
            showCountries = showCountries.filter(languageCode => languageCode !== country.languageCode);
        } else {
            showCountries.push(country.languageCode);
        }
        localStorage.setItem("showCountries", showCountries.join(","));
        this.setState({ showCountries });
    }

    handleChangeFilter(e) {
        var filter = e.target.value;
        this.setState({
            filter
        }, () => {
            if (!filter) {
                this.setStateFromApi(1);
            }
        });
    }

    handleClickSearchButton() {
        if (this.state.filter.length > 0) {
            this.checkForUnsavedContent(() => {
                this.setState({
                    pageIsLoading: true
                });
                this.setStateFromApi();
            });
        }
    }

    handleKeyPress(e) {
        if (e.key === "Enter" && this.state.filter.length > 0) {
            this.checkForUnsavedContent(() => {
                this.setState({
                    pageIsLoading: true
                });
                this.setStateFromApi(1);
            });
        }
    }

    handleShowContentSectionsName() {
        localStorage.setItem("showContentSectionsName", (!this.state.showContentSectionsName).toString());
        this.setState({ isModified: true, showContentSectionsName: !this.state.showContentSectionsName });
    }

    handleClickNavigationButton(page: number) {
        this.checkForUnsavedContent(() => {
            this.setState({
                pageIsLoading: true
            });
            this.setStateFromApi(page);
        });
    }

    handleOpenContentSectionLocalizedKitEditor(contentSectionLocalizedKitToEdit: ContentSectionLocalizedKit) {
        this.setState(
            {
                contentSectionLocalizedKitToEdit
            },
            () => {
                $("#edit-content").modal("show");
            }
        );
    }

    handleSaveContentSectionChanges(contentSectionLocalizedKit: ContentSectionLocalizedKit) {
        const { contentSections } = this.state;
        const contentSection = contentSections.filter(x => x.id === contentSectionLocalizedKit.contentSection.id)[0];

        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_ContentSection_Save, contentSection, response => {
            $("#edit-content").modal("hide");

            if (response.isValid) {
                this.setStateFromApi(this.state.page);
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    isSubmitting: false,
                    isModified: false
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isSubmitting: false
                });
            }
        });
    }

    checkForUnsavedContent(callback: () => void) {
        if (this.state.isModified || this.state.contentSections.some(c => c.isModified)) {
            if (confirm("Changes that you made may not be saved. Do you really want to leave?")) {
                this.setState(
                    {
                        isModified: false
                    },
                    () => callback()
                );
            }
        } else {
            callback();
        }
    }

    setStateFromApi(page = 1) {
        const apiParameters = {
            page: page,
            filter: this.state.filter
        };
        sumoJS.ajaxPost<IEditContentSectionsDto>(Constants.url_ContentSection_IndexJson, apiParameters, editContentSectionsDto => {
            var showContentSectionsName = localStorage.getItem("showContentSectionsName") !== "false";
            var showCountries = sumoJS.get(() => localStorage.getItem("showCountries").split(","), editContentSectionsDto.countries.map(x => x.languageCode).slice(0, 1));

            this.setState(
                {
                    showContentSectionsName,
                    showCountries,
                    authenticatedUser: new User(editContentSectionsDto.authenticatedUser),
                    countries: editContentSectionsDto.countries.map(x => new Country(x)),
                    contentSections: editContentSectionsDto.contentSections.map(x => new ContentSection(x)),
                    contentSectionsSchemaNames: editContentSectionsDto.contentSectionsSchemaNames,
                    page,
                    pageSize: editContentSectionsDto.pageSize,
                    totalPages: editContentSectionsDto.totalPages,
                    pageIsLoading: false
                },
                () => {
                    // ----------------------------------------------------
                    // Toggle textarea word-wrap based on content
                    // ----------------------------------------------------

                    const textareasWithNoWrap = new Array<string>();

                    this.state.contentSections
                        .mapMany(x => x.contentSectionLocalizedKits)
                        .forEach(localizedKit => {
                            localizedKit.content && localizedKit.content.includesSubstring("|") && textareasWithNoWrap.push(localizedKit.id);
                        });

                    this.setState({
                        textareasWithNoWrap
                    });
                }
            );
        });
    }

    renderContentSectionsTable() {
        return (
            <table>
                <thead>
                    <tr>
                        {this.state.showContentSectionsName && <th className="string-name">Name</th>}
                        {this.state.countries
                            .filter(x => this.state.showCountries.contains(x.languageCode))
                            .map(country => (
                                <th key={country.id}>
                                    <i className={`flag-icon ${country.flagIcon}`} />
                                    &nbsp;
                                    {country.name}
                                </th>
                            ))}
                        {this.state.authenticatedUser.role.deleteContentSection && <th className="remove" />}
                    </tr>
                </thead>
                <tbody>
                    {this.state.contentSections.map(contentSection => this.renderContentSection(contentSection))}
                    {this.state.contentSections.length < 1 && (
                        <tr className="empty-list-message">
                            <td colSpan={this.state.showCountries.length + 1}>No items</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    renderContentSection(contentSection: ContentSection) {
        return (
            <tr key={contentSection.id}>
                {this.state.showContentSectionsName && (
                    <td
                        title={contentSection.name}
                        className="string-name"
                        onClick={() => this.handleOpenContentSectionLocalizedKitEditor(contentSection.contentSectionLocalizedKits.filter(x => x.country.isDefault)[0])}>
                        {contentSection.name}
                    </td>
                )}
                {contentSection.contentSectionLocalizedKits
                    .filter(x => this.state.showCountries.contains(x.country.languageCode))
                    .map(localizedKit => (
                        <td key={localizedKit.id} className="string-content" onClick={() => this.handleOpenContentSectionLocalizedKitEditor(localizedKit)}>
                            <div className="string-content-text">
                                {
                                    sumoJS.get(() => JSON.stringify(JSON.parse(localizedKit.content), null, 6).replace(/^\s{6}|^{\n|\n}$/gm, ""), localizedKit.content)
                                }
                            </div>
                        </td>
                    ))}
                {this.state.authenticatedUser.role.deleteContentSection && contentSection.isNew && (
                    <td className="remove">
                        <button type="button" className="btn btn-warning btn-xs" onClick={() => this.handleRemoveContentSection(contentSection)}>
                            remove
                        </button>
                    </td>
                )}
                {!contentSection.isNew && (
                    <td className="remove">
                        <button
                            type="button"
                            className={`btn btn-danger btn-xs ${this.state.authenticatedUser.role.deleteContentSection ? "" : "disabled"}`}
                            onClick={() => this.handleConfirmDeleteContentSection(contentSection)}
                        >
                            delete
                        </button>
                    </td>
                )}
            </tr>
        );
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">Content Sections</div>
                <div className="admin-top-bar-controls">
                    <div id="keep-this-wrapper">
                        <div className="input-group">
                            <input
                                className="form-control"
                                value={this.state.filter}
                                placeholder="Search..."
                                onChange={e => this.handleChangeFilter(e)}
                                onKeyPress={e => this.handleKeyPress(e)}
                            />
                            <span className="input-group-btn">
                                <button className="btn btn-default" type="button" onClick={() => this.handleClickSearchButton()}>
                                    <i className="fa fa-search" />
                                </button>
                            </span>
                        </div>
                    </div>
                    <div className="btn-group" role="group">
                        <a
                            className={`btn btn-default ${this.state.page <= 1 || this.state.totalPages === 1 ? "disabled" : ""}`}
                            onClick={() => this.handleClickNavigationButton(this.state.page - 1)}
                        >
                            <i className="fa fa-angle-left" aria-hidden="true" />
                        </a>
                        <span className="btn btn-default">{`Page ${this.state.page} / ${this.state.totalPages}`}</span>
                        <a
                            className={`btn btn-default ${this.state.page === this.state.totalPages ? "disabled" : ""}`}
                            onClick={() => this.handleClickNavigationButton(this.state.page + 1)}
                        >
                            <i className="fa fa-angle-right" aria-hidden="true" />
                        </a>
                    </div>
                    <div className="dropdown">
                        <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                            Show columns &nbsp;
                            <span className="caret" />
                        </button>
                        <ul className="dropdown-menu">
                            {this.state.countries.map(country => (
                                <li key={country.id}>
                                    <a href="#">
                                        <label>
                                            <input
                                                type="checkbox"
                                                checked={this.state.showCountries.contains(country.languageCode)}
                                                onChange={e => {
                                                    e.stopPropagation();
                                                    this.handleCheckCountry(country);
                                                }}
                                            />
                                            &nbsp;
                                            {country.name}
                                        </label>
                                    </a>
                                </li>
                            ))}
                            <li role="separator" className="divider" />
                            <li>
                                <a>
                                    <label>
                                        <input type="checkbox" checked={this.state.showContentSectionsName} onChange={() => this.handleShowContentSectionsName()} />
                                        String name
                                    </label>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="dropdown more">
                        <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                            More &nbsp;
                            <span className="caret" />
                        </button>
                        <ul className="dropdown-menu">
                            <li>
                                <a href="#!" onClick={this.handleShowCheatsheet}>
                                    <i className="fa fa-file-text-o icon" />&nbsp;Markdown Cheatsheet
                                </a>
                            </li>
                            <li>
                                <a href="#!" onClick={this.handleExportContentSections}>
                                    <i className="fa fa-external-link icon" />&nbsp;Export
                                </a>
                            </li>
                        </ul>
                    </div>
                    <button
                        className={`btn btn-primary ${this.state.authenticatedUser.role.createContentSection ? "" : "disabled"}`}
                        type="button"
                        onClick={() => this.handleAddContentSection()}
                    >
                        <i className="fa fa-plus" />
                        Add new
                    </button>
                </div>
            </div>
        );
    }

    render() {
        const contentSectionToEdit = sumoJS.get(() => this.state.contentSections.filter(x => x.id === this.state.contentSectionLocalizedKitToEdit.contentSection.id)[0]);
        return (
            <div className="edit-content-sections-component">
                {this.renderTopBar()}
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                <div className="admin-subsection">
                    <div className="table-wrapper">
                        {this.state.pageIsLoading && <SpinnerComponent />}
                        {!this.state.pageIsLoading && this.renderContentSectionsTable()}
                    </div>
                </div>
                <ModalComponent
                    {...{
                        id: "edit-content",
                        title: `Editing Content for ${sumoJS.get(() => this.state.countries.filter(x => x.id === this.state.contentSectionLocalizedKitToEdit.country.id)[0].name, "")}`,
                        body: this.state.contentSectionLocalizedKitToEdit && (
                            <ContentSectionLocalizedKitEditor
                                content={this.state.contentSectionLocalizedKitToEdit.content}
                                contentSectionName={contentSectionToEdit.name}
                                contentSectionSchemaNames={this.state.contentSectionsSchemaNames}
                                contentSectionSchema={contentSectionToEdit.schema}
                                canEditContentSectionName={!Functions.getReadOnlyStateForRequiredField(contentSectionToEdit, this.state.authenticatedUser.role.editContentSectionName)}
                                canEditContentSectionContent={!Functions.getReadOnlyStateForRequiredField(contentSectionToEdit, this.state.authenticatedUser.role.editContentSectionAllLocalizedKits)}
                                handleContentSectionNameChange={value => this.handleUpdateContentSectionName(value, this.state.contentSectionLocalizedKitToEdit.contentSection.id)}
                                handleContentSectionSchemaChange={value => this.handleUpdateContentSectionSchema(value, this.state.contentSectionLocalizedKitToEdit.contentSection.id)}
                                handleContentChange={value => this.handleUpdateContentSectionLocalizedKit(value, this.state.contentSectionLocalizedKitToEdit)}
                            />
                        ),
                        footer: (
                            <span>
                                {this.state.contentSectionLocalizedKitToEdit && !this.state.contentSectionLocalizedKitToEdit.contentSection.isNew && (
                                    <span className="btn dropdown more">
                                        <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                            Options &nbsp;
                                            <span className="caret" />
                                        </button>
                                        <ul className="dropdown-menu">
                                            <li>
                                                <a href="#!" onClick={e => this.handleShowHistory(e, this.state.contentSectionLocalizedKitToEdit)}>
                                                    &nbsp;Show history
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#!" onClick={e => this.handleUseContentForAllCountries(e, this.state.contentSectionLocalizedKitToEdit)}>
                                                    &nbsp;Use this content for all countries
                                                </a>
                                            </li>
                                            {this.state.contentSectionLocalizedKitToEdit.country.isDefault && (
                                                <li>
                                                    <a href="#!" onClick={e => this.handleClearContentOfNonDefaultCountries(e, this.state.contentSectionLocalizedKitToEdit)}>
                                                        &nbsp;Clear content of other countries
                                                    </a>
                                                </li>
                                            )}
                                            {this.state.countries.filter(x => x.id != this.state.contentSectionLocalizedKitToEdit.country.id).map(country => (
                                                <li>
                                                    <a href="#!" onClick={() => this.handleCopyContentFromCountry(country)}>
                                                        &nbsp;Copy content from {country.name}</a>
                                                </li>
                                                ))}
                                        </ul>
                                    </span>
                                )}
                                <a className="btn btn-primary" onClick={() => this.handleSaveContentSectionChanges(this.state.contentSectionLocalizedKitToEdit)}>
                                    Save
                                </a>
                            </span>
                        )
                    } as IModalProps}
                />
                <ModalComponent
                    {...{
                        id: "confirm-delete",
                        title: "Confirmation",
                        body: (
                            <span>
                                Are you sure you want to delete the String "<b>{this.state.contentSectionToDelete.name}</b>"?
                            </span>
                        ),
                        footer: (
                            <span>
                                <a className="btn btn-primary" data-dismiss="modal" onClick={() => this.handleDeleteContentSection(this.state.contentSectionToDelete)}>
                                    {" "}
                                    Confirm
                                </a>
                                <button type="button" className="btn btn-default" data-dismiss="modal">
                                    Cancel
                                </button>
                            </span>
                        )
                    } as IModalProps}
                />
                <ModalComponent
                    {...{
                        id: "markdown-cheatsheet-modal",
                        title: "Markdown Cheatsheet",
                        body: (
                            <div>
                                <h4 className="inner-header">Headers</h4>
                                <pre>
                                    <div># H1</div>
                                    <div>## H2</div>
                                    <div>### H3</div>
                                    <div>#### H4</div>
                                    <div>##### H5</div>
                                    <div>###### H6</div>
                                </pre>
                                <h4 className="inner-header">Links</h4>
                                <pre>
                                    <div>[Link Text](https://www.external-url.com)</div>
                                    <div>[Link Text](/relative-path)</div>
                                    <div>[Link Text](/**/auto-localized-relative-path)</div>
                                </pre>
                                <h4 className="inner-header">Images</h4>
                                <pre>
                                    <div>![alt text](path-to-image.png)</div>
                                </pre>
                                <h4 className="inner-header">Videos</h4>
                                <pre>
                                    <div>!![autoplay loop muted](/path-to-video.mp4)</div>
                                </pre>
                                <h4 className="inner-header">Italic</h4>
                                <pre>
                                    <div>Use single *asterisks* for italics</div>
                                </pre>
                                <h4 className="inner-header">Bold</h4>
                                <pre>
                                    <div>Use double **asterisks** for bold</div>
                                </pre>
                                <h4 className="inner-header">Horizontal line</h4>
                                <pre>
                                    <div>---</div>
                                </pre>
                                <h4 className="inner-header">Slice separator</h4>
                                <pre>
                                    <div>===</div>
                                </pre>
                            </div>
)
} as IModalProps}
/>
<ModalComponent
    {...{
                        id: "content-history-modal",
                        title: "Content History",
                        body: this.state.contentSectionLocalizedKitToShowHistory && <span>
                            {this.state.contentVersionsIsLoading && <SpinnerComponent />}
                            {!this.state.contentVersionsIsLoading && (
                                <table>
                                    <tbody>
                                        {this.state.contentSectionLocalizedKitToShowHistory.contentVersions.length > 0 ? (
                                            this.state.contentSectionLocalizedKitToShowHistory.contentVersions.map((x, i) => (
                                                <tr key={i}>
                                                    <td className="content">{x.content}</td>
                                                    <td className="options">
                                                        {dayjs(x.createdDate).format("DD/MM/YYYY")}
                                                        <br />
                                                        {dayjs(x.createdDate).format("HH:mm")}
                                                        <br />
                                                        <br />
                                                        <button className="btn btn-primary btn-xs" onClick={e => this.handleRestoreContent(e, x)}>
                                                            Restore
                                                    </button>
                                                    </td>
                                                </tr>
                                            ))
                                        ) : (
                                                <tr>
                                                    <td>No previous versions found for this Content</td>
                                                </tr>
                                            )}
                                    </tbody>
                                </table>
                            )}
                        </span>
                    } as IModalProps}
/>
</div>
);
}
}
