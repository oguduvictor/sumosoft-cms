﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { ContentSection } from "../../../../../Scripts/classes/ContentSection";
import { ContentSectionLocalizedKit } from "../../../../../Scripts/classes/ContentSectionLocalizedKit";
import { Country } from "../../../../../Scripts/classes/Country";

export interface IEditContentSectionsState extends IReactPageState {
    filter?: string;
    showContentSectionsName?: boolean;
    /** The list of LanguageCode of the visible countries */ 
    showCountries?: Array<string>;
    contentSectionToDelete?: ContentSection;
    contentSectionLocalizedKitToEdit?: ContentSectionLocalizedKit;
    contentSectionLocalizedKitToShowHistory?: ContentSectionLocalizedKit;
    contentSections?: Array<ContentSection>;
    contentSectionsSchemaNames?: Array<string>;
    countries?: Array<Country>;
    pageIsLoading?: boolean;
    contentVersionsIsLoading?: boolean;
    page?: number;
    pageSize?: number;
    totalPages?: number;
    textareasWithNoWrap: Array<string>;
}