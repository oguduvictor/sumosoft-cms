﻿import * as React from "react";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import * as sumoJS from "../../../../../../Scripts/sumoJS";
import Textarea from "react-textarea-autosize";
import { TextareaComponent } from "../../Textarea/TextareaComponent";
import { TextboxComponent } from "../../Textbox/TextboxComponent";
import { Autocomplete, IAutocompleteProps, AutocompleteOption } from "../../../../../../Scripts/sumoReact/Autocomplete/Autocomplete";

export interface IContentSectionLocalizedKitEditorProps {
    contentSectionName: string;
    content: string;
    contentSectionSchema: string;
    contentSectionSchemaNames: Array<string>;
    canEditContentSectionName: boolean;
    canEditContentSectionContent: boolean;
    handleContentSectionSchemaChange: (schema: string) => void;
    handleContentSectionNameChange: (name: string) => void;
    handleContentChange: (content: string) => void;
}

export interface IContentSectionlocalizedKitEditorState {
    schema?: ISchemaDto;
    editingTableCell: Array<number>;
    editingSchemaItemIndex: number;
}

interface ISchemaDto {
    description: string;
    properties: ISchemaPropertyDto[];
}

interface ISchemaPropertyDto {
    name: string;
    displayName: string;
    displayDescription: string;
    type: "string" | "string[]" | "string[][]";
}

interface IStringAutocomplete {
    new(): Autocomplete<String>;
}

export class ContentSectionLocalizedKitEditor extends React.Component<IContentSectionLocalizedKitEditorProps, IContentSectionlocalizedKitEditorState> {
    constructor(props) {
        super(props);
        this.state = {
            editingTableCell: [0, 0],
            editingSchemaItemIndex: 0
        };
    }

    setStateSchema(props: IContentSectionLocalizedKitEditorProps) {
        if (!props.contentSectionSchema) {
            this.setState({
                schema: null
            });
            return;
        }
        sumoJS.ajaxPost<ISchemaDto>("/Admin/ContentSection/GetSchemaPropertiesJson", { schema: props.contentSectionSchema }, (schema) => {
            this.setState({
                schema
            });
        });
    }

    componentDidMount(): void {
        this.setStateSchema(this.props);
    }

    componentWillReceiveProps(nextProps: IContentSectionLocalizedKitEditorProps): void {
        if (this.props.contentSectionSchema !== nextProps.contentSectionSchema) {
            this.setStateSchema(nextProps);
        }

        if (!this.props.contentSectionSchema && this.props.contentSectionSchemaNames.contains(nextProps.contentSectionName)) {
            this.props.handleContentSectionSchemaChange(nextProps.contentSectionName);
        }
    }

    parseContentJson() {
        let json: {};
        try {
            json = JSON.parse(this.props.content);
        } catch (e) {
            json = {};
        }
        return json;
    }

    handleStringChange(key: string, value: string) {
        const content = this.parseContentJson();
        content[key] = value;
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);
    }

    handleListItemChange(key: string, listItemIndex: number, value: string) {
        const content = this.parseContentJson();
        content[key] = content[key] || ["", ""];
        content[key][listItemIndex] = value;
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);
    }

    handleTableCellChange(key: string, rowIndex: number, columnIndex: number, value: string) {
        const content = this.parseContentJson();
        content[key] = content[key] || [["", ""], ["", ""]];
        content[key][rowIndex][columnIndex] = value;
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);
    }

    handleTableTextareaHeightChange(height: number, schemaItemIndex: number, rowIndex: number) {
        const heights = [height];
        const textareasInRow = $(`[class^='table-textarea-${schemaItemIndex}-${rowIndex}-']`);
        textareasInRow.each((i, e) => {
            const element = $(e);
            if (element.val().toString().length > 0) {
                heights.push(element.height());
            }
        });
        textareasInRow.css("height", Math.max(...heights));
    }

    handleAddListItem(key: string) {
        const content = this.parseContentJson();
        content[key] = content[key] || ["", ""];
        content[key].push("");
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);
    }

    handleRemoveListItem(key: string, listItemIndex: number) {
        const content = this.parseContentJson();
        content[key] = content[key] || ["", ""];
        content[key].splice(listItemIndex, 1);
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);
    }

    handleAddTableRow(key: string) {
        const content = this.parseContentJson();
        content[key] = content[key] || [["", ""], ["", ""]];
        const value = content[key];
        const numberOfColumnsToAdd = value.length > 0 ? value[value.length - 1].length : 1;
        content[key].push(Array.apply(null, Array(numberOfColumnsToAdd)).map(() => ""));
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);
    }

    handleAddTableColumn(key: string) {
        const content = this.parseContentJson();
        content[key] = content[key] || [["", ""], ["", ""]];
        if (content[key].length === 0) {
            this.handleAddTableRow(key);
            return;
        }
        content[key].forEach(x => x.push(""));
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);
    }

    handleRemoveTableRow(key: string) {
        const editingTableCell = this.state.editingTableCell;
        const rowPosition = editingTableCell[0];
        const columnPosition = editingTableCell[1];
        const content = this.parseContentJson();

        content[key] = content[key] || [["", ""], ["", ""]];
        if (content[key].length < 2) return;
        content[key].splice(rowPosition, 1);
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);

        const rowToMoveTo = rowPosition === 0 ? 0 : rowPosition - 1;
        this.setState(
            {
                editingTableCell: [rowToMoveTo, columnPosition]
            },
            () => $(`.table-textarea-${rowToMoveTo}-${columnPosition}`).focus()
        );
    }

    handleRemoveTableColumn(key: string) {
        const editingTableCell = this.state.editingTableCell;
        const rowPosition = editingTableCell[0];
        const columnPosition = editingTableCell[1];
        const content = this.parseContentJson();

        content[key] = content[key] || [["", ""], ["", ""]];
        if (content[key][0].length === 1) return;
        content[key].forEach(x => x.splice(columnPosition, 1));
        const json = JSON.stringify(content);
        this.props.handleContentChange(json);

        const columnToMoveTo = columnPosition === 0 ? 0 : columnPosition - 1;
        this.setState(
            {
                editingTableCell: [rowPosition, columnToMoveTo]
            },
            () => $(`.table-textarea-${rowPosition}-${columnToMoveTo}`).focus()
        );
    }

    handleMoveTableRowUp(key: string) {
        const editingTableCell = this.state.editingTableCell;
        const rowPosition = editingTableCell[0];
        const columnPosition = editingTableCell[1];

        const content = this.parseContentJson();

        const tableContent = content[key] || [["", ""], ["", ""]];

        const rowToMoveTo = rowPosition - 1;

        if (tableContent.length < 2 || rowToMoveTo < 0) return;

        const rowToMove = tableContent[rowPosition];

        tableContent[rowPosition] = tableContent[rowToMoveTo];
        tableContent[rowToMoveTo] = rowToMove;

        const json = JSON.stringify(content);

        this.props.handleContentChange(json);

        this.setState({
            editingTableCell: [rowToMoveTo, columnPosition]
        },
            () => $(`.table-textarea-${rowToMoveTo}-${columnPosition}`).focus()
        );
    }

    handleMoveTableRowDown(key: string) {
        const editingTableCell = this.state.editingTableCell;
        const rowPosition = editingTableCell[0];
        const columnPosition = editingTableCell[1];

        const content = this.parseContentJson();

        const tableContent = content[key] || [["", ""], ["", ""]];

        const rowToMoveTo = rowPosition + 1;

        if (tableContent.length < 2 || rowToMoveTo >= tableContent.length) return;

        const rowToMove = tableContent[rowPosition];

        tableContent[rowPosition] = tableContent[rowToMoveTo];
        tableContent[rowToMoveTo] = rowToMove;

        const json = JSON.stringify(content);

        this.props.handleContentChange(json);

        this.setState({
            editingTableCell: [rowToMoveTo, columnPosition]
        },
            () => $(`.table-textarea-${rowToMoveTo}-${columnPosition}`).focus()
        );
    }

    handleMoveTableColumnRight(key: string) {
        const editingTableCell = this.state.editingTableCell;
        const rowPosition = editingTableCell[0];
        const columnPosition = editingTableCell[1];

        const content = this.parseContentJson();

        const tableContent = content[key] || [["", ""], ["", ""]];

        const rowList = tableContent[rowPosition];

        const columnToMoveTo = columnPosition + 1;

        if (columnToMoveTo >= rowList.length) return;

        const columnToMove = rowList[columnPosition];

        rowList[columnPosition] = rowList[columnToMoveTo];

        rowList[columnToMoveTo] = columnToMove;

        const json = JSON.stringify(content);

        this.props.handleContentChange(json);

        this.setState({
            editingTableCell: [rowPosition, columnToMoveTo]
        },
            () => $(`.table-textarea-${rowPosition}-${columnToMoveTo}`).focus()
        );
    }

    handleMoveTableColumnLeft(key: string) {
        const editingTableCell = this.state.editingTableCell;
        const rowPosition = editingTableCell[0];
        const columnPosition = editingTableCell[1];

        const content = this.parseContentJson();

        const tableContent = content[key] || [["", ""], ["", ""]];

        const rowList = tableContent[rowPosition];

        const columnToMoveTo = columnPosition - 1;

        if (columnToMoveTo < 0) return;

        const columnToMove = rowList[columnPosition];

        rowList[columnPosition] = rowList[columnToMoveTo];

        rowList[columnToMoveTo] = columnToMove;

        const json = JSON.stringify(content);

        this.props.handleContentChange(json);

        this.setState({
            editingTableCell: [rowPosition, columnToMoveTo]
        },
            () => $(`.table-textarea-${rowPosition}-${columnToMoveTo}`).focus()
        );
    }

    render() {
        const {
            contentSectionName,
            contentSectionSchemaNames,
            contentSectionSchema,
            canEditContentSectionName,
            canEditContentSectionContent,
            handleContentSectionSchemaChange
        } = this.props;
        const StringAutocomplete = Autocomplete as IStringAutocomplete;
        return (
            <div className="content-section-localized-kit-editor">
                {
                    this.state.schema &&
                    this.state.schema.description &&
                    <p className="schema-description">{this.state.schema.description}</p>
                }
                <TextboxComponent
                    className="form-group"
                    label="Content Section Name"
                    value={contentSectionName}
                    readOnly={!canEditContentSectionName}
                    handleChangeValue={e => this.props.handleContentSectionNameChange(e.target.value)}
                />
                <StringAutocomplete
                    {...{
                        className: "form-group",
                        label: "Select a schema",
                        placeholder: "No schema",
                        readOnly: !canEditContentSectionName,
                        options: [new AutocompleteOption(null, "No Schema"), ...contentSectionSchemaNames.map(x => new AutocompleteOption<string>(x, x))],
                        selectedOption: contentSectionSchema ? new AutocompleteOption(contentSectionSchema, contentSectionSchema) : new AutocompleteOption(null, "No Schema"),
                        handleChangeValue: value => handleContentSectionSchemaChange(value)
                    } as IAutocompleteProps<string>}
                />
                {
                    this.state.schema &&
                    this.state.schema.properties.map((schemaPropertyDto, schemaPropertyDtoIndex) => {
                        const contentObj = this.parseContentJson();
                        var value;
                        switch (schemaPropertyDto.type) {
                            case "string[]":
                                value = contentObj[schemaPropertyDto.name] || ["", ""];
                                return (
                                    <div key={schemaPropertyDtoIndex} className="content-editor list-editor">
                                        <div className="editor-header">
                                            <div className="editor-description">
                                                <label className="list-editor-key">{schemaPropertyDto.displayName}</label>
                                                {schemaPropertyDto.displayDescription && <label className="list-editor-comment">{schemaPropertyDto.displayDescription}</label>}
                                            </div>
                                            <div className="editor-controls">
                                                <button className="btn btn-link add-list-item" onClick={() => this.handleAddListItem(schemaPropertyDto.name)}>
                                                    <i className="fa fa-plus" /> List Item
                                                </button>
                                            </div>
                                        </div>
                                        {value.length === 0 && <div className="empty-message">No items yet. Click "+ List Item" to begin.</div>}
                                        {value.map((listItem, listItemIndex) => (
                                            <TextareaComponent
                                                className="form-group"
                                                key={listItemIndex}
                                                value={listItem}
                                                readOnly={!canEditContentSectionContent}
                                                addon={
                                                    <button type="button" className="textarea-addon" onClick={() => this.handleRemoveListItem(schemaPropertyDto.name, listItemIndex)}>
                                                        <i className="fa fa-trash-o" />
                                                    </button>
                                                }
                                                handleChangeValue={e => this.handleListItemChange(schemaPropertyDto.name, listItemIndex, e.target.value)}
                                            />
                                        ))}
                                    </div>
                                );
                            case "string[][]":
                                value = contentObj[schemaPropertyDto.name] || [["", ""], ["", ""]];
                                return (
                                    <div key={schemaPropertyDtoIndex} className="content-editor table-editor">
                                        <div className="editor-header">
                                            <div className="editor-description">
                                                <label className="editor-name">{schemaPropertyDto.displayName}</label>
                                                {schemaPropertyDto.displayDescription && <label className="editor-description">{schemaPropertyDto.displayDescription}</label>}
                                            </div>
                                            <div className="editor-controls">
                                                <ContextMenu id={`context-menu-${schemaPropertyDtoIndex}`}>
                                                    <MenuItem onClick={() => this.handleAddTableRow(schemaPropertyDto.name)}>
                                                        <i className="fa fa-plus" /> Add row
                                                    </MenuItem>
                                                    <MenuItem onClick={() => this.handleAddTableColumn(schemaPropertyDto.name)}>
                                                        <i className="fa fa-plus" /> Add column
                                                    </MenuItem>
                                                    <MenuItem onClick={() => this.handleRemoveTableRow(schemaPropertyDto.name)}>
                                                        <i className="fa fa-trash-o" /> Remove row
                                                    </MenuItem>
                                                    <MenuItem onClick={() => this.handleRemoveTableColumn(schemaPropertyDto.name)}>
                                                        <i className="fa fa-trash-o" /> Remove column
                                                    </MenuItem>
                                                    <MenuItem onClick={() => this.handleMoveTableRowUp(schemaPropertyDto.name)}>
                                                        <i className="fa fa-chevron-circle-up" /> Move row up
                                                    </MenuItem>
                                                    <MenuItem onClick={() => this.handleMoveTableRowDown(schemaPropertyDto.name)}>
                                                        <i className="fa fa-chevron-circle-down" /> Move row down
                                                    </MenuItem>
                                                    <MenuItem onClick={() => this.handleMoveTableColumnRight(schemaPropertyDto.name)}>
                                                        <i className="fa fa-chevron-circle-right" /> Move column right
                                                    </MenuItem>
                                                    <MenuItem onClick={() => this.handleMoveTableColumnLeft(schemaPropertyDto.name)}>
                                                        <i className="fa fa-chevron-circle-left" /> Move column left
                                                    </MenuItem>
                                                </ContextMenu>
                                            </div>
                                        </div>
                                        <div className="table-editor-body">
                                            <table cellPadding={0} cellSpacing={0}>
                                                <tbody>
                                                    {value.length === 0 && (
                                                        <tr>
                                                            <td className="empty-message">No rows yet. Click + Row to begin.</td>
                                                        </tr>
                                                    )}
                                                    {value.map((row, rowIndex) => (
                                                        <tr key={rowIndex}>
                                                            {row.map((cellContent, columnIndex) => (
                                                                <td
                                                                    key={columnIndex}
                                                                    className={
                                                                        this.state.editingSchemaItemIndex === schemaPropertyDtoIndex &&
                                                                            this.state.editingTableCell[0] === rowIndex &&
                                                                            this.state.editingTableCell[1] === columnIndex
                                                                            ? "editing"
                                                                            : ""
                                                                    }
                                                                >
                                                                    <ContextMenuTrigger id={`context-menu-${schemaPropertyDtoIndex}`}>
                                                                        <Textarea
                                                                            value={cellContent}
                                                                            minRows={2}
                                                                            maxRows={10}
                                                                            readOnly={!canEditContentSectionContent}
                                                                            className={`table-textarea-${schemaPropertyDtoIndex}-${rowIndex}-${columnIndex}`}
                                                                            onHeightChange={height => this.handleTableTextareaHeightChange(height, schemaPropertyDtoIndex, rowIndex)}
                                                                            onFocus={e =>
                                                                                this.setState({ editingTableCell: [rowIndex, columnIndex], editingSchemaItemIndex: schemaPropertyDtoIndex })
                                                                            }
                                                                            onChange={e => this.handleTableCellChange(schemaPropertyDto.name, rowIndex, columnIndex, e.target.value)}
                                                                            title={`${rowIndex}-${columnIndex}`}
                                                                        />
                                                                    </ContextMenuTrigger>
                                                                </td>
                                                            ))}
                                                        </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                );
                            default:
                                value = contentObj[schemaPropertyDto.name] || "";
                                return (
                                    <div key={schemaPropertyDtoIndex} className="content-editor string-editor">
                                        <div className="editor-header">
                                            <div className="editor-description">
                                                <label className="editor-name">{schemaPropertyDto.displayName}</label>
                                                {schemaPropertyDto.displayDescription && <label className="editor-description">{schemaPropertyDto.displayDescription}</label>}
                                            </div>
                                        </div>
                                        <TextareaComponent
                                            readOnly={!canEditContentSectionContent}
                                            className="form-group"
                                            value={value}
                                            handleChangeValue={e => this.handleStringChange(schemaPropertyDto.name, e.target.value)}
                                        />
                                    </div>
                                );
                        }
                    })
                }
                {!this.state.schema && (
                    <TextareaComponent
                        readOnly={!canEditContentSectionContent}
                        label="Content"
                        className="form-group"
                        value={this.props.content}
                        handleChangeValue={e => this.props.handleContentChange(e.target.value)}
                    />
                )}
            </div>
        );
    }
}
