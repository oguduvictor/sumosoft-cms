﻿import * as React from "react";

import { MultiSelectComponent, SmartOption, IMultiSelectProps } from "../MultiSelect/MultiSelectComponent";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export interface ITagsProps {
    value?: string;
    label?: string;
    className?: string;
    allTags?: Array<string>;
    enableSorting?: boolean;
    handleChangeValue?(newValue: string): void;
    error?: IFormError;
}

export class TagsComponent extends React.Component<ITagsProps, undefined> {

    constructor(props) {
        super(props);
    }
    
    handleAddTag(tags: Array<string>) {
        this.props.handleChangeValue(tags.join(",").toLowerCase());
    }

    render() {
        return (
            <div className={this.props.className}>
                <MultiSelectComponent {...{
                    options: this.props.allTags 
                        ? this.props.allTags.map(x => new SmartOption(x, x)) 
                        : new Array<SmartOption>(),
                    value: this.props.value && this.props.value.length
                        ? this.props.value.split(",")
                        : new Array<string>(),
                    label: "Tags",
                    className: "form-group",
                    forceLowerCase: true,
                    allowCreateOptions: true,
                    enableSorting: this.props.enableSorting,
                    handleChangeValue: (tags) => this.handleAddTag(tags),
                    error: this.props.error
                } as IMultiSelectProps} />
            </div>
        );
    }
}