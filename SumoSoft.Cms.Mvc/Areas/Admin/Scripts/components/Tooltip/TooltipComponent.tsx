﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import { Functions } from "../../shared/Functions";

export interface ITooltipProps {
    title: string;
    placement?: string;
}

export class TooltipComponent extends React.Component<ITooltipProps, undefined> {

    id = Functions.newGuid();

    componentDidMount() {
        $(`#${this.id}`).tooltip({
            title: this.props.title
        });
    }

    componentDidUpdate() {
        $(`#${this.id}`).attr("title", this.props.title).tooltip("fixTitle");
    }

    render() {
        return (
            <i id={this.id} className="fa fa-question-circle tooltip-component" data-toggle="tooltip" data-placement={this.props.placement || "right"}></i>
        );
    }
}