﻿import { IPaginatedListDto } from "./../../interfaces/IPaginatedListDto";
import { IOrderDto } from "../../../../../Scripts/interfaces/IOrderDto";
import { IShippingBoxDto } from "../../../../../Scripts/interfaces/IShippingBoxDto";
import { ICountryDto } from "../../../../../Scripts/interfaces/ICountryDto";

export interface IOrderListDto extends IPaginatedListDto {
    orders: Array<IOrderDto>;
    shippingBoxes: Array<IShippingBoxDto>;
    countries: Array<ICountryDto>;
}