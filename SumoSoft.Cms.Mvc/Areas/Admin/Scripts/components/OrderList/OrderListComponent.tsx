﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { SelectComponent, ISelectProps, ISelectProps as ISelectProps1 } from "../Select/SelectComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { Order } from "../../../../../Scripts/classes/Order";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";
import { Constants } from "../../shared/Constants";
import { Country } from "../../../../../Scripts/classes/Country";
import { IOrderListState } from "./IOrderListState";
import { IOrderListDto } from "./IOrderListDto";
import { OrderStatusEnum } from "../../../../../Scripts/enums/OrderStatusEnum";
import { Functions } from "../../shared/Functions";

export default class OrderListComponent extends React.Component<{}, IOrderListState> {

    state = {
        orders: new Array<Order>(),
        countries: new Array<Country>(),
        page: 1,
        totalPages: 1
    } as IOrderListState;

    constructor(props) {
        super(props);

        this.handleChangeStatusFilter = this.handleChangeStatusFilter.bind(this);
        this.handleChangeKeywordFilter = this.handleChangeKeywordFilter.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount() {
        this.setStateFromApi(1);
    }

    handleChangeStatusFilter(e) {
        this.setState({ orderStatusValueFilter: e.target.value }, () => {
            this.setStateFromApi(1);
        });
    }

    handleChangeKeywordFilter(e) {

        const value = e.target.value;
        this.setState({ keywordFilter: e.target.value }, () => {
            if (!value) {
                this.setStateFromApi(1);
            }
        });
    }

    handleKeyPress(e) {
        if (e.key === "Enter") {
            this.setStateFromApi(1);
        }
    }

    handleChangeCountryFilter(e) {
        this.setState({ countryNameFilter: e.target.value }, () => {
            this.setStateFromApi(1);
        });
    }

    setStateFromApi(page = 1) {
        this.setState({ pageIsLoading: true });

        sumoJS.ajaxPost<IOrderListDto>(Constants.url_Order_IndexJson,
            {
                keywordFilter: this.state.keywordFilter,
                orderStatusValueFilter: this.state.orderStatusValueFilter,
                countryNameFilter: this.state.countryNameFilter,
                page: page
            },
            (orderListDto) => {
                this.setState({
                    orders: orderListDto.orders.map(x => new Order(x)),
                    countries: orderListDto.countries.map(x => new Country(x)),
                    page,
                    totalPages: orderListDto.totalPages,
                    pageIsLoading: false
                });
            });
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar-controls">
                <div id="keep-this-wrapper">
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="Search..."
                            value={this.state.keywordFilter || ""}
                            onChange={(e) => this.handleChangeKeywordFilter(e)}
                            onKeyPress={(e) => this.handleKeyPress(e)} />
                        <span className="input-group-btn">
                            <button className="btn btn-default" type="button" onClick={() => this.setStateFromApi(1)}>
                                <i className="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <div className="btn-group" role="group">
                    <a className={`btn btn-default ${(this.state.page <= 1 || this.state.totalPages === 1) ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page - 1))}>
                        <i className="fa fa-angle-left" aria-hidden="true"></i>
                    </a>
                    <span className="btn btn-default">
                        {`Page ${this.state.page} / ${this.state.totalPages}`}
                    </span>
                    <a className={`btn btn-default ${this.state.page === this.state.totalPages ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page + 1))}>
                        <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </a>
                </div>
                <SelectComponent {...{
                    neutralOption: <option value="">All countries</option>,
                    options: this.state.countries.map(x => <option key={x.id} value={x.name}>{x.name}</option>),
                    value: this.state.countryNameFilter,
                    className: "no-bottom-margin all-countries",
                    handleChangeValue: (e) => this.handleChangeCountryFilter(e)
                } as ISelectProps} />
                <SelectComponent {...{
                    neutralOption: <option value="">All statuses</option>,
                    options: Object.keys(OrderStatusEnum).filter(orderStatus => !isNaN(Number(OrderStatusEnum[orderStatus]))).map((orderStatus, i) =>
                        <option key={i} value={OrderStatusEnum[orderStatus]}>{orderStatus}</option>),
                    value: this.state.orderStatusValueFilter,
                    className: "no-bottom-margin",
                    handleChangeValue: (e) => this.handleChangeStatusFilter(e)
                } as ISelectProps1} />
            </div>
        );
    }

    render() {
        return (
            <div className="order-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Orders
                    </div>
                    {
                        this.renderTopBar()
                    }
                </div>
                <div className="admin-subsection">
                    <div className="table-wrapper">
                        {!this.state.pageIsLoading &&
                            <TableComponent {...{
                                thList: ["#", "Date", "User", "Country", "Status", "Revenue", ""],
                                trList: this.state.orders
                                    .map((x) => [
                                        x.orderNumber.toString(),
                                        dayjs(x.createdDate).format("DD/MM/YYYY"),
                                        x.userFirstName + " " + x.userLastName,
                                        x.shippingAddress.countryName ? x.shippingAddress.countryName : x.countryName,
                                        Functions.getEnumDescription(OrderStatusEnum, x.statusDescriptions, x.status),
                                        x.currencySymbol + " " + x.totalAfterTax.toFixed(2),
                                        <a className="btn btn-primary btn-xs" href={Constants.url_Order_Edit + x.id}>open</a>])
                            } as ITableProps} />
                        }
                        {this.state.pageIsLoading && <SpinnerComponent />}
                    </div>
                </div>
            </div>
        );
    }
}
