﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Country } from "../../../../../Scripts/classes/Country";
import { Order } from "../../../../../Scripts/classes/Order";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";

export interface IOrderListState extends IReactPageState {
    orders?: Array<Order>;
    countries?: Array<Country>;
    keywordFilter?: string;
    orderStatusValueFilter?: string;
    countryNameFilter?: string;
    page?: number;
    totalPages?: number;
}