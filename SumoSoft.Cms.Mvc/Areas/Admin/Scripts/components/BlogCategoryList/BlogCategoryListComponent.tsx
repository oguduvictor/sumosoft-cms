﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { TableComponent, ITableProps } from "../Table/TableComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { BlogCategory } from "../../../../../Scripts/classes/BlogCategory";
import { Constants } from "../../shared/Constants";
import { IBlogCategoryListState } from "./IBlogCategoryListState";
import { IBlogCategoryListDto } from "./IBlogCategoryListDto";

export default class BlogCategoryListComponent extends React.Component<{}, IBlogCategoryListState> {

    state = {
        pageIsLoading: false,
        blogCategories: new Array<BlogCategory>()
    } as IBlogCategoryListState;

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        sumoJS.ajaxPost<IBlogCategoryListDto>(Constants.url_BlogCategories_IndexJson, null, categoryList => {
            this.setState({
                pageIsLoading: false,
                blogCategories: categoryList.blogCategories.map(x => new BlogCategory(x))
            });
        });
    }

    render() {
        return (
            <div className="blog-category-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Blog Categories
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_BlogCategories_Edit} className="btn btn-primary">
                            <i className="fa fa-plus"></i>
                            Create new category
                        </a>
                    </div>
                </div>
                <div className="admin-subsection">
                    <div className="table-wrapper">
                        <TableComponent {...{
                            thList: ["Name", "Localized Title", "Blog Posts", "#"],
                            trList: this.state.blogCategories
                                .map((x) => [
                                    x.name,
                                    x.localizedTitle,
                                    x.blogPosts.length,
                                    <a className="btn btn-primary btn-xs" href={Constants.url_BlogCategories_Edit + x.id}>edit</a>
                            ])
                        } as ITableProps} />
                        {this.state.pageIsLoading && <SpinnerComponent />}
                    </div>
                </div>
            </div>
        );
    }
}