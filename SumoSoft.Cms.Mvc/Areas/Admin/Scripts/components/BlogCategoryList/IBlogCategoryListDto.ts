﻿import { IBlogCategoryDto } from "../../../../../Scripts/interfaces/IBlogCategoryDto";

export interface IBlogCategoryListDto {
    blogCategories: Array<IBlogCategoryDto>;
}