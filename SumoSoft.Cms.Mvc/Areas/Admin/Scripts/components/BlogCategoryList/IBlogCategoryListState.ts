﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { BlogCategory } from "../../../../../Scripts/classes/BlogCategory";

export interface IBlogCategoryListState extends IReactPageState {
    blogCategories?: Array<BlogCategory>;
}