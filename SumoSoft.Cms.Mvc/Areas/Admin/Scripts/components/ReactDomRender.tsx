﻿import * as React from "react";
import * as ReactDOM from "react-dom";
import withErrorBoundary from './ErrorBoundary/ErrorBoundary';

declare function _import(path: string): Promise<any>;

// --------------------------------------------------------------------------------
// Note:
// The initial set of chunks is created when the _import function is executed
// for the first time. The parameter `./${$(element).data("component-path")}.tsx`
// is initially equal to "./.tsx", which is equivalent to "./**/*.tsx".
// Documentation at https://webpack.js.org/api/module-methods/#import
// --------------------------------------------------------------------------------

$("[data-component-path]").each((i, element) => {
    _import(
        `./${$(element).data("component-path")}.tsx`
    ).then(componentFile => {
        const Component = componentFile.default;
        const ComponentWithErrorBoundary = withErrorBoundary(Component);
        ReactDOM.render(<ComponentWithErrorBoundary />, element);
    }).catch(error => `An error occurred while trying to render the Component: ${error}`);
});