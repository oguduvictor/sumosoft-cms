﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { DirectoryDto } from "../../../../../Scripts/classes/DirectoryDto";
import {FileDto} from "./FileDto";
import {ResizeFile} from "./ResizeFile";
import {BatchRenameFilesDto} from "./BatchRenameFilesDto";

export interface IMediaState extends IReactPageState {
    selectAll?: boolean;
    newFileName?: string;
    newDirectoryName?: string;
    name?: string;
    localPath?: string;
    creationTime?: string;
    size?: number;
    keywordFilter?: string;
    isRoot?: boolean;
    isAddingNewDirectory?: boolean;
    isBatchRenamingFiles?: boolean;
    isBackingUpMedia?: boolean;
    isDeletingDirectory?: boolean;
    isDeletingFiles?: boolean;
    isRenamingFiles?: boolean;
    isResizingFiles?: boolean;
    isUploadingFile?: boolean;
    parent?: DirectoryDto;
    files?: Array<FileDto>;
    directories?: Array<DirectoryDto>;
    directoryToDelete?: DirectoryDto;
    filesToDelete?: Array<FileDto>;
    fileToRename?: FileDto;
    resizeFileDto?: ResizeFile;
    batchRenameFilesDto?: BatchRenameFilesDto;
}