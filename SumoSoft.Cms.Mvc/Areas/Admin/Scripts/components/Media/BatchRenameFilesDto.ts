﻿export class BatchRenameFilesDto {
    directoryLocalPath: string;
    localPaths: Array<string>;
    replaceText: string;
    replaceTextTarget: string;
    prefixText: string;
    suffixText: string;
}