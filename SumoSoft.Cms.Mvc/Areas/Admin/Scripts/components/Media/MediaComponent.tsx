﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { ModalComponent, IModalProps } from "../Modal/ModalComponent";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { DirectoryDto } from "../../../../../Scripts/classes/DirectoryDto";
import { Constants } from "../../shared/Constants";
import { IDirectoryDto } from "../../../../../Scripts/interfaces/IDirectoryDto";
import { IMediaState } from "./IMediaState";
import { ResizeFile } from "./ResizeFile";
import { FileDto } from "./FileDto";
import { BatchRenameFilesDto } from "./BatchRenameFilesDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { MediaThumbnail} from "../MediaThumbnail/MediaThumbnail";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class MediaComponent extends ReactPageComponent<{}, IMediaState> {

    constructor(props) {
        super(props);

        this.state = {
            selectAll: false,
            newFileName: "",
            isRoot: true,
            newDirectoryName: "",
            name: "",
            localPath: "",
            keywordFilter: "",
            parent: new DirectoryDto(),
            files: new Array<FileDto>(),
            directories: new Array<DirectoryDto>(),
            directoryToDelete: new DirectoryDto(),
            filesToDelete: new Array<FileDto>(),
            fileToRename: new FileDto(),
            resizeFileDto: new ResizeFile(),
            batchRenameFilesDto: new BatchRenameFilesDto()
        } as IMediaState;

        this.handleUpdateNewDirectoryName = this.handleUpdateNewDirectoryName.bind(this);
        this.handleConfirmDeleteDirectory = this.handleConfirmDeleteDirectory.bind(this);
        this.handleSelectOrDeselectAll = this.handleSelectOrDeselectAll.bind(this);
        this.handleSetNewFileName = this.handleSetNewFileName.bind(this);
        this.handleOpenResizeFileModal = this.handleOpenResizeFileModal.bind(this);
        this.handleUpdateResizeFileWidth = this.handleUpdateResizeFileWidth.bind(this);
        this.handleUpdateResizeFileCompression = this.handleUpdateResizeFileCompression.bind(this);
        this.handleUpdateResizeFileKeepOriginal = this.handleUpdateResizeFileKeepOriginal.bind(this);
        this.handleOpenRenameFileModal = this.handleOpenRenameFileModal.bind(this);
        this.handleConfirmDeleteFiles = this.handleConfirmDeleteFiles.bind(this);
        this.handleUploadFiles = this.handleUploadFiles.bind(this);
    }

    componentDidMount() {
        this.setState({ pageIsLoading: true });
        this.setStateFromApi();
    }

    getSelectedFiles() {
        return this.state.files.filter(file => file.isSelected);
    }

    handleUpdateNewDirectoryName(e) {
        this.setState({ newDirectoryName: e.target.value });
    }

    handleAddNewDirectory() {

        this.setState({
            isAddingNewDirectory: true
        });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Media_NewDirectoryForm, { name: this.state.newDirectoryName, localPath: this.state.localPath },
            response => {
                if (response.isValid) {
                    $("#new-directory").modal("hide");
                    window.location.href = response.redirectUrl;
                } else {
                    this.setState({
                        isAddingNewDirectory: false,
                        formErrors: response.errors
                    });
                }
            });
    }

    handleConfirmDeleteDirectory(directoryToDelete: DirectoryDto) {
        this.setState({ directoryToDelete });
        $("#directory-delete").modal("show");
    }

    handleDeleteDirectory() {

        this.setState({
            isDeletingDirectory: true
        });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Media_DeleteDirectoryForm, { localPath: this.state.directoryToDelete.localPath }, response => {
            this.setStateFromApi();

            $("#directory-delete").modal("hide");

            if (response.isValid) {
                this.setState({
                    isDeletingDirectory: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isDeletingDirectory: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleSelectOrDeselectFile(e, file: FileDto) {
        file.isSelected = e.target.checked;
        this.setState({
            files: this.state.files,
            selectAll: this.state.files.length === this.getSelectedFiles().length
        });
    }

    handleSelectOrDeselectAll(e) {
        $.each(this.state.files, (i, file) => {
            file.isSelected = e.target.checked;
        });

        this.setState({ files: this.state.files, selectAll: e.target.checked });
    }

    handleSetNewFileName(e) {
        this.setState({ newFileName: e.target.value });
    }

    handleOpenResizeFileModal(file?: FileDto) {
        this.state.resizeFileDto.localPaths = file ? new Array(file.localPath) : this.getSelectedFiles().filter(x => x.extension.toLowerCase() === ".jpg" || x.extension.toLowerCase() === ".jpeg").map(x => x.localPath);
        this.setState({ resizeFileDto: this.state.resizeFileDto });
        $("#file-resize").modal("show");
    }

    handleUpdateResizeFileWidth(e) {
        this.state.resizeFileDto.width = e.target.value;
        this.setState({ resizeFileDto: this.state.resizeFileDto });
    }

    handleUpdateResizeFileCompression(e) {
        this.state.resizeFileDto.compression = Number(e.target.value);
        this.setState({ resizeFileDto: this.state.resizeFileDto });
    }

    handleUpdateResizeFileKeepOriginal(e) {
        this.state.resizeFileDto.keepOriginal = e.target.checked;
        this.setState({ resizeFileDto: this.state.resizeFileDto });
    }

    handleResizeFiles() {

        this.setState({
            isResizingFiles: true
        });

        this.state.resizeFileDto.destination = this.state.localPath;

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Media_ResizeFilesForm, this.state.resizeFileDto, response => {
            this.setStateFromApi();

            $("#file-resize").modal("hide");

            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setState({
                    selectAll: false,
                    isResizingFiles: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    selectAll: false,
                    isResizingFiles: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleConfirmDeleteFiles(fileToDelete?: FileDto) {
        this.setState({ filesToDelete: (fileToDelete ? new Array(fileToDelete) : this.getSelectedFiles()) });
        $("#file-delete").modal("show");
    }

    handleDeleteFiles() {

        this.setState({
            isDeletingFiles: true
        });

        sumoJS.ajaxPost<IFormResponse<Array<string>>>(
            Constants.url_Media_DeleteFilesForm,
            {
                localPaths: this.state.filesToDelete.map(x => x.localPath)
            }, response => {
                this.setStateFromApi();

                $("#file-delete").modal("hide");

                if (response.isValid) {
                    this.setState({
                        selectAll: false,
                        isDeletingFiles: false,
                        formSuccessMessage: Constants.alert_saveSuccess
                    });
                } else {
                    this.setState({
                        selectAll: false,
                        isDeletingFiles: false,
                        formErrors: response.errors
                    });
                }
            });
    }

    handleUploadFiles(e) {
        const formData = new FormData();
        const files = e.target.files;

        let totalSize = 0;

        for (let i = 0; i < files.length; i++) {
            totalSize = totalSize + files[i].size;
        }

        if (totalSize / 1024 / 1024 > 100) {
            this.setState({
                formErrors: [{ message: "The files you're trying to upload exceed the maximum upload size (100 MB)" } as IFormError]
            });
        } else {
            formData.append("directoryFullName", this.state.localPath);

            for (let i = 0; i < files.length; i++) {
                formData.append(files[i].name, files[i]);
            }

            this.setState({
                isUploadingFile: true
            });

            Functions.ajaxUploadFile(formData, Constants.url_Media_UploadFileForm, response => {
                this.setStateFromApi();

                if (response.isValid) {
                    this.setState({
                        formSuccessMessage: Constants.alert_saveSuccess,
                        isUploadingFile: false
                    });
                } else {
                    this.setState({
                        formErrors: response.errors,
                        isUploadingFile: false
                    });
                }
            });
        }
    }

    handleOpenRenameFileModal(file: FileDto) {
        this.setState({
            newFileName: file.name,
            fileToRename: file
        });
        $("#file-rename").modal("show");
    }

    handleRenameFile() {

        this.setState({
            isRenamingFiles: true
        });

        const localPath = this.state.fileToRename.localPath;

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Media_RenameFileForm, { localPath, newName: this.state.newFileName },
            response => {
                if (response.isValid) {
                    this.setStateFromApi();

                    $("#file-rename").modal("hide");

                    this.setState({
                        isRenamingFiles: false,
                        formSuccessMessage: Constants.alert_saveSuccess
                    });
                } else {
                    this.setState({
                        isRenamingFiles: false,
                        formErrors: response.errors
                    });
                }
            }
        );
    }

    handleUpdateBatchRenameFilesReplaceText(e) {
        const batchRenameFilesDto = sumoJS.deepClone(this.state.batchRenameFilesDto);
        batchRenameFilesDto.replaceText = e.target.value;

        this.setState({
            batchRenameFilesDto
        });
    }

    handleUpdateBatchRenameFilesReplaceTextTarget(e) {
        const batchRenameFilesDto = sumoJS.deepClone(this.state.batchRenameFilesDto);
        batchRenameFilesDto.replaceTextTarget = e.target.value;

        this.setState({
            batchRenameFilesDto
        });
    }

    handleUpdateBatchRenameFilesPrefixText(e) {
        const batchRenameFilesDto = sumoJS.deepClone(this.state.batchRenameFilesDto);
        batchRenameFilesDto.prefixText = e.target.value;

        this.setState({
            batchRenameFilesDto
        });
    }

    handleUpdateBatchRenameFilesSuffixText(e) {
        const batchRenameFilesDto = sumoJS.deepClone(this.state.batchRenameFilesDto);
        batchRenameFilesDto.suffixText = e.target.value;

        this.setState({
            batchRenameFilesDto
        });
    }

    handleBatchRenameFiles() {
        this.setState({
            isBatchRenamingFiles: true
        });

        const dto = sumoJS.deepClone(this.state.batchRenameFilesDto);
        dto.directoryLocalPath = this.state.localPath;
        dto.localPaths = this.getSelectedFiles().map(x => x.localPath);

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Media_BatchRenameFiles, { dto },
            response => {
                this.setStateFromApi();

                if (response.isValid) {
                    $("#file-batch-rename").modal("hide");
                    this.setState({
                        formSuccessMessage: Constants.alert_saveSuccess,
                        isBatchRenamingFiles: false,
                        batchRenameFilesDto: new BatchRenameFilesDto()
                    });
                } else {
                    this.setState({
                        formErrors: response.errors,
                        isBatchRenamingFiles: false
                    });
                }
            }
        );
    }

    handleBackupMedia() {
        this.setState({
            isBackingUpMedia: true
        });


        sumoJS.ajaxPost<IFormResponse>(Constants.url_Media_Backup,
            {
                localPath: sumoJS.getQueryStringParameter("localPath")
            },
            response => {
                if (response.isValid) {
                    this.setState({
                        formSuccessMessage: Constants.alert_saveSuccess,
                        isBackingUpMedia: false,
                    });
                } else {
                    this.setState({
                        formErrors: response.errors,
                        isBackingUpMedia: false
                    });
                }
            }
        );
    };

    handleSearchTextBoxKeyPress(e) {
        if (e.key === "Enter") {
            this.handleFileSearch();
        }
    }

    handleChangeKeywordFilter(e) {
        const filter = e.target.value.trim();

        this.setState({
            keywordFilter: filter
        }, () => {
            if (!filter) {
                this.setStateFromApi();
            }
        });
    }

    handleFileSearch() {
        this.setState({
            pageIsLoading: true
        });

        this.setStateFromApi();
    }

    setStateFromApi() {

        sumoJS.ajaxPost<IDirectoryDto>(Constants.url_Media_IndexJson,
            {
                localPath: sumoJS.getQueryStringParameter("localPath"),
                keywordFilter: this.state.keywordFilter
            },
            (directoryDto) => {

                this.setState({
                    pageIsLoading: false,
                    isRoot: directoryDto.isRoot,
                    name: !this.state.keywordFilter ? directoryDto.localPath : `Files/Folders matching "${this.state.keywordFilter}" in "${directoryDto.localPath}" and its sub-directories`,
                    localPath: directoryDto.localPath,
                    creationTime: directoryDto.creationTime,
                    size: directoryDto.size,
                    parent: new DirectoryDto(directoryDto.parent),
                    files: directoryDto.files.map(x => new FileDto(x)),
                    directories: directoryDto.directories.map(x => new DirectoryDto(x))
                });

            });
    }

    render() {
        return (
            <div className="media-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Media
                    </div>
                    <div className="admin-top-bar-controls">
                        {
                            !this.state.isRoot &&
                            <a href={Constants.url_Media_Index + this.state.parent.localPath} className="btn btn-link">
                                <i className="fa fa-chevron-left"></i>
                                Back
                            </a>
                        }
                        <div>
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Search..."
                                       onChange={(e) => this.handleChangeKeywordFilter(e)}
                                       onKeyPress={(e) => this.handleSearchTextBoxKeyPress(e)}/>
                                <span className="input-group-btn">
                                    <button className="btn btn-default" type="button" onClick={() => this.handleFileSearch()}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        {
                            this.state.localPath.indexOf(`/${Constants.media_backups_folderName}`) === -1
                                ? <a href="#" className="btn btn-default" onClick={() => this.handleBackupMedia()}>
                                    {
                                        this.state.isBackingUpMedia &&
                                        <i className="fa fa-spinner fa-pulse fa-fw"></i>
                                    }
                                    Backup Folder
                                  </a>
                                : <a className="btn btn-default disabled" >
                                    Backup Folder
                                  </a>
                        }
                        <div className="dropdown">
                            <a className={`dropdown-toggle btn btn-default ${this.getSelectedFiles().length < 1 ? "disabled" : ""}`} data-toggle="dropdown">
                                Bulk Edit
                                <span className="caret"></span>
                            </a>
                            <ul className="dropdown-menu">
                                <li>
                                    <a href="#" data-toggle="modal" onClick={() => this.handleConfirmDeleteFiles()}>
                                        Delete
                                    </a>
                                </li>
                                <li>
                                    {
                                        this.state.localPath.indexOf(`/${Constants.media_backups_folderName}`) === -1 &&
                                        <a href="#" data-toggle="modal" onClick={() => this.handleOpenResizeFileModal()}>
                                            Resize
                                        </a>
                                    }
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#file-batch-rename">
                                        Rename
                                    </a>
                                </li>
                            </ul>
                        </div>
                        {
                            this.state.localPath.indexOf(`/${Constants.media_backups_folderName}`) === -1
                                ? <form method="post" encType="multipart/form-data" className="file-upload-form">
                                    <input type="file" ref="file" value="" name="files" multiple accept="image/*,application/pdf,text/plain,video/mp4" className="btn btn-default" id="file-upload" onChange={this.handleUploadFiles} />
                                    {
                                        this.state.isUploadingFile && <span className="file-upload-spinner"><i className="fa fa-spinner fa-pulse fa-fw"></i></span>
                                    }
                                </form>
                                : <input type="file" value="" className="btn btn-default" disabled />
                        }
                        {
                            this.state.localPath.indexOf(`/${Constants.media_backups_folderName}`) === -1
                                ? <a href="#" className="btn btn-primary" data-toggle="modal" data-target="#new-directory">
                                    <i className="fa fa-folder"></i>
                                    New folder
                                  </a>
                                : <a className="btn btn-primary disabled">
                                    <i className="fa fa-folder"></i>
                                    New folder
                                  </a>
                        }
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps}/>
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <div className="col-md-12 local-path">
                            {
                                this.state.name
                            }
                        </div>
                        <table className="table table-striped table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>Preview</th>
                                    <th>Name</th>
                                    <th>Size</th>
                                    <th></th>
                                    <th>Date</th>
                                    <th></th>
                                    <th>
                                        <label>
                                            <input type="checkbox" className="select-all" checked={this.state.selectAll} onChange={this.handleSelectOrDeselectAll} />
                                            Select all
                                        </label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.directories.map((directory) =>
                                        <tr key={directory.localPath}>
                                            <td>
                                                <a href={Constants.url_Media_Index + directory.localPath}>
                                                    <img src="/Areas/Admin/Content/img/folder.png" alt="" />
                                                </a>
                                            </td>
                                            <td>
                                                {
                                                    directory.name
                                                }
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                {dayjs(directory.creationTime).format("DD/MM/YYYY")}
                                            </td>
                                            <td>
                                                <div className="dropdown">
                                                    <button className={`btn btn-default btn-xs dropdown-toggle ${directory.name === Constants.media_backups_folderName ? "disabled" : ""}`} type="button" data-toggle="dropdown">
                                                        edit
                                                        <span className="caret"></span>
                                                    </button>
                                                    <ul className="dropdown-menu">
                                                        {
                                                            directory.name !== Constants.media_backups_folderName &&
                                                            <li>
                                                                <a href="#" data-toggle="modal" onClick={() => this.handleConfirmDeleteDirectory(directory)}>
                                                                    Delete
                                                                </a>
                                                            </li>
                                                        }
                                                    </ul>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                    )}
                                {
                                    this.state.files.map((file) =>
                                        <tr key={file.localPath}>
                                            <td>
                                                <a href={file.absolutePathWithVersion} target="_blank">
                                                    <MediaThumbnail file={file} />
                                                </a>
                                            </td>
                                            <td>
                                                {
                                                    file.name + file.extension
                                                }
                                            </td>
                                            <td>
                                                {
                                                    Math.round(file.size / 1024)
                                                }
                                                KB
                                            </td>
                                            <td>
                                                {
                                                    (file.extension !== ".pdf" && file.size / 1024 > file.heavyLoadingFileSize) &&
                                                    <label className="label label-warning">
                                                        Heavy loading
                                                    </label>
                                                }
                                            </td>
                                            <td>
                                                {dayjs(file.creationTime).format("DD/MM/YYYY")}
                                            </td>
                                            <td>
                                                <div className="dropdown">
                                                    <button className="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                                        edit
                                                        <span className="caret"></span>
                                                    </button>
                                                    <ul className="dropdown-menu">
                                                        <li>
                                                            <a href="#" data-toggle="modal" onClick={() => this.handleOpenRenameFileModal(file)}>
                                                                Rename
                                                            </a>
                                                        </li>
                                                        {
                                                            (file.extension.toLowerCase() === ".jpg" || file.extension.toLowerCase() === ".jpeg") &&
                                                            <li>
                                                                <a href="#" data-toggle="modal" onClick={() => this.handleOpenResizeFileModal(file)}>
                                                                    Resize
                                                                </a>
                                                            </li>
                                                        }
                                                        <li>
                                                            <a href="#" data-toggle="modal" onClick={() => this.handleConfirmDeleteFiles(file)}>
                                                                Delete
                                                        </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="checkbox" checked={file.isSelected} onChange={(e) => this.handleSelectOrDeselectFile(e, file)} />
                                            </td>
                                        </tr>
                                    )}

                                {
                                    !(this.state.directories.length > 0 || this.state.files.length > 0) &&
                                    <tr className="empty-list-message">
                                        <td colSpan={7}>
                                            No items
                                            </td>
                                    </tr>
                                }
                            </tbody>
                        </table>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
                <ModalComponent {...{
                    id: "new-directory",
                    title: "Create a new folder",
                    body:
                        <TextboxComponent {...{
                            label: "New folder name",
                            value: this.state.newDirectoryName,
                            placeholder: "...",
                            className: "form-group",
                            type: "text",
                            handleChangeValue: (e) => this.handleUpdateNewDirectoryName(e)
                        } as ITextboxProps} />,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                text: "Save",
                                isSubmitting: this.state.isAddingNewDirectory,
                                handleSubmit: () => this.handleAddNewDirectory()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        </span>
                } as IModalProps}/>
                <ModalComponent {...{
                    id: "directory-delete",
                    title: "Delete directory",
                    body: <span>Are you sure you want to delete this directory?</span>,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                text: "Delete",
                                isSubmitting: this.state.isDeletingDirectory,
                                handleSubmit: () => this.handleDeleteDirectory()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        </span>
                } as IModalProps}/>
                <ModalComponent {...{
                    id: "file-delete",
                    title: "Delete file",
                    body: <span>Are you sure you want to delete {this.state.filesToDelete.length > 1 ? "these files?" : "this file?"}</span>,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                text: "Delete",
                                isSubmitting: this.state.isDeletingFiles,
                                handleSubmit: () => this.handleDeleteFiles()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        </span>
                } as IModalProps}/>
                <ModalComponent {...{
                    id: "file-rename",
                    title: "Rename file",
                    body:
                        <TextboxComponent {...{
                            label: "New name",
                            value: this.state.newFileName,
                            placeholder: "...",
                            className: "form-group",
                            type: "text",
                            handleChangeValue: (e) => this.handleSetNewFileName(e)
                        } as ITextboxProps} />,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                text: "Rename",
                                isSubmitting: this.state.isRenamingFiles,
                                handleSubmit: () => this.handleRenameFile()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        </span>
                } as IModalProps}/>
                <ModalComponent {...{
                    id: "file-resize",
                    title: this.getSelectedFiles().length > 1 ? "Resize files" : "Resize file",
                    body:
                        <span>
                            <div className="row">
                                <div className="col-sm-6">
                                    <TextboxComponent {...{
                                        label: "Size",
                                        value: this.state.resizeFileDto.width,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: (e) => this.handleUpdateResizeFileWidth(e)
                                    } as ITextboxProps} />
                                </div>
                                <div className="col-sm-6">
                                    <SelectComponent {...{
                                        options: [
                                            <option key={"Minimum"} value={"50"}> Minimum </option>,
                                            <option key={"Low"} value={"60"}> Low </option>,
                                            <option key={"Medium"} value={"70"}> Medium </option>,
                                            <option key={"High"} value={"80"}> High </option>,
                                            <option key={"Maximum"} value={"90"}> Maximum </option>
                                        ],
                                        value: this.state.resizeFileDto.compression.toString(),
                                        label: "Quality",
                                        className: "form-group",
                                        handleChangeValue: (e) => this.handleUpdateResizeFileCompression(e)
                                    } as ISelectProps} />
                                </div>
                            </div>
                            <CheckboxComponent{...{
                                text: "Keep the original file and save a new image",
                                value: this.state.resizeFileDto.keepOriginal,
                                className: "form-group",
                                handleChangeValue: (e) => this.handleUpdateResizeFileKeepOriginal(e)
                            } as ICheckboxProps} />
                        </span>,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                text: "Resize",
                                isSubmitting: this.state.isResizingFiles,
                                handleSubmit: () => this.handleResizeFiles()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        </span>
                } as IModalProps}/>
                <ModalComponent {...{
                    id: "file-batch-rename",
                    title: "Bulk Rename files",
                    body:
                        <span>
                            <div className="row">
                                <div className="col-sm-6">
                                    <TextboxComponent {...{
                                        label: "Text To Replace",
                                        value: this.state.batchRenameFilesDto.replaceText,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: (e) => this.handleUpdateBatchRenameFilesReplaceText(e)
                                    } as ITextboxProps} />
                                </div>
                                <div className="col-sm-6">
                                    <TextboxComponent {...{
                                        label: "Replace Text With",
                                        value: this.state.batchRenameFilesDto.replaceTextTarget,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: (e) => this.handleUpdateBatchRenameFilesReplaceTextTarget(e)
                                    } as ITextboxProps} />
                                </div>
                                <div className="col-sm-6">
                                    <TextboxComponent {...{
                                        label: "Add Prefix",
                                        value: this.state.batchRenameFilesDto.prefixText,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: (e) => this.handleUpdateBatchRenameFilesPrefixText(e)
                                    } as ITextboxProps} />
                                </div>
                                <div className="col-sm-6">
                                    <TextboxComponent {...{
                                        label: "Add Suffix",
                                        value: this.state.batchRenameFilesDto.suffixText,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: (e) => this.handleUpdateBatchRenameFilesSuffixText(e)
                                    } as ITextboxProps} />
                                </div>
                            </div>
                        </span>,
                    footer:
                        <span>
                            <ButtonSubmitComponent {...{
                                text: "Bulk Rename",
                                isSubmitting: this.state.isBatchRenamingFiles,
                                handleSubmit: () => this.handleBatchRenameFiles()
                            } as IButtonSubmitProps} />
                            <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                        </span>
                } as IModalProps}/>
            </div>
        );
    }
}