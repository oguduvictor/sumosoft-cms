﻿export class ResizeFile {
    localPaths = new Array<string>();
    width: number;
    compression = 80;
    destination: string;
    keepOriginal = true;
}