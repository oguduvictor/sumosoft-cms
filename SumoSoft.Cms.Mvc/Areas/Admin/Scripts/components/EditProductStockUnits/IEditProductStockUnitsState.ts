﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";
import { ProductStockUnit } from '../../../../../Scripts/classes/ProductStockUnit';

export interface IEditProductStockUnitsState extends IReactPageState {
    productId?: string;
    productTitle?: string;
    variants?: Array<Variant>;
    productStockUnits?: Array<ProductStockUnit>;
    pageIsLoading?: boolean;
    canSaveEdits?: boolean;
}