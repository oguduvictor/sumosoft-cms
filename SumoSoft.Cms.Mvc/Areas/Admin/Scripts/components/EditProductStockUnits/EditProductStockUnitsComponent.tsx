﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as sumoSignalR from "../../../../../Scripts/sumoSignalR";
import * as dayjs from "dayjs";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { Variant } from "../../../../../Scripts/classes/Variant";
import { Constants } from "../../shared/Constants";
import { IEditProductStockUnitsState } from "./IEditProductStockUnitsState";
import { ProductStockUnit } from "../../../../../Scripts/classes/ProductStockUnit";
import { IEditProductStockUnitsDto } from "./IEditProductStockUnitsDto";
import { Product } from "../../../../../Scripts/classes/Product";
import { VariantOption } from "../../../../../Scripts/classes/VariantOption";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { ProductStockUnitLocalizedKit } from "../../../../../Scripts/classes/ProductStockUnitLocalizedKit";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

export default class EditProductStockUnitsComponent extends ReactPageComponent<{}, IEditProductStockUnitsState> {

    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            pageIsLoading: true,
            canSaveEdits: true,
            formErrors: new Array<IFormError>(),
            variants: new Array<Variant>(),
            productStockUnits: new Array<ProductStockUnit>()
        } as IEditProductStockUnitsState;
    }

    productId = sumoJS.getQueryStringParameter("productId");

    componentDidMount() {
        this.setStateFromApi();
        sumoSignalR.initializeSignalR(signalR => {
            const proxy = sumoSignalR.createHubProxy(signalR, Constants.hub_Product);

            proxy.on(Constants.hubEvent_Product_QueryIfEditingProductStockUnit, (productId: string, connectionId: string) => this.queryIfEditingProductStockUnit(productId, connectionId));
            proxy.on(Constants.hubEvent_Product_ShowMultipleEditingProductStockUnitWarning, () => this.showMultipleEditingProductStockUnitWarning());
            proxy.on(Constants.hubEvent_Product_RefreshEditingProductStockUnitData, (productId: string) => this.refreshEditingProductStockUnitData(productId));

            signalR.hub.start().done(() => {
                sumoSignalR.invoke(
                    Constants.hub_Product,
                    Constants.hubMethod_Product_BeginEditingProductStockUnit,
                    this.productId
                );
            });
        });
    }

    queryIfEditingProductStockUnit(productId: string, connectionId: string) {
        if (this.productId === productId) {
            sumoSignalR.invoke(
                Constants.hub_Product,
                Constants.hubMethod_Product_OnMultipleEditingProductStockUnit,
                connectionId
            );
        }
    }

    showMultipleEditingProductStockUnitWarning() {
        this.setState({
            formErrors: [{ message: Constants.alert_multiple_editing_productStockUnit } as IFormError],
            canSaveEdits: false
        });
    }

    refreshEditingProductStockUnitData(productId: string) {
        if (this.productId === productId) {
            this.setStateFromApi();
        }
    }

    handleChangeProductStockUnitStock(e, productStockUnit: ProductStockUnit) {
        productStockUnit.stock = e.target.value;
        productStockUnit.isModified = true;
        this.setState({ isModified: true, productStockUnits: this.state.productStockUnits });
    }

    handleChangeProductStockUnitEnablePreorder(e, productStockUnit: ProductStockUnit) {
        productStockUnit.enablePreorder = e.target.checked;
        productStockUnit.isModified = true;
        this.setState({ isModified: true, productStockUnits: this.state.productStockUnits });
    }

    handleChangeProductStockUnitDispatchTime(e, productStockUnit: ProductStockUnit) {
        productStockUnit.dispatchTime = e.target.value;
        productStockUnit.isModified = true;
        this.setState({ isModified: true, productStockUnits: this.state.productStockUnits });
    }

    handleChangeProductStockUnitDispatchDate(e, productStockUnit: ProductStockUnit) {
        productStockUnit.dispatchDate = e.target.value;
        productStockUnit.enablePreorder = new Date(e.target.value) > new Date() && productStockUnit.enablePreorder;
        productStockUnit.isModified = true;
        this.setState({ isModified: true, productStockUnits: this.state.productStockUnits });
    }

    handleChangeProductStockUnitCode(e, productStockUnit: ProductStockUnit) {
        productStockUnit.code = e.target.value;
        productStockUnit.isModified = true;
        this.setState({ isModified: true, productStockUnits: this.state.productStockUnits });
    }

    handleRemoveProductStockUnit(productStockUnit: ProductStockUnit) {
        productStockUnit.isDeleted = true;
        this.setState({
            isModified: true,
            productStockUnits: this.state.productStockUnits
        });
    }

    handleToggleLocalizedKits() {
        $(".product-stock-units-table th:nth-last-child(2)").slideToggle();
        $(".product-stock-units-table td:nth-last-child(2)").slideToggle();
        $(".product-stock-units-table th:nth-last-child(3)").slideToggle();
        $(".product-stock-units-table td:nth-last-child(3)").slideToggle();
        $(".product-stock-units-table th:nth-last-child(4)").slideToggle();
        $(".product-stock-units-table td:nth-last-child(4)").slideToggle();
        $(".product-stock-units-table th:nth-last-child(5)").slideToggle();
        $(".product-stock-units-table td:nth-last-child(5)").slideToggle();
    }

    handleChangeProductStockUnitLocalizedKitBasePrice(e, localizedKit: ProductStockUnitLocalizedKit, productStockUnit: ProductStockUnit) {
        localizedKit.basePrice = e.target.value;
        productStockUnit.isModified = true;
        this.setState({
            isModified: true,
            productStockUnits: this.state.productStockUnits
        });
    }

    handleChangeProductStockUnitLocalizedKitSalePrice(e, localizedKit: ProductStockUnitLocalizedKit, productStockUnit: ProductStockUnit) {
        localizedKit.salePrice = e.target.value;
        productStockUnit.isModified = true;
        this.setState({
            isModified: true,
            productStockUnits: this.state.productStockUnits
        });
    }

    handleChangeProductStockUnitLocalizedKitMembershipSalePrice(e, localizedKit: ProductStockUnitLocalizedKit, productStockUnit: ProductStockUnit) {
        localizedKit.membershipSalePrice = e.target.value;
        productStockUnit.isModified = true;
        this.setState({
            isModified: true,
            productStockUnits: this.state.productStockUnits
        });
    }

    handleUpdateAllStockUnitsBasePrices(productStockUnit: ProductStockUnit) {
        const productStockUnits = sumoJS.deepClone(this.state.productStockUnits);
        productStockUnits.forEach(psu => {
            psu.productStockUnitLocalizedKits.forEach(localizedKit => {
                const matchedLocalizedKitToUse = productStockUnit.productStockUnitLocalizedKits.filter(x => x.country && localizedKit.country && x.country.id === localizedKit.country.id)[0];
                if (matchedLocalizedKitToUse) {
                    localizedKit.basePrice = matchedLocalizedKitToUse.basePrice;
                    psu.isModified = true;
                }
            });
        });
        this.setState({
            isModified: true,
            productStockUnits
        });
    }

    handleUpdateAllStockUnitsSalePrices(productStockUnit: ProductStockUnit) {
        const productStockUnits = sumoJS.deepClone(this.state.productStockUnits);
        productStockUnits.forEach(psu => {
            psu.productStockUnitLocalizedKits.forEach(localizedKit => {
                const matchedLocalizedKitToUse = productStockUnit.productStockUnitLocalizedKits.filter(x => x.country && localizedKit.country && x.country.id === localizedKit.country.id)[0];
                if (matchedLocalizedKitToUse) {
                    localizedKit.salePrice = matchedLocalizedKitToUse.salePrice;
                    psu.isModified = true;
                }
            });
        });
        this.setState({
            isModified: true,
            productStockUnits
        });
    }

    handleUpdateAllStockUnitsMembershipSalePrices(productStockUnit: ProductStockUnit) {
        const productStockUnits = sumoJS.deepClone(this.state.productStockUnits);
        productStockUnits.forEach(psu => {
            psu.productStockUnitLocalizedKits.forEach(localizedKit => {
                const matchedLocalizedKitToUse = productStockUnit.productStockUnitLocalizedKits.filter(x => x.country && localizedKit.country && x.country.id === localizedKit.country.id)[0];
                if (matchedLocalizedKitToUse) {
                    localizedKit.membershipSalePrice = matchedLocalizedKitToUse.membershipSalePrice;
                    psu.isModified = true;
                }
            });
        });
        this.setState({
            isModified: true,
            productStockUnits
        });
    }

    handleRemoveProductStockUnitSalePrices(productStockUnit: ProductStockUnit) {
        productStockUnit.productStockUnitLocalizedKits.forEach(x => {
            x.salePrice = 0;
        });
        productStockUnit.isModified = true;

        this.setState({
            isModified: true,
            productStockUnits: this.state.productStockUnits
        });
    }

    handleRemoveMembershipSalePrice(productStockUnit: ProductStockUnit) {
        productStockUnit.productStockUnitLocalizedKits.forEach(x => {
            x.membershipSalePrice = 0;
        });
        productStockUnit.isModified = true;

        this.setState({
            isModified: true,
            productStockUnits: this.state.productStockUnits
        });
    }

    submit() {
        // simplify objects before turning them into json
        const productStockUnits = sumoJS.deepClone(this.state.productStockUnits.filter(x => x.isModified || x.isDeleted));

        productStockUnits.forEach(x => x.product = new Product({ id: x.product.id }));
        productStockUnits.forEach(x => x.variantOptions = x.variantOptions.map(variantOption => new VariantOption({ id: variantOption.id })));

        const model = {
            productId: this.state.productId,
            productTitle: this.state.productTitle,
            productStockUnits
        } as IEditProductStockUnitsDto;

        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Product_AddOrUpdateProductStockUnits, model, response => {

            if (response.isValid) {
                this.setStateFromApi();
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
                sumoSignalR.invoke(
                    Constants.hub_Product,
                    Constants.hubMethod_Product_SaveEditingProductStockUnit,
                    this.state.productId
                );
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi() {
        sumoJS.ajaxPost<IEditProductStockUnitsDto>(Constants.url_Product_EditProductStockUnitsJson,
            {
                productId: sumoJS.getQueryStringParameter("productId")
            },
            (editProductStockUnitsDto) => {
                this.setState({
                    authenticatedUser: new User(editProductStockUnitsDto.authenticatedUser),
                    productId: editProductStockUnitsDto.productId,
                    productTitle: editProductStockUnitsDto.productTitle,
                    variants: editProductStockUnitsDto.variants.map(x => new Variant(x)),
                    productStockUnits: editProductStockUnitsDto.productStockUnits.map(x => {
                        x.enablePreorder = new Date(x.dispatchDate) > new Date() && x.enablePreorder;
                        return new ProductStockUnit(x);
                    }),
                    pageIsLoading: false
                }, () => Functions.setCreateAndEditPermissions(
                    new Product(),
                    this.state.authenticatedUser.role.createProduct,
                    this.state.authenticatedUser.role.editProduct));
            });
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">
                    Edit Stock Units for {this.state.productTitle}
                </div>
                <div className="admin-top-bar-controls">
                    <a href={Constants.url_Product_Index} className="btn btn-link">
                        Back to list
                    </a>
                    <div className="dropdown">
                        <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                            Edit...
                            &nbsp;
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href={Constants.url_Product_Edit + this.state.productId}>
                                    Product
                                </a>
                            </li>
                            <li>
                                <a href={Constants.url_Product_EditProductAttributes + this.state.productId}>
                                    Product Attributes
                                </a>
                            </li>
                            <li>
                                <a href={Constants.url_Product_EditProductVariants + this.state.productId}>
                                    Product Variants
                                </a>
                            </li>
                            <li>
                                <a href={Constants.url_Product_EditProductImageKits + this.state.productId}>
                                    Product Images
                                </a>
                            </li>
                            <li className="disabled">
                                <a>
                                    Product Stock
                                </a>
                            </li>
                        </ul>
                    </div>
                    <button type="button" className="btn btn-default" data-toggle="button" onClick={() => this.handleToggleLocalizedKits()}>
                        Hide prices
                    </button>
                    <ButtonSubmitComponent {...{
                        className: this.state.canSaveEdits ? "" : "disabled",
                        isSubmitting: this.state.isSubmitting,
                        handleSubmit: () => this.state.canSaveEdits && this.submit()
                    } as IButtonSubmitProps} />
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="edit-product-stock-units-component">
                {
                    this.renderTopBar()
                }
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    this.state.pageIsLoading ||
                    <div className="admin-subsection">
                        <table className="table product-stock-units-table">
                            <thead>
                                <tr>
                                    {
                                        this.state.variants.map(variant =>
                                            <th key={variant.id}>
                                                {variant.localizedTitle}
                                            </th>)
                                    }
                                    <th>
                                        Stock
                                    </th>
                                    <th>
                                        Dispatch date
                                    </th>
                                    <th>
                                        Preorder
                                    </th>
                                    <th>
                                        Dispatch Time
                                    </th>
                                    <th>
                                        Code
                                    </th>
                                    <th>
                                        Country
                                    </th>
                                    <th className="base-price-header">
                                        Base Price
                                    </th>
                                    <th className="sale-at-price-header">
                                        Sale Price
                                    </th>
                                    <th className="membership-sale-at-price-header">
                                        Membership Sale Price
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.productStockUnits.filter(x => !x.isDeleted).map(productStockUnit =>
                                        <tr key={productStockUnit.id}>
                                            {
                                                this.state.variants.map(variant => {
                                                    var variantOption = productStockUnit.variantOptions.filter(x => x.variant.id === variant.id)[0];
                                                    if (variantOption) {
                                                        return (
                                                            <td key={variant.id}>
                                                                <span className={productStockUnit.isDisabled ? "disabled" : ""}>
                                                                    {
                                                                        variantOption.localizedTitle
                                                                    }
                                                                </span>
                                                            </td>
                                                        );
                                                    }
                                                    else {
                                                        return <td key={variant.id}>
                                                            {
                                                                productStockUnit.isDisabled &&
                                                                <label className="label label-warning">missing</label>
                                                            }
                                                        </td>;
                                                    }
                                                })
                                            }
                                            <td className="stock">
                                                <input
                                                    type="number"
                                                    value={productStockUnit.stock || 0}
                                                    readOnly={productStockUnit.isDisabled}
                                                    className={productStockUnit.isDisabled ? "disabled" : ""}
                                                    onChange={(e) => this.handleChangeProductStockUnitStock(e, productStockUnit)} />
                                            </td>
                                            <td className="dispatch-date">
                                                <input
                                                    type="date"
                                                    value={dayjs(productStockUnit.dispatchDate).format("YYYY-MM-DD")}
                                                    readOnly={productStockUnit.isDisabled}
                                                    className={productStockUnit.isDisabled ? "disabled" : ""}
                                                    onChange={(e) => this.handleChangeProductStockUnitDispatchDate(e, productStockUnit)} />
                                            </td>
                                            <td className="preorder">
                                                <input
                                                    type="checkbox"
                                                    title={new Date(productStockUnit.dispatchDate) <= new Date() ? "To enable Preorder, the Release Date must be greater than today's date" : ""}
                                                    disabled={productStockUnit.isDisabled || new Date(productStockUnit.dispatchDate) <= new Date()}
                                                    checked={productStockUnit.enablePreorder}
                                                    className={productStockUnit.isDisabled ? "checkbox-ui disabled" : "checkbox-ui"}
                                                    onChange={(e) => this.handleChangeProductStockUnitEnablePreorder(e, productStockUnit)} />
                                            </td>
                                            <td className="dispatch-time">
                                                <input
                                                    type="number"
                                                    min="0"
                                                    value={productStockUnit.dispatchTime || 0}
                                                    readOnly={productStockUnit.isDisabled}
                                                    className={productStockUnit.isDisabled ? "disabled" : ""}
                                                    onChange={(e) => this.handleChangeProductStockUnitDispatchTime(e, productStockUnit)} />
                                                <span className={productStockUnit.isDisabled ? "disabled" : ""}>days</span>
                                            </td>
                                            <td className="code">
                                                <input
                                                    type="text"
                                                    placeholder="..."
                                                    value={productStockUnit.code}
                                                    key="{productStockUnit.id}"
                                                    readOnly={productStockUnit.isDisabled}
                                                    className={productStockUnit.isDisabled ? "disabled" : ""}
                                                    onChange={(e) => this.handleChangeProductStockUnitCode(e, productStockUnit)} />
                                            </td>
                                            <td className="country">
                                                <table className="inner-table">
                                                    <tbody>
                                                        {
                                                            productStockUnit.productStockUnitLocalizedKits.map(localizedKit =>
                                                                <tr key={localizedKit.id}>
                                                                    <td className={productStockUnit.isDisabled ? "disabled" : ""}>
                                                                        {
                                                                            localizedKit.country
                                                                                ? localizedKit.country.name
                                                                                : "Country Name"
                                                                        }
                                                                    </td>
                                                                </tr>)
                                                        }
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td className="base-price">
                                                <table className="inner-table">
                                                    <tbody>
                                                        {
                                                            productStockUnit.productStockUnitLocalizedKits.map(localizedKit =>
                                                                <tr key={localizedKit.id}>
                                                                    <td>
                                                                        <input
                                                                            type="number"
                                                                            placeholder="0"
                                                                            value={localizedKit.basePrice || 0}
                                                                            readOnly={productStockUnit.isDisabled}
                                                                            className={productStockUnit.isDisabled ? "disabled" : ""}
                                                                            onChange={(e) => this.handleChangeProductStockUnitLocalizedKitBasePrice(e, localizedKit, productStockUnit)}
                                                                        />
                                                                    </td>
                                                                </tr>)
                                                        }
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td className="sale-at-price">
                                                <table className="inner-table">
                                                    <tbody>
                                                        {
                                                            productStockUnit.productStockUnitLocalizedKits.map(localizedKit =>
                                                                <tr key={localizedKit.id}>
                                                                    <td>
                                                                        <input
                                                                            type="number"
                                                                            placeholder="0"
                                                                            value={localizedKit.salePrice || 0}
                                                                            readOnly={productStockUnit.isDisabled}
                                                                            className={productStockUnit.isDisabled ? "disabled" : ""}
                                                                            onChange={(e) => this.handleChangeProductStockUnitLocalizedKitSalePrice(e, localizedKit, productStockUnit)}
                                                                        />
                                                                    </td>
                                                                </tr>)
                                                        }
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td className="membership-sale-at-price">
                                                <table className="inner-table">
                                                    <tbody>
                                                        {
                                                            productStockUnit.productStockUnitLocalizedKits.map(localizedKit =>
                                                                <tr key={localizedKit.id}>
                                                                    <td>
                                                                        <input
                                                                            type="number"
                                                                            placeholder="0"
                                                                            value={localizedKit.membershipSalePrice || 0}
                                                                            readOnly={productStockUnit.isDisabled}
                                                                            className={productStockUnit.isDisabled ? "disabled" : ""}
                                                                            onChange={(e) => this.handleChangeProductStockUnitLocalizedKitMembershipSalePrice(e, localizedKit, productStockUnit)}
                                                                        />
                                                                    </td>
                                                                </tr>)
                                                        }
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td className="options">
                                                {
                                                    productStockUnit.isDisabled ?
                                                        <span>
                                                            <p className="text-warning small">
                                                                Disabled because obsolete
                                                            </p>
                                                            <a className="btn btn-xs btn-danger"
                                                                style={{ display: "block" }}
                                                                onClick={() => { this.handleRemoveProductStockUnit(productStockUnit) }}>
                                                                remove
                                                            </a>
                                                        </span> :
                                                        <div className="dropdown">
                                                            <button className="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                                                Options
                                                            </button>
                                                            <ul className="dropdown-menu">
                                                                <li>
                                                                    <a onClick={() => this.handleUpdateAllStockUnitsBasePrices(productStockUnit)}>
                                                                        Use these Base Prices for all the Stock Units
                                                                    </a>
                                                                    <a onClick={() => this.handleUpdateAllStockUnitsSalePrices(productStockUnit)}>
                                                                        Use these Sale Prices for all the Stock Units
                                                                    </a>
                                                                    <a onClick={() => this.handleUpdateAllStockUnitsMembershipSalePrices(productStockUnit)}>
                                                                        Use these Membership Sale Prices for all the Stock Units
                                                                    </a>
                                                                    <a onClick={() => this.handleRemoveProductStockUnitSalePrices(productStockUnit)}>
                                                                        Remove Sale Prices
                                                                    </a>
                                                                    <a onClick={() => this.handleRemoveProductStockUnit(productStockUnit)}>
                                                                        Remove Stock Unit
                                                                    </a>
                                                                    <a onClick={() => this.handleRemoveMembershipSalePrice(productStockUnit)}>
                                                                        Remove Membership Sale Prices
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                }
                                            </td>
                                        </tr>)
                                }
                            </tbody>
                        </table>
                    </div>
                }
                {this.state.pageIsLoading && <SpinnerComponent />}
            </div>
        );
    }
}