﻿import { IProductStockUnitDto } from "../../../../../Scripts/interfaces/IProductStockUnitDto";
import { IVariantDto } from "../../../../../Scripts/interfaces/IVariantDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditProductStockUnitsDto {
    authenticatedUser?: IUserDto;
    productId?: string;
    productStockUnits?: Array<IProductStockUnitDto>;
    variants?: Array<IVariantDto>;
    productTitle?: string;
}
