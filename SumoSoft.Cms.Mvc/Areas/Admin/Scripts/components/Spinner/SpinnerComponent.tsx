﻿import * as React from "react";

import { Constants } from "../../shared/Constants";

export var SpinnerComponent: () => JSX.Element = () => {
    return (
        <span className="spinner-component">
            <img src={Constants.url_Content_Img_Loading} className="loading" alt="Loading..." />
        </span>
    );
}