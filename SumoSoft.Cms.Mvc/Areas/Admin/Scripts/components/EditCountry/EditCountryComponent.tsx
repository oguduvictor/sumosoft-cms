﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as sumoSignalR from "../../../../../Scripts/sumoSignalR";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Constants } from "../../shared/Constants";
import { Country } from "../../../../../Scripts/classes/Country";
import { IUpdateAllPricesDto } from "./IUpdateAllPricesDto";
import { IEditCountryState } from "./IEditCountryState";
import { IEditCountryDto } from "./IEditCountryDto";
import { ITaxDto } from "../../../../../Scripts/interfaces/ITaxDto";
import { Tax } from "../../../../../Scripts/classes/Tax";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Functions } from "../../shared/Functions";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { Autocomplete, AutocompleteOption } from "../../../../../Scripts/sumoReact/Autocomplete/Autocomplete";
import { IUpdateProductStockUnitSalePricesDto } from "./IUpdateProductStockUnitSalePricesDto";
import { IUpdateProductStockUnitMembershipSalePricesDto } from "./IUpdateProductStockUnitMembershipSalePricesDto";
import { IVoidProductStockUnitSalePricesDto } from "./IVoidProductStockUnitSalePricesDto";
import { IVoidProductStockUnitMembershipSalePricesDto } from "./IVoidProductStockUnitMembershipSalePricesDto";
import { Category } from "../../../../../Scripts/classes/Category";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

interface IStringAutocomplete {
    new(): Autocomplete<String>;
}

export default class EditCountryComponent extends ReactPageComponent<{}, IEditCountryState> {
    constructor(props) {
        super(props);

        this.state = {
            authenticatedUser: new User({ role: new UserRole() }),
            formErrors: new Array<IFormError>(),
            country: new Country(),
            cultures: new Array<string>(),
            flagIcons: new Array<string>(),
            allLocalizedCountries: new Array<Country>(),
            allCategories: new Array<Category>(),
            updateAllPricesDto: {} as IUpdateAllPricesDto,
            updateProductStockUnitSalePricesDto: {} as IUpdateProductStockUnitSalePricesDto,
            updateProductStockUnitMembershipSalePricesDto: {} as IUpdateProductStockUnitMembershipSalePricesDto,
            voidProductStockUnitSalePricesDto: {} as IVoidProductStockUnitSalePricesDto,
            voidProductStockUnitMembershipSalePricesDto: {} as IVoidProductStockUnitMembershipSalePricesDto
        } as IEditCountryState;
    }

    componentDidMount() {
        this.setStateFromApi();
        sumoSignalR.initializeSignalR(signalR => {
            var proxy = sumoSignalR.createHubProxy(signalR, Constants.hub_Country);
            proxy.on(Constants.hubEvent_Country_FinishBatchUpdateAllPrices, (formResponseJsonString: string) => this.finishBatchUpdateAllPrices(formResponseJsonString));
            proxy.on(Constants.hubEvent_Country_FinishBatchUpdateAllPricesUndo, () => this.finishBatchUpdateAllPricesUndo());
            proxy.on(Constants.hubEvent_Country_FinishBatchUpdateProductStockUnitSalePrices, () => this.finishBatchUpdateProductStockUnitSalePrices());
            proxy.on(Constants.hubEvent_Country_FinishBatchUpdateProductStockUnitSalePricesUndo, () => this.finishBatchUpdateProductStockUnitSalePricesUndo());
            proxy.on(Constants.hubEvent_Country_FinishBatchVoidProductStockUnitSalePrices, () => this.finishBatchVoidProductStockUnitSalePrices());
            proxy.on(Constants.hubEvent_Country_FinishBatchUpdateProductStockUnitMembershipSalePrices, () => this.finishBatchUpdateProductStockUnitMembershipSalePrices());
            proxy.on(Constants.hubEvent_Country_FinishBatchUpdateProductStockUnitMembershipSalePricesUndo, () => this.finishBatchUpdateProductStockUnitMembershipSalePricesUndo());
            proxy.on(Constants.hubEvent_Country_FinishBatchVoidProductStockUnitMembershipSalePrices, () => this.finishBatchVoidProductStockUnitMembershipSalePrices());
            signalR.hub.start();
        });
    }

    finishBatchUpdateAllPrices(formResponseJsonString: string) {
        const formResponse = JSON.parse(formResponseJsonString) as IFormResponse;
        if (formResponse.isValid) {
            this.setState({
                formSuccessMessage: Constants.alert_saveSuccess,
                isModified: false,
                isUpdatingAllPrices: false,
                showUndoAllPricesUpdateButton: true
            });
        } else {
            this.setState({
                formSuccessMessage: "Data was successfully updated, with some errors due to missing corresponding localized kits in the source country.",
                isModified: false,
                isUpdatingAllPrices: false,
                showUndoAllPricesUpdateButton: true
            });
        }
    }

    finishBatchUpdateAllPricesUndo() {
        this.setState({
            formSuccessMessage: Constants.alert_saveSuccess,
            isModified: false,
            isUndoingAllPricesUpdate: false,
            showUndoAllPricesUpdateButton: false
        });
    }

    finishBatchUpdateProductStockUnitSalePrices() {
        this.setState({
            formSuccessMessage: Constants.alert_saveSuccess,
            isModified: false,
            isUpdatingProductStockUnitSalePrices: false,
            showUndoProductStockUnitSalePricesUpdateButton: true
        });
    }

    finishBatchUpdateProductStockUnitSalePricesUndo() {
        this.setState({
            formSuccessMessage: Constants.alert_saveSuccess,
            isModified: false,
            isUndoingProductStockUnitSalePricesUpdate: false,
            showUndoProductStockUnitSalePricesUpdateButton: false
        });
    }

    finishBatchVoidProductStockUnitSalePrices() {
        this.setState({
            formSuccessMessage: Constants.alert_saveSuccess,
            isModified: false,
            isVoidingProductStockUnitSalePrices: false
        });
    }

    finishBatchUpdateProductStockUnitMembershipSalePrices() {
        this.setState({
            formSuccessMessage: Constants.alert_saveSuccess,
            isModified: false,
            isUpdatingProductStockUnitMembershipSalePrices: false,
            showUndoProductStockUnitMembershipSalePricesUpdateButton: true
        });
    }

    finishBatchUpdateProductStockUnitMembershipSalePricesUndo() {
        this.setState({
            formSuccessMessage: Constants.alert_saveSuccess,
            isModified: false,
            isUndoingProductStockUnitMembershipSalePricesUpdate: false,
            showUndoProductStockUnitMembershipSalePricesUpdateButton: false
        });
    }

    finishBatchVoidProductStockUnitMembershipSalePrices() {
        this.setState({
            formSuccessMessage: Constants.alert_saveSuccess,
            isModified: false,
            isVoidingProductStockUnitMembershipSalePrices: false
        });
    }

    handleChangeCountryFlagIcon(flagIcon: string) {
        this.state.country.flagIcon = flagIcon;

        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeCountryIsDisabled(e) {
        this.state.country.isDisabled = e.target.checked;
        this.setState({
            country: this.state.country
        });
    }

    handleChangeCountryLocalize(e) {
        this.state.country.localize = e.target.checked;
        this.setState({
            country: this.state.country
        });
    }

    handleChangeCountryPaymentAccountId(e) {
        this.state.country.paymentAccountId = e.target.value;
        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeCountryLanguageCode(languageCode: string) {
        this.state.country.languageCode = languageCode;

        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeCountryName(e) {
        this.state.country.name = e.target.value;
        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeCountryUrl(e) {
        this.state.country.url = e.target.value;
        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeTaxName(e, tax: ITaxDto) {
        tax.name = e.target.value;
        tax.isModified = true;

        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeTaxCode(e, tax: ITaxDto) {
        tax.code = e.target.value;
        tax.isModified = true;

        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeTaxPercentage(e, tax: ITaxDto) {
        tax.percentage = e.target.value;
        tax.isModified = true;

        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeTaxApplyToShippingPrice(e, tax: ITaxDto) {
        tax.applyToShippingPrice = e.target.checked;
        tax.isModified = true;

        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeTaxApplyToProductPrice(e, tax: ITaxDto) {
        tax.applyToProductPrice = e.target.checked;
        tax.isModified = true;

        this.setState({
            isModified: true,
            country: this.state.country
        });
    }

    handleChangeUpdatePricesDtoSourceCountry(value: string) {
        const updateAllPricesDto = this.state.updateAllPricesDto;
        updateAllPricesDto.sourceCountry = this.state.allLocalizedCountries.filter(c => c.id === value)[0];

        this.setState({
            isModified: true,
            updateAllPricesDto
        });
    }

    handleChangeUpdatePricesDtoCategory(value: string) {
        const updateAllPricesDto = this.state.updateAllPricesDto;
        updateAllPricesDto.category = this.state.allCategories.filter(c => c.id === value)[0];

        this.setState({
            isModified: true,
            updateAllPricesDto
        });
    }

    handleChangeUpdatePricesDtoRoundMethod(value: string) {
        const updateAllPricesDto = this.state.updateAllPricesDto;
        updateAllPricesDto.roundMethod = value;

        this.setState({
            isModified: true,
            updateAllPricesDto
        });
    }

    handleChangeUpdatePricesDtoConversionFactor(e: any) {
        const updateAllPricesDto = this.state.updateAllPricesDto;
        updateAllPricesDto.conversionFactor = Number(e.target.value);

        this.setState({
            isModified: true,
            updateAllPricesDto
        });
    }

    handleChangeUpdatePricesDtoRoundInterval(e: any) {
        const updateAllPricesDto = this.state.updateAllPricesDto;
        updateAllPricesDto.roundInterval = Number(e.target.value);

        this.setState({
            isModified: true,
            updateAllPricesDto
        });
    }

    handleChangeUpdatePricesDtoRoundAfterTax(e: any) {
        const updateAllPricesDto = this.state.updateAllPricesDto;
        updateAllPricesDto.roundAfterTax = e.target.checked;

        this.setState({
            isModified: true,
            updateAllPricesDto
        });
    }

    handleChangeUpdateStockUnitSalePricesDtoSalesPercentage(e: any) {
        const updateProductStockUnitSalePricesDto = this.state.updateProductStockUnitSalePricesDto;
        updateProductStockUnitSalePricesDto.salePercentage = Number(e.target.value);

        this.setState({
            isModified: true,
            updateProductStockUnitSalePricesDto
        });
    }

    handleChangeUpdateStockUnitSalePricesDtoRoundMethod(value: string) {
        const updateProductStockUnitSalePricesDto = this.state.updateProductStockUnitSalePricesDto;
        updateProductStockUnitSalePricesDto.roundMethod = value;

        this.setState({
            isModified: true,
            updateProductStockUnitSalePricesDto
        });
    }

    handleChangeUpdateSalePricesDtoRoundInterval(e: any) {
        const updateProductStockUnitSalePricesDto = this.state.updateProductStockUnitSalePricesDto;
        updateProductStockUnitSalePricesDto.roundInterval = Number(e.target.value);

        this.setState({
            isModified: true,
            updateProductStockUnitSalePricesDto
        });
    }

    handleChangeUpdateSalePricesDtoRoundAfterTax(e: any) {
        const updateProductStockUnitSalePricesDto = this.state.updateProductStockUnitSalePricesDto;
        updateProductStockUnitSalePricesDto.roundAfterTax = e.target.checked;

        this.setState({
            isModified: true,
            updateProductStockUnitSalePricesDto
        });
    }

    handleChangeUpdateSalePricesDtoCategory(value: string) {
        const updateProductStockUnitSalePricesDto = this.state.updateProductStockUnitSalePricesDto;
        updateProductStockUnitSalePricesDto.category = this.state.allCategories.filter(c => c.id === value)[0];

        this.setState({
            isModified: true,
            updateProductStockUnitSalePricesDto
        });
    }

    handleChangeVoidSalePricesDtoCategory(value: string) {
        const voidProductStockUnitSalePricesDto = this.state.voidProductStockUnitSalePricesDto;
        voidProductStockUnitSalePricesDto.category = this.state.allCategories.filter(c => c.id === value)[0];

        this.setState({
            isModified: true,
            voidProductStockUnitSalePricesDto
        });
    }

    handleChangeUpdateStockUnitMembershipSalePricesDtoSalesPercentage(e: any) {
        const updateProductStockUnitMembershipSalePricesDto = this.state.updateProductStockUnitMembershipSalePricesDto;
        updateProductStockUnitMembershipSalePricesDto.salePercentage = Number(e.target.value);

        this.setState({
            isModified: true,
            updateProductStockUnitMembershipSalePricesDto
        });
    }

    handleChangeUpdateStockUnitMembershipSalePricesDtoRoundMethod(value: string) {
        const updateProductStockUnitMembershipSalePricesDto = this.state.updateProductStockUnitMembershipSalePricesDto;
        updateProductStockUnitMembershipSalePricesDto.roundMethod = value;

        this.setState({
            isModified: true,
            updateProductStockUnitMembershipSalePricesDto
        });
    }

    handleChangeUpdateMembershipSalePricesDtoRoundInterval(e: any) {
        const updateProductStockUnitMembershipSalePricesDto = this.state.updateProductStockUnitMembershipSalePricesDto;
        updateProductStockUnitMembershipSalePricesDto.roundInterval = Number(e.target.value);

        this.setState({
            isModified: true,
            updateProductStockUnitMembershipSalePricesDto
        });
    }

    handleChangeUpdateMembershipSalePricesDtoRoundAfterTax(e: any) {
        const updateProductStockUnitMembershipSalePricesDto = this.state.updateProductStockUnitMembershipSalePricesDto;
        updateProductStockUnitMembershipSalePricesDto.roundAfterTax = e.target.checked;

        this.setState({
            isModified: true,
            updateProductStockUnitMembershipSalePricesDto
        });
    }

    handleChangeUpdateMembershipSalePricesDtoCategory(value: string) {
        const updateProductStockUnitMembershipSalePricesDto = this.state.updateProductStockUnitMembershipSalePricesDto;
        updateProductStockUnitMembershipSalePricesDto.category = this.state.allCategories.filter(c => c.id === value)[0];

        this.setState({
            isModified: true,
            updateProductStockUnitMembershipSalePricesDto
        });
    }

    handleChangeVoidMembershipSalePricesDtoCategory(value: string) {
        const voidProductStockUnitMembershipSalePricesDto = this.state.voidProductStockUnitMembershipSalePricesDto;
        voidProductStockUnitMembershipSalePricesDto.category = this.state.allCategories.filter(c => c.id === value)[0];

        this.setState({
            isModified: true,
            voidProductStockUnitMembershipSalePricesDto
        });
    }

    handleDeleteCountry() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Country_Delete, { countryId: this.state.country.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Country_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleAddNewTax() {
        const country = this.state.country;
        const tax = new Tax({
            isNew: true,
            country: new Country({
                id: country.id
            }),
            applyToShippingPrice: true,
            applyToProductPrice: true
        });

        country.taxes.unshift(tax);

        this.setState({
            country: country
        });
    }

    handleRemoveTax(e, tax: Tax) {
        const country = this.state.country;
        const taxes = country.taxes;
        const index = taxes.indexOf(tax);

        if (tax.isNew) {
            taxes.splice(index, 1);
        } else {
            tax.isDeleted = true;
            taxes[index] = tax;
        }

        this.setState({
            country,
            isModified: true
        });
    }

    getProductPricesButtonUpdateIsDisabled() {
        const { sourceCountry, roundInterval, roundMethod, conversionFactor } = this.state.updateAllPricesDto;
        return !sourceCountry || (roundMethod !== "" && !roundInterval) || !conversionFactor || this.state.isUpdatingAllPrices || this.state.isUpdatingProductStockUnitSalePrices || this.state.isUpdatingProductStockUnitMembershipSalePrices;
    }

    getProductSalePricesButtonUpdateIsDisabled() {
        const { salePercentage, roundMethod, roundInterval } = this.state.updateProductStockUnitSalePricesDto;
        return !salePercentage || salePercentage < 0 || salePercentage > 100 || (roundMethod !== "" && !roundInterval) || this.state.isUpdatingAllPrices || this.state.isUpdatingProductStockUnitSalePrices || this.state.isUpdatingProductStockUnitMembershipSalePrices;
    }

    getProductMembershipSalePricesButtonUpdateIsDisabled() {
        const { salePercentage, roundMethod, roundInterval } = this.state.updateProductStockUnitMembershipSalePricesDto;
        return !salePercentage || salePercentage < 0 || salePercentage > 100 || (roundMethod !== "" && !roundInterval) || this.state.isUpdatingAllPrices || this.state.isUpdatingProductStockUnitSalePrices || this.state.isUpdatingProductStockUnitMembershipSalePrices;
    }

    setStateFromApi() {
        this.setState({ pageIsLoading: true });
        sumoJS.ajaxPost<IEditCountryDto>(
            Constants.url_Country_EditJson,
            {
                countryId: sumoJS.getQueryStringParameter("countryId")
            },
            editCountryDto => {
                const updateAllPricesDto = {
                    sourceCountry: new Country(editCountryDto.allLocalizedCountries.filter(c => c.isDefault)[0]),
                    roundMethod: "decimals",
                    roundInterval: 2,
                    conversionFactor: 1,
                    roundAfterTax: false,
                    updateVariantOptionsPrices: true,
                    updateShippingBoxesPrices: true,
                    category: {} as Category
                } as IUpdateAllPricesDto;

                const updateProductStockUnitSalePricesDto = {
                    salePercentage: 0,
                    roundMethod: "decimals",
                    roundInterval: 2,
                    roundAfterTax: false,
                    category: {} as Category
                } as IUpdateProductStockUnitSalePricesDto;

                const voidProductStockUnitSalePricesDto = {
                    category: {} as Category
                } as IVoidProductStockUnitSalePricesDto;

                const updateProductStockUnitMembershipSalePricesDto = {
                    salePercentage: 0,
                    roundMethod: "decimals",
                    roundInterval: 2,
                    roundAfterTax: false,
                    category: {} as Category
                } as IUpdateProductStockUnitMembershipSalePricesDto;

                const voidProductStockUnitMembershipSalePricesDto = {
                    category: {} as Category
                } as IVoidProductStockUnitMembershipSalePricesDto;

                if (editCountryDto.country && editCountryDto.country.flagIcon && editCountryDto.country.flagIcon.includesSubstring(Constants.flag_icon_class, true)) {
                    editCountryDto.country.flagIcon = editCountryDto.country.flagIcon.split(Constants.flag_icon_class)[1] || "";
                }

                this.setState(
                    {
                        allLocalizedCountries: editCountryDto.allLocalizedCountries.map(x => new Country(x)),
                        allCategories: editCountryDto.allCategories.map(x => new Category(x)),
                        authenticatedUser: new User(editCountryDto.authenticatedUser),
                        cultures: editCountryDto.cultures,
                        country: new Country(editCountryDto.country),
                        flagIcons: editCountryDto.flagIcons,
                        pageIsLoading: false,
                        updateAllPricesDto,
                        updateProductStockUnitSalePricesDto,
                        voidProductStockUnitSalePricesDto,
                        updateProductStockUnitMembershipSalePricesDto,
                        voidProductStockUnitMembershipSalePricesDto
                    },
                    () => Functions.setCreateAndEditPermissions(this.state.country, this.state.authenticatedUser.role.createCountry, this.state.authenticatedUser.role.editCountry)
                );
            }
        );
    }

    submitProductPricesUpdate() {
        const updatePricesDto = this.state.updateAllPricesDto;
        this.setState({ isUpdatingAllPrices: true });
        sumoSignalR.invoke(
            Constants.hub_Country,
            Constants.hubMethod_Country_BatchUpdateAllPrices,
            this.state.country.id,
            updatePricesDto.sourceCountry.id,
            updatePricesDto.conversionFactor,
            updatePricesDto.roundMethod,
            updatePricesDto.roundInterval,
            updatePricesDto.roundAfterTax,
            updatePricesDto.category ? updatePricesDto.category.id : null
        );
    }

    submitProductStockUnitSalePricesUpdate() {
        const updateStockUnitSalePricesDto = this.state.updateProductStockUnitSalePricesDto;
        this.setState({ isUpdatingProductStockUnitSalePrices: true });
        sumoSignalR.invoke(
            Constants.hub_Country,
            Constants.hubMethod_Country_BatchUpdateProductStockUnitSalePrices,
            this.state.country.id,
            updateStockUnitSalePricesDto.salePercentage,
            updateStockUnitSalePricesDto.roundMethod,
            updateStockUnitSalePricesDto.roundInterval,
            updateStockUnitSalePricesDto.roundAfterTax,
            updateStockUnitSalePricesDto.category ? updateStockUnitSalePricesDto.category.id : null
        );
    }

    submitProductStockUnitSalePricesVoid() {
        const voidProductStockUnitSalePricesDto = this.state.voidProductStockUnitSalePricesDto;
        this.setState({ isVoidingProductStockUnitSalePrices: true });
        sumoSignalR.invoke(
            Constants.hub_Country,
            Constants.hubMethod_Country_BatchVoidProductStockUnitSalePrices,
            this.state.country.id,
            voidProductStockUnitSalePricesDto.category ? voidProductStockUnitSalePricesDto.category.id : null
        );
    }

    submitProductStockUnitMembershipSalePricesUpdate() {
        const updateStockUnitMembershipSalePricesDto = this.state.updateProductStockUnitMembershipSalePricesDto;
        this.setState({ isUpdatingProductStockUnitMembershipSalePrices: true });
        sumoSignalR.invoke(
            Constants.hub_Country,
            Constants.hubMethod_Country_BatchUpdateProductStockUnitMembershipSalePrices,
            this.state.country.id,
            updateStockUnitMembershipSalePricesDto.salePercentage,
            updateStockUnitMembershipSalePricesDto.roundMethod,
            updateStockUnitMembershipSalePricesDto.roundInterval,
            updateStockUnitMembershipSalePricesDto.roundAfterTax,
            updateStockUnitMembershipSalePricesDto.category ? updateStockUnitMembershipSalePricesDto.category.id : null
        );
    }

    submitProductStockUnitMembershipSalePricesVoid() {
        const voidProductStockUnitMembershipSalePricesDto = this.state.voidProductStockUnitMembershipSalePricesDto;
        this.setState({ isVoidingProductStockUnitMembershipSalePrices: true });
        sumoSignalR.invoke(
            Constants.hub_Country,
            Constants.hubMethod_Country_BatchVoidProductStockUnitMembershipSalePrices,
            this.state.country.id,
            voidProductStockUnitMembershipSalePricesDto.category ? voidProductStockUnitMembershipSalePricesDto.category.id : null
        );
    }

    undoProductPricesUpdate() {
        this.setState({ isUndoingAllPricesUpdate: true });
        sumoSignalR.invoke(Constants.hub_Country, Constants.hubMethod_Country_BatchUpdateAllPricesUndo, this.state.country.id);
    }

    undoProductStockUnitSalePricesUpdate() {
        this.setState({ isUndoingProductStockUnitSalePricesUpdate: true });
        sumoSignalR.invoke(Constants.hub_Country, Constants.hubMethod_Country_BatchUpdateProductStockUnitSalePricesUndo, this.state.country.id);
    }

    undoProductStockUnitMembershipSalePricesUpdate() {
        this.setState({ isUndoingProductStockUnitMembershipSalePricesUpdate: true });
        sumoSignalR.invoke(Constants.hub_Country, Constants.hubMethod_Country_BatchUpdateProductStockUnitMembershipSalePricesUndo, this.state.country.id);
    }

    submit() {
        const country = sumoJS.deepClone(this.state.country);
        country.taxes = country.taxes.filter(x => x.isModified || x.isDeleted || x.isNew);
        if (country.flagIcon) {
            country.flagIcon = Constants.flag_icon_class + country.flagIcon.replace(".svg", "");
        }

        this.setState({ isSubmitting: true });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Country_AddOrUpdate, country, response => {
            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi();
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    isModified: false,
                    isSubmitting: false
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false
                });
            }
        });
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">{this.state.country.isNew ? "Create new country" : " Edit country"}</div>
                <div className="admin-top-bar-controls">
                    <a href={Constants.url_Country_Index} className="btn btn-link">
                        Back to list
                    </a>
                    {!this.state.country.isNew && (
                        <button type="button" className="btn btn-primary" onClick={() => this.handleAddNewTax()}>
                            <i className="fa fa-plus" /> Add New Tax
                        </button>
                    )}
                    {!this.state.country.isNew && (
                        <ButtonDeleteComponent
                            {...{
                                canDelete: this.state.authenticatedUser.role.deleteCountry,
                                isDeleting: this.state.isDeleting,
                                handleDelete: () => this.handleDeleteCountry()
                            } as IButtonDeleteProps}
                        />
                    )}
                    <ButtonSubmitComponent
                        {...{
                            isSubmitting: this.state.isSubmitting,
                            handleSubmit: () => this.submit()
                        } as IButtonSubmitProps}
                    />
                </div>
            </div>
        );
    }

    renderTaxItem(tax: Tax, key: number) {
        return (
            <div className="admin-subsection" key={key}>
                <div className="admin-subsection-title">Tax</div>
                <div className="admin-subsection-controls">
                    <button className="btn btn-link" onClick={e => this.handleRemoveTax(e, tax)}>
                        <i className="fa fa-trash-o" /> Remove
                    </button>
                </div>
                <TextboxComponent
                    {...{
                        label: "Name",
                        value: tax.name,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: e => this.handleChangeTaxName(e, tax)
                    } as ITextboxProps}
                />
                <TextboxComponent
                    {...{
                        label: "Code",
                        value: tax.code,
                        className: "form-group",
                        type: "text",
                        handleChangeValue: e => this.handleChangeTaxCode(e, tax)
                    } as ITextboxProps}
                />
                <TextboxComponent
                    {...{
                        label: "Percentage",
                        value: tax.percentage,
                        className: "form-group",
                        type: "number",
                        handleChangeValue: e => this.handleChangeTaxPercentage(e, tax)
                    } as ITextboxProps}
                />
                <CheckboxComponent
                    {...{
                        value: tax.applyToShippingPrice || false,
                        className: "form-group",
                        text: "Apply to Shipping Price",
                        handleChangeValue: e => this.handleChangeTaxApplyToShippingPrice(e, tax)
                    } as ICheckboxProps}
                />
                <CheckboxComponent
                    {...{
                        value: tax.applyToProductPrice || false,
                        text: "Apply to Product Price",
                        handleChangeValue: e => this.handleChangeTaxApplyToProductPrice(e, tax)
                    } as ICheckboxProps}
                />
            </div>
        );
    }

    renderNoTax() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">Tax</div>
                <p className="text-muted">Please click on the "Add New Tax" button to add a new tax.</p>
            </div>
        );
    }

    renderBatchOperations() {
        return (
            <div className="admin-subsection">
                <div className="admin-subsection-title">Batch Operations</div>
                <div className="panel-group" id="accordion">
                    {
                        // --------------------------------------------------------------------
                        // UPDATE PRICES
                        // --------------------------------------------------------------------
                    }
                    <div className="panel panel-default" id="update-product-prices">
                        <div className="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#update-product-prices-body">
                            <span className="panel-title">Update All Prices</span>
                        </div>
                        <div className="panel-collapse collapse in" id="update-product-prices-body">
                            <div className="panel-body">
                                <SelectComponent
                                    {...{
                                        options: this.state.allLocalizedCountries.map(x => (
                                            <option key={x.id} value={x.id}>
                                                {x.name}
                                            </option>
                                        )),
                                        value: !!this.state.updateAllPricesDto.sourceCountry ? this.state.updateAllPricesDto.sourceCountry.id : "",
                                        label: "Use the following country prices as a starting point",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeUpdatePricesDtoSourceCountry(e.target.value)
                                    } as ISelectProps}
                                />
                                <SelectComponent
                                    {...{
                                        options: this.state.allCategories.map(x => (
                                            <option key={x.id} value={x.id}>
                                                {x.name}
                                            </option>
                                        )),
                                        neutralOption: <option value="">All Categories</option>,
                                        value: !!this.state.updateAllPricesDto.category ? this.state.updateAllPricesDto.category.id : "",
                                        label: "Category",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeUpdatePricesDtoCategory(e.target.value)
                                    } as ISelectProps}
                                />
                                <TextboxComponent
                                    {...{
                                        label: "Conversion factor",
                                        value: this.state.updateAllPricesDto.conversionFactor || 0,
                                        placeholder: "0",
                                        className: "form-group",
                                        type: "number",
                                        handleChangeValue: e => this.handleChangeUpdatePricesDtoConversionFactor(e)
                                    } as ITextboxProps}
                                />
                                <SelectComponent
                                    {...{
                                        options: [
                                            <option key="none" value="">
                                                None (no rounding)
                                            </option>,
                                            <option key="decimals" value="decimals">
                                                Decimals (keep only a specified number of decimals)
                                            </option>,
                                            <option key="closest" value="closest">
                                                Closest (round off to the closest integer)
                                            </option>,
                                            <option key="up" value="up">
                                                Up (round up to the closest integer)
                                            </option>,
                                            <option key="down" value="down">
                                                Down (round down to the closest integer)
                                            </option>
                                        ],
                                        value: this.state.updateAllPricesDto.roundMethod || "",
                                        label: "Round method",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeUpdatePricesDtoRoundMethod(e.target.value)
                                    } as ISelectProps}
                                />
                                <TextboxComponent
                                    {...{
                                        label: this.state.updateAllPricesDto.roundMethod === "decimals" ? "Decimal places" : "Round Interval",
                                        value: this.state.updateAllPricesDto.roundInterval || 0,
                                        placeholder: "0",
                                        className: this.state.updateAllPricesDto.roundMethod === "" ? "form-group disabled" : "form-group",
                                        type: "number",
                                        handleChangeValue: e => this.handleChangeUpdatePricesDtoRoundInterval(e)
                                    } as ITextboxProps}
                                />
                                <CheckboxComponent
                                    {...{
                                        className: this.state.updateAllPricesDto.roundMethod === "" ? "form-group disabled" : "form-group",
                                        value: this.state.updateAllPricesDto.roundAfterTax || false,
                                        text: "Round prices after tax",
                                        handleChangeValue: e => this.handleChangeUpdatePricesDtoRoundAfterTax(e)
                                    } as ICheckboxProps}
                                />
                                <ButtonSubmitComponent
                                    {...{
                                        text: "Update All Prices",
                                        className: this.getProductPricesButtonUpdateIsDisabled() ? "submit disabled" : "submit",
                                        isSubmitting: this.state.isUpdatingAllPrices,
                                        handleSubmit: () => this.getProductPricesButtonUpdateIsDisabled() || this.submitProductPricesUpdate()
                                    } as IButtonSubmitProps}
                                />
                                {this.state.showUndoAllPricesUpdateButton && (
                                    <button className="btn btn-link" onClick={() => this.undoProductPricesUpdate()}>
                                        {this.state.isUndoingAllPricesUpdate && <i className="fa fa-spinner fa-pulse fa-fw" />}
                                        &nbsp; Undo changes
                                    </button>
                                )}
                            </div>
                        </div>
                    </div>
                    {
                        // --------------------------------------------------------------------
                        // UPDATE SALE PRICES
                        // --------------------------------------------------------------------
                    }
                    <div className="panel panel-default" id="update-productstockunit-sale-prices">
                        <div className="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#update-productstockunit-sale-prices-body">
                            <span className="panel-title">Update StockUnit Sale Prices</span>
                        </div>
                        <div className="panel-collapse collapse" id="update-productstockunit-sale-prices-body">
                            <div className="panel-body">
                                <TextboxComponent
                                    {...{
                                        label: "Sale Percentage",
                                        value: this.state.updateProductStockUnitSalePricesDto.salePercentage || 0,
                                        placeholder: "0",
                                        className: "form-group",
                                        type: "number",
                                        handleChangeValue: e => this.handleChangeUpdateStockUnitSalePricesDtoSalesPercentage(e)
                                    } as ITextboxProps}
                                />
                                <SelectComponent
                                    {...{
                                        options: this.state.allCategories.map(x => (
                                            <option key={x.id} value={x.id}>
                                                {x.name}
                                            </option>
                                        )),
                                        neutralOption: <option value="">All Categories</option>,
                                        value: !!this.state.updateProductStockUnitSalePricesDto.category ? this.state.updateProductStockUnitSalePricesDto.category.id : "",
                                        label: "Category",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeUpdateSalePricesDtoCategory(e.target.value)
                                    } as ISelectProps}
                                />
                                <SelectComponent
                                    {...{
                                        options: [
                                            <option key="none" value="">
                                                None (no rounding)
                                            </option>,
                                            <option key="decimals" value="decimals">
                                                Decimals (keep only a specified number of decimals)
                                            </option>,
                                            <option key="closest" value="closest">
                                                Closest (round off to the closest integer)
                                            </option>,
                                            <option key="up" value="up">
                                                Up (round up to the closest integer)
                                            </option>,
                                            <option key="down" value="down">
                                                Down (round down to the closest integer)
                                            </option>
                                        ],
                                        value: this.state.updateProductStockUnitSalePricesDto.roundMethod || "",
                                        label: "Round method",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeUpdateStockUnitSalePricesDtoRoundMethod(e.target.value)
                                    } as ISelectProps}
                                />
                                <TextboxComponent
                                    {...{
                                        label: this.state.updateProductStockUnitSalePricesDto.roundMethod === "decimals" ? "Decimal places" : "Round Interval",
                                        value: this.state.updateProductStockUnitSalePricesDto.roundInterval || 0,
                                        placeholder: "0",
                                        className: this.state.updateProductStockUnitSalePricesDto.roundMethod === "" ? "form-group disabled" : "form-group",
                                        type: "number",
                                        handleChangeValue: e => this.handleChangeUpdateSalePricesDtoRoundInterval(e)
                                    } as ITextboxProps}
                                />
                                <CheckboxComponent
                                    {...{
                                        className: this.state.updateProductStockUnitSalePricesDto.roundMethod === "" ? "form-group disabled" : "form-group",
                                        value: this.state.updateProductStockUnitSalePricesDto.roundAfterTax || false,
                                        text: "Round prices after tax",
                                        handleChangeValue: e => this.handleChangeUpdateSalePricesDtoRoundAfterTax(e)
                                    } as ICheckboxProps}
                                />
                                <ButtonSubmitComponent
                                    {...{
                                        text: "Update Sale Prices",
                                        className: this.getProductSalePricesButtonUpdateIsDisabled() ? "submit disabled" : "submit",
                                        isSubmitting: this.state.isUpdatingProductStockUnitSalePrices,
                                        handleSubmit: () => this.getProductSalePricesButtonUpdateIsDisabled() || this.submitProductStockUnitSalePricesUpdate()
                                    } as IButtonSubmitProps}
                                />
                                {/*
                                    --------------------------------------------------------------------------
                                    Uncomment when the undo productStockUnitPrices method has been implemented
                                    --------------------------------------------------------------------------

                                    this.state.showUndoProductStockUnitSalePricesButton && (
                                    <button className="btn btn-link" onClick={() => this.undoProductStockUnitSalePricesUpdate()}>
                                        {this.state.isUndoingProductStockUnitSalePricesUpdate && <i className="fa fa-spinner fa-pulse fa-fw" />}
                                        &nbsp; Undo changes
                                    </button>)
                                */}
                            </div>
                        </div>
                    </div>
                    {
                        // --------------------------------------------------------------------
                        // VOID SALE PRICES
                        // --------------------------------------------------------------------
                    }
                    <div className="panel panel-default" id="void-productstockunit-sale-prices">
                        <div className="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#void-productstockunit-sale-prices-body">
                            <span className="panel-title">Void StockUnit Sale Prices</span>
                        </div>
                        <div className="panel-collapse collapse" id="void-productstockunit-sale-prices-body">
                            <div className="panel-body">
                                <SelectComponent
                                    {...{
                                        options: this.state.allCategories.map(x => (
                                            <option key={x.id} value={x.id}>
                                                {x.name}
                                            </option>
                                        )),
                                        neutralOption: <option value="">All Categories</option>,
                                        value: !!this.state.voidProductStockUnitSalePricesDto.category ? this.state.voidProductStockUnitSalePricesDto.category.id : "",
                                        label: "Category",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeVoidSalePricesDtoCategory(e.target.value)
                                    } as ISelectProps}
                                />
                                <ButtonSubmitComponent
                                    {...{
                                        text: "Void Sale Prices",
                                        className: "submit",
                                        isSubmitting: this.state.isVoidingProductStockUnitSalePrices,
                                        handleSubmit: () => this.submitProductStockUnitSalePricesVoid()
                                    } as IButtonSubmitProps}
                                />
                            </div>
                        </div>
                    </div>
                    {
                        // --------------------------------------------------------------------
                        // UPDATE MEMBERSHIP SALE PRICES
                        // --------------------------------------------------------------------
                    }
                    <div className="panel panel-default" id="update-productstockunit-membership-sale-prices">
                        <div className="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#update-productstockunit-membership-sale-prices-body">
                            <span className="panel-title">Update StockUnit Membership Sale Prices</span>
                        </div>
                        <div className="panel-collapse collapse" id="update-productstockunit-membership-sale-prices-body">
                            <div className="panel-body">
                                <TextboxComponent
                                    {...{
                                        label: "Membership Sale Percentage",
                                        value: this.state.updateProductStockUnitMembershipSalePricesDto.salePercentage || 0,
                                        placeholder: "0",
                                        className: "form-group",
                                        type: "number",
                                        handleChangeValue: e => this.handleChangeUpdateStockUnitMembershipSalePricesDtoSalesPercentage(e)
                                    } as ITextboxProps}
                                />
                                <SelectComponent
                                    {...{
                                        options: this.state.allCategories.map(x => (
                                            <option key={x.id} value={x.id}>
                                                {x.name}
                                            </option>
                                        )),
                                        neutralOption: <option value="">All Categories</option>,
                                        value: !!this.state.updateProductStockUnitMembershipSalePricesDto.category ? this.state.updateProductStockUnitMembershipSalePricesDto.category.id : "",
                                        label: "Category",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeUpdateMembershipSalePricesDtoCategory(e.target.value)
                                    } as ISelectProps}
                                />
                                <SelectComponent
                                    {...{
                                        options: [
                                            <option key="none" value="">
                                                None (no rounding)
                                            </option>,
                                            <option key="decimals" value="decimals">
                                                Decimals (keep only a specified number of decimals)
                                            </option>,
                                            <option key="closest" value="closest">
                                                Closest (round off to the closest integer)
                                            </option>,
                                            <option key="up" value="up">
                                                Up (round up to the closest integer)
                                            </option>,
                                            <option key="down" value="down">
                                                Down (round down to the closest integer)
                                            </option>
                                        ],
                                        value: this.state.updateProductStockUnitMembershipSalePricesDto.roundMethod || "",
                                        label: "Round method",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeUpdateStockUnitMembershipSalePricesDtoRoundMethod(e.target.value)
                                    } as ISelectProps}
                                />
                                <TextboxComponent
                                    {...{
                                        label: this.state.updateProductStockUnitMembershipSalePricesDto.roundMethod === "decimals" ? "Decimal places" : "Round Interval",
                                        value: this.state.updateProductStockUnitMembershipSalePricesDto.roundInterval || 0,
                                        placeholder: "0",
                                        className: this.state.updateProductStockUnitMembershipSalePricesDto.roundMethod === "" ? "form-group disabled" : "form-group",
                                        type: "number",
                                        handleChangeValue: e => this.handleChangeUpdateMembershipSalePricesDtoRoundInterval(e)
                                    } as ITextboxProps}
                                />
                                <CheckboxComponent
                                    {...{
                                        className: this.state.updateProductStockUnitMembershipSalePricesDto.roundMethod === "" ? "form-group disabled" : "form-group",
                                        value: this.state.updateProductStockUnitMembershipSalePricesDto.roundAfterTax || false,
                                        text: "Round prices after tax",
                                        handleChangeValue: e => this.handleChangeUpdateMembershipSalePricesDtoRoundAfterTax(e)
                                    } as ICheckboxProps}
                                />
                                <ButtonSubmitComponent
                                    {...{
                                        text: "Update Membership Sale Prices",
                                        className: this.getProductMembershipSalePricesButtonUpdateIsDisabled() ? "submit disabled" : "submit",
                                        isSubmitting: this.state.isUpdatingProductStockUnitMembershipSalePrices,
                                        handleSubmit: () => this.getProductMembershipSalePricesButtonUpdateIsDisabled() || this.submitProductStockUnitMembershipSalePricesUpdate()
                                    } as IButtonSubmitProps}
                                />
                                {/*
                                    --------------------------------------------------------------------------
                                    Uncomment when the undo productStockUnitMembershipPrices method has been implemented
                                    --------------------------------------------------------------------------

                                    this.state.showUndoProductStockUnitMembershipSalePricesButton && (
                                    <button className="btn btn-link" onClick={() => this.undoProductStockUnitMembershipSalePricesUpdate()}>
                                        {this.state.isUndoingProductStockUnitMembershipSalePricesUpdate && <i className="fa fa-spinner fa-pulse fa-fw" />}
                                        &nbsp; Undo changes
                                    </button>)
                                */}
                            </div>
                        </div>
                    </div>
                    {
                        // --------------------------------------------------------------------
                        // VOID MEMBERSHIP SALE PRICES
                        // --------------------------------------------------------------------
                    }
                    <div className="panel panel-default" id="void-productstockunit-membership-sale-prices">
                        <div className="panel-heading collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#void-productstockunit-membership-sale-prices-body">
                            <span className="panel-title">Void StockUnit Membership Sale Prices</span>
                        </div>
                        <div className="panel-collapse collapse" id="void-productstockunit-membership-sale-prices-body">
                            <div className="panel-body">
                                <SelectComponent
                                    {...{
                                        options: this.state.allCategories.map(x => (
                                            <option key={x.id} value={x.id}>
                                                {x.name}
                                            </option>
                                        )),
                                        neutralOption: <option value="">All Categories</option>,
                                        value: !!this.state.voidProductStockUnitMembershipSalePricesDto.category ? this.state.voidProductStockUnitMembershipSalePricesDto.category.id : "",
                                        label: "Category",
                                        className: "form-group",
                                        handleChangeValue: e => this.handleChangeVoidMembershipSalePricesDtoCategory(e.target.value)
                                    } as ISelectProps}
                                />
                                <ButtonSubmitComponent
                                    {...{
                                        text: "Void Membership Sale Prices",
                                        className: "submit",
                                        isSubmitting: this.state.isVoidingProductStockUnitMembershipSalePrices,
                                        handleSubmit: () => this.submitProductStockUnitMembershipSalePricesVoid()
                                    } as IButtonSubmitProps}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const StringAutocomplete = Autocomplete as IStringAutocomplete;
        const countryTaxesToRender = sumoJS.get(() => this.state.country.taxes.filter(x => !x.isDeleted), new Array<Tax>());

        return (
            <div className="edit-country-component">
                {this.renderTopBar()}
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                {this.state.pageIsLoading && <SpinnerComponent />}
                {!this.state.pageIsLoading && (
                    <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <TextboxComponent
                                    {...{
                                        label: "Name",
                                        value: this.state.country.name,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: e => this.handleChangeCountryName(e)
                                    } as ITextboxProps}
                                />
                                <StringAutocomplete
                                    label="Language Code"
                                    className="form-group"
                                    options={this.state.cultures.map(x => new AutocompleteOption(x, x))}
                                    selectedOption={new AutocompleteOption(this.state.country.languageCode, this.state.country.languageCode)}
                                    handleChangeValue={(languageCode: string) => this.handleChangeCountryLanguageCode(languageCode)}
                                />
                                <TextboxComponent
                                    {...{
                                        label: "Url",
                                        value: this.state.country.url,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: e => this.handleChangeCountryUrl(e)
                                    } as ITextboxProps}
                                />
                                <StringAutocomplete
                                    label="FlagIcon"
                                    className="form-group"
                                    options={this.state.flagIcons.map(x => new AutocompleteOption(x, x))}
                                    selectedOption={new AutocompleteOption(this.state.country.flagIcon, this.state.country.flagIcon)}
                                    handleChangeValue={(flagIcon: string) => this.handleChangeCountryFlagIcon(flagIcon)}
                                />
                                <TextboxComponent
                                    {...{
                                        label: "Payment Account Id",
                                        value: this.state.country.paymentAccountId,
                                        className: "form-group",
                                        type: "text",
                                        handleChangeValue: e => this.handleChangeCountryPaymentAccountId(e),
                                    } as ITextboxProps}
                                />
                                <CheckboxComponent
                                    {...{
                                        value: this.state.country.isDisabled || false,
                                        className: "form-group",
                                        text: "Disabled",
                                        handleChangeValue: e => this.handleChangeCountryIsDisabled(e)
                                    } as ICheckboxProps}
                                />
                                <CheckboxComponent
                                    {...{
                                        value: this.state.country.localize || false,
                                        className: "form-group",
                                        text: "Localize",
                                        handleChangeValue: e => this.handleChangeCountryLocalize(e)
                                    } as ICheckboxProps}
                                />
                            </div>
                            {!this.state.country.isNew && this.renderBatchOperations()}
                        </div>
                        <div className="col-md-6">
                            {countryTaxesToRender.length === 0 && this.renderNoTax()}
                            {countryTaxesToRender.map((x, i) => this.renderTaxItem(x, i))}
                        </div>
                    </div>
                )}
            </div>
        );
    }
}
