﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Country } from "../../../../../Scripts/classes/Country";
import { IUpdateAllPricesDto } from "./IUpdateAllPricesDto";
import { IUpdateProductStockUnitSalePricesDto } from "./IUpdateProductStockUnitSalePricesDto";
import { IUpdateProductStockUnitMembershipSalePricesDto } from "./IUpdateProductStockUnitMembershipSalePricesDto";
import { IVoidProductStockUnitSalePricesDto } from "./IVoidProductStockUnitSalePricesDto";
import { IVoidProductStockUnitMembershipSalePricesDto } from "./IVoidProductStockUnitMembershipSalePricesDto";
import { Category } from "../../../../../Scripts/classes/Category";

export interface IEditCountryState extends IReactPageState {
    allLocalizedCountries: Array<Country>;
    allCategories: Array<Category>,
    cultures?: Array<string>;
    country?: Country;
    flagIcons?: Array<string>;
    isUpdatingAllPrices?: boolean;
    isUpdatingProductStockUnitSalePrices?: boolean;
    isUpdatingProductStockUnitMembershipSalePrices?: boolean;
    isUndoingAllPricesUpdate?: boolean;
    isUndoingProductStockUnitSalePricesUpdate?: boolean;
    isUndoingProductStockUnitMembershipSalePricesUpdate?: boolean;
    isVoidingProductStockUnitSalePrices?: boolean;
    isVoidingProductStockUnitMembershipSalePrices?: boolean;
    showUndoAllPricesUpdateButton?: boolean;
    showUndoProductStockUnitSalePricesUpdateButton?: boolean;
    showUndoProductStockUnitMembershipSalePricesUpdateButton?: boolean;
    updateAllPricesDto: IUpdateAllPricesDto;
    updateProductStockUnitSalePricesDto: IUpdateProductStockUnitSalePricesDto;
    updateProductStockUnitMembershipSalePricesDto: IUpdateProductStockUnitMembershipSalePricesDto;
    voidProductStockUnitSalePricesDto: IVoidProductStockUnitSalePricesDto;
    voidProductStockUnitMembershipSalePricesDto: IVoidProductStockUnitMembershipSalePricesDto;
}