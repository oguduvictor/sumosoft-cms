﻿import { ICountryDto } from "../../../../../Scripts/interfaces/ICountryDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { ICategoryDto } from "../../../../../Scripts/interfaces/ICategoryDto";

export interface IEditCountryDto {
    allLocalizedCountries: Array<ICountryDto>;
    allCategories: Array<ICategoryDto>;
    authenticatedUser: IUserDto;
    cultures: Array<string>;
    country: ICountryDto;
    flagIcons: Array<string>;
}