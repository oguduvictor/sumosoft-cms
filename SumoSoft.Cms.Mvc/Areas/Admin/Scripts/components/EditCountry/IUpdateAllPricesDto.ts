﻿import { Country } from "../../../../../Scripts/classes/Country";
import { Category } from "../../../../../Scripts/classes/Category";

export interface IUpdateAllPricesDto {
    sourceCountry: Country;
    roundMethod: string;
    roundInterval: number;
    conversionFactor: number;
    roundAfterTax: boolean;
    category: Category;
}