﻿import { Category } from "../../../../../Scripts/classes/Category";

export interface IUpdateProductStockUnitSalePricesDto {
    salePercentage: number;
    roundMethod: string;
    roundInterval: number;
    roundAfterTax: boolean;
    category: Category;
}