﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Log } from "../../../../../Scripts/classes/Log";

export interface INotificationState extends IReactPageState {
    log?: Log;
}