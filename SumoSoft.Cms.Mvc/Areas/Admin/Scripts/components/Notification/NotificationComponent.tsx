﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { Constants } from "../../shared/Constants";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { INotificationState } from "./INotificationState";
import { Log } from "../../../../../Scripts/classes/Log";
import { ILogDto } from "../../../../../Scripts/interfaces/ILogDto";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { ButtonDeleteComponent } from '../ButtonDelete/ButtonDeleteComponent';

export default class NotificationComponent extends ReactPageComponent<{}, INotificationState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            log: new Log()
        } as INotificationState;
    }

    componentDidMount() {
        sumoJS.ajaxPost<ILogDto>(Constants.url_Notification_DetailsJson,
            {
                notificationId: sumoJS.getQueryStringParameter("notificationId")
            },
            (logDto) => {
                this.setState({
                    log: new Log(logDto),
                    pageIsLoading: false
                });
            });
    }

    handleDeleteLog() {
        this.setState({ isDeleting: true });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Notification_Delete, { id: this.state.log.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Notification_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    render() {
        return (
            <div className="notification-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Notification
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Notification_Index} className="btn btn-link">
                            Back to list
                        </a>
                        <ButtonDeleteComponent
                            isDeleting={this.state.isDeleting}
                            handleDelete={() => this.handleDeleteLog()} />
                    </div>
                </div>
                <AlertComponent
                    {...{
                        errorMessages: this.getFormErrorMessages(),
                        successMessage: this.state.formSuccessMessage,
                        handleHideAlert: () => this.handleHideAlert()
                    } as IAlertProps}
                />
                {!this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <div className="row">
                            <div className="col-md-6">
                                <TextboxComponent {...{
                                    label: "Date",
                                    readOnly: true,
                                    value: this.state.log.createdDate,
                                    className: "form-group"
                                } as ITextboxProps} />
                            </div>
                            <div className="col-md-6">
                                <TextareaComponent {...{
                                    label: "User",
                                    readOnly: true,
                                    type: "text",
                                    className: "form-group",
                                    value: (this.state.log.user && `${this.state.log.user.firstName} ${this.state.log.user.lastName} (${this.state.log.user.email})`) || ""
                                } as ITextareaProps} />
                            </div>
                        </div>
                        <TextareaComponent {...{
                            label: "Message",
                            type: "text",
                            readOnly: true,
                            className: "form-group",
                            value: this.state.log.message || ""
                        } as ITextareaProps} />
                        <TextareaComponent {...{
                            label: "Exception",
                            type: "text",
                            readOnly: true,
                            className: "form-group",
                            value: this.state.log.details || ""
                        } as ITextareaProps} />
                    </div>
                }
                {this.state.pageIsLoading && <SpinnerComponent />}
            </div>
        );
    }
}