﻿import * as React from "react";
import { Functions } from "../../shared/Functions";

export interface ISortableContentItem {
    item: string | number | JSX.Element;
    className?: string;
}

export interface ISortableData {
    id: string;
    name?: string;
    children?: Array<ISortableData>;
}

export interface ISortableRow {
    id: string;
    contentItems: Array<ISortableContentItem>;
    name?: string;
    childrenNotAllowed?: boolean;
    children?: Array<ISortableRow>;
    cellStyles?: Array<ISortableRowCellStyle>;
}

export interface ISortableRowCellStyle {
    cellIndices: Array<number>;
    style: React.CSSProperties;
}

export interface ISortableProps {
    headerRowItems?: Array<string>;
    rows: Array<ISortableRow>;
    maxDepth?: number;
    className?: string;
    isFixedTableLayout?: boolean;
    handleChangeSorting?: (data: Array<ISortableData>) => void;
}

export class SortableComponent extends React.Component<ISortableProps, undefined> {

    static defaultProps: ISortableProps;

    sortableTableId = Functions.newGuid();

    componentDidMount() {
        this.componentDidMountOrUpdate();
    }

    componentDidUpdate() {
        this.componentDidMountOrUpdate();
    }

    componentDidMountOrUpdate() {
        this.initializeSortable();
    }

    initializeSortable() {
        $(`#${this.sortableTableId}`).nestable({
            group: Functions.newGuid(), //disallows dragging between two different sortables
            scroll: true,
            maxDepth: this.props.maxDepth,
            onDragStart: function (l, e) {
                if (e.prevObject.is("button") || e.prevObject.is("a")) {
                    if (e.prevObject.is("a") && $(window).width() < 1135) {
                        var href = e.prevObject.prop("href");
                        window.location.href = href;
                    }
                    return false;
                }
            },
            callback: (l, e) => {
                if (this.props.handleChangeSorting) {
                    const serializedData = $(`#${this.sortableTableId}`).nestable("serialize") as Array<ISortableData>;
                    this.props.handleChangeSorting(serializedData);
                }
            }
        });
    }

    getCellStyle(row: ISortableRow, cellIndex: number) {
        if (row.cellStyles) {
            const cellStyles = row.cellStyles.filter(x => x.cellIndices.contains(cellIndex));
            if (cellStyles.length) {
                return cellStyles.reduce((value, cellStyle) => $.extend({}, value, cellStyle.style), {} as React.CSSProperties);
            }
        }
        return null;
    }

    renderSortableRows(rows: Array<ISortableRow>, currentDepth: number) {
        return (
            <ol className="dd-list">
                {
                    rows.map(row =>
                        <li className={`dd-item${row.childrenNotAllowed ? " dd-nochildren" : ""}`} data-id={row.id} data-name={row.name} key={row.id}>
                            <div className="dd-handle">
                                <div
                                    className="display-table"
                                    style={this.props.isFixedTableLayout ? { tableLayout: "fixed" } : null}>
                                    <div className="display-table-row">
                                        {
                                            row.contentItems.map((contentItem, i) =>
                                                <div
                                                    key={i}
                                                    className={`display-table-cell ${contentItem.className ? contentItem.className : ""}`}
                                                    style={this.getCellStyle(row, i)}>
                                                    {
                                                        contentItem.item
                                                    }
                                                </div>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                            {
                                currentDepth < this.props.maxDepth && !row.childrenNotAllowed && row.children && row.children.length ?
                                    this.renderSortableRows(row.children, currentDepth + 1)
                                    : null
                            }
                        </li>
                    )
                }
            </ol>
        );
    }

    render() {
        return (
            <div className={`sortable-component${this.props.className ? ` ${this.props.className}` : ""}`}>
                {
                    this.props.headerRowItems &&
                    <div className="header display-table">
                        <div className="display-table-row">
                            {
                                this.props.headerRowItems.map((item, i) =>
                                    <div key={i} className="display-table-cell">
                                        {
                                            item
                                        }
                                    </div>
                                )
                            }
                        </div>
                    </div>
                }
                <div className="dd" id={this.sortableTableId}>
                    {
                        this.props.rows && this.props.rows.length
                            ? this.renderSortableRows(this.props.rows, 1)
                            : null
                    }
                </div>
            </div>
        );
    }
}

SortableComponent.defaultProps = {
    rows: new Array<ISortableRow>(),
    maxDepth: 1
} as ISortableProps