﻿import { IBlogPostDto } from "../../../../../Scripts/interfaces/IBlogPostDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IBlogPostListDto {
    authenticatedUser: IUserDto;
    blogPosts?: Array<IBlogPostDto>;
}