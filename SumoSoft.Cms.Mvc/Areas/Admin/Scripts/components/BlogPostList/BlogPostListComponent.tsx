﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { TableComponent, ITableProps } from "../Table/TableComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { BlogPost } from "../../../../../Scripts/classes/BlogPost";
import { Constants } from "../../shared/Constants";
import { IBlogPostListState } from "./IBlogPostListState";
import { IBlogPostListDto } from "./IBlogPostListDto";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Functions } from "../../shared/Functions";

export default class BlogPostListComponent extends React.Component<{}, IBlogPostListState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            blogPosts: new Array<BlogPost>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IBlogPostListState;
    }

    componentDidMount() {
        sumoJS.ajaxPost<IBlogPostListDto>(Constants.url_Posts_IndexJson, null, blogPostList => {
            this.setState({
                pageIsLoading: false,
                blogPosts: blogPostList.blogPosts.map(x => new BlogPost(x)),
                authenticatedUser: new User(blogPostList.authenticatedUser)
            });
        });
    }

    render() {
        return (
            <div className="blog-post-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Posts
                    </div>
                    <div className="admin-top-bar-controls">
                        {
                            <a href={Constants.url_Posts_Edit} className={`btn btn-primary ${this.state.authenticatedUser.role.createBlogPost ? "" : "disabled"}`}>
                                <i className="fa fa-plus"></i>
                                Create new Post
                            </a>
                        }
                    </div>
                </div>
                <div className="admin-subsection">
                    <div className="table-wrapper">
                        <TableComponent {...{
                            thList: ["Title", "Date", "Author", "Published", ""],
                            trList: this.state.blogPosts
                                .map((x) => [x.title,
                                dayjs(x.createdDate).format("DD/MM/YYYY"),
                                x.author && x.author.fullName,
                                x.isPublished ? <i className="fa fa-check-square-o"></i> : <i className="fa fa-square-o"></i>,
                                <a className="btn btn-primary btn-xs" href={Constants.url_Posts_Edit + x.id}>edit</a>])
                        } as ITableProps} />
                        {
                            this.state.pageIsLoading && <SpinnerComponent />
                        }
                    </div>
                </div>
            </div>
        );
    }
}