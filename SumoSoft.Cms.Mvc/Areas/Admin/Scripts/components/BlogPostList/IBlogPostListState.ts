﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { BlogPost } from "../../../../../Scripts/classes/BlogPost";
import { User } from "../../../../../Scripts/classes/User";

export interface IBlogPostListState extends IReactPageState {
    blogPosts?: Array<BlogPost>;
    authenticatedUser?: User;
}