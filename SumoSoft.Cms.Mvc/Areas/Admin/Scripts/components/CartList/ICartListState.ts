﻿import { IReactPageState } from '../ReactPage/ReactPageComponent';
import { Country } from '../../../../../Scripts/classes/Country';
import { Cart } from '../../../../../Scripts/classes/Cart';
import { ShippingBox } from '../../../../../Scripts/classes/ShippingBox';

export interface ICartListState extends IReactPageState {
	carts?: Array<Cart>;
	countries?: Array<Country>;
	shippingBoxes?: Array<ShippingBox>;
	keywordFilter?: string;
	countryNameFilter?: string;
	shippingBoxNameFilter?: string;
	page?: number;
	totalPages?: number;
}
