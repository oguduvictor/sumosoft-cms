﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { Constants } from "../../shared/Constants";
import { Country } from "../../../../../Scripts/classes/Country";
import { Cart } from "../../../../../Scripts/classes/Cart";
import { ShippingBox } from "../../../../../Scripts/classes/ShippingBox";
import { ICartListState } from "./ICartListState";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { ICartListDto } from "./ICartListDto";

export default class CartListComponent extends React.Component<{}, ICartListState> {

    state = {
        carts: new Array<Cart>(),
        countries: new Array<Country>(),
        page: 1,
        pageIsLoading: true,
        shippingBoxes: new Array<ShippingBox>(),
        totalPages: 1
    } as ICartListState;

    constructor(props) {
        super(props);

        this.handleChangeKeywordFilter = this.handleChangeKeywordFilter.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount() {
        this.setStateFromApi(1);
    }

    handleChangeKeywordFilter(e) {

        const value = e.target.value;
        this.setState({ keywordFilter: e.target.value }, () => {
            if (!value) {
                this.setStateFromApi(1);
            }
        });
    }

    handleChangeCountryFilter(e) {
        this.setState({ countryNameFilter: e.target.value }, () => {
            this.setStateFromApi(1);
        });
    }

    handleChangeShippingBoxFilter(e) {
        this.setState({ shippingBoxNameFilter: e.target.value }, () => {
            this.setStateFromApi(1);
        });
    }

    handleKeyPress(e) {
        if (e.key === "Enter") {
            this.setStateFromApi(1);
        }
    }

    setStateFromApi(page = 1) {
        this.setState({
            pageIsLoading: true
        });
        var apiParam = {
            countryNameFilter: this.state.countryNameFilter,
            keywordFilter: this.state.keywordFilter,
            page: page,
            shippingBoxNameFilter: this.state.shippingBoxNameFilter
        };
        sumoJS.ajaxPost<ICartListDto>(Constants.url_Cart_IndexJson, apiParam, cartListDto => {
            this.setState({
                carts: cartListDto.carts.map(x => new Cart(x)),
                countries: cartListDto.countries.map(x => new Country(x)),
                shippingBoxes: cartListDto.shippingBoxes.map(x => new ShippingBox(x)),
                page,
                totalPages: cartListDto.totalPages,
                pageIsLoading: false
            });
        });
    }

    render() {
        return (
            <div className="cart-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Carts
                    </div>
                    <div className="admin-top-bar-controls">
                        <div id="keep-this-wrapper">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Search..."
                                    value={this.state.keywordFilter || ""}
                                    onChange={(e) => this.handleChangeKeywordFilter(e)}
                                    onKeyPress={(e) => this.handleKeyPress(e)} />
                                <span className="input-group-btn">
                                    <button className="btn btn-default" type="button" onClick={() => this.setStateFromApi(1)}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div className="btn-group" role="group">
                            <a className={`btn btn-default ${(this.state.page <= 1 || this.state.totalPages === 1) ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page - 1))}>
                                <i className="fa fa-angle-left" aria-hidden="true"></i>
                            </a>
                            <span className="btn btn-default">
                                {`Page ${this.state.page} / ${this.state.totalPages}`}
                            </span>
                            <a className={`btn btn-default ${this.state.page === this.state.totalPages ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page + 1))}>
                                <i className="fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <SelectComponent {...{
                            neutralOption: <option value="">All countries</option>,
                            options: this.state.countries.map(x => <option key={x.id} value={x.name}>{x.name}</option>),
                            value: this.state.countryNameFilter,
                            className: "no-bottom-margin",
                            handleChangeValue: (e) => this.handleChangeCountryFilter(e)
                        } as ISelectProps} />
                        <SelectComponent {...{
                            neutralOption: <option value="">All shipping boxes</option>,
                            options: this.state.shippingBoxes.map(x => <option key={x.id} value={x.name}>{x.localizedInternalDescription}</option>),
                            value: this.state.shippingBoxNameFilter,
                            className: "no-bottom-margin",
                            handleChangeValue: (e) => this.handleChangeShippingBoxFilter(e)
                        } as ISelectProps} />
                    </div>
                </div>
                <div className="admin-subsection">
                    <div className="table-wrapper">
                        {!this.state.pageIsLoading &&
                            <TableComponent {...{
                                thList: ["#", "Date", "User", "Country", "Total", ""],
                                trList: this.state.carts
                                    .map((x, i) => [
                                        i + 1,
                                        x.modifiedDate && dayjs(x.modifiedDate).format("DD/MM/YYYY"),
                                        x.user.email || "Null",
                                        x.user.country ? x.user.country.name : "",
                                        x.user.country ? x.user.country.currencySymbol + x.totalAfterTax : "-",
                                        <a className="btn btn-primary btn-xs" href={Constants.url_Cart_Edit + x.id}>open</a>])
                            } as ITableProps} />
                        }
                        {this.state.pageIsLoading && <SpinnerComponent />}
                    </div>
                </div>
            </div>
        );
    }
}