﻿import { IPaginatedListDto } from "./../../interfaces/IPaginatedListDto";
import { ICartDto } from "../../../../../Scripts/interfaces/ICartDto";
import { IShippingBoxDto } from "../../../../../Scripts/interfaces/IShippingBoxDto";
import { ICountryDto } from "../../../../../Scripts/interfaces/ICountryDto";

export interface ICartListDto extends IPaginatedListDto {
    carts: Array<ICartDto>;
    shippingBoxes: Array<IShippingBoxDto>;
    countries: Array<ICountryDto>;
}