﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { Coupon } from "../../../../../Scripts/classes/Coupon";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Constants } from "../../shared/Constants";
import { ICouponListState } from "./ICouponListState";
import { ICouponListDto } from "./ICouponListDto";

export default class CouponListComponent extends React.Component<{}, ICouponListState> {

    state = {
        coupons: new Array<Coupon>(),
        keywordFilter: "",
        page: 1,
        totalPages: 1
    } as ICouponListState;

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.setStateFromApi(1);
    }

    setStateFromApi(page = 1) {
        this.setState({ pageIsLoading: true });

        sumoJS.ajaxPost<ICouponListDto>(
            Constants.url_Coupon_IndexJson,
            {
                keywordFilter: this.state.keywordFilter,
                page: page
            }, couponListDto => {
                this.setState({
                    page,
                    totalPages: couponListDto.totalPages,
                    coupons: couponListDto.coupons.map(c => new Coupon(c)),
                    pageIsLoading: false
                });
            });
    }

    handleChangeKeywordFilter(e) {
        const value = e.target.value;
        this.setState({ keywordFilter: e.target.value }, () => {
            if (!value) {
                this.setStateFromApi(1);
            }
        });
    }

    handleKeyPress(e) {
        if (e.key === "Enter") {
            this.setStateFromApi(1);
        }
    }

    renderTopBar() {
        return (
            <div className="admin-top-bar">
                <div className="admin-top-bar-title">
                    Coupons
                </div>
                <div className="admin-top-bar-controls">
                    <div id="keep-this-wrapper">
                        <div className="input-group">
                            <input type="text" className="form-control" placeholder="Search..."
                                value={this.state.keywordFilter || ""}
                                onChange={(e) => this.handleChangeKeywordFilter(e)}
                                onKeyPress={(e) => this.handleKeyPress(e)} />
                            <span className="input-group-btn">
                                <button className="btn btn-default" type="button" onClick={() => this.setStateFromApi(1)}>
                                    <i className="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div className="btn-group" role="group">
                        <a className={`btn btn-default ${(this.state.page <= 1 || this.state.totalPages === 1) ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page - 1))}>
                            <i className="fa fa-angle-left" aria-hidden="true"></i>
                        </a>
                        <span className="btn btn-default">
                            {`Page ${this.state.page} / ${this.state.totalPages}`}
                        </span>
                        <a className={`btn btn-default ${this.state.page === this.state.totalPages ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page + 1))}>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                    <a href={Constants.url_Coupon_Edit} className="btn btn-primary">
                        <i className="fa fa-plus"></i>
                        Create new coupons
                    </a>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="coupon-list-component">
                {
                    this.renderTopBar()
                }
                {!this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Published</th>
                                    <th>Expiration</th>
                                    <th>Validity times</th>
                                    <th>Country</th>
                                    <th>Amount</th>
                                    <th>Percentage</th>
                                    <th>Note</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.coupons.map(coupon =>
                                    <tr key={coupon.id}>
                                        <td>
                                            {coupon.code}
                                        </td>
                                        <td>
                                            {coupon.published.toString()}
                                        </td>
                                        <td>
                                            {dayjs(coupon.expirationDate).format("DD/MM/YYYY")}
                                        </td>
                                        <td>
                                            {coupon.validityTimes}
                                        </td>
                                        <td>
                                            {coupon.country ? coupon.country.name : "All Countries"}
                                        </td>
                                        <td>
                                            {coupon.amount.toFixed(2)}
                                        </td>
                                        <td>
                                            {`${coupon.percentage} %`}
                                        </td>
                                        <td>
                                            {coupon.note}
                                        </td>
                                        <td>
                                            <a className="btn btn-primary btn-xs" href={Constants.url_Coupon_Edit + coupon.id}>
                                                edit
                                            </a>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}