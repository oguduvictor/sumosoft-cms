﻿import { IReactPageState } from '../ReactPage/ReactPageComponent';
import { Coupon } from '../../../../../Scripts/classes/Coupon';

export interface ICouponListState extends IReactPageState {
	coupons?: Array<Coupon>;
	keywordFilter: string;
	page?: number;
	totalPages?: number;
}
