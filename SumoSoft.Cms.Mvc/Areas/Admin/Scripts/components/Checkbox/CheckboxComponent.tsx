﻿import * as React from "react";
import { TopLabel } from "../TopLabel/TopLabel";

export interface ICheckboxProps {
    value: boolean;
    label?: string;
    text?: string;
    tooltip?: string;
    addon?: string;
    className?: string;
    handleChangeValue?(e): void;
}

export var CheckboxComponent: (props: ICheckboxProps) => JSX.Element = props => {
    return (
        <div className={`checkbox-component input-sub-parent ${props.className ? props.className : ""}`}>
            <TopLabel
                label={props.label}
                tooltip={props.tooltip} />
            <label>
                <input type="checkbox" checked={props.value || false} onChange={(e) => props.handleChangeValue && props.handleChangeValue(e)} /><span className="post-input">{props.text}</span>
            </label>
        </div>
    );
}