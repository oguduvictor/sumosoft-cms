﻿import * as React from "react";

export interface ITableProps {
    thList?: Array<JSX.Element | string>;
    trList?: Array<Array<JSX.Element | string>>;
    className?: string;
}

export var TableComponent: (props: ITableProps) => JSX.Element = props => {

    return (
        <div className={`table-component${props.className ? ` ${props.className}` : ""}`}>
            <table className="table table-responsive-sm">
                {
                    props.thList &&
                    <thead>
                        <tr> 
                            {
                                props.thList.map((th, i) =>
                                    <th key={i}>
                                        {th}
                                    </th>
                                )
                            }
                        </tr>
                    </thead>
                }
                <tbody>
                    {
                        props.trList.map((tr, i) =>
                            <tr key={i}>
                                {tr.map((td, ii) =>
                                    <td key={ii}>
                                        {td}
                                    </td>
                                )}
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </div>
    );
}