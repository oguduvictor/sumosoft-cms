﻿import * as React from "react"

export interface IAlertProps {
    errorMessages?: Array<string>;
    successMessage?: string;
    handleHideAlert?();
}

export interface IAlertState {
    timeOutId: number;
}

export class AlertComponent extends React.Component<IAlertProps, IAlertState> {

    state = {
        timeOutId: 0
    } as IAlertState;

    componentWillReceiveProps() {
        this.state.timeOutId > 0 && clearTimeout(this.state.timeOutId);
        const timeOutId = window.setTimeout(() => this.props.handleHideAlert(), 10000);
        this.setState({ timeOutId });
    }

    componentDidUpdate() {
        $(".alert").slideDown(1000);
    }

    renderSuccessAlert() {
        return (
            <div className="alert-component">
                <div className="alert alert-success alert-custom" style={{ display: "none", opacity: 0 }}>
                    <button type="button" className="close" onClick={this.props.handleHideAlert}>&times;</button>
                    <ul>
                        <li className="message">{this.props.successMessage}</li>
                    </ul>
                </div>
                <div className="alert alert-success fixed alert-custom" style={{ display: "none" }}>
                    <button type="button" className="close" onClick={this.props.handleHideAlert}>&times;</button>
                    <ul>
                        <li className="message">{this.props.successMessage}</li>
                    </ul>
                </div>
            </div>
        );
    }

    renderErrorAlert() {
        return (
            <div className="alert-component">
                <div className="alert alert-warning alert-custom" style={{ display: "none", opacity: 0 }}>
                    <button type="button" className="close" onClick={this.props.handleHideAlert}>&times;</button>
                    <ul>
                        {
                            this.props.errorMessages.map((message, i) =>
                                <li key={i} className="message">{message}</li>)
                        }
                    </ul>
                </div>
                <div className="alert alert-warning fixed alert-custom" style={{ display: "none" }}>
                    <button type="button" className="close" onClick={this.props.handleHideAlert}>&times;</button>
                    <ul>
                        {
                            this.props.errorMessages.map((message, i) =>
                                <li key={i} className="message">{message}</li>)
                        }
                    </ul>
                </div>
            </div>
        );
    }

    render() {
        if (!this.props.successMessage && (!this.props.errorMessages || !this.props.errorMessages.length)) return null;
        if (!!this.props.successMessage) return this.renderSuccessAlert();
        if (!!this.props.errorMessages) return this.renderErrorAlert();
    }
}