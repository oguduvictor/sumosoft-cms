﻿import * as React from "react";

export interface IModalProps {
    id?: string;
    title: string;
    body: JSX.Element;
    footer?: JSX.Element;
}

export var ModalComponent: (props: IModalProps) => JSX.Element = props => {

    return (
        <div className="modal-component">
            <div className="modal fade" id={props.id} role="dialog">
                <div className= "modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span className="sr-only">Close</span>
                            </button>
                            <h4 className="modal-title">
                                {props.title}
                            </h4>
                        </div>
                        <div className="modal-body">
                            {props.body}
                        </div>
                        {props.footer &&
                            <div className="modal-footer">
                                {props.footer}
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );

}

