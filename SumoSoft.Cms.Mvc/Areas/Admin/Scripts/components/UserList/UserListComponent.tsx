﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as dayjs from "dayjs";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { TableComponent, ITableProps } from "../Table/TableComponent";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { IUserListState } from "./IUserListState";
import { IUserListDto } from "./IUserListDto";
import { Functions } from "../../shared/Functions";

export default class UserListComponent extends ReactPageComponent<{}, IUserListState> {

    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            totalPages: 1,
            users: new Array<User>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IUserListState;

        this.handleChangeKeywordFilter = this.handleChangeKeywordFilter.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount() {
        this.setStateFromApi(1);
    }

    handleChangeKeywordFilter(e) {
        const value = e.target.value;
        this.setState({ keywordFilter: e.target.value }, () => {
            if (!value) {
                this.setStateFromApi(1);
            }
        });
    }

    handleKeyPress(e) {
        if (e.key === "Enter") {
            this.setStateFromApi(1);
        }
    }

    setStateFromApi(page = 1) {
        this.setState({ pageIsLoading: true });

        sumoJS.ajaxPost<IUserListDto>(Constants.url_User_IndexJson,
            {
                keywordFilter: this.state.keywordFilter,
                page: page
            },
            (userListDto) => {
                this.setState({
                    authenticatedUser: new User(userListDto.authenticatedUser),
                    users: userListDto.users.map(x => new User(x)),
                    page,
                    totalPages: userListDto.totalPages,
                    pageIsLoading: false
                });
            });
    }

    render() {
        return (
            <div className="user-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        User List
                    </div>
                    <div className="admin-top-bar-controls">
                        <div id="keep-this-wrapper">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Search..."
                                    value={this.state.keywordFilter || ""}
                                    onChange={(e) => this.handleChangeKeywordFilter(e)}
                                    onKeyPress={(e) => this.handleKeyPress(e)} />
                                <span className="input-group-btn">
                                    <button className="btn btn-default" type="button" onClick={() => this.setStateFromApi(1)}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div className="btn-group" role="group">
                            <a className={`btn btn-default ${(this.state.page <= 1 || this.state.totalPages === 1) ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page - 1))}>
                                <i className="fa fa-angle-left" aria-hidden="true"></i>
                            </a>
                            <span className="btn btn-default">
                                {`Page ${this.state.page} / ${this.state.totalPages}`}
                            </span>
                            <a className={`btn btn-default ${this.state.totalPages === 1 ? "disabled" : ""}`} onClick={() => this.setStateFromApi(Number(this.state.page + 1))}>
                                <i className="fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <a href={Constants.url_User_Export} className="btn btn-default">
                            <i className="fa fa-table" aria-hidden="true"></i>
                            Export users
                        </a>
                        <a href={Constants.url_User_Edit} className={`btn btn-primary ${this.state.authenticatedUser.role.createUser ? "" : "disabled"}`}>
                            <i className="fa fa-plus"></i>
                            Create user
                        </a>
                    </div>
                </div>
                {
                    !this.state.pageIsLoading &&
                    <div className="admin-subsection">
                        <TableComponent {...{
                            thList: ["#", "Name", "Email", "Country", "Creation date", "Role", "Type", ""],
                            trList: this.state.users
                                .map((user, index) => [
                                    index + 1,
                                    user.fullName,
                                    user.email,
                                    user.country ? user.country.name : "",
                                    dayjs(user.createdDate).format("DD/MM/YYYY"),
                                    user.role ? user.role.name : "",
                                    user.isRegistered ? "Registered" : "Partial",
                                    <a className="btn btn-primary btn-xs" href={Constants.url_User_Edit + user.id}>edit</a>])
                        } as ITableProps} />
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}