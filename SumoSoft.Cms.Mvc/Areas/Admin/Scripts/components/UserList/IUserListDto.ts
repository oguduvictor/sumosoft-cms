﻿import { IPaginatedListDto } from "./../../interfaces/IPaginatedListDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IUserListDto extends IPaginatedListDto {
    authenticatedUser: IUserDto;
    users: Array<IUserDto>;
}