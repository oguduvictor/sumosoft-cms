﻿import { User } from "../../../../../Scripts/classes/User";

export interface IUserListState {
    authenticatedUser: User,
    keywordFilter?: string;
    page?: number;
    pageIsLoading?: boolean;
    totalPages?: number;
    users?: Array<User>;
} 