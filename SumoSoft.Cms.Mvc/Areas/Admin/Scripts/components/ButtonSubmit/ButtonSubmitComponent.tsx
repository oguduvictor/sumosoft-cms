﻿import * as React from "react";

export interface IButtonSubmitProps {
    text?: string;
    className?: string;
    isSubmitting: boolean;
    handleSubmit(): void;
}

export var ButtonSubmitComponent: (props: IButtonSubmitProps) => JSX.Element = props => {
    {/* If isSubmitting === true, the button is disabled and the onClick event handler is removed */ }

    if (props.isSubmitting)
        return (
            <span className={`button-submit-component ${props.className ? props.className : ""}`}>
                <button className="btn btn-primary btn-submit disabled" type="button">
                    <i className="fa fa-spinner fa-pulse fa-fw"></i>
                    {props.text || "Save"}
                </button>
            </span>
        );

    {/* If isSubmitting === false, enable the button and add the onClick event handler */ }

    return (
        <span className={`button-submit-component ${props.className ? props.className : ""}`}>
            <button className="btn btn-primary btn-submit" type="button" onClick={props.handleSubmit}>
                {props.text || "Save"}
            </button>
        </span>
    );

}