﻿import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { ICategoryDto } from "../../../../../Scripts/interfaces/ICategoryDto";

export interface ICategoryListDto {
    authenticatedUser: IUserDto;
    categories?: Array<ICategoryDto>;
}