﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Category } from "../../../../../Scripts/classes/Category";

export interface ICategoryListState extends IReactPageState {
    categories?: Array<Category>;
    sortedCategories?: Array<Category>;
    pageIsLoading?: boolean;
}