﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Category } from "../../../../../Scripts/classes/Category";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { ICategoryListState } from "./ICategoryListState";
import { ICategoryListDto } from "./ICategoryListDto";
import { SortableComponent, ISortableData, ISortableProps, ISortableRow } from "../Sortable/SortableComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";

export default class CategoryListComponent extends ReactPageComponent<{}, ICategoryListState> {

    constructor(props) {
        super(props);

        this.state = {
            categories: new Array<Category>(),
            sortedCategories: new Array<Category>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as ICategoryListState;
    }

    componentDidMount() {
        this.setState({
            pageIsLoading: true
        });
        sumoJS.ajaxPost<ICategoryListDto>(Constants.url_Category_IndexJson, null, categoryListDto => {
            this.setState({
                authenticatedUser: new User(categoryListDto.authenticatedUser),
                categories: categoryListDto.categories.map(x => new Category(x)),
                pageIsLoading: false
            });
        });
    }

    getSortableRows(categories: Array<Category>) {
        categories = categories.orderBy(x => x.sortOrder);
        return categories.map(category => {
            return {
                id: category.id,
                contentItems: [
                    { item: `${category.localizedTitle} (${category.name})`},
                    {
                        item: <div className="text-right">
                                  <a href={Constants.url_Category_Edit + category.id} className="btn btn-primary btn-xs">
                                      edit
                                  </a>
                              </div>
                    }
                ],
                children: category.children && category.children.length ? this.getSortableRows(category.children) : null
            } as ISortableRow;
        });
    }

    handleChangeSorting(data: Array<ISortableData>) {
        const sortedCategories = data as Array<Category>;
        this.setState({
            sortedCategories,
            isModified: true
        });
    }

    handleSubmit() {
        this.setState({
            isSubmitting: true
        });

        sumoJS.ajaxPost<IFormResponse<Array<Category>>>(Constants.url_Category_UpdateHierarchy, { categories: this.state.sortedCategories }, response => {
            if (response.isValid) {
                this.setState({
                    categories: []
                }, () => {
                    $(".categories .dd").nestable("destroy");

                    this.setState({
                        categories: response.target,
                        formSuccessMessage: Constants.alert_saveSuccess,
                        isModified: false,
                        isSubmitting: false
                    });
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false
                });
            }
        });
    }

    render() {
        return (
            <div className="category-list-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        Categories
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Category_Edit} className={`btn btn-primary ${this.state.authenticatedUser.role.createCategory ? "" : "disabled"}`}>
                            <i className="fa fa-plus"></i>
                            Create new category
                        </a>
                        <ButtonSubmitComponent {...{
                            isSubmitting: this.state.isSubmitting,
                            className: this.state.isModified ? "" : "disabled",
                            handleSubmit: () => this.handleSubmit()
                        } as IButtonSubmitProps} />
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-12">
                            <div className="admin-subsection">
                                <SortableComponent {...{
                                    headerRowItems: ["Localized Title (Name)"],
                                    rows: this.getSortableRows(this.state.categories),
                                    className: "categories",
                                    maxDepth: 10,
                                    handleChangeSorting: (data) => this.handleChangeSorting(data)
                                } as ISortableProps} />
                            </div>
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}