﻿import { ContentSection } from '../../../../../Scripts/classes/ContentSection';
import { Country } from '../../../../../Scripts/classes/Country';
import { MailingList } from '../../../../../Scripts/classes/MailingList';
import { User } from '../../../../../Scripts/classes/User';
import { IEmailDto } from "../../../../../Scripts/interfaces/IEmailDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";

export interface IEditEmailDto {
    authenticatedUser: IUserDto;
    email: IEmailDto;
    viewNames: Array<string>;
    allContentSections: Array<ContentSection>;
    contentSectionsSchemaNames: Array<string>;
    allUsers: Array<User>;
    allMailingLists: Array<MailingList>;
    allCountries: Array<Country>;
}
