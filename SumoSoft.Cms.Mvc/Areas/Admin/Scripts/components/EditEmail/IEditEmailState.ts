﻿import { ContentSection } from '../../../../../Scripts/classes/ContentSection';
import { Country } from '../../../../../Scripts/classes/Country';
import { Email } from "../../../../../Scripts/classes/Email";
import { MailingList } from '../../../../../Scripts/classes/MailingList';
import { User } from '../../../../../Scripts/classes/User';
import { IFormError } from '../../../../../Scripts/interfaces/IFormError';
import { IReactPageState } from "../ReactPage/ReactPageComponent";

export interface IEditEmailState extends IReactPageState {
    email?: Email;
    viewNames?: Array<string>;
    isSendingEmail?: boolean;
    allContentSections: Array<ContentSection>;
    matchedContentSection: ContentSection;
    newContentSectionErrors: Array<IFormError>;
    newContentSection: ContentSection;
    contentSectionsSchemaNames?: Array<string>;
    allCountries: Array<Country>;
    allUsers: Array<User>;
    allMailingLists: Array<MailingList>;
    mailToType?: string;
    selectedMailingList?: MailingList;
    selectedEmails: Array<string>;
    selectedCountry?: Country;
}