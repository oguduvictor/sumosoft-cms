﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";
import * as ReactDOMServer from 'react-dom/server';

import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { Email } from "../../../../../Scripts/classes/Email";
import { EmailLocalizedKit } from "../../../../../Scripts/classes/EmailLocalizedKit";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { Constants } from "../../shared/Constants";
import { IEditEmailState } from "./IEditEmailState";
import { IEditEmailDto } from "./IEditEmailDto";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { Country } from "../../../../../Scripts/classes/Country";
import { Functions } from "../../shared/Functions";
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";
import { Autocomplete, AutocompleteOption } from '../../../../../Scripts/sumoReact/Autocomplete/Autocomplete';
import { ContentSection } from '../../../../../Scripts/classes/ContentSection';
import { ContentSectionLocalizedKit } from '../../../../../Scripts/classes/ContentSectionLocalizedKit';
import { ModalComponent, IModalProps } from '../Modal/ModalComponent';
import { ICreateContentSectionDto } from '../../interfaces/ICreateContentSectionDto';
import { ContentSectionLocalizedKitEditor } from '../EditContentSections/ContentSectionLocalizedKitEditor/ContentSectionLocalizedKitEditor';
import { MultiSelectComponent, SmartOption } from '../MultiSelect/MultiSelectComponent';
import { MailingList } from '../../../../../Scripts/classes/MailingList';

interface IContentSectionAutocomplete {
    new(): Autocomplete<ContentSection>;
}

export default class EditEmailComponent extends ReactPageComponent<{}, IEditEmailState> {

    constructor(props) {
        super(props);

        this.state = {
            formErrors: new Array<IFormError>(),
            email: new Email({ contentSection: new ContentSection() }),
            pageIsLoading: true,
            viewNames: new Array<string>(),
            newContentSectionErrors: new Array<IFormError>(),
            allContentSections: new Array<ContentSection>(),
            newContentSection: new ContentSection({
                contentSectionLocalizedKits: [new ContentSectionLocalizedKit()]
            }),
            contentSectionsSchemaNames: new Array<string>(),
            contentSectionLocalizedKitToCreate: new ContentSectionLocalizedKit({ contentSection: new ContentSection() }),
            matchedContentSection: new ContentSection(),
            authenticatedUser: new User({ role: new UserRole() }),
            selectedEmails: new Array<string>(),
            allCountries: new Array<Country>(),
            allMailingLists: new Array<MailingList>(),
            allUsers: new Array<User>()
        } as IEditEmailState;
    }

    componentDidMount() {
        this.setStateFromApi().done(() => {
            sumoJS.ajaxPost<Array<User>>(Constants.url_Email_AllUsers,
                null, users => this.setState({ allUsers: users }));

            sumoJS.ajaxPost<Array<Country>>(Constants.url_Email_AllCountries,
                null, countries => this.setState({ allCountries: countries }));

            sumoJS.ajaxPost<Array<MailingList>>(Constants.url_Email_AllMailingLists,
                null, mailingLists => this.setState({ allMailingLists: mailingLists }));
        });
    }

    handleChangeEmailLocalizedFrom(e, emailLocalizedKit: EmailLocalizedKit) {
        emailLocalizedKit.from = e.target.value;
        this.setState({
            email: this.state.email,
            isModified: true
        });
    }

    handleChangeEmailLocalizedReplyTo(e, emailLocalizedKit: EmailLocalizedKit) {
        emailLocalizedKit.replyTo = e.target.value;
        this.setState({
            email: this.state.email,
            isModified: true
        });
    }

    handleChangeEmailLocalizedDisplayName(e, emailLocalizedKit: EmailLocalizedKit) {
        emailLocalizedKit.displayName = e.target.value;
        this.setState({
            email: this.state.email,
            isModified: true
        });
    }

    handleChangeEmailName(e) {
        this.state.email.name = e.target.value;
        this.setState({
            email: this.state.email,
            isModified: true
        });
    }

    handleChangeViewTemplate(e) {
        this.state.email.viewName = e.target.value;
        this.setState({
            email: this.state.email,
            isModified: true
        });
    }

    handleChangeContentSection(contentSection: ContentSection) {
        this.setState({
            email: new Email({ ...this.state.email, contentSection })
        })
    }

    handleEmailDelete() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Email_Delete, { id: this.state.email.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Email_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    handleLocalizedKitCopyValuesFrom(source, target, completed) {
        target.subject = source.subject;
        const sourceContent = $(`#${source.id}`).val();
        $(`#${target.id}`).val(sourceContent);

        this.setState({
            isModified: true,
            email: this.state.email
        }, () => completed());
    }

    handleOpenNewContentSectionModal() {
        const newContentSection = new ContentSection();
        newContentSection.name = `Email_${this.state.email.name}`;
        newContentSection.contentSectionLocalizedKits = [
            new ContentSectionLocalizedKit({
                content: "",
                contentSection: new ContentSection({ id: newContentSection.id })
            })
        ];

        this.setState({
            newContentSection,
            matchedContentSection: new ContentSection(),
            newContentSectionErrors: new Array<IFormError>()
        }, () => $("#new-content-section-add").modal("show"));
    }

    handleChangeNewContentSectionName(value: string) {
        const { newContentSection } = this.state;

        newContentSection.name = value;

        this.setState({
            isModified: true,
            newContentSection
        });
    }

    handleChangeNewContentSectionContentSchema(value: string) {
        const { newContentSection } = this.state;

        newContentSection.schema = value;

        this.setState({ newContentSection });
    }

    handleChangeNewContentSectionContent(value: string) {
        const { newContentSection } = this.state;

        newContentSection.contentSectionLocalizedKits = newContentSection.contentSectionLocalizedKits.map(x => {
            x.content = value;
            return x;
        });

        this.setState({ newContentSection });
    }

    handleSaveContentSection(eventTarget, checkForSimilarContent: boolean) {
        $(eventTarget).prepend("<i class='fa fa-spinner fa-pulse fa-fw'></i>");

        sumoJS.ajaxPost<ICreateContentSectionDto>(
            Constants.url_Email_AddContentSection,
            {
                contentSectionDto: this.state.newContentSection,
                checkForSimilarContent
            },
            createContentSectionDto => {
                $(eventTarget).find("i").remove();

                if (createContentSectionDto.similarContentSection) {
                    this.setState({
                        isModified: true,
                        matchedContentSection: new ContentSection(createContentSectionDto.similarContentSection),
                        newContentSectionErrors: new Array<IFormError>()
                    });
                } else if (createContentSectionDto.formResponse.isValid) {
                    const savedContentSection = new ContentSection(createContentSectionDto.savedContentSection);

                    const email = sumoJS.deepClone(this.state.email);

                    email.contentSection = savedContentSection;

                    this.setState({
                        isModified: true,
                        email,
                        matchedContentSection: new ContentSection(),
                        newContentSectionErrors: new Array<IFormError>()
                    }, () => this.submit());

                    $("#new-content-section-add").modal("hide");
                } else {
                    this.setState({
                        isModified: false,
                        isSubmitting: false,
                        matchedContentSection: new ContentSection(),
                        newContentSectionErrors: createContentSectionDto.formResponse.errors
                    });
                }
            }
        );
    }

    handleUseExistingContentSection() {
        this.setState({
            email: new Email({ ...this.state.email, contentSection: this.state.matchedContentSection }),
            newContentSectionErrors: new Array<IFormError>(),
            matchedContentSection: new ContentSection()
        })

        $("#new-content-section-add").modal("hide");
    }

    handleSelectMailingList(e) {
        const selectedMailingList = this.state.allMailingLists.filter(x => x.id === e.target.value)[0];

        this.setState({
            selectedMailingList,
            selectedEmails: new Array<string>()
        }, () => this.handleSelectCountry(""));
    }

    handleSelectUserEmail(selectedValues: Array<string>) {
        this.setState({
            selectedEmails: selectedValues,
            selectedMailingList: null
        });
    }

    handleSelectMailingType(e) {
        this.setState({
            mailToType: e.target.value,
            selectedEmails: new Array<string>(),
            selectedMailingList: null,
            selectedCountry: null
        });
    }

    handleSelectCountry(value) {
        const selectedCountry = this.state.allCountries.filter(x => x.id === value)[0];

        this.setState({
            selectedCountry
        });
    }

    handleSendEmail() {
        this.setState({
            isSendingEmail: true
        });

        const url = this.state.mailToType === Constants.mail_To_Type_Single_Users ? Constants.url_Email_SendEmailToUsers : Constants.url_Email_SendEmailToMailingList;
        const data = this.state.mailToType === Constants.mail_To_Type_Single_Users ?
            {
                emailId: this.state.email.id,
                to: this.state.selectedEmails
            } :
            {
                emailId: this.state.email.id,
                mailingListId: this.state.selectedMailingList && this.state.selectedMailingList.id,
                countryId: this.state.selectedCountry && this.state.selectedCountry.id
            };

        sumoJS.ajaxPost(url, data, (formResponse: IFormResponse) => {
            if (formResponse.isValid) {
                this.setState({
                    formSuccessMessage: formResponse.successMessage,
                    isSendingEmail: false
                });
            } else {
                this.setState({
                    formErrors: formResponse.errors,
                    isSendingEmail: false
                });
            }
        });
    }

    setStateFromApi() {
        return sumoJS.ajaxPost<IEditEmailDto>(Constants.url_Email_EditJson,
            {
                id: sumoJS.getQueryStringParameter("id")
            },
            (editEmailDto) => {
                editEmailDto.email.contentSection = editEmailDto.email.contentSection || new ContentSection();

                this.setState({
                    authenticatedUser: new User(editEmailDto.authenticatedUser),
                    email: new Email(editEmailDto.email),
                    pageIsLoading: false,
                    viewNames: editEmailDto.viewNames,
                    allContentSections: editEmailDto.allContentSections,
                    contentSectionsSchemaNames: editEmailDto.contentSectionsSchemaNames
                }, () => {

                    Functions.setCreateAndEditPermissions(
                        this.state.email,
                        this.state.authenticatedUser.role.createEmail,
                        this.state.authenticatedUser.role.editEmail, $(".non-localized"));

                    const defaultLocalizedKitCountry =
                        sumoJS.get(() => this.state.email.emailLocalizedKits.filter(x => x.country.isDefault)[0].country, new Country());

                    Functions.setCreateAndEditPermissions(
                        this.state.email,
                        this.state.authenticatedUser.role.createEmail,
                        this.state.authenticatedUser.role.editEmail, $(`#${defaultLocalizedKitCountry.id}${defaultLocalizedKitCountry.id}`));
                });
            });
    }

    submit() {
        this.setState({
            isSubmitting: true
        });
        sumoJS.ajaxPost<IFormResponse>(Constants.url_Email_AddOrUpdate, this.state.email, response => {
            if (response.isValid) {
                if (response.redirectUrl) {
                    history.pushState({}, "", response.redirectUrl);
                }
                this.setStateFromApi();
                this.setState({
                    formSuccessMessage: Constants.alert_saveSuccess,
                    isModified: false,
                    isSubmitting: false
                });
            } else {
                this.setState({
                    formErrors: response.errors,
                    isModified: false,
                    isSubmitting: false
                });
            }
        });
    }

    renderContentSection() {
        const ContentSectionAutocomplete = Autocomplete as IContentSectionAutocomplete;
        return (
            <div className="row form-group">
                <div className="col-md-9">
                    <ContentSectionAutocomplete
                        label="Content Section"
                        options={this.state.allContentSections.map(x => new AutocompleteOption(x, x.name))}
                        selectedOption={
                            new AutocompleteOption(
                                this.state.email.contentSection,
                                this.state.email.contentSection && this.state.email.contentSection.name
                            )
                        }
                        handleChangeValue={(contentSection: ContentSection) => this.handleChangeContentSection(contentSection)}
                    />
                </div>
                <div className="col-md-3">
                    {
                        this.state.email.contentSection &&
                        <a
                            href={`${Constants.url_ContentSection_Index}?filter=${this.state.email.contentSection.name}`}
                            target="_blank"
                            title="Edit"
                            className="btn btn-default">
                            <i className="fa fa-edit" />
                        </a>
                    }
                    <button type="button" title="Create" className="btn btn-default" onClick={() => this.handleOpenNewContentSectionModal()}>
                        <i className="fa fa-plus" />
                    </button>
                </div>
            </div>
        );
    }

    handleOpenSendConfirmationModal() {
        $("#confirm-send-modal").modal("show");
    }

    render() {
        return (
            <div className="edit-email-component">
                <div className="admin-top-bar">
                    <div className="admin-top-bar-title">
                        {
                            this.state.email.isNew ? "New Email" : "Edit Email"
                        }
                    </div>
                    <div className="admin-top-bar-controls">
                        <a href={Constants.url_Email_Index} className="btn btn-link">
                            Back to list
                        </a>
                        {
                            !this.state.email.isNew &&
                            <ButtonDeleteComponent {...{
                                handleDelete: () => this.handleEmailDelete(),
                                canDelete: this.state.authenticatedUser.role.deleteEmail,
                                isDeleting: this.state.isDeleting
                            } as IButtonDeleteProps} />
                        }
                        <ButtonSubmitComponent {...{
                            handleSubmit: () => this.submit(),
                            isSubmitting: this.state.isSubmitting
                        } as IButtonSubmitProps}/>
                    </div>
                </div>
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps}/>
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-5">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Global properties
                                </div>
                                <TextboxComponent {...{
                                    label: "Name",
                                    value: this.state.email.name,
                                    className: "form-group",
                                    type: "text",
                                    readOnly: Functions.getReadOnlyStateForRequiredField(
                                        this.state.email,
                                        this.state.authenticatedUser.role.editEmailName),
                                    handleChangeValue: (e) => this.handleChangeEmailName(e)
                                } as ITextboxProps} />

                                <SelectComponent {...{
                                    options: this.state.viewNames.map(x => <option key={Functions.newGuid().substring(0, 8)} value={x}> {x} </option>),
                                    value: this.state.email.viewName,
                                    label: "View Template",
                                    neutralOption: this.state.viewNames.length === 0
                                        ? <option value="">- No Templates available -</option>
                                        : <option value="">- Select a Template -</option>,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeViewTemplate(e)
                                } as ISelectProps} />

                                {
                                    this.renderContentSection()
                                }

                            </div>
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Localized Properties
                                </div>
                                <LocalizedKitsComponent {...{
                                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source, target, completed),
                                    editAllLocalizedKits: this.state.authenticatedUser.role.editEmailAllLocalizedKits,
                                    localizedKitUIs: this.state.email.emailLocalizedKits.map((emailLocalizedKit, i) => {
                                        return {
                                            localizedKit: emailLocalizedKit,
                                            html: <React.Fragment>
                                                <TextboxComponent {...{
                                                    label: "From",
                                                    value: emailLocalizedKit.from,
                                                    placeholder: this.state.email.emailLocalizedKits.filter(x => x.country.isDefault)[0].from,
                                                    className: "form-group",
                                                    type: "text",
                                                    handleChangeValue: (e) => this.handleChangeEmailLocalizedFrom(e, emailLocalizedKit),
                                                } as ITextboxProps} />
                                                <TextboxComponent {...{
                                                    label: "Reply to",
                                                    value: emailLocalizedKit.replyTo,
                                                    placeholder: this.state.email.emailLocalizedKits.filter(x => x.country.isDefault)[0].replyTo,
                                                    className: "form-group",
                                                    type: "text",
                                                    handleChangeValue: (e) => this.handleChangeEmailLocalizedReplyTo(e, emailLocalizedKit),
                                                } as ITextboxProps} />
                                                <TextboxComponent {...{
                                                    label: "Display Name",
                                                    value: emailLocalizedKit.displayName,
                                                    placeholder: this.state.email.emailLocalizedKits.filter(x => x.country.isDefault)[0].displayName,
                                                    className: "form-group",
                                                    type: "text",
                                                    handleChangeValue: (e) => this.handleChangeEmailLocalizedDisplayName(e, emailLocalizedKit),
                                                } as ITextboxProps} />
                                            </React.Fragment>
                                        } as ILocalizedKitUI;
                                    })
                                } as ILocalizedKitsProps} />
                            </div>
                        </div>
                        <div className="col-md-7">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Preview
                                </div>
                                {
                                    !this.state.email.isNew &&
                                    <iframe
                                        scrolling="no"
                                        style={{ width: "100%", border: "1px solid lightgray" }}
                                        src={Constants.url_Email_ViewInBrowser + this.state.email.id}
                                        onLoad={(e) => (e.target as any).style.height = ((e.target as any).contentWindow.document.body.scrollHeight + 20) + 'px'}/>
                                }
                            </div>
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">Send to</div>
                                <SelectComponent
                                    options={[
                                        <option key="" value=""></option>,
                                        <option key={Constants.mail_To_Type_Mailing_List} value={Constants.mail_To_Type_Mailing_List}> Mailing List</option>,
                                        <option key={Constants.mail_To_Type_Single_Users} value={Constants.mail_To_Type_Single_Users}> Single Users </option>,
                                    ]}
                                    value={this.state.mailToType}
                                    label="Choose Mailing list or single users"
                                    className="form-group"
                                    handleChangeValue={(e) => this.handleSelectMailingType(e)} />
                                {
                                    this.state.mailToType === Constants.mail_To_Type_Mailing_List &&
                                    <SelectComponent
                                        options={[
                                            <option key="" value=""></option>,
                                            ...this.state.allMailingLists.map(x => (<option key={x.id} value={x.id}>{x.localizedTitle}</option>))
                                        ]}
                                        label="Mailing Lists"
                                        className="form-group"
                                        handleChangeValue={(e) => this.handleSelectMailingList(e)}
                                    />
                                }
                                {
                                    this.state.mailToType === Constants.mail_To_Type_Mailing_List && this.state.selectedMailingList &&
                                    <SelectComponent
                                        options={[
                                            <option key="" value="">Send to all users in the mailing list</option>,
                                            ...this.state.allCountries.map(x => <option key={x.id} value={x.id}>Send only to users in {x.name}</option>)
                                        ]}
                                        value={this.state.selectedCountry ? this.state.selectedCountry.id : ""}
                                        label="Country filter"
                                        className="form-group"
                                        handleChangeValue={(e) => this.handleSelectCountry(e.target.value)}
                                    />
                                }
                                {
                                    this.state.mailToType === Constants.mail_To_Type_Single_Users &&
                                    <MultiSelectComponent
                                        options={this.state.allUsers.map(x => (new SmartOption(x.email, x.email, x.fullName)))}
                                        label="Users"
                                        className="form-group"
                                        handleChangeValue={(selectedValues) => this.handleSelectUserEmail(selectedValues)}
                                    />
                                }
                                {
                                    <button
                                        className="btn btn-default btn-block btn-submit"
                                        type="button"
                                        data-toggle="modal"
                                        disabled={this.state.isSendingEmail || (this.state.mailToType === Constants.mail_To_Type_Single_Users
                                                ? this.state.selectedEmails.length < 1 : !this.state.selectedMailingList)}
                                        onClick={() => this.handleOpenSendConfirmationModal()}>
                                        {this.state.isSendingEmail && <i className="fa fa-spinner fa-pulse fa-fw"></i>}
                                        Send
                                    </button>
                                }
                            </div>
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }

                <ModalComponent {...{
                    id: "confirm-send-modal",
                    title: "Confirmation",
                    body:
                        <span>
                            Are you sure you want to proceed?
                        </span>,
                    footer:
                        <span>
                            <a className="btn btn-primary" data-dismiss="modal" onClick={() => this.handleSendEmail()}>
                                Confirm
                            </a>
                            <button type="button" className="btn btn-default" data-dismiss="modal">
                                Cancel
                            </button>
                        </span>
                } as IModalProps} />

                <ModalComponent
                    {...{
                        id: "new-content-section-add",
                        title: "New Content Section",
                        body: (
                            <div>
                                <div className="modal-errors">
                                    {this.state.newContentSectionErrors.map((x, i) => (
                                        <p key={i}>{x.message}</p>
                                    ))}
                                </div>
                                {this.state.matchedContentSection.name && (
                                    <p>
                                        {`A Content Section with name "${this.state.matchedContentSection.name}" exists with similar content "${
                                            this.state.matchedContentSection.contentSectionLocalizedKits[0].content
                                            }". Do you want to associate this content to the Email "${this.state.email.name}" ?`}
                                    </p>
                                )}
                                {
                                    !this.state.matchedContentSection.name &&
                                    <ContentSectionLocalizedKitEditor
                                        content={this.state.newContentSection.contentSectionLocalizedKits[0].content}
                                        contentSectionName={this.state.newContentSection.name}
                                        contentSectionSchemaNames={this.state.contentSectionsSchemaNames}
                                        contentSectionSchema={this.state.newContentSection.schema}
                                        canEditContentSectionName={!Functions.getReadOnlyStateForRequiredField(this.state.email.contentSection, this.state.authenticatedUser.role.editContentSectionName)}
                                        canEditContentSectionContent={!Functions.getReadOnlyStateForRequiredField(this.state.email.contentSection, this.state.authenticatedUser.role.editContentSectionAllLocalizedKits)}
                                        handleContentSectionNameChange={value => this.handleChangeNewContentSectionName(value)}
                                        handleContentSectionSchemaChange={value => this.handleChangeNewContentSectionContentSchema(value)}
                                        handleContentChange={value => this.handleChangeNewContentSectionContent(value)}
                                    />
                                }
                            </div>
                        ),
                        footer: (
                            <div>
                                {this.state.matchedContentSection.name && (
                                    <div>
                                        <button className="btn btn-primary" type="button" onClick={() => this.handleUseExistingContentSection()}>
                                            Use Existing
                                    </button>
                                        <button type="button" className="btn btn-default" onClick={e => this.handleSaveContentSection(e.target, false)}>
                                            Create New
                                    </button>
                                    </div>
                                )}
                                {!this.state.matchedContentSection.name && (
                                    <div>
                                        <button className="btn btn-primary" type="button" onClick={e => this.handleSaveContentSection(e.target, true)}>
                                            Create
                                        </button>
                                        <button type="button" className="btn btn-default" data-dismiss="modal">
                                            Cancel
                                        </button>
                                    </div>
                                )}
                            </div>
                        )
                    } as IModalProps}/>
            </div>
        );
    }
}