﻿import { ICategoryDto } from "../../../../../Scripts/interfaces/ICategoryDto";
import { IProductDto } from "../../../../../Scripts/interfaces/IProductDto";
import { IUserDto } from "../../../../../Scripts/interfaces/IUserDto";
import { IProductImageKitDto } from "../../../../../Scripts/interfaces/IProductImageKitDto";
import { IProductStockUnitDto } from "../../../../../Scripts/interfaces/IProductStockUnitDto";

export interface IEditCategoryDto {
    authenticatedUser: IUserDto;
    category: ICategoryDto;
    allCategories: Array<ICategoryDto>;
    allTags: Array<string>;
    allProducts: Array<IProductDto>;
    allProductImageKits?: Array<IProductImageKitDto>;
    allProductStockUnits?: Array<IProductStockUnitDto>;
}
