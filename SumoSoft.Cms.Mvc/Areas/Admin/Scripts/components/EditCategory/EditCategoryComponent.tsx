﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { Constants } from "../../shared/Constants";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";
import { AlertComponent, IAlertProps } from "../Alert/AlertComponent";
import { SpinnerComponent } from "../Spinner/SpinnerComponent";
import { ButtonSubmitComponent, IButtonSubmitProps } from "../ButtonSubmit/ButtonSubmitComponent";
import { ButtonDeleteComponent, IButtonDeleteProps } from "../ButtonDelete/ButtonDeleteComponent";
import { TextboxComponent, ITextboxProps } from "../Textbox/TextboxComponent";
import { NestedInputComponent, INestedInputProps } from "../NestedInput/NestedInputComponent";
import { Category } from "../../../../../Scripts/classes/Category";
import { CategoryLocalizedKit } from "../../../../../Scripts/classes/CategoryLocalizedKit";
import { LocalizedKitsComponent, ILocalizedKitUI, ILocalizedKitsProps } from "../LocalizedKits/LocalizedKitsComponent";
import { IEditCategoryState } from "./IEditCategoryState";
import { IEditCategoryDto } from "./IEditCategoryDto";
import { MultiSelectComponent, IMultiSelectProps, SmartOption } from "../MultiSelect/MultiSelectComponent";
import { Product } from "../../../../../Scripts/classes/Product";
import { User } from "../../../../../Scripts/classes/User";
import { UserRole } from "../../../../../Scripts/classes/UserRole";
import { CheckboxComponent, ICheckboxProps } from "../Checkbox/CheckboxComponent";
import { FilePickerComponent, IFilePickerProps } from "../FilePicker/FilePickerComponent";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";
import { CategoryTypesEnum } from "../../../../../Scripts/enums/CategoryTypesEnum";
import { ProductImageKit } from "../../../../../Scripts/classes/ProductImageKit";
import { ProductStockUnit } from "../../../../../Scripts/classes/ProductStockUnit";
import { TagsComponent, ITagsProps } from "../Tags/TagsComponent";
import { ICategoryLocalizedKitDto } from "../../../../../Scripts/interfaces/ICategoryLocalizedKitDto";
import { Functions } from "../../shared/Functions";
import { TextareaComponent, ITextareaProps } from "../Textarea/TextareaComponent";
import { SortableAutocompleteComponent, ISortableAutocompleteProps, SortableAutocompleteOption } from "../SortableAutocomplete/SortableAutocompleteComponent"
import { IFormError } from "../../../../../Scripts/interfaces/IFormError";

interface IProductSortableAutocomplete {
    new(): SortableAutocompleteComponent<Product>;
}

interface IProductImageKitSortableAutocomplete {
    new(): SortableAutocompleteComponent<ProductImageKit>;
}

interface IProductStockUnitSortableAutocomplete {
    new(): SortableAutocompleteComponent<ProductStockUnit>;
}

export default class EditCategoryComponent extends ReactPageComponent<{}, IEditCategoryState> {

    constructor(props) {
        super(props);

        this.state = {
            pageIsLoading: true,
            formErrors: new Array<IFormError>(),
            category: new Category(),
            allCategories: new Array<Category>(),
            allTags: new Array<string>(),
            allProducts: new Array<Product>(),
            allProductImageKits: new Array<ProductImageKit>(),
            allProductStockUnits: new Array<ProductStockUnit>(),
            authenticatedUser: new User({ role: new UserRole() })
        } as IEditCategoryState;

    }

    componentDidMount() {
        this.setStateFromApi(true).done(() => {
            sumoJS.ajaxPost<Array<Product>>(Constants.url_Category_GetAllProductsJson,
                null,
                (allProducts) => {
                    this.setState({
                        allProducts,
                    });
                });
            sumoJS.ajaxPost<Array<ProductImageKit>>(Constants.url_Category_GetAllProductImageKitsJson,
                null,
                (allProductImageKits) => {
                    this.setState({
                        allProductImageKits,
                    });
                });
            //sumoJS.ajaxPost<Array<ProductStockUnit>>(Constants.url_EcommerceCategory_GetAllProductStockUnitsJson,
            //    null,
            //    (allProductStockUnits) => {
            //        this.setState({
            //            allProductStockUnits,
            //        });
            //    });
        });
    }

    handleChangeCategoryName(e) {
        this.state.category.name = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleUpdateUrlOnBlur(source: string) {
        const category = this.state.category;
        if (!category.url || category.url === "") {
            category.url = Functions.convertToUrl(source);
            this.setState({
                category
            });
        }
    }

    handleChangeCategoryUrl(e) {
        this.state.category.url = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategorySortOrder(e) {
        this.state.category.sortOrder = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitTitle(e, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.title = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedImage(absoluteUrl, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.image = absoluteUrl;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitDescription(e, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.description = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitFeaturedTitle(e, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.featuredTitle = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitFeaturedDescription(e, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.featuredDescription = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitFeaturedImage(absoluteUrl, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.featuredImage = absoluteUrl;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitMetaTitle(e, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.metaTitle = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitMetaDescription(e, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.metaDescription = e.target.value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryLocalizedKitIsDisabled(e, categoryLocalizedKit: CategoryLocalizedKit) {
        categoryLocalizedKit.isDisabled = e.target.checked;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleSelectParent(newValue: string) {
        this.state.category.parent = this.state.allCategories.filter(x => x.id === newValue)[0];
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleSelectChildren(newValue: Array<string>) {
        this.state.category.children = newValue.map(categoryId => this.state.allCategories.filter(x => x.id === categoryId)[0]);
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleDeleteCategory() {
        this.setState({ isDeleting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Category_Delete, { id: this.state.category.id }, response => {
            if (response.isValid) {
                window.location.href = Constants.url_Category_Index;
            } else {
                this.setState({
                    isDeleting: false,
                    formErrors: this.state.formErrors.concat(response.errors)
                });
            }

            this.setState({ isModified: false, isSubmitting: false, isDeleting: false, formErrors: this.state.formErrors });
        });
    }

    handleSelectCategoryProduct(newValue: Array<Product>) {
        this.state.category.products = newValue;
        this.state.category.productsSortOrder = newValue.map(x => x.id).join(",");
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleSelectCategoryProductImageKit(newValue: Array<ProductImageKit>) {
        this.state.category.productImageKits = newValue;
        this.state.category.productImageKitsSortOrder = newValue.map(x => x.id).join(",");
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleSelectCategoryProductStockUnit(newValue: Array<ProductStockUnit>) {
        this.state.category.productStockUnits = newValue;
        this.state.category.productStockUnitsSortOrder = newValue.map(x => x.id).join(",");
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleSelectCategoryType(e) {
        const value = parseInt(e.target.value);
        this.state.category.type = value;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleChangeCategoryIsDisabled(e) {
        this.state.category.isDisabled = e.target.checked;
        this.setState({ category: this.state.category });
    }

    handleChangeCategoryTags(newValue) {
        this.state.category.tags = newValue;
        this.setState({
            isModified: true,
            category: this.state.category
        });
    }

    handleLocalizedKitCopyValuesFrom(source: ICategoryLocalizedKitDto, target: ICategoryLocalizedKitDto, completed) {
        target.title = source.title;
        target.description = source.description;
        target.featuredTitle = source.featuredTitle;
        target.featuredDescription = source.featuredDescription;
        target.isDisabled = source.isDisabled;

        this.setState({
            isModified: true,
            category: this.state.category
        }, () => completed());
    }

    submit() {
        this.setState({ isSubmitting: true });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Category_AddOrUpdate, this.state.category, response => {
            if (response.isValid) {
                if (response.redirectUrl) history.pushState({}, "", response.redirectUrl);

                this.setStateFromApi(false);
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formSuccessMessage: Constants.alert_saveSuccess
                });
            } else {
                this.setState({
                    isModified: false,
                    isSubmitting: false,
                    formErrors: response.errors
                });
            }
        });
    }

    setStateFromApi(isFirstLoad: boolean) {

        return sumoJS.ajaxPost<IEditCategoryDto>(Constants.url_Category_EditJson,
            {
                categoryId: sumoJS.getQueryStringParameter("categoryId")
            },
            (editCategoryDto) => {
                this.setState({
                    category: new Category(editCategoryDto.category),
                    authenticatedUser: new User(editCategoryDto.authenticatedUser),
                    allCategories: editCategoryDto.allCategories.map(x => new Category(x)),
                    allTags: editCategoryDto.allTags,
                    allProducts: isFirstLoad ? editCategoryDto.allProducts.map(x => new Product(x)) : this.state.allProducts,
                    allProductImageKits: isFirstLoad ? editCategoryDto.allProductImageKits.map(x => new ProductImageKit(x)) : this.state.allProductImageKits,
                    allProductStockUnits: isFirstLoad ? editCategoryDto.allProductStockUnits.map(x => new ProductStockUnit(x)) : this.state.allProductStockUnits,
                    pageIsLoading: false
                }, () => {
                    Functions.setCreateAndEditPermissions(
                        this.state.category,
                        this.state.authenticatedUser.role.createCategory,
                        this.state.authenticatedUser.role.editCategory);
                });
            });
    }

    render() {
        const SortableAutocompleteProduct = SortableAutocompleteComponent as IProductSortableAutocomplete;
        const SortableAutocompleteProductImageKitComponent = SortableAutocompleteComponent as IProductImageKitSortableAutocomplete;
        const SortableAutocompleteProductStockUnit = SortableAutocompleteComponent as IProductStockUnitSortableAutocomplete;

        return (
            <div className="edit-category-component">
                {
                    this.state.category.isNew &&
                    <div className="admin-top-bar">
                        <div className="admin-top-bar-title">
                            Create Category
                        </div>
                        <div className="admin-top-bar-controls">
                            <a href={Constants.url_Category_Index} className="btn btn-link">
                                Cancel
                            </a>
                            <ButtonSubmitComponent {...{
                                isSubmitting: this.state.isSubmitting,
                                handleSubmit: () => this.submit()
                            } as IButtonSubmitProps} />
                        </div>
                    </div>
                }
                {
                    !this.state.category.isNew &&
                    <div className="admin-top-bar">
                        <div className="admin-top-bar-title">
                            Edit Category
                        </div>
                        <div className="admin-top-bar-controls">
                            <a href={Constants.url_Category_Index} className="btn btn-link">
                                Back to list
                            </a>
                            <ButtonDeleteComponent {...{
                                isDeleting: this.state.isDeleting,
                                canDelete: this.state.authenticatedUser.role.deleteCategory,
                                handleDelete: () => this.handleDeleteCategory()
                            } as IButtonDeleteProps} />
                            <ButtonSubmitComponent {...{
                                isSubmitting: this.state.isSubmitting,
                                handleSubmit: () => this.submit()
                            } as IButtonSubmitProps} />
                        </div>
                    </div>
                }
                <AlertComponent {...{
                    errorMessages: this.getFormErrorMessages(),
                    successMessage: this.state.formSuccessMessage,
                    handleHideAlert: () => this.handleHideAlert()
                } as IAlertProps} />
                {
                    !this.state.pageIsLoading &&
                    <div className="row">
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Global properties
                                </div>
                                <TextboxComponent {...{
                                    label: "Name",
                                    type: "text",
                                    value: this.state.category.name,
                                    className: "form-group",
                                    readOnly: Functions.getReadOnlyStateForRequiredField(
                                        this.state.category,
                                        this.state.authenticatedUser.role.editCategoryName),
                                    handleBlur: (e) => this.handleUpdateUrlOnBlur(e.target.value),
                                    handleChangeValue: (e) => this.handleChangeCategoryName(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Url",
                                    type: "text",
                                    value: this.state.category.url,
                                    className: "form-group",
                                    readOnly: Functions.getReadOnlyStateForRequiredField(
                                        this.state.category,
                                        this.state.authenticatedUser.role.editCategoryUrl),
                                    handleChangeValue: (e) => this.handleChangeCategoryUrl(e)
                                } as ITextboxProps} />
                                <TextboxComponent {...{
                                    label: "Sort Order",
                                    type: "number",
                                    value: this.state.category.sortOrder,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCategorySortOrder(e),
                                } as ITextboxProps} />
                                <SelectComponent {...{
                                    options: this.state.allCategories.map(x => <option key={x.id} value={x.id}>{x.localizedTitle}</option>),
                                    neutralOption: <option value=""></option>,
                                    value: sumoJS.get<string>(() => this.state.category.parent.id, ""),
                                    label: "Parent Category",
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleSelectParent(e.target.value)
                                } as ISelectProps} />
                                <MultiSelectComponent {...{
                                    options: this.state.allCategories.map(x => new SmartOption(x.id, x.localizedTitle)),
                                    value: this.state.category.children.map(category => category.id),
                                    label: "Children Categories",
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectChildren(newValue)
                                } as IMultiSelectProps} />
                                <TagsComponent {...{
                                    value: this.state.category.tags,
                                    allTags: this.state.allTags,
                                    handleChangeValue: (newValue) => this.handleChangeCategoryTags(newValue),
                                    className: "form-group"
                                } as ITagsProps} />
                                <SelectComponent {...{
                                    options: Object.keys(CategoryTypesEnum).filter(x => typeof CategoryTypesEnum[x] !== "number").map((value, i) =>
                                        <option key={value} value={value}>{this.state.category.typeDescriptions[i]}</option>),
                                    value: this.state.category.type ? this.state.category.type.toString() : CategoryTypesEnum.Products.toString(),
                                    label: "Type",
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleSelectCategoryType(e)
                                } as ISelectProps} />
                                <SortableAutocompleteProduct {... {
                                    placeholder: "Add options...",
                                    options: this.state.allProducts.map(x => new SortableAutocompleteOption(x, x.localizedTitle)),
                                    selectedOptionIds: this.state.category.products.map(product => product.id),
                                    label: this.state.category.typeDescriptions[CategoryTypesEnum.Products],
                                    disabled: this.state.category.type !== CategoryTypesEnum.Products,
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectCategoryProduct(newValue)
                                } as ISortableAutocompleteProps<Product>} />
                                <SortableAutocompleteProductImageKitComponent {...{
                                    placeholder: "Add options...",
                                    options: this.state.allProductImageKits.map(pik => new SortableAutocompleteOption(pik, pik.product.localizedTitle + ": " + pik.variantOptions.map(vo => vo.localizedTitle).join(", "))),
                                    selectedOptionIds: this.state.category.productImageKits.map(x => x.id),
                                    label: this.state.category.typeDescriptions[CategoryTypesEnum.ProductImageKits],
                                    disabled: this.state.category.type !== CategoryTypesEnum.ProductImageKits,
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectCategoryProductImageKit(newValue)
                                } as ISortableAutocompleteProps<ProductImageKit>} />
                                <SortableAutocompleteProductStockUnit {...{
                                    placeholder: "Add options...",
                                    options: this.state.allProductStockUnits.map(psu => new SortableAutocompleteOption(psu, psu.product.localizedTitle + ": " + psu.variantOptions.map(vo => vo.localizedTitle).join(", "))),
                                    selectedOptionIds: this.state.category.productStockUnits.map(x => x.id),
                                    label: this.state.category.typeDescriptions[CategoryTypesEnum.ProductStockUnits],
                                    disabled: this.state.category.type !== CategoryTypesEnum.ProductStockUnits,
                                    className: "form-group",
                                    handleChangeValue: (newValue) => this.handleSelectCategoryProductStockUnit(newValue)
                                } as ISortableAutocompleteProps<ProductStockUnit>} />
                                <CheckboxComponent {...{
                                    text: "Disabled",
                                    value: this.state.category.isDisabled || false,
                                    className: "form-group",
                                    handleChangeValue: (e) => this.handleChangeCategoryIsDisabled(e)
                                } as ICheckboxProps} />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="admin-subsection">
                                <div className="admin-subsection-title">
                                    Localized Properties
                                </div>
                                <LocalizedKitsComponent {...{
                                    handleCopyValuesFrom: (source, target, completed) => this.handleLocalizedKitCopyValuesFrom(source as ICategoryLocalizedKitDto, target as ICategoryLocalizedKitDto, completed),
                                    editAllLocalizedKits: this.state.authenticatedUser.role.editCategoryAllLocalizedKits,
                                    localizedKitUIs: this.state.category.categoryLocalizedKits.map((categoryLocalizedKit, i) => {
                                        return {
                                            localizedKit: categoryLocalizedKit,
                                            html:
                                                <div>
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Title",
                                                            type: "text",
                                                            value: categoryLocalizedKit.title,
                                                            placeholder: this.state.category.categoryLocalizedKits.filter(x => x.country.isDefault)[0].title,
                                                            handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitTitle(e, categoryLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "Description",
                                                            type: "text",
                                                            value: categoryLocalizedKit.description,
                                                            placeholder: this.state.category.categoryLocalizedKits.filter(x => x.country.isDefault)[0].description,
                                                            handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitDescription(e, categoryLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <FilePickerComponent {...{
                                                            label: "Image",
                                                            type: "text",
                                                            value: categoryLocalizedKit.image,
                                                            className: "form-group",
                                                            handleChangeValue: (absoluteUrl) => this.handleChangeCategoryLocalizedImage(absoluteUrl, categoryLocalizedKit)
                                                        } as IFilePickerProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Featured Title",
                                                            type: "text",
                                                            value: categoryLocalizedKit.featuredTitle,
                                                            placeholder: this.state.category.categoryLocalizedKits.filter(x => x.country.isDefault)[0].featuredTitle,
                                                            handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitFeaturedTitle(e, categoryLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "Featured Description",
                                                            type: "text",
                                                            value: categoryLocalizedKit.featuredDescription,
                                                            placeholder: this.state.category.categoryLocalizedKits.filter(x => x.country.isDefault)[0].featuredDescription,
                                                            handleChangeValue: (absoluteUrl) => this.handleChangeCategoryLocalizedKitFeaturedDescription(absoluteUrl, categoryLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <FilePickerComponent {...{
                                                            label: "Featured Image",
                                                            type: "text",
                                                            value: categoryLocalizedKit.featuredImage,
                                                            className: "form-group",
                                                            handleChangeValue: (absoluteUrl) => this.handleChangeCategoryLocalizedKitFeaturedImage(absoluteUrl, categoryLocalizedKit)
                                                        } as IFilePickerProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextboxComponent {...{
                                                            label: "Meta Title",
                                                            type: "text",
                                                            value: categoryLocalizedKit.metaTitle,
                                                            placeholder: this.state.category.categoryLocalizedKits.filter(x => x.country.isDefault)[0].metaTitle,
                                                            handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitMetaTitle(e, categoryLocalizedKit)
                                                        } as ITextboxProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <TextareaComponent {...{
                                                            label: "Meta Description",
                                                            type: "text",
                                                            value: categoryLocalizedKit.metaDescription,
                                                            placeholder: this.state.category.categoryLocalizedKits.filter(x => x.country.isDefault)[0].metaDescription,
                                                            handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitMetaDescription(e, categoryLocalizedKit)
                                                        } as ITextareaProps} />
                                                    } as INestedInputProps} />
                                                    <NestedInputComponent {...{
                                                        input: <CheckboxComponent {...{
                                                            text: "Disabled",
                                                            value: categoryLocalizedKit.isDisabled,
                                                            className: "form-group",
                                                            handleChangeValue: (e) => this.handleChangeCategoryLocalizedKitIsDisabled(e, categoryLocalizedKit)
                                                        } as ICheckboxProps} />
                                                    } as INestedInputProps} />
                                                </div>
                                        } as ILocalizedKitUI;
                                    })
                                } as ILocalizedKitsProps} />
                            </div>
                        </div>
                    </div>
                }
                {
                    this.state.pageIsLoading && <SpinnerComponent />
                }
            </div>
        );
    }
}