﻿import { IReactPageState } from "../ReactPage/ReactPageComponent";
import { Category } from "../../../../../Scripts/classes/Category";
import { Product } from "../../../../../Scripts/classes/Product";
import { ProductImageKit } from "../../../../../Scripts/classes/ProductImageKit";
import { ProductStockUnit } from "../../../../../Scripts/classes/ProductStockUnit";

export interface IEditCategoryState extends IReactPageState {
    category?: Category;
    allCategories?: Array<Category>;
    allTags?: Array<string>;
    allProducts?: Array<Product>;
    allProductImageKits?: Array<ProductImageKit>;
    allProductStockUnits?: Array<ProductStockUnit>;
}