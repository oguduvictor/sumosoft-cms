﻿import * as React from "react";

export interface INestedInputProps {
    input: JSX.Element;
}

export var NestedInputComponent: (props: INestedInputProps) => JSX.Element = props => {
    return (
        <div className="nested-input-component clearfix" >
            <div className="input-marker"></div>
            {props.input}
        </div>
    );
}