﻿import * as React from "react";
import * as sumoJS from "../../../../../Scripts/sumoJS";

import { Constants } from "../../shared/Constants";
import { IFormResponse } from "../../../../../Scripts/interfaces/IFormResponse";
import { IRegisterDetails, RegisterForm } from "../../../../../Scripts/sumoReact/RegisterForm/RegisterForm";
import { IRegisterState } from "./IRegisterState";
import { ReactPageComponent } from "../ReactPage/ReactPageComponent";

export default class RegisterComponent extends ReactPageComponent<{}, IRegisterState> {
    constructor(props) {
        super(props);

        this.state = {
            formResponse: {} as IFormResponse,
            isSubmitting: false
        } as IRegisterState;
    }

    getReturnUrl = () => {
        const loginUrl = "/admin/account/login";
        const returnUrl = sumoJS.getQueryStringParameter("returnUrl");

        if (returnUrl && returnUrl.length > 0) {
            return `${loginUrl}?returnUrl=${returnUrl}`;
        }

        return loginUrl;
    };

    handleSubmit = (registerDetails: IRegisterDetails) => {
        this.setState({ isSubmitting: true, formResponse: {} as IFormResponse });

        sumoJS.ajaxPost<IFormResponse>(Constants.url_Account_RegisterForm, { registerDetails }, formResponse => {
            if (formResponse.isValid) {
                window.location.href = formResponse.redirectUrl;
            } else {
                this.setState({
                    formResponse,
                    isSubmitting: false
                });
            }
        });
    };

    render() {
        const { formResponse, isSubmitting } = this.state;
        return (
            <div className="register-component">
                <div className="form-container">
                    <h1>Register</h1>
                    <RegisterForm
                        registerButtonClassName="form__button"
                        isSubmitting={isSubmitting}
                        formResponse={formResponse}
                        onSubmit={this.handleSubmit}
                        loginUrl={this.getReturnUrl()}
                        loginUrlTitle="Click here"
                        enableFacebookRegister={false}
                        enableGoogleRegister={false}
                    />
                </div>
            </div>
        );
    }
}
