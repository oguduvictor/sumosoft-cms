﻿import {IRegisterDetailsDto} from "./IRegisterDetailsDto";

export class RegisterDetailsDto implements IRegisterDetailsDto {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}