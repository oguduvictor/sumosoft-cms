﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.EmailServices.Interfaces;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    [AuthorizeAdmin]
    public class CartController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IUtilityService utilityService;
        private readonly ICmsService cmsService;
        private readonly IEmailService emailService;
        private readonly IValidator<SendEmailConfirmationModalDto> sendEmailConfirmationModalDtoValidator;

        public CartController(
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            ICmsService cmsService,
            IEmailService emailService,
            IValidator<SendEmailConfirmationModalDto> sendEmailConfirmationModalDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.utilityService = utilityService;
            this.cmsService = cmsService;
            this.emailService = emailService;
            this.sendEmailConfirmationModalDtoValidator = sendEmailConfirmationModalDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson(int page = 1, string keywordFilter = null, string countryNameFilter = null, string shippingBoxNameFilter = null)
        {
            const int pageSize = 20;

            var filteredCarts = this.requestDbContext.Carts.Where(c =>
                c.User != null &&
                c.CartShippingBoxes.Any());

            if (!string.IsNullOrEmpty(keywordFilter))
            {
                filteredCarts = filteredCarts.Where(cart =>
                    cart.User.Email.Contains(keywordFilter) ||
                    cart.User.FirstName.Contains(keywordFilter) ||
                    cart.User.LastName.Contains(keywordFilter));
            }

            if (!string.IsNullOrEmpty(countryNameFilter))
            {
                filteredCarts = filteredCarts.Where(cart => cart.User.Country.Name.Contains(countryNameFilter));
            }

            if (!string.IsNullOrEmpty(shippingBoxNameFilter))
            {
                filteredCarts = filteredCarts.Where(cart => cart.CartShippingBoxes.Any(cartShippingBox => cartShippingBox.ShippingBox.Name.Contains(shippingBoxNameFilter)));
            }

            var dto = new CartListDto
            {
                TotalItems = filteredCarts.Count(),
                PageSize = pageSize,
                Carts = filteredCarts
                    .Select(x => new
                    {
                        Cart = x,
                        ModifiedDate = x.CartShippingBoxes
                            .SelectMany(csb => csb.CartItems)
                            .Select(ci => ci.ModifiedDate)
                            .OrderByDescending(md => md)
                            .FirstOrDefault()
                    })
                    .OrderByDescending(c => c.ModifiedDate)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .AsEnumerable()
                    .Select(x => new CartDto
                    {
                        Id = x.Cart.Id,
                        ModifiedDate = x.ModifiedDate,
                        TotalAfterTax = x.Cart.GetTotalAfterTax(this.requestDbContext, x.Cart.User?.Country?.Taxes, x.Cart.User?.Country?.LanguageCode),
                        User = x.Cart.User == null ? null : new UserDto
                        {
                            Email = x.Cart.User.Email ?? "-",
                            Country = x.Cart.User.Country == null ? null : new CountryDto
                            {
                                Name = x.Cart.User.Country.Name,
                                CurrencySymbol = x.Cart.User.Country.GetCurrencySymbol()
                            }
                        }
                    }).ToList(),
                ShippingBoxes = this.requestDbContext.ShippingBoxes.Select(shippingBox => new ShippingBoxDto
                {
                    Id = shippingBox.Id,
                    Name = shippingBox.Name,
                    LocalizedInternalDescription = shippingBox.ShippingBoxLocalizedKits.FirstOrDefault(x => x.Country.IsDefault).InternalDescription
                }).ToList(),
                Countries = this.requestDbContext.Countries.OrderBy(x => x.Name).Select(country => new CountryDto
                {
                    Id = country.Id,
                    Name = country.Name
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid cartId)
        {
            var cart = this.requestDbContext.Carts.Find(cartId);

            var country = cart?.User?.Country;
            var taxes = country?.Taxes ?? new List<Tax>();
            var languageCode = country?.LanguageCode;

            var editCartDto = new EditCartDto
            {
                Cart = cart == null ? null : new CartDto
                {
                    Id = cart.Id,
                    CreatedDate = cart.CreatedDate,
                    User = cart.User == null ? new UserDto() : new UserDto
                    {
                        Email = cart.User.Email,
                        FirstName = cart.User.FirstName,
                        Id = cart.User.Id,
                        LastName = cart.User.LastName,
                        Country = cart.User.Country == null ? null : new CountryDto
                        {
                            Id = cart.User.Country.Id,
                            Name = cart.User.Country.Name,
                            Url = cart.User.Country.Url,
                            LanguageCode = cart.User.Country.LanguageCode,
                            CurrencySymbol = cart.User.Country.GetCurrencySymbol()
                        }
                    },
                    CartShippingBoxes = cart.CartShippingBoxes.Select(cartShippingBox => new CartShippingBoxDto
                    {
                        Cart = cartShippingBox.Cart == null ? null : new CartDto(cartShippingBox.Cart.Id),
                        CartItems = cartShippingBox.CartItems.Select(cartItem => new CartItemDto
                        {
                            Id = cartItem.Id,
                            TotalAfterTax = cartItem.GetTotalAfterTax(this.requestDbContext, taxes, languageCode),
                            CartItemVariants = cartItem.CartItemVariants.Select(cartItemVariant => new CartItemVariantDto
                            {
                                Id = cartItemVariant.Id,
                                BooleanValue = cartItemVariant.BooleanValue,
                                DoubleValue = cartItemVariant.DoubleValue,
                                IsDisabled = cartItemVariant.IsDisabled,
                                IntegerValue = cartItemVariant.IntegerValue,
                                JsonValue = cartItemVariant.JsonValue,
                                PriceModifier = cartItemVariant.GetPriceModifier(languageCode),
                                StringValue = cartItemVariant.StringValue,
                                CartItem = cartItemVariant.CartItem == null ? null : new CartItemDto(cartItemVariant.CartItem.Id),
                                Variant = cartItemVariant.Variant == null ? null : new VariantDto
                                {
                                    Id = cartItemVariant.Variant.Id,
                                    Name = cartItemVariant.Variant.Name,
                                    Type = cartItemVariant.Variant.Type,
                                    LocalizedTitle = cartItemVariant.Variant.GetLocalizedTitle(languageCode)
                                },
                                VariantOptionValue = cartItemVariant.VariantOptionValue == null ? null : new VariantOptionDto
                                {
                                    Id = cartItemVariant.VariantOptionValue.Id,
                                    Name = cartItemVariant.VariantOptionValue.Name
                                }
                            }).ToList(),
                            Product = cartItem.Product == null ? null : new ProductDto
                            {
                                Id = cartItem.Product.Id,
                                LocalizedTitle = cartItem.Product.GetLocalizedTitle(languageCode)
                            }
                        }).ToList(),
                        TotalAfterTax = cartShippingBox.GetTotalAfterTax(this.requestDbContext, taxes, languageCode),
                        ShippingPriceAfterTax = cartShippingBox.GetShippingPriceAfterTax(this.requestDbContext, taxes, languageCode),
                        ShippingBox = cartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                        {
                            Id = cartShippingBox.ShippingBox.Id,
                            Name = cartShippingBox.ShippingBox.Name
                        }
                    }).ToList(),
                    ShippingAddress = cart.ShippingAddress == null ? null : new AddressDto
                    {
                        Id = cart.ShippingAddress.Id,
                        AddressLine1 = cart.ShippingAddress.AddressLine1,
                        AddressLine2 = cart.ShippingAddress.AddressLine2,
                        City = cart.ShippingAddress.City,
                        FirstName = cart.ShippingAddress.FirstName,
                        LastName = cart.ShippingAddress.LastName,
                        IsDisabled = cart.ShippingAddress.IsDisabled,
                        PhoneNumber = cart.ShippingAddress.PhoneNumber,
                        Postcode = cart.ShippingAddress.Postcode,
                        StateCountyProvince = cart.ShippingAddress.StateCountyProvince,
                        Country = cart.ShippingAddress.Country == null ? null : new CountryDto
                        {
                            Id = cart.ShippingAddress.Country.Id,
                            Name = cart.ShippingAddress.Country.Name
                        },
                        User = cart.ShippingAddress.User == null ? null : new UserDto(cart.ShippingAddress.User.Id)
                    },
                    Coupon = cart.Coupon == null ? null : new CouponDto(cart.Coupon.Id),
                    CouponValue = cart.GetCouponValue(this.requestDbContext, languageCode),
                    UserCredit = cart.UserCredit == null ? null : new UserCreditDto
                    {
                        Amount = cart.UserCredit.Amount
                    },
                    SentEmails = cart.SentEmails.Select(x => new SentEmailDto
                    {
                        Id = x.Id,
                        Email = x.Email == null ? null : new EmailDto
                        {
                            Id = x.Email.Id
                        }
                    }).ToList(),
                    TotalAfterTax = cart.GetTotalAfterTax(this.requestDbContext, taxes, languageCode),
                    TotalToBePaid = cart.GetTotalToBePaid(this.requestDbContext, taxes, languageCode)
                },
                AllEmails = this.requestDbContext.Emails.Where(x => !x.IsDisabled).OrderBy(x => x.Name).Select(x => new EmailDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    ViewName = x.ViewName
                }).ToList()
            };

            if (editCartDto.Cart?.ShippingAddress?.Country != null)
            {
                editCartDto.Cart.ShippingAddress.Country.Taxes = new List<TaxDto>();
            }

            return this.utilityService.GetCamelCaseJsonResult(editCartDto);
        }

        [HttpPost]
        public virtual JsonResult SendEmail(SendEmailConfirmationModalDto dto)
        {
            var validationResult = this.sendEmailConfirmationModalDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            try
            {
                var cart = this.requestDbContext.Carts.Find(dto.EntityId);

                this.emailService.ExecuteEmailAction(this.requestDbContext, dto.EmailName, dto.SendTo.InList(), cart);
            }
            catch (Exception e)
            {
                validationResult.Errors.Add(new ValidationFailure(null, e.Message));

                return this.utilityService.GetFormResponseJson(validationResult);
            }

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { cartId = dto.EntityId }));
        }

        [HttpPost]
        [Route("{country:length(2)}/Admin/Cart/CreateOrder", Order = 1)]
        [Route("Admin/Cart/CreateOrder", Order = 2)]
        public JsonResult CreateOrder(Guid cartId)
        {
            var formResponse = new FormResponse();
            var cart = this.requestDbContext.Carts.Find(cartId);

            if (cart == null)
            {
                formResponse.Errors.Add(new FormError("Impossible to create the Order: the Cart is null."));
                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            this.cmsService.CreateOrder(this.requestDbContext, cart);

            this.cmsService.DeleteCart(this.requestDbContext, cart.Id);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}