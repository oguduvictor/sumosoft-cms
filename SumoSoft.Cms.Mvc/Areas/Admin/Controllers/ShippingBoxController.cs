﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class ShippingBoxController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IAdminShippingBoxService adminShippingBoxService;
        private readonly IValidator<ShippingBoxDto> shippingBoxDtoValidator;

        public ShippingBoxController(
            IAdminUtilityService adminUtilityService,
            IAdminShippingBoxService adminShippingBoxService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<ShippingBoxDto> shippingBoxDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.adminShippingBoxService = adminShippingBoxService;
            this.utilityService = utilityService;
            this.shippingBoxDtoValidator = shippingBoxDtoValidator;
        }

        public ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var shippingBoxes = this.requestDbContext.ShippingBoxes.OrderBy(x => x.SortOrder).AsEnumerable().Select(shippingBox => new ShippingBoxDto()
            {
                Name = shippingBox.Name,
                Id = shippingBox.Id,
                SortOrder = shippingBox.SortOrder,
                LocalizedDescription = shippingBox.GetLocalizedDescription(),
                LocalizedTitle = shippingBox.GetLocalizedTitle(),
            }).OrderBy(a => a.SortOrder).ToList();

            var shippingBoxListDto = new ShippingBoxListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                ShippingBoxes = shippingBoxes
            };

            return this.utilityService.GetCamelCaseJsonResult(shippingBoxListDto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? shippingBoxId)
        {
            var isNew = shippingBoxId == null;

            var shippingBox = isNew ? new ShippingBox() : this.requestDbContext.ShippingBoxes.Find(shippingBoxId);

            if (shippingBox == null)
            {
                throw new NullReferenceException();
            }

            // ---------------------------------------------------------------------
            // Add missing ShippingBoxLocalizedKits
            // ---------------------------------------------------------------------

            shippingBox.ShippingBoxLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(shippingBox.ShippingBoxLocalizedKits, new ShippingBoxLocalizedKit
            {
                ShippingBox = shippingBox
            });

            // ---------------------------------------------------------------------
            // Model
            // ---------------------------------------------------------------------

            var shippingBoxDto = new ShippingBoxDto
            {
                IsNew = isNew,
                Id = shippingBox.Id,
                Name = shippingBox.Name,
                RequiresShippingAddress = shippingBox.RequiresShippingAddress,
                SortOrder = shippingBox.SortOrder,
                IsDisabled = shippingBox.IsDisabled,
                LocalizedPrice = shippingBox.GetLocalizedPrice(),
                LocalizedTitle = shippingBox.GetLocalizedTitle(),
                LocalizedDescription = shippingBox.GetLocalizedDescription(),
                LocalizedInternalDescription = shippingBox.GetLocalizedInternalDescription(),
                LocalizedMinDays = shippingBox.GetLocalizedMinDays(),
                LocalizedMaxDays = shippingBox.GetLocalizedMaxDays(),
                LocalizedCountWeekends = shippingBox.GetLocalizedCountWeekends(),
                LocalizedFreeShippingMinimumPrice = shippingBox.GetLocalizedFreeShippingMinimumPrice(),
                Countries = shippingBox.Countries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).OrderBy(x => x.Name).ToList(),
                Products = shippingBox.Products.OrderBy(x => x.Name).Select(x => new ProductDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.GetLocalizedTitle()
                }).ToList(),
                ShippingBoxLocalizedKits = shippingBox.ShippingBoxLocalizedKits.OrderByDefaultCountry().Select(localizedKit => new ShippingBoxLocalizedKitDto
                {
                    Id = localizedKit.Id,
                    Title = localizedKit.Title,
                    Description = localizedKit.Description,
                    InternalDescription = localizedKit.InternalDescription,
                    Carrier = localizedKit.Carrier,
                    Price = localizedKit.Price,
                    FreeShippingMinimumPrice = localizedKit.FreeShippingMinimumPrice,
                    MinDays = localizedKit.MinDays,
                    MaxDays = localizedKit.MaxDays,
                    CountWeekends = localizedKit.CountWeekends,
                    Country = localizedKit.Country == null ? null : new CountryDto
                    {
                        Id = localizedKit.Country.Id,
                        Name = localizedKit.Country.Name,
                        CurrencySymbol = localizedKit.Country.GetCurrencySymbol(),
                        IsDefault = localizedKit.Country.IsDefault
                    },
                    ShippingBox = localizedKit.ShippingBox == null ? null : new ShippingBoxDto(localizedKit.ShippingBox.Id)
                }).ToList()
            };

            var editShippingBoxDto = new EditShippingBoxDto
            {
                ShippingBox = shippingBoxDto,
                AllCountries = this.requestDbContext.Countries.OrderBy(x => x.Name).Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext)
            };

            return this.utilityService.GetCamelCaseJsonResult(editShippingBoxDto);
        }

        [HttpPost]
        public virtual JsonResult Duplicate(Guid id)
        {
            var shippingBox = this.requestDbContext.ShippingBoxes.Find(id);

            if (shippingBox == null)
            {
                throw new NullReferenceException();
            }

            var duplicate = this.adminShippingBoxService.Duplicate(this.requestDbContext, shippingBox);

            return this.utilityService.GetFormResponseJson(new ValidationResult(), new { id = duplicate.Id }, this.Url.Action("Edit", new { shippingBoxId = duplicate.Id }));
        }

        [HttpPost]
        public virtual JsonResult GetAllProductsJson()
        {
            var productsDto = this.requestDbContext.Products.AsEnumerable().Select(x => new ProductDto
            {
                Id = x.Id,
                LocalizedTitle = x.GetLocalizedTitle()
            }).OrderBy(x => x.LocalizedTitle).ToList();

            return this.utilityService.GetCamelCaseJsonResult(productsDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(ShippingBoxDto dto)
        {
            var validationResult = this.shippingBoxDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var shippingBox = this.adminShippingBoxService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { shippingBoxId = shippingBox.Id }));
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid shippingBoxId)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.ShippingBoxes.Delete(shippingBoxId, true);
            }
            catch (Exception)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the entity"));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult UpdateSortOrder(List<ShippingBoxDto> shippingBoxDtos)
        {
            var formResponse = new FormResponse<List<ShippingBoxDto>>();

            this.adminShippingBoxService.UpdateSortOrder(this.requestDbContext, shippingBoxDtos);

            formResponse.Target = this.requestDbContext.ShippingBoxes.OrderBy(x => x.SortOrder).AsEnumerable().Select(shippingBox => new ShippingBoxDto
            {
                Id = shippingBox.Id,
                Name = shippingBox.Name,
                SortOrder = shippingBox.SortOrder,
                LocalizedDescription = shippingBox.GetLocalizedDescription(),
                LocalizedTitle = shippingBox.GetLocalizedTitle(),
            }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}