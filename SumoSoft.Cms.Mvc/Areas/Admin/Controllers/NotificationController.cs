﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation.Results;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class NotificationController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAuthenticationService authenticationService;
        private readonly IUtilityService utilityService;

        public NotificationController(
            IRequestDbContextService requestDbContextService,
            IAuthenticationService authenticationService,
            IUtilityService utilityService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.authenticationService = authenticationService;
            this.utilityService = utilityService;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var userRole = this.authenticationService
                .GetAuthenticatedUser(this.requestDbContext)
                .AsNotNull()
                .Role
                .AsNotNull();

            var logs = this.requestDbContext.Logs.AsQueryable();

            if (!userRole.ViewExceptionLogs)
            {
                logs = logs.Where(x => x.Type != LogTypesEnum.Exception);
            }

            if (!userRole.ViewEcommerceLogs)
            {
                logs = logs.Where(x => x.Type != LogTypesEnum.Ecommerce);
            }

            if (!userRole.ViewContentLogs)
            {
                logs = logs.Where(x => x.Type != LogTypesEnum.Content);
            }

            var dto = logs
                .OrderByDescending(x => x.CreatedDate)
                .Take(100)
                .AsEnumerable()
                .Select(x => new LogDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    Message = x.Message.Shorten(80, true)
                }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        public virtual ActionResult Details(Guid notificationId)
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult DetailsJson(Guid notificationId)
        {
            var log = this.requestDbContext.Logs.Find(notificationId);

            var logDto = log == null ? null : new LogDto
            {
                Id = log.Id,
                CreatedDate = log.CreatedDate,
                Type = log.Type,
                Message = log.Message,
                Details = log.Details,
                User = log.User == null ? null : new UserDto
                {
                    Id = log.User.Id,
                    FirstName = log.User.FirstName,
                    LastName = log.User.LastName,
                    Email = log.User.Email
                }
            };

            return this.utilityService.GetCamelCaseJsonResult(logDto);
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.Logs.Delete(id, true);
            }
            catch (Exception e)
            {
                formResponse.AddError(e.Message);
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult DeleteAll()
        {
            var logs = this.requestDbContext.Logs.ToList();

            foreach (var log in logs)
            {
                this.requestDbContext.Logs.Delete(log.Id, false);
            }

            this.requestDbContext.SaveChanges();

            return this.utilityService.GetFormResponseJson(new ValidationResult(), this.Url.Action("Index"));
        }
    }
}