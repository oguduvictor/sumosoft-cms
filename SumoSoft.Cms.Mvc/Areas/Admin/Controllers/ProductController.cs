﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Web.Mvc;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class ProductController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly ICountryService countryService;
        private readonly IValidator<ProductDto> productDtoValidator;
        private readonly IValidator<EditProductAttributesDto> editProductAttributesDtoValidator;
        private readonly IValidator<ContentSectionDto> contentSectionDtoValidator;
        private readonly IValidator<EditProductVariantsDto> editProductVariantsDtoValidator;
        private readonly IValidator<EditProductStockUnitsDto> editProductStockUnitsDtoValidator;
        private readonly IValidator<EditProductImageKitsDto> editProductImageKitsDtoValidator;
        private readonly IAdminProductService adminProductService;

        public ProductController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            ICountryService countryService,
            IValidator<ProductDto> productDtoValidator,
            IValidator<EditProductAttributesDto> editProductAttributesDtoValidator,
            IValidator<ContentSectionDto> contentSectionDtoValidator,
            IValidator<EditProductVariantsDto> editProductVariantsDtoValidator,
            IValidator<EditProductStockUnitsDto> editProductStockUnitsDtoValidator,
            IValidator<EditProductImageKitsDto> editProductImageKitsDtoValidator,
            IAdminProductService adminProductService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.countryService = countryService;
            this.productDtoValidator = productDtoValidator;
            this.editProductAttributesDtoValidator = editProductAttributesDtoValidator;
            this.contentSectionDtoValidator = contentSectionDtoValidator;
            this.editProductVariantsDtoValidator = editProductVariantsDtoValidator;
            this.editProductStockUnitsDtoValidator = editProductStockUnitsDtoValidator;
            this.editProductImageKitsDtoValidator = editProductImageKitsDtoValidator;
            this.adminProductService = adminProductService;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson(int page = 1, string keywordFilter = null, string categoryFilter = null)
        {
            const int pageSize = 100;

            // -----------------------------------------------------
            // Filters
            // -----------------------------------------------------

            var products = this.requestDbContext.Products
                .Where(x => string.IsNullOrEmpty(keywordFilter) ||
                            x.Name.Contains(keywordFilter) ||
                            x.Url.Contains(keywordFilter) ||
                            x.Comments.Contains(keywordFilter) ||
                            x.Code.Contains(keywordFilter) ||
                            x.AutoTags.Contains(keywordFilter) ||
                            x.ProductStockUnits.Any(psu => psu.Code.Contains(keywordFilter)))
                .Where(x => string.IsNullOrEmpty(categoryFilter) ||
                            x.Categories.Any(c => c.Name.Contains(categoryFilter)))
                .OrderBy(x => x.Url);

            // -----------------------------------------------------
            // Dto
            // -----------------------------------------------------

            var productListDto = new ProductListDto
            {
                TotalItems = products.Count(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                PageSize = pageSize,
                AllCategories = this.requestDbContext.Categories.Where(x => x.Parent == null).OrderBy(x => x.SortOrder).Select(x => new CategoryDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.CategoryLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).Title,
                    Name = x.Name
                }).ToList(),
                Products = products.Skip((page - 1) * pageSize).Take(pageSize).Select(x => new ProductDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.ProductLocalizedKits.FirstOrDefault(l => l.Country.IsDefault).Title,
                    Url = x.Url,
                    Code = x.Code,
                    IsDisabled = x.IsDisabled
                }).ToList(),
                LocalizedCountries = this.requestDbContext.Countries.Where(x => x.Localize).OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name).Select(x => x.Name).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(productListDto);
        }

        // -------------------------------------------------------------------------------------

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? productId)
        {
            var isNew = productId == null;

            var product = isNew ? new Product() : this.requestDbContext.Products.Find(productId);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            product.ProductLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(product.ProductLocalizedKits, new ProductLocalizedKit
            {
                Product = product
            });

            var editProductDto = new EditProductDto
            {
                Product = new ProductDto
                {
                    IsNew = isNew,
                    Id = product.Id,
                    ProductLocalizedKits = product.ProductLocalizedKits.Select(productLocalizedKit => new ProductLocalizedKitDto
                    {
                        Id = productLocalizedKit.Id,
                        Country = productLocalizedKit.Country == null ? null : new CountryDto
                        {
                            Id = productLocalizedKit.Country.Id,
                            Name = productLocalizedKit.Country.Name,
                            IsDefault = productLocalizedKit.Country.IsDefault,
                            Localize = productLocalizedKit.Country.Localize,
                            IsDisabled = productLocalizedKit.Country.IsDisabled
                        },
                        Product = productLocalizedKit.Product == null ? null : new ProductDto(productLocalizedKit.Product.Id),
                        Title = productLocalizedKit.Title,
                        Description = productLocalizedKit.Description,
                        MetaTitle = productLocalizedKit.MetaTitle,
                        MetaDescription = productLocalizedKit.MetaDescription,
                        IsDisabled = productLocalizedKit.IsDisabled
                    }).ToList(),
                    Name = product.Name,
                    Url = product.Url,
                    Code = product.Code,
                    TaxCode = product.TaxCode,
                    Tags = product.Tags,
                    Comments = product.Comments,
                    Categories = product.Categories.Select(category => new CategoryDto
                    {
                        Id = category.Id,
                        LocalizedTitle = category.GetLocalizedTitle()
                    }).ToList(),
                    IsDisabled = product.IsDisabled,
                    ShippingBoxes = product.ShippingBoxes.Select(shippingBox => new ShippingBoxDto
                    {
                        Id = shippingBox.Id,
                        Name = shippingBox.Name
                    }).ToList(),
                    RelatedProducts = product.RelatedProducts.Select(relatedProduct => new ProductDto
                    {
                        Id = relatedProduct.Id,
                        LocalizedTitle = relatedProduct.GetLocalizedTitle()
                    }).ToList(),
                    ProductImageKits = product.ProductImageKits.Select(productImageKit => new ProductImageKitDto
                    {
                        Id = productImageKit.Id,
                        VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            LocalizedTitle = variantOption.GetLocalizedTitle(),
                            Variant = variantOption.Variant == null ? null : new VariantDto
                            {
                                LocalizedTitle = variantOption.Variant.GetLocalizedTitle()
                            }
                        }).ToList(),
                        RelatedProductImageKits = productImageKit.RelatedProductImageKits.Select(relatedProductImageKit => new ProductImageKitDto
                        {
                            Id = relatedProductImageKit.Id
                        }).ToList()
                    }).ToList()
                },
                AllTags = this.requestDbContext.Products.AsEnumerable().SelectMany(x => x.GetTagList().Distinct()).ToList(),
                AllCategories = this.requestDbContext.Categories.OrderBy(x => x.SortOrder).Select(category => new CategoryDto
                {
                    Id = category.Id,
                    LocalizedTitle = category.CategoryLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).Title
                }).ToList(),
                AllProducts = this.requestDbContext.Products.OrderBy(x => x.Url).Select(x => new ProductDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.ProductLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).Title,
                    IsDisabled = x.IsDisabled
                }).ToList(),
                AllShippingBoxes = this.requestDbContext.ShippingBoxes.OrderBy(x => x.SortOrder).Select(x => new ShippingBoxDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    LocalizedInternalDescription = x.ShippingBoxLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).InternalDescription
                }).ToList(),
                AllProductImageKits = this.requestDbContext.ProductImageKits
                    .Where(productImageKit => productImageKit.Product.Id != productId)
                    .Select(productImageKit => new ProductImageKitDto
                    {
                        Id = productImageKit.Id,
                        Product = productImageKit.Product == null ? null : new ProductDto
                        {
                            LocalizedTitle = productImageKit.Product.ProductLocalizedKits.FirstOrDefault(x => x.Country.IsDefault).Title
                        },
                        VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            LocalizedTitle = variantOption.VariantOptionLocalizedKits.FirstOrDefault(x => x.Country.IsDefault).Title,
                            Variant = variantOption.Variant == null ? null : new VariantDto
                            {
                                LocalizedTitle = variantOption.Variant.VariantLocalizedKits.FirstOrDefault(x => x.Country.IsDefault).Title
                            }
                        }).ToList()
                    }).ToList(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext)
            };

            return this.utilityService.GetCamelCaseJsonResult(editProductDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateProduct(ProductDto productDto)
        {
            var validationResult = this.productDtoValidator.Validate(productDto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            if (this.requestDbContext.Products.Any(x => x.Url == productDto.Url && x.Id != productDto.Id))
            {
                validationResult.Errors.Add(new ValidationFailure(string.Empty, "Already existing Url"));
            }

            var product = this.adminProductService.AddOrUpdate(this.requestDbContext, productDto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { productId = product.Id }));
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            this.requestDbContext.Products.Delete(id, true);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult Duplicate(Guid id)
        {
            var product = this.requestDbContext.Products.Find(id);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            var newProduct = this.adminProductService.Duplicate(this.requestDbContext, product);

            return this.utilityService.GetFormResponseJson(new ValidationResult(), new { id = newProduct.Id }, this.Url.Action("Edit", new { productId = newProduct.Id }));
        }

        public virtual ActionResult EditProductAttributes()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditProductAttributesJson(Guid productId)
        {
            var product = this.requestDbContext.Products.Find(productId);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            var dto = new EditProductAttributesDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                ProductId = productId,
                ProductTitle = product.GetLocalizedTitle(),
                ProductAttributes = product.ProductAttributes.Select(productAttribute => new ProductAttributeDto
                {
                    Id = productAttribute.Id,
                    Product = productAttribute.Product == null ? null : new ProductDto
                    {
                        Id = productAttribute.Product.Id,
                        Name = productAttribute.Product.Name
                    },
                    Attribute = productAttribute.Attribute == null ? null : new AttributeDto
                    {
                        Id = productAttribute.Attribute.Id,
                        Name = productAttribute.Attribute.Name,
                        SortOrder = productAttribute.Attribute.SortOrder,
                        LocalizedTitle = productAttribute.Attribute.GetLocalizedTitle(),
                        Type = productAttribute.Attribute.Type,
                        AttributeOptions = productAttribute.Attribute.AttributeOptions.Select(attributeOption => new AttributeOptionDto
                        {
                            Id = attributeOption.Id,
                            Name = attributeOption.Name
                        }).ToList()
                    },
                    BooleanValue = productAttribute.BooleanValue,
                    StringValue = productAttribute.StringValue,
                    ImageValue = productAttribute.ImageValue,
                    DoubleValue = productAttribute.DoubleValue,
                    DateTimeValue = productAttribute.DateTimeValue,
                    AttributeOptionValue = productAttribute.AttributeOptionValue == null ? null : new AttributeOptionDto
                    {
                        Id = productAttribute.AttributeOptionValue.Id,
                        Name = productAttribute.AttributeOptionValue.Name
                    },
                    ContentSectionValue = productAttribute.ContentSectionValue == null ? null : new ContentSectionDto
                    {
                        Id = productAttribute.ContentSectionValue.Id,
                        Name = productAttribute.ContentSectionValue.Name
                    }
                }).OrderBy(x => x.Attribute?.SortOrder).ToList(),
                AllAttributes = this.requestDbContext.Attributes
                    .OrderBy(x => x.SortOrder)
                    .Select(attribute => new AttributeDto
                    {
                        Id = attribute.Id,
                        Name = attribute.Name,
                        LocalizedTitle = attribute.AttributeLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).Title,
                        Type = attribute.Type,
                        AttributeOptions = attribute.AttributeOptions.Select(x => new AttributeOptionDto
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).ToList()
                    }).ToList(),
                AllContentSections = this.requestDbContext.ContentSections.OrderBy(x => x.Name).Select(x => new ContentSectionDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList()
            };

            var contentSectionSchemaDirectory = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("~/Scripts/contentSectionSchemas"));

            if (contentSectionSchemaDirectory.Exists)
            {
                dto.ContentSectionsSchemaNames = contentSectionSchemaDirectory.GetFiles().Where(x => x.Extension == ".ts").Select(x => x.Name.Substring(1, x.Name.IndexOf(x.Extension, StringComparison.Ordinal) - 1)).ToList();
            }

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateProductAttributes(EditProductAttributesDto dto)
        {
            var validationResult = this.editProductAttributesDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            this.adminProductService.AddOrUpdateProductAttributes(this.requestDbContext, dto.ProductId, dto.ProductAttributes);

            return this.utilityService.GetFormResponseJson(validationResult);
        }

        [HttpPost]
        public virtual JsonResult AddContentSection(ContentSectionDto contentSectionDto, bool checkForSimilarContent)
        {
            var defaultCountry = this.countryService.GetDefaultCountry(this.requestDbContext);
            contentSectionDto.ContentSectionLocalizedKits[0].Country = new CountryDto(defaultCountry.Id);

            var validationResult = this.contentSectionDtoValidator.Validate(contentSectionDto);
            var createContentSectionDto = new CreateContentSectionDto();

            if (!validationResult.IsValid)
            {
                createContentSectionDto.FormResponse = this.utilityService.GetFormResponse(validationResult);
                return this.utilityService.GetCamelCaseJsonResult(createContentSectionDto);
            }

            var similarContentSectionDto = checkForSimilarContent ? this.SimilarityMatch(contentSectionDto) : null;

            if (similarContentSectionDto != null)
            {
                createContentSectionDto.SimilarContentSection = similarContentSectionDto;
                return this.utilityService.GetCamelCaseJsonResult(createContentSectionDto);
            }

            var newContentSection = this.adminProductService.AddContentSection(this.requestDbContext, contentSectionDto);

            createContentSectionDto.SavedContentSection = new ContentSectionDto
            {
                Id = newContentSection.Id,
                Name = newContentSection.Name
            };

            createContentSectionDto.FormResponse = this.utilityService.GetFormResponse(validationResult);

            return this.utilityService.GetCamelCaseJsonResult(createContentSectionDto);
        }

        // -------------------------------------------------------------------------------------

        public virtual ActionResult EditProductVariants()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditProductVariantsJson(Guid? productId)
        {
            var product = this.requestDbContext.Products.Find(productId);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            var dto = new EditProductVariantsDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                ProductId = product.Id,
                ProductLocalizedName = product.GetLocalizedTitle(),
                ProductVariants = product.ProductVariants.OrderBy(x => x.Variant?.SortOrder).Select(x => new ProductVariantDto
                {
                    Id = x.Id,
                    DefaultBooleanValue = x.DefaultBooleanValue,
                    DefaultDoubleValue = x.DefaultDoubleValue,
                    DefaultIntegerValue = x.DefaultIntegerValue,
                    DefaultStringValue = x.DefaultStringValue,
                    DefaultVariantOptionValue = x.DefaultVariantOptionValue == null ? null : new VariantOptionDto
                    {
                        Id = x.DefaultVariantOptionValue.Id,
                        LocalizedTitle = x.DefaultVariantOptionValue.GetLocalizedTitle()
                    },
                    VariantOptions = x.VariantOptions.Select(variantOption => new VariantOptionDto
                    {
                        Id = variantOption.Id,
                        LocalizedTitle = variantOption.GetLocalizedTitle()
                    }).OrderBy(z => z.SortOrder).ThenBy(za => za.LocalizedTitle).ToList(),
                    Variant = x.Variant == null ? null : new VariantDto
                    {
                        Id = x.Variant.Id,
                        Type = x.Variant.Type,
                        Name = x.Variant.Name,
                        LocalizedTitle = x.Variant.GetLocalizedTitle(),
                        VariantOptions = x.Variant.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            Name = variantOption.Name,
                            LocalizedTitle = variantOption.GetLocalizedTitle()
                        }).ToList()
                    },
                    Product = x.Product == null ? null : new ProductDto
                    {
                        Id = x.Product.Id
                    }
                }).ToList(),
                AllVariants = this.requestDbContext.Variants
                    .Where(x => !x.IsDisabled)
                    .OrderBy(x => x.SortOrder)
                    .AsEnumerable()
                    .Select(x => new VariantDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        LocalizedTitle = x.GetLocalizedTitle(),
                        Type = x.Type,
                        DefaultBooleanValue = x.DefaultBooleanValue,
                        DefaultDoubleValue = x.DefaultDoubleValue,
                        DefaultIntegerValue = x.DefaultIntegerValue,
                        DefaultStringValue = x.DefaultStringValue,
                        DefaultVariantOptionValue = x.DefaultVariantOptionValue == null ? null : new VariantOptionDto
                        {
                            Id = x.DefaultVariantOptionValue.Id,
                            LocalizedTitle = x.DefaultVariantOptionValue.GetLocalizedTitle()
                        },
                        VariantOptions = x.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            LocalizedTitle = variantOption.GetLocalizedTitle(),
                            Name = variantOption.Name
                        }).ToList()
                    }).ToList()
            };


            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateProductVariants(EditProductVariantsDto model)
        {
            var validationResult = this.editProductVariantsDtoValidator.Validate(model);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            this.adminProductService.AddOrUpdateProductVariants(this.requestDbContext, model.ProductId, model.ProductVariants);

            return this.utilityService.GetFormResponseJson(validationResult);
        }

        // -------------------------------------------------------------------------------------

        public virtual ActionResult EditProductStockUnits()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditProductStockUnitsJson(Guid productId)
        {
            var dbProduct = this.requestDbContext.Products.Find(productId);

            if (dbProduct == null)
            {
                throw new NullReferenceException();
            }

            // ---------------------------------------------------------
            // Create Matrix of the expected ProductStockUnits
            // ---------------------------------------------------------

            var matrixSeed = dbProduct.ProductVariants.Where(x => x.Variant != null && x.Variant.CreateProductStockUnits && x.VariantOptions.Any()).Select(x => x.VariantOptions).ToList();

            var matrix = GetMatrix(matrixSeed);

            var expectedProductStockUnits = matrix.Select(variantOptions => new ProductStockUnit
            {
                Product = dbProduct,
                VariantOptions = variantOptions
            }).ToList();

            // ---------------------------------------------------------
            // Create a ProductStockUnits without VariantOptions for products
            // that don't have any Variant that affects the ProductStockUnits
            // ---------------------------------------------------------

            if (!expectedProductStockUnits.Any())
            {
                expectedProductStockUnits.Add(new ProductStockUnit
                {
                    Product = dbProduct
                });
            }

            // ---------------------------------------------------------
            // Add missing ProductStockUnits
            // ---------------------------------------------------------

            foreach (var productStockUnit in expectedProductStockUnits)
            {
                if (!dbProduct.ProductStockUnits.Exists(x => x.VariantOptions.ContainsAll(productStockUnit.VariantOptions)))
                {
                    dbProduct.ProductStockUnits.Add(productStockUnit);
                }
            }

            // ---------------------------------------------------------
            // LocalizedKits
            // ---------------------------------------------------------

            foreach (var productStockUnit in dbProduct.ProductStockUnits)
            {
                // ---------------------------------------------------------
                // Delete obsolete
                // ---------------------------------------------------------

                var localizedKitsFromNonLocalizedCountries = productStockUnit.ProductStockUnitLocalizedKits
                    .Where(x => x.Country == null || !x.Country.Localize)
                    .ToList();

                foreach (var localizedKitFromNonLocalizedCountry in localizedKitsFromNonLocalizedCountries)
                {
                    this.requestDbContext.ProductStockUnitLocalizedKits.Delete(localizedKitFromNonLocalizedCountry.Id, true);

                    productStockUnit.ProductStockUnitLocalizedKits.Remove(localizedKitFromNonLocalizedCountry);
                }

                // ---------------------------------------------------------
                // Add missing
                // ---------------------------------------------------------

                productStockUnit.ProductStockUnitLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(productStockUnit.ProductStockUnitLocalizedKits, new ProductStockUnitLocalizedKit
                {
                    ProductStockUnit = productStockUnit
                });
            }

            // ---------------------------------------------------------
            // Create Dto
            // ---------------------------------------------------------

            var dto = new EditProductStockUnitsDto
            {
                ProductId = dbProduct.Id,
                ProductTitle = dbProduct.GetLocalizedTitle(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                ProductStockUnits = dbProduct.ProductStockUnits.Select(stockUnit => new ProductStockUnitDto
                {
                    Id = stockUnit.Id,
                    IsDisabled = stockUnit.IsDisabled,
                    Stock = stockUnit.Stock,
                    DispatchDate = stockUnit.DispatchDate,
                    EnablePreorder = stockUnit.EnablePreorder,
                    Code = stockUnit.Code,
                    DispatchTime = stockUnit.DispatchTime,
                    Product = stockUnit.Product == null ? null : new ProductDto(stockUnit.Product.Id),
                    VariantOptions = stockUnit.VariantOptions.OrderBySortOrder().Select(x => new VariantOptionDto
                    {
                        Id = x.Id,
                        LocalizedTitle = x.GetLocalizedTitle(),
                        Variant = x.Variant == null ? null : new VariantDto(x.Variant.Id)
                    }).ToList(),
                    ProductStockUnitLocalizedKits = stockUnit.ProductStockUnitLocalizedKits.Select(x => new ProductStockUnitLocalizedKitDto
                    {
                        Id = x.Id,
                        BasePrice = x.BasePrice,
                        SalePrice = x.SalePrice,
                        MembershipSalePrice = x.MembershipSalePrice,
                        Country = x.Country == null ? null : new CountryDto
                        {
                            Id = x.Country.Id,
                            Name = x.Country.Name,
                            LanguageCode = x.Country.LanguageCode
                        },
                        ProductStockUnit = new ProductStockUnitDto(x.ProductStockUnit.Id)
                    }).ToList()
                }).ToList(),
                Variants = dbProduct.ProductVariants.Where(x => x.Variant != null && x.Variant.CreateProductStockUnits && x.VariantOptions.Any()).Select(productVariant => new VariantDto
                {
                    Id = productVariant.Variant?.Id ?? throw new NullReferenceException(),
                    VariantLocalizedKits = productVariant.Variant.VariantLocalizedKits.Select(x => new VariantLocalizedKitDto
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Country = x.Country == null ? null : new CountryDto(x.Country.Id)
                    }).ToList()
                })
                .OrderBy(x => x.SortOrder)
                .ToList()
            };

            // ---------------------------------------------------------
            // Sorting
            // ---------------------------------------------------------

            foreach (var productStockUnit in dto.ProductStockUnits)
            {
                productStockUnit.VariantOptions = productStockUnit.VariantOptions.OrderBy(x => x.Variant?.Id).ToList();
            }

            dto.ProductStockUnits = dto.ProductStockUnits
                .OrderBy(x => x.IsDisabled)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(0)?.LocalizedTitle)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(1)?.LocalizedTitle)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(2)?.LocalizedTitle)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(3)?.LocalizedTitle)
                .ToList();

            // ---------------------------------------------------------
            // Add the old Obsolete Variants to EditProductStockUnitsDto
            // ---------------------------------------------------------

            var existingVariants = dbProduct.ProductStockUnits.SelectMany(x => x.VariantOptions).Select(x => x.Variant);

            foreach (var variant in existingVariants)
            {
                if (dto.Variants.All(x => !x.Id.Equals(variant?.Id)))
                {
                    dto.Variants.Add(new VariantDto
                    {
                        Id = (variant ?? throw new InvalidOperationException()).Id,
                        VariantLocalizedKits = variant.VariantLocalizedKits.Select(x => new VariantLocalizedKitDto
                        {
                            Id = x.Id,
                            Title = x.Title,
                            Country = x.Country == null ? null : new CountryDto(x.Country.Id)
                        }).ToList()
                    });
                }
            }

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateProductStockUnits(EditProductStockUnitsDto dto)
        {
            using (var dbContext = new CmsDbContext())
            {
                var validationResult = this.editProductStockUnitsDtoValidator.Validate(dto);

                if (!validationResult.IsValid)
                {
                    return this.utilityService.GetFormResponseJson(validationResult);
                }

                this.adminProductService.AddOrUpdateProductStockUnits(dbContext, dto.ProductId, dto.ProductStockUnits);

                return this.utilityService.GetFormResponseJson(validationResult);
            }
        }

        // -------------------------------------------------------------------------------------

        public virtual ActionResult EditProductImageKits()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditProductImageKitsJson(Guid? productId)
        {
            var dbProduct = this.requestDbContext.Products.Find(productId);

            if (dbProduct == null)
            {
                throw new NullReferenceException();
            }

            var editProductImageKitsDto = new EditProductImageKitsDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                ProductId = dbProduct.Id,
                ProductTitle = dbProduct.GetLocalizedTitle(),
                ProductImageKits = dbProduct.ProductImageKits
                    .Select(productImagekit => new ProductImageKitDto
                    {
                        Id = productImagekit.Id,
                        IsDisabled = productImagekit.IsDisabled,
                        Product = new ProductDto(dbProduct.Id),
                        ProductImages = productImagekit.ProductImages
                        .OrderBy(productImage => productImage.SortOrder)
                        .Select(productImage => new ProductImageDto
                        {
                            Id = productImage.Id,
                            Name = productImage.Name,
                            Url = productImage.Url,
                            AltText = productImage.AltText,
                            SortOrder = productImage.SortOrder,
                            IsDisabled = productImage.IsDisabled
                        }).ToList(),
                        VariantOptions = productImagekit.VariantOptions
                        .OrderBy(variantOption => variantOption.Variant?.SortOrder)
                        .Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            Variant = new VariantDto
                            {
                                Id = variantOption.Variant?.Id ?? Guid.NewGuid(),
                                LocalizedTitle = variantOption.Variant?.GetLocalizedTitle()
                            },
                            LocalizedTitle = variantOption.GetLocalizedTitle()
                        }).ToList()
                    }).ToList()
            };

            var matrixSeed = dbProduct.ProductVariants.Where(x => x.Variant != null && x.VariantOptions.Any() && x.Variant.CreateProductImageKits).Select(x => x.VariantOptions).ToList();

            var matrix = GetMatrix(matrixSeed);

            var expectedProductImageKits = matrix.Select(variantOptions => new ProductImageKitDto
            {
                Product = new ProductDto(editProductImageKitsDto.ProductId),
                VariantOptions = variantOptions.Select(variantOption => new VariantOptionDto
                {
                    Id = variantOption.Id,
                    Variant = new VariantDto
                    {
                        Id = variantOption.Variant?.Id ?? Guid.NewGuid(),
                        LocalizedTitle = variantOption.Variant?.GetLocalizedTitle()
                    },
                    LocalizedTitle = variantOption.GetLocalizedTitle()
                }).ToList()
            }).ToList();

            // ---------------------------------------------------------
            // Create a ProductImageKit without VariantOptions for products
            // that don't have any Variant that affects the ProductImageKits
            // ---------------------------------------------------------

            if (!expectedProductImageKits.Any())
            {
                expectedProductImageKits.Add(new ProductImageKitDto
                {
                    Product = new ProductDto(editProductImageKitsDto.ProductId),
                });
            }

            // ---------------------------------------------------------
            // Add missing ProductImageKits
            // ---------------------------------------------------------

            foreach (var productImageKit in expectedProductImageKits)
            {
                var isMissing = editProductImageKitsDto.ProductImageKits.All(x => x.VariantOptions.Count != productImageKit.VariantOptions.Count || !x.Matches(productImageKit.VariantOptions));
                if (isMissing)
                {
                    editProductImageKitsDto.ProductImageKits.Add(productImageKit);
                }
            }

            // ---------------------------------------------------------
            // Sorting
            // ---------------------------------------------------------

            foreach (var productImageKit in editProductImageKitsDto.ProductImageKits)
            {
                productImageKit.VariantOptions = productImageKit.VariantOptions.OrderBy(x => x.Variant?.SortOrder).ToList();
                productImageKit.ProductImages = productImageKit.ProductImages.OrderBy(x => x.SortOrder).ToList();
            }

            editProductImageKitsDto.ProductImageKits = editProductImageKitsDto.ProductImageKits
                .OrderBy(x => x.IsDisabled)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(0)?.LocalizedTitle)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(1)?.LocalizedTitle)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(2)?.LocalizedTitle)
                .ThenBy(x => x.VariantOptions.ElementAtOrDefault(3)?.LocalizedTitle).ToList();

            return this.utilityService.GetCamelCaseJsonResult(editProductImageKitsDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateProductImageKits(EditProductImageKitsDto model)
        {
            using (var dbContext = new CmsDbContext())
            {
                var validationResult = this.editProductImageKitsDtoValidator.Validate(model);

                if (!validationResult.IsValid)
                {
                    return this.utilityService.GetFormResponseJson(validationResult);
                }

                this.adminProductService.AddOrUpdateProductImageKits(dbContext, model.ProductId, model.ProductImageKits);

                return this.utilityService.GetFormResponseJson(validationResult);
            }
        }

        private static List<List<T>> GetMatrix<T>(List<List<T>> lists)
        {
            var firstList = lists.Take(1).FirstOrDefault() ?? new List<T>();

            var rows = firstList.Select(i => new[] { i }.ToList()).ToList();

            return lists.Skip(1).Aggregate(rows, (current, series) => current.Join(series, combination => true, i => true, (combination, i) =>
            {
                var nextLevelCombination = new List<T>(combination);
                nextLevelCombination.Add(i);
                return nextLevelCombination;
            }).ToList());
        }

        private ContentSectionDto SimilarityMatch(ContentSectionDto contentSectionDto)
        {
            var defaultLocalizedContent = contentSectionDto.ContentSectionLocalizedKits.First();

            var allDefaultLocalizedContents = this.requestDbContext.ContentSectionLocalizedKits.Where(x => x.Country.IsDefault).OrderBy(x => x.Country.LanguageCode == Thread.CurrentThread.CurrentCulture.Name).ToList();

            var defaultLocalizedKit = allDefaultLocalizedContents.FirstOrDefault(x => x.Content != null && x.Content.AproximatelyEqualsTo(defaultLocalizedContent.Content, 3));

            var similarContentSection = defaultLocalizedKit?.ContentSection;

            similarContentSection?.ContentSectionLocalizedKits.Clear();
            similarContentSection?.ContentSectionLocalizedKits.Add(defaultLocalizedKit);

            return similarContentSection == null ? null : new ContentSectionDto
            {
                Id = similarContentSection.Id,
                Name = similarContentSection.Name,
                IsDisabled = similarContentSection.IsDisabled,
                ContentSectionLocalizedKits = similarContentSection.ContentSectionLocalizedKits.Select(contentSectionLocalizedKit => new ContentSectionLocalizedKitDto
                {
                    Id = contentSectionLocalizedKit.Id,
                    Country = contentSectionLocalizedKit.Country == null ? null : new CountryDto(contentSectionLocalizedKit.Country.Id),
                    Content = contentSectionLocalizedKit.Content
                }).ToList()
            };
        }
    }
}

