﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class MailingListController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IUtilityService utilityService;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminMailingListService adminMailingListService;
        private readonly IValidator<MailingListDto> mailingListDtoValidator;

        public MailingListController(
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IAdminUtilityService adminUtilityService,
            IAdminMailingListService adminMailingListService,
            IValidator<MailingListDto> mailingListDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.utilityService = utilityService;
            this.adminUtilityService = adminUtilityService;
            this.adminMailingListService = adminMailingListService;
            this.mailingListDtoValidator = mailingListDtoValidator;
        }

        // GET: Admin/MailingList
        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var mailingListDto = this.requestDbContext.MailingLists.Select(x => new MailingListDto
            {
                Id = x.Id,
                Name = x.Name,
                SortOrder = x.SortOrder,
                TotalUsers = x.MailingListSubscriptions.Count(),
                TotalSubscribers = x.MailingListSubscriptions.Count(y => y.Status == MailingListSubscriptionStatusEnum.Subscribed)
            }).OrderBy(x => x.SortOrder).ToList();

            return this.utilityService.GetCamelCaseJsonResult(mailingListDto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var mailingList = isNew ? new MailingList() : this.requestDbContext.MailingLists.Find(id);

            if (mailingList == null)
            {
                throw new NullReferenceException();
            }

            mailingList.MailingListLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(mailingList.MailingListLocalizedKits, new MailingListLocalizedKit
            {
                MailingList = mailingList
            });

            var editMailingListDto = new EditMailingListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                MailingList = new MailingListDto
                {
                    IsNew = isNew,
                    Id = mailingList.Id,
                    IsDisabled = mailingList.IsDisabled,
                    LocalizedDescription = mailingList.GetLocalizedDescription(),
                    LocalizedTitle = mailingList.GetLocalizedTitle(),
                    Name = mailingList.Name,
                    SortOrder = mailingList.SortOrder,
                    MailingListLocalizedKits = mailingList.MailingListLocalizedKits.Select(mailingListLocalizedKit => new MailingListLocalizedKitDto
                    {
                        Id = mailingListLocalizedKit.Id,
                        Title = mailingListLocalizedKit.Title,
                        Description = mailingListLocalizedKit.Description,
                        IsDisabled = mailingListLocalizedKit.IsDisabled,
                        MailingList = new MailingListDto
                        {
                            Id = mailingList.Id
                        },
                        Country = mailingListLocalizedKit.Country == null ? null : new CountryDto
                        {
                            Id = mailingListLocalizedKit.Country.Id,
                            Name = mailingListLocalizedKit.Country.Name,
                            IsDefault = mailingListLocalizedKit.Country.IsDefault,
                            LanguageCode = mailingListLocalizedKit.Country.LanguageCode,
                            Url = mailingListLocalizedKit.Country.Url,
                            Localize = mailingListLocalizedKit.Country.Localize
                        }
                    }).ToList(),
                    MailingListSubscriptions = mailingList.MailingListSubscriptions.Select(subscription => new MailingListSubscriptionDto
                    {
                        MailingList = new MailingListDto(subscription.MailingList.Id),
                        User = new UserDto
                        {
                            Id = subscription.User.Id,
                            FirstName = subscription.User.FirstName,
                            LastName = subscription.User.LastName,
                            Email = subscription.User.Email
                        },
                        Status = subscription.Status,
                        StatusDescriptions = subscription.Status.GetType().GetDescriptions()
                    }).ToList()
                }
            };

            return this.utilityService.GetCamelCaseJsonResult(editMailingListDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(MailingListDto dto)
        {
            var validationResult = this.mailingListDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var mailingList = this.adminMailingListService.AddOrUpdateMailingList(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { id = mailingList.Id }));
        }

        [HttpPost]
        public virtual JsonResult Duplicate(Guid id)
        {
            var mailingList = this.requestDbContext.MailingLists.Find(id);

            if (mailingList == null)
            {
                throw new NullReferenceException();
            }

            var newMailingList = this.adminMailingListService.Duplicate(this.requestDbContext, mailingList);

            return this.utilityService.GetFormResponseJson(new ValidationResult(), new { id = newMailingList.Id }, this.Url.Action("Edit", new { id = newMailingList.Id }));
        }

        [HttpPost]
        public virtual JsonResult UpdateSortOrder(List<MailingListDto> mailingListDtos)
        {
            var formResponse = new FormResponse<List<MailingListDto>>();

            this.adminMailingListService.UpdateSortOrder(this.requestDbContext, mailingListDtos);

            formResponse.Target = this.requestDbContext.MailingLists.OrderBy(x => x.SortOrder).Select(x => new MailingListDto
            {
                Id = x.Id,
                Name = x.Name,
                SortOrder = x.SortOrder,
                TotalUsers = x.MailingListSubscriptions.Count(),
                TotalSubscribers = x.MailingListSubscriptions.Count(y => y.Status == MailingListSubscriptionStatusEnum.Subscribed)
            }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.MailingLists.Delete(id, true);
            }
            catch (Exception)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the entity"));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        public FileContentResult Export(Guid id)
        {
            var mailingList = this.requestDbContext.MailingLists.Find(id);

            if (mailingList == null)
            {
                return null;
            }

            var csv =
                "Id,Email,First name,Last name, Country name, Country url, Subscription status" +
                Environment.NewLine +
                mailingList.MailingListSubscriptions
                .ToCsv(
                    x => x.User?.Id.ToString(),
                    x => x.User?.Email,
                    x => x.User?.FirstName,
                    x => x.User?.LastName,
                    x => x.User?.Country?.Name,
                    x => x.User?.Country?.GetUrlForStringConcatenation(),
                    x => x.Status.GetDescription());

            return this.File(new UTF8Encoding().GetBytes(csv), "text/csv", $"{mailingList.Name}.csv");
        }

        [HttpPost]
        public virtual JsonResult ImportEmails()
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var formResponse = new FormResponse();

            var uploadedFiles = this.Request.Files;

            if (uploadedFiles.Count > 1)
            {
                formResponse.AddError("The maximum number of files that is possible to upload at the same time is 1");

                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            var mailingListId = Guid.Parse(this.Request.Form[0]);

            var mailingList = this.requestDbContext.MailingLists.Find(mailingListId);

            if (mailingList.MailingListSubscriptions.Count > 0)
            {
                formResponse.AddError("Unable to upload users as this mailing list already have users");

                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            // ---------------------------------------------------------------
            // Upload files and collect exceptions as validation errors
            // ---------------------------------------------------------------

            for (var i = 0; i < uploadedFiles.Count; i++)
            {
                var file = uploadedFiles[i];

                if (!file.FileName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                {
                    formResponse.AddError($"The file '{file.FileName}' could not be uploaded. Only CSV format is supported.");

                    continue;
                }

                var emailsInCsv = this.utilityService.GetDataFromCsv(file.InputStream).Select(row => row[0]).ToList();

                foreach (var email in emailsInCsv)
                {
                    var user = this.requestDbContext.Users.FirstOrDefault(x => x.Email == email);

                    if (user == null)
                    {
                        formResponse.AddError($"'{email}' does not exist");

                        return this.utilityService.GetCamelCaseJsonResult(formResponse);
                    }

                    mailingList.MailingListSubscriptions.Add(new MailingListSubscription
                    {
                        Status = MailingListSubscriptionStatusEnum.Subscribed,
                        User = user
                    });
                }
            }

            this.requestDbContext.SaveChanges();

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}