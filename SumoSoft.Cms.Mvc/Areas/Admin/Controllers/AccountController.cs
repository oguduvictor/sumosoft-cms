﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class AccountController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IUtilityService utilityService;
        private readonly IAuthenticationService authenticationService;

        public AccountController(
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IAuthenticationService authenticationService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.utilityService = utilityService;
            this.authenticationService = authenticationService;
        }

        public virtual ActionResult Login()
        {
            var usersCount = this.requestDbContext.Users.Count();

            if (usersCount == 0)
            {
                return this.RedirectToAction("Register");
            }

            return this.View();

        }

        public virtual ActionResult Logout()
        {
            this.authenticationService.UserSignOut();

            return this.Redirect("/");
        }

        public virtual ActionResult Register()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult LoginForm(LoginDetailsDto loginDetails, string returnUrl)
        {
            // -------------------------------------------------
            // Authentication
            // -------------------------------------------------

            var formResponse = this.authenticationService.AuthenticateUser(this.requestDbContext, loginDetails.Email, loginDetails.Password);

            if (!formResponse.IsValid)
            {
                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            formResponse.RedirectUrl = returnUrl ?? this.Url.Action("Index", "Notification");

            return this.utilityService.GetCamelCaseJsonResult(formResponse);

        }

        [HttpPost]
        public virtual JsonResult RegisterForm(RegisterDetailsDto registerDetails, string returnUrl)
        {
            // -------------------------------------------------
            // If this is the first user, make it administrator
            // -------------------------------------------------

            var usersCount = this.requestDbContext.Users.Count();

            var userRole = usersCount == 0
                ? this.requestDbContext.UserRoles.FirstOrDefault(x => x.Name == SharedConstants.DefaultRoleSystemAdmin)
                : this.requestDbContext.UserRoles.FirstOrDefault(x => x.Name == SharedConstants.DefaultUserRole);

            var newUser = new User
            {
                Email = registerDetails.Email,
                Password = registerDetails.Password,
                FirstName = registerDetails.FirstName,
                LastName = registerDetails.LastName,
                Role = userRole
            };

            // -------------------------------------------------
            // Registration
            // -------------------------------------------------

            var formResponse = this.authenticationService.RegisterUser(this.requestDbContext, newUser);

            if (!formResponse.IsValid)
            {
                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            formResponse.RedirectUrl = returnUrl ?? this.Url.Action("Index", "Notification");

            return this.utilityService.GetCamelCaseJsonResult(formResponse);

        }
    }
}