﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class UserController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<UserDto> userDtoValidator;
        private readonly IValidator<UserRoleDto> userRoleDtoValidator;
        private readonly IAdminUserService adminUserService;

        public UserController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<UserDto> userDtoValidator,
            IValidator<UserRoleDto> userRoleDtoValidator,
            IAdminUserService adminUserService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.utilityService = utilityService;
            this.userDtoValidator = userDtoValidator;
            this.userRoleDtoValidator = userRoleDtoValidator;
            this.adminUserService = adminUserService;
            this.adminUtilityService = adminUtilityService;
        }

        // ------------------------------------------------------------
        // USERS
        // ------------------------------------------------------------

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson(int page = 1, string keywordFilter = null)
        {
            const int pageSize = 100;

            var filteredUsers = this.requestDbContext.Users
                .Where(x => !string.IsNullOrEmpty(x.Email) &&
                            string.IsNullOrEmpty(keywordFilter) ||
                            x.Email.Contains(keywordFilter) ||
                            x.FirstName.Contains(keywordFilter) ||
                            x.LastName.Contains(keywordFilter) ||
                            (x.Role != null && x.Role.Name.Contains(keywordFilter)))
                .OrderBy(x => x.CreatedDate);

            var dto = new UserListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                TotalItems = filteredUsers.Count(),
                PageSize = pageSize,
                Users = filteredUsers.Skip((page - 1) * pageSize).Take(pageSize).Select(x => new UserDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    Password = x.Password,
                    Country = x.Country == null ? null : new CountryDto { Name = x.Country.Name },
                    Role = x.Role == null ? null : new UserRoleDto { Name = x.Role.Name }
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        public FileContentResult Export()
        {
            var users = this.requestDbContext.Users.Where(x => x.Email != null && x.Email != string.Empty);

            var csv =
                "Id,Email,First name,Last name, Country name, Country url" +
                Environment.NewLine +
                users.ToCsv(x => x.Id.ToString(), x => x.Email, x => x.FirstName, x => x.LastName, x => x.Country?.Name, x => x.Country?.GetUrlForStringConcatenation());

            return this.File(new UTF8Encoding().GetBytes(csv), "text/csv", "Users.csv");
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? userId)
        {
            var isNew = userId == null;

            var user = isNew ? new User() : this.requestDbContext.Users.Find(userId);

            if (user == null)
            {
                throw new NullReferenceException();
            }

            // ----------------------------------------------------
            // Add and Map Missing Localized Kits
            // ----------------------------------------------------

            user.UserLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(user.UserLocalizedKits, new UserLocalizedKit
            {
                User = user
            });

            var userRoles = this.requestDbContext.UserRoles.OrderByDescending(x => x.Name == SharedConstants.DefaultUserRole).ToList();
            var countries = this.requestDbContext.Countries.OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name).ToList();
            var orders = this.requestDbContext.Orders.Where(x => x.UserId == user.Id).ToList();

            var userCountry = user.Country ?? countries.First();
            var userRole = user.Role ?? userRoles.First();

            var authenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext);

            var editUserDto = new EditUserDto
            {
                UserRoles = userRoles.Select(x => new UserRoleDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                Countries = countries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                Orders = orders.Select(x => new OrderDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    CurrencySymbol = x.CurrencySymbol,
                    CountryName = x.CountryName,
                    CountryLanguageCode = x.CountryLanguageCode,
                    OrderNumber = x.OrderNumber,
                    Status = x.Status,
                    StatusDescriptions = x.Status.GetType().GetDescriptions(),
                    TotalAfterTax = x.TotalAfterTax
                }).ToList(),
                User = new UserDto
                {
                    IsNew = isNew,
                    Id = user.Id,
                    CreatedDate = user.CreatedDate,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    ShowMembershipSalePrice = user.ShowMembershipSalePrice,
                    IsDisabled = user.IsDisabled,
                    Gender = user.Gender,
                    GenderDescriptions = user.Gender.GetType().GetDescriptions(),
                    Role = new UserRoleDto
                    {
                        Name = userRole.Name,
                        Id = userRole.Id
                    },
                    Country = new CountryDto
                    {
                        Name = userCountry.Name,
                        Id = userCountry.Id
                    },
                    Addresses = user.Addresses.Select(x => new AddressDto
                    {
                        Id = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        AddressLine1 = x.AddressLine1,
                        AddressLine2 = x.AddressLine2,
                        City = x.City,
                        Country = x.Country == null ? new CountryDto() : new CountryDto
                        {
                            Id = x.Country.Id,
                            Name = x.Country.Name
                        },
                        StateCountyProvince = x.StateCountyProvince,
                        Postcode = x.Postcode,
                        PhoneNumber = x.PhoneNumber
                    }).ToList(),
                    UserLocalizedKits = user.UserLocalizedKits.Select(userLocalizedKit => new UserLocalizedKitDto
                    {
                        Id = userLocalizedKit.Id,
                        Country = userLocalizedKit.Country == null ? new CountryDto() : new CountryDto
                        {
                            Id = userLocalizedKit.Country.Id,
                            Name = userLocalizedKit.Country.Name,
                            IsDefault = userLocalizedKit.Country.IsDefault,
                            CurrencySymbol = userLocalizedKit.Country.GetCurrencySymbol()
                        },
                        User = userLocalizedKit.User == null ? new UserDto() : new UserDto(userLocalizedKit.User.Id),
                        UserCredits = userLocalizedKit.UserCredits.Select(userCredit => new UserCreditDto
                        {
                            Id = userCredit.Id,
                            Amount = userCredit.Amount,
                            Reference = userCredit.Reference,
                            UserLocalizedKit = userCredit.UserLocalizedKit == null ? new UserLocalizedKitDto() : new UserLocalizedKitDto(userCredit.UserLocalizedKit.Id)
                        }).ToList()
                    }).ToList()
                },
                AuthenticatedUser = authenticatedUser
            };

            return this.utilityService.GetCamelCaseJsonResult(editUserDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(UserDto dto)
        {
            dto.Password = string.IsNullOrEmpty(dto.Password)
                ? this.utilityService.Decrypt(this.requestDbContext.Users.Find(dto.Id)?.Password)
                : dto.Password;

            var validationResult = this.userDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var user = this.adminUserService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { userId = user.Id }));
        }

        public ActionResult CleanPartialUsers()
        {
            System.Web.HttpContext.Current.Server.ScriptTimeout = 60 * 4;

            var maxDate = DateTime.Now.AddMonths(-3);

            var partialUsersToDelete = this.requestDbContext.Users.Where(x => x.Password == null && x.Email == null && x.CreatedDate < maxDate).ToList();

            foreach (var user in partialUsersToDelete)
            {
                this.requestDbContext.Users.Delete(user.Id, false);
            }

            this.requestDbContext.SaveChanges();

            return this.View("Index");
        }

        public ActionResult DeleteDisabled()
        {
            System.Web.HttpContext.Current.Server.ScriptTimeout = 60 * 4;

            var disabledUsersToDelte = this.requestDbContext.Users.Where(x => x.IsDisabled).ToList();

            foreach (var user in disabledUsersToDelte)
            {
                this.requestDbContext.Users.Delete(user.Id, false);
            }

            this.requestDbContext.SaveChanges();

            return this.View("Index");
        }

        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.Users.Delete(id, true);
            }
            catch (Exception ex)
            {
                formResponse.Errors.Add(new FormError(ex.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        // ------------------------------------------------------------
        // USER ROLES
        // ------------------------------------------------------------

        public virtual ActionResult Roles()
        {
            return this.View();
        }

        public virtual JsonResult RolesJson()
        {
            var dto = new UserRoleListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                UserRoles = this.requestDbContext.UserRoles.Select(userRole => new UserRoleDto
                {
                    Id = userRole.Id,
                    Name = userRole.Name,
                    IsDisabled = userRole.IsDisabled
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        public virtual ActionResult EditUserRole()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditUserRoleJson(Guid? userRoleId)
        {
            var userRole = userRoleId == null ? new UserRole() : this.requestDbContext.UserRoles.Find(userRoleId);

            if (userRole == null)
            {
                throw new NullReferenceException();
            }

            var dto = new EditUserRoleDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                UserRole = new UserRoleDto
                {
                    Id = userRole.Id,
                    AccessAdmin = userRole.AccessAdmin,
                    AccessAdminBlog = userRole.AccessAdminBlog,
                    AccessAdminContent = userRole.AccessAdminContent,
                    AccessAdminCountries = userRole.AccessAdminCountries,
                    AccessAdminEcommerce = userRole.AccessAdminEcommerce,
                    AccessAdminEmails = userRole.AccessAdminEmails,
                    AccessAdminMedia = userRole.AccessAdminMedia,
                    AccessAdminOrders = userRole.AccessAdminOrders,
                    AccessAdminSeo = userRole.AccessAdminSeo,
                    AccessAdminSettings = userRole.AccessAdminSettings,
                    AccessAdminUsers = userRole.AccessAdminUsers,
                    CreateAttribute = userRole.CreateAttribute,
                    CreateAttributeOption = userRole.CreateAttributeOption,
                    CreateBlogPost = userRole.CreateBlogPost,
                    CreateContentSection = userRole.CreateContentSection,
                    CreateCountry = userRole.CreateCountry,
                    CreateCoupon = userRole.CreateCoupon,
                    CreateCategory = userRole.CreateCategory,
                    CreateEmail = userRole.CreateEmail,
                    CreateMailingList = userRole.CreateMailingList,
                    CreateProduct = userRole.CreateProduct,
                    CreateSeoSection = userRole.CreateSeoSection,
                    CreateShippingBox = userRole.CreateShippingBox,
                    CreateUser = userRole.CreateUser,
                    CreateUserRole = userRole.CreateUserRole,
                    CreateVariant = userRole.CreateVariant,
                    CreateVariantOption = userRole.CreateVariantOption,
                    DeleteAttribute = userRole.DeleteAttribute,
                    DeleteAttributeOption = userRole.DeleteAttributeOption,
                    DeleteBlogPost = userRole.DeleteBlogPost,
                    DeleteContentSection = userRole.DeleteContentSection,
                    DeleteCountry = userRole.DeleteCountry,
                    DeleteCoupon = userRole.DeleteCoupon,
                    DeleteCategory = userRole.DeleteCategory,
                    DeleteEmail = userRole.DeleteEmail,
                    DeleteMailingList = userRole.DeleteMailingList,
                    DeleteProduct = userRole.DeleteProduct,
                    DeleteSeoSection = userRole.DeleteSeoSection,
                    DeleteShippingBox = userRole.DeleteShippingBox,
                    DeleteUser = userRole.DeleteUser,
                    DeleteUserRole = userRole.DeleteUserRole,
                    DeleteVariant = userRole.DeleteVariant,
                    DeleteVariantOption = userRole.DeleteVariantOption,
                    EditAttribute = userRole.EditAttribute,
                    EditAttributeAllLocalizedKits = userRole.EditAttributeAllLocalizedKits,
                    EditAttributeName = userRole.EditAttributeName,
                    EditAttributeOption = userRole.EditAttributeOption,
                    EditAttributeOptionAllLocalizedKits = userRole.EditAttributeOptionAllLocalizedKits,
                    EditAttributeOptionName = userRole.EditAttributeOptionName,
                    EditBlogPost = userRole.EditBlogPost,
                    EditContentSection = userRole.EditContentSection,
                    EditContentSectionAllLocalizedKits = userRole.EditContentSectionAllLocalizedKits,
                    EditContentSectionName = userRole.EditContentSectionName,
                    EditCountry = userRole.EditCountry,
                    EditCoupon = userRole.EditCoupon,
                    EditCategory = userRole.EditCategory,
                    EditCategoryAllLocalizedKits = userRole.EditCategoryAllLocalizedKits,
                    EditCategoryName = userRole.EditCategoryName,
                    EditCategoryUrl = userRole.EditCategoryUrl,
                    EditEmail = userRole.EditEmail,
                    EditMailingList = userRole.EditMailingList,
                    EditMailingListName = userRole.EditMailingListName,
                    EditEmailAllLocalizedKits = userRole.EditEmailAllLocalizedKits,
                    EditMailingListAllLocalizedKits = userRole.EditMailingListAllLocalizedKits,
                    EditEmailName = userRole.EditEmailName,
                    EditOrder = userRole.EditOrder,
                    EditProduct = userRole.EditProduct,
                    EditProductAllLocalizedKits = userRole.EditProductAllLocalizedKits,
                    EditSeoSection = userRole.EditSeoSection,
                    EditSeoSectionAllLocalizedKits = userRole.EditSeoSectionAllLocalizedKits,
                    EditSeoSectionPage = userRole.EditSeoSectionPage,
                    EditShippingBox = userRole.EditShippingBox,
                    EditShippingBoxAllLocalizedKits = userRole.EditShippingBoxAllLocalizedKits,
                    EditShippingBoxName = userRole.EditShippingBoxName,
                    EditUser = userRole.EditUser,
                    EditUserRole = userRole.EditUserRole,
                    EditVariant = userRole.EditVariant,
                    EditVariantAllLocalizedKits = userRole.EditVariantAllLocalizedKits,
                    EditVariantName = userRole.EditVariantName,
                    EditVariantSelector = userRole.EditVariantSelector,
                    EditVariantOption = userRole.EditVariantOption,
                    EditVariantOptionAllLocalizedKits = userRole.EditVariantOptionAllLocalizedKits,
                    EditVariantOptionName = userRole.EditVariantOptionName,
                    IsDisabled = userRole.IsDisabled,
                    Name = userRole.Name,
                    ViewExceptionLogs = userRole.ViewExceptionLogs,
                    ViewEcommerceLogs = userRole.ViewEcommerceLogs,
                    ViewContentLogs = userRole.ViewContentLogs
                }
            };

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateUserRole(UserRoleDto dto)
        {
            var validationResult = this.userRoleDtoValidator.Validate(dto);

            var dbUserRole = this.requestDbContext.UserRoles.Find(dto.Id);

            if (dbUserRole == null)
            {
                var userRoleByName = this.requestDbContext.UserRoles.FirstOrDefault(x => x.Name == dto.Name);

                if (userRoleByName != null)
                {
                    validationResult.Errors.Add(new ValidationFailure(string.Empty, "Already existing user role name"));
                }
            }

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var userRole = this.adminUserService.AddOrUpdateUserRole(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("EditUserRole", new { userRoleId = userRole.Id }));
        }

        public virtual JsonResult DeleteUserRole(Guid userRoleId)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.UserRoles.Delete(userRoleId, true);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        // -------------------------------------------------------------
        // User Credit
        // -------------------------------------------------------------

        [HttpPost]
        public virtual JsonResult AddOrUpdateUserCredit(UserCreditDto dto)
        {
            var formResponse = new FormResponse();

            if (dto.Amount.ApproximatelyEqualsTo(0))
            {
                formResponse.Errors.Add(new FormError("Amount", "Amount cannot be 0"));

                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            this.adminUserService.AddOrUpdateUserCredit(this.requestDbContext, dto);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateUserCredits(List<UserCreditDto> userCredits)
        {
            var formResponse = new FormResponse();

            if (userCredits.Any(x => x.Amount.ApproximatelyEqualsTo(0)))
            {
                formResponse.Errors.Add(new FormError("Amount", "Amount cannot be 0"));

                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            this.adminUserService.AddOrUpdateUserCredits(this.requestDbContext, userCredits);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        public virtual JsonResult DeleteUserCredit(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.UserCredits.Delete(id, true);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}