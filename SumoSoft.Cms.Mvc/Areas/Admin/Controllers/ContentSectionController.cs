﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class ContentSectionController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminContentSectionService adminContentSectionService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<ContentSectionDto> contentSectionDtoValidator;

        public ContentSectionController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IAdminContentSectionService adminContentSectionService,
            IValidator<ContentSectionDto> contentSectionDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.adminContentSectionService = adminContentSectionService;
            this.contentSectionDtoValidator = contentSectionDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson(int page = 1, string filter = null)
        {
            const int pageSize = 50;

            var countries = this.requestDbContext.Countries.Where(x => x.Localize).OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name);

            var contentSectionQuery = this.requestDbContext.ContentSections.AsQueryable();

            if (!string.IsNullOrEmpty(filter))
            {
                contentSectionQuery = contentSectionQuery.Where(x => x.Name.Contains(filter) || x.ContentSectionLocalizedKits.Any(y => y.Content.Contains(filter)));
            }

            var contentSectionsCount = contentSectionQuery.Count();

            var contentSections = contentSectionQuery
                .OrderBy(x => x.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            foreach (var contentSection in contentSections)
            {
                contentSection.ContentSectionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(contentSection.ContentSectionLocalizedKits, new ContentSectionLocalizedKit
                {
                    ContentSection = contentSection
                });
            }

            // -----------------------------------------------------------------------------
            // Create dto
            // -----------------------------------------------------------------------------

            var dto = new EditContentSectionsDto
            {
                TotalItems = contentSectionsCount,
                PageSize = pageSize,
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Countries = countries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    LanguageCode = x.LanguageCode,
                    IsDefault = x.IsDefault,
                    FlagIcon = x.FlagIcon
                }).ToList(),
                ContentSections = contentSections.Select(x => new ContentSectionDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Schema = x.Schema,
                    ContentSectionLocalizedKits = x.ContentSectionLocalizedKits.Select(localizedKit => new ContentSectionLocalizedKitDto
                    {
                        Id = localizedKit.Id,
                        ContentSection = localizedKit.ContentSection == null ? null : new ContentSectionDto
                        {
                            Id = localizedKit.ContentSection.Id,
                            Name = localizedKit.ContentSection.Name
                        },
                        Country = localizedKit.Country == null ? null : new CountryDto
                        {
                            Id = localizedKit.Country.Id,
                            Name = localizedKit.Country.Name,
                            LanguageCode = localizedKit.Country.LanguageCode,
                            IsDefault = localizedKit.Country.IsDefault
                        },
                        Content = localizedKit.Content
                    }).OrderByDescending(y => y.Country?.IsDefault).ThenBy(y => y.Country?.Name).ToList()
                }).ToList()
            };

            // -----------------------------------------------------------------------------
            // Get schema names from schema directory
            // -----------------------------------------------------------------------------

            var contentSectionSchemaDirectory = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("~/Scripts/contentSectionSchemas"));

            if (contentSectionSchemaDirectory.Exists)
            {
                dto.ContentSectionsSchemaNames = contentSectionSchemaDirectory.GetFiles().Where(x => x.Extension == ".ts").Select(x => x.Name.Substring(1, x.Name.IndexOf(x.Extension, StringComparison.Ordinal) - 1)).ToList();
            }

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        /// <summary>
        /// This endpoint is only used in Hiro.
        /// </summary>
        /// <param name="contentSectionId"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult EditJson(Guid? contentSectionId)
        {
            var contentSection = contentSectionId == null ?
                new ContentSection() :
                this.requestDbContext.ContentSections.Find(contentSectionId) ?? throw new NullReferenceException();

            contentSection.ContentSectionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(contentSection.ContentSectionLocalizedKits, new ContentSectionLocalizedKit
            {
                ContentSection = contentSection
            });

            var contentSectionDto = new ContentSectionDto
            {
                Id = contentSection.Id,
                Name = contentSection.Name,
                Schema = contentSection.Schema,
                ContentSectionLocalizedKits = contentSection.ContentSectionLocalizedKits.Select(localizedKit => new ContentSectionLocalizedKitDto
                {
                    Id = localizedKit.Id,
                    ContentSection = localizedKit.ContentSection == null ? null : new ContentSectionDto
                    {
                        Id = localizedKit.ContentSection.Id,
                        Name = localizedKit.ContentSection.Name
                    },
                    Country = localizedKit.Country == null ? null : new CountryDto
                    {
                        Id = localizedKit.Country.Id,
                        Name = localizedKit.Country.Name,
                        LanguageCode = localizedKit.Country.LanguageCode,
                        IsDefault = localizedKit.Country.IsDefault
                    },
                    Content = localizedKit.Content
                }).OrderByDescending(y => y.Country?.IsDefault).ThenBy(y => y.Country?.Name).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(contentSectionDto);
        }

        [HttpPost]
        public virtual JsonResult GetSchemaPropertiesJson(string schema)
        {
            if (string.IsNullOrEmpty(schema))
            {
                return null;
            }

            var schemaType = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .FirstOrDefault(t => t.FullName != null && t.FullName.EndsWith("ContentSectionSchemas." + schema));

            if (schemaType == null)
            {
                throw new NullReferenceException("Schema not found");
            }

            var schemaDescriptionAttributes = (DescriptionAttribute[])schemaType.GetCustomAttributes(typeof(DescriptionAttribute), false);

            var schemaDescription = schemaDescriptionAttributes.ElementAtOrDefault(0)?.Description;

            var schemaProperties = schemaType.GetProperties().Select(propertyInfo => new SchemaPropertyDto
            {
                Name = propertyInfo.Name.ToLowerFirstLetter(),
                DisplayName = propertyInfo.GetDisplayAttribute()?.Name ?? propertyInfo.Name,
                DisplayDescription = propertyInfo.GetDisplayAttribute()?.Description,
                Type = propertyInfo.PropertyType == typeof(List<string>)
                    ? "string[]"
                    : propertyInfo.PropertyType == typeof(List<List<string>>)
                        ? "string[][]"
                        : "string"
            });

            var model = new
            {
                Description = schemaDescription,
                Properties = schemaProperties
            };

            return this.utilityService.GetCamelCaseJsonResult(model);
        }

        [HttpPost]
        public virtual JsonResult GetContentVersions(Guid contentSectionLocalizedKitId)
        {
            var localizedKit = this.requestDbContext.ContentSectionLocalizedKits.Find(contentSectionLocalizedKitId).AsNotNull();

            var model = localizedKit.ContentVersions.Select(version => new ContentVersionDto
            {
                Id = version.Id,
                Content = version.Content,
                CreatedDate = version.CreatedDate,
                ContentSectionLocalizedKit = version.ContentSectionLocalizedKit == null ? null : new ContentSectionLocalizedKitDto
                {
                    Id = version.ContentSectionLocalizedKit.Id,
                    ContentSection = localizedKit.ContentSection == null ? null : new ContentSectionDto(localizedKit.ContentSection.Id),
                }
            }).OrderByDescending(y => y.CreatedDate).ToList();

            return this.utilityService.GetCamelCaseJsonResult(model);
        }

        [HttpPost]
        public virtual JsonResult Save(ContentSectionDto dto)
        {
            using (var dbContext = new CmsDbContext())
            {
                var validationResult = this.contentSectionDtoValidator.Validate(dto);

                if (!validationResult.IsValid)
                {
                    return this.utilityService.GetFormResponseJson(validationResult);
                }

                this.adminContentSectionService.AddOrUpdate(dbContext, dto);

                return this.utilityService.GetFormResponseJson(this.ModelState);
            }
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.ContentSections.Delete(id, true);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the content section: " + e.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        public FileContentResult ExportCsv()
        {
            var contentSections = this.requestDbContext.ContentSections.OrderBy(x => x.Name).ToList();

            var countryLanguageCodes = this.requestDbContext.Countries.OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name).Select(x => x.LanguageCode).ToList();

            var csv = new StringBuilder();

            csv.Append("Name,");

            foreach (var countryLanguageCode in countryLanguageCodes)
            {
                csv.Append($"{countryLanguageCode},");
            }

            csv.AppendLine();

            foreach (var contentSection in contentSections)
            {
                csv.Append($"{contentSection.Name.ToValidCsvString()},");

                foreach (var countryLanguageCode in countryLanguageCodes)
                {
                    var localizedKit = contentSection.ContentSectionLocalizedKits.GetByLanguageCode(countryLanguageCode);

                    csv.Append($"{localizedKit?.Content.ToValidCsvString()},");
                }

                csv.AppendLine();
            }

            return this.File(new UTF8Encoding().GetBytes(csv.ToString()), "text/csv", "ContentSections.csv");
        }
    }
}