﻿#pragma warning disable 4014

#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services.Dto;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class MediaController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly bool useAzureStorage;
        private readonly IAzureStorageService azureStorageService;
        private readonly ILocalStorageService localStorageService;
        private readonly IAdminUtilityService adminIUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<DirectoryDto> directoryDtoValidator;
        private readonly IValidator<BatchRenameFilesDto> batchRenameFilesDtoValidator;
        private readonly IValidator<RenameFileDto> renameFileDtoValidator;
        private readonly IValidator<ResizeFileDto> resizeFileDtoValidator;

        public MediaController(
            IAzureStorageService azureStorageService,
            ILocalStorageService localStorageService,
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<DirectoryDto> directoryDtoValidator,
            IValidator<BatchRenameFilesDto> batchRenameFilesDtoValidator,
            IValidator<RenameFileDto> renameFileDtoValidator,
            IValidator<ResizeFileDto> resizeFileDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.azureStorageService = azureStorageService;
            this.localStorageService = localStorageService;
            this.adminIUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.directoryDtoValidator = directoryDtoValidator;
            this.batchRenameFilesDtoValidator = batchRenameFilesDtoValidator;
            this.renameFileDtoValidator = renameFileDtoValidator;
            this.resizeFileDtoValidator = resizeFileDtoValidator;

            this.useAzureStorage = this.requestDbContext.CmsSettings.First().UseAzureStorage;
        }

        public virtual ActionResult Index(string localPath)
        {
            return this.View("Index");
        }

        [HttpPost]
        public virtual JsonResult IndexJson(string localPath, string keywordFilter)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                localPath = $"/{this.utilityService.GetAppSetting("AzureStorageContainer")}/";
            }

            return this.utilityService.GetCamelCaseJsonResult(this.useAzureStorage ? this.azureStorageService.GetDirectoryDto(localPath, keywordFilter) : this.localStorageService.GetDirectoryDto(localPath, keywordFilter));
        }

        [HttpPost]
        public virtual JsonResult UploadFileForm()
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var formResponse = new FormResponse();

            var uploadedFiles = this.Request.Files;

            if (uploadedFiles.Count > 100)
            {
                formResponse.AddError("The maximum number of files that is possible to upload at the same time is 100");

                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            // ---------------------------------------------------------------
            // Upload files and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var directoryFullName = this.Request.Form[0];

            for (var i = 0; i < uploadedFiles.Count; i++)
            {
                var file = uploadedFiles[i];

                if (file == null || file.ContentLength <= 0)
                {
                    continue;
                }

                var stream = file.InputStream;
                var fileName = file.FileName.ToPrettyImageUrl();

                if (!SharedConstants.ValidMediaExtensions.Any(item => fileName.EndsWith(item.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    formResponse.AddError($"The file '{fileName}' could not be uploaded. This file type is not supported.");

                    continue;
                }

                var errors = this.useAzureStorage
                    ? this.azureStorageService.UploadFile(directoryFullName, stream, fileName).Errors
                    : this.localStorageService.UploadFile(directoryFullName, stream, fileName).Errors;

                formResponse.Errors.AddRange(errors);
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult NewDirectoryForm(DirectoryDto model)
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var validationResult = this.directoryDtoValidator.Validate(model);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            // ---------------------------------------------------------------
            // Create new directory and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var formResponse = new FormResponse();

            model.Name = model.Name.ToPrettyUrl();

            List<FormError> errors;

            if (this.useAzureStorage)
            {
                errors = this.azureStorageService.CreateDirectory(model.LocalPath, model.Name).Errors;

                formResponse.RedirectUrl = this.Url.Action("Index", "Media", new { localPath = $"{model.LocalPath.AsNotNull().TrimEnd('/')}/{model.Name}/" }).AsNotNull().Replace("%2F", "/");
            }
            else
            {
                errors = this.localStorageService.CreateDirectory(model.LocalPath, model.Name).Errors;

                formResponse.RedirectUrl = this.Url.Action("Index", "Media", new { localPath = $"{model.LocalPath}/{model.Name}" }).AsNotNull().Replace("%2F", "/");
            }

            formResponse.Errors.AddRange(errors);
            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult DeleteFilesForm(List<string> localPaths)
        {
            // ---------------------------------------------------------------
            // Delete files and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var formResponse = new FormResponse();

            foreach (var url in localPaths)
            {
                var errors = this.useAzureStorage ? this.azureStorageService.DeleteFile(url).Errors : this.localStorageService.DeleteFile(url).Errors;

                formResponse.Errors.AddRange(errors);
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult DeleteDirectoryForm(string localPath)
        {
            var formResponse = new FormResponse();

            var errors = this.useAzureStorage
                ? this.azureStorageService.DeleteDirectory(localPath).Errors
                : this.localStorageService.DeleteDirectory(localPath).Errors;

            formResponse.Errors.AddRange(errors);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult ResizeFilesForm(ResizeFileDto dto)
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var validationResult = this.resizeFileDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            // ---------------------------------------------------------------
            // Resize files and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var formResponse = new FormResponse();

            if (this.useAzureStorage)
            {
                foreach (var url in dto.LocalPaths)
                {
                    var errors = this.azureStorageService.ResizeFile(url, dto).Errors;

                    formResponse.Errors.AddRange(errors);
                }
            }
            else
            {
                foreach (var url in dto.LocalPaths)
                {
                    var errors = this.localStorageService.ResizeFile(url, dto).Errors;

                    formResponse.Errors.AddRange(errors);
                }
            }

            formResponse.RedirectUrl = this.Url.Action("Index", "Media", new { localPath = dto.Destination }).AsNotNull().Replace("%2F", "/");

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult RenameFileForm(RenameFileDto dto)
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var validationResult = this.renameFileDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            // ---------------------------------------------------------------
            // Rename file and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var formResponse = this.RenameFile(dto.LocalPath, dto.NewName);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult BatchRenameFiles(BatchRenameFilesDto dto)
        {
            // ---------------------------------------------------------------
            // Formal validation
            // ---------------------------------------------------------------

            var validationResult = this.batchRenameFilesDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            // ---------------------------------------------------------------
            // Batch Rename files and collect exceptions as validation errors
            // ---------------------------------------------------------------

            var formResponse = new FormResponse();

            var files = dto.LocalPaths.Select(localPath =>
                this.useAzureStorage
                    ? this.azureStorageService.GetFileDto(localPath)
                    : this.localStorageService.GetFileDto(localPath)).ToList();

            foreach (var fileDto in files)
            {
                var newName = this.useAzureStorage ? this.azureStorageService.GetFileDto(fileDto.LocalPath)?.Name : this.localStorageService.GetFileDto(fileDto.LocalPath)?.Name;

                if (newName == null)
                {
                    formResponse.Errors.Add(new FormError($"Name is null for file: '{fileDto.LocalPath}'"));

                    continue;
                }

                if (!string.IsNullOrEmpty(dto.ReplaceText))
                {
                    newName = newName.Replace(dto.ReplaceText, dto.ReplaceTextTarget);
                }

                if (!string.IsNullOrEmpty(dto.PrefixText))
                {
                    newName = dto.PrefixText + newName;
                }

                if (!string.IsNullOrEmpty(dto.SuffixText))
                {
                    newName = newName + dto.SuffixText;
                }

                var errors = this.RenameFile(fileDto.LocalPath, newName).Errors;

                formResponse.Errors.AddRange(errors);
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult Backup(string localPath)
        {
            var backupFileName = this.adminIUtilityService.CreateBackupFileName(localPath);

            if (this.useAzureStorage)
            {
                this.azureStorageService.BackupDirectory(localPath);
            }
            else
            {
                this.localStorageService.BackupDirectory(localPath, backupFileName);
            }

            return this.utilityService.GetFormResponseJson();
        }

        private FormResponse RenameFile(string localPath, string newName)
        {
            var formResponse = new FormResponse();

            var oldname = localPath.Split('/').Last();

            newName = newName.ToPrettyImageUrl();

            var extension = Path.GetExtension(localPath);

            if (newName + extension == oldname)
            {
                formResponse.Errors.Add(new FormError($"The new file name cannot be the same with it's previous name: '{localPath}'"));

                return formResponse;
            }

            string newLocalPath;
            string newAbsolutePath;

            if (this.useAzureStorage)
            {
                newLocalPath = this.azureStorageService.GetFileDto(localPath)?.GetDirectory(this.azureStorageService)?.LocalPath + "/" + newName + extension;

                formResponse = this.azureStorageService.RenameFile(localPath, newName);

                newAbsolutePath = this.azureStorageService.GetFileDto(newLocalPath)?.AbsolutePath;
            }
            else
            {
                newLocalPath = this.localStorageService.GetFileDto(localPath)?.GetDirectory(this.localStorageService)?.LocalPath + "/" + newName + extension;

                formResponse = this.localStorageService.RenameFile(localPath, newName);

                newAbsolutePath = this.localStorageService.GetFileDto(newLocalPath)?.AbsolutePath;
            }

            // ---------------------------------------------------------------------------------
            // Update the entities that were referencing the image
            // ---------------------------------------------------------------------------------

            this.requestDbContext.ProductImages.BulkUpdate("Url", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.requestDbContext.ProductAttributes.BulkUpdate("ImageValue", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.requestDbContext.VariantOptions.BulkUpdate("Image", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.requestDbContext.CategoryLocalizedKits.BulkUpdate("Image", localPath, newAbsolutePath, PreviousValueMatch.Contains);
            this.requestDbContext.BlogPosts.BulkUpdate("FeaturedImage", localPath, newAbsolutePath, PreviousValueMatch.Contains);

            return formResponse;
        }
    }
}