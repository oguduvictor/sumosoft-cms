﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class BlogCategoryController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IAdminBlogCategoryService adminBlogCategoryService;
        private readonly IValidator<BlogCategoryDto> blogCategoryDtoValidator;

        public BlogCategoryController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IAdminBlogCategoryService adminBlogCategoryService,
            IValidator<BlogCategoryDto> blogCategoryDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.adminBlogCategoryService = adminBlogCategoryService;
            this.blogCategoryDtoValidator = blogCategoryDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public JsonResult IndexJson()
        {
            var blogCategories = new BlogCategoryListDto
            {
                BlogCategories = this.requestDbContext.BlogCategories.OrderByDescending(x => x.CreatedDate).AsEnumerable().Select(x => new BlogCategoryDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    LocalizedTitle = x.GetLocalizedTitle(),
                    BlogPosts = x.BlogPosts.Select(post => new BlogPostDto(post.Id)).ToList()
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(blogCategories);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public JsonResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var blogCategory = isNew ? new BlogCategory() : this.requestDbContext.BlogCategories.Find(id);

            if (blogCategory == null)
            {
                throw new NullReferenceException();
            }

            // ---------------------------------------------------------------------
            // Add missing ShippingBoxLocalizedKits
            // ---------------------------------------------------------------------

            blogCategory.BlogCategoryLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(blogCategory.BlogCategoryLocalizedKits, new BlogCategoryLocalizedKit
            {
                BlogCategory = blogCategory
            });

            // ---------------------------------------------------------------------
            // Model
            // ---------------------------------------------------------------------

            var blogCategoryDto = new BlogCategoryDto
            {
                Id = blogCategory.Id,
                IsNew = isNew,
                IsDisabled = blogCategory.IsDisabled,
                Name = blogCategory.Name,
                Url = blogCategory.Url,
                LocalizedTitle = blogCategory.GetLocalizedTitle(),
                LocalizedDescription = blogCategory.GetLocalizedDescription(),
                BlogCategoryLocalizedKits = blogCategory.BlogCategoryLocalizedKits.OrderByDefaultCountry().Select(localizedKit => new BlogCategoryLocalizedKitDto
                {
                    Id = localizedKit.Id,
                    Title = localizedKit.Title,
                    Description = localizedKit.Description,
                    Country = localizedKit.Country == null ? null : new CountryDto
                    {
                        Id = localizedKit.Country.Id,
                        Name = localizedKit.Country.Name,
                        IsDefault = localizedKit.Country.IsDefault
                    },
                    BlogCategory = localizedKit.BlogCategory == null ? null : new BlogCategoryDto(localizedKit.BlogCategory.Id)
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(blogCategoryDto);
        }

        [HttpPost]
        public JsonResult AddOrUpdate(BlogCategoryDto dto)
        {
            var validationResult = this.blogCategoryDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var blogCategory = this.adminBlogCategoryService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { id = blogCategory.Id }));
        }

        public JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.BlogCategories.Delete(id, true);
            }
            catch (Exception)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the entity"));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}