﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class AttributeController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<AttributeDto> attributeDtoValidator;
        private readonly IAdminAttributeService adminAttributeService;

        public AttributeController(IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<AttributeDto> attributeDtoValidator,
            IAdminAttributeService adminAttributeService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.attributeDtoValidator = attributeDtoValidator;
            this.adminAttributeService = adminAttributeService;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        public virtual JsonResult IndexJson()
        {
            var attributeListDto = new AttributeListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Attributes = this.requestDbContext.Attributes.OrderBy(x => x.SortOrder).AsEnumerable().Select(attribute => new AttributeDto
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    SortOrder = attribute.SortOrder,
                    Type = attribute.Type,
                    TypeDescriptions = attribute.Type.GetType().GetDescriptions(),
                    LocalizedTitle = attribute.GetLocalizedTitle(),
                })
                .ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(attributeListDto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? attributeId)
        {
            var isNew = attributeId == null;

            var attribute = isNew ? new Domain.Attribute() : this.requestDbContext.Attributes.Find(attributeId);

            if (attribute == null)
            {
                throw new NullReferenceException();
            }

            attribute.AttributeLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(attribute.AttributeLocalizedKits, new AttributeLocalizedKit
            {
                Attribute = attribute
            });

            foreach (var attributeOption in attribute.AttributeOptions)
            {
                attributeOption.AttributeOptionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(attributeOption.AttributeOptionLocalizedKits, new AttributeOptionLocalizedKit
                {
                    AttributeOption = attributeOption
                });
            }

            var editAttributeDto = new EditAttributeDto
            {
                Attribute = new AttributeDto
                {
                    Id = attribute.Id,
                    IsNew = isNew,
                    Name = attribute.Name,
                    Url = attribute.Url,
                    SortOrder = attribute.SortOrder,
                    Type = attribute.Type,
                    TypeDescriptions = attribute.Type.GetType().GetDescriptions(),
                    AttributeOptions = attribute.AttributeOptions.Select(option => new AttributeOptionDto
                    {
                        Id = option.Id,
                        Name = option.Name,
                        Url = option.Url,
                        Tags = option.Tags,
                        LocalizedTitle = option.GetLocalizedTitle(),
                        Attribute = new AttributeDto(option.Attribute.Id),
                        AttributeOptionLocalizedKits = option.AttributeOptionLocalizedKits.Select(localizedKit => new AttributeOptionLocalizedKitDto
                        {
                            Id = localizedKit.Id,
                            Title = localizedKit.Title,
                            Description = localizedKit.Description,
                            IsDisabled = localizedKit.IsDisabled,
                            AttributeOption = localizedKit.AttributeOption == null ? null : new AttributeOptionDto(localizedKit.AttributeOption.Id),
                            Country = new CountryDto
                            {
                                Id = localizedKit.Country?.Id ?? throw new NullReferenceException(),
                                Name = localizedKit.Country.Name,
                                IsDefault = localizedKit.Country.IsDefault
                            }
                        }).ToList()
                    }).ToList(),
                    AttributeLocalizedKits = attribute.AttributeLocalizedKits.Select(localizedKit => new AttributeLocalizedKitDto
                    {
                        Id = localizedKit.Id,
                        Title = localizedKit.Title,
                        Description = localizedKit.Description,
                        Attribute = localizedKit.Attribute == null ? null : new AttributeDto(localizedKit.Attribute.Id),
                        Country = localizedKit.Country == null ? null : new CountryDto
                        {
                            Id = localizedKit.Country.Id,
                            Name = localizedKit.Country.Name,
                            IsDefault = localizedKit.Country.IsDefault
                        }
                    }).ToList(),
                    LocalizedTitle = attribute.GetLocalizedTitle()
                },
                NewAttributeOption = new AttributeOptionDto
                {
                    IsNew = true,
                    Attribute = new AttributeDto(attribute.Id),
                    AttributeOptionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(new List<AttributeOptionLocalizedKit>(), new AttributeOptionLocalizedKit
                    {
                        AttributeOption = new AttributeOption()
                    }).Select(localizedKit => new AttributeOptionLocalizedKitDto
                    {
                        Id = localizedKit.Id,
                        IsNew = true,
                        Title = localizedKit.Title,
                        Description = localizedKit.Description,
                        AttributeOption = new AttributeOptionDto(),
                        Country = new CountryDto
                        {
                            Id = localizedKit.Country?.Id ?? throw new NullReferenceException(),
                            Name = localizedKit.Country.Name,
                            IsDefault = localizedKit.Country.IsDefault
                        }
                    }).ToList()
                },
                AllTags = this.requestDbContext.AttributeOptions.AsEnumerable().SelectMany(x => x.GetTagList()).Distinct().ToList(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext)
            };

            editAttributeDto.Attribute.IsNew = isNew;

            return this.utilityService.GetCamelCaseJsonResult(editAttributeDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(AttributeDto dto)
        {
            var validationResult = this.attributeDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var attribute = this.adminAttributeService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { attributeId = attribute.Id }));
        }

        [HttpPost]
        public virtual JsonResult UpdateSortOrder(List<AttributeDto> attributeDtos)
        {
            var formResponse = new FormResponse<List<AttributeDto>>();

            this.adminAttributeService.UpdateSortOrder(this.requestDbContext, attributeDtos);

            formResponse.Target = this.requestDbContext.Attributes.OrderBy(x => x.SortOrder).AsEnumerable().Select(attribute => new AttributeDto
            {
                Id = attribute.Id,
                Name = attribute.Name,
                SortOrder = attribute.SortOrder,
                Type = attribute.Type,
                TypeDescriptions = attribute.Type.GetType().GetDescriptions(),
                LocalizedTitle = attribute.GetLocalizedTitle(),
            }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid id)
        {
            var attribute = this.requestDbContext.Attributes.Find(id);

            if (attribute == null)
            {
                throw new NullReferenceException();
            }

            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.Attributes.Delete(id, true);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}