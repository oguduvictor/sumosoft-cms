﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class CategoryController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<CategoryDto> categoryDtoValidator;
        private readonly IAdminCategoryService adminCategoryService;
        private readonly IContentSectionService contentSectionService;

        public CategoryController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IContentSectionService contentSectionService,
            IValidator<CategoryDto> categoryDtoValidator,
            IAdminCategoryService adminCategoryService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.categoryDtoValidator = categoryDtoValidator;
            this.adminCategoryService = adminCategoryService;
            this.contentSectionService = contentSectionService;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var categoryListDto = new CategoryListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Categories = this.adminCategoryService.GetRootCategories(this.requestDbContext).Select(GetRootCategoryDto).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(categoryListDto);
        }

        [HttpPost]
        public virtual JsonResult UpdateHierarchy(List<CategoryDto> categories)
        {
            var formResponse = new FormResponse<List<CategoryDto>>();

            this.adminCategoryService.UpdateHierarchy(this.requestDbContext, categories);

            formResponse.Target = this.adminCategoryService.GetRootCategories(this.requestDbContext).Select(GetRootCategoryDto).ToList();

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? categoryId)
        {
            var isNew = categoryId == null;

            var category = isNew ? new Category() : this.requestDbContext.Categories.Find(categoryId);

            if (category == null)
            {
                throw new NullReferenceException();
            }

            category.CategoryLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(category.CategoryLocalizedKits, new CategoryLocalizedKit
            {
                Category = category
            });

            var products = (string.IsNullOrEmpty(category.ProductsSortOrder)
                ? category.Products.OrderBy(x => x.Url)
                : category.GetSortedProducts(true)).Select(x => new ProductDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.GetLocalizedTitle()
                }).ToList();

            var productImageKits = category.ProductImageKits.OrderBy(x => x.Product?.Url).Select(productImageKit => new ProductImageKitDto
            {
                Id = productImageKit.Id,
                VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                {
                    Variant = new VariantDto { SortOrder = variantOption.Variant?.SortOrder ?? 0 },
                    SortOrder = variantOption.SortOrder,
                    LocalizedTitle = variantOption.GetLocalizedTitle()
                }).OrderBy(x => x.Variant?.SortOrder).ThenBy(x => x.SortOrder).ToList(),
                Product = productImageKit.Product == null ? null : new ProductDto { LocalizedTitle = productImageKit.Product.GetLocalizedTitle() }
            }).ToList();

            var productStockUnits = category.ProductStockUnits.OrderBy(x => x.Product?.Url).Select(stockUnit => new ProductStockUnitDto
            {
                Id = stockUnit.Id,
                VariantOptions = stockUnit.VariantOptions.Select(variantOption => new VariantOptionDto { LocalizedTitle = variantOption.GetLocalizedTitle() }).ToList(),
                Product = stockUnit.Product == null ? null : new ProductDto { LocalizedTitle = stockUnit.Product.GetLocalizedTitle() }
            }).ToList();

            var editCategoryDto = new EditCategoryDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Category = new CategoryDto
                {
                    IsNew = isNew,
                    Id = category.Id,
                    Url = category.Url,
                    SortOrder = category.SortOrder,
                    Name = category.Name,
                    TypeDescriptions = new List<string>
                    {
                        this.contentSectionService.ContentSection(
                            this.requestDbContext,
                            ContentSectionName.Cms_CategoryTypesEnum_Products,
                            ContentSectionName.Cms_CategoryTypesEnum_Products_FallbackValue,
                            true),
                        this.contentSectionService.ContentSection(
                            this.requestDbContext,
                            ContentSectionName.Cms_CategoryTypesEnum_ProductImageKits,
                            ContentSectionName.Cms_CategoryTypesEnum_ProductImageKits_FallbackValue,
                            true),
                        this.contentSectionService.ContentSection(
                            this.requestDbContext,
                            ContentSectionName.Cms_CategoryTypesEnum_ProductStockUnits,
                            ContentSectionName.Cms_CategoryTypesEnum_ProductStockUnits_FallbackValue,
                            true)
                    },
                    Type = category.Type,
                    IsDisabled = category.IsDisabled,
                    LocalizedTitle = category.GetLocalizedTitle(),
                    Parent = category.Parent == null ? null : new CategoryDto
                    {
                        Id = category.Parent.Id,
                        LocalizedTitle = category.Parent.GetLocalizedTitle()
                    },
                    Children = category.Children.Select(x => new CategoryDto
                    {
                        Id = x.Id,
                        LocalizedTitle = x.GetLocalizedTitle()
                    }).OrderBy(x => x.Name).ToList(),
                    Tags = category.Tags,
                    CategoryLocalizedKits = category.CategoryLocalizedKits.Select(categoryLocalizedKit => new CategoryLocalizedKitDto
                    {
                        Id = categoryLocalizedKit.Id,
                        Category = categoryLocalizedKit.Category == null ? null : new CategoryDto(categoryLocalizedKit.Category.Id),
                        Country = categoryLocalizedKit.Country == null ? null : new CountryDto
                        {
                            Id = categoryLocalizedKit.Country.Id,
                            Name = categoryLocalizedKit.Country.Name,
                            IsDefault = categoryLocalizedKit.Country.IsDefault
                        },
                        Title = categoryLocalizedKit.Title,
                        Description = categoryLocalizedKit.Description,
                        Image = categoryLocalizedKit.Image,
                        FeaturedTitle = categoryLocalizedKit.FeaturedTitle,
                        FeaturedDescription = categoryLocalizedKit.FeaturedDescription,
                        FeaturedImage = categoryLocalizedKit.FeaturedImage,
                        IsDisabled = categoryLocalizedKit.IsDisabled,
                        MetaDescription = categoryLocalizedKit.MetaDescription,
                        MetaTitle = categoryLocalizedKit.MetaTitle,
                    }).ToList(),
                    Products = products,
                    ProductImageKits = category.GetSortedProductImageKits(true).Select(x => new ProductImageKitDto(x.Id)).ToList(),
                    ProductStockUnits = category.GetSortedProductStockUnits(true).Select(x => new ProductStockUnitDto(x.Id)).ToList(),
                    ProductsSortOrder = category.ProductsSortOrder,
                    ProductImageKitsSortOrder = category.ProductImageKitsSortOrder,
                    ProductStockUnitsSortOrder = category.ProductStockUnitsSortOrder
                },
                AllTags = this.requestDbContext.Categories.OrderBy(x => x.SortOrder).AsEnumerable().SelectMany(x => x.GetTagList()).Distinct().ToList(),
                AllCategories = this.requestDbContext.Categories.Where(x => x.Id != category.Id).OrderBy(x => x.Name).AsEnumerable().Select(x => new CategoryDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.GetLocalizedTitle()
                }).ToList(),
                // --------------------------------------------------------------------------------------------------------------
                // initially load only the current items. The rest will be Lazy Loaded using Ajax from the React component:
                // --------------------------------------------------------------------------------------------------------------
                AllProducts = products,
                AllProductImageKits = productImageKits,
                AllProductStockUnits = productStockUnits
            };

            return this.utilityService.GetCamelCaseJsonResult(editCategoryDto);
        }

        [HttpPost]
        public virtual JsonResult GetAllProductsJson()
        {
            var productDtos = this.requestDbContext.Products.AsEnumerable().Select(x => new ProductDto
            {
                Id = x.Id,
                LocalizedTitle = x.GetLocalizedTitle()
            }).OrderBy(x => x.LocalizedTitle).ToList();

            return this.utilityService.GetCamelCaseJsonResult(productDtos);
        }

        [HttpPost]
        public virtual JsonResult GetAllProductImageKitsJson()
        {
            var productImageKitDtos = this.requestDbContext.ProductImageKits.AsEnumerable().Select(productImageKit => new ProductImageKitDto
            {
                Id = productImageKit.Id,
                VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                {
                    Variant = new VariantDto { SortOrder = variantOption.Variant?.SortOrder ?? 0 },
                    SortOrder = variantOption.SortOrder,
                    LocalizedTitle = variantOption.GetLocalizedTitle()
                }).OrderBy(x => x.Variant?.SortOrder).ThenBy(x => x.SortOrder).ToList(),
                Product = productImageKit.Product == null ? null : new ProductDto { LocalizedTitle = productImageKit.Product.GetLocalizedTitle() }
            })
            .OrderBy(x => x.Product?.LocalizedTitle).ToList();

            return this.utilityService.GetCamelCaseJsonResult(productImageKitDtos);
        }

        [HttpPost]
        public virtual JsonResult GetAllProductStockUnitsJson()
        {
            var productStockUnitDtos = this.requestDbContext.ProductStockUnits.AsEnumerable().Select(stockUnit => new ProductStockUnitDto
            {
                Id = stockUnit.Id,
                VariantOptions = stockUnit.VariantOptions.Select(variantOption => new VariantOptionDto
                {
                    LocalizedTitle = variantOption.GetLocalizedTitle()
                }).ToList(),
                Product = stockUnit.Product == null ? null : new ProductDto
                {
                    LocalizedTitle = stockUnit.Product.GetLocalizedTitle()
                }
            }).OrderBy(x => x.Product?.LocalizedTitle).ToList();

            return this.utilityService.GetCamelCaseJsonResult(productStockUnitDtos);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(CategoryDto dto)
        {
            var validationResult = this.categoryDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var category = this.adminCategoryService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { categoryId = category.Id }));
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid id)
        {
            var category = this.requestDbContext.Categories.Find(id);

            if (category == null)
            {
                throw new NullReferenceException();
            }

            this.requestDbContext.Categories.Delete(category.Id, true);

            return this.utilityService.GetFormResponseJson();
        }

        private static CategoryDto GetRootCategoryDto(Category category)
        {
            return new CategoryDto
            {
                Id = category.Id,
                Name = category.Name,
                SortOrder = category.SortOrder,
                LocalizedTitle = category.GetLocalizedTitle(),
                Children = category.Children.Any() ? category.Children.Select(GetRootCategoryDto).ToList() : new List<CategoryDto>()
            };
        }
    }
}