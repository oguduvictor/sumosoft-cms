﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.EmailServices.Interfaces;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class EmailController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly ICountryService countryService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<ContentSectionDto> contentSectionDtoValidator;
        private readonly IValidator<EmailDto> emailDtoValidator;
        private readonly IAdminEmailService adminEmailService;
        private readonly IEmailService emailService;

        public EmailController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            ICountryService countryService,
            IUtilityService utilityService,
            IAdminEmailService adminEmailService,
            IValidator<ContentSectionDto> contentSectionDtoValidator,
            IEmailService emailService,
            IValidator<EmailDto> emailDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.countryService = countryService;
            this.utilityService = utilityService;
            this.adminEmailService = adminEmailService;
            this.contentSectionDtoValidator = contentSectionDtoValidator;
            this.emailService = emailService;
            this.emailDtoValidator = emailDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var emailListDto = new EmailListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Emails = this.requestDbContext.Emails.OrderBy(x => x.Name).Select(email => new EmailDto
                {
                    Id = email.Id,
                    Name = email.Name,
                    ViewName = email.ViewName
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(emailListDto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var email = isNew ? new Email() : this.requestDbContext.Emails.Find(id);

            if (email == null)
            {
                throw new NullReferenceException();
            }

            email.EmailLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(email.EmailLocalizedKits, new EmailLocalizedKit
            {
                Email = email
            });

            var editEmailDto = new EditEmailDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Email = new EmailDto
                {
                    IsNew = isNew,
                    Id = email.Id,
                    Name = email.Name,
                    ViewName = email.ViewName,
                    ContentSection = email.ContentSection == null ? null : new ContentSectionDto
                    {
                        Id = email.ContentSection.Id,
                        Name = email.ContentSection.Name
                    },
                    LocalizedFrom = email.GetLocalizedFrom(),
                    LocalizedReplyTo = email.GetLocalizedReplyTo(),
                    LocalizedDisplayName = email.GetLocalizedDisplayName(),
                    EmailLocalizedKits = email.EmailLocalizedKits.Select(emailLocalizedKit => new EmailLocalizedKitDto
                    {
                        Id = emailLocalizedKit.Id,
                        From = emailLocalizedKit.From,
                        ReplyTo = emailLocalizedKit.ReplyTo,
                        DisplayName = emailLocalizedKit.DisplayName,
                        IsDisabled = emailLocalizedKit.IsDisabled,
                        Country = emailLocalizedKit.Country == null ? null : new CountryDto
                        {
                            Id = emailLocalizedKit.Country.Id,
                            Name = emailLocalizedKit.Country.Name,
                            IsDefault = emailLocalizedKit.Country.IsDefault,
                            LanguageCode = emailLocalizedKit.Country.LanguageCode,
                            Url = emailLocalizedKit.Country.Url,
                            Localize = emailLocalizedKit.Country.Localize
                        }
                    }).ToList()
                },
                AllContentSections = this.requestDbContext.ContentSections.Select(x => new ContentSectionDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).OrderBy(x => x.Name).ToList()
            };

            var emailViewsDirectory = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("~/Views/Emails"));

            if (emailViewsDirectory.Exists)
            {
                editEmailDto.ViewNames = emailViewsDirectory
                    .GetFiles()
                    .Where(x => !x.Name.StartsWith("_"))
                    .Select(x => x.Name?.Substring(0, x.Name.IndexOf(x.Extension, StringComparison.Ordinal)))
                    .ToList();
            }

            var contentSectionSchemaDirectory = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("~/Scripts/contentSectionSchemas"));

            if (contentSectionSchemaDirectory.Exists)
            {
                editEmailDto.ContentSectionsSchemaNames = contentSectionSchemaDirectory.GetFiles().Where(x => x.Extension == ".ts").Select(x => x.Name.Substring(1, x.Name.IndexOf(x.Extension, StringComparison.Ordinal) - 1)).ToList();
            }

            return this.utilityService.GetCamelCaseJsonResult(editEmailDto);
        }

        [HttpPost]
        public virtual JsonResult GetAllMailingListsJson()
        {
            var mailingListsDto = this.requestDbContext.MailingLists.Select(mailingList => new MailingListDto
            {
                Id = mailingList.Id,
                LocalizedTitle = mailingList.MailingListLocalizedKits.FirstOrDefault(x => x.Country.IsDefault).Title,
                Name = mailingList.Name
            }).OrderBy(x => x.Name).ToList();

            return this.utilityService.GetCamelCaseJsonResult(mailingListsDto);
        }

        [HttpPost]
        public virtual JsonResult GetAllUsersJson()
        {
            var usersDto = this.requestDbContext.Users.Where(x => !x.IsDisabled && x.Email != null).Select(user => new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(usersDto);
        }

        [HttpPost]
        public virtual JsonResult GetAllCountriesJson()
        {
            var countriesDto = this.requestDbContext.Countries.Where(x => !x.IsDisabled && x.Localize).Select(x => new CountryDto
            {
                Id = x.Id,
                Name = x.Name,
                IsDefault = x.IsDefault
            }).OrderByDescending(x => x.IsDefault).ThenBy(x => x.Name).ToList();

            return this.utilityService.GetCamelCaseJsonResult(countriesDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(EmailDto dto)
        {
            var validationResult = this.emailDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var email = this.adminEmailService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { id = email.Id }));
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.Emails.Delete(id, true);
            }
            catch (Exception)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the entity"));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        public virtual ActionResult ViewInBrowser(Guid id)
        {
            var email = this.requestDbContext.Emails.Find(id);

            if (email == null)
            {
                throw new NullReferenceException();
            }

            object content = email.ContentSection?.GetLocalizedContent();

            return this.View("/Views/Emails/" + email.ViewName + ".cshtml", content);
        }

        [HttpPost]
        public virtual string RenderEmailView(Guid id)
        {
            var email = this.requestDbContext.Emails.Find(id);

            if (email == null)
            {
                throw new NullReferenceException();
            }

            object content = email.ContentSection?.GetLocalizedContent();

            return this.emailService.RenderEmailView(email.ViewName, content);
        }

        [HttpPost]
        public virtual JsonResult SendEmailToUsers(Guid emailId, List<string> to)
        {
            var formResponse = new FormResponse();

            var email = this.requestDbContext.Emails.Find(emailId);

            if (email == null)
            {
                formResponse.Errors.Add(new FormError("An error occurred: email not found"));

                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            try
            {
                this.emailService.ExecuteEmailAction(this.requestDbContext, email.Name, to);

                formResponse.SuccessMessage = "Email successfully sent";
            }
            catch (Exception ex)
            {
                formResponse.Errors.Add(new FormError(ex.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult SendEmailToMailingList(Guid emailId, Guid mailingListId, Guid? countryId)
        {
            var formResponse = new FormResponse();

            var email = this.requestDbContext.Emails.Find(emailId);

            if (email == null)
            {
                formResponse.Errors.Add(new FormError("An error occurred: email not found"));

                return this.utilityService.GetCamelCaseJsonResult(formResponse);
            }

            var subscriptionsQuery = this.requestDbContext.MailingListSubscriptions.Where(x =>
                x.MailingListId == mailingListId &&
                x.Status == MailingListSubscriptionStatusEnum.Subscribed);

            if (countryId.HasValue)
            {
                subscriptionsQuery = subscriptionsQuery.Where(x => x.User.Country.Id == countryId);
            }

            var to = subscriptionsQuery.Select(x => x.User.Email).ToList();

            try
            {
                this.emailService.ExecuteEmailAction(this.requestDbContext, email.Name, to);

                formResponse.SuccessMessage = "Email successfully sent";
            }
            catch (Exception ex)
            {
                formResponse.AddError(ex.Message);
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult AddContentSection(ContentSectionDto contentSectionDto, bool checkForSimilarContent)
        {
            var defaultCountry = this.countryService.GetDefaultCountry(this.requestDbContext);
            contentSectionDto.ContentSectionLocalizedKits[0].Country = new CountryDto(defaultCountry.Id);

            var validationResult = this.contentSectionDtoValidator.Validate(contentSectionDto);
            var createContentSectionDto = new CreateContentSectionDto();

            if (!validationResult.IsValid)
            {
                createContentSectionDto.FormResponse = this.utilityService.GetFormResponse(validationResult);
                return this.utilityService.GetCamelCaseJsonResult(createContentSectionDto);
            }

            var similarContentSectionDto = checkForSimilarContent ? this.SimilarityMatch(contentSectionDto) : null;

            if (similarContentSectionDto != null)
            {
                createContentSectionDto.SimilarContentSection = similarContentSectionDto;
                return this.utilityService.GetCamelCaseJsonResult(createContentSectionDto);
            }

            var newContentSection = this.adminEmailService.AddContentSection(this.requestDbContext, contentSectionDto);

            createContentSectionDto.SavedContentSection = new ContentSectionDto
            {
                Id = newContentSection.Id,
                Name = newContentSection.Name
            };

            createContentSectionDto.FormResponse = this.utilityService.GetFormResponse(validationResult);

            return this.utilityService.GetCamelCaseJsonResult(createContentSectionDto);
        }

        // -------------------------------------------------------------------------------------
        // PRIVATE METHODS
        // -------------------------------------------------------------------------------------

        private ContentSectionDto SimilarityMatch(ContentSectionDto contentSectionDto)
        {
            var defaultLocalizedContent = contentSectionDto.ContentSectionLocalizedKits.First();

            var allDefaultLocalizedContents = this.requestDbContext.ContentSectionLocalizedKits.Where(x => x.Country.IsDefault).OrderBy(x => x.Country.LanguageCode == Thread.CurrentThread.CurrentCulture.Name).ToList();

            var defaultLocalizedKit = allDefaultLocalizedContents.FirstOrDefault(x => x.Content != null && x.Content.AproximatelyEqualsTo(defaultLocalizedContent.Content, 3));

            var similarContentSection = defaultLocalizedKit?.ContentSection;

            similarContentSection?.ContentSectionLocalizedKits.Clear();
            similarContentSection?.ContentSectionLocalizedKits.Add(defaultLocalizedKit);

            return similarContentSection == null ? null : new ContentSectionDto
            {
                Id = similarContentSection.Id,
                Name = similarContentSection.Name,
                IsDisabled = similarContentSection.IsDisabled,
                ContentSectionLocalizedKits = similarContentSection.ContentSectionLocalizedKits.Select(contentSectionLocalizedKit => new ContentSectionLocalizedKitDto
                {
                    Id = contentSectionLocalizedKit.Id,
                    Country = contentSectionLocalizedKit.Country == null ? null : new CountryDto(contentSectionLocalizedKit.Country.Id),
                    Content = contentSectionLocalizedKit.Content
                }).ToList()
            };
        }
    }
}