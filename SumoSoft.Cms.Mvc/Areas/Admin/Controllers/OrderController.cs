﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.EmailServices.Interfaces;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;
    using System;
    using System.Linq;
    using System.Web.Mvc;

    [AuthorizeAdmin]
    public class OrderController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminOrderService adminOrderService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<OrderDto> orderDtoValidator;
        private readonly IValidator<SendEmailConfirmationModalDto> sendEmailConfirmationModalDtoValidator;
        private readonly IEmailService emailService;

        public OrderController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IAdminOrderService adminOrderService,
            IValidator<OrderDto> orderDtoValidator,
            IEmailService emailService,
            IValidator<SendEmailConfirmationModalDto> sendEmailConfirmationModalDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.adminOrderService = adminOrderService;
            this.orderDtoValidator = orderDtoValidator;
            this.emailService = emailService;
            this.sendEmailConfirmationModalDtoValidator = sendEmailConfirmationModalDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson(int page = 1, string keywordFilter = null, int? orderStatusValueFilter = null, string countryNameFilter = null)
        {
            const int pageSize = 100;

            // -----------------------------------------------------
            // Filters
            // -----------------------------------------------------

            var filteredOrders = this.requestDbContext.Orders
                .Where(x => string.IsNullOrEmpty(keywordFilter) ||
                            x.OrderNumber.ToString().Contains(keywordFilter) ||
                            x.TransactionId.Contains(keywordFilter) ||
                            x.UserEmail.Contains(keywordFilter) ||
                            x.UserFirstName.Contains(keywordFilter) ||
                            x.UserLastName.Contains(keywordFilter) ||
                            x.Comments.Contains(keywordFilter) ||
                            x.Tags.Contains(keywordFilter) ||
                            x.AutoTags.Contains(keywordFilter))
                .Where(x => orderStatusValueFilter == null ||
                            x.Status == (OrderStatusEnum)orderStatusValueFilter.Value)
                .Where(x => string.IsNullOrEmpty(countryNameFilter) ||
                            x.CountryName == countryNameFilter)
                .OrderByDescending(x => x.CreatedDate);

            // -----------------------------------------------------
            // Dto
            // -----------------------------------------------------

            var dto = new OrderListDto
            {
                TotalItems = filteredOrders.Count(),
                PageSize = pageSize,
                Orders = filteredOrders.Skip((page - 1) * pageSize).Take(pageSize).AsEnumerable().Select(order => new OrderDto
                {
                    Id = order.Id,
                    CreatedDate = order.CreatedDate,
                    CountryName = order.CountryName,
                    CountryLanguageCode = order.CountryLanguageCode,
                    CurrencySymbol = order.CurrencySymbol,
                    OrderNumber = order.OrderNumber,
                    UserLastName = order.UserLastName,
                    UserFirstName = order.UserFirstName,
                    UserEmail = order.UserEmail,
                    ShippingAddress = new OrderAddressDto
                    {
                        Id = order.ShippingAddress?.Id ?? Guid.NewGuid(),
                        CountryName = order.ShippingAddress?.CountryName
                    },
                    Status = order.Status,
                    StatusDescriptions = order.Status.GetType().GetDescriptions(),
                    TotalAfterTax = order.TotalAfterTax
                }).ToList(),
                Countries = this.requestDbContext.Countries.OrderBy(x => x.Name).Select(x => new CountryDto
                {
                    Name = x.Name
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult Update(OrderDto dto)
        {
            var validationResult = this.orderDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            this.adminOrderService.Update(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult);
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid orderId)
        {
            var order = this.requestDbContext.Orders.Find(orderId);

            if (order == null)
            {
                throw new NullReferenceException();
            }

            var editOrderDto = new EditOrderDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Order = new OrderDto
                {
                    Id = order.Id,
                    CreatedDate = order.CreatedDate,
                    OrderNumber = order.OrderNumber,
                    Status = order.Status,
                    StatusDescriptions = typeof(OrderStatusEnum).GetDescriptions(), // simple implementation based on the [Description] attribute
                    CurrencySymbol = order.CurrencySymbol,
                    UserId = order.UserId,
                    UserFirstName = order.UserFirstName,
                    UserLastName = order.UserLastName,
                    UserEmail = order.UserEmail,
                    TransactionId = order.TransactionId,
                    OrderShippingBoxes = order.OrderShippingBoxes.Select(shippingBox => new OrderShippingBoxDto
                    {
                        Id = shippingBox.Id,
                        CountWeekends = shippingBox.CountWeekends,
                        Description = shippingBox.Description,
                        InternalDescription = shippingBox.InternalDescription,
                        IsDisabled = shippingBox.IsDisabled,
                        MaxDays = shippingBox.MaxDays,
                        MinDays = shippingBox.MinDays,
                        Name = shippingBox.Name,
                        Order = shippingBox.Order == null ? null : new OrderDto(shippingBox.Order.Id),
                        ShippingPriceAfterTax = shippingBox.ShippingPriceAfterTax,
                        ShippingPriceBeforeTax = shippingBox.ShippingPriceBeforeTax,
                        Status = shippingBox.Status,
                        StatusDescriptions = shippingBox.Status.GetType().GetDescriptions(),
                        Title = shippingBox.Title,
                        TrackingCode = shippingBox.TrackingCode,
                        OrderItems = shippingBox.OrderItems.Select(orderItem => new OrderItemDto
                        {
                            Id = orderItem.Id,
                            MaxEta = orderItem.MaxEta,
                            MinEta = orderItem.MinEta,
                            StockStatusDescriptions = orderItem.StockStatus.GetType().GetDescriptions(),
                            ProductCode = orderItem.ProductCode,
                            ProductId = orderItem.ProductId,
                            ProductName = orderItem.ProductName,
                            ProductStockUnitCode = orderItem.ProductStockUnitCode,
                            ProductTitle = orderItem.ProductTitle,
                            ProductUrl = orderItem.ProductUrl,
                            ProductStockUnitEnablePreorder = orderItem.ProductStockUnitEnablePreorder,
                            ProductStockUnitId = orderItem.ProductStockUnitId,
                            ProductStockUnitReleaseDate = orderItem.ProductStockUnitReleaseDate,
                            ProductStockUnitSalePrice = orderItem.ProductStockUnitSalePrice,
                            ProductStockUnitBasePrice = orderItem.ProductStockUnitBasePrice,
                            ProductStockUnitMembershipSalePrice = orderItem.ProductStockUnitMembershipSalePrice,
                            ProductQuantity = orderItem.ProductQuantity,
                            ProductStockUnitShipsIn = orderItem.ProductStockUnitShipsIn,
                            Status = orderItem.Status,
                            StatusDescriptions = orderItem.Status.GetType().GetDescriptions(),
                            StockStatus = orderItem.StockStatus,
                            SubtotalAfterTax = orderItem.SubtotalAfterTax,
                            SubtotalBeforeTax = orderItem.SubtotalBeforeTax,
                            TotalAfterTax = orderItem.TotalAfterTax,
                            TotalBeforeTax = orderItem.TotalBeforeTax,
                            OrderShippingBox = orderItem.OrderShippingBox == null ? null : new OrderShippingBoxDto(orderItem.OrderShippingBox.Id),
                            OrderItemVariants = orderItem.OrderItemVariants.Select(x => new OrderItemVariantDto
                            {
                                Id = x.Id,
                                VariantName = x.VariantName,
                                VariantUrl = x.VariantUrl,
                                VariantType = x.VariantType,
                                VariantLocalizedTitle = x.VariantLocalizedTitle,
                                VariantLocalizedDescription = x.VariantLocalizedDescription,
                                VariantOptionLocalizedTitle = x.VariantOptionLocalizedTitle,
                                VariantOptionCode = x.VariantOptionCode,
                                VariantOptionLocalizedPriceModifier = x.VariantOptionLocalizedPriceModifier,
                                JsonValue = x.JsonValue,
                                DoubleValue = x.DoubleValue,
                                IntegerValue = x.IntegerValue,
                                BooleanValue = x.BooleanValue,
                                StringValue = x.StringValue
                            }).ToList(),
                            OrderTaxes = orderItem.OrderTaxes.Select(orderItemTax => new OrderItemTaxDto
                            {
                                Id = orderItemTax.Id,
                                Amount = orderItemTax.Amount,
                                Code = orderItemTax.Code,
                                IsDisabled = orderItemTax.IsDisabled,
                                Name = orderItemTax.Name
                            }).ToList()
                        }).ToList()
                    }).ToList(),
                    ShippingAddress = order.ShippingAddress == null ? null : new OrderAddressDto
                    {
                        Id = order.ShippingAddress.Id,
                        AddressFirstName = order.ShippingAddress.AddressFirstName,
                        AddressLastName = order.ShippingAddress.AddressLastName,
                        AddressLine1 = order.ShippingAddress.AddressLine1,
                        AddressLine2 = order.ShippingAddress.AddressLine2,
                        City = order.ShippingAddress.City,
                        CountryName = order.ShippingAddress.CountryName,
                        PhoneNumber = order.ShippingAddress.PhoneNumber,
                        Postcode = order.ShippingAddress.Postcode,
                        StateCountyProvince = order.ShippingAddress.StateCountyProvince
                    },
                    Comments = order.Comments,
                    SentEmails = order.SentEmails.Select(x => new SentEmailDto
                    {
                        Id = x.Id,
                        Email = x.Email == null ? null : new EmailDto
                        {
                            Id = x.Email.Id
                        }
                    }).ToList(),
                    OrderCoupon = order.OrderCoupon == null ? null : new OrderCouponDto
                    {
                        Id = order.OrderCoupon.Id,
                        Code = order.OrderCoupon.Code,
                        Amount = order.OrderCoupon.Amount
                    },
                    OrderCredit = order.OrderCredit == null ? null : new OrderCreditDto
                    {
                        Id = order.OrderCredit.Id,
                        Amount = order.OrderCredit.Amount
                    },
                    TotalBeforeTax = order.TotalBeforeTax,
                    TotalAfterTax = order.TotalAfterTax,
                    TotalToBePaid = order.TotalToBePaid
                },
                AllEmails = this.requestDbContext.Emails.Where(x => !x.IsDisabled).OrderBy(x => x.Name).Select(x => new EmailDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    ViewName = x.ViewName
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(editOrderDto);
        }

        [HttpPost]
        public virtual JsonResult RestoreStock(Guid orderItemId)
        {
            var orderItem = this.requestDbContext.OrderItems.Find(orderItemId);

            if (orderItem == null)
            {
                throw new NullReferenceException();
            }

            var productStockUnit = this.requestDbContext.ProductStockUnits.Find(orderItem.ProductStockUnitId);

            if (productStockUnit == null || orderItem.StockStatus == OrderItemStockStatusEnum.Restored)
            {
                return this.utilityService.GetFormResponseJson(this.Url.Action("Edit", new { orderId = orderItem.OrderShippingBox?.Order.Id }));
            }

            productStockUnit.Stock += 1;

            orderItem.StockStatus = OrderItemStockStatusEnum.Restored;

            this.requestDbContext.SaveChanges();

            return this.utilityService.GetFormResponseJson(this.Url.Action("Edit", new { orderId = orderItem.OrderShippingBox?.Order.Id }));
        }

        [HttpPost]
        public virtual JsonResult SendEmail(SendEmailConfirmationModalDto dto)
        {
            var validationResult = this.sendEmailConfirmationModalDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            try
            {
                var order = this.requestDbContext.Orders.Find(dto.EntityId);

                this.emailService.ExecuteEmailAction(this.requestDbContext, dto.EmailName, dto.SendTo.InList(), order);
            }
            catch (Exception e)
            {
                validationResult.Errors.Add(new ValidationFailure(null, e.Message));

                return this.utilityService.GetFormResponseJson(validationResult);
            }

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { orderId = dto.EntityId }));
        }
    }
}