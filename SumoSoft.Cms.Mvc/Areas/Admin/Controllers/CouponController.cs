﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class CouponController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAdminCouponService adminCouponService;
        private readonly IUtilityService utilityService;
        private readonly ICountryService countryService;
        private readonly IValidator<EditCouponDto> editCouponDtoValidator;

        public CouponController(
            IAdminUtilityService adminUtilityService,
            IAdminCouponService adminCouponService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            ICountryService countryService,
            IValidator<EditCouponDto> editCouponDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.adminCouponService = adminCouponService;
            this.utilityService = utilityService;
            this.countryService = countryService;
            this.editCouponDtoValidator = editCouponDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson(int page = 1, string keywordFilter = null)
        {
            const int pageSize = 100;
            var skipSize = (page - 1) * pageSize;
            // -----------------------------------------------------
            // FILTERS
            // -----------------------------------------------------

            var couponsQueryAble = this.requestDbContext.Coupons
                .Where(x => string.IsNullOrEmpty(keywordFilter)
                            || x.Code.Contains(keywordFilter)
                            || x.Note.Contains(keywordFilter)
                            || x.Country.Name.Contains(keywordFilter))
                .OrderByDescending(x => x.CreatedDate);

            // -----------------------------------------------------
            // DTO
            // -----------------------------------------------------

            var couponListDto = new CouponListDto
            {
                TotalItems = couponsQueryAble.Count(),
                PageSize = pageSize,
                Coupons = couponsQueryAble.Skip(skipSize).Take(pageSize).AsEnumerable().Select(coupon => new CouponDto
                {
                    Id = coupon.Id,
                    Code = coupon.Code,
                    Note = coupon.Note.Shorten(20),
                    Published = coupon.Published,
                    ExpirationDate = coupon.ExpirationDate,
                    ValidityTimes = coupon.ValidityTimes,
                    Amount = coupon.Amount,
                    Percentage = coupon.Percentage,
                    Country = coupon.Country == null ? null : new CountryDto
                    {
                        Id = coupon.Country.Id,
                        Name = coupon.Country.Name
                    }
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(couponListDto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? couponId)
        {
            var isNew = couponId == null;

            var coupon = isNew ? new Coupon
            {
                Amount = 0,
                Percentage = 0,
                Country = this.countryService.GetCurrentCountry(this.requestDbContext),
                ExpirationDate = DateTime.Now.AddMonths(1),
                ValidityTimes = 1,
                Published = true,
                Referee = new User()
            } : this.requestDbContext.Coupons.Find(couponId);

            if (coupon == null)
            {
                throw new NullReferenceException();
            }

            var editCouponDto = new EditCouponDto
            {
                Quantity = 1,
                Coupon = new CouponDto
                {
                    IsNew = isNew,
                    Id = coupon.Id,
                    Code = coupon.Code,
                    Amount = coupon.Amount,
                    Percentage = coupon.Percentage,
                    Country = coupon.Country != null ? new CountryDto
                    {
                        Id = coupon.Country.Id,
                        Name = coupon.Country.Name
                    } : null,
                    Published = coupon.Published,
                    AllowOnSale = coupon.AllowOnSale,
                    ExpirationDate = coupon.ExpirationDate,
                    ValidityTimes = coupon.ValidityTimes,
                    Categories = coupon.Categories.Select(x => new CategoryDto
                    {
                        Id = x.Id,
                        LocalizedTitle = x.GetLocalizedTitle()
                    }).ToList(),
                    Referee = coupon.Referee != null ? new UserDto
                    {
                        Id = coupon.Referee.Id,
                        FirstName = coupon.Referee.FirstName,
                        LastName = coupon.Referee.FirstName,
                        Email = coupon.Referee.Email,
                    } : null,
                    RefereeReward = coupon.RefereeReward,
                    Recipient = coupon.Recipient,
                    Note = coupon.Note
                },
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                AllCategories = this.requestDbContext.Categories.OrderBy(x => x.SortOrder).Select(x => new CategoryDto
                {
                    Id = x.Id,
                    LocalizedTitle = x.CategoryLocalizedKits.FirstOrDefault(y => y.Country.IsDefault).Title
                }).ToList(),
                AllCountries = this.requestDbContext.Countries.OrderBy(x => x.Name).Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                AllUsers = this.requestDbContext.Users
                    .Where(x => 
                        !x.IsDisabled &&
                        !string.IsNullOrEmpty(x.Email) &&
                        !string.IsNullOrEmpty(x.Password)).Select(x => new UserDto
                {
                    Id = x.Id,
                    Email = x.Email
                }).OrderBy(x => x.Email).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(editCouponDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(EditCouponDto editCouponDto)
        {
            var validationResult = this.editCouponDtoValidator.Validate(editCouponDto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var coupon = this.adminCouponService.AddOrUpdate(this.requestDbContext, editCouponDto.Coupon, editCouponDto.Quantity);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { couponId = coupon.Id }));
        }

        [HttpPost]
        public virtual JsonResult DeleteJson(Guid couponId)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.Coupons.Delete(couponId, true);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        public virtual ActionResult CleanDatabase()
        {
            this.HttpContext.Server.ScriptTimeout = 60 * 10;

            var currentDateTime = DateTime.Now;

            var couponsToDelete = this.requestDbContext.Coupons.Where(coupon =>
                    coupon.ExpirationDate < currentDateTime || // Coupons expired
                    coupon.ValidityTimes < 1 || // Coupons used
                    (coupon.Amount < 1 && coupon.Percentage < 1)) // Coupons created wrong
                .ToList();

            if (couponsToDelete.Any())
            {
                foreach (var coupon in couponsToDelete)
                {
                    this.requestDbContext.Coupons.Delete(coupon.Id, false);
                }

                this.requestDbContext.SaveChanges();
            }

            return this.RedirectToAction("Index");
        }
    }
}