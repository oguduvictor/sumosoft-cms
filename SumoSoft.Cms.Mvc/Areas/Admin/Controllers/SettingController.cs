﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.MemoryCacheServices.Interfaces;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class SettingController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IUtilityService utilityService;
        private readonly IAdminSettingService adminSettingService;
        private readonly ICmsMemoryCacheService cmsMemoryCacheService;
        private readonly IValidator<CmsSettingsDto> cmsSettingsDtoValidator;

        public SettingController(
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IAdminSettingService adminSettingService,
            ICmsMemoryCacheService cmsMemoryCacheService,
            IValidator<CmsSettingsDto> cmsSettingsDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.utilityService = utilityService;
            this.adminSettingService = adminSettingService;
            this.cmsMemoryCacheService = cmsMemoryCacheService;
            this.cmsSettingsDtoValidator = cmsSettingsDtoValidator;
        }

        public ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var setting = this.requestDbContext.CmsSettings.First();

            if (!string.IsNullOrEmpty(setting.SmtpPassword))
            {
                setting.SmtpPassword = this.utilityService.Decrypt(setting.SmtpPassword);
            }

            var model = new CmsSettingsDto
            {
                Id = setting.Id,
                IsSeeded = setting.IsSeeded,
                SmtpDisplayName = setting.SmtpDisplayName,
                SmtpEmail = setting.SmtpEmail,
                SmtpHost = setting.SmtpHost,
                SmtpPassword = setting.SmtpPassword,
                SmtpPort = setting.SmtpPort,
                UseAzureStorage = setting.UseAzureStorage,
                ModifiedDate = setting.ModifiedDate,
                CreatedDate = setting.CreatedDate
            };

            return this.utilityService.GetCamelCaseJsonResult(model);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(CmsSettingsDto dto)
        {
            var validationResult = this.cmsSettingsDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            this.adminSettingService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Index"));
        }

        [HttpPost]
        public virtual JsonResult FlushCache(string key)
        {
            var result = this.cmsMemoryCacheService.RemoveAllWhere(x => x.Key.ContainsCaseInsensitive(key));

            var formResponse = new FormResponse
            {
                SuccessMessage = $"{result} MemoryCache entries successfully flushed"
            };

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}