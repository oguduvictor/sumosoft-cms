﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class CountryController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminCountryService adminCountryService;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly ILocalStorageService localStorageService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<CountryDto> countryDtoValidator;

        public CountryController(
            IAdminUtilityService adminUtilityService,
            ILocalStorageService localStorageService,
            IAdminCountryService adminCountryService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<CountryDto> countryDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.utilityService = utilityService;
            this.adminCountryService = adminCountryService;
            this.countryDtoValidator = countryDtoValidator;
            this.adminUtilityService = adminUtilityService;
            this.localStorageService = localStorageService;
        }

        public ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public JsonResult IndexJson()
        {
            var dto = new CountryListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Countries = this.requestDbContext.Countries
                    .AsEnumerable()
                    .OrderByDescending(x => x.IsDefault)
                    .ThenByDescending(x => x.Localize)
                    .ThenBy(x => x.IsDisabled)
                    .ThenBy(x => x.Name).Select(x => new CountryDto
                    {
                        Id = x.Id,
                        IsDefault = x.IsDefault,
                        FlagIcon = x.FlagIcon,
                        Name = x.Name,
                        Localize = x.Localize,
                        Url = x.Url,
                        LanguageCode = x.LanguageCode,
                        IsDisabled = x.IsDisabled,
                        PaymentAccountId = x.PaymentAccountId.IfNullOrEmptyConvertTo("-"),
                        CurrencySymbol = x.GetCurrencySymbol(),
                        Iso4217CurrencySymbol = x.GetIso4217CurrencySymbol()
                    }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? countryId)
        {
            var isNew = countryId == null;

            var country = isNew ? new Country() : this.requestDbContext.Countries.Find(countryId);

            if (country == null)
            {
                throw new NullReferenceException();
            }

            var countryDto = new CountryDto
            {
                Id = country.Id,
                CurrencySymbol = country.GetCurrencySymbol(),
                FlagIcon = country.FlagIcon,
                IsDefault = country.IsDefault,
                IsDisabled = country.IsDisabled,
                IsNew = isNew,
                Iso4217CurrencySymbol = country.GetIso4217CurrencySymbol(),
                LanguageCode = country.LanguageCode,
                Localize = country.Localize,
                Name = country.Name,
                PaymentAccountId = country.PaymentAccountId,
                Url = country.Url,
                Taxes = country.Taxes.Select(tax => new TaxDto
                {
                    Id = tax.Id,
                    Name = tax.Name,
                    IsDisabled = tax.IsDisabled,
                    Amount = tax.Amount,
                    ApplyToProductPrice = tax.ApplyToProductPrice,
                    ApplyToShippingPrice = tax.ApplyToShippingPrice,
                    Code = tax.Code,
                    Percentage = tax.Percentage,
                    CartItem = tax.CartItem == null ? null : new CartItemDto(tax.CartItem.Id),
                    ShippingAddress = tax.ShippingAddress == null ? null : new AddressDto(tax.ShippingAddress.Id),
                    Country = tax.Country == null ? null : new CountryDto(tax.Country.Id)
                }).ToList()
            };

            var allLocalizedCountries = this.requestDbContext.Countries.Where(x => x.Localize).OrderBy(x => x.Name).ToList();
            var allCategories = this.requestDbContext.Categories.OrderBy(x => x.Name).ToList();

            var editCountryDto = new EditCountryDto
            {
                AllLocalizedCountries = allLocalizedCountries.Select(x => new CountryDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsDefault = x.IsDefault
                }).ToList(),
                AllCategories = allCategories.Select(x => new CategoryDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Country = countryDto,
                FlagIcons = this.localStorageService.GetDirectoryDto(SharedConstants.FlagIconsPath).Files.Select(x => x.Name),
                Cultures = CultureInfo.GetCultures(CultureTypes.AllCultures).OrderBy(c => c.Name).Select(x => x.Name)
            };

            return this.utilityService.GetCamelCaseJsonResult(editCountryDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(CountryDto dto)
        {
            var validationResult = this.countryDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var country = this.adminCountryService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { countryId = country.Id }));
        }

        [HttpPost]
        public virtual JsonResult SetDefaultCountry(Guid countryId)
        {
            var formResponse = this.adminCountryService.SetDefaultCountry(this.requestDbContext, countryId);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid countryId)
        {
            var formResponse = new FormResponse();

            var dbCountry = this.requestDbContext.Countries.Find(countryId);

            if (dbCountry == null)
            {
                throw new NullReferenceException("Impossible to delete the Country. Entity not found using the ID: " + countryId);
            }

            if (dbCountry.IsDefault)
            {
                throw new NullReferenceException("It's not possible to delete the Default Country. Switch Default Country first.");
            }

            try
            {
                this.requestDbContext.Countries.Delete(countryId, true);
            }
            catch (Exception)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the entity"));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}