﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class PostsController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminPostsService adminPostsService;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<BlogPostDto> blogPostDtoValidator;

        public PostsController(
            IAdminPostsService adminPostsService,
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<BlogPostDto> blogPostDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminPostsService = adminPostsService;
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.blogPostDtoValidator = blogPostDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var blogPostListDto = new BlogPostListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                BlogPosts = this.requestDbContext.BlogPosts.OrderByDescending(x => x.PublicationDate).AsEnumerable().Select(x => new BlogPostDto
                {
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    IsPublished = x.IsPublished,
                    Author = new UserDto
                    {
                        FirstName = x.Author?.FirstName,
                        LastName = x.Author?.LastName
                    },
                    Title = x.Title
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(blogPostListDto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? postId)
        {
            var isNew = postId == null;

            var blogPost = isNew ? new BlogPost() : this.requestDbContext.BlogPosts.Find(postId).AsNotNull();

            var dto = new EditBlogPostDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                BlogPost = new BlogPostDto
                {
                    IsNew = isNew,
                    Id = blogPost.Id,
                    Title = blogPost.Title,
                    Url = blogPost.Url,
                    FeaturedImage = blogPost.FeaturedImage,
                    FeaturedImageVerticalCrop = blogPost.FeaturedImageVerticalCrop,
                    FeaturedImageHorizontalCrop = blogPost.FeaturedImageHorizontalCrop,
                    Content = blogPost.Content,
                    IsPublished = blogPost.IsPublished,
                    PublicationDate = blogPost.PublicationDate,
                    Author = blogPost.Author == null ? null : new UserDto(blogPost.Author.Id),
                    Excerpt = blogPost.Excerpt,
                    MetaTitle = blogPost.MetaTitle,
                    MetaDescription = blogPost.MetaDescription,
                    BlogCategories = blogPost.BlogCategories.Select(blogCategory => new BlogCategoryDto(blogCategory.Id)).ToList()
                },
                Users = this.requestDbContext.Users.Where(u => u.Role.AccessAdmin).OrderByDescending(x => x.CreatedDate).AsEnumerable().Select(x => new UserDto
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList(),
                AllBlogCategories = this.requestDbContext.BlogCategories.OrderByDescending(x => x.CreatedDate).AsEnumerable().Select(x => new BlogCategoryDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    LocalizedTitle = x.GetLocalizedTitle()
                }).ToList(),
                PostPreviewLink = Path.Combine(this.utilityService.GetAppSetting("AdminPostPreviewLink"),
                    !string.IsNullOrEmpty(blogPost.Url) ? blogPost.Url : "")
            };

            return this.utilityService.GetCamelCaseJsonResult(dto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(BlogPostDto dto)
        {
            var validationResult = this.blogPostDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            dto.Url = dto.Url?.ToLower();

            if (this.requestDbContext.BlogPosts.Any(x => x.Url == dto.Url && x.Id != dto.Id))
            {
                validationResult.Errors.Add(new ValidationFailure(string.Empty, "Already existing Url"));

                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var blogPost = this.adminPostsService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { postId = blogPost.Id }));
        }

        [HttpPost]
        public virtual JsonResult Delete(Guid postId)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.BlogPosts.Delete(postId, true);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}