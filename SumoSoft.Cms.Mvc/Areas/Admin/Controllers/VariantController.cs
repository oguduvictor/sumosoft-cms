﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class VariantController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<VariantOptionDto> variantOptionDtoValidator;
        private readonly IValidator<VariantDto> variantDtoValidator;
        private readonly IAdminVariantService adminVariantService;

        public VariantController(
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<VariantOptionDto> variantOptionDtoValidator,
            IValidator<VariantDto> variantDtoValidator,
            IAdminVariantService adminVariantService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.variantOptionDtoValidator = variantOptionDtoValidator;
            this.variantDtoValidator = variantDtoValidator;
            this.adminVariantService = adminVariantService;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var variants = this.requestDbContext.Variants.AsEnumerable().Select(variant => new VariantDto
            {
                Name = variant.Name,
                SortOrder = variant.SortOrder,
                Id = variant.Id,
                LocalizedTitle = variant.GetLocalizedTitle(),
                LocalizedDescription = variant.GetLocalizedDescription(),
                Type = variant.Type,
                TypeDescriptions = typeof(VariantTypesEnum).GetDescriptions()
            }).OrderBy(x => x.SortOrder).ToList();

            var variantList = new VariantListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                Variants = variants.ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(variantList);

        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? variantId)
        {
            var isNew = variantId == null;

            var variant = isNew ? new Variant() : this.requestDbContext.Variants.Find(variantId);

            if (variant == null)
            {
                throw new NullReferenceException();
            }

            variant.VariantLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(variant.VariantLocalizedKits, new VariantLocalizedKit
            {
                Variant = variant
            });

            var variantDto = new VariantDto
            {
                Id = variant.Id,
                Name = variant.Name,
                CreateProductImageKits = variant.CreateProductImageKits,
                CreateProductStockUnits = variant.CreateProductStockUnits,
                DefaultBooleanValue = variant.DefaultBooleanValue,
                DefaultDoubleValue = variant.DefaultDoubleValue,
                DefaultIntegerValue = variant.DefaultIntegerValue,
                DefaultJsonValue = variant.DefaultJsonValue,
                DefaultStringValue = variant.DefaultStringValue,
                DefaultVariantOptionValue = variant.DefaultVariantOptionValue == null ? null : new VariantOptionDto
                {
                    Id = variant.DefaultVariantOptionValue.Id,
                    Name = variant.DefaultVariantOptionValue.Name,
                    LocalizedTitle = variant.DefaultVariantOptionValue.GetLocalizedTitle(),
                    LocalizedDescription = variant.DefaultVariantOptionValue.GetLocalizedDescription(),
                    LocalizedIsDisabled = variant.DefaultVariantOptionValue.GetLocalizedIsDisabled(),
                    LocalizedPriceModifier = variant.DefaultVariantOptionValue.GetLocalizedPriceModifier(),
                    IsDisabled = variant.DefaultVariantOptionValue.IsDisabled,
                    SortOrder = variant.DefaultVariantOptionValue.SortOrder,
                    Code = variant.DefaultVariantOptionValue.Code
                },
                IsNew = isNew,
                IsDisabled = variant.IsDisabled,
                LocalizedDescription = variant.GetLocalizedDescription(),
                LocalizedTitle = variant.GetLocalizedTitle(),
                SortOrder = variant.SortOrder,
                Type = variant.Type,
                TypeDescriptions = typeof(VariantTypesEnum).GetDescriptions(),
                Url = variant.Url,
                VariantLocalizedKits = variant.VariantLocalizedKits.Select(variantLocalizedKit => new VariantLocalizedKitDto
                {
                    Id = variantLocalizedKit.Id,
                    Country = variantLocalizedKit.Country == null ? null : new CountryDto
                    {
                        Id = variantLocalizedKit.Country.Id,
                        Name = variantLocalizedKit.Country.Name,
                        IsDefault = variantLocalizedKit.Country.IsDefault,
                        IsDisabled = variantLocalizedKit.Country.IsDisabled,
                        Localize = variantLocalizedKit.Country.Localize,
                        Url = variantLocalizedKit.Country.Url
                    },
                    Description = variantLocalizedKit.Description,
                    Title = variantLocalizedKit.Title,
                    IsDisabled = variantLocalizedKit.IsDisabled
                }).ToList(),
                VariantOptions = variant.VariantOptions.Select(variantOptionDto => new VariantOptionDto
                {
                    Id = variantOptionDto.Id,
                    SortOrder = variantOptionDto.SortOrder,
                    Name = variantOptionDto.Name,
                    Url = variantOptionDto.Url,
                    Code = variantOptionDto.Code,
                    Tags = variantOptionDto.Tags,
                    LocalizedTitle = variantOptionDto.GetLocalizedTitle(),
                    LocalizedDescription = variantOptionDto.GetLocalizedDescription(),
                    Variant = variantOptionDto.Variant == null ? null : new VariantDto(variantOptionDto.Variant.Id)
                }).OrderBy(x => x.SortOrder).ThenBy(x => x.Name).ToList()
            };

            var editVariantDto = new EditVariantDto
            {
                Variant = variantDto,
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext)
            };

            return this.utilityService.GetCamelCaseJsonResult(editVariantDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(VariantDto variantDto)
        {
            var validationResult = this.variantDtoValidator.Validate(variantDto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var variant = this.adminVariantService.AddOrUpdate(this.requestDbContext, variantDto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { variantId = variant.Id }));
        }

        [HttpPost]
        public virtual JsonResult UpdateSortOrder(List<VariantDto> variantDtos)
        {
            var formResponse = new FormResponse<List<VariantDto>>();

            this.adminVariantService.UpdateSortOrder(this.requestDbContext, variantDtos);

            formResponse.Target = this.requestDbContext.Variants.OrderBy(x => x.SortOrder).AsEnumerable().Select(variant => new VariantDto
            {
                Id = variant.Id,
                Name = variant.Name,
                SortOrder = variant.SortOrder,
                Type = variant.Type,
                TypeDescriptions = variant.Type.GetType().GetDescriptions(),
                LocalizedTitle = variant.GetLocalizedTitle(),
                LocalizedDescription = variant.GetLocalizedDescription()
            }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            this.requestDbContext.Variants.Delete(id, true);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        // -----------------------------------------------------------------------------------
        // VariantOptions
        // -----------------------------------------------------------------------------------

        public virtual ActionResult EditVariantOption()
        {
            return this.View();
        }

        public virtual JsonResult EditVariantOptionJson(Guid variantId, Guid? variantOptionId)
        {
            var variantOption = this.requestDbContext.VariantOptions.Find(variantOptionId);

            if (variantOption == null)
            {
                var variant = this.requestDbContext.Variants.Find(variantId);

                if (variant == null)
                {
                    throw new NullReferenceException();
                }

                variantOption = new VariantOption
                {
                    Variant = variant
                };
            }

            if (variantOption.Variant?.Id != variantId)
            {
                throw new Exception("The 'variantId' parameter does not match the Id of the VariantOption.Variant");
            }

            variantOption.VariantOptionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(variantOption.VariantOptionLocalizedKits, new VariantOptionLocalizedKit
            {
                VariantOption = variantOption
            });

            var variantOptionDto = new VariantOptionDto
            {
                Id = variantOption.Id,
                Code = variantOption.Code,
                Image = variantOption.Image,
                IsDisabled = variantOption.IsDisabled,
                IsNew = variantOptionId == null,
                Name = variantOption.Name,
                Variant = variantOption.Variant == null ? null : new VariantDto(variantOption.Variant.Id),
                Tags = variantOption.Tags,
                Url = variantOption.Url,
                SortOrder = variantOption.SortOrder,
                LocalizedTitle = variantOption.GetLocalizedTitle(),
                LocalizedDescription = variantOption.GetLocalizedDescription(),
                LocalizedPriceModifier = variantOption.GetLocalizedPriceModifier(),
                LocalizedIsDisabled = variantOption.GetLocalizedIsDisabled(),
                VariantOptionLocalizedKits = variantOption.VariantOptionLocalizedKits.Select(variantOptionLocalizedKit => new VariantOptionLocalizedKitDto
                {
                    Id = variantOptionLocalizedKit.Id,
                    Title = variantOptionLocalizedKit.Title,
                    Description = variantOptionLocalizedKit.Description,
                    PriceModifier = variantOptionLocalizedKit.PriceModifier,
                    IsDisabled = variantOptionLocalizedKit.IsDisabled,
                    Country = variantOptionLocalizedKit.Country == null ? null : new CountryDto
                    {
                        Id = variantOptionLocalizedKit.Country.Id,
                        Name = variantOptionLocalizedKit.Country.Name,
                        IsDefault = variantOptionLocalizedKit.Country.IsDefault,
                        IsDisabled = variantOptionLocalizedKit.Country.IsDisabled,
                        Localize = variantOptionLocalizedKit.Country.Localize,
                        Url = variantOptionLocalizedKit.Country.Url,
                        CurrencySymbol = variantOptionLocalizedKit.Country.GetCurrencySymbol()
                    },
                    ModifiedDate = variantOptionLocalizedKit.ModifiedDate,
                    CreatedDate = variantOptionLocalizedKit.CreatedDate
                }).ToList()
            };

            variantOptionDto.Variant.AsNotNull().Name = variantOption.Variant?.Name;

            var editVariantOptionDto = new EditVariantOptionDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                AllTags = this.requestDbContext.VariantOptions.Where(x => x.Variant.Id == variantId).OrderBy(x => x.SortOrder).AsEnumerable().SelectMany(x => x.GetTagList()).Distinct().ToList(),
                VariantOption = variantOptionDto
            };

            return this.utilityService.GetCamelCaseJsonResult(editVariantOptionDto);
        }

        public virtual JsonResult AddOrUpdateVariantOption(VariantOptionDto variantOptionDto)
        {
            var validationResult = this.variantOptionDtoValidator.Validate(variantOptionDto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var variantOption = this.adminVariantService.AddOrUpdateVariantOption(this.requestDbContext, variantOptionDto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("EditVariantOption", new { variantId = variantOptionDto.Variant?.Id, variantOptionId = variantOption.Id }));
        }

        public virtual JsonResult DeleteVariantOption(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.VariantOptions.Delete(id, true);
            }
            catch (Exception)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the entity"));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}