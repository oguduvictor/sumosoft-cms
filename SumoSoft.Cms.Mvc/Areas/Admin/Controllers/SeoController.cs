﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Attributes;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [AuthorizeAdmin]
    public class SeoController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly IAdminSeoService adminSeoService;
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IUtilityService utilityService;
        private readonly IValidator<SeoSectionDto> seoSectionDtoValidator;

        public SeoController(
            IAdminSeoService adminSeoService,
            IAdminUtilityService adminUtilityService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService,
            IValidator<SeoSectionDto> seoSectionDtoValidator)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.adminSeoService = adminSeoService;
            this.adminUtilityService = adminUtilityService;
            this.utilityService = utilityService;
            this.seoSectionDtoValidator = seoSectionDtoValidator;
        }

        public virtual ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult IndexJson()
        {
            var seoSectionsDto = new SeoSectionListDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                SeoSections = this.requestDbContext.SeoSections.OrderBy(x => x.Page).AsEnumerable().Select(seoSection => new SeoSectionDto
                {
                    Id = seoSection.Id,
                    Page = seoSection.Page,
                    LocalizedTitle = seoSection.GetLocalizedTitle(),
                    LocalizedDescription = seoSection.GetLocalizedDescription(),
                    LocalizedImage = seoSection.GetLocalizedImage()
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(seoSectionsDto);
        }

        public virtual ActionResult Edit()
        {
            return this.View();
        }

        [HttpPost]
        public virtual JsonResult EditJson(Guid? id)
        {
            var isNew = id == null;

            var seoSection = isNew ? new SeoSection() : this.requestDbContext.SeoSections.Find(id);

            if (seoSection == null)
            {
                throw new NullReferenceException();
            }

            seoSection.SeoSectionLocalizedKits = this.adminUtilityService.UpdateLocalizedKits(seoSection.SeoSectionLocalizedKits, new SeoSectionLocalizedKit
            {
                SeoSection = seoSection
            });

            var editSeoSectionDto = new EditSeoSectionDto
            {
                AuthenticatedUser = this.adminUtilityService.GetAuthenticatedUserDto(this.requestDbContext),
                SeoSection = new SeoSectionDto
                {
                    Id = seoSection.Id,
                    IsNew = isNew,
                    Page = seoSection.Page,
                    LocalizedDescription = seoSection.GetLocalizedDescription(),
                    LocalizedTitle = seoSection.GetLocalizedTitle(),
                    LocalizedImage = seoSection.GetLocalizedImage(),
                    SeoSectionLocalizedKits = seoSection.SeoSectionLocalizedKits.Select(x => new SeoSectionLocalizedKitDto
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Description = x.Description,
                        Image = x.Image,
                        IsDisabled = x.IsDisabled,
                        ModifiedDate = x.ModifiedDate,
                        SeoSection = x.SeoSection == null ? null : new SeoSectionDto(x.SeoSection.Id),
                        Country = x.Country == null ? null : new CountryDto
                        {
                            Id = x.Country.Id,
                            Name = x.Country.Name,
                            CurrencySymbol = x.Country.GetCurrencySymbol(),
                            IsDefault = x.Country.IsDefault
                        }
                    }).ToList()
                }
            };

            return this.utilityService.GetCamelCaseJsonResult(editSeoSectionDto);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdate(SeoSectionDto dto)
        {
            var validationResult = this.seoSectionDtoValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var seo = this.adminSeoService.AddOrUpdate(this.requestDbContext, dto);

            return this.utilityService.GetFormResponseJson(validationResult, this.Url.Action("Edit", new { id = seo.Id }));
        }

        public virtual JsonResult Delete(Guid id)
        {
            var formResponse = new FormResponse();

            try
            {
                this.requestDbContext.SeoSections.Delete(id, true);
            }
            catch (Exception)
            {
                formResponse.Errors.Add(new FormError("An error occurred while deleting the entity"));
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}