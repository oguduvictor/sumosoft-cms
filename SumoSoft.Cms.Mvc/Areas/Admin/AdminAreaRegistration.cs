﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin
{
    using System.Web.Mvc;

    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext areaRegistrationContext)
        {
            areaRegistrationContext.Routes.MapMvcAttributeRoutes();

            areaRegistrationContext.MapRoute(
                name: "Admin",
                url: "admin/{controller}/{action}",
                defaults: new { controller = "Notification", action = "Index" },
                namespaces: new[] { "SumoSoft.Cms.Mvc.Areas.Admin.Controllers" }
            );
        }
    }
}