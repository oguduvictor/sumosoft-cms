﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class ContentSectionLocalizedKitDtoValidator : AbstractValidator<ContentSectionLocalizedKitDto>
    {
        public ContentSectionLocalizedKitDtoValidator()
        {
            this.RuleFor(x => x.Id).NotNull();
            this.RuleFor(x => x.Country).NotNull();
            this.RuleFor(x => x.ContentSection).NotNull();
        }
    }
}