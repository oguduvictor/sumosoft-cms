﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;

    public class ShippingBoxLocalizedKitDtoValidator : AbstractValidator<ShippingBoxLocalizedKitDto>
    {
        public ShippingBoxLocalizedKitDtoValidator()
        {
            this.RuleFor(x => x.Id).NotNull();
            this.RuleFor(x => x.Country).NotNull();
            this.RuleFor(x => x.ShippingBox).NotNull();
            this.When(x => x.Country.AsNotNull().IsDefault, () =>
            {
                this.RuleFor(x => x.Title).NotNull();
                this.RuleFor(x => x.Description).NotNull();
                this.RuleFor(x => x.InternalDescription).NotEmpty();
            });
        }
    }
}