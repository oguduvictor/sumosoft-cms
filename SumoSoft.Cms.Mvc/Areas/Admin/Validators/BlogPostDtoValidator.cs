﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class BlogPostDtoValidator : AbstractValidator<BlogPostDto>
    {
        public BlogPostDtoValidator()
        {
            this.RuleFor(x => x.Title).NotEmpty();
            this.RuleFor(x => x.Content).NotEmpty();
            this.RuleFor(x => x.Url).NotEmpty().SetValidator(x => new UrlValidator(x.Url));
        }
    }
}