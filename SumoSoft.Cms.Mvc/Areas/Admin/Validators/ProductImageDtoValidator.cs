﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class ProductImageDtoValidator : AbstractValidator<ProductImageDto>
    {
        public ProductImageDtoValidator()
        {
            this.When(x => !string.IsNullOrEmpty(x.Name), () =>
            {
                this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
            });
            this.RuleFor(x => x.Url).NotEmpty();
        }
    }
}