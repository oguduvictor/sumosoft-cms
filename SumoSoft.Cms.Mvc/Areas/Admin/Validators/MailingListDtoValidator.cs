﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class MailingListDtoValidator : AbstractValidator<MailingListDto>
    {
        public MailingListDtoValidator(IValidator<MailingListLocalizedKitDto> mailingListLocalizedKitDtoValidator)
        {
            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
            this.RuleFor(x => x.MailingListLocalizedKits).SetCollectionValidator(mailingListLocalizedKitDtoValidator);
        }
    }
}