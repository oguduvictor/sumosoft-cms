﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class CmsSettingsDtoValidator : AbstractValidator<CmsSettingsDto>
    {
        public CmsSettingsDtoValidator()
        {
            this.RuleFor(x => x.SmtpHost).NotEmpty();
            this.RuleFor(x => x.SmtpPort).NotEmpty();
            this.RuleFor(x => x.SmtpPassword).NotEmpty();
            this.RuleFor(x => x.SmtpEmail).NotEmpty();
            this.RuleFor(x => x.SmtpEmail).EmailAddress();
        }
    }
}