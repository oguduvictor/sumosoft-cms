﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Collections.Generic;
    using System.Linq;
    using FluentValidation;

    public class UrlValidator : AbstractValidator<string>
    {
        public UrlValidator(string url, IEnumerable<string> conflictingUrls = null)
        {
            this.RuleFor(x => url)
                .Length(1, 100).WithMessage("The length of the Url must be between 1 and 100 chars.")
                .Matches("^[a-z0-9-]*$").WithMessage("The Url can only contain lowercase letters, numbers and dashes.")
                .Must(x => !url.Contains(" ")).WithMessage("The Url cannot contain empty spaces. Please use dashed instead.")
                .Must(x => !conflictingUrls?.Contains(url) ?? true)
                .WithMessage("This Url is already in use.");
        }
    }
}