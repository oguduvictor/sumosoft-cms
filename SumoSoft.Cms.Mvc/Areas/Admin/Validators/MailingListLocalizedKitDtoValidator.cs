﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class MailingListLocalizedKitDtoValidator : AbstractValidator<MailingListLocalizedKitDto>
    {
        public MailingListLocalizedKitDtoValidator()
        {
            this.RuleFor(x => x.Id).NotNull();
            this.RuleFor(x => x.Country).NotNull();
        }
    }
}