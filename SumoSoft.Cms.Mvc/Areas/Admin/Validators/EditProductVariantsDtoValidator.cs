﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class EditProductVariantsDtoValidator : AbstractValidator<EditProductVariantsDto>
    {
        public EditProductVariantsDtoValidator(IValidator<ProductVariantDto> productVariantDtoValidator)
        {
            this.RuleFor(x => x.ProductId).NotNull();
            this.RuleFor(x => x.ProductVariants).SetCollectionValidator(productVariantDtoValidator);
        }
    }
}