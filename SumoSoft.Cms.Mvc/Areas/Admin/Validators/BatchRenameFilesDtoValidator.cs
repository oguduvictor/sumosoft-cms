﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class BatchRenameFilesDtoValidator : AbstractValidator<BatchRenameFilesDto>
    {
        public BatchRenameFilesDtoValidator()
        {
            char[] disAllowedCharacters = { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };
            const string disAllowedCharactersMessage = "cannot contain any of the following characters \\ / : * ? \" < > | ";

            this.RuleFor(x => x.DirectoryLocalPath).NotEmpty();
            this.RuleFor(x => x.LocalPaths).NotNull();
            this.When(x => x.LocalPaths != null, () =>
            {
                this.RuleFor(x => x.LocalPaths).Must(x => x.Count >= 1);
            });
            this.Custom(dto =>
            {
                if (string.IsNullOrEmpty(dto.ReplaceText) && string.IsNullOrEmpty(dto.ReplaceTextTarget) && string.IsNullOrEmpty(dto.PrefixText) && string.IsNullOrEmpty(dto.SuffixText))
                {
                    return new ValidationFailure("ReplaceText", "At least one field must contain a value.");
                }

                return null;
            });
            this.When(x => !string.IsNullOrEmpty(x.ReplaceTextTarget), () =>
            {
                this.RuleFor(x => x.ReplaceText).NotEmpty().WithMessage("'Text To Replace' should not be empty.");
                this.RuleFor(x => x.ReplaceTextTarget).NotEqual(x => x.ReplaceText).WithMessage("'Replace Text With' should not be equal to 'Text To Replace'.");
                this.RuleFor(x => x.ReplaceTextTarget).Must(x => !x.ContainsAny(disAllowedCharacters)).WithMessage($"'Replace Text With' {disAllowedCharactersMessage}");
            });
            this.When(x => !string.IsNullOrEmpty(x.PrefixText), () =>
            {
                this.RuleFor(x => x.PrefixText).Must(x => !x.ContainsAny(disAllowedCharacters)).WithMessage($"'Add Prefix' {disAllowedCharactersMessage}");
            });
            this.When(x => !string.IsNullOrEmpty(x.SuffixText), () =>
            {
                this.RuleFor(x => x.SuffixText).Must(x => !x.ContainsAny(disAllowedCharacters)).WithMessage($"'Add Suffix' {disAllowedCharactersMessage}");
            });
        }
    }
}