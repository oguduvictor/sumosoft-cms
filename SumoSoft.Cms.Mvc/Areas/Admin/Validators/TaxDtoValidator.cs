﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class TaxDtoValidator : AbstractValidator<TaxDto>
    {
        public TaxDtoValidator()
        {
            this.RuleFor(x => x.Country).NotNull();
            this.RuleFor(x => x.Percentage)
                .Must(x => x >= 0 && x <= 100)
                .WithMessage("'Percentage' must be between 0 and 100");
            this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
        }
    }
}