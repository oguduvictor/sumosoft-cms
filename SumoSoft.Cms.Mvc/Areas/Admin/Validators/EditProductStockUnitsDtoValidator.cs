﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class EditProductStockUnitsDtoValidator : AbstractValidator<EditProductStockUnitsDto>
    {
        public EditProductStockUnitsDtoValidator(IValidator<ProductStockUnitDto> productStockUnitDtoValidator)
        {
            this.Custom(editProductStockUnitsDto =>
            {
                var barcodes = editProductStockUnitsDto.ProductStockUnits
                .Where(x => !string.IsNullOrEmpty(x.Code) && !x.IsDeleted.IsTrue())
                .Select(x => x.Code);
                if (barcodes.ContainsDuplicates())
                {
                    return new ValidationFailure("Barcode", "Barcodes must be unique.");
                }
                return null;
            });
            this.RuleFor(x => x.ProductStockUnits).SetCollectionValidator(productStockUnitDtoValidator);
        }
    }
}