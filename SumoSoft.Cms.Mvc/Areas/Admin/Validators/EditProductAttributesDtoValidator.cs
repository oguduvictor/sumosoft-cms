﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class EditProductAttributesDtoValidator : AbstractValidator<EditProductAttributesDto>
    {
        public EditProductAttributesDtoValidator(IValidator<ProductAttributeDto> productAttributeDtoValidator)
        {
            this.RuleFor(x => x.ProductId).NotEmpty();
            this.RuleFor(x => x.ProductAttributes).SetCollectionValidator(productAttributeDtoValidator);
        }
    }
}