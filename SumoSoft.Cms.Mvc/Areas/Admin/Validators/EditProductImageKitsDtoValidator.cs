﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class EditProductImageKitsDtoValidator : AbstractValidator<EditProductImageKitsDto>
    {
        public EditProductImageKitsDtoValidator(IValidator<ProductImageKitDto> productImageKitDtoValidator)
        {
            this.RuleFor(x => x.ProductImageKits).SetCollectionValidator(productImageKitDtoValidator);
        }
    }
}