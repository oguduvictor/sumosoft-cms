﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Collections.Generic;
    using System.Linq;
    using FluentValidation;
    using FluentValidation.Results;

    public class NameValidator : AbstractValidator<string>
    {
        public NameValidator(string name, IEnumerable<string> conflictingNames = null)
        {
            this.CascadeMode = CascadeMode.StopOnFirstFailure;

            this.RuleFor(x => x)
                .Length(1, 100)
                .WithMessage("The length of the Name must be between 1 and 200 chars.")
                .Must(x => !x.Contains(" "))
                .WithMessage("The Name cannot contain empty spaces. Use the underscore character to separate words.")
                .Matches("^[a-zA-Z0-9_]*$")
                .WithMessage("The Name can only contain letters, numbers and underscores.")
                .Must(x => char.IsUpper(x, 0))
                .WithMessage("The Name must start with a capital letter.")
                .Must(x => !conflictingNames?.Contains(name) ?? true)
                .WithMessage("{0}", x => $"The Name '{x}' is already in use.");

            this.Custom(x =>
            {
                var splitStrings = x.Split('_');

                foreach (var str in splitStrings)
                {
                    if (string.IsNullOrEmpty(str))
                    {
                        return new ValidationFailure("Name", "Two consecutive underscore characters are not allowed");
                    }

                    if (char.IsLower(str, 0))
                    {
                        return new ValidationFailure("", "Each underscore characted must be followed by a capital letter");
                    }
                }

                return null;
            });
        }
    }
}