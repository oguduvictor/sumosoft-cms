﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class ProductImageKitDtoValidator : AbstractValidator<ProductImageKitDto>
    {
        public ProductImageKitDtoValidator(IValidator<ProductImageDto> productImageDtoValidator)
        {
            this.RuleFor(x => x.ProductImages).SetCollectionValidator(productImageDtoValidator);
        }
    }
}