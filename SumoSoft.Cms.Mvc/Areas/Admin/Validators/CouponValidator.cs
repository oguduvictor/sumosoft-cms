﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class CouponValidator : AbstractValidator<CouponDto>
    {
        public CouponValidator(
            IRequestDbContextService requestDbContextService)
        {
            var requestDbContext = requestDbContextService.Get();

            this.RuleFor(x => x.Amount)
                .Must(amount => amount > 0).When(x => x.Percentage.ApproximatelyEqualsTo(0))
                .WithMessage("Please set the value of the Coupon using the Amount property or the Percentage property");
            this.RuleFor(x => x.Percentage)
                .Must(percentage => percentage.ApproximatelyEqualsTo(0)).When(x => x.Amount > 0)
                .WithMessage("It's not possible to set both the Amount and Percentage properties");
            this.RuleFor(x => x.ExpirationDate).NotEmpty();
            this.Custom(x =>
            {
                var sameCouponSameCode = requestDbContext.Coupons.Any(y => y.Code == x.Code && y.Id != x.Id);

                return sameCouponSameCode ? new ValidationFailure("Code", $"The Code '{x.Code}' is already in use.") : null;
            });
        }
    }
}