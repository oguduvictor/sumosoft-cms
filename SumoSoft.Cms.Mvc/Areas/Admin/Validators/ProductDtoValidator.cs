﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class ProductDtoValidator : AbstractValidator<ProductDto>
    {
        public ProductDtoValidator(
            IRequestDbContextService requestDbContextService,
            IContentSectionService contentSectionService,
            IValidator<ProductLocalizedKitDto> productLocalizedKitDtoValidator)
        {
            var requestDbContext = requestDbContextService.Get();

            this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
            this.RuleFor(x => x.Url).NotEmpty();
            this.RuleFor(x => x.Url).SetValidator(x =>
            {
                var conflictingUrls = requestDbContext.Products.Where(y => y.Url == x.Url && y.Id != x.Id).Select(z => z.Url).ToList();
                return new UrlValidator(x.Url, conflictingUrls);
            });
            this.RuleFor(x => x.ShippingBoxes).Must(x => x.Count > 0).WithMessage("You must select at least one shipping option");
            this.RuleFor(x => x.ProductLocalizedKits)
                .Must(localizedKits => !localizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates())
                .WithMessage(contentSectionService.ContentSection(requestDbContext,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue, true));
            this.RuleFor(x => x.ProductLocalizedKits).SetCollectionValidator(productLocalizedKitDtoValidator);
        }
    }
}