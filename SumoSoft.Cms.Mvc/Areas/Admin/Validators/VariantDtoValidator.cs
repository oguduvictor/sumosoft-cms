﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class VariantDtoValidator : AbstractValidator<VariantDto>
    {
        public VariantDtoValidator(
            IRequestDbContextService requestDbContextService,
            IContentSectionService contentSectionService,
            IValidator<VariantLocalizedKitDto> variantLocalizedKitDtoValidator)
        {
            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
            this.RuleFor(x => x.Url).NotEmpty();
            this.RuleFor(x => x.Url).SetValidator(x => new UrlValidator(x.Url));
            this.RuleFor(x => x.VariantLocalizedKits)
                .Must(localizedKits => !localizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates())
                .WithMessage(contentSectionService.ContentSection(requestDbContextService.Get(),
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue, true));
            this.RuleFor(x => x.VariantLocalizedKits).SetCollectionValidator(variantLocalizedKitDtoValidator);
            this.When(x => x.Type == VariantTypesEnum.Options && x.VariantOptions.Any(), () =>
            {
                this.RuleFor(x => x.DefaultVariantOptionValue).NotNull();
            });
        }
    }
}