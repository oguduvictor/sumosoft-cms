﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class OrderItemVariantDtoValidator : AbstractValidator<OrderItemVariantDto>
    {
        public OrderItemVariantDtoValidator()
        {
            this.RuleFor(x => x.OrderItem).NotNull();
        }
    }
}