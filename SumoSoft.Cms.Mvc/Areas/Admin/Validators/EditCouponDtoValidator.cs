﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class EditCouponDtoValidator : AbstractValidator<EditCouponDto>
    {
        public EditCouponDtoValidator(IValidator<CouponDto> couponValidator)
        {
            this.RuleFor(x => x.Quantity).NotNull().GreaterThanOrEqualTo(1);
            this.RuleFor(x => x.Coupon).NotNull().SetValidator(couponValidator);
        }
    }
}