﻿#pragma warning disable 1591
namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.Services.Dto;

    public class DirectoryDtoValidator : AbstractValidator<DirectoryDto>
    {
        public DirectoryDtoValidator()
        {
            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).NotEqual(SharedConstants.BackupsFolderName)
                .WithMessage($"'{SharedConstants.BackupsFolderName}' is a reserved word and cannot be used as the name of a directory");
            this.RuleFor(x => x.LocalPath).NotEmpty();
        }
    }
}