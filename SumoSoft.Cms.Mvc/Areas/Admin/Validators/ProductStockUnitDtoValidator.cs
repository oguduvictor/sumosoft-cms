﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;

    public class ProductStockUnitDtoValidator : AbstractValidator<ProductStockUnitDto>
    {
        public ProductStockUnitDtoValidator(IValidator<ProductStockUnitLocalizedKitDto> productStockUnitLocalizedKitDtoValidator)
        {
            this.RuleFor(x => x.DispatchDate).NotEmpty();
            this.When(x => x.IsDisabled.IsNullOrFalse(), () =>
            {
                this.RuleFor(x => x.ProductStockUnitLocalizedKits).SetCollectionValidator(productStockUnitLocalizedKitDtoValidator);
            });
        }
    }
}