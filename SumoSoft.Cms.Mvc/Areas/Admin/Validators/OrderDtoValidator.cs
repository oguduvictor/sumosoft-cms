﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class OrderDtoValidator : AbstractValidator<OrderDto>
    {
        public OrderDtoValidator(IValidator<OrderShippingBoxDto> orderShippingBoxDtoValidator)
        {
            this.RuleFor(x => x.OrderShippingBoxes).SetCollectionValidator(orderShippingBoxDtoValidator);
        }
    }
}