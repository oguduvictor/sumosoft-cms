﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class UserDtoValidator : AbstractValidator<UserDto>
    {
        public UserDtoValidator(
            IUtilityService utilityService,
            IRequestDbContextService requestDbContextService,
            IValidator<AddressDto> addressDtoValidator)
        {
            var requestDbContext = requestDbContextService.Get();

            this.RuleFor(x => x.Role).NotNull();
            this.RuleFor(x => x.Email).NotEmpty();
            this.RuleFor(x => x.Email).EmailAddress();
            this.RuleFor(x => x.Addresses).SetCollectionValidator(addressDtoValidator);
            this.Custom(x =>
            {
                var userSameEmail = requestDbContext.Users.FirstOrDefault(y => y.Email == x.Email);

                if (userSameEmail != null && userSameEmail.Id != x.Id)
                {
                    return new ValidationFailure("Email", "Another User with the same email already exists.");
                }

                var role = requestDbContext.UserRoles.Find(x.Role?.Id);

                if (role == null)
                {
                    return new ValidationFailure("Role", "Invalid User Role.");
                }

                if (role.AccessAdmin && utilityService.CheckPasswordStrength(x.Password) < utilityService.GetAppSetting("AdminPasswordStrength").ToInt())
                {
                    return new ValidationFailure("Password", "The password strength of this User is too weak to make them Administrators.");
                }

                return null;
            });
        }
    }
}
