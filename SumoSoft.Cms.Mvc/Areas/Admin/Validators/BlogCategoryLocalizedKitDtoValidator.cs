﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class BlogCategoryLocalizedKitDtoValidator : AbstractValidator<BlogCategoryLocalizedKitDto>
    {
        public BlogCategoryLocalizedKitDtoValidator()
        {
            this.RuleFor(x => x.Id).NotNull();
            this.RuleFor(x => x.Country).NotNull();
            this.RuleFor(x => x.BlogCategory).NotNull();
            this.When(x => x.Country != null && x.Country.IsDefault, () =>
            {
                this.RuleFor(x => x.Title).NotEmpty();
            });
        }
    }
}