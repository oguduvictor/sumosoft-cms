﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class VariantOptionDtoValidator : AbstractValidator<VariantOptionDto>
    {
        public VariantOptionDtoValidator(
            IRequestDbContextService requestDbContextService,
            IContentSectionService contentSectionService,
            IValidator<VariantOptionLocalizedKitDto> variantOptionLocalizedKitDtoValidator)
        {
            var requestDbContext = requestDbContextService.Get();

            this.RuleFor(x => x.Id).NotEmpty();

            this.RuleFor(x => x.Name).NotEmpty();

            this.RuleFor(x => x.Name).SetValidator(x =>
            {
                var variantOptionsSameVariant = requestDbContext.VariantOptions
                    .Where(variantOption => variantOption.Variant.Id == x.Id
                        || (variantOption.Name == x.Name && variantOption.Variant.Id == x.Variant.Id));

                var conflictingNames = variantOptionsSameVariant.Where(y => y.Name == x.Name && y.Id != x.Id).Select(z => z.Name).ToList();

                return new NameValidator(x.Name, conflictingNames);
            });

            this.RuleFor(x => x.Url).NotEmpty();

            this.RuleFor(x => x.Url).SetValidator(x =>
            {
                var variantOptionsSameVariant = requestDbContext.VariantOptions
                    .Where(variantOption => variantOption.Variant.Id == x.Id
                        || (variantOption.Url == x.Url && variantOption.Variant.Id == x.Variant.Id));

                var conflictingUrls = variantOptionsSameVariant.Where(y => y.Url == x.Url && y.Id != x.Id).Select(z => z.Url).ToList();

                return new UrlValidator(x.Url, conflictingUrls);
            });

            this.RuleFor(x => x.VariantOptionLocalizedKits)
                .Must(localizedKits => !localizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates())
                .WithMessage(contentSectionService.ContentSection(requestDbContext,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue, true));

            this.RuleFor(x => x.VariantOptionLocalizedKits).SetCollectionValidator(variantOptionLocalizedKitDtoValidator);
        }
    }
}