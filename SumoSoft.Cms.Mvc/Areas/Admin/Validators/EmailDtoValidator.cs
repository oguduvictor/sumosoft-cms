﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;

    public class EmailDtoValidator : AbstractValidator<EmailDto>
    {
        public EmailDtoValidator(
            IValidator<EmailLocalizedKitDto> emailLocalizedKitDtoValidator)
        {
            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
            this.RuleFor(x => x.ViewName).NotEmpty();
            this.RuleFor(x => x.ContentSection.AsNotNull().Id)
                .NotEmpty()
                .WithMessage("{0}", x => $"Please select a an Content Section for '{x.Name}'");
            this.RuleFor(x => x.EmailLocalizedKits).SetCollectionValidator(emailLocalizedKitDtoValidator);
        }
    }
}