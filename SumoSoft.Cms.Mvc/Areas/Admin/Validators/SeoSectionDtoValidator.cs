﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class SeoSectionDtoValidator : AbstractValidator<SeoSectionDto>
    {
        public SeoSectionDtoValidator(
            IContentSectionService contentSectionService,
            IRequestDbContextService requestDbContextService,
            IValidator<SeoSectionLocalizedKitDto> seoSectionLocalizedKitDtoValidator)
        {
            this.RuleFor(x => x.Page).NotEmpty();
            this.When(x => x.Page != null, () =>
            {
                this.RuleFor(x => x.Page).Must(x => x.StartsWith("/"));
            });
            this.RuleFor(x => x.SeoSectionLocalizedKits)
                .Must(localizedKits => !localizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates())
                .WithMessage(contentSectionService.ContentSection(requestDbContextService.Get(),
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue, true));
            this.RuleFor(x => x.SeoSectionLocalizedKits).SetCollectionValidator(seoSectionLocalizedKitDtoValidator);
        }
    }
}