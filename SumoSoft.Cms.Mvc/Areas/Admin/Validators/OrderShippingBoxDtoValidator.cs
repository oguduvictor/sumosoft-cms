﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class OrderShippingBoxDtoValidator : AbstractValidator<OrderShippingBoxDto>
    {
        public OrderShippingBoxDtoValidator(IValidator<OrderItemDto> orderItemDtoValidator)
        {
            this.RuleFor(x => x.Order).NotNull();
            this.RuleFor(x => x.OrderItems).SetCollectionValidator(orderItemDtoValidator);
        }
    }
}