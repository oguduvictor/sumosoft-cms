﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Dto;

    public class ContentSectionDtoValidator : AbstractValidator<ContentSectionDto>
    {
        public ContentSectionDtoValidator(
            CmsDbContext dbContext,
            IValidator<ContentSectionLocalizedKitDto> contentSectionLocalizedKitDtoValidator)
        {
            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x =>
            {
                var conflictingNames = dbContext.ContentSections.Where(y => y.Name == x.Name && y.Id != x.Id).Select(z => z.Name).ToList();

                return new NameValidator(x.Name, conflictingNames);
            });
            this.RuleFor(x => x.ContentSectionLocalizedKits).SetCollectionValidator(contentSectionLocalizedKitDtoValidator);
        }
    }
}