﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class SendEmailConfirmationModalDtoValidator : AbstractValidator<SendEmailConfirmationModalDto>
    {
        public SendEmailConfirmationModalDtoValidator()
        {
            RuleFor(x => x.EntityId).NotNull();
            RuleFor(x => x.EmailName).NotNull();
        }
    }
}