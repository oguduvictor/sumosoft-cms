﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;

    public class ProductVariantDtoValidator : AbstractValidator<ProductVariantDto>
    {
        public ProductVariantDtoValidator()
        {
            this.RuleFor(x => x.Id).NotNull();
            this.RuleFor(x => x.Product).NotNull();
            this.RuleFor(x => x.Variant).NotNull();
            this.RuleFor(x => x.Variant.Id).NotEmpty();
            this.When(x => x.Variant?.Type == VariantTypesEnum.Options, () =>
            {
                this.RuleFor(x => x.VariantOptions).Must(x => x.Count > 0).WithMessage("You must select at least one option");
                this.RuleFor(x => x.DefaultVariantOptionValue).NotNull();
            });
            this.When(x => x.Variant?.Type == VariantTypesEnum.Boolean, () =>
            {
                this.RuleFor(x => x.DefaultBooleanValue).NotNull();
            });
            this.When(x => x.Variant?.Type == VariantTypesEnum.Double, () =>
            {
                this.RuleFor(x => x.DefaultDoubleValue).NotNull();
            });
            this.When(x => x.Variant?.Type == VariantTypesEnum.Integer, () =>
            {
                this.RuleFor(x => x.DefaultIntegerValue).NotNull();
            });
        }
    }
}