﻿#pragma warning disable 1591
namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class SeoSectionLocalizedKitDtoValidator : AbstractValidator<SeoSectionLocalizedKitDto>
    {
        public SeoSectionLocalizedKitDtoValidator()
        {
            this.RuleFor(x => x.Id).NotNull();
            this.RuleFor(x => x.Country).NotNull();
            this.RuleFor(x => x.SeoSection).NotNull();
            this.When(x => x.Country != null && x.Country.IsDefault, () =>
            {
                this.RuleFor(x => x.Title).NotEmpty().WithMessage("Title for Default Country must not be empty");
            });
        }
    }
}