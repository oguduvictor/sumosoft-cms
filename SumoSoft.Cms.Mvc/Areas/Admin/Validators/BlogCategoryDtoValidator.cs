﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class BlogCategoryDtoValidator : AbstractValidator<BlogCategoryDto>
    {
        public BlogCategoryDtoValidator(
            IRequestDbContextService requestDbContextService,
            IContentSectionService contentSectionService,
            IValidator<BlogCategoryLocalizedKitDto> blogCategoryLocalizedKitDtoValidator)
        {
            var cmsDbContext = requestDbContextService.Get();

            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x =>
            {
                var conflictingNames = cmsDbContext.BlogCategories.Where(y => y.Name == x.Name && y.Id != x.Id).Select(z => z.Name).ToList();
                return new NameValidator(x.Name, conflictingNames);
            });
            this.RuleFor(x => x.Url).NotEmpty().SetValidator(x => new UrlValidator(x.Url));
            this.RuleFor(x => x.BlogCategoryLocalizedKits)
                .Must(localizedKits => !localizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates())
                .WithMessage(contentSectionService.ContentSection(cmsDbContext,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue, true));
            this.RuleFor(x => x.BlogCategoryLocalizedKits).SetCollectionValidator(blogCategoryLocalizedKitDtoValidator);
        }
    }
}