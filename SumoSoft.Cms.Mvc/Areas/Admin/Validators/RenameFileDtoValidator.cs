﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class RenameFileDtoValidator : AbstractValidator<RenameFileDto>
    {
        public RenameFileDtoValidator()
        {
            char[] disAllowedCharacters = { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };

            this.RuleFor(x => x.LocalPath).NotEmpty();
            this.RuleFor(x => x.NewName).NotEmpty();
            this.When(x => !string.IsNullOrEmpty(x.NewName), () =>
            {
                this.RuleFor(x => x.NewName).Must(x => !x.ContainsAny(disAllowedCharacters)).WithMessage("'New Name' cannot contain any of the following characters \\ / : * ? \" < > | ");
            });
        }
    }
}