﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Extensions;

    public class ProductAttributeDtoValidator : AbstractValidator<ProductAttributeDto>
    {
        public ProductAttributeDtoValidator()
        {
            this.RuleFor(x => x.Id).NotEmpty();
            this.RuleFor(x => x.Product).NotNull();
            this.RuleFor(x => x.Attribute).NotNull();
            this.RuleFor(x => x.Attribute.Id).NotEmpty();
            this.When(x => x.Attribute?.Type == AttributeTypesEnum.Options, () =>
            {
                this.RuleFor(x => x.AttributeOptionValue.AsNotNull().Id)
                    .NotEmpty()
                    .WithMessage("{0}", x => $"Please select a an Attribute Option for '{x.Attribute.LocalizedTitle}'");
            });
            this.When(x => x.Attribute?.Type == AttributeTypesEnum.Boolean, () =>
            {
                this.RuleFor(x => x.BooleanValue).NotNull();
            });
            this.When(x => x.Attribute?.Type == AttributeTypesEnum.String, () =>
            {
                this.RuleFor(x => x.StringValue).NotEmpty();
            });
            this.When(x => x.Attribute?.Type == AttributeTypesEnum.DateTime, () =>
            {
                this.RuleFor(x => x.DateTimeValue).NotNull();
            });
            this.When(x => x.Attribute?.Type == AttributeTypesEnum.Image, () =>
            {
                this.RuleFor(x => x.ImageValue).NotEmpty();
            });
            this.When(x => x.Attribute?.Type == AttributeTypesEnum.Double, () =>
            {
                this.RuleFor(x => x.DoubleValue).InclusiveBetween(0.0, double.MaxValue);
            });
            this.When(x => x.Attribute?.Type == AttributeTypesEnum.ContentSection, () =>
            {
                this.RuleFor(x => x.ContentSectionValue.AsNotNull().Id)
                    .NotEmpty()
                    .WithMessage("{0}", x => $"Please select a an Content Section for '{x.Attribute.LocalizedTitle}'");
            });
        }
    }
}