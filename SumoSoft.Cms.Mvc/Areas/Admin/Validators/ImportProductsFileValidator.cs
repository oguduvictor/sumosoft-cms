﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System;
    using System.Web;
    using FluentValidation;

    public class ImportProductsFileValidator : AbstractValidator<HttpFileCollectionBase>
    {
        public ImportProductsFileValidator()
        {
            this.RuleFor(x => x.Count).Must(x => x != 0).WithMessage("No file was found.");
            this.RuleFor(x => x.Count).Must(x => x == 1).WithMessage("One file only is allowed per import.");
            this.When(x => x.Count == 1, () =>
            {
                this.RuleFor(x => x[0].ContentLength).Must(x => x > 0).WithMessage("File must not be empty.");
                this.RuleFor(x => x[0].FileName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase)).Must(x => x).WithMessage("File could not be imported: File type is not supported.");
            });
        }
    }
}