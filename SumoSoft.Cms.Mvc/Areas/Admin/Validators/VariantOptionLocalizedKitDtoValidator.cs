﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class VariantOptionLocalizedKitDtoValidator : AbstractValidator<VariantOptionLocalizedKitDto>
    {
        public VariantOptionLocalizedKitDtoValidator()
        {
            this.When(x => x.Country != null && x.Country.IsDefault, () =>
            {
                this.RuleFor(x => x.Title).NotEmpty();
            });
        }
    }
}