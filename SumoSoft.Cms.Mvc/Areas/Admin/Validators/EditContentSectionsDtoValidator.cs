﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;

    public class EditContentSectionsDtoValidator : AbstractValidator<EditContentSectionsDto>
    {
        public EditContentSectionsDtoValidator(IValidator<ContentSectionDto> contentSectionDtoValidator)
        {
            this.RuleFor(x => x.ContentSections).SetCollectionValidator(contentSectionDtoValidator);
        }
    }
}