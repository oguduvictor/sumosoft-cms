﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class AddressDtoValidator : AbstractValidator<AddressDto>
    {
        public AddressDtoValidator()
        {
            this.RuleFor(x => x.FirstName)
                .NotEmpty()
                .Length(1, 100);

            this.RuleFor(x => x.LastName)
                .NotEmpty()
                .Length(1, 100);

            this.RuleFor(x => x.AddressLine1)
                .NotEmpty()
                .Length(1, 100);

            this.RuleFor(x => x.City)
                .NotEmpty()
                .Length(1, 100);

            this.RuleFor(x => x.StateCountyProvince)
                .Length(0, 100);

            this.RuleFor(x => x.Postcode)
                .NotEmpty()
                .Length(1, 100);

            this.RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .Length(1, 100);

            this.RuleFor(x => x.Country)
                .NotNull();

            this.When(x => x.Country != null, () =>
            {
                this.RuleFor(x => x.Country.Id).NotEmpty();
            });

        }
    }
}