﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class CountryDtoValidator : AbstractValidator<CountryDto>
    {
        public CountryDtoValidator(
            IRequestDbContextService requestDbContextService,
            IValidator<TaxDto> taxDtoValidator)
        {
            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
            this.RuleFor(x => x.LanguageCode).NotEmpty();
            this.Custom(countryDto =>
            {
                if (countryDto.IsDefault && (countryDto.IsDisabled ?? false))
                {
                    return new ValidationFailure("IsDisabled", "The default Country cannot be disabled");
                }

                return requestDbContextService.Get().Countries.Any(x => x.LanguageCode == countryDto.LanguageCode && x.Id != countryDto.Id)
                    ? new ValidationFailure("LanguageCode", "The LanguageCode must be unique") : null;
            });
            this.RuleFor(x => x.Taxes).SetCollectionValidator(taxDtoValidator);
            this.When(x => x.Localize && !(x.IsDisabled ?? false), () =>
            {
                this.RuleFor(x => x.PaymentAccountId).NotEmpty();
            });
        }
    }
}