﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class ProductStockUnitLocalizedKitDtoValidator : AbstractValidator<ProductStockUnitLocalizedKitDto>
    {
        public ProductStockUnitLocalizedKitDtoValidator()
        {
            this.RuleFor(x => x.ProductStockUnit).NotNull();
            this.RuleFor(x => x.Country).NotNull();
        }
    }
}