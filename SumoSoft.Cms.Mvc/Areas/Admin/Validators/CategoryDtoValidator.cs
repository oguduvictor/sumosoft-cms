﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class CategoryDtoValidator : AbstractValidator<CategoryDto>
    {
        public CategoryDtoValidator(
            IRequestDbContextService requestDbContextService,
            IContentSectionService contentSectionService,
            IValidator<CategoryLocalizedKitDto> categoryLocalizedKitDto)
        {
            var requestDbContext = requestDbContextService.Get();

            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x =>
            {
                var conflictingNames = requestDbContext.Categories.Where(y => y.Name == x.Name && y.Id != x.Id).Select(z => z.Name).ToList();
                return new NameValidator(x.Name, conflictingNames);
            });
            this.RuleFor(x => x.Url).NotEmpty();
            this.RuleFor(x => x.Url).SetValidator(x =>
            {
                var conflictingUrls = requestDbContext.Categories.Where(y => y.Url == x.Url && y.Id != x.Id).Select(z => z.Url).ToList();
                return new UrlValidator(x.Url, conflictingUrls);
            });
            this.RuleFor(x => x.Children).SetCollectionValidator(categoryDto => new CategoryDtoChildValidator(categoryDto));
            this.RuleFor(x => x.CategoryLocalizedKits)
                .Must(localizedKits => !localizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates())
                .WithMessage(contentSectionService.ContentSection(requestDbContext,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue, true));
            this.RuleFor(x => x.CategoryLocalizedKits).SetCollectionValidator(categoryLocalizedKitDto);
        }
    }

    public class CategoryDtoChildValidator : AbstractValidator<CategoryDto>
    {
        public CategoryDtoChildValidator(CategoryDto parentCategoryDto)
        {
            this.RuleFor(x => x.Id)
                .Must(childId => parentCategoryDto.Parent?.Id != childId)
                .WithMessage("It's not possible to simultaneusly set another category as both parent and child.");
        }
    }
}