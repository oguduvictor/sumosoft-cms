﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class ShippingBoxDtoValidator : AbstractValidator<ShippingBoxDto>
    {
        public ShippingBoxDtoValidator(
            IRequestDbContextService requestDbContextService,
            IContentSectionService contentSectionService,
            IValidator<ShippingBoxLocalizedKitDto> shippingBoxLocalizedKitDtoValidator)
        {
            this.RuleFor(x => x.Name).NotEmpty();
            this.RuleFor(x => x.Name).SetValidator(x => new NameValidator(x.Name));
            this.RuleFor(x => x.ShippingBoxLocalizedKits)
                .Must(localizedKits => !localizedKits.Select(xx => xx.Country?.Id).ContainsDuplicates())
                .WithMessage(contentSectionService.ContentSection(requestDbContextService.Get(),
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound,
                    ContentSectionName.Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue, true));
            this.RuleFor(x => x.ShippingBoxLocalizedKits).SetCollectionValidator(shippingBoxLocalizedKitDtoValidator);
        }
    }
}