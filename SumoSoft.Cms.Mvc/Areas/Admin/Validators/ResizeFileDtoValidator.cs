﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using System.Linq;
    using FluentValidation;
    using SumoSoft.Cms.Dto;

    public class ResizeFileDtoValidator : AbstractValidator<ResizeFileDto>
    {
        public ResizeFileDtoValidator()
        {
            this.RuleFor(x => x.LocalPaths).NotNull().Must(x => x.Any());
            this.RuleFor(x => x.Compression).NotNull();
            this.RuleFor(x => x.KeepOriginal).NotNull();
        }
    }
}