﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain.Dto;

    public class EmailLocalizedKitDtoValidator : AbstractValidator<EmailLocalizedKitDto>
    {
        public EmailLocalizedKitDtoValidator()
        {
            this.RuleFor(x => x.Country).NotNull();
            this.When(x => !string.IsNullOrEmpty(x.From), () => this.RuleFor(x => x.From).EmailAddress());
            this.When(x => !string.IsNullOrEmpty(x.ReplyTo), () => this.RuleFor(x => x.ReplyTo).EmailAddress());
        }
    }
}