﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Hubs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.MemoryCacheServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    [HubName("countryHub")]
    public class CountryHub : Hub
    {
        private readonly ICmsMemoryCacheService cmsMemoryCacheService;
        private readonly IUtilityService utilityService;

        public CountryHub(
            ICmsMemoryCacheService cmsMemoryCacheService,
            IUtilityService utilityService)
        {
            this.cmsMemoryCacheService = cmsMemoryCacheService;
            this.utilityService = utilityService;
        }

        /// <summary>
        /// Batch update the SalePrice of a range of ProductStockUnits.
        /// </summary>
        /// <param name="targetCountryId"></param>
        /// <param name="salePercentage"></param>
        /// <param name="roundMethod"></param>
        /// <param name="roundInterval"></param>
        /// <param name="roundAfterTax"></param>
        /// <param name="categoryId">Optional. If set, the price change will be limited to the products from a certain category.</param>
        /// <returns></returns>
        /// <example>If roundMethod = "decimals" and roundInterval = 2: 10,9999 => 10,99.</example>
        /// <example>If roundMethod = "closest" and roundInterval = 5: 10,9999 => 10.</example>
        /// <example>If roundMethod = "up" and roundInterval = 5: 10,9999 => 15.</example>
        /// <example>If roundMethod = "down" and roundInterval = 5: 10,9999 => 10.</example>
        public async Task BatchUpdateProductStockUnitSalePrices(
            Guid targetCountryId,
            double salePercentage,
            string roundMethod,
            int roundInterval,
            bool roundAfterTax,
            Guid? categoryId = null)
        {
            using (var dbContext = new CmsDbContext())
            {
                var targetCountry = dbContext.Countries.Find(targetCountryId);

                if (targetCountry == null)
                {
                    throw new Exception("Target country not found");
                }

                if (!targetCountry.Localize)
                {
                    throw new Exception("The target country must be localized");
                }

                List<ProductStockUnit> productStockUnits;

                if (categoryId == null)
                {
                    productStockUnits = dbContext.ProductStockUnits.ToList();
                }
                else
                {
                    var category = dbContext.Categories.Find(categoryId);

                    if (category == null)
                    {
                        throw new NullReferenceException("Category not found");
                    }

                    switch (category.Type)
                    {
                        case CategoryTypesEnum.Products:
                            productStockUnits = category.Products.SelectMany(p => p.ProductStockUnits).ToList();
                            break;
                        case CategoryTypesEnum.ProductImageKits:
                            productStockUnits = category.ProductImageKits
                                .AsEnumerable()
                                .SelectMany(pik =>
                                {
                                    var targetVariantOptions = pik.VariantOptions.Where(x => x.Variant?.CreateProductStockUnits ?? false);
                                    return pik.Product.AsNotNull().ProductStockUnits.Where(psu => psu.VariantOptions.ContainsAll(targetVariantOptions));
                                })
                                .Distinct()
                                .ToList();
                            break;
                        case CategoryTypesEnum.ProductStockUnits:
                            productStockUnits = category.ProductStockUnits.ToList();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                productStockUnits.ForEach(productStockUnit =>
                {
                    var targetLocalizedKit = productStockUnit.ProductStockUnitLocalizedKits.FirstOrDefault(x => x.Country?.Id == targetCountryId);

                    if (targetLocalizedKit == null)
                    {
                        targetLocalizedKit = new ProductStockUnitLocalizedKit
                        {
                            ProductStockUnit = productStockUnit,
                            Country = targetCountry
                        };

                        dbContext.ProductStockUnitLocalizedKits.Add(targetLocalizedKit);
                    }

                    double salePrice;

                    if (roundAfterTax)
                    {
                        var targetCountryTaxes = targetLocalizedKit.Country?.Taxes;

                        salePrice = targetLocalizedKit.BasePrice - targetLocalizedKit.BasePrice.CalculatePercentage(salePercentage);
                        salePrice = salePrice.AddTaxes(targetCountryTaxes);
                        salePrice = RoundPrice(salePrice, roundMethod, roundInterval);
                        salePrice = salePrice.RemoveTaxes(targetCountryTaxes);
                    }
                    else
                    {
                        salePrice = targetLocalizedKit.BasePrice - targetLocalizedKit.BasePrice.CalculatePercentage(salePercentage);
                        salePrice = RoundPrice(salePrice, roundMethod, roundInterval);
                    }

                    if (!targetLocalizedKit.SalePrice.ApproximatelyEqualsTo(salePrice))
                    {
                        targetLocalizedKit.SalePrice = salePrice;
                    }
                });

                await dbContext.SaveChangesAsync();
            }

            this.Clients.Caller.finishBatchUpdateProductStockUnitSalePrices();
        }

        // todo:
        public async Task BatchUpdateProductStockUnitSalePricesUndo()
        {
            throw new NotImplementedException();
            //Clients.Caller.finishBatchUpdateProductStockUnitSalePricesUndo();
        }

        /// <summary>
        /// Batch void the SalePrice of a range of ProductStockUnits.
        /// </summary>
        /// <param name="targetCountryId"></param>
        /// <param name="categoryId">Optional. If set, the price change will be limited to the products from a certain category.</param>
        /// <returns></returns>
        public async Task BatchVoidProductStockUnitSalePrices(
            Guid targetCountryId,
            Guid? categoryId = null)
        {
            using (var dbContext = new CmsDbContext())
            {
                var targetCountry = dbContext.Countries.Find(targetCountryId);

                if (targetCountry == null)
                {
                    throw new Exception("Target country not found");
                }

                if (!targetCountry.Localize)
                {
                    throw new Exception("The target country must be localized");
                }

                List<ProductStockUnit> productStockUnits;

                if (categoryId == null)
                {
                    productStockUnits = dbContext.ProductStockUnits.ToList();
                }
                else
                {
                    var category = dbContext.Categories.Find(categoryId);

                    if (category == null)
                    {
                        throw new NullReferenceException("Category not found");
                    }

                    switch (category.Type)
                    {
                        case CategoryTypesEnum.Products:
                            productStockUnits = category.Products.SelectMany(p => p.ProductStockUnits).ToList();
                            break;
                        case CategoryTypesEnum.ProductImageKits:
                            productStockUnits = category.ProductImageKits
                                .AsEnumerable()
                                .SelectMany(pik =>
                                {
                                    var targetVariantOptions = pik.VariantOptions.Where(x => x.Variant?.CreateProductStockUnits ?? false);
                                    return pik.Product.AsNotNull().ProductStockUnits.Where(psu => psu.VariantOptions.ContainsAll(targetVariantOptions));
                                })
                                .Distinct()
                                .ToList();
                            break;
                        case CategoryTypesEnum.ProductStockUnits:
                            productStockUnits = category.ProductStockUnits.ToList();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                productStockUnits.ForEach(productStockUnit =>
                {
                    var targetLocalizedKit = productStockUnit.ProductStockUnitLocalizedKits.FirstOrDefault(x => x.Country?.Id == targetCountryId);

                    if (targetLocalizedKit == null)
                    {
                        targetLocalizedKit = new ProductStockUnitLocalizedKit
                        {
                            ProductStockUnit = productStockUnit,
                            Country = targetCountry
                        };

                        dbContext.ProductStockUnitLocalizedKits.Add(targetLocalizedKit);
                    }

                    targetLocalizedKit.SalePrice = 0;
                });

                await dbContext.SaveChangesAsync();
            }

            this.Clients.Caller.finishBatchVoidProductStockUnitSalePrices();
        }

        /// <summary>
        /// Batch update the MembershipSalePrice of a range of ProductStockUnits.
        /// </summary>
        /// <param name="targetCountryId"></param>
        /// <param name="salePercentage"></param>
        /// <param name="roundMethod"></param>
        /// <param name="roundInterval"></param>
        /// <param name="roundAfterTax"></param>
        /// <param name="categoryId">Optional. If set, the price change will be limited to the products from a certain category.</param>
        /// <returns></returns>
        /// <example>If roundMethod = "decimals" and roundInterval = 2: 10,9999 => 10,99.</example>
        /// <example>If roundMethod = "closest" and roundInterval = 5: 10,9999 => 10.</example>
        /// <example>If roundMethod = "up" and roundInterval = 5: 10,9999 => 15.</example>
        /// <example>If roundMethod = "down" and roundInterval = 5: 10,9999 => 10.</example>
        public async Task BatchUpdateProductStockUnitMembershipSalePrices(
            Guid targetCountryId,
            double salePercentage,
            string roundMethod,
            int roundInterval,
            bool roundAfterTax,
            Guid? categoryId = null)
        {
            using (var dbContext = new CmsDbContext())
            {
                var targetCountry = dbContext.Countries.Find(targetCountryId);

                if (targetCountry == null)
                {
                    throw new Exception("Target country not found");
                }

                if (!targetCountry.Localize)
                {
                    throw new Exception("The target country must be localized");
                }

                List<ProductStockUnit> productStockUnits;

                if (categoryId == null)
                {
                    productStockUnits = dbContext.ProductStockUnits.ToList();
                }
                else
                {
                    var category = dbContext.Categories.Find(categoryId);

                    if (category == null)
                    {
                        throw new NullReferenceException("Category not found");
                    }

                    switch (category.Type)
                    {
                        case CategoryTypesEnum.Products:
                            productStockUnits = category.Products.SelectMany(p => p.ProductStockUnits).ToList();
                            break;
                        case CategoryTypesEnum.ProductImageKits:
                            productStockUnits = category.ProductImageKits
                                .AsEnumerable()
                                .SelectMany(pik =>
                                {
                                    var targetVariantOptions = pik.VariantOptions.Where(x => x.Variant?.CreateProductStockUnits ?? false);
                                    return pik.Product.AsNotNull().ProductStockUnits.Where(psu => psu.VariantOptions.ContainsAll(targetVariantOptions));
                                })
                                .Distinct()
                                .ToList();
                            break;
                        case CategoryTypesEnum.ProductStockUnits:
                            productStockUnits = category.ProductStockUnits.ToList();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                productStockUnits.ForEach(productStockUnit =>
                {
                    var targetLocalizedKit = productStockUnit.ProductStockUnitLocalizedKits.FirstOrDefault(x => x.Country?.Id == targetCountryId);

                    if (targetLocalizedKit == null)
                    {
                        targetLocalizedKit = new ProductStockUnitLocalizedKit
                        {
                            ProductStockUnit = productStockUnit,
                            Country = targetCountry
                        };

                        dbContext.ProductStockUnitLocalizedKits.Add(targetLocalizedKit);
                    }

                    double membershipSalePrice;

                    if (roundAfterTax)
                    {
                        var targetCountryTaxes = targetLocalizedKit.Country?.Taxes;

                        membershipSalePrice = targetLocalizedKit.BasePrice - targetLocalizedKit.BasePrice.CalculatePercentage(salePercentage);
                        membershipSalePrice = membershipSalePrice.AddTaxes(targetCountryTaxes);
                        membershipSalePrice = RoundPrice(membershipSalePrice, roundMethod, roundInterval);
                        membershipSalePrice = membershipSalePrice.RemoveTaxes(targetCountryTaxes);
                    }
                    else
                    {
                        membershipSalePrice = targetLocalizedKit.BasePrice - targetLocalizedKit.BasePrice.CalculatePercentage(salePercentage);
                        membershipSalePrice = RoundPrice(membershipSalePrice, roundMethod, roundInterval);
                    }

                    if (!targetLocalizedKit.MembershipSalePrice.ApproximatelyEqualsTo(membershipSalePrice))
                    {
                        targetLocalizedKit.MembershipSalePrice = membershipSalePrice;
                    }
                });

                await dbContext.SaveChangesAsync();
            }

            this.Clients.Caller.finishBatchUpdateProductStockUnitMembershipSalePrices();
        }

        // todo:
        public async Task BatchUpdateProductStockUnitMembershipSalePricesUndo()
        {
            throw new NotImplementedException();
            //Clients.Caller.finishBatchUpdateProductStockUnitMembershipSalePricesUndo();
        }

        /// <summary>
        /// Batch void the MembershipSalePrice of a range of ProductStockUnits.
        /// </summary>
        /// <param name="targetCountryId"></param>
        /// <param name="categoryId">Optional. If set, the price change will be limited to the products from a certain category.</param>
        /// <returns></returns>
        public async Task BatchVoidProductStockUnitMembershipSalePrices(
            Guid targetCountryId,
            Guid? categoryId = null)
        {
            using (var dbContext = new CmsDbContext())
            {
                var targetCountry = dbContext.Countries.Find(targetCountryId);

                if (targetCountry == null)
                {
                    throw new Exception("Target country not found");
                }

                if (!targetCountry.Localize)
                {
                    throw new Exception("The target country must be localized");
                }

                List<ProductStockUnit> productStockUnits;

                if (categoryId == null)
                {
                    productStockUnits = dbContext.ProductStockUnits.ToList();
                }
                else
                {
                    var category = dbContext.Categories.Find(categoryId);

                    if (category == null)
                    {
                        throw new NullReferenceException("Category not found");
                    }

                    switch (category.Type)
                    {
                        case CategoryTypesEnum.Products:
                            productStockUnits = category.Products.SelectMany(p => p.ProductStockUnits).ToList();
                            break;
                        case CategoryTypesEnum.ProductImageKits:
                            productStockUnits = category.ProductImageKits
                                .AsEnumerable()
                                .SelectMany(pik =>
                                {
                                    var targetVariantOptions = pik.VariantOptions.Where(x => x.Variant?.CreateProductStockUnits ?? false);
                                    return pik.Product.AsNotNull().ProductStockUnits.Where(psu => psu.VariantOptions.ContainsAll(targetVariantOptions));
                                })
                                .Distinct()
                                .ToList();
                            break;
                        case CategoryTypesEnum.ProductStockUnits:
                            productStockUnits = category.ProductStockUnits.ToList();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                productStockUnits.ForEach(productStockUnit =>
                {
                    var targetLocalizedKit = productStockUnit.ProductStockUnitLocalizedKits.FirstOrDefault(x => x.Country?.Id == targetCountryId);

                    if (targetLocalizedKit == null)
                    {
                        targetLocalizedKit = new ProductStockUnitLocalizedKit
                        {
                            ProductStockUnit = productStockUnit,
                            Country = targetCountry
                        };

                        dbContext.ProductStockUnitLocalizedKits.Add(targetLocalizedKit);
                    }

                    targetLocalizedKit.MembershipSalePrice = 0;
                });

                await dbContext.SaveChangesAsync();
            }

            this.Clients.Caller.finishBatchVoidProductStockUnitMembershipSalePrices();
        }

        /// <summary>
        /// Update all the ProductStockUnitLocalizedKits BasePrice and SalePrice, VariantOptionLocalizedKits PriceModifiers and ShippingBoxLocalizedKits Price
        /// taking the value of another country and converting the currency with a custom multiplier.
        /// </summary>
        /// <param name="targetCountryId">The Country of the target LocalizedKits.</param>
        /// <param name="sourceCountryId">The Country of the LocalizedKits to be used as source for the conversion.</param>
        /// <param name="conversionFactor">The multiplication factor of the price conversion between source and target countries.</param>
        /// <param name="roundMethod">Possible values: null, decimals, closest, up, down.</param>
        /// <param name="roundInterval">The interval the price should be rounded to.</param>
        /// <param name="roundAfterTax">If this is true, VAT is considered when calculating the final converted prices.</param>
        /// /// <param name="categoryId">Optional. If set, the price change will be limited to the products from a certain category.</param>
        /// <example>If roundMethod = "decimals" and roundInterval = 2: 10,9999 => 10,99.</example>
        /// <example>If roundMethod = "closest" and roundInterval = 5: 10,9999 => 10.</example>
        /// <example>If roundMethod = "up" and roundInterval = 5: 10,9999 => 15.</example>
        /// <example>If roundMethod = "down" and roundInterval = 5: 10,9999 => 10.</example>
        public async Task BatchUpdateAllPrices(
            Guid targetCountryId,
            Guid sourceCountryId,
            double conversionFactor,
            string roundMethod,
            int roundInterval,
            bool roundAfterTax,
            Guid? categoryId = null)
        {
            var formResponse = new FormResponse();

            using (var dbContext = new CmsDbContext())
            {
                var targetCountry = dbContext.Countries.Find(targetCountryId);

                if (targetCountry == null)
                {
                    throw new Exception("Target country not found");
                }

                if (!targetCountry.Localize)
                {
                    throw new Exception("The target country must be localized");
                }

                var sourceCountry = dbContext.Countries.Find(sourceCountryId);

                if (sourceCountry == null)
                {
                    throw new Exception("Source country not found");
                }

                // ---------------------------------------------------------------------------------
                // TARGET ENTITIES
                // ---------------------------------------------------------------------------------

                List<ProductStockUnit> productStockUnits;

                if (categoryId == null)
                {
                    productStockUnits = dbContext.ProductStockUnits.ToList();
                }
                else
                {
                    var category = dbContext.Categories.Find(categoryId);

                    if (category == null)
                    {
                        throw new NullReferenceException("Category not found");
                    }

                    switch (category.Type)
                    {
                        case CategoryTypesEnum.Products:
                            productStockUnits = category.Products.SelectMany(p => p.ProductStockUnits).ToList();
                            break;
                        case CategoryTypesEnum.ProductImageKits:
                            productStockUnits = category.ProductImageKits
                                .AsEnumerable()
                                .SelectMany(pik =>
                                {
                                    var targetVariantOptions = pik.VariantOptions.Where(x => x.Variant?.CreateProductStockUnits ?? false);
                                    return pik.Product.AsNotNull().ProductStockUnits.Where(psu => psu.VariantOptions.ContainsAll(targetVariantOptions));
                                })
                                .Distinct()
                                .ToList();
                            break;
                        case CategoryTypesEnum.ProductStockUnits:
                            productStockUnits = category.ProductStockUnits.ToList();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                var variantOptions = dbContext.VariantOptions.ToList();
                var shippingBoxes = dbContext.ShippingBoxes.ToList();

                var modifiedProductStockUnitsLocalizedKits = new List<ProductStockUnitLocalizedKitDto>();
                var modifiedVariantOptionsLocalizedKits = new List<VariantOptionLocalizedKitDto>();
                var modifiedShippingBoxesLocalizedKits = new List<ShippingBoxLocalizedKitDto>();

                // ---------------------------------------------------------------------------------
                // UPDATE PRODUCTSTOCKUNIT BASEPRICE AND SALEPRICE
                // ---------------------------------------------------------------------------------

                productStockUnits.ForEach(productStockUnit =>
                {
                    var targetLocalizedKit = productStockUnit.ProductStockUnitLocalizedKits.FirstOrDefault(x => x.Country?.Id == targetCountryId);

                    if (targetLocalizedKit == null)
                    {
                        targetLocalizedKit = new ProductStockUnitLocalizedKit
                        {
                            ProductStockUnit = productStockUnit,
                            Country = targetCountry
                        };

                        dbContext.ProductStockUnitLocalizedKits.Add(targetLocalizedKit);
                    }

                    var sourceLocalizedKit = productStockUnit.ProductStockUnitLocalizedKits.FirstOrDefault(x => x.Country?.Id == sourceCountryId);

                    if (sourceLocalizedKit == null)
                    {
                        formResponse.Errors.Add(new FormError($"Some of the prices for the product {productStockUnit.Product.GetLocalizedTitle()} have not been found using {sourceCountry.Name} as source."));

                        return;
                    }

                    modifiedProductStockUnitsLocalizedKits.Add(new ProductStockUnitLocalizedKitDto
                    {
                        Id = targetLocalizedKit.Id,
                        BasePrice = targetLocalizedKit.BasePrice,
                        SalePrice = targetLocalizedKit.SalePrice
                    });

                    var basePrice = sourceLocalizedKit.BasePrice;
                    var salePrice = sourceLocalizedKit.SalePrice;

                    if (roundAfterTax)
                    {
                        var targetCountryTaxes = targetLocalizedKit.Country?.Taxes;

                        basePrice = RoundPrice(basePrice.AddTaxes(targetCountryTaxes) * conversionFactor, roundMethod, roundInterval);
                        salePrice = RoundPrice(salePrice.AddTaxes(targetCountryTaxes) * conversionFactor, roundMethod, roundInterval);

                        basePrice = basePrice.RemoveTaxes(targetCountryTaxes);
                        salePrice = salePrice.RemoveTaxes(targetCountryTaxes);
                    }
                    else
                    {
                        basePrice = RoundPrice(basePrice * conversionFactor, roundMethod, roundInterval);
                        salePrice = RoundPrice(salePrice * conversionFactor, roundMethod, roundInterval);
                    }

                    if (!targetLocalizedKit.BasePrice.ApproximatelyEqualsTo(basePrice))
                    {
                        targetLocalizedKit.BasePrice = basePrice;
                    }

                    if (!targetLocalizedKit.SalePrice.ApproximatelyEqualsTo(salePrice))
                    {
                        targetLocalizedKit.SalePrice = salePrice;
                    }
                });

                // ---------------------------------------------------------------------------------
                // UPDATE VARIANTOPTIONLOCALIZEDKIT PRICE MODIFIER
                // ---------------------------------------------------------------------------------

                variantOptions.ForEach(variantOption =>
                {
                    var targetLocalizedKit = variantOption.VariantOptionLocalizedKits.FirstOrDefault(x => x.Country?.Id == targetCountryId);

                    if (targetLocalizedKit == null)
                    {
                        targetLocalizedKit = new VariantOptionLocalizedKit
                        {
                            VariantOption = variantOption,
                            Country = targetCountry
                        };

                        dbContext.VariantOptionLocalizedKits.Add(targetLocalizedKit);
                    }

                    var sourceLocalizedKit = variantOption.VariantOptionLocalizedKits.FirstOrDefault(x => x.Country?.Id == sourceCountryId);

                    if (sourceLocalizedKit == null)
                    {
                        return;
                    }

                    modifiedVariantOptionsLocalizedKits.Add(new VariantOptionLocalizedKitDto
                    {
                        Id = targetLocalizedKit.Id,
                        PriceModifier = targetLocalizedKit.PriceModifier
                    });

                    var priceModifier = sourceLocalizedKit.PriceModifier;

                    priceModifier = RoundPrice(priceModifier * conversionFactor, roundMethod, roundInterval);

                    if (!targetLocalizedKit.PriceModifier.ApproximatelyEqualsTo(priceModifier))
                    {
                        targetLocalizedKit.PriceModifier = priceModifier;
                    }
                });

                // ---------------------------------------------------------------------------------
                // UPDATE SHIPPINGBOXLOCALIZEDKIT PRICE
                // ---------------------------------------------------------------------------------

                shippingBoxes.ForEach(shippingBox =>
                {
                    var targetLocalizedKit = shippingBox.ShippingBoxLocalizedKits.FirstOrDefault(x => x.Country?.Id == targetCountryId);

                    if (targetLocalizedKit == null)
                    {
                        targetLocalizedKit = new ShippingBoxLocalizedKit
                        {
                            ShippingBox = shippingBox,
                            Country = targetCountry
                        };

                        dbContext.ShippingBoxLocalizedKits.Add(targetLocalizedKit);
                    }

                    var sourceLocalizedKit = shippingBox.ShippingBoxLocalizedKits.FirstOrDefault(x => x.Country?.Id == sourceCountryId);

                    if (sourceLocalizedKit == null)
                    {
                        formResponse.Errors.Add(new FormError($"Some of the prices for the shipping box {shippingBox.GetLocalizedTitle()} have not been found using {sourceCountry.Name} as source."));

                        return;
                    }

                    modifiedShippingBoxesLocalizedKits.Add(new ShippingBoxLocalizedKitDto
                    {
                        Id = targetLocalizedKit.Id,
                        Price = targetLocalizedKit.Price
                    });

                    var price = sourceLocalizedKit.Price;

                    price = RoundPrice(price * conversionFactor, roundMethod, roundInterval);

                    if (!targetLocalizedKit.Price.ApproximatelyEqualsTo(price))
                    {
                        targetLocalizedKit.Price = price;
                    }
                });

                // ---------------------------------------------------------------------------------
                // SET OLD VALUES IN MEMORY CACHE TO ALLOW UNDO
                // ---------------------------------------------------------------------------------

                cmsMemoryCacheService.Set("BatchUpdateProductStockUnitLocalizedKitPricesUndo" + targetCountryId, modifiedProductStockUnitsLocalizedKits, DateTimeOffset.Now.AddHours(1), null);
                cmsMemoryCacheService.Set("BatchUpdateVariantOptionLocalizedKitPricesUndo" + targetCountryId, modifiedVariantOptionsLocalizedKits, DateTimeOffset.Now.AddHours(1), null);
                cmsMemoryCacheService.Set("BatchUpdateShippingBoxLocalizedKitPricesUndo" + targetCountryId, modifiedShippingBoxesLocalizedKits, DateTimeOffset.Now.AddHours(1), null);

                await dbContext.SaveChangesAsync();
            }

            this.Clients.Caller.finishBatchUpdateAllPrices(formResponse.ToCamelCaseJson());
        }

        public async Task BatchUpdateAllPricesUndo(Guid targetCountryId)
        {
            using (var dbContext = new CmsDbContext())
            {
                var cachedProductStockUnitsValue = cmsMemoryCacheService.Get<List<ProductStockUnitLocalizedKitDto>>("BatchUpdateProductStockUnitLocalizedKitPricesUndo" + targetCountryId, null);
                var cachedVariantOptionsValue = cmsMemoryCacheService.Get<List<VariantOptionLocalizedKitDto>>("BatchUpdateVariantOptionLocalizedKitPricesUndo" + targetCountryId, null);
                var cachedShippingBoxesValue = cmsMemoryCacheService.Get<List<ShippingBoxLocalizedKitDto>>("BatchUpdateShippingBoxLocalizedKitPricesUndo" + targetCountryId, null);

                if (cachedProductStockUnitsValue == null)
                {
                    throw new Exception("Impossible to recover the old values. No entry found in the MemoryCache.");
                }

                // ---------------------------------------------------------------------------------
                // UNDO PRODUCTSTOCKUNITLOCALIZEDKIT BASEPRICE AND SALEPRICE
                // ---------------------------------------------------------------------------------

                foreach (var productStockUnitLocalizedKitDto in cachedProductStockUnitsValue)
                {
                    var productStockUnitLocalizedKit = dbContext.ProductStockUnitLocalizedKits.Find(productStockUnitLocalizedKitDto.Id);

                    if (productStockUnitLocalizedKit == null)
                    {
                        continue;
                    }

                    if (!productStockUnitLocalizedKit.BasePrice.ApproximatelyEqualsTo(productStockUnitLocalizedKitDto.BasePrice))
                    {
                        productStockUnitLocalizedKit.BasePrice = productStockUnitLocalizedKitDto.BasePrice;
                    }

                    if (!productStockUnitLocalizedKit.SalePrice.ApproximatelyEqualsTo(productStockUnitLocalizedKitDto.SalePrice))
                    {
                        productStockUnitLocalizedKit.SalePrice = productStockUnitLocalizedKitDto.SalePrice;
                    }
                }

                // ---------------------------------------------------------------------------------
                // UNDO VARIANTOPTIONLOCALIZEDKIT PRICEMODIFIER, IF VALUE WAS FOUND IN CACHE
                // ---------------------------------------------------------------------------------

                if (cachedVariantOptionsValue != null && cachedVariantOptionsValue.Any())
                {
                    foreach (var variantOptionLocalizedKitDto in cachedVariantOptionsValue)
                    {
                        var variantOptionLocalizedKit = dbContext.VariantOptionLocalizedKits.Find(variantOptionLocalizedKitDto.Id);

                        if (variantOptionLocalizedKit == null)
                        {
                            continue;
                        }

                        if (!variantOptionLocalizedKit.PriceModifier.ApproximatelyEqualsTo(variantOptionLocalizedKitDto.PriceModifier))
                        {
                            variantOptionLocalizedKit.PriceModifier = variantOptionLocalizedKitDto.PriceModifier;
                        }
                    }
                }

                // ---------------------------------------------------------------------------------
                // UNDO SHIPPINGBOXLOCALIZEDKIT PRICE, IF VALUE WAS FOUND IN CACHE
                // ---------------------------------------------------------------------------------

                if (cachedShippingBoxesValue != null && cachedShippingBoxesValue.Any())
                {
                    foreach (var shippingBoxLocalizedKitDto in cachedShippingBoxesValue)
                    {
                        var shippingBoxLocalizedKit = dbContext.ShippingBoxLocalizedKits.Find(shippingBoxLocalizedKitDto.Id);

                        if (shippingBoxLocalizedKit == null)
                        {
                            continue;
                        }

                        if (!shippingBoxLocalizedKit.Price.ApproximatelyEqualsTo(shippingBoxLocalizedKitDto.Price))
                        {
                            shippingBoxLocalizedKit.Price = shippingBoxLocalizedKitDto.Price;
                        }
                    }
                }

                await dbContext.SaveChangesAsync();

                this.Clients.Caller.finishBatchUpdateAllPricesUndo();
            }
        }

        // ----------------------------------------------------------------------------------
        // PRIVATE METHODS
        // ----------------------------------------------------------------------------------

        private static double RoundPrice(double price, string roundMethod, int roundInterval)
        {
            switch (roundMethod)
            {
                case null:
                case "":
                    break;
                case "decimals":
                    price = price.Round(roundInterval);
                    break;
                case "closest":
                    price = ((int)price).RoundOff(roundInterval);
                    break;
                case "up":
                    price = ((int)price).RoundUp(roundInterval);
                    break;
                case "down":
                    price = ((int)price).RoundDown(roundInterval);
                    break;
                default:
                    throw new Exception("The 'roundMethod' parameter is not valid");
            }

            return price;
        }
    }
}