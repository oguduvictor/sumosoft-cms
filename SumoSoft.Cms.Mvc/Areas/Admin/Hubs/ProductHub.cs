﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Hubs
{
    using System;
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;

    [HubName("productHub")]
    public class ProductHub : Hub
    {
        public void BeginEditingProductStockUnit(Guid productId)
        {
            var connectionId = this.Context.ConnectionId;

            this.Clients.Others.queryIfEditingProductStockUnit(productId, connectionId);
        }

        public void OnMultipleEditingProductStockUnit(string connectionId)
        {
            this.Clients.Client(connectionId).showMultipleEditingProductStockUnitWarning();
        }

        public void SaveEditingProductStockUnit(Guid productId)
        {
            this.Clients.Others.refreshEditingProductStockUnitData(productId);
        }
    }
}