﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminCategoryService : IAdminCategoryService
    {
        public virtual IEnumerable<Category> GetRootCategories(CmsDbContext dbContext)
        {
            return dbContext.Categories.Where(x => x.Parent == null).OrderBy(x => x.SortOrder).ToList();
        }

        public virtual Category AddOrUpdate(CmsDbContext dbContext, CategoryDto categoryDto)
        {
            var category = dbContext.Categories.Find(categoryDto.Id);

            if (category == null)
            {
                category = new Category
                {
                    Id = categoryDto.Id,
                    Name = categoryDto.Name,
                    Url = categoryDto.Url,
                    SortOrder = categoryDto.SortOrder,
                    Tags = categoryDto.Tags,
                    Type = categoryDto.Type,
                    IsDisabled = categoryDto.IsDisabled ?? false,
                    Parent = dbContext.Categories.Find(categoryDto.Parent?.Id),
                    Children = categoryDto.Children.Select(child => dbContext.Categories.Find(child.Id)).ToList(),
                    Products = categoryDto.Products.Select(p => dbContext.Products.Find(p.Id)).ToList(),
                    ProductsSortOrder = categoryDto.ProductsSortOrder,
                    ProductImageKits = categoryDto.ProductImageKits.Select(pik => dbContext.ProductImageKits.Find(pik.Id)).ToList(),
                    ProductImageKitsSortOrder = categoryDto.ProductImageKitsSortOrder,
                    ProductStockUnits = categoryDto.ProductStockUnits.Select(psu => dbContext.ProductStockUnits.Find(psu.Id)).ToList(),
                    ProductStockUnitsSortOrder = categoryDto.ProductStockUnitsSortOrder,
                    CategoryLocalizedKits = categoryDto.CategoryLocalizedKits.Select(categoryLocalizedKitDto => new CategoryLocalizedKit
                    {
                        Id = categoryLocalizedKitDto.Id,
                        Category = category,
                        Country = dbContext.Countries.Find(categoryLocalizedKitDto.Country?.Id),
                        Title = categoryLocalizedKitDto.Title,
                        Description = categoryLocalizedKitDto.Description,
                        FeaturedTitle = categoryLocalizedKitDto.FeaturedTitle,
                        FeaturedDescription = categoryLocalizedKitDto.FeaturedDescription,
                        FeaturedImage = categoryLocalizedKitDto.FeaturedImage,
                        MetaTitle = categoryLocalizedKitDto.MetaTitle,
                        MetaDescription = categoryLocalizedKitDto.MetaDescription,
                        Image = categoryLocalizedKitDto.Image,
                        IsDisabled = categoryLocalizedKitDto.IsDisabled ?? false
                    }).ToList()
                };

                dbContext.Categories.Add(category);
            }
            else
            {
                category.Name = categoryDto.Name;
                category.Url = categoryDto.Url;
                category.SortOrder = categoryDto.SortOrder;
                category.Tags = categoryDto.Tags;
                category.Type = categoryDto.Type;
                category.IsDisabled = categoryDto.IsDisabled ?? false;
                category.Parent = dbContext.Categories.Find(categoryDto.Parent?.Id);
                category.Children.Clear();
                category.Children = categoryDto.Children.Select(child => dbContext.Categories.Find(child.Id)).ToList();
                category.Products.Clear();
                category.Products = categoryDto.Products.Select(p => dbContext.Products.Find(p.Id)).ToList();
                category.ProductsSortOrder = categoryDto.ProductsSortOrder;
                category.ProductImageKits.Clear();
                category.ProductImageKits = categoryDto.ProductImageKits.Select(pik => dbContext.ProductImageKits.Find(pik.Id)).ToList();
                category.ProductImageKitsSortOrder = categoryDto.ProductImageKitsSortOrder;
                category.ProductStockUnits.Clear();
                category.ProductStockUnits = categoryDto.ProductStockUnits.Select(psu => dbContext.ProductStockUnits.Find(psu.Id)).ToList();
                category.ProductStockUnitsSortOrder = categoryDto.ProductStockUnitsSortOrder;

                foreach (var categoryLocalizedKitDto in categoryDto.CategoryLocalizedKits)
                {
                    var categoryLocalizedKit = dbContext.CategoryLocalizedKits.FirstOrDefault(x => x.Id == categoryLocalizedKitDto.Id);

                    if (categoryLocalizedKit == null)
                    {
                        categoryLocalizedKit = new CategoryLocalizedKit
                        {
                            Id = categoryLocalizedKitDto.Id,
                            Category = category,
                            Country = dbContext.Countries.Find(categoryLocalizedKitDto.Country?.Id),
                            Title = categoryLocalizedKitDto.Title,
                            Description = categoryLocalizedKitDto.Description,
                            FeaturedTitle = categoryLocalizedKitDto.FeaturedTitle,
                            FeaturedDescription = categoryLocalizedKitDto.FeaturedDescription,
                            FeaturedImage = categoryLocalizedKitDto.FeaturedImage,
                            MetaTitle = categoryLocalizedKitDto.MetaTitle,
                            MetaDescription = categoryLocalizedKitDto.MetaDescription,
                            Image = categoryLocalizedKitDto.Image,
                            IsDisabled = categoryLocalizedKitDto.IsDisabled ?? false
                        };

                        category.CategoryLocalizedKits.Add(categoryLocalizedKit);
                    }
                    else
                    {
                        categoryLocalizedKit.Title = categoryLocalizedKitDto.Title;
                        categoryLocalizedKit.Description = categoryLocalizedKitDto.Description;
                        categoryLocalizedKit.FeaturedTitle = categoryLocalizedKitDto.FeaturedTitle;
                        categoryLocalizedKit.FeaturedDescription = categoryLocalizedKitDto.FeaturedDescription;
                        categoryLocalizedKit.FeaturedImage = categoryLocalizedKitDto.FeaturedImage;
                        categoryLocalizedKit.MetaTitle = categoryLocalizedKitDto.MetaTitle;
                        categoryLocalizedKit.MetaDescription = categoryLocalizedKitDto.MetaDescription;
                        categoryLocalizedKit.Image = categoryLocalizedKitDto.Image;
                        categoryLocalizedKit.IsDisabled = categoryLocalizedKitDto.IsDisabled ?? false;
                    }
                }
            }

            dbContext.SaveChanges();

            return category;
        }

        public virtual void UpdateHierarchy(CmsDbContext dbContext, List<CategoryDto> rootCategories)
        {
            var flatCategoryDtoList = FlattenCategoryDtoList(rootCategories);

            foreach (var categoryDto in flatCategoryDtoList)
            {
                var dbCategory = dbContext.Categories.Find(categoryDto.Id);

                if (dbCategory == null)
                {
                    throw new NullReferenceException();
                }

                dbCategory.SortOrder = flatCategoryDtoList.IndexOf(categoryDto);
                dbCategory.Children.Clear();
                dbCategory.Children = categoryDto.Children.Select(x => dbContext.Categories.Find(x.Id)).ToList();
            }

            dbContext.SaveChanges();
        }

        private static List<CategoryDto> FlattenCategoryDtoList(List<CategoryDto> rootCategories)
        {
            var result = rootCategories.ToList();

            foreach (var category in rootCategories)
            {
                result.AddRange(FlattenCategoryDtoList(category.Children));
            }

            return result;
        }
    }
}
