#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminSeoService : IAdminSeoService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminSeoService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual SeoSection AddOrUpdate(CmsDbContext dbContext, SeoSectionDto seoSectionDto)
        {
            var seoSection = dbContext.SeoSections.Find(seoSectionDto.Id);

            if (seoSection == null)
            {
                seoSection = new SeoSection
                {
                    Id = seoSectionDto.Id,
                    Page = seoSectionDto.Page,
                    IsDisabled = seoSectionDto.IsDisabled ?? false,
                    SeoSectionLocalizedKits = seoSectionDto.SeoSectionLocalizedKits.Select(seoSectionLocalizedKitDto => new SeoSectionLocalizedKit
                    {
                        Id = seoSectionLocalizedKitDto.Id,
                        Country = dbContext.Countries.Find(seoSectionLocalizedKitDto.Country?.Id),
                        Title = seoSectionLocalizedKitDto.Title,
                        Description = seoSectionLocalizedKitDto.Description,
                        Image = seoSectionLocalizedKitDto.Image,
                        IsDisabled = seoSectionLocalizedKitDto.IsDisabled ?? false
                    }).ToList()
                };

                dbContext.SeoSections.Add(seoSection);
            }
            else
            {
                seoSection.Page = seoSectionDto.Page;
                seoSection.IsDisabled = seoSectionDto.IsDisabled ?? false;

                foreach (var seoSectionLocalizedKitDto in seoSectionDto.SeoSectionLocalizedKits)
                {
                    var seoSectionLocalizedKit = seoSection.SeoSectionLocalizedKits.FirstOrDefault(x => x.Id == seoSectionLocalizedKitDto.Id);

                    if (seoSectionLocalizedKit == null)
                    {
                        seoSectionLocalizedKit = new SeoSectionLocalizedKit
                        {
                            Id = seoSectionLocalizedKitDto.Id,
                            Title = seoSectionLocalizedKitDto.Title,
                            Description = seoSectionLocalizedKitDto.Description,
                            Image = seoSectionLocalizedKitDto.Image,
                            IsDisabled = seoSectionLocalizedKitDto.IsDisabled ?? false,
                            SeoSection = seoSection,
                            Country = dbContext.Countries.Find(seoSectionLocalizedKitDto.Country?.Id)
                        };

                        seoSection.SeoSectionLocalizedKits.Add(seoSectionLocalizedKit);
                    }
                    else
                    {
                        seoSectionLocalizedKit.Title = seoSectionLocalizedKitDto.Title;
                        seoSectionLocalizedKit.Description = seoSectionLocalizedKitDto.Description;
                        seoSectionLocalizedKit.Image = seoSectionLocalizedKitDto.Image;
                        seoSectionLocalizedKit.IsDisabled = seoSectionLocalizedKitDto.IsDisabled ?? false;
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(seoSection.SeoSectionLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.SeoSectionLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return seoSection;
        }

    }
}
