#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminBlogCategoryService : IAdminBlogCategoryService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminBlogCategoryService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual BlogCategory AddOrUpdate(CmsDbContext dbContext, BlogCategoryDto blogCategoryDto)
        {
            var blogCategory = dbContext.BlogCategories.Find(blogCategoryDto.Id);

            if (blogCategory == null)
            {
                blogCategory = new BlogCategory
                {
                    Id = blogCategoryDto.Id,
                    Name = blogCategoryDto.Name,
                    Url = blogCategoryDto.Url,
                    IsDisabled = blogCategoryDto.IsDisabled ?? false,
                    BlogCategoryLocalizedKits = blogCategoryDto.BlogCategoryLocalizedKits.Select(blogCategoryLocalizedKitDto => new BlogCategoryLocalizedKit
                    {
                        Id = blogCategoryLocalizedKitDto.Id,
                        Country = dbContext.Countries.Find(blogCategoryLocalizedKitDto.Country?.Id),
                        Title = blogCategoryLocalizedKitDto.Title,
                        Description = blogCategoryLocalizedKitDto.Description,
                        IsDisabled = blogCategoryLocalizedKitDto.IsDisabled ?? false
                    }).ToList()
                };

                dbContext.BlogCategories.Add(blogCategory);
            }
            else
            {
                blogCategory.Name = blogCategoryDto.Name;
                blogCategory.Url = blogCategoryDto.Url;
                blogCategory.IsDisabled = blogCategoryDto.IsDisabled ?? false;

                foreach (var blogCategoryLocalizedKitDto in blogCategoryDto.BlogCategoryLocalizedKits)
                {
                    var blogCategoryLocalizedKit = blogCategory.BlogCategoryLocalizedKits.FirstOrDefault(x => x.Id == blogCategoryLocalizedKitDto.Id);

                    if (blogCategoryLocalizedKit == null)
                    {
                        blogCategoryLocalizedKit = new BlogCategoryLocalizedKit
                        {
                            Id = blogCategoryLocalizedKitDto.Id,
                            Country = dbContext.Countries.Find(blogCategoryLocalizedKitDto.Country?.Id),
                            Title = blogCategoryLocalizedKitDto.Title,
                            Description = blogCategoryLocalizedKitDto.Description,
                            IsDisabled = blogCategoryLocalizedKitDto.IsDisabled ?? false,
                            BlogCategory = blogCategory
                        };

                        blogCategory.BlogCategoryLocalizedKits.Add(blogCategoryLocalizedKit);
                    }
                    else
                    {
                        blogCategoryLocalizedKit.Title = blogCategoryLocalizedKitDto.Title;
                        blogCategoryLocalizedKit.Description = blogCategoryLocalizedKitDto.Description;
                        blogCategoryLocalizedKit.IsDisabled = blogCategoryLocalizedKit.IsDisabled;
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(blogCategory.BlogCategoryLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.BlogCategoryLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return blogCategory;
        }
    }
}
