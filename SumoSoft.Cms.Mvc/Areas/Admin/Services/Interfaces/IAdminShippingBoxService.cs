#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminShippingBoxService
    {
        ShippingBox AddOrUpdate(CmsDbContext dbContext, ShippingBoxDto shippingBoxDto);

        ShippingBox Duplicate(CmsDbContext dbContext, ShippingBox shippingBox);

        void UpdateSortOrder(CmsDbContext dbContext, List<ShippingBoxDto> shippingBoxs);
    }
}
