﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Interfaces;

    public interface IAdminUtilityService
    {
        List<T> UpdateLocalizedKits<T>(List<T> localizedKits, T localizedKit) where T : BaseEntity, ILocalizedKit, new();

        List<T> GetDuplicatedLocalizedKitsToDelete<T>(List<T> localizedKits) where T : BaseEntity, ILocalizedKit;

        string CreateBackupFileName(string directoryLocalPath);

        UserDto GetAuthenticatedUserDto(CmsDbContext dbContext);

        List<TBaseEntity> GetEntitiesToDelete<TBaseEntity, TBaseEntityDto>(IEnumerable<TBaseEntity> originalCollection, IList<TBaseEntityDto> newCollection) where TBaseEntity : BaseEntity where TBaseEntityDto : BaseEntityDto;
    }
}