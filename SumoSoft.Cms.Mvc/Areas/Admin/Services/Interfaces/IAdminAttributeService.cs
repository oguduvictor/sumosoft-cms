#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminAttributeService
    {
        Attribute AddOrUpdate(CmsDbContext dbContext, AttributeDto attributeDto);

        void UpdateSortOrder(CmsDbContext dbContext, List<AttributeDto> attributeDtos);
    }
}
