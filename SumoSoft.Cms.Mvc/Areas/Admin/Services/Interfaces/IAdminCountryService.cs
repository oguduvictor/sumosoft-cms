#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Dto;

    public interface IAdminCountryService
    {
        Country AddOrUpdate(CmsDbContext dbContext, CountryDto countryDto);

        FormResponse SetDefaultCountry(CmsDbContext dbContext, Guid countryId);
    }
}
