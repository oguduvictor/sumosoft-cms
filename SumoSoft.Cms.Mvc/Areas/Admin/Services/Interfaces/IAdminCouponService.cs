#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminCouponService
    {
        Coupon AddOrUpdate(CmsDbContext dbContext, CouponDto couponDto, int quantity = 1);
    }
}
