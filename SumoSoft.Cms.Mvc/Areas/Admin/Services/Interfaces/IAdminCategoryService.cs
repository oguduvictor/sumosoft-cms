﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminCategoryService
    {
        IEnumerable<Category> GetRootCategories(CmsDbContext dbContext);

        Category AddOrUpdate(CmsDbContext dbContext, CategoryDto categoryDto);

        void UpdateHierarchy(CmsDbContext dbContext, List<CategoryDto> rootCategories);
    }
}