﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminProductService
    {
        Product AddOrUpdate(CmsDbContext dbContext, ProductDto productDto);

        void AddOrUpdateProductVariants(CmsDbContext dbContext, Guid productId, List<ProductVariantDto> productVariantDtos);

        void AddOrUpdateProductAttributes(CmsDbContext dbContext, Guid productId, List<ProductAttributeDto> productAttributeDtos);

        void AddOrUpdateProductImageKits(CmsDbContext dbContext, Guid productId, List<ProductImageKitDto> productImageKitDtos);

        Product Duplicate(CmsDbContext dbContext, Product product);

        void AddOrUpdateProductStockUnits(CmsDbContext dbContext, Guid productId, List<ProductStockUnitDto> productStockUnitDtos);

        ContentSection AddContentSection(CmsDbContext dbContext, ContentSectionDto contentSectionDto);
    }
}