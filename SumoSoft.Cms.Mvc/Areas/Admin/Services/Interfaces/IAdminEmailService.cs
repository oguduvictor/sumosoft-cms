namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminEmailService
    {
        Email AddOrUpdate(CmsDbContext dbContext, EmailDto emailDto);

        ContentSection AddContentSection(CmsDbContext dbContext, ContentSectionDto contentSectionDto);
    }
}
