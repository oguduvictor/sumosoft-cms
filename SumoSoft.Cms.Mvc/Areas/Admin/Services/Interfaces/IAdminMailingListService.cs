﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminMailingListService
    {
        MailingList AddOrUpdateMailingList(CmsDbContext dbContext, MailingListDto mailingListDto);

        void UpdateSortOrder(CmsDbContext dbContext, List<MailingListDto> mailingListDtos);

        MailingList Duplicate(CmsDbContext dbContext, MailingList mailingList);
    }
}
