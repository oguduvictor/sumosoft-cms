﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminOrderService
    {
        Order Update(CmsDbContext dbContext, OrderDto orderDto);
    }
}