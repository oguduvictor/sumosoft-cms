﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminUserService
    {
        User AddOrUpdate(CmsDbContext dbContext, UserDto userDto);

        UserRole AddOrUpdateUserRole(CmsDbContext dbContext, UserRoleDto userRoleDto);

        UserCredit AddOrUpdateUserCredit(CmsDbContext dbContext, UserCreditDto userCreditDto);

        void AddOrUpdateUserCredits(CmsDbContext dbContext, List<UserCreditDto> userCreditDtos);

    }
}