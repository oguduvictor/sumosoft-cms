﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminVariantService
    {
        Variant AddOrUpdate(CmsDbContext dbContext, VariantDto variantDto);

        VariantOption AddOrUpdateVariantOption(CmsDbContext dbContext, VariantOptionDto variantOptionDto);

        void UpdateSortOrder(CmsDbContext dbContext, List<VariantDto> variantDtos);
    }
}