#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces
{
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;

    public interface IAdminSettingService
    {
        CmsSettings AddOrUpdate(CmsDbContext dbContext, CmsSettingsDto cmsSettingsDto);
    }
}
