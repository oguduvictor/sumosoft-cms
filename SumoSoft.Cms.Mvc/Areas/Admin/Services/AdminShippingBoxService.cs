#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminShippingBoxService : IAdminShippingBoxService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminShippingBoxService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual ShippingBox AddOrUpdate(CmsDbContext dbContext, ShippingBoxDto shippingBoxDto)
        {
            var shippingBox = dbContext.ShippingBoxes.Find(shippingBoxDto.Id);

            if (shippingBox == null)
            {
                shippingBox = new ShippingBox
                {
                    Id = shippingBoxDto.Id,
                    Name = shippingBoxDto.Name,
                    RequiresShippingAddress = shippingBoxDto.RequiresShippingAddress,
                    IsDisabled = shippingBoxDto.IsDisabled ?? false,
                    SortOrder = shippingBoxDto.SortOrder,
                    Countries = shippingBoxDto.Countries.Select(c => dbContext.Countries.Find(c.Id)).ToList(),
                    Products = shippingBoxDto.Products.Select(p => dbContext.Products.Find(p.Id)).ToList(),
                    ShippingBoxLocalizedKits = shippingBoxDto.ShippingBoxLocalizedKits.Select(shippingBoxLocalizedKitDto => new ShippingBoxLocalizedKit
                    {
                        Id = shippingBoxLocalizedKitDto.Id,
                        Country = dbContext.Countries.Find(shippingBoxLocalizedKitDto.Country?.Id),
                        Title = shippingBoxLocalizedKitDto.Title,
                        Description = shippingBoxLocalizedKitDto.Description,
                        InternalDescription = shippingBoxLocalizedKitDto.InternalDescription,
                        Carrier = shippingBoxLocalizedKitDto.Carrier,
                        MinDays = shippingBoxLocalizedKitDto.MinDays,
                        MaxDays = shippingBoxLocalizedKitDto.MaxDays,
                        CountWeekends = shippingBoxLocalizedKitDto.CountWeekends,
                        Price = shippingBoxLocalizedKitDto.Price,
                        FreeShippingMinimumPrice = shippingBoxLocalizedKitDto.FreeShippingMinimumPrice,
                        IsDisabled = shippingBoxLocalizedKitDto.IsDisabled ?? false
                    }).ToList()
                };

                dbContext.ShippingBoxes.Add(shippingBox);
            }
            else
            {
                shippingBox.Name = shippingBoxDto.Name;
                shippingBox.RequiresShippingAddress = shippingBoxDto.RequiresShippingAddress;
                shippingBox.IsDisabled = shippingBoxDto.IsDisabled ?? false;
                shippingBox.SortOrder = shippingBoxDto.SortOrder;
                shippingBox.Countries.Clear();
                shippingBox.Countries = shippingBoxDto.Countries.Select(c => dbContext.Countries.Find(c.Id)).ToList();
                shippingBox.Products.Clear();
                shippingBox.Products = shippingBoxDto.Products.Select(p => dbContext.Products.Find(p.Id)).ToList();

                foreach (var shippingBoxLocalizedKitDto in shippingBoxDto.ShippingBoxLocalizedKits)
                {
                    var shippingBoxLocalizedKit = shippingBox.ShippingBoxLocalizedKits.FirstOrDefault(x => x.Id == shippingBoxLocalizedKitDto.Id);

                    if (shippingBoxLocalizedKit == null)
                    {
                        shippingBoxLocalizedKit = new ShippingBoxLocalizedKit
                        {
                            Id = shippingBoxLocalizedKitDto.Id,
                            Title = shippingBoxLocalizedKitDto.Title,
                            Description = shippingBoxLocalizedKitDto.Description,
                            InternalDescription = shippingBoxLocalizedKitDto.InternalDescription,
                            Carrier = shippingBoxLocalizedKitDto.Carrier,
                            MinDays = shippingBoxLocalizedKitDto.MinDays,
                            MaxDays = shippingBoxLocalizedKitDto.MaxDays,
                            CountWeekends = shippingBoxLocalizedKitDto.CountWeekends,
                            Price = shippingBoxLocalizedKitDto.Price,
                            FreeShippingMinimumPrice = shippingBoxLocalizedKitDto.FreeShippingMinimumPrice,
                            IsDisabled = shippingBoxLocalizedKitDto.IsDisabled ?? false,
                            Country = dbContext.Countries.Find(shippingBoxLocalizedKitDto.Country?.Id),
                            ShippingBox = shippingBox
                        };

                        shippingBox.ShippingBoxLocalizedKits.Add(shippingBoxLocalizedKit);
                    }
                    else
                    {
                        shippingBoxLocalizedKit.Title = shippingBoxLocalizedKitDto.Title;
                        shippingBoxLocalizedKit.Description = shippingBoxLocalizedKitDto.Description;
                        shippingBoxLocalizedKit.InternalDescription = shippingBoxLocalizedKitDto.InternalDescription;
                        shippingBoxLocalizedKit.Carrier = shippingBoxLocalizedKitDto.Carrier;
                        shippingBoxLocalizedKit.MinDays = shippingBoxLocalizedKitDto.MinDays;
                        shippingBoxLocalizedKit.MaxDays = shippingBoxLocalizedKitDto.MaxDays;
                        shippingBoxLocalizedKit.CountWeekends = shippingBoxLocalizedKitDto.CountWeekends;
                        shippingBoxLocalizedKit.Price = shippingBoxLocalizedKitDto.Price;
                        shippingBoxLocalizedKit.FreeShippingMinimumPrice = shippingBoxLocalizedKitDto.FreeShippingMinimumPrice;
                        shippingBoxLocalizedKit.IsDisabled = shippingBoxLocalizedKitDto.IsDisabled ?? false;
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(shippingBox.ShippingBoxLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.ShippingBoxLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return shippingBox;
        }

        public ShippingBox Duplicate(CmsDbContext dbContext, ShippingBox shippingBox)
        {
            var duplicate = new ShippingBox
            {
                Name = shippingBox.Name + "_Copy_Disabled",
                Countries = shippingBox.Countries,
                Products = shippingBox.Products,
                IsDisabled = true,
                RequiresShippingAddress = shippingBox.RequiresShippingAddress,
                SortOrder = shippingBox.SortOrder + 1,
                ShippingBoxLocalizedKits = shippingBox.ShippingBoxLocalizedKits.Select(lk => new ShippingBoxLocalizedKit
                {
                    Country = lk.Country,
                    Title = lk.Title,
                    Description = lk.Description,
                    InternalDescription = lk.InternalDescription,
                    Carrier = lk.Carrier,
                    IsDisabled = lk.IsDisabled,
                    CountWeekends = lk.CountWeekends,
                    Price = lk.Price,
                    FreeShippingMinimumPrice = lk.FreeShippingMinimumPrice,
                    MinDays = lk.MinDays,
                    MaxDays = lk.MaxDays
                }).ToList()
            };

            dbContext.ShippingBoxes.Add(duplicate);

            dbContext.SaveChanges();

            return duplicate;
        }

        public virtual void UpdateSortOrder(CmsDbContext dbContext, List<ShippingBoxDto> shippingBoxDtos)
        {
            foreach (var shippingBoxDto in shippingBoxDtos)
            {
                var shippingBox = dbContext.ShippingBoxes.Find(shippingBoxDto.Id) ??
                                           throw new NullReferenceException();

                shippingBox.SortOrder = shippingBoxDtos.FindIndex(x => x.Id == shippingBoxDto.Id);
            }

            dbContext.SaveChanges();
        }
    }
}
