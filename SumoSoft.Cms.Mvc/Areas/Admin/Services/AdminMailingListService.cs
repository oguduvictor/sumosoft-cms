﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminMailingListService : IAdminMailingListService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminMailingListService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public MailingList AddOrUpdateMailingList(CmsDbContext dbContext, MailingListDto mailingListDto)
        {
            var mailingList = dbContext.MailingLists.Find(mailingListDto.Id);

            if (mailingList == null)
            {
                mailingList = new MailingList
                {
                    Id = mailingListDto.Id,
                    IsDisabled = mailingListDto.IsDisabled ?? false,
                    Name = mailingListDto.Name,
                    SortOrder = mailingListDto.SortOrder,
                    MailingListLocalizedKits = mailingListDto.MailingListLocalizedKits.Select(mailingListLocalizedKitDto => new MailingListLocalizedKit
                    {
                        Id = mailingListLocalizedKitDto.Id,
                        Country = dbContext.Countries.Find(mailingListLocalizedKitDto.Country?.Id),
                        IsDisabled = mailingListLocalizedKitDto.IsDisabled ?? false,
                        Title = mailingListLocalizedKitDto.Title,
                        Description = mailingListLocalizedKitDto.Description
                    }).ToList()
                };

                dbContext.MailingLists.Add(mailingList);
            }
            else
            {
                mailingList.Name = mailingListDto.Name;
                mailingList.IsDisabled = mailingListDto.IsDisabled ?? false;
                mailingList.SortOrder = mailingListDto.SortOrder;

                foreach (var mailingListLocalizedKitDto in mailingListDto.MailingListLocalizedKits)
                {
                    var mailingListLocalizedKit = dbContext.MailingListLocalizedKits.Find(mailingListLocalizedKitDto.Id);

                    if (mailingListLocalizedKit == null)
                    {
                        mailingListLocalizedKit = new MailingListLocalizedKit
                        {
                            Id = mailingListLocalizedKitDto.Id,
                            Country = dbContext.Countries.Find(mailingListLocalizedKitDto.Country?.Id),
                            IsDisabled = mailingListLocalizedKitDto.IsDisabled ?? false,
                            Title = mailingListLocalizedKitDto.Title,
                            Description = mailingListLocalizedKitDto.Description
                        };

                        mailingList.MailingListLocalizedKits.Add(mailingListLocalizedKit);
                    }
                    else
                    {
                        mailingListLocalizedKit.IsDisabled = mailingListLocalizedKitDto.IsDisabled ?? false;
                        mailingListLocalizedKit.Title = mailingListLocalizedKitDto.Title;
                        mailingListLocalizedKit.Description = mailingListLocalizedKitDto.Description;
                    }
                }
            }

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(mailingList.MailingListLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.MailingListLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return mailingList;
        }

        public virtual void UpdateSortOrder(CmsDbContext dbContext, List<MailingListDto> mailingListDtos)
        {
            foreach (var mailingListDto in mailingListDtos)
            {
                var mailingList = dbContext.MailingLists.FirstOrDefault(x => x.Id == mailingListDto.Id);

                if (mailingList == null)
                {
                    throw new NullReferenceException();
                }

                mailingList.SortOrder = mailingListDtos.FindIndex(x => x.Id == mailingListDto.Id);
            }

            dbContext.SaveChanges();
        }

        public virtual MailingList Duplicate(CmsDbContext dbContext, MailingList mailingList)
        {
            var newMailingList = new MailingList
            {
                Name = mailingList.Name + "_Copy",
                MailingListSubscriptions = mailingList.MailingListSubscriptions.Select(x => new MailingListSubscription
                {
                    User = x.User,
                    Status = x.Status
                }).ToList(),
                IsDisabled = true,
                SortOrder = mailingList.SortOrder + 1,
                MailingListLocalizedKits = mailingList.MailingListLocalizedKits.Select(lk => new MailingListLocalizedKit
                {
                    Title = lk.Title,
                    Country = lk.Country,
                    Description = lk.Description,
                    IsDisabled = lk.IsDisabled
                }).ToList()
            };

            dbContext.MailingLists.Add(newMailingList);

            dbContext.SaveChanges();

            return newMailingList;
        }

    }
}