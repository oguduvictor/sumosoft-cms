#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class AdminSettingService : IAdminSettingService
    {
        private readonly IUtilityService utilityService;

        public AdminSettingService(IUtilityService utilityService)
        {
            this.utilityService = utilityService;
        }

        public virtual CmsSettings AddOrUpdate(CmsDbContext dbContext, CmsSettingsDto cmsSettingsDto)
        {
            var cmsSettings = dbContext.CmsSettings.Find(cmsSettingsDto.Id);

            if (cmsSettings == null)
            {
                cmsSettings = new CmsSettings
                {
                    Id = cmsSettingsDto.Id,
                    IsSeeded = cmsSettingsDto.IsSeeded,
                    SmtpHost = cmsSettingsDto.SmtpHost,
                    SmtpPort = cmsSettingsDto.SmtpPort,
                    SmtpDisplayName = cmsSettingsDto.SmtpDisplayName,
                    SmtpEmail = cmsSettingsDto.SmtpEmail,
                    SmtpPassword = this.utilityService.Encrypt(cmsSettingsDto.SmtpPassword),
                    UseAzureStorage = cmsSettingsDto.UseAzureStorage,
                    IsDisabled = cmsSettingsDto.IsDisabled ?? false
                };

                dbContext.CmsSettings.Add(cmsSettings);
            }
            else
            {
                cmsSettings.IsSeeded = cmsSettingsDto.IsSeeded;
                cmsSettings.SmtpHost = cmsSettingsDto.SmtpHost;
                cmsSettings.SmtpPort = cmsSettingsDto.SmtpPort;
                cmsSettings.SmtpDisplayName = cmsSettingsDto.SmtpDisplayName;
                cmsSettings.SmtpEmail = cmsSettingsDto.SmtpEmail;
                cmsSettings.SmtpPassword = this.utilityService.Encrypt(cmsSettingsDto.SmtpPassword);
                cmsSettings.UseAzureStorage = cmsSettingsDto.UseAzureStorage;
                cmsSettings.IsDisabled = cmsSettingsDto.IsDisabled ?? false;
            }

            dbContext.SaveChanges();

            return cmsSettings;
        }
    }
}
