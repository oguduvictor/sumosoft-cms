﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.AutoTagServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminProductService : IAdminProductService
    {
        private readonly IAdminUtilityService adminUtilityService;
        private readonly IAutoTagService autoTagService;

        public AdminProductService(
            IAdminUtilityService adminUtilityService,
            IAutoTagService autoTagService)
        {
            this.adminUtilityService = adminUtilityService;
            this.autoTagService = autoTagService;
        }

        public virtual Product AddOrUpdate(CmsDbContext dbContext, ProductDto productDto)
        {
            var product = dbContext.Products.Find(productDto.Id);

            if (product == null)
            {
                product = new Product
                {
                    Id = productDto.Id,
                    Name = productDto.Name?.Trim(),
                    Url = productDto.Url?.Trim(),
                    Tags = productDto.Tags?.Trim(),
                    Code = productDto.Code?.Trim(),
                    IsDisabled = productDto.IsDisabled ?? false,
                    Comments = productDto.Comments?.Trim(),
                    RelatedProducts = productDto.RelatedProducts.Select(rp => dbContext.Products.Find(rp.Id)).ToList(),
                    ShippingBoxes = productDto.ShippingBoxes.Select(sb => dbContext.ShippingBoxes.Find(sb.Id)).ToList(),
                    TaxCode = productDto.TaxCode?.Trim(),
                    Categories = productDto.Categories.Select(c => dbContext.Categories.Find(c.Id)).ToList(),
                    ProductLocalizedKits = productDto.ProductLocalizedKits.Select(productLocalizedKitDto => new ProductLocalizedKit
                    {
                        Id = productLocalizedKitDto.Id,
                        Product = product,
                        Country = dbContext.Countries.Find(productLocalizedKitDto.Country?.Id),
                        IsDisabled = productLocalizedKitDto.IsDisabled ?? false,
                        Title = productLocalizedKitDto.Title?.Trim(),
                        Description = productLocalizedKitDto.Description?.Trim(),
                        MetaTitle = productLocalizedKitDto.MetaTitle?.Trim(),
                        MetaDescription = productLocalizedKitDto.MetaDescription?.Trim()
                    }).ToList()
                };

                dbContext.Products.Add(product);
            }
            else
            {
                product.Name = productDto.Name?.Trim();
                product.Url = productDto.Url?.Trim();
                product.Tags = productDto.Tags?.Trim();
                product.Code = productDto.Code?.Trim();
                product.IsDisabled = productDto.IsDisabled ?? false;
                product.Comments = productDto.Comments?.Trim();
                product.RelatedProducts.Clear();
                product.RelatedProducts = productDto.RelatedProducts.Select(rp => dbContext.Products.Find(rp.Id)).ToList();
                product.ShippingBoxes.Clear();
                product.ShippingBoxes = productDto.ShippingBoxes.Select(sb => dbContext.ShippingBoxes.Find(sb.Id)).ToList();
                product.TaxCode = productDto.TaxCode?.Trim();
                product.Categories.Clear();
                product.Categories = productDto.Categories.Select(c => dbContext.Categories.Find(c.Id)).ToList();

                foreach (var productLocalizedKitDto in productDto.ProductLocalizedKits)
                {
                    var productLocalizedKit = product.ProductLocalizedKits.FirstOrDefault(x => x.Id == productLocalizedKitDto.Id);

                    if (productLocalizedKit == null)
                    {
                        productLocalizedKit = new ProductLocalizedKit
                        {
                            Id = productLocalizedKitDto.Id,
                            Product = product,
                            Country = dbContext.Countries.Find(productLocalizedKitDto.Country?.Id),
                            IsDisabled = productLocalizedKitDto.IsDisabled ?? false,
                            Title = productLocalizedKitDto.Title?.Trim(),
                            Description = productLocalizedKitDto.Description?.Trim(),
                            MetaTitle = productLocalizedKitDto.MetaTitle?.Trim(),
                            MetaDescription = productLocalizedKitDto.MetaDescription?.Trim()
                        };

                        product.ProductLocalizedKits.Add(productLocalizedKit);
                    }
                    else
                    {
                        productLocalizedKit.IsDisabled = productLocalizedKitDto.IsDisabled ?? false;
                        productLocalizedKit.Title = productLocalizedKitDto.Title?.Trim();
                        productLocalizedKit.Description = productLocalizedKitDto.Description?.Trim();
                        productLocalizedKit.MetaTitle = productLocalizedKitDto.MetaTitle?.Trim();
                        productLocalizedKit.MetaDescription = productLocalizedKitDto.MetaDescription?.Trim();
                    }
                }

                // -----------------------------------------------------------------------------
                // Update Related ProductImageKits
                // -----------------------------------------------------------------------------

                foreach (var productImageKitDto in productDto.ProductImageKits)
                {
                    var productImageKit = dbContext.ProductImageKits.Find(productImageKitDto.Id);

                    var relatedProductImageKitIds = productImageKit.RelatedProductImageKits.Select(pik => pik.Id).ToList();
                    var relatedProductImageKitDtoIds = productImageKitDto.RelatedProductImageKits.Select(pik => pik.Id).ToList();

                    if (!relatedProductImageKitIds.SequenceEqualIgnoreOrder(relatedProductImageKitDtoIds))
                    {
                        productImageKit.RelatedProductImageKits.Clear();
                        productImageKit.RelatedProductImageKits = relatedProductImageKitDtoIds.Select(id => dbContext.ProductImageKits.Find(id)).ToList();
                    }
                }
            }

            product.AutoTags = this.autoTagService.GetAutoTags(product);

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(product.ProductLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.ProductStockUnitLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return product;
        }

        public virtual void AddOrUpdateProductVariants(CmsDbContext dbContext, Guid productId, List<ProductVariantDto> productVariantDtos)
        {
            var product = dbContext.Products.Find(productId);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            foreach (var productVariantDto in productVariantDtos)
            {
                if (productVariantDto.IsDeleted ?? false)
                {
                    dbContext.ProductVariants.Delete(productVariantDto.Id, false);
                }
                else
                {
                    var productVariant = dbContext.ProductVariants.Find(productVariantDto.Id);

                    if (productVariant == null)
                    {
                        productVariant = new ProductVariant
                        {
                            Id = productVariantDto.Id,
                            Product = product,
                            Variant = dbContext.Variants.Find(productVariantDto.Variant?.Id),
                            IsDisabled = productVariantDto.IsDisabled ?? false,
                            VariantOptions = productVariantDto.VariantOptions.Select(vo => dbContext.VariantOptions.Find(vo.Id)).ToList(),
                            DefaultBooleanValue = productVariantDto.DefaultBooleanValue,
                            DefaultDoubleValue = productVariantDto.DefaultDoubleValue,
                            DefaultIntegerValue = productVariantDto.DefaultIntegerValue,
                            DefaultStringValue = productVariantDto.DefaultStringValue,
                            DefaultJsonValue = productVariantDto.DefaultJsonValue,
                            DefaultVariantOptionValue = dbContext.VariantOptions.Find(productVariantDto.DefaultVariantOptionValue?.Id)
                        };

                        dbContext.ProductVariants.Add(productVariant);
                    }
                    else
                    {
                        productVariant.IsDisabled = productVariantDto.IsDisabled ?? false;
                        productVariant.VariantOptions.Clear();
                        productVariant.VariantOptions = productVariantDto.VariantOptions.Select(vo => dbContext.VariantOptions.Find(vo.Id)).ToList();
                        productVariant.DefaultBooleanValue = productVariantDto.DefaultBooleanValue;
                        productVariant.DefaultDoubleValue = productVariantDto.DefaultDoubleValue;
                        productVariant.DefaultIntegerValue = productVariantDto.DefaultIntegerValue;
                        productVariant.DefaultStringValue = productVariantDto.DefaultStringValue;
                        productVariant.DefaultJsonValue = productVariantDto.DefaultJsonValue;
                        productVariant.DefaultVariantOptionValue = dbContext.VariantOptions.Find(productVariantDto.DefaultVariantOptionValue?.Id);
                    }
                }
            }

            // -------------------------------------------------------------------------
            // Disable obsolete ProductImageKits and ProductStockUnits
            // -------------------------------------------------------------------------

            var productVariantAffectingImageKits = product.ProductVariants
                .Where(pvo => pvo.Variant.AsNotNull().CreateProductImageKits)
                .ToList();

            foreach (var productImageKit in product.ProductImageKits)
            {
                if (productVariantAffectingImageKits.Count == productImageKit.VariantOptions.Count &&
                    productVariantAffectingImageKits.SelectMany(pv => pv.VariantOptions).ContainsAllById(productImageKit.VariantOptions))
                {
                    productImageKit.IsDisabled = false;
                }
                else
                {
                    productImageKit.IsDisabled = true;
                }
            }

            var productVariantAffectingStockUnits = product.ProductVariants
                .Where(pvo => pvo.Variant.AsNotNull().CreateProductStockUnits)
                .ToList();

            foreach (var productStockUnit in product.ProductStockUnits)
            {
                if (productVariantAffectingStockUnits.Count == productStockUnit.VariantOptions.Count &&
                    productVariantAffectingStockUnits.SelectMany(pv => pv.VariantOptions).ContainsAllById(productStockUnit.VariantOptions))
                {
                    productStockUnit.IsDisabled = false;
                }
                else
                {
                    productStockUnit.IsDisabled = true;
                }
            }

            dbContext.SaveChanges();
        }

        public virtual void AddOrUpdateProductAttributes(CmsDbContext dbContext, Guid productId, List<ProductAttributeDto> productAttributeDtos)
        {
            var product = dbContext.Products.Find(productId);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            var productAttributesToDelete = this.adminUtilityService.GetEntitiesToDelete(product.ProductAttributes, productAttributeDtos);

            foreach (var productAttribute in productAttributesToDelete)
            {
                dbContext.ProductAttributes.Delete(productAttribute.Id, false);
            }

            foreach (var productAttributeDto in productAttributeDtos)
            {
                var productAttribute = dbContext.ProductAttributes.Find(productAttributeDto.Id);

                if (productAttribute == null)
                {
                    productAttribute = new ProductAttribute
                    {
                        Id = productAttributeDto.Id,
                        Product = product,
                        Attribute = dbContext.Attributes.Find(productAttributeDto.Attribute?.Id),
                        IsDisabled = productAttributeDto.IsDisabled ?? false,
                        AttributeOptionValue = dbContext.AttributeOptions.Find(productAttributeDto.AttributeOptionValue?.Id),
                        BooleanValue = productAttributeDto.BooleanValue,
                        ContentSectionValue = dbContext.ContentSections.Find(productAttributeDto.ContentSectionValue?.Id),
                        DateTimeValue = productAttributeDto.DateTimeValue,
                        DoubleValue = productAttributeDto.DoubleValue,
                        ImageValue = productAttributeDto.ImageValue,
                        StringValue = productAttributeDto.StringValue
                    };

                    dbContext.ProductAttributes.Add(productAttribute);
                }
                else
                {
                    productAttribute.IsDisabled = productAttributeDto.IsDisabled ?? false;
                    productAttribute.AttributeOptionValue = dbContext.AttributeOptions.Find(productAttributeDto.AttributeOptionValue?.Id);
                    productAttribute.BooleanValue = productAttributeDto.BooleanValue;
                    productAttribute.ContentSectionValue = dbContext.ContentSections.Find(productAttributeDto.ContentSectionValue?.Id);
                    productAttribute.DateTimeValue = productAttributeDto.DateTimeValue;
                    productAttribute.DoubleValue = productAttributeDto.DoubleValue;
                    productAttribute.ImageValue = productAttributeDto.ImageValue;
                    productAttribute.StringValue = productAttributeDto.StringValue;
                }
            }

            dbContext.SaveChanges();
        }

        public virtual void AddOrUpdateProductImageKits(CmsDbContext dbContext, Guid productId, List<ProductImageKitDto> productImageKitDtos)
        {
            var product = dbContext.Products.Find(productId);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            foreach (var productImageKitDto in productImageKitDtos)
            {
                if (productImageKitDto.IsDeleted ?? false)
                {
                    dbContext.ProductImageKits.Delete(productImageKitDto.Id, false);
                }
                else
                {
                    var productImageKit = dbContext.ProductImageKits.Find(productImageKitDto.Id);

                    if (productImageKit == null)
                    {
                        productImageKit = new ProductImageKit
                        {
                            Id = productImageKitDto.Id,
                            Product = product,
                            IsDisabled = productImageKitDto.IsDisabled ?? false,
                            VariantOptions = productImageKitDto.VariantOptions.Select(vo => dbContext.VariantOptions.Find(vo.Id)).ToList(),
                            ProductImages = productImageKitDto.ProductImages.Select(productImageDto => new ProductImage
                            {
                                Id = productImageDto.Id,
                                Name = productImageDto.Name?.Trim(),
                                Url = productImageDto.Url?.Trim(),
                                AltText = productImageDto.AltText?.Trim(),
                                SortOrder = productImageKitDto.ProductImages.FindIndex(x => x.Id == productImageDto.Id),
                                IsDisabled = productImageDto.IsDisabled ?? false
                            }).ToList(),
                            Categories = productImageKitDto.Categories.Select(c => dbContext.Categories.Find(c.Id)).ToList()
                        };

                        dbContext.ProductImageKits.Add(productImageKit);
                    }
                    else
                    {
                        productImageKit.IsDisabled = productImageKitDto.IsDisabled ?? false;

                        foreach (var productImageDto in productImageKitDto.ProductImages)
                        {
                            if (productImageDto.IsDeleted ?? false)
                            {
                                dbContext.ProductImages.Delete(productImageDto.Id, false);
                            }
                            else
                            {
                                var productImage = productImageKit.ProductImages.FirstOrDefault(x => x.Id == productImageDto.Id);

                                if (productImage == null)
                                {
                                    productImage = new ProductImage
                                    {
                                        Id = productImageDto.Id,
                                        Name = productImageDto.Name?.Trim(),
                                        Url = productImageDto.Url?.Trim(),
                                        AltText = productImageDto.AltText?.Trim(),
                                        SortOrder = productImageKitDto.ProductImages.FindIndex(x => x.Id == productImageDto.Id),
                                        IsDisabled = productImageDto.IsDisabled ?? false
                                    };

                                    productImageKit.ProductImages.Add(productImage);
                                }
                                else
                                {
                                    productImage.Name = productImageDto.Name?.Trim();
                                    productImage.Url = productImageDto.Url?.Trim();
                                    productImage.AltText = productImageDto.AltText?.Trim();
                                    productImage.SortOrder = productImageKitDto.ProductImages.FindIndex(x => x.Id == productImageDto.Id);
                                    productImage.IsDisabled = productImageDto.IsDisabled ?? false;
                                }
                            }
                        }
                    }
                }
            }

            dbContext.SaveChanges();
        }

        public virtual void AddOrUpdateProductStockUnits(CmsDbContext dbContext, Guid productId, List<ProductStockUnitDto> productStockUnitDtos)
        {
            var product = dbContext.Products.Find(productId);

            if (product == null)
            {
                throw new NullReferenceException();
            }

            foreach (var productStockUnitDto in productStockUnitDtos)
            {
                if (productStockUnitDto.IsDeleted ?? false)
                {
                    dbContext.ProductStockUnits.Delete(productStockUnitDto.Id, false);
                }
                else
                {
                    var productStockUnit = dbContext.ProductStockUnits.Find(productStockUnitDto.Id);

                    if (productStockUnit == null)
                    {
                        productStockUnit = new ProductStockUnit
                        {
                            Product = product,
                            Code = productStockUnitDto.Code,
                            EnablePreorder = productStockUnitDto.EnablePreorder,
                            DispatchDate = productStockUnitDto.DispatchDate,
                            DispatchTime = productStockUnitDto.DispatchTime,
                            Stock = productStockUnitDto.Stock,
                            VariantOptions = productStockUnitDto.VariantOptions.Select(vo => dbContext.VariantOptions.Find(vo.Id)).ToList(),
                            ProductStockUnitLocalizedKits = productStockUnitDto.ProductStockUnitLocalizedKits.Select(lk => new ProductStockUnitLocalizedKit
                            {
                                ProductStockUnit = productStockUnit,
                                Country = dbContext.Countries.Find(lk.Country?.Id),
                                BasePrice = lk.BasePrice,
                                SalePrice = lk.SalePrice,
                                MembershipSalePrice = lk.MembershipSalePrice
                            }).ToList()
                        };

                        dbContext.ProductStockUnits.Add(productStockUnit);
                    }
                    else
                    {
                        productStockUnit.Code = productStockUnitDto.Code?.Trim();
                        productStockUnit.EnablePreorder = productStockUnitDto.EnablePreorder;
                        productStockUnit.DispatchDate = productStockUnitDto.DispatchDate;
                        productStockUnit.DispatchTime = productStockUnitDto.DispatchTime;
                        productStockUnit.Stock = productStockUnitDto.Stock;

                        foreach (var productStockUnitLocalizedKitDto in productStockUnitDto.ProductStockUnitLocalizedKits)
                        {
                            var productStockUnitLocalizedKit = dbContext.ProductStockUnitLocalizedKits.Find(productStockUnitLocalizedKitDto.Id);

                            if (productStockUnitLocalizedKit == null)
                            {
                                productStockUnitLocalizedKit = new ProductStockUnitLocalizedKit
                                {
                                    ProductStockUnit = productStockUnit,
                                    Country = dbContext.Countries.Find(productStockUnitLocalizedKitDto.Country?.Id),
                                    BasePrice = productStockUnitLocalizedKitDto.BasePrice,
                                    SalePrice = productStockUnitLocalizedKitDto.SalePrice,
                                    MembershipSalePrice = productStockUnitLocalizedKitDto.MembershipSalePrice
                                };

                                productStockUnit.ProductStockUnitLocalizedKits.Add(productStockUnitLocalizedKit);
                            }
                            else
                            {
                                productStockUnitLocalizedKit.BasePrice = productStockUnitLocalizedKitDto.BasePrice;
                                productStockUnitLocalizedKit.SalePrice = productStockUnitLocalizedKitDto.SalePrice;
                                productStockUnitLocalizedKit.MembershipSalePrice = productStockUnitLocalizedKitDto.MembershipSalePrice;
                            }
                        }
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            foreach (var productStockUnit in product.ProductStockUnits)
            {
                var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(productStockUnit.ProductStockUnitLocalizedKits);

                foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
                {
                    dbContext.ProductStockUnitLocalizedKits.Delete(localizedKit.Id, false);
                }
            }

            dbContext.SaveChanges();
        }

        public virtual Product Duplicate(CmsDbContext dbContext, Product product)
        {
            var newProduct = new Product
            {
                Categories = product.Categories,
                ShippingBoxes = product.ShippingBoxes
            };

            foreach (var productAttribute in product.ProductAttributes)
            {
                var newProductAttribute = new ProductAttribute
                {
                    Product = newProduct,
                    Attribute = productAttribute.Attribute,
                    StringValue = productAttribute.StringValue,
                    BooleanValue = productAttribute.BooleanValue,
                    ContentSectionValue = productAttribute.ContentSectionValue,
                    DateTimeValue = productAttribute.DateTimeValue,
                    DoubleValue = productAttribute.DoubleValue,
                    AttributeOptionValue = productAttribute.AttributeOptionValue,
                    ImageValue = productAttribute.ImageValue
                };

                newProduct.ProductAttributes.Add(newProductAttribute);
            }

            foreach (var productVariant in product.ProductVariants)
            {
                var newProductVariant = new ProductVariant
                {
                    Product = newProduct,
                    Variant = productVariant.Variant,
                    VariantOptions = productVariant.VariantOptions,
                    DefaultVariantOptionValue = productVariant.DefaultVariantOptionValue,
                    DefaultDoubleValue = productVariant.DefaultDoubleValue,
                    DefaultBooleanValue = productVariant.DefaultBooleanValue,
                    DefaultIntegerValue = productVariant.DefaultIntegerValue,
                    DefaultStringValue = productVariant.DefaultStringValue
                };

                newProduct.ProductVariants.Add(newProductVariant);
            }

            dbContext.Products.Add(newProduct);

            dbContext.SaveChanges();

            return newProduct;
        }

        public virtual ContentSection AddContentSection(CmsDbContext dbContext, ContentSectionDto contentSectionDto)
        {
            var contentSection = new ContentSection
            {
                Id = contentSectionDto.Id,
                Name = contentSectionDto.Name?.Trim(),
                IsDisabled = contentSectionDto.IsDisabled ?? false,
                ContentSectionLocalizedKits = contentSectionDto.ContentSectionLocalizedKits.Select(contentSectionLocalizedKitDto => new ContentSectionLocalizedKit
                {
                    Id = contentSectionLocalizedKitDto.Id,
                    Country = dbContext.Countries.Find(contentSectionLocalizedKitDto.Country?.Id),
                    Content = contentSectionLocalizedKitDto.Content?.Trim()
                }).ToList()
            };

            dbContext.ContentSections.Add(contentSection);

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(contentSection.ContentSectionLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.ContentSectionLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return contentSection;
        }
    }
}
