﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System.Linq;
    using SumoSoft.Cms.AutoTagServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminOrderService : IAdminOrderService
    {
        private readonly IAutoTagService autoTagService;

        public AdminOrderService(IAutoTagService autoTagService)
        {
            this.autoTagService = autoTagService;
        }

        public virtual Order Update(CmsDbContext dbContext, OrderDto orderDto)
        {
            var order = dbContext.Orders.Find(orderDto.Id);

            if (order == null)
            {
                return null;
            }

            order.Comments = orderDto.Comments;
            order.Status = orderDto.Status;

            foreach (var orderShippingBox in order.OrderShippingBoxes)
            {
                var orderShippingBoxDto = orderDto.OrderShippingBoxes.FirstOrDefault(x => x.Id == orderShippingBox.Id);

                orderShippingBox.TrackingCode = orderShippingBoxDto?.TrackingCode;
                orderShippingBox.Status = orderShippingBoxDto?.Status ?? OrderShippingBoxStatusEnum.Preparing;

                foreach (var orderItem in orderShippingBox.OrderItems)
                {
                    var orderItemDto = orderShippingBoxDto?.OrderItems.FirstOrDefault(x => x.Id == orderItem.Id);

                    orderItem.Status = orderItemDto?.Status ?? OrderItemStatusEnum.Default;
                }
            }

            order.AutoTags = this.autoTagService.GetAutoTags(order);

            dbContext.SaveChanges();

            return order;
        }
    }
}
