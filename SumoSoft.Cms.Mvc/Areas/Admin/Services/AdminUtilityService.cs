﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Domain.Interfaces;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class AdminUtilityService : IAdminUtilityService
    {
        private readonly CmsDbContext dbContext;
        private readonly IAuthenticationService authenticationService;

        public AdminUtilityService(
            IRequestDbContextService requestDbContextService,
            IAuthenticationService authenticationService)
        {
            this.dbContext = requestDbContextService.Get();
            this.authenticationService = authenticationService;
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localizedKits"></param>
        /// <param name="localizedKit">A new LocalizedKit that defines the relationship with the father entity.</param>
        /// <returns></returns>
        public virtual List<T> UpdateLocalizedKits<T>(List<T> localizedKits, T localizedKit) where T : BaseEntity, ILocalizedKit, new()
        {
            var localizedCountries = this.dbContext.Countries.Where(c => c.Localize).ToList();

            foreach (var localizedCountry in localizedCountries)
            {
                var localizedKitsAssociatedToThisCountry = localizedKits.Where(x => x.Country != null && x.Country.Id == localizedCountry.Id).ToList();

                if (localizedKitsAssociatedToThisCountry.Count > 1)
                {
                    var localizedKitsToDelete = localizedKitsAssociatedToThisCountry
                        .OrderBy(x => x.ModifiedDate)
                        .Take(localizedKitsAssociatedToThisCountry.Count - 1);

                    foreach (var localizedKitToDelete in localizedKitsToDelete)
                    {
                        switch (localizedKit)
                        {
                            case AttributeLocalizedKit _:
                                this.dbContext.AttributeLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case AttributeOptionLocalizedKit _:
                                this.dbContext.AttributeOptionLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case BlogCategoryLocalizedKit _:
                                this.dbContext.BlogCategoryLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case ContentSectionLocalizedKit _:
                                this.dbContext.ContentSectionLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case CategoryLocalizedKit _:
                                this.dbContext.CategoryLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case ProductLocalizedKit _:
                                this.dbContext.ProductLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case ProductStockUnitLocalizedKit _:
                                this.dbContext.ProductStockUnitLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case ShippingBoxLocalizedKit _:
                                this.dbContext.ShippingBoxLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case VariantLocalizedKit _:
                                this.dbContext.VariantLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case VariantOptionLocalizedKit _:
                                this.dbContext.VariantOptionLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case EmailLocalizedKit _:
                                this.dbContext.EmailLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case SeoSectionLocalizedKit _:
                                this.dbContext.SeoSectionLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                            case UserLocalizedKit _:
                                this.dbContext.UserLocalizedKits.Delete(localizedKitToDelete.Id, false);
                                break;
                        }
                    }

                    this.dbContext.SaveChanges();
                }

                if (!localizedKitsAssociatedToThisCountry.Any())
                {
                    var newLocalizedKit = localizedKit.Clone();

                    newLocalizedKit.Id = Guid.NewGuid();
                    newLocalizedKit.Country = localizedCountry;

                    localizedKits.Add(newLocalizedKit);
                }
            }

            return localizedKits.OrderByDefaultCountry();
        }

        public virtual List<T> GetDuplicatedLocalizedKitsToDelete<T>(List<T> localizedKits) where T : BaseEntity, ILocalizedKit
        {
            var localizedKitGroups = localizedKits.GroupBy(x => x.Country?.Id);

            var duplicatedLocalizedKitGroups = localizedKitGroups.Where(x => x.Count() > 1);

            return duplicatedLocalizedKitGroups
                .SelectMany(group => group.OrderByDescending(x => x.ModifiedDate).Skip(1))
                .ToList();
        }

        /// <summary>
        /// Return Authenticated User Dto.
        /// </summary>
        [NotNull]
        public virtual UserDto GetAuthenticatedUserDto(CmsDbContext dbContext)
        {
            var authenticatedUser = this.authenticationService.GetAuthenticatedUser(dbContext).AsNotNull();

            return new UserDto
            {
                Id = authenticatedUser.Id,
                Role = authenticatedUser.Role == null ? null : new UserRoleDto
                {
                    Id = authenticatedUser.Role.Id,
                    Name = authenticatedUser.Role.Name,
                    AccessAdmin = authenticatedUser.Role.AccessAdmin,
                    AccessAdminBlog = authenticatedUser.Role.AccessAdminBlog,
                    AccessAdminContent = authenticatedUser.Role.AccessAdminContent,
                    AccessAdminCountries = authenticatedUser.Role.AccessAdminCountries,
                    AccessAdminEcommerce = authenticatedUser.Role.AccessAdminEcommerce,
                    AccessAdminEmails = authenticatedUser.Role.AccessAdminEmails,
                    AccessAdminMedia = authenticatedUser.Role.AccessAdminMedia,
                    AccessAdminOrders = authenticatedUser.Role.AccessAdminOrders,
                    AccessAdminSeo = authenticatedUser.Role.AccessAdminSeo,
                    AccessAdminSettings = authenticatedUser.Role.AccessAdminSettings,
                    AccessAdminUsers = authenticatedUser.Role.AccessAdminUsers,
                    CreateAttribute = authenticatedUser.Role.CreateAttribute,
                    CreateAttributeOption = authenticatedUser.Role.CreateAttributeOption,
                    CreateBlogPost = authenticatedUser.Role.CreateBlogPost,
                    CreateContentSection = authenticatedUser.Role.CreateContentSection,
                    CreateCountry = authenticatedUser.Role.CreateCountry,
                    CreateCoupon = authenticatedUser.Role.CreateCoupon,
                    CreateCategory = authenticatedUser.Role.CreateCategory,
                    CreateEmail = authenticatedUser.Role.CreateEmail,
                    CreateMailingList = authenticatedUser.Role.CreateMailingList,
                    CreateProduct = authenticatedUser.Role.CreateProduct,
                    CreateSeoSection = authenticatedUser.Role.CreateSeoSection,
                    CreateShippingBox = authenticatedUser.Role.CreateShippingBox,
                    CreateUser = authenticatedUser.Role.CreateUser,
                    CreateUserRole = authenticatedUser.Role.CreateUserRole,
                    CreateVariant = authenticatedUser.Role.CreateVariant,
                    CreateVariantOption = authenticatedUser.Role.CreateVariantOption,
                    DeleteAttribute = authenticatedUser.Role.DeleteAttribute,
                    DeleteAttributeOption = authenticatedUser.Role.DeleteAttributeOption,
                    DeleteBlogPost = authenticatedUser.Role.DeleteBlogPost,
                    DeleteContentSection = authenticatedUser.Role.DeleteContentSection,
                    DeleteCountry = authenticatedUser.Role.DeleteCountry,
                    DeleteCoupon = authenticatedUser.Role.DeleteCoupon,
                    DeleteCategory = authenticatedUser.Role.DeleteCategory,
                    DeleteEmail = authenticatedUser.Role.DeleteEmail,
                    DeleteMailingList = authenticatedUser.Role.DeleteMailingList,
                    DeleteProduct = authenticatedUser.Role.DeleteProduct,
                    DeleteSeoSection = authenticatedUser.Role.DeleteSeoSection,
                    DeleteShippingBox = authenticatedUser.Role.DeleteShippingBox,
                    DeleteUser = authenticatedUser.Role.DeleteUser,
                    DeleteUserRole = authenticatedUser.Role.DeleteUserRole,
                    DeleteVariant = authenticatedUser.Role.DeleteVariant,
                    DeleteVariantOption = authenticatedUser.Role.DeleteVariantOption,
                    EditAttribute = authenticatedUser.Role.EditAttribute,
                    EditAttributeAllLocalizedKits = authenticatedUser.Role.EditAttributeAllLocalizedKits,
                    EditAttributeName = authenticatedUser.Role.EditAttributeName,
                    EditAttributeOption = authenticatedUser.Role.EditAttributeOption,
                    EditAttributeOptionAllLocalizedKits = authenticatedUser.Role.EditAttributeOptionAllLocalizedKits,
                    EditAttributeOptionName = authenticatedUser.Role.EditAttributeOptionName,
                    EditContentSection = authenticatedUser.Role.EditContentSection,
                    EditBlogPost = authenticatedUser.Role.EditBlogPost,
                    EditContentSectionAllLocalizedKits = authenticatedUser.Role.EditContentSectionAllLocalizedKits,
                    EditContentSectionName = authenticatedUser.Role.EditContentSectionName,
                    EditCountry = authenticatedUser.Role.EditCountry,
                    EditCoupon = authenticatedUser.Role.EditCoupon,
                    EditCategory = authenticatedUser.Role.EditCategory,
                    EditCategoryAllLocalizedKits = authenticatedUser.Role.EditCategoryAllLocalizedKits,
                    EditCategoryName = authenticatedUser.Role.EditCategoryName,
                    EditCategoryUrl = authenticatedUser.Role.EditCategoryUrl,
                    EditEmail = authenticatedUser.Role.EditEmail,
                    EditEmailAllLocalizedKits = authenticatedUser.Role.EditEmailAllLocalizedKits,
                    EditEmailName = authenticatedUser.Role.EditEmailName,
                    EditMailingList = authenticatedUser.Role.EditMailingList,
                    EditMailingListAllLocalizedKits = authenticatedUser.Role.EditMailingListAllLocalizedKits,
                    EditMailingListName = authenticatedUser.Role.EditMailingListName,
                    EditOrder = authenticatedUser.Role.EditOrder,
                    EditProduct = authenticatedUser.Role.EditProduct,
                    EditProductAllLocalizedKits = authenticatedUser.Role.EditProductAllLocalizedKits,
                    EditSeoSection = authenticatedUser.Role.EditSeoSection,
                    EditSeoSectionAllLocalizedKits = authenticatedUser.Role.EditSeoSectionAllLocalizedKits,
                    EditSeoSectionPage = authenticatedUser.Role.EditSeoSectionPage,
                    EditShippingBox = authenticatedUser.Role.EditShippingBox,
                    EditShippingBoxAllLocalizedKits = authenticatedUser.Role.EditShippingBoxAllLocalizedKits,
                    EditShippingBoxName = authenticatedUser.Role.EditShippingBoxName,
                    EditVariantAllLocalizedKits = authenticatedUser.Role.EditVariantAllLocalizedKits,
                    EditVariantOptionAllLocalizedKits = authenticatedUser.Role.EditVariantOptionAllLocalizedKits,
                    EditVariant = authenticatedUser.Role.EditVariant,
                    EditUserRole = authenticatedUser.Role.EditUserRole,
                    EditVariantOption = authenticatedUser.Role.EditVariantOption,
                    EditVariantName = authenticatedUser.Role.EditVariantName,
                    EditVariantSelector = authenticatedUser.Role.EditVariantSelector,
                    EditVariantOptionName = authenticatedUser.Role.EditVariantOptionName,
                    EditUser = authenticatedUser.Role.EditUser,
                    ViewExceptionLogs = authenticatedUser.Role.ViewExceptionLogs,
                    ViewEcommerceLogs = authenticatedUser.Role.ViewEcommerceLogs,
                    ViewContentLogs = authenticatedUser.Role.ViewContentLogs
                },
                Country = authenticatedUser.Country == null ? null : new CountryDto
                {
                    Id = authenticatedUser.Country.Id
                }
            };
        }

        public virtual string CreateBackupFileName(string directoryLocalPath)
        {
            var splitPath = directoryLocalPath.Trim('/').Split('/');

            var fileName = "";

            foreach (var pathname in splitPath)
            {
                fileName = $"{fileName}{pathname}_";
            }

            return $"{fileName}{DateTime.Now:yyyyMMddhhmmss}.zip";
        }

        /// <summary>
        /// Returns an IList of the original BaseEntities that are missing from a Dto.
        /// </summary>
        public virtual List<TBaseEntity> GetEntitiesToDelete<TBaseEntity, TBaseEntityDto>(IEnumerable<TBaseEntity> originalCollection, IList<TBaseEntityDto> newCollection) where TBaseEntity : BaseEntity where TBaseEntityDto : BaseEntityDto
        {
            return originalCollection.Where(o => newCollection.All(n => n.Id != o.Id)).ToList();
        }

        public virtual List<TBaseEntity> GetEntitiesToUpdate<TBaseEntity, TBaseEntityDto>(IEnumerable<TBaseEntity> originalCollection, IList<TBaseEntityDto> newCollection) where TBaseEntity : BaseEntity where TBaseEntityDto : BaseEntityDto
        {
            return originalCollection.Where(o => newCollection.Any(n => n.Id == o.Id)).ToList();
        }

        public virtual List<TBaseEntityDto> GetEntitiesToAdd<TBaseEntity, TBaseEntityDto>(IEnumerable<TBaseEntity> originalCollection, IList<TBaseEntityDto> newCollection) where TBaseEntity : BaseEntity where TBaseEntityDto : BaseEntityDto
        {
            return newCollection.Where(o => newCollection.All(n => n.Id != o.Id)).ToList();
        }
    }
}