#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminContentSectionService : IAdminContentSectionService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminContentSectionService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual void AddOrUpdate(CmsDbContext dbContext, ContentSectionDto contentSectionDto)
        {
            var contentSection = dbContext.ContentSections.Find(contentSectionDto.Id);

            if (contentSection == null)
            {
                contentSection = new ContentSection
                {
                    Id = contentSectionDto.Id,
                    Name = contentSectionDto.Name,
                    Schema = contentSectionDto.Schema,
                    IsDisabled = contentSectionDto.IsDisabled ?? false,
                    ContentSectionLocalizedKits = contentSectionDto.ContentSectionLocalizedKits.Select(contentSectionLocalizedKitDto => new ContentSectionLocalizedKit
                    {
                        Id = contentSectionLocalizedKitDto.Id,
                        Country = dbContext.Countries.Find(contentSectionLocalizedKitDto.Country?.Id),
                        Content = contentSectionLocalizedKitDto.Content
                    }).ToList()
                };

                dbContext.ContentSections.Add(contentSection);
            }
            else
            {
                contentSection.Name = contentSectionDto.Name;
                contentSection.Schema = contentSectionDto.Schema;
                contentSection.IsDisabled = contentSectionDto.IsDisabled ?? false;

                foreach (var contentSectionLocalizedKitDto in contentSectionDto.ContentSectionLocalizedKits)
                {
                    var contentSectionLocalizedKit = contentSection.ContentSectionLocalizedKits.FirstOrDefault(x => x.Id == contentSectionLocalizedKitDto.Id);

                    if (contentSectionLocalizedKit == null)
                    {
                        contentSectionLocalizedKit = new ContentSectionLocalizedKit
                        {
                            Id = contentSectionLocalizedKitDto.Id,
                            ContentSection = contentSection,
                            Content = contentSectionLocalizedKitDto.Content,
                            Country = dbContext.Countries.Find(contentSectionLocalizedKitDto.Country?.Id),
                            IsDisabled = contentSectionLocalizedKitDto.IsDisabled ?? false
                        };

                        contentSection.ContentSectionLocalizedKits.Add(contentSectionLocalizedKit);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(contentSectionLocalizedKit.Content) && contentSectionLocalizedKit.Content != contentSectionLocalizedKitDto.Content)
                        {
                            contentSectionLocalizedKit.ContentVersions.Add(new ContentVersion
                            {
                                Content = contentSectionLocalizedKit.Content,
                                ContentSectionLocalizedKit = contentSectionLocalizedKit
                            });
                        }

                        contentSectionLocalizedKit.Content = contentSectionLocalizedKitDto.Content;
                        contentSectionLocalizedKit.IsDisabled = contentSectionLocalizedKitDto.IsDisabled ?? false;
                    }
                }
            }

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(contentSection.ContentSectionLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.ContentSectionLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();
        }
    }
}
