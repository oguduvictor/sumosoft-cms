#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminPostsService : IAdminPostsService
    {
        public virtual BlogPost AddOrUpdate(CmsDbContext dbContext, BlogPostDto blogPostDto)
        {
            var blogPost = dbContext.BlogPosts.Find(blogPostDto.Id);

            if (blogPost == null)
            {
                blogPost = new BlogPost
                {
                    Id = blogPostDto.Id,
                    Title = blogPostDto.Title,
                    Content = blogPostDto.Content,
                    IsPublished = blogPostDto.IsPublished,
                    PublicationDate = blogPostDto.PublicationDate,
                    Author = dbContext.Users.Find(blogPostDto.Author?.Id),
                    FeaturedImage = blogPostDto.FeaturedImage,
                    FeaturedImageHorizontalCrop = blogPostDto.FeaturedImageHorizontalCrop,
                    FeaturedImageVerticalCrop = blogPostDto.FeaturedImageVerticalCrop,
                    Url = blogPostDto.Url,
                    MetaTitle = blogPostDto.MetaTitle,
                    MetaDescription = blogPostDto.MetaDescription,
                    Excerpt = blogPostDto.Excerpt,
                    IsDisabled = blogPostDto.IsDisabled ?? false,
                    BlogCategories = blogPostDto.BlogCategories.Select(bc => dbContext.BlogCategories.Find(bc.Id)).ToList(),
                    BlogComments = blogPostDto.BlogComments.Select(bc => dbContext.BlogComments.Find(bc.Id)).ToList()
                };

                dbContext.BlogPosts.Add(blogPost);
            }
            else
            {
                blogPost.Title = blogPostDto.Title;
                blogPost.Content = blogPostDto.Content;
                blogPost.IsPublished = blogPostDto.IsPublished;
                blogPost.PublicationDate = blogPostDto.PublicationDate;
                blogPost.Author = dbContext.Users.Find(blogPostDto.Author?.Id);
                blogPost.FeaturedImage = blogPostDto.FeaturedImage;
                blogPost.FeaturedImageHorizontalCrop = blogPostDto.FeaturedImageHorizontalCrop;
                blogPost.FeaturedImageVerticalCrop = blogPostDto.FeaturedImageVerticalCrop;
                blogPost.Url = blogPostDto.Url;
                blogPost.MetaTitle = blogPostDto.MetaTitle;
                blogPost.MetaDescription = blogPostDto.MetaDescription;
                blogPost.Excerpt = blogPostDto.Excerpt;
                blogPost.IsDisabled = blogPostDto.IsDisabled ?? false;
                blogPost.BlogCategories.Clear();
                blogPost.BlogCategories = blogPostDto.BlogCategories.Select(bc => dbContext.BlogCategories.Find(bc.Id)).ToList();
                blogPost.BlogComments.Clear();
                blogPost.BlogComments = blogPostDto.BlogComments.Select(bc => dbContext.BlogComments.Find(bc.Id)).ToList();
            }

            dbContext.SaveChanges();

            return blogPost;
        }
    }
}
