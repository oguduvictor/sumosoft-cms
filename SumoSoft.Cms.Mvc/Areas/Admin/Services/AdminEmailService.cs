#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminEmailService : IAdminEmailService
    {
        private readonly IAdminUtilityService adminUtilityService;

        public AdminEmailService(IAdminUtilityService adminUtilityService)
        {
            this.adminUtilityService = adminUtilityService;
        }

        public virtual ContentSection AddContentSection(CmsDbContext dbContext, ContentSectionDto contentSectionDto)
        {
            var contentSection = new ContentSection
            {
                Id = contentSectionDto.Id,
                Name = contentSectionDto.Name?.Trim(),
                IsDisabled = contentSectionDto.IsDisabled ?? false,
                ContentSectionLocalizedKits = contentSectionDto.ContentSectionLocalizedKits.Select(contentSectionLocalizedKitDto => new ContentSectionLocalizedKit
                {
                    Id = contentSectionLocalizedKitDto.Id,
                    Country = dbContext.Countries.Find(contentSectionLocalizedKitDto.Country?.Id),
                    Content = contentSectionLocalizedKitDto.Content?.Trim()
                }).ToList()
            };

            dbContext.ContentSections.Add(contentSection);

            // -----------------------------------------------------------------------------
            // Resolve multiple localizedKits associated to the same country
            // -----------------------------------------------------------------------------

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(contentSection.ContentSectionLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.ContentSectionLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return contentSection;
        }

        public virtual Email AddOrUpdate(CmsDbContext dbContext, EmailDto emailDto)
        {
            var email = dbContext.Emails.Find(emailDto.Id);

            if (email == null)
            {
                email = new Email
                {
                    Id = emailDto.Id,
                    IsDisabled = emailDto.IsDisabled ?? false,
                    Name = emailDto.Name,
                    ContentSection = dbContext.ContentSections.Find(emailDto.ContentSection?.Id),
                    ViewName = emailDto.ViewName,
                    EmailLocalizedKits = emailDto.EmailLocalizedKits.Select(emailLocalizedKitDto => new EmailLocalizedKit
                    {
                        Id = emailLocalizedKitDto.Id,
                        Email = email,
                        Country = dbContext.Countries.Find(emailLocalizedKitDto.Country?.Id),
                        IsDisabled = emailLocalizedKitDto.IsDisabled ?? false,
                        From = emailLocalizedKitDto.From,
                        ReplyTo = emailLocalizedKitDto.ReplyTo,
                        DisplayName = emailLocalizedKitDto.DisplayName
                    }).ToList()
                };

                dbContext.Emails.Add(email);
            }
            else
            {
                email.IsDisabled = emailDto.IsDisabled ?? false;
                email.Name = emailDto.Name;
                email.ContentSection = dbContext.ContentSections.Find(emailDto.ContentSection?.Id);
                email.ViewName = emailDto.ViewName;

                foreach (var emailLocalizedKitDto in emailDto.EmailLocalizedKits)
                {
                    var emailLocalizedKit = dbContext.EmailLocalizedKits.Find(emailLocalizedKitDto.Id);

                    if (emailLocalizedKit == null)
                    {
                        emailLocalizedKit = new EmailLocalizedKit
                        {
                            Id = emailLocalizedKitDto.Id,
                            Email = email,
                            Country = dbContext.Countries.Find(emailLocalizedKitDto.Country?.Id),
                            IsDisabled = emailLocalizedKitDto.IsDisabled ?? false,
                            From = emailLocalizedKitDto.From,
                            ReplyTo = emailLocalizedKitDto.ReplyTo,
                            DisplayName = emailLocalizedKitDto.DisplayName
                        };

                        email.EmailLocalizedKits.Add(emailLocalizedKit);
                    }
                    else
                    {
                        emailLocalizedKit.IsDisabled = emailLocalizedKitDto.IsDisabled ?? false;
                        emailLocalizedKit.From = emailLocalizedKitDto.From;
                        emailLocalizedKit.ReplyTo = emailLocalizedKitDto.ReplyTo;
                        emailLocalizedKit.DisplayName = emailLocalizedKitDto.DisplayName;
                    }
                }
            }

            var duplicatedLocalizedKitsToDelete = this.adminUtilityService.GetDuplicatedLocalizedKitsToDelete(email.EmailLocalizedKits);

            foreach (var localizedKit in duplicatedLocalizedKitsToDelete)
            {
                dbContext.EmailLocalizedKits.Delete(localizedKit.Id, false);
            }

            dbContext.SaveChanges();

            return email;
        }
    }
}
