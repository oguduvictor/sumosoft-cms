#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Services
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;

    public class AdminCouponService : IAdminCouponService
    {
        public virtual Coupon AddOrUpdate(CmsDbContext dbContext, CouponDto couponDto, int quantity = 1)
        {
            var coupon = dbContext.Coupons.Include(x => x.Country).FirstOrDefault(x => x.Id == couponDto.Id);

            if (coupon == null)
            {
                for (var i = 0; i < quantity; i++)
                {
                    coupon = new Coupon
                    {
                        Code = Guid.NewGuid().ToString().ToUpper().Shorten(6),
                        Amount = couponDto.Amount,
                        Percentage = couponDto.Percentage,
                        Country = dbContext.Countries.Find(couponDto.Country?.Id),
                        Published = couponDto.Published,
                        AllowOnSale = couponDto.AllowOnSale,
                        ExpirationDate = couponDto.ExpirationDate,
                        ValidityTimes = couponDto.ValidityTimes,
                        Categories = couponDto.Categories.Select(c => dbContext.Categories.Find(c.Id)).ToList(),
                        Referee = dbContext.Users.Find(couponDto.Referee?.Id),
                        RefereeReward = couponDto.RefereeReward,
                        Recipient = couponDto.Recipient,
                        Note = couponDto.Note
                    };

                    dbContext.Coupons.Add(coupon);
                }
            }
            else
            {
                coupon.Code = couponDto.Code;
                coupon.Percentage = couponDto.Percentage;
                coupon.Country = dbContext.Countries.Find(couponDto.Country?.Id);
                coupon.Amount = couponDto.Amount;
                coupon.RefereeReward = couponDto.RefereeReward;
                coupon.Referee = dbContext.Users.Find(couponDto.Referee?.Id);
                coupon.Recipient = couponDto.Recipient;
                coupon.ExpirationDate = couponDto.ExpirationDate;
                coupon.ValidityTimes = couponDto.ValidityTimes;
                coupon.Note = couponDto.Note;
                coupon.Published = couponDto.Published;
                coupon.AllowOnSale = couponDto.AllowOnSale;

                coupon.Categories.Clear();
                coupon.Categories = couponDto.Categories.Select(c => dbContext.Categories.Find(c.Id)).ToList();
            }

            dbContext.SaveChanges();

            return coupon;
        }
    }
}
