﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class AttributeListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<AttributeDto> Attributes { get; set; }
    }
}