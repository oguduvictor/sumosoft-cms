﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditProductImageKitsDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<ProductImageKitDto> ProductImageKits { get; set; } = new List<ProductImageKitDto>();

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }
    }
}