﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class ProductListDto : PaginatedListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<CategoryDto> AllCategories { get; set; }

        public List<ProductDto> Products { get; set; } = new List<ProductDto>();

        public List<string> LocalizedCountries { get; set; } = new List<string>();
    }
}