﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class OrderListDto : PaginatedListDto
    {
        [NotNull]
        public List<OrderDto> Orders { get; set; } = new List<OrderDto>();

        [NotNull]
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();
    }
}