﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class EditCountryDto
    {
        public List<CountryDto> AllLocalizedCountries { get; set; }

        public List<CategoryDto> AllCategories { get; set; }

        public UserDto AuthenticatedUser { get; set; }

        public CountryDto Country { get; set; }

        public IEnumerable<string> FlagIcons { get; set; }

        public IEnumerable<string> Cultures { get; set; }
    }
}