﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditOrderDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public OrderDto Order { get; set; }

        [NotNull]
        public List<EmailDto> AllEmails { get; set; } = new List<EmailDto>();
    }
}