﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using SumoSoft.Cms.Domain.Dto;

    public class SendEmailDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<CountryDto> AllCountries { get; set; } = new List<CountryDto>();

        public List<MailingListDto> AllMailingLists { get; set; } = new List<MailingListDto>();

        public List<UserDto> AllUsers { get; set; } = new List<UserDto>();
    }
}