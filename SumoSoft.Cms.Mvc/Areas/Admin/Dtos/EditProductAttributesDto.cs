﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditProductAttributesDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<ProductAttributeDto> ProductAttributes { get; set; } = new List<ProductAttributeDto>();

        public List<AttributeDto> AllAttributes { get; set; } = new List<AttributeDto>();

        public List<ContentSectionDto> AllContentSections { get; set; } = new List<ContentSectionDto>();

        public List<string> ContentSectionsSchemaNames { get; set; } = new List<string>();

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }
    }
}