﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class EditShippingBoxDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public ShippingBoxDto ShippingBox { get; set; }

        public List<CountryDto> AllCountries { get; set; }

        public List<ProductDto> AllProducts { get; set; }
    }
}