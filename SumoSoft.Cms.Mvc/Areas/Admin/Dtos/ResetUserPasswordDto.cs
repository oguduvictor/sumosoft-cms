﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using SumoSoft.Cms.Domain.Dto;

    public class ResetUserPasswordDto : BaseEntityDto
    {
        public Guid UserId { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}