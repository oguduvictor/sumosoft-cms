﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditCouponDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public CouponDto Coupon { get; set; }

        public int Quantity { get; set; }

        [NotNull]
        public List<CategoryDto> AllCategories { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<CountryDto> AllCountries { get; set; } = new List<CountryDto>();

        [NotNull]
        public List<UserDto> AllUsers { get; set; } = new List<UserDto>();

    }
}