﻿
#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    public class PaginatedListDto
    {
        public int Page { get; set; } = 1;

        public int PageSize { get; set; }

        public int TotalItems { get; set; }

        public int TotalPages => this.TotalItems >= this.PageSize ? (this.TotalItems + this.PageSize - 1) / this.PageSize : 1;
    }
}