﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditProductDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public ProductDto Product { get; set; }

        public List<string> AllTags { get; set; }

        [NotNull]
        public List<ShippingBoxDto> AllShippingBoxes { get; set; } = new List<ShippingBoxDto>();

        [NotNull]
        public List<ProductDto> AllProducts { get; set; } = new List<ProductDto>();

        [NotNull]
        public List<CategoryDto> AllCategories { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<ProductImageKitDto> AllProductImageKits { get; set; } = new List<ProductImageKitDto>();
    }
}