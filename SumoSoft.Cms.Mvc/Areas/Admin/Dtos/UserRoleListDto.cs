﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class UserRoleListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<UserRoleDto> UserRoles { get; set; } = new List<UserRoleDto>();
    }
}