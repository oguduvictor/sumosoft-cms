﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class EditVariantOptionDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<string> AllTags { get; set; }

        public VariantOptionDto VariantOption { get; set; }
    }
}