﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditProductStockUnitsDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<ProductStockUnitDto> ProductStockUnits { get; set; } = new List<ProductStockUnitDto>();

        /// <summary>
        /// The Variants affecting the stock.
        /// </summary>
        public List<VariantDto> Variants { get; set; }

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }
    }
}