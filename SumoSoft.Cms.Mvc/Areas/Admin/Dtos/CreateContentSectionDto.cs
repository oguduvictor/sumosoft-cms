﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Dto;

    public class CreateContentSectionDto
    {
        public FormResponse FormResponse { get; set; }

        public ContentSectionDto SimilarContentSection { get; set; }

        public ContentSectionDto SavedContentSection { get; set; }
    }
}