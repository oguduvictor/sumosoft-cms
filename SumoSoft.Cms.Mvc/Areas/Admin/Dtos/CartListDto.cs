﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class CartListDto : PaginatedListDto
    {
        [NotNull]
        public List<CartDto> Carts { get; set; } = new List<CartDto>();

        [NotNull]
        public List<ShippingBoxDto> ShippingBoxes { get; set; } = new List<ShippingBoxDto>();

        [NotNull]
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();
    }
}