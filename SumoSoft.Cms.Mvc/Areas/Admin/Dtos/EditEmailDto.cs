﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditEmailDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public EmailDto Email { get; set; }

        [NotNull]
        public List<string> ViewNames { get; set; } = new List<string>();

        public List<ContentSectionDto> AllContentSections { get; set; } = new List<ContentSectionDto>();

        public List<string> ContentSectionsSchemaNames { get; set; } = new List<string>();
    }
}