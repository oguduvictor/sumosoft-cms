﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;
    using System.Collections.Generic;

    public class EditCartDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public CartDto Cart { get; set; }

        [NotNull]
        public List<EmailDto> AllEmails { get; set; } = new List<EmailDto>();
    }
}