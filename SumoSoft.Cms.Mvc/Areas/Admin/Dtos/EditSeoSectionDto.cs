﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using SumoSoft.Cms.Domain.Dto;

    public class EditSeoSectionDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public SeoSectionDto SeoSection { get; set; }
    }
}