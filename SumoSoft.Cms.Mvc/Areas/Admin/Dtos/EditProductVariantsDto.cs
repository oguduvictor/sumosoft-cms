﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditProductVariantsDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<ProductVariantDto> ProductVariants { get; set; } = new List<ProductVariantDto>();

        [NotNull]
        public List<VariantDto> AllVariants { get; set; } = new List<VariantDto>();

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductLocalizedName { get; set; }
    }
}