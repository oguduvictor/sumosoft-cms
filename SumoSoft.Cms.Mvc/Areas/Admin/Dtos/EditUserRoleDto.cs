﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using SumoSoft.Cms.Domain.Dto;

    public class EditUserRoleDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public UserRoleDto UserRole { get; set; }
    }
}