﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class CouponListDto : PaginatedListDto
    {
        public List<CouponDto> Coupons { get; set; }
    }
}