﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using JetBrains.Annotations;

    public class LoginDetailsDto
    {
        [CanBeNull]
        public string Email { get; set; }

        [CanBeNull]
        public string Password { get; set; }
    }
}