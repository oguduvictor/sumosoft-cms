﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditContentSectionsDto : PaginatedListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<ContentSectionDto> ContentSections { get; set; } = new List<ContentSectionDto>();

        [NotNull]
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();

        [NotNull]
        public List<string> ContentSectionsSchemaNames { get; set; } = new List<string>();
    }
}