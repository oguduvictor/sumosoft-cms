﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    public class SchemaPropertyDto
    {
        /// <summary>
        /// The property Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The property Display Name, which will be shown in the Admin panel.
        /// </summary>
        /// <example>[Display(Name = "My name")].</example>
        public string DisplayName { get; set; }

        /// <summary>
        /// The property Display Description, which will be shown in the Admin panel.
        /// </summary>
        /// <example>[Display(Description = "My description")].</example>
        public string DisplayDescription { get; set; }

        /// <summary>
        /// string | string[] | string[][].
        /// </summary>
        public string Type { get; set; }
    }
}