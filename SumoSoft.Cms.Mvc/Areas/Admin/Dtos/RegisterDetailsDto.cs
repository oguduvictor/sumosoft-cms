﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using JetBrains.Annotations;

    public class RegisterDetailsDto
    {
        [CanBeNull]
        public string FirstName { get; set; }

        [CanBeNull]
        public string LastName { get; set; }

        [CanBeNull]
        public string Email { get; set; }

        [CanBeNull]
        public string Password { get; set; }
    }
}