﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;

    public class BatchRenameFilesDto
    {
        public string DirectoryLocalPath { get; set; }

        public List<string> LocalPaths { get; set; }

        public string ReplaceText { get; set; }

        public string ReplaceTextTarget { get; set; }

        public string PrefixText { get; set; }

        public string SuffixText { get; set; }
    }
}