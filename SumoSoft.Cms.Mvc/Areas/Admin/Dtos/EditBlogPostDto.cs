﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class EditBlogPostDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public BlogPostDto BlogPost { get; set; }

        public List<UserDto> Users { get; set; }

        public List<BlogCategoryDto> AllBlogCategories { get; set; }

        [CanBeNull]
        public string PostPreviewLink { get; set; }
    }
}