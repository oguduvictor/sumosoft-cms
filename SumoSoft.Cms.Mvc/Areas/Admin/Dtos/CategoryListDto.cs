﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class CategoryListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();
    }
}