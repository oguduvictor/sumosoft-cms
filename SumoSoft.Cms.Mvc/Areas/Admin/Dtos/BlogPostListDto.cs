﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class BlogPostListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<BlogPostDto> BlogPosts { get; set; }
    }
}