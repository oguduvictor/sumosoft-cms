﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class EditVariantDto
    {
        public List<string> AllVariantSelectors { get; set; }

        public UserDto AuthenticatedUser { get; set; }

        public VariantDto Variant { get; set; }
    }
}