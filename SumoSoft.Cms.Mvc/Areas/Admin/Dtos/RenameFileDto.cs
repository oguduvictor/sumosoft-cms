﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    public class RenameFileDto
    {
        public string LocalPath { get; set; }

        public string NewName { get; set; }
    }
}