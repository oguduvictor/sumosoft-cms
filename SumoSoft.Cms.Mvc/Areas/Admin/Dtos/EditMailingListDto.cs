﻿namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using SumoSoft.Cms.Domain.Dto;

    public class EditMailingListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public MailingListDto MailingList { get; set; }
    }
}