﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Dto;

    public class EditAttributeDto
    {
        public UserDto AuthenticatedUser { get; set; }

        public List<string> AllTags { get; set; }

        public AttributeDto Attribute { get; set; }

        public AttributeOptionDto NewAttributeOption { get; set; }
    }
}