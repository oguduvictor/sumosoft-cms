﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Areas.Admin.Dtos
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Dto;

    public class UserListDto : PaginatedListDto
    {
        public UserDto AuthenticatedUser { get; set; }

        [NotNull]
        public List<UserDto> Users { get; set; } = new List<UserDto>();
    }
}