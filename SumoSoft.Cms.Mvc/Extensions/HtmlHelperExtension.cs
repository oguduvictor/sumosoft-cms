﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public static class HtmlHelperExtension
    {
        // -----------------------------------------------------------------
        // HtmlHelpers will use the Request DbContext in otder to
        // allow lazy loading on the cshtml Views
        // -----------------------------------------------------------------

        private static CmsDbContext _requestDbContext => DependencyResolver.Current.GetService<IRequestDbContextService>().Get();

        private static ICmsService _cmsService => DependencyResolver.Current.GetService<ICmsService>();

        private static ILocalStorageService _localStorageService => DependencyResolver.Current.GetService<ILocalStorageService>();

        private static ICountryService _countryService => DependencyResolver.Current.GetService<ICountryService>();

        private static IContentSectionService _contentSectionService => DependencyResolver.Current.GetService<IContentSectionService>();

        private static IAuthenticationService _authenticationService => DependencyResolver.Current.GetService<IAuthenticationService>();

        private static IUtilityService _utilityService => DependencyResolver.Current.GetService<IUtilityService>();

        [CanBeNull]
        public static Cart GetCart(this HtmlHelper htmlHelper)
        {
            return _cmsService.GetCart(_requestDbContext);
        }

        [NotNull]
        public static Country GetCurrentCountry(this HtmlHelper htmlHelper)
        {
            return _countryService.GetCurrentCountry(_requestDbContext);
        }

        [NotNull]
        public static List<Country> GetLocalizedCountries(this HtmlHelper htmlHelper)
        {
            return _requestDbContext.Countries.Where(x => x.Localize && !x.IsDisabled).OrderBy(x => x.Name).ToList();
        }

        public static MvcHtmlString ContentSection(this HtmlHelper htmlHelper, string contentSectionName, string fallbackValue)
        {
            return _contentSectionService.ContentSection(_requestDbContext, contentSectionName, fallbackValue).ToMvcHtmlString();
        }

        public static MvcHtmlString ContentSection(this HtmlHelper htmlHelper, string contentSectionName)
        {
            return ContentSection(htmlHelper, contentSectionName, string.Empty);
        }

        public static T ContentSectionAs<T>(this HtmlHelper htmlHelper, string contentSectionName)
        {
            return _contentSectionService.ContentSectionAs<T>(_requestDbContext, contentSectionName);
        }

        [NotNull]
        public static string AppendVersionToFile(this HtmlHelper htmlHelper, string localPath)
        {
            return _localStorageService.GetFileDto(localPath)?.LocalPathWithVersion ?? string.Empty;
        }

        [CanBeNull]
        public static User GetAuthenticatedUser(this HtmlHelper htmlHelper)
        {
            return _authenticationService.GetAuthenticatedUser(_requestDbContext);
        }

        public static string GetAuthenticatedUserId(this HtmlHelper htmlHelper)
        {
            return _authenticationService.GetAuthenticatedUserIdFromCookie()?.ToString();
        }

        [NotNull]
        public static string GetAppSetting(this HtmlHelper htmlHelper, string key, string fallbackValue = "")
        {
            return _utilityService.GetAppSetting(key, fallbackValue);
        }

        /// <summary>
        /// Generates a url Sitemap Tag. If the multilanguage setting of the Cms is enabled, it also creates the rel='alternate' tags for each country.
        /// </summary>
        public static MvcHtmlString SitemapUrl(this HtmlHelper htmlHelper, string domainUrl, string pageUrl)
        {
            var sitemapUrl = new StringBuilder();

            const string tabulation = "    ";

            var isMultiLanguage = _requestDbContext.Countries.Count(x => !x.IsDisabled) > 1;

            if (isMultiLanguage)
            {
                var countries = _requestDbContext.Countries.Where(x => !x.IsDisabled && x.Localize).ToList();

                foreach (var country in countries)
                {
                    sitemapUrl.Append($"<url>{Environment.NewLine}");

                    sitemapUrl.Append($"{tabulation}<loc>{domainUrl}{country.GetUrlForStringConcatenation()}{pageUrl}</loc>{Environment.NewLine}");

                    foreach (var countryBis in countries)
                    {
                        sitemapUrl.Append($"{tabulation}<xhtml:link rel='alternate' hreflang='{countryBis.LanguageCode}' href='{domainUrl}{countryBis.GetUrlForStringConcatenation()}{pageUrl}'/>{Environment.NewLine}");
                    }

                    sitemapUrl.Append($"</url>{Environment.NewLine}");
                }
            }
            else
            {
                sitemapUrl.Append($"<url>{Environment.NewLine}");

                sitemapUrl.Append($"{tabulation}<loc>{domainUrl}/{pageUrl}</loc>{Environment.NewLine}");

                sitemapUrl.Append($"</url>{Environment.NewLine}");
            }

            return new MvcHtmlString(sitemapUrl.ToString());
        }
    }
}