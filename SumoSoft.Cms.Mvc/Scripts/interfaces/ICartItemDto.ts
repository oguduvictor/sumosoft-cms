﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICartShippingBoxDto } from "./ICartShippingBoxDto";
import { ICartItemVariantDto } from "./ICartItemVariantDto";
import { IProductDto } from "./IProductDto";
import { ITaxDto } from './ITaxDto';

export interface ICartItemDto extends IBaseEntityDto {
    cartShippingBox: ICartShippingBoxDto;
    product: IProductDto;
    quantity: number;
    image: string;
    cartItemVariants: Array<ICartItemVariantDto>;
    minEta: string;
    maxEta: string;
    couponApplies: boolean;
    subtotalBeforeTax: number;
    subtotalAfterTax: number;
    subtotalBeforeTaxWithoutSalePrice: number;
    subtotalAfterTaxWithoutSalePrice: number;
    totalBeforeTax: number;
    totalAfterTax: number;
    taxesOverride: Array<ITaxDto>;
}