﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductDto } from "./IProductDto";
import { IVariantDto } from "./IVariantDto";
import { IVariantOptionDto } from "./IVariantOptionDto";

export interface IProductVariantDto extends IBaseEntityDto {
    product: IProductDto;
    variant: IVariantDto;
    variantOptions: Array<IVariantOptionDto>;
    defaultBooleanValue: boolean;
    defaultDoubleValue: number;
    defaultIntegerValue: number;
    defaultStringValue: string;
    defaultJsonValue: string;
    defaultVariantOptionValue: IVariantOptionDto;
}