﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICartItemDto } from './ICartItemDto';
import { IUserDto } from './IUserDto';

export interface IWishListDto extends IBaseEntityDto {
    user: IUserDto;
    cartItems: Array<ICartItemDto>;
}