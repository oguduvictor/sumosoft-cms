﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IOrderCouponDto } from "./IOrderCouponDto";
import { IOrderCreditDto } from "./IOrderCreditDto";
import { ISentEmailDto } from "./ISentEmailDto";
import { IOrderShippingBoxDto } from "./IOrderShippingBoxDto";
import { IOrderAddressDto } from "./IOrderAddressDto";
import { OrderStatusEnum } from "../enums/OrderStatusEnum";

export interface IOrderDto extends IBaseEntityDto {
    orderNumber: number;
    status: OrderStatusEnum;
    tags: string;
    statusDescriptions: Array<string>;
    countryName: string;
    countryLanguageCode: string;
    currencySymbol: string;
    iso4217CurrencySymbol: string;
    userId: string;
    userFirstName: string;
    userLastName: string;
    userEmail: string;
    transactionId: string;
    orderShippingBoxes: Array<IOrderShippingBoxDto>;
    shippingAddress: IOrderAddressDto;
    billingAddress: IOrderAddressDto;
    comments: string;
    sentEmails: Array<ISentEmailDto>;
    orderCoupon: IOrderCouponDto;
    orderCredit: IOrderCreditDto;
    totalBeforeTax: number;
    totalAfterTax: number;
    totalToBePaid: number;
    totalPaid: number;
}