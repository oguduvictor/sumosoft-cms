﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICartItemDto } from "./ICartItemDto";
import { IVariantDto } from "./IVariantDto";
import { IVariantOptionDto } from "./IVariantOptionDto";

export interface ICartItemVariantDto extends IBaseEntityDto {
    cartItem: ICartItemDto;
    variant: IVariantDto;
    booleanValue: boolean;
    doubleValue: number;
    integerValue: number;
    stringValue: string;
    jsonValue: string;
    variantOptionValue: IVariantOptionDto;
    priceModifier: number;
}