﻿import { ICountryDto } from "./ICountryDto";
import { ISeoSectionDto } from "./ISeoSectionDto";
import { IBaseEntityDto } from './IBaseEntityDto';
import { ILocalizedKitDto } from './ILocalizedKitDto';

export interface ISeoSectionLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    id: string;
    country: ICountryDto;
    seoSection: ISeoSectionDto;
    title: string;
    description: string;
}