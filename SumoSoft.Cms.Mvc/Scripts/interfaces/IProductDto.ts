﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductVariantDto } from "./IProductVariantDto";
import { IProductAttributeDto } from "./IProductAttributeDto";
import { IShippingBoxDto } from "./IShippingBoxDto";
import { ICategoryDto } from "./ICategoryDto";
import { IProductStockUnitDto } from "./IProductStockUnitDto";
import { IProductImageKitDto } from "./IProductImageKitDto";
import { IProductLocalizedKitDto } from "./IProductLocalizedKitDto";

export interface IProductDto extends IBaseEntityDto {
    name: string;
    url: string;
    code: string;
    taxCode: string;
    comments: string;
    tags: string;
    categories: Array<ICategoryDto>;
    productAttributes: Array<IProductAttributeDto>;
    productVariants: Array<IProductVariantDto>;
    productStockUnits: Array<IProductStockUnitDto>;
    productImageKits: Array<IProductImageKitDto>;
    shippingBoxes: Array<IShippingBoxDto>;
    relatedProducts: Array<IProductDto>;
    productLocalizedKits: Array<IProductLocalizedKitDto>;
    localizedTitle: string;
    localizedDescription: string;
    localizedMetaTitle: string;
    localizedMetaDescription: string;
}
