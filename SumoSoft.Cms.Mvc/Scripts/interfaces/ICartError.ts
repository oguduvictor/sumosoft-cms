﻿import { CartErrorCode } from '../enums/CartErrorCode';

export interface ICartError {
	code: CartErrorCode;
	message: string;
	targetId: string;
}
