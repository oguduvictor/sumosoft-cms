﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IBlogCategoryDto } from "./IBlogCategoryDto";
import { ILocalizedKitDto } from "./ILocalizedKitDto";

export interface IBlogCategoryLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    blogCategory: IBlogCategoryDto;
    country: ICountryDto;
    title: string;
    description: string;
}