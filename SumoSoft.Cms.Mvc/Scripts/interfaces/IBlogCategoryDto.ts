﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IBlogPostDto } from "./IBlogPostDto";
import { IBlogCategoryLocalizedKitDto } from "./IBlogCategoryLocalizedKitDto";

export interface IBlogCategoryDto extends IBaseEntityDto {
    name: string;
    url: string;
    blogPosts: Array<IBlogPostDto>;
    blogCategoryLocalizedKits: Array<IBlogCategoryLocalizedKitDto>;
    localizedTitle: string;
    localizedDescription: string;
}