﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IAttributeDto } from "./IAttributeDto";
import { IAttributeOptionLocalizedKitDto } from "./IAttributeOptionLocalizedKitDto";

export interface IAttributeOptionDto extends IBaseEntityDto {
    attribute: IAttributeDto;
    name: string;
    url: string;
    tags: string;
    attributeOptionLocalizedKits: Array<IAttributeOptionLocalizedKitDto>;
    localizedTitle: string;
}
