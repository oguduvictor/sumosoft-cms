﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IAttributeDto } from "./IAttributeDto";
import { ILocalizedKitDto } from "./ILocalizedKitDto";

export interface IAttributeLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    attribute: IAttributeDto;
    title: string;
    description: string;
}
