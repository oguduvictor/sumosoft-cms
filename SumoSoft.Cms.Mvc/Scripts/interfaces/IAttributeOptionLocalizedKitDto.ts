﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IAttributeOptionDto } from "./IAttributeOptionDto";
import { ILocalizedKitDto } from "./ILocalizedKitDto";

export interface IAttributeOptionLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    attributeOption: IAttributeOptionDto;
    title: string;
    description: string;
}
