﻿export interface IBraintreePaymentMethodBinData {
    commercial: string;
    countryOfIssuance: string;
    debit: string;
    durbinRegulated: string;
    healthcare: string;
    issuingBank: string;
    payroll: string;
    prepaid: string;
    productId: string;
}