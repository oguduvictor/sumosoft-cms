﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductDto } from "./IProductDto";
import { IShippingBoxLocalizedKitDto } from "./IShippingBoxLocalizedKitDto";

export interface IShippingBoxDto extends IBaseEntityDto {
    sortOrder: number;
    name: string;
    products: Array<IProductDto>;
    shippingBoxLocalizedKits: Array<IShippingBoxLocalizedKitDto>;
    requiresShippingAddress: boolean;
    localizedTitle: string;
    localizedDescription: string;
    localizedInternalDescription: string;
    localizedPrice: number;
    localizedMinDays: number;
    localizedMaxDays: number;
    localizedCountWeekends: boolean;
    localizedFreeShippingMinimumPrice: number;
    localizedIsDisabled: boolean;
}