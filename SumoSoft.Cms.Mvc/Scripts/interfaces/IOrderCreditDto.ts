﻿import { IBaseEntityDto } from "./IBaseEntityDto";

export interface IOrderCreditDto extends IBaseEntityDto {
    amount: number;
    reference: string;
}