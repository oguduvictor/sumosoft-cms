﻿import { IBaseEntityDto } from "./IBaseEntityDto";

export interface IOrderCouponDto extends IBaseEntityDto {
    code: string;
    amount: number;
}