﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { VariantTypesEnum } from "../enums/VariantTypesEnum";
import { IOrderItemDto } from "./IOrderItemDto";

export interface IOrderItemVariantDto extends IBaseEntityDto {
    orderItem: IOrderItemDto;
    variantType: VariantTypesEnum;
    variantName: string;
    variantLocalizedTitle: string;
    booleanValue: boolean;
    doubleValue: number;
    integerValue: number;
    stringValue: string;
    jsonValue: string;
    variantOptionName: string;
    variantOptionUrl: string;
    variantOptionCode: string;
    variantOptionLocalizedTitle: string;
    variantOptionLocalizedDescription: string;
    variantOptionLocalizedPriceModifier: number;
    variantUrl: string;
    variantLocalizedDescription: string;
}