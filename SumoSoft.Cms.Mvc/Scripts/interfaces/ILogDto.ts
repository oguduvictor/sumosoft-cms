﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IUserDto } from "./IUserDto";

export interface ILogDto extends IBaseEntityDto {
    message: string;
    details: string;
    user: IUserDto;
}