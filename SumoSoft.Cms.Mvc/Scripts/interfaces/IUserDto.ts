﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IAddressDto } from "./IAddressDto";
import { ICartDto } from "./ICartDto";
import { ICountryDto } from "./ICountryDto";
import { IUserRoleDto } from "./IUserRoleDto";
import { IUserLocalizedKitDto } from "./IUserLocalizedKitDto";
import { GenderEnum } from "../enums/GenderEnum";
import { IUserAttributeDto } from "./IUserAttributeDto";
import { IUserVariantDto } from './IUserVariantDto';
import { IMailingListSubscription } from './IMailingListSubscription';

export interface IUserDto extends IBaseEntityDto {
    firstName: string;
    lastName: string;
    fullName: string;
    createdDate: string;
    email: string;
    password: string;
    role: IUserRoleDto;
    lastLogin: string;
    addresses: Array<IAddressDto>;
    gender: GenderEnum;
    genderDescriptions: Array<string>;
    loginProvider: string;
    isRegistered: boolean;
    showMembershipSalePrice: boolean;
    cart: ICartDto;
    country: ICountryDto;
    userAttributes: Array<IUserAttributeDto>;
    userLocalizedKits: Array<IUserLocalizedKitDto>;
    userVariants: Array<IUserVariantDto>;
    mailingListSubscriptions: Array<IMailingListSubscription>;
}
