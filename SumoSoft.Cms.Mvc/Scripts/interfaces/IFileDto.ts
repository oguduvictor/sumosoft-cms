﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IDirectoryDto } from "./IDirectoryDto";

export interface IFileDto extends IBaseEntityDto {
    name: string;
    extension: string;
    localPath: string;
    localPathWithVersion: string;
    absolutePath: string;
    absolutePathWithVersion: string;
    size: number;
    heavyLoadingFileSize: number;
    creationTime: string;
    useAzureStorage: boolean;
    directory: IDirectoryDto;
}