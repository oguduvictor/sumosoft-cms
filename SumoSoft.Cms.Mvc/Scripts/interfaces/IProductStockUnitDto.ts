﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IVariantOptionDto } from "./IVariantOptionDto";
import { ICategoryDto } from "./ICategoryDto";
import { IProductDto } from "./IProductDto";
import { IProductStockUnitLocalizedKitDto } from "./IProductStockUnitLocalizedKitDto";

export interface IProductStockUnitDto extends IBaseEntityDto {
    product: IProductDto;
    productStockUnitLocalizedKits: Array<IProductStockUnitLocalizedKitDto>;
    variantOptions: Array<IVariantOptionDto>;
    categories: Array<ICategoryDto>;
    dispatchDate: string;
    stock: number;
    code: string;
    enablePreorder: boolean;
    dispatchTime: number;
    isDeleted: boolean;
    localizedBasePrice: number;
    localizedSalePrice: number;
    localizedMembershipSalePrice: number;
}
