﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IContentSectionLocalizedKitDto } from "./IContentSectionLocalizedKitDto";
import { IProductAttributeDto } from "./IProductAttributeDto";

export interface IContentSectionDto extends IBaseEntityDto {
    name: string;
    schema: string;
    localizedContent: string;
    productAttributes: Array<IProductAttributeDto>;
    contentSectionLocalizedKits: Array<IContentSectionLocalizedKitDto>;
}