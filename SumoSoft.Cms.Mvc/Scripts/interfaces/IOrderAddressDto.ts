﻿import { IBaseEntityDto } from "./IBaseEntityDto";

export interface IOrderAddressDto extends IBaseEntityDto {
    addressFirstName: string;
    addressLastName: string;
    addressLine1: string;
    addressLine2: string;
    countryName: string;
    stateCountyProvince: string;
    city: string;
    postcode: string;
    phoneNumber: string;
}