﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IShippingBoxDto } from "./IShippingBoxDto";
import { ICountryDto } from "./ICountryDto";
import { ILocalizedKitDto } from './ILocalizedKitDto';

export interface IShippingBoxLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    shippingBox: IShippingBoxDto;
    title: string;
    description: string;
    internalDescription: string;
    carrier: string;
    minDays: number;
    maxDays: number;
    countWeekends: boolean;
    price: number;
    freeShippingMinimumPrice: number;
}