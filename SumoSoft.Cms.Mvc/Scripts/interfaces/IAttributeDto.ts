﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { AttributeTypesEnum } from "../enums/AttributeTypesEnum";
import { IAttributeOptionDto } from "./IAttributeOptionDto";
import { IAttributeLocalizedKitDto } from "./IAttributeLocalizedKitDto";

export interface IAttributeDto extends IBaseEntityDto {
    name: string;
    sortOrder: number;
    type: AttributeTypesEnum;
    url: string;
    typeDescriptions: Array<string>;
    localizedTitle: string;
    attributeOptions: Array<IAttributeOptionDto>;
    attributeLocalizedKits: Array<IAttributeLocalizedKitDto>;
}
