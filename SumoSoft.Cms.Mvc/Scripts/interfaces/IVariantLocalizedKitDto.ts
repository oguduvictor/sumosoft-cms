﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IVariantDto } from "./IVariantDto";
import { ILocalizedKitDto } from './ILocalizedKitDto';

export interface IVariantLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    variant: IVariantDto;
    title: string;
    description: string;
}