﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IUserDto } from "./IUserDto";
import { ICountryDto } from './ICountryDto';

export interface IAddressDto extends IBaseEntityDto {
    user: IUserDto;
    firstName: string;
    lastName: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    country: ICountryDto;
    stateCountyProvince: string;
    postcode: string;
    phoneNumber: string;
}