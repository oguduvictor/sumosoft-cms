﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductImageKitDto } from "./IProductImageKitDto";

export interface IProductImageDto extends IBaseEntityDto {
    productImageKit: IProductImageKitDto;
    name: string;
    url: string;
    altText: string;
    sortOrder: number;
}
