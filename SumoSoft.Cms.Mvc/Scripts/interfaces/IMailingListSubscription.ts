﻿import { IUserDto } from './IUserDto';
import { IMailingListDto } from './IMailingListDto';
import { MailingListSubscriptionStatusEnum } from '../enums/MailingListSubscriptionStatusEnum';

export interface IMailingListSubscription {
    user: IUserDto;
    mailingList: IMailingListDto;
    status: MailingListSubscriptionStatusEnum;
    statusDescriptions: Array<string>;
}