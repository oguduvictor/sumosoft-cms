﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IVariantDto } from "./IVariantDto";
import { IVariantOptionLocalizedKitDto } from "./IVariantOptionLocalizedKitDto";

export interface IVariantOptionDto extends IBaseEntityDto {
    name: string;
    image: string;
    url: string;
    code: string;
    sortOrder: number;
    tags: string;
    localizedIsDisabled: boolean;
    localizedTitle: string;
    localizedDescription: string;
    variant: IVariantDto;
    variantOptionLocalizedKits: Array<IVariantOptionLocalizedKitDto>;
    localizedPriceModifier: number;
}