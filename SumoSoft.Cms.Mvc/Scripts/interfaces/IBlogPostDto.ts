﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IUserDto } from "./IUserDto";
import { IBlogCategoryDto } from "./IBlogCategoryDto";
import { IBlogCommentDto } from "./IBlogCommentDto";

export interface IBlogPostDto extends IBaseEntityDto {
    title: string;
    content: string;
    isPublished: boolean;
    publicationDate: string;
    author: IUserDto;
    featuredImage: string;
    featuredImageHorizontalCrop: string;
    featuredImageVerticalCrop: string;
    url: string;
    metaTitle: string;
    metaDescription: string;
    excerpt: string;
    contentPreview: string;
    blogCategories: Array<IBlogCategoryDto>;
    blogComments: Array<IBlogCommentDto>;
}
