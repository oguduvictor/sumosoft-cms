﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductDto } from "./IProductDto";
import { ICategoryLocalizedKitDto } from "./ICategoryLocalizedKitDto";
import { CategoryTypesEnum } from "../enums/CategoryTypesEnum";
import { IProductStockUnitDto } from "./IProductStockUnitDto";
import { IProductImageKitDto } from "./IProductImageKitDto";

export interface ICategoryDto extends IBaseEntityDto {
    name: string;
    url: string;
    sortOrder: number;
    tags: string;
    productsSortOrder: string;
    productStockUnitsSortOrder: string;
    productImageKitsSortOrder: string;
    typeDescriptions: Array<string>;
    type: CategoryTypesEnum;
    parent: ICategoryDto;
    children: Array<ICategoryDto>;
    products: Array<IProductDto>;
    productStockUnits: Array<IProductStockUnitDto>;
    productImageKits: Array<IProductImageKitDto>;
    categoryLocalizedKits: Array<ICategoryLocalizedKitDto>;
    localizedTitle: string;
    localizedDescription: string;
    localizedImage: string;
    localizedFeaturedTitle: string;
    localizedFeaturedDescription: string;
    localizedFeaturedImage: string;
}