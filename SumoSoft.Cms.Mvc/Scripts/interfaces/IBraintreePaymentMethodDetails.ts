﻿export interface IBraintreePaymentMethodDetails {
    bin: string;
    cardType: string;
    lastFour: string;
    lastTwo: string;
    email: string;
}