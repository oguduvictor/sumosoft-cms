﻿export interface IResetUserPasswordDto {
    userId: string;
    newPassword: string;
}