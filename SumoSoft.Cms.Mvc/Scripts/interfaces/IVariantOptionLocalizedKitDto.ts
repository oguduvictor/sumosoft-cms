﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IVariantOptionDto } from "./IVariantOptionDto";
import { ILocalizedKitDto } from "./ILocalizedKitDto";

export interface IVariantOptionLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    variantOption: IVariantOptionDto;
    title: string;
    description: string;
    priceModifier: number;
}