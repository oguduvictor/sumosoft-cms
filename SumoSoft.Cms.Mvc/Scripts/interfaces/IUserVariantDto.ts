﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IVariantDto } from "./IVariantDto";
import { IVariantOptionDto } from "./IVariantOptionDto";
import { IUserDto } from './IUserDto';

export interface IUserVariantDto extends IBaseEntityDto {
    user: IUserDto;
    variant: IVariantDto;
    booleanValue: boolean;
    doubleValue: number;
    integerValue: number;
    stringValue: string;
    jsonValue: string;
    variantOptionValue: IVariantOptionDto;
}