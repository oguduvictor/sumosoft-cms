import { IBaseEntityDto } from "./IBaseEntityDto";
import { IContentSectionDto } from "./IContentSectionDto";
import { IAttributeDto } from "./IAttributeDto";
import { IAttributeOptionDto } from "./IAttributeOptionDto";
import { IUserDto } from "./IUserDto";

export interface IUserAttributeDto extends IBaseEntityDto {
    user: IUserDto;
    attribute: IAttributeDto;
    stringValue: string;
    booleanValue: boolean;
    dateTimeValue: string;
    imageValue: string;
    doubleValue: number;
    attributeOptionValue: IAttributeOptionDto;
    contentSectionValue: IContentSectionDto;
    jsonValue: string;
}
