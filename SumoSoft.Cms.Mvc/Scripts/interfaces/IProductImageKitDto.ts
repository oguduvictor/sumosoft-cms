﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductDto } from "./IProductDto";
import { IVariantOptionDto } from "./IVariantOptionDto";
import { IProductImageDto } from "./IProductImageDto";
import { ICategoryDto } from "./ICategoryDto";

export interface IProductImageKitDto extends IBaseEntityDto {
    product: IProductDto;
    variantOptions: Array<IVariantOptionDto>;
    productImages: Array<IProductImageDto>;
    categories: Array<ICategoryDto>;
    relatedProductImageKits: Array<IProductImageKitDto>;
}
