﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IAddressDto } from "./IAddressDto";
import { IUserDto } from "./IUserDto";
import { IUserCreditDto } from "./IUserCreditDto";
import { ICartShippingBoxDto } from "./ICartShippingBoxDto";
import { ICouponDto } from "./ICouponDto";
import { ICartError } from "./ICartError";

export interface ICartDto extends IBaseEntityDto {
    cartShippingBoxes: Array<ICartShippingBoxDto>;
    shippingAddress: IAddressDto;
    billingAddress: IAddressDto;
    user: IUserDto;
    coupon: ICouponDto;
    userCredit: IUserCreditDto;
    couponValue: number;
    totalBeforeTax: number;
    totalAfterTax: number;
    totalToBePaid: number;
    errors: Array<ICartError>;
}