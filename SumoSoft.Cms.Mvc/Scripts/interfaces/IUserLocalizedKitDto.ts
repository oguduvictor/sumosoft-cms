﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IUserDto } from "./IUserDto";
import { IUserCreditDto } from "./IUserCreditDto";
import { ILocalizedKitDto } from './ILocalizedKitDto';

export interface IUserLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    user: IUserDto;
    userCredits: Array<IUserCreditDto>;
}