﻿import { ISeoSectionLocalizedKitDto } from "./ISeoSectionLocalizedKitDto";

export interface ISeoSectionDto {
    id: string;
    page: string;
    seoSectionLocalizedKits: Array<ISeoSectionLocalizedKitDto>;
    localizedTitle: string;
    localizedDescription: string;
}