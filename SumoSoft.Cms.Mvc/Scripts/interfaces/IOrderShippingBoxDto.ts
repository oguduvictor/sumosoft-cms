﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IOrderDto } from "./IOrderDto";
import { IOrderItemDto } from "./IOrderItemDto";
import { OrderShippingBoxStatusEnum } from "../enums/OrderShippingBoxStatusEnum";

export interface IOrderShippingBoxDto extends IBaseEntityDto {
    order: IOrderDto;
    name: string;
    title: string;
    description: string;
    internalDescription: string;
    minDays: number;
    maxDays: number;
    countWeekends: boolean;
    trackingCode: string;
    shippingPriceBeforeTax: number;
    shippingPriceAfterTax: number;
    status: OrderShippingBoxStatusEnum;
    statusDescriptions: Array<string>;
    orderItems: Array<IOrderItemDto>;
}