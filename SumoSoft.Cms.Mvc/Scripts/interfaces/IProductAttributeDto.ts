﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductDto } from "./IProductDto";
import { IContentSectionDto } from "./IContentSectionDto";
import { IAttributeDto } from "./IAttributeDto";
import { IAttributeOptionDto } from "./IAttributeOptionDto";

export interface IProductAttributeDto extends IBaseEntityDto {
    product: IProductDto;
    attribute: IAttributeDto;
    stringValue: string;
    booleanValue: boolean;
    dateTimeValue: string;
    imageValue: string;
    doubleValue: number;
    attributeOptionValue: IAttributeOptionDto;
    contentSectionValue: IContentSectionDto;
    jsonValue: string;
}
