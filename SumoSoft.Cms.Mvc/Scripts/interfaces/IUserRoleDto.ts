﻿import { IBaseEntityDto } from "./IBaseEntityDto";

export interface IUserRoleDto extends IBaseEntityDto {
    name: string;
    // -------------------------------------------------------------
    // Access to Administration panel sections
    // -------------------------------------------------------------

    accessAdmin: boolean;
    accessAdminEcommerce: boolean;
    accessAdminOrders: boolean;
    accessAdminBlog: boolean;
    accessAdminMedia: boolean;
    accessAdminContent: boolean;
    accessAdminEmails: boolean;
    accessAdminSeo: boolean;
    accessAdminUsers: boolean;
    accessAdminCountries: boolean;
    accessAdminSettings: boolean;

    // -------------------------------------------------------------
    // Orders section
    // -------------------------------------------------------------

    editOrder: boolean;

    // -------------------------------------------------------------
    // Product section
    // -------------------------------------------------------------

    createProduct: boolean;
    editProduct: boolean;
    editProductAllLocalizedKits: boolean;
    deleteProduct: boolean;

    // -------------------------------------------------------------
    // Attribute section
    // -------------------------------------------------------------

    createAttribute: boolean;
    editAttribute: boolean;
    editAttributeName: boolean;
    editAttributeAllLocalizedKits: boolean;
    deleteAttribute: boolean;

    // -------------------------------------------------------------
    // AttributeOption section
    // -------------------------------------------------------------

    createAttributeOption: boolean;
    editAttributeOption: boolean;
    editAttributeOptionName: boolean;
    editAttributeOptionAllLocalizedKits: boolean;
    deleteAttributeOption: boolean;

    // -------------------------------------------------------------
    // Variant section
    // -------------------------------------------------------------

    createVariant: boolean;
    editVariant: boolean;
    editVariantName: boolean;
    editVariantSelector: boolean;
    editVariantAllLocalizedKits: boolean;
    deleteVariant: boolean;

    // -------------------------------------------------------------
    // VariantOption section
    // -------------------------------------------------------------

    createVariantOption: boolean;
    editVariantOption: boolean;
    editVariantOptionName: boolean;
    editVariantOptionAllLocalizedKits: boolean;
    deleteVariantOption: boolean;

    // -------------------------------------------------------------
    // ShippingBox section
    // -------------------------------------------------------------

    createShippingBox: boolean;
    editShippingBox: boolean;
    editShippingBoxName: boolean;
    editShippingBoxAllLocalizedKits: boolean;
    deleteShippingBox: boolean;

    // -------------------------------------------------------------
    // Category section
    // -------------------------------------------------------------

    createCategory: boolean;
    editCategory: boolean;
    editCategoryName: boolean;
    editCategoryUrl: boolean;
    editCategoryAllLocalizedKits: boolean;
    deleteCategory: boolean;

    // -------------------------------------------------------------
    // Blog section
    // -------------------------------------------------------------

    createBlogPost: boolean;
    editBlogPost: boolean;
    deleteBlogPost: boolean;

    // -------------------------------------------------------------
    // Content section
    // -------------------------------------------------------------

    createContentSection: boolean;
    editContentSection: boolean;
    editContentSectionName: boolean;
    editContentSectionAllLocalizedKits: boolean;
    deleteContentSection: boolean;

    // -------------------------------------------------------------
    // Email section
    // -------------------------------------------------------------

    createEmail: boolean;
    editEmail: boolean;
    editEmailName: boolean;
    editEmailAllLocalizedKits: boolean;
    deleteEmail: boolean;

    // -------------------------------------------------------------
    // Mailing List section
    // -------------------------------------------------------------

    createMailingList: boolean;
    editMailingList: boolean;
    editMailingListName: boolean;
    editMailingListAllLocalizedKits: boolean;
    deleteMailingList: boolean;

    // -------------------------------------------------------------
    // Seo section
    // -------------------------------------------------------------

    createSeoSection: boolean;
    editSeoSection: boolean;
    editSeoSectionPage: boolean;
    editSeoSectionAllLocalizedKits: boolean;
    deleteSeoSection: boolean;

    // -------------------------------------------------------------
    // User section
    // -------------------------------------------------------------

    createUser: boolean;
    editUser: boolean;
    deleteUser: boolean;

    // -------------------------------------------------------------
    // UserRole section
    // -------------------------------------------------------------

    createUserRole: boolean;
    editUserRole: boolean;
    deleteUserRole: boolean;

    // -------------------------------------------------------------
    // Country section
    // -------------------------------------------------------------

    createCountry: boolean;
    editCountry: boolean;
    deleteCountry: boolean;

    // -------------------------------------------------------------
    // Coupon section
    // -------------------------------------------------------------

    createCoupon: boolean;
    editCoupon: boolean;
    deleteCoupon: boolean;

    // -------------------------------------------------------------
    // Logs
    // -------------------------------------------------------------

    viewExceptionLogs: boolean;
    viewEcommerceLogs: boolean;
    viewContentLogs: boolean;
}