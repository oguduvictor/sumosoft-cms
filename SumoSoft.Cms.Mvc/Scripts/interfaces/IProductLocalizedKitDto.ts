﻿import { ICountryDto } from "./ICountryDto";
import { IProductDto } from "./IProductDto";
import { ILocalizedKitDto } from "./ILocalizedKitDto";
import { IBaseEntityDto } from './IBaseEntityDto';

export interface IProductLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    product: IProductDto;
    title: string;
    description: string;
    metaTitle: string;
    metaDescription: string;
}
