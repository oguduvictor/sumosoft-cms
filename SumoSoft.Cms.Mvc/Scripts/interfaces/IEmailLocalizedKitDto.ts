﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IEmailDto } from "./IEmailDto";

export interface IEmailLocalizedKitDto extends IBaseEntityDto {
    country: ICountryDto;
    email: IEmailDto;
    from: string;
    displayName: string;
    replyTo: string;
}