﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IUserLocalizedKitDto } from "./IUserLocalizedKitDto";

export interface IUserCreditDto extends IBaseEntityDto {
    userLocalizedKit: IUserLocalizedKitDto;
    amount: number;
    reference: string;
}