﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IEmailLocalizedKitDto } from "./IEmailLocalizedKitDto";
import { IContentSectionDto } from './IContentSectionDto';

export interface IEmailDto extends IBaseEntityDto {
    name: string;
    contentSection: IContentSectionDto;
    viewName: string;
    emailLocalizedKits: Array<IEmailLocalizedKitDto>;
    localizedFrom: string;
    localizedDisplayName: string;
    localizedReplyTo: string;
}