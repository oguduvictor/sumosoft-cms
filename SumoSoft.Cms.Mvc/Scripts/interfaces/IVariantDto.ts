﻿import { VariantTypesEnum } from "../enums/VariantTypesEnum";
import { IBaseEntityDto } from "./IBaseEntityDto";
import { IVariantOptionDto } from "./IVariantOptionDto";
import { IVariantLocalizedKitDto } from "./IVariantLocalizedKitDto";

export interface IVariantDto extends IBaseEntityDto {
    name: string;
    url: string;
    sortOrder: number;
    variantOptions: Array<IVariantOptionDto>;
    type: VariantTypesEnum;
    typeDescriptions: Array<string>;
    variantLocalizedKits: Array<IVariantLocalizedKitDto>;
    defaultBooleanValue: boolean;
    defaultDoubleValue: number;
    defaultIntegerValue: number;
    defaultStringValue: string;
    defaultJsonValue: string;
    defaultVariantOptionValue: IVariantOptionDto;
    createProductStockUnits: boolean;
    createProductImageKits: boolean;
    localizedTitle: string;
    localizedDescription: string;
}