﻿import { IFileDto } from "./IFileDto"

export interface IDirectoryDto {
    name: string;
    localPath: string;
    size: number;
    creationTime: string;
    useAzureStorage: boolean;
    files: Array<IFileDto>;
    directories: Array<IDirectoryDto>;
    parent: IDirectoryDto;
    isRoot: boolean;
}