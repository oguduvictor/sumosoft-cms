﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IMailingListDto } from './IMailingListDto';
import { ICountryDto } from './ICountryDto';
import { ILocalizedKitDto } from './ILocalizedKitDto';

export interface IMailingListLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    mailingList: IMailingListDto;
    country: ICountryDto;

}