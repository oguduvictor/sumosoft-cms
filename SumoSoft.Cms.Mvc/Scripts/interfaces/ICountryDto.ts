﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ITaxDto } from './ITaxDto';
import { IShippingBoxDto } from './IShippingBoxDto';

export interface ICountryDto extends IBaseEntityDto
{
    flagIcon: string;
    url: string;
    name: string;
    languageCode: string;
    isDefault: boolean;
    localize: boolean;
    currencySymbol: string;
    iso4217CurrencySymbol: string;
    urlForStringConcatenation: string;
    paymentAccountId: string;
    taxes: Array<ITaxDto>;
    shippingBoxes: Array<IShippingBoxDto>;
}
