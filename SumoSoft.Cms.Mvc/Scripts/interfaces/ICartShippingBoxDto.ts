﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICartDto } from "./ICartDto";
import { IShippingBoxDto } from "./IShippingBoxDto";
import { ICartItemDto } from "./ICartItemDto";

export interface ICartShippingBoxDto extends IBaseEntityDto {
    shippingBox: IShippingBoxDto;
    cart: ICartDto;
    cartItems: Array<ICartItemDto>;
    shippingPriceBeforeTax: number;
    shippingPriceAfterTax: number;
    totalBeforeTax: number;
    totalAfterTax: number;
}