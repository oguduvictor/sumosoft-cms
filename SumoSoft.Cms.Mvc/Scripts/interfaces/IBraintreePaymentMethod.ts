﻿import { IBraintreePaymentMethodBinData } from './IBraintreePaymentMethodBinData';
import { IBraintreePaymentMethodDetails } from './IBraintreePaymentMethodDetails';

export interface IBraintreePaymentMethod {
    binData: IBraintreePaymentMethodBinData;
    description: string;
    details: IBraintreePaymentMethodDetails;
    nonce: string;
    type: string;
    vaulted: boolean;
}