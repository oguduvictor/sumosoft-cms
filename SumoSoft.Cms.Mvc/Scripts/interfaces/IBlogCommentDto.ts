﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IUserDto } from "./IUserDto";
import { IBlogPostDto } from "./IBlogPostDto";

export interface IBlogCommentDto extends IBaseEntityDto {
    title: string;
    author: IUserDto;
    email: string;
    url: string;
    body: string;
    published: boolean;
    post: IBlogPostDto;
}
