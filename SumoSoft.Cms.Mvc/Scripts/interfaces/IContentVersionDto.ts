﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IContentSectionLocalizedKitDto } from "./IContentSectionLocalizedKitDto";

export interface IContentVersionDto extends IBaseEntityDto {
    content: string;
    contentSectionLocalizedKit: IContentSectionLocalizedKitDto;
}