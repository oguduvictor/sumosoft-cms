﻿import { IFormError } from "./IFormError";

export interface IFormResponse<T = {}> {
    isValid: boolean;
    successMessage: string;
    errors: Array<IFormError>;
    redirectUrl: string;
    target?: T;
}