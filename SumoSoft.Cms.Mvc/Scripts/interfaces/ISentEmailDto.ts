﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IUserDto } from "./IUserDto";
import { ICouponDto } from "./ICouponDto";
import { IEmailDto } from "./IEmailDto";
import { IOrderDto } from "./IOrderDto";

export interface ISentEmailDto extends IBaseEntityDto {
    email: IEmailDto;
    user: IUserDto;
    order: IOrderDto;
    coupon: ICouponDto;
    from: string;
    to: string;
    reference: string;
}