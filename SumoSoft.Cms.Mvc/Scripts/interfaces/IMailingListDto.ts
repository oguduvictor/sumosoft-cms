﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IMailingListLocalizedKitDto } from './IMailingListLocalizedKitDto';
import { IMailingListSubscription } from './IMailingListSubscription';

export interface IMailingListDto extends IBaseEntityDto {
    name: string;
    sortOrder: number;
    mailingListLocalizedKits: Array<IMailingListLocalizedKitDto>;
    localizedTitle: string;
    localizedDescription: string;
    totalSubscribers: number;
    totalUsers: number;
    mailingListSubscriptions: Array<IMailingListSubscription>;
}