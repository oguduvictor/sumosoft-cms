﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IOrderItemDto } from './IOrderItemDto';

export interface IOrderItemTaxDto extends IBaseEntityDto {
    orderItem: IOrderItemDto;
    name: string;
    code: string;
    amount: number
}