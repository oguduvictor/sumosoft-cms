﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { IUserDto } from "./IUserDto";
import { ICategoryDto } from "./ICategoryDto";

export interface ICouponDto extends IBaseEntityDto {
    code: string;
    amount: number;
    percentage: number;
    country: ICountryDto;
    published: boolean;
    expirationDate: string;
    validityTimes: number;
    categories: Array<ICategoryDto>;
    referee: IUserDto;
    refereeReward: number;
    recipient: string;
    note: string;
    allowOnSale: boolean;
}