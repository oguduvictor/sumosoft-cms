﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IProductStockUnitDto } from "./IProductStockUnitDto";
import { ICountryDto } from "./ICountryDto";
import { ILocalizedKitDto } from './ILocalizedKitDto';

export interface IProductStockUnitLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    productStockUnit: IProductStockUnitDto;
    country: ICountryDto;
    basePrice: number;
    salePrice: number;
    membershipSalePrice: number;
}