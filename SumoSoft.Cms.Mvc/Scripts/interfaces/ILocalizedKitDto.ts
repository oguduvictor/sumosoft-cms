﻿import { ICountryDto } from "./ICountryDto";
import { IBaseEntityDto } from "./IBaseEntityDto";

export interface ILocalizedKitDto extends IBaseEntityDto {
    country: ICountryDto;
}