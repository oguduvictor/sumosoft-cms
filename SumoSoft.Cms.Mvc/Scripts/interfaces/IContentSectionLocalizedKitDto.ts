﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { IContentSectionDto } from "./IContentSectionDto";
import { ICountryDto } from "./ICountryDto";
import { ILocalizedKitDto } from "./ILocalizedKitDto";
import { IContentVersionDto } from "./IContentVersionDto";

export interface IContentSectionLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    contentSection: IContentSectionDto;
    content: string;
    contentVersions: Array<IContentVersionDto>;
}