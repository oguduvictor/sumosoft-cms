﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { ICartItemDto } from "./ICartItemDto";

export interface ITaxDto extends IBaseEntityDto {
    country: ICountryDto;
    cartItem: ICartItemDto;
    name: string;
    code: string;
    percentage: number;
    applyToShippingPrice: boolean;
    applyToProductPrice: boolean;
}
