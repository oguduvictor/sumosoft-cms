﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { OrderItemStockStatusEnum } from "../enums/OrderItemStockStatusEnum";
import { IOrderShippingBoxDto } from "./IOrderShippingBoxDto";
import { IOrderCouponDto } from "./IOrderCouponDto";
import { IOrderItemVariantDto } from "./IOrderItemVariantDto";
import { OrderItemStatusEnum } from "../enums/OrderItemStatusEnum";
import { IOrderItemTaxDto } from './IOrderItemTaxDto';

export interface IOrderItemDto extends IBaseEntityDto {
    orderShippingBox: IOrderShippingBoxDto;
    productId: string;
    productTitle: string;
    productName: string;
    productUrl: string;
    productCode: string;
    productQuantity: number;
    productStockUnitId: string;
    productStockUnitCode: string;
    productStockUnitReleaseDate: string;
    productStockUnitEnablePreorder: boolean;
    productStockUnitShipsIn: number;
    productStockUnitBasePrice: number;
    productStockUnitSalePrice: number;
    productStockUnitMembershipSalePrice: number;
    image: string;
    orderItemVariants: Array<IOrderItemVariantDto>;
    orderTaxes: Array<IOrderItemTaxDto>;
    status: OrderItemStatusEnum;
    statusDescriptions: Array<string>;
    stockStatus: OrderItemStockStatusEnum;
    stockStatusDescriptions: Array<string>;
    orderCoupon: IOrderCouponDto;
    minEta: string;
    maxEta: string;
    subtotalBeforeTax: number;
    subtotalAfterTax: number;
    totalBeforeTax: number;
    totalAfterTax: number;
}