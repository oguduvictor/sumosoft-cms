﻿export interface IBaseEntityDto {
    id: string;
    isDisabled: boolean;
    createdDate: string;
    deletedDate: string;
    modifiedDate: string;
    isNew: boolean;
    isModified: boolean;
    isDeleted: boolean;
}