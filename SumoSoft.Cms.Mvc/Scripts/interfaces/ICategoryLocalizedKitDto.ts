﻿import { IBaseEntityDto } from "./IBaseEntityDto";
import { ICountryDto } from "./ICountryDto";
import { ICategoryDto } from "./ICategoryDto";
import { ILocalizedKitDto } from "./ILocalizedKitDto";

export interface ICategoryLocalizedKitDto extends IBaseEntityDto, ILocalizedKitDto {
    country: ICountryDto;
    category: ICategoryDto;
    title: string;
    image: string;
    description: string;
    featuredTitle: string;
    featuredDescription: string;
    featuredImage: string;
    metaTitle: string;
    metaDescription: string;
}