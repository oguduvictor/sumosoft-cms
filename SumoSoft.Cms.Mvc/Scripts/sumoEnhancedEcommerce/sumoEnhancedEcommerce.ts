﻿import { Order } from "../classes/Order";
import { CartItem } from "../classes/CartItem";
import { Cart } from "../classes/Cart";
import { ProductStockUnit } from "../classes/ProductStockUnit";

declare global {
    var dataLayer;
}

export function purchase(order: Order) {

    const orderItems = order.orderShippingBoxes.mapMany(x => x.orderItems);

    dataLayer.push({
        'event': "purchase",
        'ecommerce': {
            'currencyCode': order.iso4217CurrencySymbol,
            'purchase': {
                'actionField': {
                    'id': order.orderNumber.toString(),
                    'affiliation': "Online Store",
                    'revenue': order.totalAfterTax,
                    'tax': order.totalAfterTax - order.totalBeforeTax,
                    'shipping': order.orderShippingBoxes.sum(x => x.shippingPriceAfterTax),
                    'coupon': order.orderCoupon && order.orderCoupon.code || "",
                },
                'products': orderItems.map(x => {
                    return {
                        'name': x.productTitle,
                        'id': getGoogleProductId(x.productStockUnitId),
                        'price': x.subtotalAfterTax,
                        'brand': "",
                        'category': "",
                        'variant': "",
                        'quantity': x.productQuantity
                    }
                })
            }
        }
    });
}

export function productDetail(cartItem: CartItem, iso4217CurrencySymbol: string) {

    dataLayer.push({
        'event': "productDetail",
        'ecommerce': {
            'currencyCode': iso4217CurrencySymbol,
            'detail': {
                'products': cartItem.product.productStockUnits.map(productStockUnit => {
                    return {
                        'name': cartItem.product.localizedTitle,
                        'id': getGoogleProductId(productStockUnit.id),
                        'price': cartItem.totalAfterTax,
                        'variant': ""
                    }
                })
            }
        }
    });
}

export function addToCart(cartItems: Array<CartItem>, iso4217CurrencySymbol: string) {

    dataLayer.push({
        'event': "addToCart",
        'ecommerce': {
            'currencyCode': iso4217CurrencySymbol,
            'add': {
                'products': cartItems.map(x => {
                    return {
                        'name': x.product.localizedTitle,
                        'id': getGoogleProductId(x.getProductStockUnit().id),
                        'price': x.totalAfterTax,
                        'quantity': x.quantity,
                        'variant': ""
                    }
                })
            }
        }
    });
}

export function checkout(cart: Cart, step: number, iso4217CurrencySymbol: string) {

    const cartItems = cart.cartShippingBoxes.mapMany(x => x.cartItems);

    dataLayer.push({
        'event': "checkout",
        'ecommerce': {
            'currencyCode': iso4217CurrencySymbol,
            'checkout': {
                'actionField': {
                    'step': step
                },
                'products': cartItems.map(x => {
                    return {
                        'name': x.product.localizedTitle,
                        'id': getGoogleProductId(x.getProductStockUnit().id),
                        'price': x.totalAfterTax,
                        'variant': "",
                        'quantity': x.quantity
                    }
                })
            }
        }
    });
}

function getGoogleProductId(productStockUnitId: string) {
    return productStockUnitId.replaceAll("-", "").substr(0, 10);
}