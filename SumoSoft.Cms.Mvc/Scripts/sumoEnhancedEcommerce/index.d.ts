export as namespace sumoEnhancedEcommerce;
export = sumoEnhancedEcommerce;

import { Order } from '../classes/Order';
import { CartItem } from '../classes/CartItem';
import { Cart } from '../classes/Cart';

declare namespace sumoEnhancedEcommerce {
	export function purchase(order: Order);

	export function productDetail(
		cartItem: CartItem,
		iso4217CurrencySymbol: string
	);

	export function addToCart(
		cartItems: Array<CartItem>,
		iso4217CurrencySymbol: string
	);

	export function checkout(
		cart: Cart,
		step: number,
		iso4217CurrencySymbol: string
	);
}
