﻿// -------------------------------------------------------------------------------------------------
/** Initialize a new instance of SignalR */
export function initializeSignalR(done: (connection: SignalR) => void) {
    $.getScript("//cdn.jsdelivr.net/npm/signalr@2.3.0/jquery.signalR.min.js").done(() =>
        $.getScript("/signalr/hubs").done(() => {
            const connection = $.connection;
            connection.hub.url = "/signalr";
            done(connection);
        })
    );
}

// -------------------------------------------------------------------------------------------------
/** Creates a proxy for a specified SignalR Hub */
export function createHubProxy(signalR: SignalR, hubName: string) {
    const proxy = signalR[hubName] as SignalR.Hub.Proxy;
    window[hubName] = proxy;
    return proxy;
}

// -------------------------------------------------------------------------------------------------
/** Invoke a server-side method on a specified SignalR Hub with supplied arguments */
export function invoke(hubName: string, methodName: string, ...args: any[]) {
    var loop = setInterval(() => {
        var proxy = window[hubName] as SignalR.Hub.Proxy;
        if (proxy) {
            proxy.invoke(methodName, ...args);
            clearInterval(loop);
        }
    }, 500);
}
