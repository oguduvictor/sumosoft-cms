﻿export as namespace sumoSignalR;
export = sumoSignalR;

declare namespace sumoSignalR {
    // -------------------------------------------------------------------------------------------------
    /** Initialize a new instance of SignalR */
    export function initializeSignalR(done: (connection: SignalR) => void): void;

    // -------------------------------------------------------------------------------------------------
    /** Creates a proxy for a specified SignalR Hub */
    export function createHubProxy(signalR: SignalR, hubName: string): SignalR.Hub.Proxy;

    // -------------------------------------------------------------------------------------------------
    /** Invoke a server-side method on a specified SignalR Hub with supplied arguments */
    export function invoke(hubName: string, methodName: string, ...args: any[]): void;
}