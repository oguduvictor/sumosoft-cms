﻿import * as React from "react";
import * as dayjs from "dayjs";

import { ProductStockUnit } from "../../classes/ProductStockUnit";

export interface IStockWarningProps {
    productStockUnit: ProductStockUnit;
    /** Message shown when the product is out of stock */
    outOfStockMessage: string;
    /** Message shown when the release date of the product is in the future and preorder is enabled */
    preOrderMessage: string;
    /** Message shown when the release date of the product is in the future and preorder is not enabled */
    comingSoonMessage: string;
    /** Message showing the number of days it takes for a product to be ready for shipping */
    shipsInMessage: string;
}

export class StockWarning extends React.Component<IStockWarningProps, undefined> {

    constructor(props) {
        super(props);
    }

    getShipsInMessage() {
        return this.props.shipsInMessage.replace("[[DispatchTime]]", this.props.productStockUnit.dispatchTime.toString());
    }

    getComingSoonMessage() {
        return this.props.comingSoonMessage.replace("[[DispatchDate]]", dayjs(this.props.productStockUnit.dispatchDate).format("dddd, MMMM D YYYY"));
    }

    getPreOrderMessage() {
        return this.props.preOrderMessage.replace("[[DispatchDate]]", dayjs(this.props.productStockUnit.dispatchDate).format("dddd, MMMM D YYYY"));
    }

    // -----------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------

    render() {
        let stockMessage: JSX.Element;
        let shipsInMessage: JSX.Element;
        if (this.props.productStockUnit.stock < 1) {
            stockMessage = <div className="out-of-stock-message" dangerouslySetInnerHTML={{ __html: this.props.outOfStockMessage.renderMarkdown() }} />;
        } else {
            if (new Date(this.props.productStockUnit.dispatchDate) > new Date()) {
                if (this.props.productStockUnit.enablePreorder) {
                    stockMessage = <div className="pre-order-message" dangerouslySetInnerHTML={{ __html: this.getPreOrderMessage().renderMarkdown() }} />;
                } else {
                    stockMessage = <div className="coming-soon-message" dangerouslySetInnerHTML={{ __html: this.getComingSoonMessage().renderMarkdown() }} />;
                }
            }
        }
        if (this.props.productStockUnit.dispatchTime > 0) {
            shipsInMessage = <div className="ships-in-message" dangerouslySetInnerHTML={{ __html: this.getShipsInMessage().renderMarkdown() }} />;
        }
        return (
            <div className="stock-warning-component">
                {
                    stockMessage
                }
                {
                    !stockMessage && shipsInMessage
                }
            </div>
        );
    }
}