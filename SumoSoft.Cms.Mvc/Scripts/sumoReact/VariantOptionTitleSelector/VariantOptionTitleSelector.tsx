﻿import * as React from "react";
import { VariantOption } from "../../classes/VariantOption";

export interface IVariantOptionTitleSelectorProps {
    variantOptions: Array<VariantOption>;
    value: VariantOption;
    handleChangeValue(variantOption: VariantOption): void;
    className?: string;
    disableVariantOptionsWhere?(variantOption: VariantOption): boolean;
}

export var VariantOptionTitleSelector: (props: IVariantOptionTitleSelectorProps) => JSX.Element = props => {

    return (
        <span className={`variant-option-title-selector ${props.className ? props.className : ""}`}>
            {
                props.variantOptions.map(variantOption =>
                    <span
                        key={variantOption.id}
                        data-variantoptionname={variantOption.name}
                        className={`${props.value && props.value.name === variantOption.name ? "selected" : ""} ${props.disableVariantOptionsWhere && props.disableVariantOptionsWhere(variantOption) ? "disabled" : ""}`}
                        onClick={() => props.handleChangeValue(variantOption)}>
                        {
                            variantOption.localizedTitle
                        }
                    </span>
                )
            }
        </span>
    );
}