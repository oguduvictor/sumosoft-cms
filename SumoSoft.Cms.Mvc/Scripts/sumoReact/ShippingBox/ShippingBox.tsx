﻿import * as React from "react";
import { IShippingBoxProps } from './IShippingBoxProps';

export class ShippingBox extends React.Component<IShippingBoxProps, {}> {

    constructor(props: IShippingBoxProps) {
        super(props);
    }

    static defaultProps: IShippingBoxProps;

    render() {
        var minDays = this.props.shippingBox.localizedMinDays;
        var maxDays = this.props.shippingBox.localizedMaxDays;
        if (this.props.cartItem) {
            minDays = minDays + this.props.cartItem.getProductStockUnit().dispatchTime;
            maxDays = maxDays + this.props.cartItem.getProductStockUnit().dispatchTime;
        }
        var daysRange = minDays === maxDays ? minDays : `${minDays} - ${maxDays}`;
        return (
            <div className="shipping-box">
                <div className="shipping-box-title">
                    {this.props.shippingBox.localizedTitle}
                </div>
                <div className="shipping-box-price">
                    {`${this.props.priceLabel} ${this.props.shippingBox.localizedPrice > 0 ? this.props.country.currencySymbol + this.props.shippingBox.localizedPrice : this.props.freeLabel}`}
                </div>
                {
                    this.props.shippingBox.localizedPrice > 0 &&
                    this.props.shippingBox.localizedFreeShippingMinimumPrice > 0 &&
                    <div className="shipping-box-free-shipping-minimum-price">
                        {`(${this.props.freeShippingLabel} ${this.props.country.currencySymbol + this.props.shippingBox.localizedFreeShippingMinimumPrice})`}
                    </div>
                }
                <div className="delivery-time">
                    {`${this.props.deliveryTimeLabel} ${daysRange} working days`}
                </div>
                <div className="shipping-box-countries">
                    {`${this.props.shippingCountriesLabel} ${this.props.shippingBox.countries.map(c => c.name).join(", ")}`}
                </div>
            </div>
        );
    }
}

ShippingBox.defaultProps = {
    priceLabel: "Price:",
    freeLabel: "Free",
    freeShippingLabel: "free when you spend over",
    deliveryTimeLabel: "Delivery time:",
    shippingCountriesLabel: "Shipping to:"
} as IShippingBoxProps;
