﻿import { CartItem } from '../../classes/CartItem';
import { ShippingBox } from '../../classes/ShippingBox';
import { Country } from '../../classes/Country';

export interface IShippingBoxProps {
    country: Country;
    shippingBox: ShippingBox;
    /** Specifying the cartItem property will cause the component to factor the ProductStockUnit.ShipsIn value into the delivery time */
    cartItem?: CartItem;
    priceLabel?: string;
    freeLabel?: string;
    freeShippingLabel?: string;
    deliveryTimeLabel?: string;
    shippingCountriesLabel?: string;
}