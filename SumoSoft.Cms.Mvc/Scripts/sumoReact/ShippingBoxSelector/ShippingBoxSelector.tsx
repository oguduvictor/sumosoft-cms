﻿import * as React from "react";
import { IShippingBoxSelectorProps } from './IShippingBoxSelectorProps';
import { ShippingBox } from '../../classes/ShippingBox';
import { BigRadioSelector } from '../BigRadioSelector/BigRadioSelector';

export class ShippingBoxSelector extends React.Component<IShippingBoxSelectorProps, {}> {

    constructor(props: IShippingBoxSelectorProps) {
        super(props);
    }

    static defaultProps: IShippingBoxSelectorProps;

    getValidShippingBoxesForShippingCountry(): Array<ShippingBox> {
        const shippingBoxes = this.props.cartItem.product.shippingBoxes;
        const shippingCountry = this.props.shippingCountryOrCurrentCountry;
        let validShippingBoxes = shippingBoxes.filter(shippingBox => shippingBox.countries.some(country => country.id === shippingCountry.id));

        if (this.props.disableShippingBoxesWhere) {
            validShippingBoxes = validShippingBoxes.filter(shippingBox => !this.props.disableShippingBoxesWhere(shippingBox));
        }

        return validShippingBoxes;
    }

    getDaysSentence(shippingBox: ShippingBox): string {
        const minDays = shippingBox.localizedMinDays;
        const maxDays = shippingBox.localizedMaxDays;
        const countWeekends = shippingBox.localizedCountWeekends;
        let daysSentence = "";
        if (minDays === maxDays && minDays === 1) {
            daysSentence = countWeekends ? this.props.oneDaySentence : this.props.oneWorkingDaySentence;
        }
        if (minDays === maxDays && minDays > 1) {
            daysSentence = countWeekends ? this.props.daysSentence : this.props.workingDaysSentence;
        }
        if (minDays !== maxDays) {
            daysSentence = countWeekends ? this.props.minMaxDaysSentence : this.props.minMaxWorkingDaysSentence;
        }
        daysSentence = daysSentence
            .replaceAll("[[MinDays]]", minDays.toString())
            .replaceAll("[[MaxDays]]", maxDays.toString());
        return daysSentence;
    }

    renderShippingBoxDescription(shippingBox: ShippingBox) {
        return (
            <React.Fragment>
                <div>
                    {shippingBox.localizedTitle}
                    <span className="shipping-price">{
                        shippingBox.localizedPrice > 0
                            ? this.props.currencySymbol + shippingBox.localizedPrice
                            : this.props.freeShippingPriceLabel}
                    </span>
                </div>
                <div>
                    {this.getDaysSentence(shippingBox)}
                </div>
            </React.Fragment>
        );
    }

    render() {
        const shippingBoxes = this.getValidShippingBoxesForShippingCountry();
        return (
            <div className="shipping-box-selector">
                {
                    shippingBoxes.map(shippingBox =>
                        <BigRadioSelector
                            className={shippingBoxes.length < 2 ? "single" : ""}
                            key={shippingBox.id}
                            isSelected={this.props.cartItem.cartShippingBox.shippingBox.id === shippingBox.id}
                            content={this.renderShippingBoxDescription(shippingBox)}
                            onClick={() => this.props.handleChangeValue(shippingBox)} />
                    )
                }
            </div>
        );
    }
}

ShippingBoxSelector.defaultProps = {
    freeShippingPriceLabel: "Free",
    minMaxWorkingDaysSentence: "[[MinDays]] - [[MaxDays]] working days",
    minMaxDaysSentence: "[[MinDays]] - [[MaxDays]] days",
    workingDaysSentence: "[[MaxDays]] working days",
    daysSentence: "[[MaxDays]] days",
    oneWorkingDaySentence: "One working day",
    oneDaySentence: "One day"
} as IShippingBoxSelectorProps;
