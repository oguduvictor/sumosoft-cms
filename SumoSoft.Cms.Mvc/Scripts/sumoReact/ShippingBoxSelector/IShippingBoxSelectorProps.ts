﻿import { CartItem } from '../../classes/CartItem';
import { ShippingBox } from '../../classes/ShippingBox';
import { Country } from '../../classes/Country';

export interface IShippingBoxSelectorProps {
    cartItem: CartItem;
    currencySymbol: string;
    freeShippingPriceLabel?: string;
    shippingCountryOrCurrentCountry: Country;
    disableShippingBoxesWhere?(shippingBox: ShippingBox): boolean;
    handleChangeValue(shippingBox: ShippingBox): void;
    minMaxWorkingDaysSentence?: string;
    minMaxDaysSentence?: string;
    workingDaysSentence?: string
    daysSentence?: string;
    oneWorkingDaySentence?: string;
    oneDaySentence?: string;
}