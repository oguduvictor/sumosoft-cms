﻿import * as React from "react";
import * as Cookies from "js-cookie";

export interface IPrivacyProps {
    className?: string;
    content?: string | JSX.Element;
    handleWarningShown?(): void;
}

export var Privacy: (props: IPrivacyProps) => JSX.Element = props => {
    const privacyCookieName = "dismissPrivacyWarning";

    const handleDismissWarning = () => {
        props.handleWarningShown();
        if (!Cookies.get(privacyCookieName)) {
            Cookies.set(privacyCookieName, "true", { expires: 365 });
        }
    }

    if (!Cookies.get(privacyCookieName)) {
        return (
            <div className={`privacy ${props.className ? props.className : ""}`}>
                <div className="display-table">
                    <div className="display-table-row">
                        <div className="display-table-cell content">
                            {
                                props.content
                            }
                        </div>
                        <div className="display-table-cell">
                            {
                                <span className="close" onClick={() => handleDismissWarning()}>
                                    &times;
                                </span>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    return null;
}