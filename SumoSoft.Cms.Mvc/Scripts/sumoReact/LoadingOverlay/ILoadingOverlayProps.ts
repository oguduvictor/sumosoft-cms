﻿export interface ILoadingOverlayProps {
    loadingIcon?: string;
}