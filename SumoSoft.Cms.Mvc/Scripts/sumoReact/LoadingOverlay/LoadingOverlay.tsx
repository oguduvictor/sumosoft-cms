﻿import * as React from "react";
import { ILoadingOverlayProps } from './ILoadingOverlayProps';

export class LoadingOverlay extends React.Component<ILoadingOverlayProps, {}> {

    constructor(props) {
        super(props);
    }

    static defaultProps: ILoadingOverlayProps;

    render() {
        return (
            <div className="loading-overlay">
                <img className="loading-icon" src={this.props.loadingIcon} />
            </div>
        );
    }
}

LoadingOverlay.defaultProps = {
    loadingIcon: "/scripts/sumoReact/loadingoverlay/loading.gif"
} as ILoadingOverlayProps;