﻿import * as React from "react";

import { IFormResponse } from "../../interfaces/IFormResponse";
import { IFormError } from "../../interfaces/IFormError";
import { Button } from '../Button/Button';

declare var zxcvbn;

export interface IRegisterDetails {
    email?: string;
    firstName?: string;
    lastName?: string;
    password?: string;
}

export interface IRegisterFormProps {
    autoFocus?: boolean;
    initialValues: IRegisterDetails;
    emailInputPlaceholder?: string;
    enableFacebookRegister?: boolean;
    enableGoogleRegister?: boolean;
    facebookLoginButtonClassName?: string;
    facebookLoginButtonTitle?: string;
    firstNamePlaceholder?: string;
    formResponse: IFormResponse;
    googleLoginButtonClassName?: string;
    googleLoginButtonTitle?: string;
    googleLoginButtonId?: string;
    isSubmitting: boolean;
    lastNamePlaceholder?: string;
    loginUrl: string;
    loginUrlTitle?: string;
    nextStepButtonTitle?: string;
    passwordInputPlaceholder?: string;
    previousStepLinkText?: string;
    registerButtonClassName?: string;
    registerButtonTitle?: string;
    secondaryActionText?: string;
    onClickFacebookLogin?: () => void;
    onClickGoogleLogin?: () => void;
    onNavigateToStep?: (step: number) => void;
    onSubmit: (registerDetails: IRegisterDetails) => void;
}

interface IRegisterFormState {
    formStep: number;
    passwordVisible: boolean;
    registerDetails: IRegisterDetails;
}

export class RegisterForm extends React.Component<IRegisterFormProps, IRegisterFormState> {
    constructor(props: IRegisterFormProps) {
        super(props);

        this.state = {
            formStep: 0,
            passwordVisible: false,
            registerDetails: this.props.initialValues || {} as IRegisterDetails
        } as IRegisterFormState;
    }

    static defaultProps: IRegisterFormProps;

    // ------------------------------------------------------------
    // Component lifecycle
    // ------------------------------------------------------------

    componentDidMount() {
        $.getScript("https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js");
    }

    componentWillReceiveProps(nextProps: IRegisterFormProps) {
        const { formStep } = this.state;
        if (nextProps.formResponse.isValid !== this.props.formResponse.isValid) {
            if (formStep === 1
                && !nextProps.formResponse.isValid
                && !!this.getFormErrorForSpecificProperty("email", nextProps.formResponse)) {
                this.handleNavigateToStep(0);
            }
        }
    }

    // ------------------------------------------------------------
    // Helper Methods
    // ------------------------------------------------------------

    getIsEmailValid = (): boolean => {
        const email = this.state.registerDetails.email;
        const emailRegex = /^[^@\s]+@[^@\s]+\.[^@\s]+$/gm;
        return !!email && email.length > 0 && emailRegex.test(email);
    };

    getIsRegisterDetailsValid = (): boolean => {
        const { email, password, firstName, lastName } = this.state.registerDetails;
        return !!email && !!firstName && !!lastName && !!password && email.length > 0 && password.length >= 6 && firstName.length > 0 && lastName.length > 0;
    };

    getFormErrorForSpecificProperty(propertyName: string, formResponse = this.props.formResponse): IFormError {
        const formErrors = formResponse.errors || new Array<IFormError>();
        return formErrors.filter(x => x.propertyName && x.propertyName.toLowerCase() === propertyName.toLowerCase())[0];
    }

    getPasswordStrength = (): number => {
        const { password } = this.state.registerDetails;
        if (!password || !zxcvbn) return 0;
        return zxcvbn(password).score;
    };

    getPasswordStrengthText = (passwordStrength: number): string => {
        const texts = ["poor", "poor", "average", "strong", "very strong"];
        return texts[passwordStrength];
    };

    // ------------------------------------------------------------
    // Event Handlers
    // ------------------------------------------------------------

    handleNavigateToStep = (formStep: number): void => {
        this.setState({ formStep }, () => this.props.onNavigateToStep && this.props.onNavigateToStep(formStep));
    };

    handleTogglePasswordVisibility = () => {
        this.setState({ passwordVisible: !this.state.passwordVisible });
    };

    handleChangeEmail = e => {
        const registerDetails = this.state.registerDetails;
        registerDetails.email = e.target.value;

        this.setState({ registerDetails });
    };

    handleChangeFirstName = e => {
        const registerDetails = this.state.registerDetails;
        registerDetails.firstName = e.target.value;

        this.setState({ registerDetails });
    };

    handleChangeLastName = e => {
        const registerDetails = this.state.registerDetails;
        registerDetails.lastName = e.target.value;

        this.setState({ registerDetails });
    };

    handleChangePassword = e => {
        const registerDetails = this.state.registerDetails;
        registerDetails.password = e.target.value;

        this.setState({ registerDetails });
    };

    handleSubmit = () => {
        this.getIsRegisterDetailsValid() && this.props.onSubmit(this.state.registerDetails);
    };

    handleKeyPress = e => {
        if (e.key === "Enter") {
            if (this.state.formStep === 0) {
                this.getIsEmailValid() && this.handleNavigateToStep(1);
            } else {
                this.handleSubmit();
            }
        }
    };

    // ------------------------------------------------------------
    // Render Methods
    // ------------------------------------------------------------

    renderFormStepOne = (): JSX.Element => {
        const {
            autoFocus,
            emailInputPlaceholder,
            enableFacebookRegister,
            enableGoogleRegister,
            facebookLoginButtonClassName,
            facebookLoginButtonTitle,
            googleLoginButtonClassName,
            googleLoginButtonTitle,
            googleLoginButtonId,
            loginUrl,
            loginUrlTitle,
            nextStepButtonTitle,
            onClickFacebookLogin,
            onClickGoogleLogin,
            registerButtonClassName,
            secondaryActionText
        } = this.props;
        const { registerDetails } = this.state;
        const { email } = registerDetails;

        const emailError = this.getFormErrorForSpecificProperty("email");

        return (
            <div className="form-step">
                <div className="form-inputs">
                    <div className={emailError ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">Email</div>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            placeholder={emailInputPlaceholder}
                            value={email || ""}
                            onChange={this.handleChangeEmail}
                            onKeyPress={this.handleKeyPress}
                            className="form-input__input"
                            autoFocus={autoFocus}
                        />
                        {emailError && (
                            <div className="form-input__help-text">
                                <span>{emailError.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <Button
                    fullWidth
                    label={nextStepButtonTitle}
                    className={registerButtonClassName}
                    isDisabled={!this.getIsEmailValid()}
                    onClick={() => this.handleNavigateToStep(1)} />
                {(enableFacebookRegister || enableGoogleRegister) && <div className="form__button-separator">or continue with</div>}
                <div className="form__social-login">
                    {enableFacebookRegister &&
                        <Button
                            fullWidth
                            label={facebookLoginButtonTitle}
                            className={facebookLoginButtonClassName}
                            onClick={onClickFacebookLogin} />
                    }
                    {enableGoogleRegister &&
                        <Button
                            fullWidth
                            label={googleLoginButtonTitle}
                            id={googleLoginButtonId}
                            className={googleLoginButtonClassName}
                            onClick={onClickGoogleLogin} />
                    }
                </div>
                <div className="form__secondary-action">
                    {secondaryActionText} <a href={loginUrl}>{loginUrlTitle}</a>
                </div>
            </div>
        );
    };

    renderFormStepTwo = (): JSX.Element => {
        const {
            autoFocus,
            isSubmitting,
            firstNamePlaceholder,
            lastNamePlaceholder,
            registerButtonClassName,
            registerButtonTitle,
            passwordInputPlaceholder,
            previousStepLinkText,
        } = this.props;
        const { registerDetails, passwordVisible } = this.state;
        const { firstName, lastName, password } = registerDetails;

        const firstNameError = this.getFormErrorForSpecificProperty("firstName");
        const lastNameError = this.getFormErrorForSpecificProperty("lastName");
        const passwordError = this.getFormErrorForSpecificProperty("password");

        const passwordStrength = this.getPasswordStrength();
        const passwordStrengthText = this.getPasswordStrengthText(passwordStrength);

        return (
            <div className="form-step">
                <div className="form-inputs">
                    <div className={firstNameError ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">First name</div>
                        <input
                            type="text"
                            name="firstname"
                            id="firstname"
                            placeholder={firstNamePlaceholder}
                            value={firstName || ""}
                            onChange={this.handleChangeFirstName}
                            onKeyPress={this.handleKeyPress}
                            className="form-input__input"
                            autoFocus={autoFocus}
                        />
                        {firstNameError && (
                            <div className="form-input__help-text">
                                <span>{firstNameError.message}</span>
                            </div>
                        )}
                    </div>
                    <div className={lastNameError ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">Last name</div>
                        <input
                            type="text"
                            name="lastname"
                            id="lastname"
                            placeholder={lastNamePlaceholder}
                            value={lastName || ""}
                            onChange={this.handleChangeLastName}
                            onKeyPress={this.handleKeyPress}
                            className="form-input__input"
                        />
                        {lastNameError && (
                            <div className="form-input__help-text">
                                <span>{lastNameError.message}</span>
                            </div>
                        )}
                    </div>
                    <div className={passwordError ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">Choose a password</div>
                        <div className="form-input__input-wrapper">
                            <input
                                name="password"
                                id="password"
                                type={passwordVisible ? "text" : "password"}
                                placeholder={passwordInputPlaceholder}
                                value={password || ""}
                                onChange={this.handleChangePassword}
                                onKeyPress={this.handleKeyPress}
                                className="form-input__input"
                            />
                            <img
                                src={
                                    passwordVisible
                                        ? "https://sumosoft.blob.core.windows.net/archibald/shared/icons/password-hidden.svg?v=1214"
                                        : "https://sumosoft.blob.core.windows.net/archibald/shared/icons/password-visible.svg?v=581"
                                }
                                alt="Password visible"
                                className="password-visibility-toggle"
                                onClick={this.handleTogglePasswordVisibility}
                            />
                        </div>
                        <div className="form-input__help-text">
                            <span
                                className={
                                    password && password.length > 0
                                        ? `password-strength password-strength-${passwordStrength} password-strength--visible`
                                        : `password-strength password-strength-${passwordStrength}`
                                }
                            >
                                Password strength: <strong>{passwordStrengthText}</strong>
                            </span>
                            {passwordError && <span>{passwordError.message}</span>}
                        </div>
                    </div>
                </div>
                <Button
                    fullWidth
                    className={registerButtonClassName}
                    onClick={this.handleSubmit}
                    label={isSubmitting
                        ? <React.Fragment>{registerButtonTitle} <i className="fa fa-circle-o-notch fa-pulse fa-fw" /></React.Fragment>
                        : registerButtonTitle}
                    isDisabled={!this.getIsRegisterDetailsValid() || isSubmitting} />
                <span className="form__secondary-action form__secondary-action-cursor" onClick={() => this.handleNavigateToStep(0)}>
                    {`<`} {previousStepLinkText}
                </span>
            </div>
        );
    };

    render() {
        const { formStep } = this.state;
        return (
            <div className="register-form">
                {formStep === 0 && this.renderFormStepOne()}
                {formStep === 1 && this.renderFormStepTwo()}
            </div>
        );
    }
}

RegisterForm.defaultProps = {
    autoFocus: true,
    initialValues: {},
    emailInputPlaceholder: "someone@example.com",
    enableFacebookRegister: true,
    enableGoogleRegister: true,
    facebookLoginButtonTitle: "Facebook",
    firstNamePlaceholder: "John",
    googleLoginButtonTitle: "Google",
    googleLoginButtonId: "google-login-btn",
    lastNamePlaceholder: "Doe",
    loginUrlTitle: "Log in",
    nextStepButtonTitle: "Continue",
    passwordInputPlaceholder: "At least 6 characters",
    previousStepLinkText: "Go back",
    registerButtonTitle: "Create an account",
    secondaryActionText: "Already have an account?",
} as IRegisterFormProps;
