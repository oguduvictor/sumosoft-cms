﻿import * as React from 'react';
import { ICartErrorsProps } from './ICartErrorsProps';
import { CartError } from '../../classes/CartError';

export class CartErrors extends React.Component<ICartErrorsProps> {
	constructor(props) {
		super(props);
	}

	static defaultProps: ICartErrorsProps;

	render() {
		if (this.props.cartErrors.length) {
			return (
				<div className='cart-errors'>
					<ul>
						{this.props.cartErrors.map((e, i) => (
							<li key={i}>{e.message}</li>
						))}
					</ul>
				</div>
			);
		} else {
			return null;
		}
	}
}

CartErrors.defaultProps = {
	cartErrors: new Array<CartError>()
} as ICartErrorsProps;
