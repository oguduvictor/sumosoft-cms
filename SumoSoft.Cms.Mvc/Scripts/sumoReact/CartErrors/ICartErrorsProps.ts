﻿import { CartError } from '../../classes/CartError';

export interface ICartErrorsProps {
	cartErrors: Array<CartError>;
}
