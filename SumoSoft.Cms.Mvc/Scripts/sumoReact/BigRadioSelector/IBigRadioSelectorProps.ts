﻿export interface IBigRadioSelectorProps {
    className?: string;
    isSelected: boolean;
    disabled?: boolean;
    content: React.ReactNode;
    onClick: () => void;
}