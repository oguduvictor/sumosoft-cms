﻿import * as React from "react";
import { IBigRadioSelectorProps } from './IBigRadioSelectorProps';

export class BigRadioSelector extends React.Component<IBigRadioSelectorProps> {
    render() {
        return (
            <div
                className={`big-radio-selector${this.props.isSelected ? " selected" : ""}${this.props.disabled ? " disabled" : ""} ${this.props.className}`}
                onClick={() => this.props.isSelected ? null : this.props.onClick()}>
                <div className="bullet">
                    <div className="inner-bullet"></div>
                </div>
                <div className="content">{this.props.content}</div>
            </div>
        );
    }
}