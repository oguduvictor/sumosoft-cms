﻿import * as React from "react";

import { ProductStockUnit } from "../../classes/ProductStockUnit";
import { Button } from '../Button/Button';

export interface IAddToCartButtonProps {
    productStockUnit: ProductStockUnit;
    outOfStockMessage: string;
    preOrderMessage: string;
    comingSoonMessage: string;
    addToCartMessage: string;
    addToCartButtonClassName?: string;
    handleAddToCart();
}

export class AddToCartButton extends React.Component<IAddToCartButtonProps, undefined> {

    constructor(props) {
        super(props);
    }

    static defaultProps: Partial<IAddToCartButtonProps>;

    // -----------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------

    render() {
        if (this.props.productStockUnit.stock < 1) {
            return (
                <Button
                    className={this.props.addToCartButtonClassName}
                    isDisabled={true}
                    label={this.props.outOfStockMessage} />
            );
        } else {
            if (new Date(this.props.productStockUnit.dispatchDate) > new Date()) {
                if (this.props.productStockUnit.enablePreorder) {
                    return (
                        <Button
                            className={this.props.addToCartButtonClassName}
                            onClick={() => this.props.handleAddToCart()}
                            label={this.props.preOrderMessage} />
                    );
                } else {
                    return (
                        <Button
                            className={this.props.addToCartButtonClassName}
                            isDisabled={true}
                            label={this.props.comingSoonMessage} />
                    );
                }
            } else {
                return (
                    <Button
                        className={this.props.addToCartButtonClassName}
                        onClick={() => this.props.handleAddToCart()}
                        label={this.props.addToCartMessage} />
                );
            }
        }
    }
}

AddToCartButton.defaultProps = {
    addToCartButtonClassName: "add-to-cart-button"
}