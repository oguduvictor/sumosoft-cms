﻿import * as React from "react";

import { IFormResponse } from "../../interfaces/IFormResponse";
import { IFormError } from '../../interfaces/IFormError';
import { Button } from '../Button/Button';

export interface IForgotPasswordFormProps {
    confirmationMessage?: string;
    emailInputPlaceholder?: string;
    formResponse: IFormResponse;
    isSubmitting: boolean;
    loginUrl: string;
    loginUrlTitle?: string;
    resetPasswordButtonClassName?: string;
    resetPasswordButtonTitle?: string;
    title?: string;
    onSubmit: (email: string) => void;
}

interface IForgotPasswordFormState {
    email: string;
}

export class ForgotPasswordForm extends React.Component<IForgotPasswordFormProps, IForgotPasswordFormState> {
    constructor(props: IForgotPasswordFormProps) {
        super(props);

        this.state = {
            email: ""
        } as IForgotPasswordFormState;
    }

    static defaultProps: IForgotPasswordFormProps;

    // ------------------------------------------------------------
    // Helper Methods
    // ------------------------------------------------------------

    getIsEmailValid = (): boolean => {
        const email = this.state.email;
        const emailRegex = /^[^@\s]+@[^@\s]+\.[^@\s]+$/gm;
        return !!email && email.length > 0 && emailRegex.test(email);
    };

    // ------------------------------------------------------------
    // Event Handlers
    // ------------------------------------------------------------

    handleChangeEmail = e => {
        const email = e.target.value;
        this.setState({ email });
    };

    handleSubmit = () => {
        this.getIsEmailValid() && this.props.onSubmit(this.state.email);
    };

    handleKeyPress = e => {
        if (e.key === "Enter") {
            this.handleSubmit();
        }
    };

    // ------------------------------------------------------------
    // Render Methods
    // ------------------------------------------------------------

    renderForgotPasswordForm = (): JSX.Element => {
        const { email } = this.state;
        const { loginUrl, isSubmitting, title, emailInputPlaceholder, resetPasswordButtonClassName, resetPasswordButtonTitle, loginUrlTitle, formResponse } = this.props;
        const { errors } = formResponse;
        return (
            <div className="form-step">
                <div className="form-subtext">{title}</div>
                <div className="form-inputs">
                    <div className={!!errors && errors.length > 0 ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">Email</div>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            placeholder={emailInputPlaceholder}
                            value={email}
                            onChange={this.handleChangeEmail}
                            onKeyPress={this.handleKeyPress}
                            className="form-input__input"
                            autoFocus
                        />
                        {!!errors && errors.length > 0 && (
                            <div className="form-input__help-text">
                                {errors.map(o => <span key={o.message}>{o.message}</span>)}
                            </div>
                        )}
                    </div>
                </div>
                <Button
                    fullWidth
                    label={isSubmitting
                        ? <React.Fragment>{resetPasswordButtonTitle} <i className="fa fa-circle-o-notch fa-pulse fa-fw" /></React.Fragment>
                        : resetPasswordButtonTitle}
                    className={resetPasswordButtonClassName}
                    onClick={this.handleSubmit}
                    isDisabled={!this.getIsEmailValid() || isSubmitting} />
                <a className="form__secondary-action" href={loginUrl}>
                    {`<`} {loginUrlTitle}
                </a>
            </div>
        );
    };

    renderForgotPasswordFormConfirmation = (): JSX.Element => {
        const { confirmationMessage, loginUrl, loginUrlTitle } = this.props;
        return (
            <div className="form-step">
                <div className="form-confirmation">{confirmationMessage}</div>
                <a className="form__secondary-action" href={loginUrl}>
                    {`<`} {loginUrlTitle}
                </a>
            </div>
        );
    };

    render() {
        const { formResponse } = this.props;
        return <div className="forgot-password-form">{formResponse.isValid ? this.renderForgotPasswordFormConfirmation() : this.renderForgotPasswordForm()}</div>;
    }
}

ForgotPasswordForm.defaultProps = {
    confirmationMessage: "An email with your instructions on how to reset your password has been sent to your address.",
    emailInputPlaceholder: "someone@example.com",
    loginUrlTitle: "Go back to login",
    resetPasswordButtonTitle: "Reset password",
    title: "Please insert the email address you used to register the account and press the button below to reset your password."
} as IForgotPasswordFormProps;
