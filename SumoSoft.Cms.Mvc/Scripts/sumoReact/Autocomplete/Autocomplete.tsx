﻿//
//     _____                       _____        __ _   
//    / ____|                     / ____|      / _| |  
//   | (___  _   _ _ __ ___   ___| (___   ___ | |_| |_ 
//    \___ \| | | | '_ ` _ \ / _ \\___ \ / _ \|  _| __|
//    ____) | |_| | | | | | | (_) |___) | (_) | | | |_ 
//   |_____/ \__,_|_| |_| |_|\___/_____/ \___/|_|  \__|
//
//

import * as React from "react";
import { TopLabel } from "../../../Areas/Admin/Scripts/components/TopLabel/TopLabel";

export class AutocompleteOption<T> {
    value: T;
    displayTitle: string;
    displayDescription?: string;

    constructor(value: T, displayTitle: string, displayDescription?: string) {
        this.value = value;
        this.displayTitle = displayTitle;
        this.displayDescription = displayDescription;
    }
}

export interface IAutocompleteProps<T> {
    placeholder?: string;
    label?: string;
    className?: string;
    options: Array<AutocompleteOption<T>>;
    selectedOption: AutocompleteOption<T>;
    handleChangeValue(value: T): void;
}

interface IAutocompleteState {
    filter: string;
    showDropdown: boolean;
}

export class Autocomplete<T> extends React.Component<IAutocompleteProps<T>, IAutocompleteState> {

    constructor(props: IAutocompleteProps<T>) {
        super(props);

        this.state = {
            filter: "",
        } as IAutocompleteState;
    }

    autocompleteRef: HTMLDivElement;

    componentDidMount() {
        this.setState({
            filter: this.props.selectedOption.displayTitle
        });
    }

    componentWillMount() {
        document.addEventListener("mousedown", this.handleDocumentClick, false);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleDocumentClick, false);
    }

    componentWillReceiveProps(nextProps: IAutocompleteProps<T>) {
        if (nextProps.selectedOption.displayTitle !== this.state.filter) {
            this.setState({
                filter: nextProps.selectedOption.displayTitle
            });
        }
    }

    handleDocumentClick = e => {
        if (!this.state.showDropdown || this.autocompleteRef.contains(e.target)) {
            return;
        }

        this.setState({
            showDropdown: false
        });
    };

    handleClickInput() {
        this.setState({
            showDropdown: true
        });
    }

    handleKeydownInput(e) {
        if (e.key === "Enter" && this.getFilteredOptions().length >= 1) {
            this.handleClickOption(this.getFilteredOptions()[0]);
        }
    }

    handleChangeInput(e) {
        this.setState({
            filter: e.target.value
        });
    }

    handleClickOption(option: AutocompleteOption<T>) {
        this.setState({
            filter: option.displayTitle,
            showDropdown: false
        });
        this.props.handleChangeValue(option.value);
    }

    getFilteredOptions(): Array<AutocompleteOption<T>> {
        if (this.props.selectedOption.displayTitle === this.state.filter) {
            return this.props.options;
        }
        return this.props.options.filter(o =>
            o.displayTitle && o.displayTitle.includesSubstring(this.state.filter, true) ||
            o.displayDescription && o.displayDescription.includesSubstring(this.state.filter, true));
    }

    render() {
        const componentClassNames = ["autocomplete"];
        if (this.state.showDropdown) componentClassNames.push("dropdown-open");
        if (this.props.className) componentClassNames.push(this.props.className);

        return (
            <div
                className={componentClassNames.join(" ")} ref={autocompleteRef => this.autocompleteRef = autocompleteRef}>
                <TopLabel label={this.props.label}/>
                <div className="input-wrapper">
                    <input
                        type="text"
                        placeholder={this.props.placeholder}
                        value={this.state.filter || ""}
                        onClick={() => this.handleClickInput()}
                        onKeyDown={(e) => this.handleKeydownInput(e)}
                        onChange={(e) => this.handleChangeInput(e)} />
                    <i className="fa fa-caret-down autocomplete-caret"></i>
                </div>
                <div className="options-dropdown-wrapper">
                    <div className="options-dropdown">
                        {
                            this.getFilteredOptions().map((o, i) =>
                                <div className="option" key={i} onClick={() => this.handleClickOption(o)}>
                                    <div className="display-title">{o.displayTitle}</div>
                                    {
                                        o.displayDescription &&
                                        <div className="display-description">{o.displayDescription}</div>
                                    }
                                </div>
                            )
                        }
                        {
                            this.getFilteredOptions().length < 1 &&
                            <div className="option">
                                <div className="display-title">No match found</div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}