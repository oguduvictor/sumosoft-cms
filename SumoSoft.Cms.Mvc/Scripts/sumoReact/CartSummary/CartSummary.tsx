﻿//
//     _____                       _____        __ _   
//    / ____|                     / ____|      / _| |  
//   | (___  _   _ _ __ ___   ___| (___   ___ | |_| |_ 
//    \___ \| | | | '_ ` _ \ / _ \\___ \ / _ \|  _| __|
//    ____) | |_| | | | | | | (_) |___) | (_) | | | |_ 
//   |_____/ \__,_|_| |_| |_|\___/_____/ \___/|_|  \__|
//
//

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// WITHOUT CREDIT APPLIED:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Items subtotal before tax        100      
// Coupon                           -30       
// Tax                               20       
// Shipping                           5
// Total to be paid                  95

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// WITH CREDIT APPLIED
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Items subtotal before tax        100      
// Coupon                           -30       
// Tax                               20       
// Shipping                           5
// Total after tax                   95
// Credit applied                    50
// Total to be paid                  45

import * as React from "react";
import * as sumoJS from "../../sumoJS";

import { Cart } from "../../classes/Cart";
import { CartShippingBox } from "../../classes/CartShippingBox";
import { CartItem } from "../../classes/CartItem";
import { Button } from '../Button/Button';
import { IFormResponse } from '../../interfaces/IFormResponse';
import { FormResponseComponent } from '../FormResponse/FormResponseComponent';

export interface ICartSummaryProps {
    cart: Cart;
    currencySymbol: string;
    fractionDigits?: number;

    couponInputPlaceholder?: string;
    applyCouponButtonClassName?: string,
    applyCouponButtonContent?: string | JSX.Element,
    removeCouponButtonClassName?: string;
    removeCouponButtonContent?: string | JSX.Element;

    userCreditTotal?: number;
    creditInputPlaceholder?: string;
    applyCreditButtonClassName?: string,
    applyCreditButtonContent?: string | JSX.Element,
    removeCreditButtonClassName?: string;
    removeCreditButtonContent?: string | JSX.Element;

    rowDescriptionItemsSubtotalBeforeTax?: string;
    rowDescriptionCoupon?: string;
    rowDescriptionTax?: string;
    rowDescriptionShipping?: string;
    rowDescriptionTotalAfterTax?: string;
    rowDescriptionCredit?: string;
    rowDescriptionTotalToBePaid?: string;

    handleApplyCoupon?(code: string): void;
    handleRemoveCoupon?(): void;
    handleApplyCredit?(amount: number): void;
    handleRemoveCredit?(): void;
    formResponse?: IFormResponse;
}

export class CartSummary extends React.Component<ICartSummaryProps, undefined> {

    static defaultProps: ICartSummaryProps;

    constructor(props: ICartSummaryProps) {
        super(props);
    }

    getCouponCodeInput(): JQuery {
        return $("#coupon-code-input");
    }

    getCreditAmountInput(): JQuery {
        return $("#credit-amount-input");
    }

    getFormattedPrice(price: number): string {
        return this.props.currencySymbol + price.toFixed(this.props.fractionDigits === undefined ? 2 : this.props.fractionDigits);
    }

    getCartShippingBoxes() {
        return sumoJS.get(() => this.props.cart.cartShippingBoxes, new Array<CartShippingBox>());
    }

    getCartItems() {
        return sumoJS.get(() => this.getCartShippingBoxes().mapMany(cartShippingBox => cartShippingBox.cartItems), new Array<CartItem>());
    }

    getItemsSubtotalBeforeTax(): number {
        return this.getCartItems().sum(x => x.subtotalBeforeTax);
    }

    getCouponValue(): number {
        return sumoJS.get(() => this.props.cart.couponValue, 0);
    }

    getShippingPrice(): number {
        return sumoJS.get(() => this.getCartShippingBoxes().sum(x => x.shippingPriceAfterTax), 0);
    }

    getTotalTax(): number {
        return sumoJS.get(() => this.props.cart.totalAfterTax - this.props.cart.totalBeforeTax, 0);
    }

    getTotalBeforeTax(): number {
        return sumoJS.get(() => this.props.cart.totalBeforeTax, 0);
    }

    getTotalAfterTax(): number {
        return sumoJS.get(() => this.props.cart.totalAfterTax, 0);
    }

    getCreditValue(): number {
        return sumoJS.get(() => this.props.cart.userCredit.amount, 0);
    }

    getTotalToBePaid(): number {
        return sumoJS.get(() => this.props.cart.totalToBePaid, 0);
    }

    handleKeyPress(e: any, callback: () => void) {
        if (e.key === "Enter") {
            callback();
        }
    }

    // -----------------------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------------------

    renderTableRow(rowClassName: string, rowDescription: string, rowContent: string | JSX.Element) {
        return (
            <tr className={rowClassName}>
                <td className="row-description">
                    {rowDescription}
                </td>
                <td className="row-content">
                    {rowContent}
                </td>
            </tr>
        );
    }

    renderCouponRowContent() {
        if (this.getCouponValue() > 0) {
            return (
                <React.Fragment>
                    <Button
                        className={this.props.removeCouponButtonClassName}
                        label={this.props.removeCouponButtonContent}
                        onClick={() => this.props.handleRemoveCoupon()} />
                    {` - ${this.getFormattedPrice(this.getCouponValue())}`}
                </React.Fragment>
            );
        } else {
            if (this.props.handleApplyCoupon) {
                return (
                    <React.Fragment>
                        <input
                            id="coupon-code-input"
                            disabled={!this.props.handleApplyCoupon}
                            placeholder={this.props.couponInputPlaceholder}
                            onKeyPress={(e) => this.handleKeyPress(e, () => this.props.handleApplyCoupon(this.getCouponCodeInput().val().toString()))} />
                        <Button
                            className={this.props.applyCouponButtonClassName}
                            isDisabled={!this.props.handleApplyCoupon}
                            label={this.props.applyCouponButtonContent}
                            onClick={() => this.props.handleApplyCoupon(this.getCouponCodeInput().val().toString())} />
                        <FormResponseComponent formResponse={this.props.formResponse} />
                    </React.Fragment>
                );
            } else {
                return ""
            }
        }
    }

    renderCreditRowContent() {
        if (this.getCreditValue() < 0) {
            return (
                <React.Fragment>
                    <Button
                        className={this.props.removeCreditButtonClassName}
                        label={this.props.removeCreditButtonContent}
                        onClick={() => this.props.handleRemoveCredit()} />
                    {` - ${this.getFormattedPrice(Math.abs(this.getCreditValue()))}`}
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>
                    <input
                        id="credit-amount-input"
                        disabled={!this.props.handleApplyCredit}
                        placeholder={this.props.creditInputPlaceholder}
                        type="number"
                        min="0"
                        onKeyPress={(e) => this.handleKeyPress(e, () => this.props.handleApplyCredit(Number(this.getCreditAmountInput().val()) || 0))} />
                    <Button
                        className={this.props.applyCreditButtonClassName}
                        isDisabled={!this.props.handleApplyCredit}
                        label={<i className="fa fa-angle-right"></i>}
                        onClick={() => this.props.handleApplyCredit(Number(this.getCreditAmountInput().val()) || 0)} />
                </React.Fragment>
            );
        }
    }

    render() {
        return (
            <div className="cart-summary">
                <table>
                    <tbody>
                        {
                            this.renderTableRow("items-subtotal-before-tax", this.props.rowDescriptionItemsSubtotalBeforeTax, this.getFormattedPrice(this.getItemsSubtotalBeforeTax()))
                        }
                        {
                            (this.props.handleApplyCoupon || this.getCouponValue() > 0) &&
                            this.renderTableRow("coupon", this.props.rowDescriptionCoupon, this.renderCouponRowContent())
                        }
                        {
                            this.renderTableRow("tax", this.props.rowDescriptionTax, this.getFormattedPrice(this.getTotalTax()))
                        }
                        {
                            this.renderTableRow("shipping", this.props.rowDescriptionShipping, this.getFormattedPrice(this.getShippingPrice()))
                        }
                        {
                            this.getCreditValue() < 0 &&
                            this.renderTableRow("total-after-tax", this.props.rowDescriptionTotalAfterTax, this.getFormattedPrice(this.getTotalAfterTax()))
                        }
                        {
                            ((this.props.handleApplyCredit && this.props.userCreditTotal) || this.getCreditValue() < 0) &&
                            this.renderTableRow("credit", `${this.props.rowDescriptionCredit} (${this.props.currencySymbol}${this.props.userCreditTotal} available)`, this.renderCreditRowContent())
                        }
                        {
                            this.renderTableRow("total-to-be-paid", this.props.rowDescriptionTotalToBePaid, this.getFormattedPrice(this.getTotalToBePaid()))
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

CartSummary.defaultProps = {
    cart: new Cart(),
    currencySymbol: "",
    fractionDigits: 2,
    canApplyCoupon: true,
    canApplyCredit: true,
    rowDescriptionItemsSubtotalBeforeTax: "Items subtotal before tax",
    rowDescriptionCoupon: "Apply coupon",
    rowDescriptionTax: "Tax",
    rowDescriptionShipping: "Shipping",
    rowDescriptionTotalAfterTax: "Total",
    rowDescriptionCredit: "Apply credit",
    rowDescriptionTotalToBePaid: "Total to be paid",
    couponInputPlaceholder: "Coupon code",
    applyCouponButtonContent: "Apply",
    removeCouponButtonContent: "Remove",
    disableCouponForm: false,
    userCreditTotal: 0,
    creditInputPlaceholder: "0.00",
    applyCreditButtonContent: "Apply",
    removeCreditButtonContent: "Remove",
    disableCredifForm: false,
    applyCouponButtonClassName: "apply-coupon-button",
    applyCreditButtonClassName: "apply-credit-button",
    removeCouponButtonClassName: "remove-coupon-button",
    removeCreditButtonClassName: "remove-credit-button",
    formResponse: {} as IFormResponse
} as ICartSummaryProps;