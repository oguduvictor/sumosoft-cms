﻿import * as React from "react";

export interface IBootstrapModalComponentProps {
    id: string;
    modalContent: JSX.Element;
}

export var BootstrapModalComponent: (props: IBootstrapModalComponentProps) => JSX.Element = props => {
    return (
        <div className="bootstrap-modal-component modal fade" id={props.id} role="dialog">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    {
                        props.modalContent
                    }
                </div>
            </div>
        </div>
    );
}