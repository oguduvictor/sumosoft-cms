﻿export interface IStructuredDataBrandProps {
    brand: string;
    logo: string;
    url: string;
    socialProfiles: string[];
    contacts: IStructuredDataContactPoint[];
    address?: IStructureDataAddress;
    aggregateRating?: IStructuredDataAggregateRating;
}

interface IStructuredDataContactPoint {
    telephone: string;
    contactType: string;
    areaServed: string[];
}

interface IStructureDataAddress {
    locality: string;
    region: string;
    postalCode: string;
    streetAddress: string;
}

interface IStructuredDataAggregateRating {
    ratingValue: number;
    reviewCount: number;
}

export var StructuredDataForBrand: (props: IStructuredDataBrandProps) => JSX.Element = props => {
    let structureDataMarkup : any = {
        "@context": "http://schema.org",
        "@type": "Organization",
        "logo": props.logo,
        "name": props.brand,
        "brand": {
            "@type": "Brand",
            "name": props.brand,
            "logo": props.logo
        },
        "url": props.url,
        "sameAs": props.socialProfiles,
        "contactPoint": props.contacts && props.contacts.map(contact => ({
            "@type": "ContactPoint",
            "telephone": contact.telephone,
            "contactType": contact.contactType,
            "areaServed": contact.areaServed
        }))
    };

    if (props.address) {
        structureDataMarkup = {
            ...structureDataMarkup,
            "address": {
                "@type": "PostalAddress",
                "addressLocality": props.address.locality,
                "addressRegion": props.address.region,
                "postalCode": props.address.postalCode,
                "streetAddress": props.address.streetAddress
            }
        }
    }

    if (props.aggregateRating) {
        structureDataMarkup = {
            ...structureDataMarkup,
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": props.aggregateRating.ratingValue,
                "reviewCount": props.aggregateRating.reviewCount
            }
        };
    }

    let structureDataMarkupScript = document.getElementById("organizationStructureDataMarkup") as HTMLScriptElement;

    if (!structureDataMarkupScript) {
        structureDataMarkupScript = document.createElement("script");
        structureDataMarkupScript.id = "organizationStructureDataMarkup";
        structureDataMarkupScript.type = "application/ld+json";
        structureDataMarkupScript.async = true;
    }

    structureDataMarkupScript.innerHTML = JSON.stringify(structureDataMarkup);

    document.body.appendChild(structureDataMarkupScript);

    return null;
}
