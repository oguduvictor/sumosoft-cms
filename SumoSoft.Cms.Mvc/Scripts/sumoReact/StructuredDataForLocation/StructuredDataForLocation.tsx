﻿import * as React from "react";

export interface IStructuredDataLocationProps {
    description: string;
    name: string;
    openingHours: string[];
    url: string;
    image: string;
    telephone: string;
    address: IStructureDataAddress;
}

interface IStructureDataAddress {
    locality: string;
    region: string;
    postalCode: string;
    streetAddress: string;
}

export var StructuredDataForLocation: (props: IStructuredDataLocationProps) => JSX.Element = props => {
    const structureDataMarkup = {
        "@context": "http://schema.org",
        "@type": "LocalBusiness",
        "address": !props.address ? null : {
            "@type": "PostalAddress",
            "addressLocality": props.address.locality,
            "addressRegion": props.address.region,
            "postalCode": props.address.postalCode,
            "streetAddress": props.address.streetAddress
        },
        "description": props.description,
        "image": props.image,
        "name": props.name,
        "openingHours": props.openingHours,
        "telephone": props.telephone,
        "url": props.url
    };

    let structureDataMarkupScript = document.getElementById("locationStructureDataMarkup") as HTMLScriptElement;

    if (!structureDataMarkupScript) {
        structureDataMarkupScript = document.createElement("script");
        structureDataMarkupScript.id = "locationStructureDataMarkup";
        structureDataMarkupScript.type = "application/ld+json";
        structureDataMarkupScript.async = true;
    }

    structureDataMarkupScript.innerHTML = JSON.stringify(structureDataMarkup);

    document.body.appendChild(structureDataMarkupScript);

    return null;
}
