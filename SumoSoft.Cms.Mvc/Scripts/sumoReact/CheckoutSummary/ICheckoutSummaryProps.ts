﻿import { Address } from '../../classes/Address';
import { CartItem } from '../../classes/CartItem';
import { IBraintreePaymentMethod } from '../../interfaces/IBraintreePaymentMethod';

export interface ICheckoutSummaryProps {
    shippingAddress: Address;
    billingAddress: Address;
    braintreePaymentMethod: IBraintreePaymentMethod;
    cartItems: Array<CartItem>;
    currencySymbol: string;
    noShippingAddressLabel?: string | JSX.Element;
    shippingAddressLabel?: string;
    freeShippingPriceLabel?: string;
    paymentMethodLabel?: string;
    billingAddressLabel?: string;
    shippingMethodButtonClassName?: string;
    handleClickShippingAddress?: () => void;
    handleClickShippingMethod?: () => void;
    handleClickBillingAddress?: () => void;
    handleClickPaymentMethod?: () => void;
}