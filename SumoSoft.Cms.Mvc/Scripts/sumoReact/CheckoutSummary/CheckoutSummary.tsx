﻿import * as React from "react";
import { ICheckoutSummaryProps } from './ICheckoutSummaryProps';
import { ICheckoutSummaryState } from './ICheckoutSummaryState';
import { CartItemComponent } from '../CartItem/CartItem';
import { Address } from '../../classes/Address';
import { Button } from '../Button/Button';
import { ShippingBox } from '../../classes/ShippingBox';
import { CartItem } from '../../classes/CartItem';

export class CheckoutSummary extends React.Component<ICheckoutSummaryProps, ICheckoutSummaryState>{

    static defaultProps: Partial<ICheckoutSummaryProps>;

    getShippingMethodSentence(shippingBox: ShippingBox) {
        return `${shippingBox.localizedTitle} - ${shippingBox.localizedPrice > 0 ? this.props.currencySymbol + shippingBox.localizedPrice : this.props.freeShippingPriceLabel}`
    }

    renderTextLine(text: string) {
        return text ? <div>{text}</div> : null;
    }

    renderAddress(address: Address) {
        if (!address) {
            return this.props.noShippingAddressLabel;
        }
        return (
            <React.Fragment>
                {this.renderTextLine(`${address.firstName} ${address.lastName}`)}
                {this.renderTextLine(address.addressLine1)}
                {this.renderTextLine(address.addressLine2)}
                {this.renderTextLine(`${address.postcode} ${address.city}`)}
                {this.renderTextLine(address.country && address.country.name)}
            </React.Fragment>
        );
    }


    renderCartItemAdditionalInformation(cartItem: CartItem) {
        if (this.props.handleClickShippingMethod) {
            return <Button
                className={this.props.shippingMethodButtonClassName}
                label={this.getShippingMethodSentence(cartItem.cartShippingBox.shippingBox)}
                onClick={this.props.handleClickShippingMethod} />
        }
        return null;
    }

    render() {
        return (
            <div className="checkout-summary">
                <div className={`clickable-area${!!this.props.handleClickShippingAddress ? "" : " readonly"}`} onClick={this.props.handleClickShippingAddress}>
                    <div className="title">{this.props.shippingAddressLabel}{this.props.handleClickShippingAddress && ` ›`}</div>
                    {this.renderAddress(this.props.shippingAddress)}
                </div>
                <div className={`clickable-area${!!this.props.handleClickPaymentMethod ? "" : " readonly"}`} onClick={this.props.handleClickPaymentMethod}>
                    <div className="title">{this.props.paymentMethodLabel}{this.props.handleClickShippingAddress && ` ›`}</div>
                    {this.renderTextLine(this.props.braintreePaymentMethod.type === "CreditCard" ? `${this.props.braintreePaymentMethod.details.cardType} •••• ${this.props.braintreePaymentMethod.details.lastFour}` : "Paypal")}
                    {this.props.braintreePaymentMethod.type !== "CreditCard" && this.renderTextLine(this.props.braintreePaymentMethod.details.email)}
                </div>
                {
                    this.props.billingAddress &&
                    <div className={`clickable-area${!!this.props.handleClickBillingAddress ? "" : " readonly"}`} onClick={this.props.handleClickBillingAddress}>
                        <div className="title">{this.props.billingAddressLabel}{this.props.handleClickShippingAddress && ` ›`}</div>
                        {this.renderAddress(this.props.billingAddress)}
                    </div>
                }
                <div className="items-summary">
                    <div className="title">Your items</div>
                    {
                        this.props.cartItems.map(cartItem =>
                            <div className="item" key={cartItem.id}>
                                <CartItemComponent
                                    cartItem={cartItem}
                                    currencySymbol={this.props.currencySymbol}
                                    additionalInformation={this.renderCartItemAdditionalInformation(cartItem)} />
                            </div>)
                    }
                </div>
            </div>
        );
    }
}

CheckoutSummary.defaultProps = {
    noShippingAddressLabel: "Collect in store",
    shippingAddressLabel: "Shipping address",
    freeShippingPriceLabel: "Free",
    paymentMethodLabel: "Payment method",
    billingAddressLabel: "Billing address",
    shippingMethodButtonClassName: "",
}