﻿import * as React from "react";
import { VariantOption } from "../../classes/VariantOption";
import { SelectComponent, ISelectProps } from "../Select/SelectComponent";

export interface IVariantOptionDropdownSelectorProps {
    label?: string;
    variantOptions: Array<VariantOption>;
    value: VariantOption;
    neutralOption?: string;
    className?: string;
    handleChangeValue(variantOption: VariantOption): void;
    disableVariantOptionsWhere?(variantOption: VariantOption): boolean;
}

export var VariantOptionDropdownSelector: (props: IVariantOptionDropdownSelectorProps) => JSX.Element = props => {
    return (
        <div className={`variant-option-dropdown-selector ${props.className ? props.className : ""}`}>
            <SelectComponent {...{
                label: props.label,
                value: props.value && props.value.id,
                neutralOption: props.neutralOption && <option value="">{props.neutralOption}</option>,
                options: props.variantOptions.map(x => <option key={x.id} value={x.id}>{x.localizedTitle}</option>),
                handleChangeValue: (e) => props.handleChangeValue(props.variantOptions.filter(x => x.id === e.target.value)[0])
            } as ISelectProps} />
        </div>
    );
}