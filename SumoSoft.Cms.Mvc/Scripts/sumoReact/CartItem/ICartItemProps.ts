﻿import { CartItem } from '../../classes/CartItem';

export interface ICartItemProps {
    cartItem: CartItem;
    currencySymbol: string;
    productUrl?: string;
    removeButtonClassName?: string;
    additionalInformation?: string | JSX.Element;
    handleChangeQuantity?(quantity: number): void;
    handleRemove?(): void;
}
