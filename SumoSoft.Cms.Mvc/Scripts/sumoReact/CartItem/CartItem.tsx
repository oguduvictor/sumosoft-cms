﻿import * as React from "react";
import * as dayjs from "dayjs";

import { ICartItemProps } from "./ICartItemProps";
import { Button } from '../Button/Button';

export class CartItemComponent extends React.Component<ICartItemProps, undefined> {

    getQuantityOptions() {
        var options = [1];
        for (var i = 2; i <= this.props.cartItem.getStock(); i++) {
            options.push(i);
        }
        return options.filter(x => x < 21);
    }

    getCartItemVariants() {
        return this.props.cartItem.cartItemVariants.filter(civ => civ.variantOptionValue);
    }

    getEtaSentence() {
        if (this.props.cartItem.minEta === this.props.cartItem.maxEta) {
            return `Estimated delivery ${dayjs(this.props.cartItem.minEta).format("MMM D")}`
        } else {
            return `Estimated delivery ${dayjs(this.props.cartItem.minEta).format("MMM D")} - ${dayjs(this.props.cartItem.maxEta).format("MMM D")}`;
        }
    }

    render() {
        return (
            <div className="cart-item">
                <a className="product-image-link" href={this.props.productUrl}>
                    <img className="product-image" src={this.props.cartItem.image} />
                </a>
                <div className="product-info">
                    <div className="product-title-and-price-wrapper">
                        <div className="product-title">
                            {this.props.cartItem.product.localizedTitle}
                        </div>
                        <div className="product-price">
                            {this.props.cartItem.totalAfterTax ? `${this.props.currencySymbol}${this.props.cartItem.totalAfterTax.toFixed(2)}` : "Free"}
                        </div>
                    </div>
                    {
                        this.getCartItemVariants().length &&
                        <div className="variants">
                            {
                                this.getCartItemVariants().map(civ =>
                                    <div
                                        key={civ.id}
                                        data-variantname={civ.variant.name}>
                                        {`${civ.variant.localizedTitle}: ${civ.variantOptionValue.localizedTitle}`}
                                    </div>
                                )
                            }
                        </div>
                    }
                    <div className="eta">
                        {this.getEtaSentence()}
                    </div>
                    {
                        this.props.additionalInformation &&
                        <div className="additional-information">
                            {this.props.additionalInformation}
                        </div>
                    }
                    {
                        (this.props.handleChangeQuantity || this.props.handleRemove) &&
                        <div className="controls">
                            {
                                this.props.handleChangeQuantity &&
                                <div className="quantity">
                                    <label>
                                        Quantity
                                </label>
                                    <select
                                        value={this.props.cartItem.quantity}
                                        onChange={e => this.props.handleChangeQuantity(Number(e.target.value))}>
                                        {
                                            this.getQuantityOptions().map((x) => <option key={x}>{x}</option>)
                                        }
                                    </select>
                                </div>
                            }
                            {
                                this.props.handleRemove &&
                                <Button
                                    className={"remove " + this.props.removeButtonClassName}
                                    onClick={this.props.handleRemove}
                                    label="Remove"
                                    requireConfirmation />
                            }
                        </div>
                    }
                </div>
            </div>
        );
    }
}