﻿import { Address } from "../../classes/address";

export interface IAddressSelectorState {
    addressFormValue: Address;
    showAddressForm: boolean;
}