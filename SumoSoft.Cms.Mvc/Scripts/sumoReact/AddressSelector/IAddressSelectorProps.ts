﻿import { Address } from "../../classes/address";
import { Country } from '../../classes/Country';
import { IFormResponse } from '../../interfaces/IFormResponse';

export interface IAddressSelectorProps {
    addresses?: Array<Address>;
    value?: Address;
    allCountries?: Array<Country>;
    addressFormResponse?: IFormResponse
    addAddressLabel?: string;
    collectInStoreLabel?: string;
    proceedButtonLabel?: string;
    proceedButtonClassName?: string;
    editButtonClassName?: string;
    deleteButtonClassName?: string;
    addressFormSumbitButtonClassName?: string;
    addressFormCancelButtonClassName?: string;
    onChangeValue?(address: Address): JQueryPromise<IFormResponse>;
    onAddOrUpdateAddress?(address: Address): JQueryPromise<IFormResponse>;
    onDeleteAddress?(address: Address): void;
    onProceed?(): void;
    onCollectInStore?(): void;
    onAddressFormShown?(address: Address): void;
    onAddressFormHidden?(address: Address): void;
}