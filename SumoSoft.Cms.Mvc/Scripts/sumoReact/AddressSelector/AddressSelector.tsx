﻿import * as React from "react";
import { AddressForm } from '../AddressForm/AddressForm';
import { Country } from '../../classes/Country';
import { Button } from '../Button/Button';
import { BigRadioSelector } from '../BigRadioSelector/BigRadioSelector';
import { IAddressSelectorProps } from './IAddressSelectorProps';
import { IAddressSelectorState } from './IAddressSelectorState';
import { Address } from '../../classes/Address';

export class AddressSelector extends React.Component<IAddressSelectorProps, IAddressSelectorState> {

    constructor(props: IAddressSelectorProps) {
        super(props);

        this.state = {
            addressFormValue: new Address()
        } as IAddressSelectorState;
    }

    static defaultProps: IAddressSelectorProps;

    handleDeleteAddress(address: Address) {
        // todo: ask for confirmation first
        this.props.onDeleteAddress(address);
    }

    handleChangeValue(address: Address) {
        this.props.onChangeValue(address).done(formResponse => {
            if (!formResponse.isValid) {
                this.handleShowAddressForm(address);
            }
        });
    }

    handleAddOrUpdateAddress(address: Address) {
        this.props.onAddOrUpdateAddress(address).done(formResponse => {
            if (formResponse.isValid) {
                this.handleHideAddressForm();
            }
        });
    }

    handleShowAddressForm(address: Address) {
        this.setState({
            addressFormValue: address,
            showAddressForm: true
        }, () => this.props.onAddressFormShown && this.props.onAddressFormShown(address));
    }

    handleHideAddressForm() {
        this.setState({
            showAddressForm: false
        }, () => this.props.onAddressFormHidden && this.props.onAddressFormHidden(this.state.addressFormValue));
    }

    renderTextLine(text: string) {
        return text ? <div>{text}</div> : null;
    }

    render() {
        return (
            <div className="address-selector">
                {
                    this.state.showAddressForm &&
                    <div className="edit-modal">
                        <div className="edit-modal-content">
                            <AddressForm
                                value={this.state.addressFormValue}
                                handleChangeValue={address => this.setState({ addressFormValue: address })}
                                formResponse={this.props.addressFormResponse}
                                allCountries={this.props.allCountries}
                            />
                            <div className="edit-modal-buttons">
                                <Button
                                    className={this.props.addressFormSumbitButtonClassName}
                                    onClick={() => this.handleAddOrUpdateAddress(this.state.addressFormValue)}
                                    label="Save" />
                                <Button
                                    className={this.props.addressFormCancelButtonClassName}
                                    onClick={() => this.handleHideAddressForm()}
                                    label="Cancel" />
                            </div>
                        </div>
                    </div>
                }
                {
                    !this.state.showAddressForm &&
                    <React.Fragment>
                        {
                            this.props.addresses.length > 0 &&
                            <div className="addresses">
                                {this.props.addresses.map(address => {
                                    var isSelected = this.props.value && this.props.value.id === address.id;
                                    return <div
                                        key={address.id}
                                        className={isSelected ? "address selected" : "address"}>
                                        <BigRadioSelector
                                            isSelected={isSelected}
                                            onClick={() => this.handleChangeValue(address)}
                                            content={
                                                <React.Fragment>
                                                    {this.renderTextLine(`${address.firstName} ${address.lastName}`)}
                                                    {this.renderTextLine(address.addressLine1)}
                                                    {this.renderTextLine(address.addressLine2)}
                                                    {this.renderTextLine(`${address.postcode} ${address.city}`)}
                                                    {this.renderTextLine(address.country && address.country.name)}
                                                </React.Fragment>
                                            } />
                                        <div className="buttons">
                                            <Button
                                                className={"proceed " + this.props.proceedButtonClassName}
                                                onClick={this.props.onProceed}
                                                label={this.props.proceedButtonLabel} />
                                            <Button
                                                className={"edit " + this.props.editButtonClassName}
                                                onClick={() => this.handleShowAddressForm(address)}
                                                label="Edit" />
                                            <Button
                                                className={"delete " + this.props.deleteButtonClassName}
                                                onClick={() => this.handleDeleteAddress(address)}
                                                label="Delete" />
                                        </div>
                                    </div>
                                })}
                            </div>
                        }
                        <div
                            className="add-address"
                            onClick={() => this.handleShowAddressForm(new Address({ isNew: true }))}>
                            {this.props.addAddressLabel}
                            <span className="caret-right">›</span>
                        </div>
                        {
                            this.props.onCollectInStore &&
                            <div
                                className="collect-in-store"
                                onClick={this.props.onCollectInStore}>
                                {this.props.collectInStoreLabel}
                                <span className="caret-right">›</span>
                            </div>
                        }
                    </React.Fragment>
                }
            </div>
        );
    }
}

AddressSelector.defaultProps = {
    proceedButtonLabel: "Select this address",
    addAddressLabel: "Add new address",
    collectInStoreLabel: "Collect in store",
    allCountries: new Array<Country>()
} as IAddressSelectorProps;
