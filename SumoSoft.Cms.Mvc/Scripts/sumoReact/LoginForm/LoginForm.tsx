﻿import * as React from "react";

import { IFormResponse } from "../../interfaces/IFormResponse";
import { IFormError } from "../../interfaces/IFormError";
import { Button } from '../Button/Button';
import { IRegisterDetails } from '../RegisterForm/RegisterForm';

export interface ILoginDetails {
    email?: string;
    password?: string;
}

export interface ILoginFormProps {
    autoFocus?: boolean;
    initialValues: ILoginDetails;
    emailInputPlaceholder?: string;
    enableFacebookLogin?: boolean;
    enableGoogleLogin?: boolean;
    facebookLoginButtonClassName?: string;
    facebookLoginButtonTitle?: string;
    forgotPasswordText?: string;
    forgotPasswordUrl?: string;
    formResponse: IFormResponse;
    googleLoginButtonClassName?: string;
    googleLoginButtonId?: string;
    googleLoginButtonTitle?: string;
    isSubmitting: boolean;
    loginButtonClassName?: string;
    loginButtonTitle?: string;
    passwordInputPlaceholder?: string;
    registerUrl: string;
    registerUrlTitle?: string;
    secondaryActionText?: string;
    showForgotPassword?: boolean;
    onClickFacebookLogin?: () => void;
    onClickGoogleLogin?: () => void;
    onSubmit: (loginDetails: ILoginDetails) => void;
}

export interface ILoginFormState {
    loginDetails: ILoginDetails;
}

export class LoginForm extends React.Component<ILoginFormProps, ILoginFormState> {
    constructor(props: ILoginFormProps) {
        super(props);

        this.state = {
            loginDetails: this.props.initialValues || {} as ILoginDetails,
            isEmailValid: true
        } as ILoginFormState;
    }

    static defaultProps: ILoginFormProps;

    // ------------------------------------------------------------
    // Helper Methods
    // ------------------------------------------------------------

    getIsLoginDetailsValid = (): boolean => {
        const { loginDetails } = this.state;
        const { email, password } = loginDetails;
        const emailRegex = /^[^@\s]+@[^@\s]+\.[^@\s]+$/gm;
        return email && email.length && emailRegex.test(email) && password && password.length > 0;
    };

    getFormErrorForSpecificProperty(propertyName: string): IFormError {
        const formErrors = this.props.formResponse.errors || new Array<IFormError>();
        return formErrors.filter(x => x.propertyName && x.propertyName.toLowerCase() === propertyName.toLowerCase())[0];
    }

    // ------------------------------------------------------------
    // Event handlers
    // ------------------------------------------------------------

    handleChangeEmail = e => {
        const loginDetails = this.state.loginDetails;
        loginDetails.email = e.target.value;

        this.setState({ loginDetails });
    };

    handleChangePassword = e => {
        const loginDetails = this.state.loginDetails;
        loginDetails.password = e.target.value;

        this.setState({ loginDetails });
    };

    handleKeyPress = e => {
        if (e.key === "Enter") {
            this.handleSubmit();
        }
    };

    handleSubmit = () => {
        this.props.onSubmit(this.state.loginDetails);
    };

    // ------------------------------------------------------------
    // Render Methods
    // ------------------------------------------------------------

    render() {
        const {
            autoFocus,
            emailInputPlaceholder,
            enableFacebookLogin,
            enableGoogleLogin,
            facebookLoginButtonClassName,
            facebookLoginButtonTitle,
            forgotPasswordText,
            forgotPasswordUrl,
            googleLoginButtonClassName,
            googleLoginButtonTitle,
            googleLoginButtonId,
            isSubmitting,
            loginButtonClassName,
            loginButtonTitle,
            onClickFacebookLogin,
            onClickGoogleLogin,
            passwordInputPlaceholder,
            registerUrl,
            registerUrlTitle,
            secondaryActionText,
            showForgotPassword
        } = this.props;
        const { loginDetails } = this.state;
        const { email, password } = loginDetails;

        const emailError = this.getFormErrorForSpecificProperty("email");
        const passwordError = this.getFormErrorForSpecificProperty("password");

        return (
            <div className="login-form">
                <div className="form-inputs">
                    <div className={emailError ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">Email</div>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            placeholder={emailInputPlaceholder}
                            className="form-input__input"
                            autoFocus={autoFocus}
                            value={email || ""}
                            onChange={this.handleChangeEmail}
                            onKeyPress={this.handleKeyPress}
                        />
                        {emailError && (
                            <div className="form-input__help-text">
                                <span>{emailError.message}</span>
                            </div>
                        )}
                    </div>
                    <div className={passwordError ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">Password</div>
                        <input
                            type="password"
                            name="password"
                            id="password"
                            placeholder={passwordInputPlaceholder}
                            className="form-input__input"
                            value={password || ""}
                            onChange={this.handleChangePassword}
                            onKeyPress={this.handleKeyPress}
                        />
                        {passwordError && (
                            <div className="form-input__help-text">
                                <span>{passwordError.message}</span>
                            </div>
                        )}
                        {showForgotPassword && (
                            <div className="form-input__help-text">
                                <a href={forgotPasswordUrl}>{forgotPasswordText}</a>
                            </div>
                        )}
                    </div>
                </div>
                <Button
                    fullWidth
                    label={isSubmitting
                        ? <React.Fragment>{loginButtonTitle} <i className="fa fa-circle-o-notch fa-pulse fa-fw" /></React.Fragment>
                        : loginButtonTitle}
                    className={loginButtonClassName}
                    onClick={this.handleSubmit}
                    isDisabled={!this.getIsLoginDetailsValid() || isSubmitting} />
                {(enableFacebookLogin || enableGoogleLogin) && <div className="form__button-separator">or continue with</div>}
                <div className="form__social-login">
                    {enableFacebookLogin &&
                        <Button
                            fullWidth
                            className={facebookLoginButtonClassName}
                            onClick={onClickFacebookLogin}
                            label={facebookLoginButtonTitle} />
                    }
                    {enableGoogleLogin &&
                        <Button
                            fullWidth
                            className={googleLoginButtonClassName}
                            onClick={onClickGoogleLogin}
                            label={googleLoginButtonTitle} />
                    }
                </div>
                <div className="form__secondary-action">
                    {secondaryActionText} <a href={registerUrl}>{registerUrlTitle}</a>
                </div>
            </div>
        );
    }
}

LoginForm.defaultProps = {
    autoFocus: true,
    initialValues: {},
    emailInputPlaceholder: "someone@example.com",
    enableFacebookLogin: true,
    enableGoogleLogin: true,
    facebookLoginButtonClassName: "",
    facebookLoginButtonTitle: "Facebook",
    forgotPasswordText: "Forgotten your password?",
    googleLoginButtonClassName: "",
    googleLoginButtonTitle: "Google",
    googleLoginButtonId: "google-login-btn",
    loginButtonClassName: "",
    loginButtonTitle: "Log in",
    passwordInputPlaceholder: "••••••••",
    registerUrlTitle: "Register",
    secondaryActionText: "Don't have an account?",
    showForgotPassword: true
} as ILoginFormProps;
