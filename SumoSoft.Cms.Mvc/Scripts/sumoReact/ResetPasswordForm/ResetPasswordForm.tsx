﻿import * as React from "react";

import { IFormResponse } from "../../interfaces/IFormResponse";
import { IFormError } from "../../interfaces/IFormError";
import { Button } from '../Button/Button';

declare var zxcvbn;

export interface IResetPasswordDetails {
    password: string;
    confirmPassword: string;
}
export interface IResetPasswordFormProps {
    confirmPasswordInputPlaceholder?: string;
    formResponse: IFormResponse;
    isSubmitting: boolean;
    passwordInputPlaceholder?: string;
    resetPasswordButtonClassName?: string;
    resetPasswordButtonTitle?: string;
    onSubmit: (resetPasswordDetails: IResetPasswordDetails) => void;
}

interface IResetPasswordFormState {
    zxcvbnIsLoaded: boolean;
    resetPasswordDetails: IResetPasswordDetails;
}

export class ResetPasswordForm extends React.Component<IResetPasswordFormProps, IResetPasswordFormState> {
    constructor(props: IResetPasswordFormProps) {
        super(props);

        this.state = {
            zxcvbnIsLoaded: false,
            resetPasswordDetails: {} as IResetPasswordDetails
        } as IResetPasswordFormState;
    }

    static defaultProps: IResetPasswordFormProps;

    // ------------------------------------------------------------
    // Component lifecycle
    // ------------------------------------------------------------

    componentDidMount() {
        $.getScript("https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js", () => {
            this.setState({
                zxcvbnIsLoaded: true
            })
        });
    }

    // ------------------------------------------------------------
    // Helper Methods
    // ------------------------------------------------------------

    getIsResetPasswordDetailsValid = (): boolean => {
        const { password, confirmPassword } = this.state.resetPasswordDetails;
        return !!password && !!confirmPassword && password.length >= 6 && confirmPassword.length >= 6;
    };

    getFormErrorForSpecificProperty(propertyName: string): IFormError {
        const formErrors = this.props.formResponse.errors || new Array<IFormError>();
        return formErrors.filter(x => x.propertyName && x.propertyName.toLowerCase() === propertyName.toLowerCase())[0];
    }

    getPasswordStrength = (): number => {
        const { password } = this.state.resetPasswordDetails;
        if (!password || !this.state.zxcvbnIsLoaded) return 0;
        return zxcvbn(password).score;
    };

    getPasswordStrengthText = (passwordStrength: number): string => {
        const texts = ["poor", "poor", "average", "strong", "very strong"];
        return texts[passwordStrength];
    };

    // ------------------------------------------------------------
    // Event Handlers
    // ------------------------------------------------------------

    handleChangePassword = e => {
        const resetPasswordDetails = this.state.resetPasswordDetails;
        resetPasswordDetails.password = e.target.value;

        this.setState({ resetPasswordDetails });
    };

    handleChangeConfirmPassword = e => {
        const resetPasswordDetails = this.state.resetPasswordDetails;
        resetPasswordDetails.confirmPassword = e.target.value;

        this.setState({ resetPasswordDetails });
    };

    handleSubmit = () => {
        this.getIsResetPasswordDetailsValid() && this.props.onSubmit(this.state.resetPasswordDetails);
    };

    handleKeyPress = e => {
        if (e.key === "Enter") {
            this.handleSubmit();
        }
    };

    // ------------------------------------------------------------
    // Render Methods
    // ------------------------------------------------------------

    render() {
        const { password, confirmPassword } = this.state.resetPasswordDetails;
        const { confirmPasswordInputPlaceholder, isSubmitting, passwordInputPlaceholder, resetPasswordButtonClassName, resetPasswordButtonTitle } = this.props;

        const passwordStrength = this.getPasswordStrength();
        const passwordStrengthText = this.getPasswordStrengthText(passwordStrength);

        const confirmPasswordError = this.getFormErrorForSpecificProperty("confirmPassword");

        return (
            <div className="reset-password-form">
                <div className="form-inputs">
                    <div className="form-input">
                        <div className="form-input__label">New Password</div>
                        <input
                            autoFocus
                            type="password"
                            placeholder={passwordInputPlaceholder}
                            value={password || ""}
                            className="form-input__input"
                            onChange={this.handleChangePassword}
                            onKeyPress={this.handleKeyPress}
                        />
                        <div className="form-input__help-text">
                            <span
                                className={
                                    password && password.length > 0
                                        ? `password-strength password-strength-${passwordStrength} password-strength--visible`
                                        : `password-strength password-strength-${passwordStrength}`
                                }>
                                Password strength: <strong>{passwordStrengthText}</strong>
                            </span>
                        </div>
                    </div>
                    <div className={confirmPasswordError ? "form-input form-input--error" : "form-input"}>
                        <div className="form-input__label">Confirm New Password</div>
                        <input
                            type="password"
                            placeholder={confirmPasswordInputPlaceholder}
                            value={confirmPassword || ""}
                            className="form-input__input"
                            onChange={this.handleChangeConfirmPassword}
                            onKeyPress={this.handleKeyPress}
                        />
                        {confirmPasswordError && (
                            <div className="form-input__help-text">
                                <span>{confirmPasswordError.message}</span>
                            </div>
                        )}
                    </div>
                </div>
                <Button
                    fullWidth
                    label={isSubmitting
                        ? <React.Fragment>{resetPasswordButtonTitle} <i className="fa fa-circle-o-notch fa-pulse fa-fw" /></React.Fragment>
                        : resetPasswordButtonTitle}
                    className={resetPasswordButtonClassName}
                    onClick={this.handleSubmit}
                    isDisabled={isSubmitting} />
            </div>
        );
    }
}

ResetPasswordForm.defaultProps = {
    confirmPasswordInputPlaceholder: "••••••••",
    passwordInputPlaceholder: "At least 6 characters",
    resetPasswordButtonTitle: "Change Password"
} as IResetPasswordFormProps;
