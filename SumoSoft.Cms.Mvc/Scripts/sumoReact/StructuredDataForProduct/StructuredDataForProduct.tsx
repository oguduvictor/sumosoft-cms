﻿import * as React from "react";

import { Country } from "../../classes/Country";
import { CartItem } from "../../classes/CartItem";

export interface IStructuredDataProductProps {
    cartItem: CartItem;
    country: Country;
    brand: string;
}

export var StructuredDataForProduct: (props: IStructuredDataProductProps) => JSX.Element = props => {
    const productStockUnit = props.cartItem.getProductStockUnit(props.cartItem.getSelectedVariantOptions());

    const structuredDataMarkup = {
        "@context": "http://schema.org/",
        "@type": "Product",
        "name": props.cartItem.product.localizedTitle,
        "description": props.cartItem.product.localizedDescription,
        "image": props.cartItem.image,
        "mpn": props.cartItem.product.id,
        "sku": productStockUnit == null ? props.cartItem.product.id : productStockUnit.id,
        "url": location.href,
        "offers": {
            "@type": "Offer",
            "price": props.cartItem.totalAfterTax.toFixed(2),
            "priceCurrency": props.country.iso4217CurrencySymbol,
            "url": location.href,
            "availability": "http://schema.org/InStock"
        },
        "brand": {
            "@type": "Brand",
            "name": props.brand
        }
    };

    let script = document.getElementById("productStructureDataMarkup") as HTMLScriptElement;

    if (!script) {
        script = document.createElement("script");
        script.id = "productStructureDataMarkup";
        script.type = "application/ld+json";
        script.async = true;
    }

    script.innerHTML = JSON.stringify(structuredDataMarkup);

    document.body.appendChild(script);

    return null;
}
