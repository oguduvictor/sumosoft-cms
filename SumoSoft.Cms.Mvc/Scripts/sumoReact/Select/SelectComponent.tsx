﻿import * as React from "react";

export interface ISelectProps {
    label?: string;
    value?: string;
    className?: string;
    options?: Array<JSX.Element>;
    required?: boolean;
    borderless?: boolean;
    neutralOption?: JSX.Element;
    isDisabled?: boolean;
    handleChangeValue?(e): void;
}

export class SelectComponent extends React.Component<ISelectProps, undefined> {

    render() {
        return (
            <div className={`select-component ${this.props.className ? this.props.className : ""} ${this.props.required ? "required" : ""} ${this.props.borderless ? "borderless" : ""}`}>
                {this.props.label && <label>{this.props.label}</label>}
                <select disabled={this.props.isDisabled} value={this.props.value} className={this.props.className} onChange={this.props.handleChangeValue}>
                    {this.props.neutralOption}
                    {this.props.options}
                </select>
            </div>
        );
    }
}