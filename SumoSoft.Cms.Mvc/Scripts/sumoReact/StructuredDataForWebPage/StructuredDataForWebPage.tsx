﻿import * as React from "react";

export interface IStructuredDataWebPageProps {
    url: string;
    name: string;
    inLanguage: string;
}

export var StructuredDataForWebPage: (props: IStructuredDataWebPageProps) => JSX.Element = props => {
    const structureDataMarkup = {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "url": props.url,
        "name": props.name,
        "inLanguage": props.inLanguage
    };

    let structureDataMarkupScript = document.getElementById("webPageStructureDataMarkup") as HTMLScriptElement;

    if (!structureDataMarkupScript) {
        structureDataMarkupScript = document.createElement("script");
        structureDataMarkupScript.id = "webPageStructureDataMarkup";
        structureDataMarkupScript.type = "application/ld+json";
        structureDataMarkupScript.async = true;
    }

    structureDataMarkupScript.innerHTML = JSON.stringify(structureDataMarkup);

    document.body.appendChild(structureDataMarkupScript);

    return null;
}
