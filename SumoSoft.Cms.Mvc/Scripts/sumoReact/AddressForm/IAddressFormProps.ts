﻿import { Country } from '../../classes/Country';
import { Address } from '../../classes/Address';
import { IFormResponse } from '../../interfaces/IFormResponse';

export interface IAddressFormProps {
    value: Address;
    handleChangeValue: (address: Address) => void;
    formResponse?: IFormResponse;
    allCountries?: Array<Country>;
    firstNameLabel?: string;
    lastNameLabel?: string;
    addressLine1Label?: string;
    addressLine2Label?: string;
    cityLabel?: string;
    postcodeLabel?: string;
    countryLabel?: string;
    stateCountyProvinceLabel?: string;
    phoneNumberLabel?: string;
}