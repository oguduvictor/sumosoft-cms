﻿import * as React from 'react';
import * as sumoJS from "../../sumoJS";

import { IAddressFormProps } from './IAddressFormProps';

export class AddressForm extends React.Component<IAddressFormProps, {}> {

    constructor(props: IAddressFormProps) {
        super(props);
    }

    static defaultProps: IAddressFormProps

    getGenericErrors() {
        return this.props.formResponse ? this.props.formResponse.errors.filter(x => !x.propertyName) : [];
    }

    // -----------------------------------------------------------------
    // Handlers
    // -----------------------------------------------------------------

    handleChangeFirstName(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.firstName, e.target.value);
        this.props.handleChangeValue(address);
    }

    handleChangeLastName(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.lastName, e.target.value);
        this.props.handleChangeValue(address);
    }

    handleChangeAddressLine1(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.addressLine1, e.target.value);
        this.props.handleChangeValue(address);
    }

    handleChangeAddressLine2(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.addressLine2, e.target.value);
        this.props.handleChangeValue(address);
    }

    handleChangePostCode(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.postcode, e.target.value);
        this.props.handleChangeValue(address);
    }

    handleChangeCity(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.city, e.target.value);
        this.props.handleChangeValue(address);
    }

    handleChangeStateCountyProvince(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.stateCountyProvince, e.target.value);
        this.props.handleChangeValue(address);
    }

    handleChangeCountry(e) {
        var country = this.props.allCountries.filter(x => x.id === e.target.value)[0]
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.country, country);
        this.props.handleChangeValue(address);

    }

    handleChangePhoneNumber(e) {
        var address = sumoJS.shallowCopyAndSetProperty(this.props.value, a => a.phoneNumber, e.target.value);
        this.props.handleChangeValue(address);
    }

    // -----------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------

    renderInput(propertyName: string, label: string, value: string, onChange: (e: React.ChangeEvent) => void) {
        var error = sumoJS.get(() => this.props.formResponse.errors.find(error => error.propertyName.toLowerCase() === propertyName.toLowerCase()).message, "");
        return (
            <div className="form-line">
                <label>{label}</label>
                <input
                    value={value || ""}
                    onChange={onChange} />
                {error && <div className="error">{error}</div>}
            </div>
        );
    }

    renderSelect(propertyName: string, label: string, value: string, onChange: (e: React.ChangeEvent) => void) {
        var error = sumoJS.get(() => this.props.formResponse.errors.find(error => error.propertyName.toLowerCase() === propertyName.toLowerCase()).message, "");
        return (
            <div className="form-line">
                <label>{label}</label>
                <select
                    onChange={onChange}
                    value={value}>
                    <option value="">Select a Country</option>
                    {this.props.allCountries.map(x => (
                        <option value={x.id} key={x.id}>
                            {x.name}
                        </option>
                    ))}
                </select>
                {error && <div className="error">{error}</div>}
            </div>
        )
    }

    render() {
        const genericErrors = this.getGenericErrors();
        return (
            <div className="address-form">
                {!!genericErrors.length &&
                    <ul className="address-errors">
                        {genericErrors.map((x, i) => <li key={i}>{x.message}</li>)}
                    </ul>
                }
                {this.renderInput("FirstName", this.props.firstNameLabel, this.props.value.firstName, e => this.handleChangeFirstName(e))}
                {this.renderInput("LastName", this.props.lastNameLabel, this.props.value.lastName, e => this.handleChangeLastName(e))}
                {this.renderInput("AddressLine1", this.props.addressLine1Label, this.props.value.addressLine1, e => this.handleChangeAddressLine1(e))}
                {this.renderInput("AddressLine2", this.props.addressLine2Label, this.props.value.addressLine2, e => this.handleChangeAddressLine2(e))}
                {this.renderInput("PostCode", this.props.postcodeLabel, this.props.value.postcode, e => this.handleChangePostCode(e))}
                {this.renderInput("City", this.props.cityLabel, this.props.value.city, e => this.handleChangeCity(e))}
                {this.renderInput("StateCountyProvince", this.props.stateCountyProvinceLabel, this.props.value.stateCountyProvince, e => this.handleChangeStateCountyProvince(e))}
                {this.renderSelect("Country", this.props.countryLabel, sumoJS.get(() => this.props.value.country.id), e => this.handleChangeCountry(e))}
                {this.renderInput("PhoneNumber", this.props.phoneNumberLabel, this.props.value.phoneNumber, e => this.handleChangePhoneNumber(e))}
            </div>
        );
    }
}

AddressForm.defaultProps = {
    firstNameLabel: "First name",
    lastNameLabel: "Last name",
    addressLine1Label: "Address line 1",
    addressLine2Label: "Address line 2",
    cityLabel: "City",
    countryLabel: "Country",
    postcodeLabel: "Postcode",
    phoneNumberLabel: "Phone number",
    stateCountyProvinceLabel: "State/County/Province"
} as IAddressFormProps