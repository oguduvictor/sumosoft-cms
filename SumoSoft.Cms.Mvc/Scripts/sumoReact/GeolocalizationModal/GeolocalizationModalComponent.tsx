﻿import * as React from "react";
import * as sumoJS from "../../sumoJS";
import * as Cookies from "js-cookie";

import { BootstrapModalComponent, IBootstrapModalComponentProps } from "../BootstrapModal/BootstrapModalComponent";
import { Country } from "../../classes/Country";

export interface IGeolocalizationModalComponentProps {

    // -------------------------------------------------------------------------------------------------
    /** The html to be injected into the modal */
    html: JSX.Element;

    // -------------------------------------------------------------------------------------------------
    /** The Id to be given to the Geolocalization modal */
    modalId: string;

    // -------------------------------------------------------------------------------------------------
    /** The user location country gotten using an IP database*/
    userLocationCountry: Country;

    // -------------------------------------------------------------------------------------------------
    /** The user's current country */
    currentCountry: Country;

    // -------------------------------------------------------------------------------------------------
    /** The function to be fired when geolocalization check fails */
    handleOnGeolocalizationCheckFail(): void;

    // -------------------------------------------------------------------------------------------------
    /** The function to be fired when geolocalization check passes */
    handleOnGeolocalizationCheckPass(): void;
}

export class GeolocalizationModalComponent extends React.Component<IGeolocalizationModalComponentProps, undefined> {

    constructor(props) {
        super(props);
    }

    private cookieName = "dismissGeolocalizationWarning";

    // -----------------------------------------------------------------
    // Component lifecycle
    // -----------------------------------------------------------------

    componentDidUpdate() {
        if (!Cookies.get(this.cookieName)) {
            if (this.props.currentCountry &&
                this.props.userLocationCountry &&
                this.props.userLocationCountry.localize) {
                if (this.props.currentCountry.id !== this.props.userLocationCountry.id) {
                    this.props.handleOnGeolocalizationCheckFail();
                    Cookies.set(this.cookieName, "true", { expires: 365 });
                } else {
                    this.props.handleOnGeolocalizationCheckPass();
                }
            }
        } else {
            this.props.handleOnGeolocalizationCheckPass();
        }
    }

    // -----------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------

    render() {
        return (
            <BootstrapModalComponent {...{
                id: this.props.modalId,
                modalContent: this.props.html
            } as IBootstrapModalComponentProps} />
        );
    }
}