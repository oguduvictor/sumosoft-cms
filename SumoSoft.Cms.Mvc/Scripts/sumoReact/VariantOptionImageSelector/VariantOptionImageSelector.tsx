﻿import * as React from "react";
import { VariantOption } from "../../classes/VariantOption";

export interface IVariantOptionImageSelectorProps {
    variantOptions: Array<VariantOption>;
    value: VariantOption;
    handleChangeValue(variantOption: VariantOption): void;
    className?: string;
    disableVariantOptionsWhere?(variantOption: VariantOption): boolean;
}

export var VariantOptionImageSelector: (props: IVariantOptionImageSelectorProps) => JSX.Element = props => {
    return (
        <div className={`variant-option-image-selector ${props.className ? props.className : ""}`}>
            <span className="options">
                {
                    props.variantOptions.map(variantOption =>
                        <span key={variantOption.id}
                            title={variantOption.localizedTitle}
                            data-variantoptionname={variantOption.name}
                            className={
                                `option ${props.value.name === variantOption.name ? "selected" : ""} ${props.disableVariantOptionsWhere && props.disableVariantOptionsWhere(variantOption) ? "disabled" : ""}`
                            }
                            onClick={() => props.handleChangeValue(variantOption)}>
                            <span className="option-image-wrapper">
                                <img src={variantOption.image} alt={variantOption.localizedTitle} />
                            </span>
                            <span className="option-title">
                                {variantOption.localizedTitle}
                            </span>
                        </span>
                    )
                }
            </span>
        </div>
    );
}