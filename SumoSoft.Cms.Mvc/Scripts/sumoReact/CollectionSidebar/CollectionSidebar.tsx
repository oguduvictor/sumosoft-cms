﻿import * as React from "react";
import * as sumoJS from "../../sumoJS";

import { Category } from "../../classes/Category";

export interface ICollectionSidebarProps {
    className?: string;
    categories?: Array<Category>;
    selectedTagUrls?: Array<string>;
    handleCategoryClick?(category: Category): void;
}

export class CollectionSidebarComponent extends React.Component<ICollectionSidebarProps, undefined> {

    constructor(props) {
        super(props);
    }

    // -----------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------

    flattenCategoryChildren(array: Array<Category>): Array<Category> {
        let result = [];

        array.forEach((a) => {
            result.push(a);

            if (Array.isArray(a.children)) {
                result = result.concat(this.flattenCategoryChildren(a.children));
            }
        });

        return result;
    }

    renderCategory(category: Category, path: Array<Category> = new Array<Category>()) {

        const isCurrentCategory = this.props.selectedTagUrls.contains(category.url);
        const isOpen = isCurrentCategory || this.props.selectedTagUrls.some(url => this.flattenCategoryChildren(category.children).map(x => x.url).contains(url));

        const pathClone = sumoJS.deepClone(path);
        pathClone.push(category);

        return (
            <div
                key={category.id}
                data-category-url={category.url}
                className="category-wrapper">
                <a
                    onClick={() => this.props.handleCategoryClick(category)}
                    className={isCurrentCategory ? "category-link selected" : "category-link"}>
                    {
                        category.localizedTitle
                    }
                </a>
                {
                    category.children.length > 0 &&
                    <div className={isOpen ? "category-children-wrapper open" : "category-children-wrapper"}>
                        {
                            category.children.map(childCategory => this.renderCategory(childCategory, pathClone))
                        }
                    </div>
                }
            </div>
        );
    }

    render() {
        return (
            <div className={`collection-sidebar-component ${this.props.className ? this.props.className : ""}`}>
                {
                    this.props.categories.map(category => this.renderCategory(category))
                }
            </div>
        );
    }
}