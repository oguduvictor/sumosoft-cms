﻿import * as React from "react";

import { IFormResponse } from "../../interfaces/IFormResponse";

interface IFormResponseProps {
    formResponse: IFormResponse
}

export class FormResponseComponent extends React.Component<IFormResponseProps, undefined> {

    constructor(props) {
        super(props);
    }

    // -----------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------

    render() {
        return (
            <div className="form-response-component">
                {
                    this.props.formResponse && this.props.formResponse.isValid &&
                        <div
                            className="success"
                            dangerouslySetInnerHTML={{
                                __html: this.props.formResponse && this.props.formResponse.successMessage
                            }}>
                        </div>
                }
                {
                    this.props.formResponse && !this.props.formResponse.isValid &&
                        <div className="errors">
                            {
                                this.props.formResponse.errors && this.props.formResponse.errors.map((error, i) =>
                                    <p
                                        key={i}
                                        dangerouslySetInnerHTML={{
                                            __html: error.message
                                    }}>
                                    </p>
                                )
                            }
                        </div>
                }
            </div>
        );
    }
}