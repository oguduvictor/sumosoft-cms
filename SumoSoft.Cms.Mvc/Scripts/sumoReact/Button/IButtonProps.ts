﻿export interface IButtonProps {
    id?: string;
    className?: string;
    fullWidth?: boolean;
    isDisabled?: boolean;
    label: string | JSX.Element;
    confirmLabel?: string;
    cancelLabel?: string;
    href?: string;
    requireConfirmation?: boolean;
    onClick?(e): void;
}