﻿import * as React from "react";

import { IButtonProps } from './IButtonProps';
import { IButtonState } from './IButtonState';

export class Button extends React.Component<IButtonProps, IButtonState> {

    constructor(props) {
        super(props);

        this.state = {
            showConfirmationPrompt: false
        };
    }

    static defaultProps: Partial<IButtonProps>;

    handleToggleConfirmationPrompt() {
        this.setState({
            showConfirmationPrompt: !this.state.showConfirmationPrompt
        });
    }

    render() {
        var classNames = ["button"];
        if (this.props.className) classNames.push(this.props.className);
        if (this.props.fullWidth) classNames.push("full-width");
        if (this.props.isDisabled) classNames.push("disabled");

        if (this.props.requireConfirmation) {
            return (
                this.state.showConfirmationPrompt ?
                    <React.Fragment>
                        <a
                            href={this.props.isDisabled ? null : this.props.href}
                            onClick={e => this.props.isDisabled ? null : this.props.onClick && this.props.onClick(e)}
                            className={[...classNames, "confirm"].join(" ")}>
                            {this.props.confirmLabel}
                        </a>
                        <a
                            onClick={e => this.props.isDisabled ? null : this.handleToggleConfirmationPrompt()}
                            className={classNames.join(" ")}>
                            {this.props.cancelLabel}
                        </a>
                    </React.Fragment> :
                    <a
                        onClick={e => this.props.isDisabled ? null : this.handleToggleConfirmationPrompt()}
                        id={this.props.id}
                        className={classNames.join(" ")}>
                        {this.props.label}
                    </a>
            );
        }

        return (
            <a
                href={this.props.isDisabled ? null : this.props.href}
                onClick={e => this.props.isDisabled ? null : this.props.onClick && this.props.onClick(e)}
                id={this.props.id}
                className={classNames.join(" ")}>
                {this.props.label}
            </a>
        );
    }
}

Button.defaultProps = {
    confirmLabel: "Confirm",
    cancelLabel: "Cancel",
    fullWidth: false
}
