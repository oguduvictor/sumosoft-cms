﻿// ----------------------------------------------------------------
//
//     _____                       _____        __ _   
//    / ____|                     / ____|      / _| |  
//   | (___  _   _ _ __ ___   ___| (___   ___ | |_| |_ 
//    \___ \| | | | '_ ` _ \ / _ \\___ \ / _ \|  _| __|
//    ____) | |_| | | | | | | (_) |___) | (_) | | | |_ 
//   |_____/ \__,_|_| |_| |_|\___/_____/ \___/|_|  \__|
//
//
// ----------------------------------------------------------------

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// WITHOUT CREDIT APPLIED:
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Items subtotal before tax        100      
// Coupon                           -30       
// Tax                               20       
// Shipping                           5
// --------------------------------------------------------------
// Total                             95
// --------------------------------------------------------------

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// WITH CREDIT APPLIED
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Items subtotal before tax        100      
// Coupon                           -30       
// Tax                               20       
// Shipping                           5
// --------------------------------------------------------------
// Total                             95
// Credit applied                    50
// --------------------------------------------------------------
// Total paid                  45
// --------------------------------------------------------------

import * as React from "react";
import * as sumoJS from "../../sumoJS";

import { Order } from "../../classes/Order";
import { OrderShippingBox } from "../../classes/OrderShippingBox";
import { OrderItem } from "../../classes/OrderItem";

export interface IOrderSummaryProps {
    order: Order;

    /** The number of fraction digits to use when displaying the price */
    fractionDigits?: number;

    rowDescriptionItemsSubtotalBeforeTax?: string;
    rowDescriptionCoupon?: string;
    rowDescriptionTax?: string;
    rowDescriptionShipping?: string;
    rowDescriptionTotalAfterTax?: string;
    rowDescriptionCredit?: string;
    rowDescriptionTotalToBePaid?: string;

    showCouponCode?: boolean;
}

export class OrderSummary extends React.Component<IOrderSummaryProps, undefined> {

    static defaultProps: IOrderSummaryProps;

    constructor(props) {
        super(props);
    }

    getFormattedPrice(price: number): string {
        return this.props.order.currencySymbol + price.toFixed(this.props.fractionDigits === undefined ? 2 : this.props.fractionDigits);
    }

    getOrderShippingBoxes() {
        return sumoJS.get(() => this.props.order.orderShippingBoxes, new Array<OrderShippingBox>());
    }

    getOrderItems() {
        return sumoJS.get(() => this.getOrderShippingBoxes().mapMany(orderShippingBox => orderShippingBox.orderItems), new Array<OrderItem>());
    }

    getItemsSubtotalBeforeTax() {
        return sumoJS.get(() => this.getOrderItems().sum(x => x.subtotalBeforeTax), 0);
    }

    getShippingPrice(): number {
        return sumoJS.get<number>(() => this.getOrderShippingBoxes().sum(x => x.shippingPriceAfterTax), 0);
    }

    getTotalTax(): number {
        return sumoJS.get<number>(() => this.props.order.totalAfterTax - this.props.order.totalBeforeTax, 0);
    }

    getTotalAfterTax(): number {
        return sumoJS.get<number>(() => this.props.order.totalAfterTax, 0);
    }

    getCreditValue(): number {
        return sumoJS.get<number>(() => this.props.order.orderCredit.amount, 0);
    }

    getTotalToBePaid(): number {
        return sumoJS.get<number>(() => this.props.order.totalToBePaid, 0);
    }

    // -----------------------------------------------------------------------------
    // Render methods
    // -----------------------------------------------------------------------------

    renderCouponRowContent() {
        return <span>
            {
                this.props.showCouponCode &&
                <span className="coupon-code">
                    {
                        this.props.order.orderCoupon.code
                    }
                    &nbsp;
                    &nbsp;
                  </span>
            }
            {
                `- ${this.getFormattedPrice(this.props.order.orderCoupon.amount)}`
            }
        </span>;
    }

    renderTableRow(rowClassName: string, rowDescription: string, rowContent: string | JSX.Element) {
        return (
            <tr className={rowClassName}>
                <td className="row-description">
                    {rowDescription}
                </td>
                <td className="row-content">
                    {rowContent}
                </td>
            </tr>
        );
    }

    render() {
        return (
            <div className="order-summary-component">
                <table>
                    <tbody>
                        {
                            this.renderTableRow("items-subtotal-before-tax", this.props.rowDescriptionItemsSubtotalBeforeTax, this.getFormattedPrice(this.getItemsSubtotalBeforeTax()))
                        }
                        {
                            this.props.order.orderCoupon &&
                            this.renderTableRow("coupon", this.props.rowDescriptionCoupon, this.renderCouponRowContent())
                        }
                        {
                            this.renderTableRow("tax", this.props.rowDescriptionTax, this.getFormattedPrice(this.getTotalTax()))
                        }
                        {
                            this.renderTableRow("shipping", this.props.rowDescriptionShipping, this.getFormattedPrice(this.getShippingPrice()))
                        }
                        {
                            this.renderTableRow("total-after-tax", this.props.rowDescriptionTotalAfterTax, this.getFormattedPrice(this.getTotalAfterTax()))
                        }
                        {
                            this.getCreditValue() < 0 &&
                            this.renderTableRow("credit", this.props.rowDescriptionCredit, `- ${this.getFormattedPrice(Math.abs(this.getCreditValue()))}`)
                        }
                        {
                            this.getCreditValue() < 0 &&
                            this.renderTableRow("total-to-be-paid", this.props.rowDescriptionTotalToBePaid, this.getFormattedPrice(this.getTotalToBePaid()))
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

OrderSummary.defaultProps = {
    order: new Order(),
    currencySymbol: "",
    fractionDigits: 2,

    rowDescriptionItemsSubtotalBeforeTax: "Items subtotal before tax",
    rowDescriptionCoupon: "Coupon",
    rowDescriptionTax: "Tax",
    rowDescriptionShipping: "Shipping",
    rowDescriptionTotalAfterTax: "Total",
    rowDescriptionCredit: "Credit applied",
    rowDescriptionTotalToBePaid: "Total paid",

    showCouponCode: false
} as IOrderSummaryProps;