﻿import * as dayjs from "dayjs";

Date.prototype.addWorkingDays = function (days: number) {
    var date = dayjs(this);
    while (days > 0) {
        date = date.add(1, "day");
        if (date.day() !== 0 && date.day() !== 6) {
            days -= 1;
        }
    }
    return date;
}