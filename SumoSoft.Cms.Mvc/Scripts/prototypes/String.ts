﻿String.prototype.includesSubstring = function (substring: string, ignoreCase = false) {
    var origin = this;
    if (ignoreCase) {
        origin = origin.toLowerCase();
        substring = substring.toLowerCase();
    }
    return origin.indexOf(substring) !== -1;
};

String.prototype.replaceAll = function (search: string, replacement: string) {
    // escape 'search' parameter
    search = search.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    const target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

String.prototype.renderMarkdown = function (): string {
    var content = this;
    if (!content) return "";
    content = replaceMarkdownVideo(content);
    content = replaceMarkdownImage(content);
    content = replaceSmartCountryUrlInMarkdownAnchors(content);
    content = replaceMarkdownAnchors(content);
    content = replaceMarkdownHeaders(content);
    content = replaceMarkdownHorizontalRule(content);
    content = replaceMarkdownTables(content);
    content = createParagraphs(content);
    content = removeEmptyParagraphs(content);
    content = replaceMarkdownBold(content);
    content = replaceMarkdownItalic(content);
    return content;
};

String.prototype.parseJsonAs = function <T>(): T {
    return this ? JSON.parse(this) as T : this;
};

// --------------------------------------------------------
// Markdown
// --------------------------------------------------------

// /(*)/g in javascript the regex is wrapped in backslashes, with the options parameter at the end (g)
// g is "global"
// gm is "global multiline"
// [] square brackets describe a character set. Example [a-z] or [abc]. Matches any charachter in the set.
// [^] if a caret is inserted before the set, it will match any character BUT the ones in the set.
// () capturing group (g1, g1...)
// (?:) non capturing group
// (?=something) match "something" (positive lookahead)
// (?!something) all but "something" (negative lookahead)
// \s match any white space chars
// \S match any non-white space chars
// . matches anything except new line
// .* matches anything (greedy)
// .*?something matches anything until something (non-greedy)
// ^ string starts with
// $ string ends with
// | "or"
// \r and \n are line-breaks
// \ escape following character. Example: \* matches an actual asterisk.
// start of the string: \A
// end of the string: \z

function replaceMarkdownBold(text: string) {
    if (!text) return "";
    return text.replace(/\*\*([\S][^\r\n]*?[\S])\*\*/g, (match, g1) => `<b>${g1}</b>`);
}

function replaceMarkdownItalic(text: string) {
    if (!text) return "";
    return text.replace(/\*([\S][^\r\n]*?[\S])\*/g, (match, g1) => `<i>${g1}</i>`);
}

function replaceMarkdownVideo(text: string) {
    if (!text) return "";
    return text.replace(/\!\!\[([\s\S][^\r\n]*?)\]\(([\s\S][^\r\n]*?)\)/g, (match, g1, g2) => `<video src='${g2}' ${g1}></video>`);
}

function replaceMarkdownImage(text: string) {
    if (!text) return "";
    return text.replace(/\!\[([\s\S][^\r\n]*?)\]\(([\s\S][^\r\n]*?)\)/g, (match, g1, g2) => `<img src='${g2}' alt='${g1}'/>`);
}

function replaceSmartCountryUrlInMarkdownAnchors(text: string) {
    if (!text) return "";
    let currentCountryUrl: string = "";
    const pathArray = window.location.pathname.toLowerCase().split("/");
    if (pathArray[1].length === 2) {
        currentCountryUrl = pathArray[1];
    }
    return text.replace(/(.*?)(\/\*\*\/)(.*?)(?=\))/g, (match, g1, g2, g3) => {
        var countryParameter = g2.replace("/**/", `/${currentCountryUrl}/`).replace("//", "/");
        return `${g1}${countryParameter}${g3}`;
    });
}

function replaceMarkdownAnchors(text: string) {
    if (!text) return "";
    return text.replace(/\[([\s\S][^\r\n]*?)\]\(([\s\S^\r\n]*?)\)/g, (match, g1, g2) => `<a href='${g2}'>${g1}</a>`);
}

function replaceMarkdownHeaders(text: string) {
    if (!text) return "";
    return text.replace(/(\#{1,6})([\s\S]*?)(\r+|\n+|$)/g, (match, g1, g2) => `<h${g1.length}>${g2.trim()}</h${g1.length}>\n`);
}

function replaceMarkdownHorizontalRule(text: string) {
    if (!text) return "";
    return text.replace(/---/g, "<hr>");
}

function replaceMarkdownTables(text: string) {
    // the regex is slightly different. Since javascript doesn't accept \z as "end of string",
    // I had to use end of line ($) followed by a negative lookahead $(?![r\n])
    return text.replace(/^\|([\s\S]*?)\|(?:\s*?)(?:$(?![r\n])|[\r\n](?!\|))/gm, (match, g1: string) => {
        var rows = g1.split('\n').map(x => x.replace(/^\|+|\|+$/g, '')); // trim pipes on both sides
        var cells = rows.map(x => x.split('|'));
        return `<table>${cells.map(tr => `<tr>${tr.map(td => `<td>${td}</td>`).join("")}</tr>`).join("")}</table>`;
    });
}

function createParagraphs(text: string) {
    return text.replace(/^(?!<|\s*$|===)(.*)/gm, (match, g1) => `<p>${g1}</p>`);
}

function removeEmptyParagraphs(text: string) {
    return text.replace(/<p>[\s]{0,}<\/p>/gm, (match, g1) => "");
}