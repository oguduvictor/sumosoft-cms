﻿interface Array<T> {
    contains(item: T): boolean;
    containsAll(array: Array<T>): boolean;
    containsAny(array: Array<T>): boolean;
    mapMany<T1>(expression: (item: T) => Array<T1>): Array<T1>;
    sum(expression: (item: T) => number): number;
    orderBy(expression: (item: T) => any): any[];
    orderByDescending(expression: (item: T) => any): any[];
    groupBy<T>(expression: (item: T) => string): any;
    distinct(): Array<T>;
    getByLanguageCode(languageCode: string): T;
}

interface String {
    includesSubstring(substring: string, ignoreCase?: boolean): boolean;
    replaceAll(search: string, replacement: string): string;
    renderMarkdown(): string;
    parseJsonAs<T>(): T;
}

interface Date {
    addWorkingDays(days: number);
}