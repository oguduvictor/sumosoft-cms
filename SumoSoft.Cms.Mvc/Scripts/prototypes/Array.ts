﻿Array.prototype.contains = function <T>(item: T): boolean {
    return this.indexOf(item) > -1;
}

Array.prototype.containsAny = function <T>(array: Array<T>): boolean {
    return this.some(x => array.some(y => y === x));
}

Array.prototype.containsAll = function <T>(array: Array<T>): boolean {
    return array.every(x => this.some(y => y === x));
}

Array.prototype.mapMany = function <T, T1>(expression: (x: T) => Array<T1>): Array<T1> {
    return this.map(expression).reduce((a, b) => a.concat(b), []);
}

Array.prototype.sum = function <T>(expression: (item: T) => number): number {
    return this.map(expression).reduce((a, b) => a + b, 0);
}

Array.prototype.orderBy = function (expression: (item: any) => any) {
    const result = [];
    const compareFunction = (item1: any, item2: any): number => {
        if (expression(item1) > expression(item2)) return 1;
        if (expression(item2) > expression(item1)) return -1;
        return 0;
    };
    for (const i = 0; i < (this as Array<any>).length;) {
        return (this as Array<any>).sort(compareFunction);
    }
    return result;
}

Array.prototype.orderByDescending = function (expression: (item: any) => any) {
    const result = [];
    const compareFunction = (item1: any, item2: any): number => {
        if (expression(item1) > expression(item2)) return -1;
        if (expression(item2) > expression(item1)) return 1;
        return 0;
    };
    for (const i = 0; i < (this as Array<any>).length;) {
        return (this as Array<any>).sort(compareFunction);
    }
    return result;
}

Array.prototype.groupBy = function <T>(expression: (item: T) => string) {
    return this.reduce((objectsByKeyValue, obj) => {
        const value = expression(obj);
        objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
        return objectsByKeyValue;
    }, {}); 
}

function unique(value, index, self) {
    return self.indexOf(value) === index;
}

Array.prototype.distinct = function <T>() {
    return this.filter(unique);
};

Array.prototype.getByLanguageCode = function <T>(languageCode: string): T {
    return this.filter(x => x.country && x.country.languageCode === languageCode)[0];
}