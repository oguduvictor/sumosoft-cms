export interface ICms_CartErrors {
    cartItemWithDisabledProduct: string;
    cartItemWithOutOfStockProduct: string;
    cartItemWithInvalidShippingBox: string;
    invalidCoupon: string;
    invalidUserCredit: string;
    invalidTaxOverride: string;
    localizedShippingAddressCountryNotMatchingCurrentCountry: string;
    nonLocalizedShippingAddressCountryNotMatchingDefaultCountry: string;
    shippingAddressRequired: string;
}