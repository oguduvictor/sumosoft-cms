﻿import "url-search-params-polyfill";

import { IContentSectionDto } from "../interfaces/IContentSectionDto";
import { ContentSection } from "../classes/ContentSection";
import { ICountryDto } from "../interfaces/ICountryDto";
import { Country } from "../classes/Country";
import { IUserDto } from "../interfaces/IUserDto";
import { User } from "../classes/User";
import { Variant } from "../classes/Variant";
import { Product } from "../classes/Product";
import { ICartDto } from "../interfaces/ICartDto";
import { Cart } from "../classes/Cart";
import { ICartItemDto } from "../interfaces/ICartItemDto";
import { CartItem } from "../classes/CartItem";
import { ProductAttribute } from "../classes/ProductAttribute";
import { Log } from "../classes/Log";
import { CartShippingBox } from "../classes/CartShippingBox";
import { ShippingBox } from "../classes/ShippingBox";
import { IFormResponse } from "../interfaces/IFormResponse";
import { Address } from "../classes/Address";
import { WishList } from "../classes/WishList";
import { IWishListDto } from "../interfaces/IWishListDto";
import { IAddressDto } from "../interfaces/IAddressDto";
import { Order } from "../classes/Order";
import { IOrderDto } from "../interfaces/IOrderDto";
import { IShippingBoxDto } from "../interfaces/IShippingBoxDto";
import { CartItemVariant } from "../classes/CartItemVariant";
import { VariantOption } from "../classes/VariantOption";
import { LogTypesEnum } from "../enums/LogTypesEnum";

// -------------------------------------------------------------------------------------------------
// Non-exported methods and variables
// -------------------------------------------------------------------------------------------------

declare var FB;

declare var gapi;

var logHistory = new Array<Log>();

function minifyCartItem(cartItem: CartItem): CartItem {
    return {
        id: cartItem.id,
        cartShippingBox: {
            id: get(() => cartItem.cartShippingBox.id, null),
            shippingBox: new ShippingBox({
                id: get(() => cartItem.cartShippingBox.shippingBox.id, null)
            })
        } as CartShippingBox,
        product: new Product({
            id: cartItem.product.id
        }),
        quantity: cartItem.quantity,
        image: cartItem.image,
        cartItemVariants: cartItem.cartItemVariants.map(
            cartItemVariant =>
                new CartItemVariant({
                    id: cartItemVariant.id,
                    cartItem: new CartItem({ id: cartItem.id }),
                    variant: new Variant({ id: cartItemVariant.variant.id, name: cartItemVariant.variant.name }),
                    variantOptionValue: cartItemVariant.variantOptionValue ? new VariantOption({ id: cartItemVariant.variantOptionValue.id, name: cartItemVariant.variantOptionValue.name }) : null,
                    booleanValue: cartItemVariant.booleanValue,
                    doubleValue: cartItemVariant.doubleValue,
                    integerValue: cartItemVariant.integerValue,
                    stringValue: cartItemVariant.stringValue,
                    jsonValue: cartItemVariant.jsonValue
                })
        ),
        taxesOverride: cartItem.taxesOverride
    } as CartItem;
}

// -------------------------------------------------------------------------------------------------
/** Creates a shallow copy of an object and sets one of its properties using a lambda expression. */
export function shallowCopyAndSetProperty<T, P>(obj: T, expression: (x: T) => P, value: P): T {
    const regex = /\.(.*?)(;|\s|})/gm;
    const matches = regex.exec(expression.toString());
    const propertyAccessor = matches[1];
    const shallowCopy = { ...(obj as Object) };
    eval(`shallowCopy.${propertyAccessor} = value`);
    return shallowCopy as T;
}

// -------------------------------------------------------------------------------------------------
/** Return an Order */
export function getOrder(orderNumber: number): JQueryPromise<Order> {
    var deferred = $.Deferred<Order>();
    ajaxPost<IOrderDto>(getLocalizedUrl("/CmsApi/GetOrder"), { orderNumber }, dto => {
        var order = new Order(dto);
        deferred.resolve(order);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Return the Wishlist */
export function getWishList(): JQueryPromise<WishList> {
    var deferred = $.Deferred<WishList>();
    ajaxPost<IWishListDto>(getLocalizedUrl("/CmsApiWishList/GetWishList"), null, dto => {
        var wishList = new WishList(dto);
        deferred.resolve(wishList);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Return the Array of CartItems contained in the WishList with only the Id property set */
export function getWishListLite(): JQueryPromise<Array<CartItem>> {
    var deferred = $.Deferred<Array<CartItem>>();
    ajaxPost<Array<ICartItemDto>>(getLocalizedUrl("/CmsApiWishList/GetWishListLite"), null, dto => {
        var cartItems = dto.map(x => new CartItem(x));
        deferred.resolve(cartItems);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Add a CartItem to the WishList */
export function addToWishList(cartItem: CartItem, success?: (cartItemId: string) => void): JQueryPromise<string> {
    var deferred = $.Deferred<string>();
    const minifiedCartItem = minifyCartItem(cartItem);
    ajaxPost<string>(getLocalizedUrl("/CmsApiWishList/AddToWishList"), minifiedCartItem, cartItemId => {
        deferred.resolve();
        success && success(cartItemId);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Remove CartItem from WishList */
export function removeFromWishList(cartItemId: string, success?: () => void): void {
    ajaxPost(getLocalizedUrl("/CmsApiWishList/RemoveFromWishList"), { cartItemId }, () => {
        success && success();
    });
}

// -------------------------------------------------------------------------------------------------
/** Return the Cart */
export function getCart(): JQueryPromise<Cart> {
    var deferred = $.Deferred<Cart>();
    ajaxPost<ICartDto>(getLocalizedUrl("/CmsApiCart/GetCart"), null, dto => {
        var cart = new Cart(dto);
        cart.cartShippingBoxes.forEach(cartShippingBox => {
            cartShippingBox.cart = cart;
            cartShippingBox.cartItems.forEach(cartItem => (cartItem.cartShippingBox = cartShippingBox));
        });
        deferred.resolve(cart);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Return the Array of CartItems contained in the Cart with only the Id property set */
export function getCartLite(): JQueryPromise<Array<CartItem>> {
    var deferred = $.Deferred<Array<CartItem>>();
    ajaxPost<Array<ICartItemDto>>(getLocalizedUrl("/CmsApiCart/GetCartLite"), null, dto => {
        var cartItems = dto.map(x => new CartItem(x));
        deferred.resolve(cartItems);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Return a CartItem */
export function getCartItem(id: string): JQueryPromise<CartItem> {
    var deferred = $.Deferred<CartItem>();
    ajaxPost<ICartItemDto>(getLocalizedUrl("/CmsApiCart/GetCartItem"), { id }, dto => {
        var cartItem = new CartItem(dto);
        deferred.resolve(cartItem);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Initialize a new CartItem from the Url of an Product */
export function productToCartItem(productUrl: string, selectedVariantOptionUrls?: string[], success?: (cartItem: CartItem) => void): JQueryPromise<CartItem> {
    var deferred = $.Deferred<CartItem>();
    ajaxPost<ICartItemDto>(
        getLocalizedUrl("/CmsApiCart/ProductToCartItem"),
        {
            productUrl,
            selectedVariantOptionUrls
        },
        dto => {
            var cartItem = new CartItem(dto);
            deferred.resolve(cartItem);
            success && success(cartItem);
        }
    );
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Add an CartItem to the Cart */
export function addToCart(cartItem: CartItem, success?: () => void): JQueryPromise<any> {
    const minifiedCartItem = minifyCartItem(cartItem);

    var deferred = $.Deferred();

    ajaxPost(getLocalizedUrl("/CmsApiCart/AddToCart"), minifiedCartItem, resp => {
        deferred.resolve(resp);
        success && success();
    });

    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Update a CartItem */
export function updateCartItem(cartItem: CartItem, success?: () => void): void {
    const minifiedCartItem = minifyCartItem(cartItem);
    ajaxPost(getLocalizedUrl("/CmsApiCart/UpdateCartItem"), minifiedCartItem, success);
}

// -------------------------------------------------------------------------------------------------
/** Update an CartItem Quantity */
export function updateCartItemQuantity(cartItemId: string, quantity: number, returnCart = false): JQueryPromise<Cart> {
    var deferred = $.Deferred<Cart>();
    ajaxPost(getLocalizedUrl("/CmsApiCart/UpdateCartItemQuantity"), { cartItemId, quantity, returnCart }, cartDto => {
        if (returnCart) {
            const cart = new Cart(cartDto);
            cart.cartShippingBoxes.forEach(cartShippingBox => {
                cartShippingBox.cart = cart;
                cartShippingBox.cartItems.forEach(cartItem => (cartItem.cartShippingBox = cartShippingBox));
            });
            deferred.resolve(cart);
        }
        deferred.resolve(null);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Remove an CartItem from the Cart */
export function removeFromCart(cartItemId: string, success?: () => void): void {
    ajaxPost(getLocalizedUrl("/CmsApiCart/RemoveFromCart"), { cartItemId: cartItemId }, success);
}

// -------------------------------------------------------------------------------------------------
/** Add User Credit to Cart */
export function applyUserCreditToCart(amount: number, success?: (formResponse: IFormResponse<number>) => void): void {
    ajaxPost(getLocalizedUrl("/CmsApiCart/ApplyUserCreditToCart"), { amount }, (formResponse: IFormResponse<number>) => {
        success && success(formResponse);
    });
}

// -------------------------------------------------------------------------------------------------
/** Remove User Credit from Cart */
export function removeUserCreditFromCart(success?: (formResponse: IFormResponse<number>) => void): void {
    ajaxPost(getLocalizedUrl("/CmsApiCart/RemoveUserCreditFromCart"), null, (formResponse: IFormResponse<number>) => {
        success && success(formResponse);
    });
}

// -------------------------------------------------------------------------------------------------
/** Add Coupon to Cart */
export function applyCouponToCart(couponCode: string, success?: (formResponse: IFormResponse) => void): JQueryPromise<IFormResponse> {
    var deferred = $.Deferred<IFormResponse>();
    ajaxPost(getLocalizedUrl("/CmsApiCart/ApplyCouponToCart"), { couponCode: couponCode }, (formResponse: IFormResponse) => {
        deferred.resolve();
        success && success(formResponse);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Remove Coupon from Cart */
export function removeCouponFromCart(success?: () => void): void {
    ajaxPost(getLocalizedUrl("/CmsApiCart/RemoveCouponFromCart"), null, success);
}

// -------------------------------------------------------------------------------------------------
/** Return all addresses for an authenticated user */
export function getUserAddresses(): JQueryPromise<Array<Address>> {
    var deferred = $.Deferred<Array<Address>>();
    ajaxPost<Array<IAddressDto>>(getLocalizedUrl("/CmsApi/GetUserAddresses"), null, dto => {
        var addresses = dto.map(address => new Address(address));
        deferred.resolve(addresses);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Get User Credit Total */
export function getUserCreditTotal(): JQueryPromise<number> {
    var deferred = $.Deferred<number>();
    ajaxPost(getLocalizedUrl("/CmsApi/GetUserCreditTotal"), null, (response: number) => {
        deferred.resolve(response);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Add Or Update Address */
export function addOrUpdateAddress(address: Address, setUser = false, setAsCartShippingAddress = false, setAsCartBillingAddress = false, success?: (formResponse: IFormResponse<Address>) => void): JQueryPromise<IFormResponse<Address>> {
    var deferred = $.Deferred<IFormResponse<Address>>();
    ajaxPost(getLocalizedUrl("/CmsApi/AddOrUpdateAddress"), { address, setUser, setAsCartShippingAddress, setAsCartBillingAddress }, (formResponse: IFormResponse<Address>) => {
        deferred.resolve();
        success && success(formResponse);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Delete Address */
export function deleteAddress(addressId: string, success?: (formResponse: IFormResponse) => void): JQueryPromise<IFormResponse> {
    var deferred = $.Deferred<IFormResponse>();
    ajaxPost(getLocalizedUrl("/CmsApi/DeleteAddress"), { addressId }, (formResponse: IFormResponse) => {
        deferred.resolve();
        success && success(formResponse);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Set Cart Shipping Address */
export function setCartShippingAddress(addressId: string, success?: (formResponse: IFormResponse) => void): JQueryPromise<IFormResponse> {
    var deferred = $.Deferred<IFormResponse>();
    ajaxPost(getLocalizedUrl("/CmsApiCart/SetCartShippingAddress"), { addressId }, (formResponse: IFormResponse) => {
        deferred.resolve();
        success && success(formResponse);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Set Cart Billing Address */
export function setCartBillingAddress(addressId: string, success?: (formResponse: IFormResponse) => void): JQueryPromise<IFormResponse> {
    var deferred = $.Deferred<IFormResponse>();
    ajaxPost(getLocalizedUrl("/CmsApiCart/SetCartBillingAddress"), { addressId }, (formResponse: IFormResponse) => {
        deferred.resolve();
        success && success(formResponse);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Get all the query string parameters */
export function getQueryStringParameters(): { name: string; value: string }[] {
    const queryString = window.location.search.substring(1);
    const queryStringParameters = queryString.split("&");
    return queryStringParameters
        .map(p => {
            try {
                return {
                    name: decodeURIComponent(p.split("=")[0]),
                    value: decodeURIComponent(p.split("=")[1])
                };
            } catch (e) {
                console.error(e);
            }
            return null;
        })
        .filter(p => p !== null);
}

// -------------------------------------------------------------------------------------------------
/** Get a query string parameter by name */
export function getQueryStringParameter(name: string): string {
    const queryStringParameters = getQueryStringParameters();
    const queryStringParameter = queryStringParameters.filter(x => x.name.toLowerCase() === name.toLowerCase())[0];
    return queryStringParameter ? queryStringParameter.value : undefined;
}

// -------------------------------------------------------------------------------------------------
/** Set a query string parameter */
export function setQueryStringParameter(name: string, value: string): void {
    const params = new URLSearchParams(location.search);
    if (value) {
        params.set(name, value);
    } else {
        params.delete(name);
    }
    window.history.replaceState({}, "", decodeURIComponent(!!params.toString() ? `${location.pathname}?${params}` : location.pathname));
}

// -------------------------------------------------------------------------------------------------
/** Stringify an object to Json and encode the result as a valid URI Component */
export function encodeObjectToURIComponent(obj: any): string {
    return encodeURIComponent(JSON.stringify(obj));
}

// -------------------------------------------------------------------------------------------------
/** Parse a Json encoded URI component to return the original object */
export function decodeObjectFromURIComponent<T>(uriComponent: string, fallbackValue?: T): T {
    return get<T>(() => JSON.parse(decodeURIComponent(uriComponent)), fallbackValue);
}

// -------------------------------------------------------------------------------------------------
/** Get a url parameter by its index, excluding the country parameter */
export function getUrlParameter(index: number): string {
    const pathArray = window.location.pathname
        .toLowerCase()
        .split("/")
        .filter(x => x.length);
    const isLocalized = pathArray[0].length === 2;
    if (isLocalized) pathArray.shift();
    return pathArray[index];
}

// -------------------------------------------------------------------------------------------------
/** Safely access sub-properties of any object */
export function get<T>(func: () => T, fallbackValue?: T): T {
    try {
        const value = func();
        return value === null || value === undefined ? fallbackValue : value;
    } catch (e) {
        return fallbackValue;
    }
}

// -------------------------------------------------------------------------------------------------
/** Get an array of ContentSections by name and options */
export function getContentSections(contentSectionNames: Array<string>, languageCode?: string): JQueryPromise<Array<ContentSection>> {
    const duplicates = contentSectionNames.filter(i => contentSectionNames.filter(ii => ii === i).length > 1);
    if (duplicates.length) {
        console.log(`Some ContentSections were fetched more than once: ${duplicates}`);
    }
    var deferred = $.Deferred<Array<ContentSection>>();
    if (!contentSectionNames || !contentSectionNames.length) {
        deferred.resolve(new Array<ContentSection>());
    } else {
        ajaxPost<Array<IContentSectionDto>>(getLocalizedUrl("/CmsApi/ContentSections"), { contentSectionNames, languageCode }, contentSections => {
            deferred.resolve(contentSections.map(dto => new ContentSection(dto)));
        });
    }
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Render a ContentSection from an array of ContentSections */
export function contentSection(source: Array<ContentSection>, name: string, fallbackValue?: string): string {
    if (!source || !source.length) return fallbackValue || "";
    const content = get(() => source.filter(x => x.name === name)[0].localizedContent);
    return content ? content : fallbackValue || "";
}

// -------------------------------------------------------------------------------------------------
/** EXPERIMENTAL */
export function contentSectionAs<T>(source: Array<ContentSection>, name: string): T {
    const content = get(() => source.filter(x => x.name === name)[0].localizedContent, null);
    try {
        return JSON.parse(content) as T;
    } catch (e) {
        console.error(e);
        log(LogTypesEnum.exception, `ContentSection parsing error: ${name}`, e, true);
        return {} as T;
    }
}

// -------------------------------------------------------------------------------------------------
/** Render the ContentSection value of a ProductAttribute */
export function contentSectionFromProductAttribute(productAttribute: ProductAttribute, fallbackValue?: string): string {
    var contentSectionValue = get(() => productAttribute.contentSectionValue, null);
    if (!contentSectionValue) return fallbackValue || "";
    return contentSection([contentSectionValue], contentSectionValue.name, fallbackValue);
}

// -------------------------------------------------------------------------------------------------
/** Asynchronously log a message or exception */
export function log(type: LogTypesEnum, message: string, details?: string, avoidDuplication?: boolean): void {
    if (avoidDuplication && logHistory.some(x => x.message === message && x.details === details)) {
        return;
    }
    ajaxPost(getLocalizedUrl("/CmsApi/Log"), { type: type, message: message, details: details });
    const logHistoryEntry = new Log();
    logHistoryEntry.createdDate = new Date().toDateString();
    logHistoryEntry.message = message;
    logHistoryEntry.details = details;
    logHistory.push(logHistoryEntry);
}

// -------------------------------------------------------------------------------------------------
/** Return the localized version of a certain URL based on the current country */
export function getLocalizedUrl(url: string, countryUrl?: string) {
    if (arguments.length === 1) {
        const pathArray = window.location.pathname.toLowerCase().split("/");
        if (pathArray[1].length === 2) {
            countryUrl = pathArray[1];
        }
    }
    return `/${countryUrl || ""}/${url}`.replace("//", "/").replace("//", "/");
}

// -------------------------------------------------------------------------------------------------
/** Return the current User Location using an IP database */
export function getUserLocation(success?: (country: Country) => void): JQueryPromise<Country> {
    var deferred = $.Deferred<Country>();
    ajaxPost<ICountryDto>(getLocalizedUrl("/CmsApi/GetUserLocation"), null, dto => {
        var country = new Country(dto);
        deferred.resolve(country);
        success && success(country);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Return the current Country */
export function getCurrentCountry(): JQueryPromise<Country> {
    var deferred = $.Deferred<Country>();
    ajaxPost<ICountryDto>(getLocalizedUrl("/CmsApi/GetCurrentCountry"), null, dto => {
        var user = new Country(dto);
        deferred.resolve(user);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Return the list of all the enabled Countries */
export function getCountries(): JQueryPromise<Array<Country>> {
    var deferred = $.Deferred<Array<Country>>();
    ajaxPost<Array<ICountryDto>>(getLocalizedUrl("/CmsApi/GetCountries"), null, dto => {
        var countries = dto.map(countryDto => new Country(countryDto));
        deferred.resolve(countries);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Return the list of all shipping boxes */
export function getShippingBoxes(): JQueryPromise<Array<ShippingBox>> {
    var deferred = $.Deferred<Array<ShippingBox>>();
    ajaxPost<Array<IShippingBoxDto>>(getLocalizedUrl("/CmsApi/GetShippingBoxes"), null, dto => {
        var shippingBoxes = dto.map(shippingBox => new ShippingBox(shippingBox));
        deferred.resolve(shippingBoxes);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Sets the shipping box for a certain cart item */
export function setShippingBox(cartItemId: string, shippingBoxName: string): JQueryPromise<IFormResponse> {
    var deferred = $.Deferred<IFormResponse>();
    ajaxPost<IFormResponse>(getLocalizedUrl("/CmsApi/SetShippingBox"), { cartItemId, shippingBoxName }, response => {
        deferred.resolve(response);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Register a new User */
export function registerUser(user: User, success?: (formResponse: IFormResponse) => void): JQueryPromise<IFormResponse> {
    var deferred = $.Deferred<IFormResponse>();
    ajaxPost(getLocalizedUrl("/CmsApi/RegisterUser"), { user }, (formResponse: IFormResponse) => {
        deferred.resolve();
        success && success(formResponse);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Authenticate User */
export function authenticateUser(email: string, password: string, success?: (formResponse: IFormResponse) => void): JQueryPromise<IFormResponse> {
    var deferred = $.Deferred<IFormResponse>();
    ajaxPost(getLocalizedUrl("/CmsApi/AuthenticateUser"), { email, password }, (formResponse: IFormResponse) => {
        deferred.resolve();
        success && success(formResponse);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Authenticate User using Facebook. Facebook JS SDK is required. */
export function authenticateUserWithFacebook(authenticationUrl: string, redirectUrl?: string, authenticationParams?: {}): void {
    FB.login(
        response => {
            if (response.authResponse) {
                const uid = response.authResponse.userID;
                FB.api("/me?fields=id,name,first_name,last_name,gender,email,permissions", response => {
                    const credentials = { fbid: uid, firstname: response.first_name, lastname: response.last_name, email: response.email, gender: response.gender };
                    $.ajax({
                        type: "POST",
                        url: authenticationUrl,
                        data: JSON.stringify({ credentials, ...authenticationParams }),
                        contentType: "application/json",
                        success: response => {
                            window.location.href = redirectUrl || response.redirectUrl;
                        },
                        error: () => {
                            alert("An error occurred while attempting to connect with Facebook");
                            log(LogTypesEnum.exception, "Facebook Login attempt failed");
                        }
                    });
                });
            }
        },
        { scope: "email" }
    );
}

// ----------------------------------------------------------------------------------------------------------------
/** Authenticate User using Google. Google JS SDK is required.*/
export function authenticateUserWithGoogle(clientID: string, authenticationUrl: string, redirectUrl?: string, authenticationParams?: {}): void {
    gapi.load("auth2", () => {
        const auth2 = gapi.auth2.init({ client_id: clientID, cookiepolicy: "single_host_origin" });
        auth2.signIn({ scope: "profile email" }).then(googleUser => {
            const googleUserProfile = googleUser.getBasicProfile();
            const credentials = { firstname: googleUserProfile.getGivenName(), lastname: googleUserProfile.getFamilyName(), email: googleUserProfile.getEmail() };
            $.ajax({
                type: "POST",
                url: authenticationUrl,
                data: JSON.stringify({ credentials, ...authenticationParams }),
                contentType: "application/json",
                success: response => {
                    window.location.href = redirectUrl || response.redirectUrl;
                },
                error: () => {
                    alert("An error occurred while attempting to connect with Google");
                    log(LogTypesEnum.exception, "Google Login attempt failed");
                }
            });
        });
    });
}

// -------------------------------------------------------------------------------------------------
/** Return the Authenticated User */
export function getAuthenticatedUser(): JQueryPromise<User> {
    var deferred = $.Deferred<User>();
    ajaxPost<IUserDto>(getLocalizedUrl("/CmsApi/GetAuthenticatedUser"), null, dto => {
        var user = new User(dto);
        deferred.resolve(user);
    });
    return deferred.promise();
}

// -------------------------------------------------------------------------------------------------
/** Deep clone objects or arrays */
export function deepClone<T>(data: T): T {
    return JSON.parse(JSON.stringify(data));
}

// -------------------------------------------------------------------------------------------------
/** Queries an API */
export function ajaxPost<T>(url: string, data: any, success?: (response: T) => void): JQueryPromise<T> {
    url = (window["sumoJS_AjaxBaseUrl"] || "") + url;
    var deferred = $.Deferred<T>();
    const promise = deferred.promise();
    promise.done(m => {
        success && success(m);
    });
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json",
        success: response => {
            try {
                const model = JSON.parse(response) as T;
                deferred.resolve(model);
            } catch (e) {
                deferred.resolve(null);
            }
        }
    });
    return promise;
}

// -------------------------------------------------------------------------------------------------
/** Queries the API the first time, then returns a cached object */
export function ajaxPostAndCache<T>(url: string, data: any, success?: (response: T) => void): JQueryPromise<T> {
    var deferred = $.Deferred<T>();
    const promise = deferred.promise();
    promise.done(m => success && success(m));
    var cacheKey = url + JSON.stringify(data);
    const cachedModel = get(() => window["sumoJS_Cache"].filter(x => x.key === cacheKey)[0].value as T, null);
    if (cachedModel) {
        deferred.resolve(cachedModel);
    } else {
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            success: response => {
                try {
                    const model = JSON.parse(response) as T;
                    window["sumoJS_Cache"] = window["sumoJS_Cache"] || [];
                    window["sumoJS_Cache"].push({
                        key: cacheKey,
                        value: model
                    });
                    deferred.resolve(model);
                } catch (e) {
                    deferred.resolve(null);
                }
            }
        });
    }
    return promise;
}
