export as namespace sumoJS;
export = sumoJS;

import { IFormResponse } from '../interfaces/IFormResponse';
import { CartItem } from '../classes/CartItem';
import { ContentSection } from './../classes/ContentSection';
import { ProductAttribute } from '../classes/ProductAttribute';
import { Country } from './../classes/Country';
import { User } from './../classes/User';
import { Cart } from '../classes/Cart';
import { Address } from '../classes/Address';
import { WishList } from '../classes/WishList';
import { Order } from '../classes/Order';
import { ShippingBox } from '../classes/ShippingBox';
import { LogTypesEnum } from '../enums/LogTypesEnum';

declare namespace sumoJS {
	// -------------------------------------------------------------------------------------------------
	/** Creates a shallow copy of an object and sets one of its properties using a lambda expression. */
	function shallowCopyAndSetProperty<T, P>(
		obj: T,
		expression: (x: T) => P,
		value: P
	): T;
	// -------------------------------------------------------------------------------------------------
	/** Return an Order */
	function getOrder(orderNumber: number): JQueryPromise<Order>;
	// -------------------------------------------------------------------------------------------------
	/** Return the Wishlist */
	function getWishList(): JQueryPromise<WishList>;
	// -------------------------------------------------------------------------------------------------
	/** Return the Array of CartItems contained in the WishList with only the Id property set */
	function getWishListLite(): JQueryPromise<Array<CartItem>>;
	// -------------------------------------------------------------------------------------------------
	/** Add an CartItem to the WishList */
	function addToWishList(
		cartItem: CartItem,
		success?: (cartItemId: string) => void
	): JQueryPromise<string>;
	// -------------------------------------------------------------------------------------------------
	/** Remove CartItem from WishList */
	function removeFromWishList(cartItemId: string, success?: () => void): void;
	// -------------------------------------------------------------------------------------------------
	/** Return the Cart */
	function getCart(): JQueryPromise<Cart>;
	// -------------------------------------------------------------------------------------------------
	/** Return the Array of CartItems contained in the Cart with only the Id property set */
	function getCartLite(): JQueryPromise<Array<CartItem>>;
	// -------------------------------------------------------------------------------------------------
	/** Return a CartItem */
	function getCartItem(id: string): JQueryPromise<CartItem>;
	// -------------------------------------------------------------------------------------------------
	/** Initialize a new CartItem from the Url of an Product */
	function productToCartItem(
		productUrl: string,
		selectedVariantOptionUrls?: string[],
		success?: (cartItem: CartItem) => void
	): JQueryPromise<CartItem>;
	// -------------------------------------------------------------------------------------------------
	/** Add an CartItem to the Cart */
	function addToCart(
		cartItem: CartItem,
		success?: () => void
	): JQueryPromise<any>;
	// -------------------------------------------------------------------------------------------------
	/** Update an CartItem */
	function updateCartItem(
		cartItem: CartItem,
		success?: () => void
	): void;
	// -------------------------------------------------------------------------------------------------
	/** Update a CartItem Quantity */
	function updateCartItemQuantity(
		cartItemId: string,
		quantity: number,
		returnCart?: boolean
	): JQueryPromise<Cart>;
	// -------------------------------------------------------------------------------------------------
	/** Remove a CartItem from the Cart */
	function removeFromCart(cartItemId: string, success?: () => void): void;
	// -------------------------------------------------------------------------------------------------
	/** Add User Credit to Cart */
	function applyUserCreditToCart(
		amount: number,
		success?: (formResponse: IFormResponse<number>) => void
	): void;
	// -------------------------------------------------------------------------------------------------
	/** Remove User Credit from Cart */
	function removeUserCreditFromCart(
		success?: (formResponse: IFormResponse<number>) => void
	): void;
	// -------------------------------------------------------------------------------------------------
	/** Get User Credit Total */
	function getUserCreditTotal(): JQueryPromise<number>;
	// -------------------------------------------------------------------------------------------------
	/** Add Coupon to Cart */
	function applyCouponToCart(
		couponCode: string,
		success?: (formResponse: IFormResponse) => void
	): JQueryPromise<IFormResponse>;
	// -------------------------------------------------------------------------------------------------
	/** Remove Coupon from Cart */
	function removeCouponFromCart(success?: () => void): void;
	// -------------------------------------------------------------------------------------------------
	/** Return all addresses for an authenticated user */
	function getUserAddresses(): JQueryPromise<Array<Address>>;
	// -------------------------------------------------------------------------------------------------
	/** Add Or Update Address */
	function addOrUpdateAddress(
		address: Address,
		setUser?: boolean,
		setAsCartShippingAddress?: boolean,
		setAsCartBillingAddress?: boolean,
		success?: (formResponse: IFormResponse<Address>) => void
	): JQueryPromise<IFormResponse<Address>>;
	// -------------------------------------------------------------------------------------------------
	/** Delete Address */
	function deleteAddress(
		addressId: string,
		success?: (formResponse: IFormResponse) => void
	): JQueryPromise<IFormResponse>;
	// -------------------------------------------------------------------------------------------------
	/** Set Cart Shipping Address */
	function setCartShippingAddress(
		addressId: string,
		success?: (formResponse: IFormResponse) => void
	): JQueryPromise<IFormResponse>;
	// -------------------------------------------------------------------------------------------------
	/** Set Cart Billing Address */
	function setCartBillingAddress(
		addressId: string,
		success?: (formResponse: IFormResponse) => void
	): JQueryPromise<IFormResponse>;
	// -------------------------------------------------------------------------------------------------
	/** Get all the query string parameters */
	function getQueryStringParameters(): { name: string; value: string }[];
	// -------------------------------------------------------------------------------------------------
	/** Get a query string parameter by name */
	function getQueryStringParameter(name: string): string;
	// -------------------------------------------------------------------------------------------------
	/** Set a query string parameter */
	function setQueryStringParameter(name: string, value: string): void;
	// -------------------------------------------------------------------------------------------------
	/** Stringify an object to Json and encode the result as a valid URI Component */
	function encodeObjectToURIComponent(obj: any): string;
	// -------------------------------------------------------------------------------------------------
	/** Parse a Json encoded URI component to return the original object */
	function decodeObjectFromURIComponent<T>(
		uriComponent: string,
		fallbackValue?: T
	): T;
	// -------------------------------------------------------------------------------------------------
	/** Get a url parameter by its index, excluding the country parameter */
	function getUrlParameter(index: number): string;
	// -------------------------------------------------------------------------------------------------
	/** Safely access sub-properties of any object */
	function get<T>(func: () => T, fallbackValue?: T): T;
	// -------------------------------------------------------------------------------------------------
	/** Get an array of ContentSections by name and options */
	function getContentSections(
		contentSectionNames: Array<string>,
		languageCode?: string
	): JQueryPromise<Array<ContentSection>>;
	// -------------------------------------------------------------------------------------------------
	/** Render a ContentSection from an array of ContentSections */
	function contentSection(
		source: Array<ContentSection>,
		name: string,
		fallbackValue?: string
	): string;
	// -------------------------------------------------------------------------------------------------
	/** EXPERIMENTAL */
	function contentSectionAs<T>(
		source: Array<ContentSection>,
		name: string
	): T;
	// -------------------------------------------------------------------------------------------------
	/** Render the ContentSection value of a ProductAttribute */
	function contentSectionFromProductAttribute(
		productAttribute: ProductAttribute,
		fallbackValue?: string
	): string;
	// -------------------------------------------------------------------------------------------------
	/** Asynchronously log a message or exception */
	function log(
		type: LogTypesEnum,
		message: string,
		details?: string,
		avoidDuplication?: boolean
	): void;
	// -------------------------------------------------------------------------------------------------
	/** Return the localized version of a certain URL based on the current country */
	function getLocalizedUrl(url: string, countryUrl?: string);
	// -------------------------------------------------------------------------------------------------
	/** Return the current User Location using an IP database */
	function getUserLocation(
		success?: (country: Country) => void
	): JQueryPromise<Country>;
	// -------------------------------------------------------------------------------------------------
	/** Return the current Country */
	function getCurrentCountry(): JQueryPromise<Country>;
	// -------------------------------------------------------------------------------------------------
	/** Return the list of all the enabled Countries */
	function getCountries(): JQueryPromise<Array<Country>>;
	// -------------------------------------------------------------------------------------------------
	/** Return the list of all shipping boxes */
	function getShippingBoxes(): JQueryPromise<Array<ShippingBox>>;
	// -------------------------------------------------------------------------------------------------
	/** Sets the shipping box for a certain cart item */
	function setShippingBox(
		cartItemId: string,
		shippingBoxName: string
	): JQueryPromise<IFormResponse>;
	// -------------------------------------------------------------------------------------------------
	/** Register a new User */
	function registerUser(
		user: User,
		success?: (formResponse: IFormResponse) => void
	): JQueryPromise<IFormResponse>;
	// -------------------------------------------------------------------------------------------------
	/** Authenticate User */
	function authenticateUser(
		email: string,
		password: string,
		success?: (formResponse: IFormResponse) => void
	): JQueryPromise<IFormResponse>;
	// -------------------------------------------------------------------------------------------------
	/** Authenticate User using Facebook. Facebook JS SDK is required. */
	function authenticateUserWithFacebook(
		authenticationUrl: string,
		redirectUrl?: string,
		authenticationParams?: {}
	): void;
	// ----------------------------------------------------------------------------------------------------------------
	/** Authenticate User using Google. Google JS SDK is required.*/
	function authenticateUserWithGoogle(
		clientID: string,
		authenticationUrl: string,
		redirectUrl?: string,
		authenticationParams?: {}
	): void;
	// -------------------------------------------------------------------------------------------------
	/** Return the Authenticated User */
	function getAuthenticatedUser(): JQueryPromise<User>;
	// -------------------------------------------------------------------------------------------------
	/** Deep clone objects or arrays */
	function deepClone<T>(data: T): T;
	// -------------------------------------------------------------------------------------------------
	/** Queries an API */
	function ajaxPost<T>(
		url: string,
		data: any,
		success?: (response: T) => void
	): JQueryPromise<T>;
	// -------------------------------------------------------------------------------------------------
	/** Queries the API the first time, then returns a cached object */
	function ajaxPostAndCache<T>(
		url: string,
		data: any,
		success?: (response: T) => void
	): JQueryPromise<T>;
}
