﻿export enum OrderStatusEnum {
    Placed = 0,
    Confirmed = 50,
    Processing = 100,
    Completed = 200,
    Cancelled = 300
}