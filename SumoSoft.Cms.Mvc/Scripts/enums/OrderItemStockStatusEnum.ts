﻿export enum OrderItemStockStatusEnum {
    Decreased = 100,
    Restored = 200
}