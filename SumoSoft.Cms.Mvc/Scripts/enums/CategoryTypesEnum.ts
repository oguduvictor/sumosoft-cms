﻿export enum CategoryTypesEnum {
    Products = 0,
    ProductImageKits = 1,
    ProductStockUnits = 2,
}