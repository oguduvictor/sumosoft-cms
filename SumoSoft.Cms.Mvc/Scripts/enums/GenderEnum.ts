﻿export enum GenderEnum {
    None = 0,
    Male = 10,
    Female = 20,
}