﻿export enum VariantTypesEnum {
    Boolean = 0,
    Options = 1,
    Double = 2,
    Integer = 3,
    String = 4,
    Json = 5
}