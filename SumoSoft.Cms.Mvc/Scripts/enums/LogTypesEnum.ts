﻿export enum LogTypesEnum {
    general = 0,
    exception = 10,
    ecommerce = 20,
    content = 30
}