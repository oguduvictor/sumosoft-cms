﻿export enum MailingListSubscriptionStatusEnum {
    Subscribed = 0,
    Unsubscribed = 5,
    Invalid = 10
}
