﻿export enum OrderShippingBoxStatusEnum
{
    Preparing = 0,
    Dispatched = 100,
    Returned = 200
}