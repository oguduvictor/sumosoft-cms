﻿export enum AttributeTypesEnum {
    String = 0,
    Options = 2,
    Boolean = 3,
    DateTime = 4,
    Image = 5,
    Double = 6,
    ContentSection = 7,
    Json = 8
}
