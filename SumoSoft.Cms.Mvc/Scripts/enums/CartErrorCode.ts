﻿export enum CartErrorCode {
	CartItemWithDisabledProduct = 100,
	CartItemWithOutOfStockProduct = 150,
	CartItemWithInvalidShippingBox = 200,
	InvalidCoupon = 300,
	InvalidUserCredit = 400,
	InvalidTaxOverride = 500,
	LocalizedShippingAddressCountryNotMatchingCurrentCountry = 600,
	NonLocalizedShippingAddressCountryNotMatchingDefaultCountry = 700,
	ShippingAddressRequired = 800
}
