﻿export enum OrderItemStatusEnum {
    Default = 0,
    ReturnRequested = 100,
    ReturnCompleted = 200,
    ReturnDenied = 300
}