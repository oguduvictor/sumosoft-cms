﻿import { BaseEntity } from "./BaseEntity";
import { IUserRoleDto } from "../interfaces/IUserRoleDto";

export class UserRole extends BaseEntity implements IUserRoleDto {
    name: string;

    // -------------------------------------------------------------
    // Access to Administration panel sections
    // -------------------------------------------------------------

    accessAdmin: boolean;
    accessAdminEcommerce: boolean;
    accessAdminOrders: boolean;
    accessAdminBlog: boolean;
    accessAdminMedia: boolean;
    accessAdminContent: boolean;
    accessAdminEmails: boolean;
    accessAdminSeo: boolean;
    accessAdminUsers: boolean;
    accessAdminCountries: boolean;
    accessAdminSettings: boolean;

    // -------------------------------------------------------------
    // Orders section
    // -------------------------------------------------------------

    editOrder: boolean;

    // -------------------------------------------------------------
    // Product section
    // -------------------------------------------------------------

    createProduct: boolean;
    editProduct: boolean;
    editProductAllLocalizedKits: boolean;
    deleteProduct: boolean;

    // -------------------------------------------------------------
    // Attribute section
    // -------------------------------------------------------------

    createAttribute: boolean;
    editAttribute: boolean;
    editAttributeName: boolean;
    editAttributeAllLocalizedKits: boolean;
    deleteAttribute: boolean;

    // -------------------------------------------------------------
    // AttributeOption section
    // -------------------------------------------------------------

    createAttributeOption: boolean;
    editAttributeOption: boolean;
    editAttributeOptionName: boolean;
    editAttributeOptionAllLocalizedKits: boolean;
    deleteAttributeOption: boolean;

    // -------------------------------------------------------------
    // Variant section
    // -------------------------------------------------------------

    createVariant: boolean;
    editVariant: boolean;
    editVariantName: boolean;
    editVariantSelector: boolean;
    editVariantAllLocalizedKits: boolean;
    deleteVariant: boolean;

    // -------------------------------------------------------------
    // VariantOption section
    // -------------------------------------------------------------

    createVariantOption: boolean;
    editVariantOption: boolean;
    editVariantOptionName: boolean;
    editVariantOptionAllLocalizedKits: boolean;
    deleteVariantOption: boolean;

    // -------------------------------------------------------------
    // ShippingBox section
    // -------------------------------------------------------------

    createShippingBox: boolean;
    editShippingBox: boolean;
    editShippingBoxName: boolean;
    editShippingBoxAllLocalizedKits: boolean;
    deleteShippingBox: boolean;

    // -------------------------------------------------------------
    // Category section
    // -------------------------------------------------------------

    createCategory: boolean;
    editCategory: boolean;
    editCategoryName: boolean;
    editCategoryUrl: boolean;
    editCategoryAllLocalizedKits: boolean;
    deleteCategory: boolean;

    // -------------------------------------------------------------
    // Blog section
    // -------------------------------------------------------------

    createBlogPost: boolean;
    editBlogPost: boolean;
    deleteBlogPost: boolean;

    // -------------------------------------------------------------
    // Content section
    // -------------------------------------------------------------

    createContentSection: boolean;
    editContentSection: boolean;
    editContentSectionName: boolean;
    editContentSectionAllLocalizedKits: boolean;
    deleteContentSection: boolean;

    // -------------------------------------------------------------
    // Email section
    // -------------------------------------------------------------

    createEmail: boolean;
    editEmail: boolean;
    editEmailName: boolean;
    editEmailAllLocalizedKits: boolean;
    deleteEmail: boolean;

    // -------------------------------------------------------------
    // Maling List section
    // -------------------------------------------------------------

    createMailingList: boolean;
    editMailingList: boolean;
    editMailingListName: boolean;
    editMailingListAllLocalizedKits: boolean;
    deleteMailingList: boolean;

    // -------------------------------------------------------------
    // Seo section
    // -------------------------------------------------------------

    createSeoSection: boolean;
    editSeoSection: boolean;
    editSeoSectionPage: boolean;
    editSeoSectionAllLocalizedKits: boolean;
    deleteSeoSection: boolean;

    // -------------------------------------------------------------
    // User section
    // -------------------------------------------------------------

    createUser: boolean;
    editUser: boolean;
    deleteUser: boolean;

    // -------------------------------------------------------------
    // UserRole section
    // -------------------------------------------------------------

    createUserRole: boolean;
    editUserRole: boolean;
    deleteUserRole: boolean;

    // -------------------------------------------------------------
    // Country section
    // -------------------------------------------------------------

    createCountry: boolean;
    editCountry: boolean;
    deleteCountry: boolean;

    // -------------------------------------------------------------
    // Coupon section
    // -------------------------------------------------------------

    createCoupon: boolean;
    editCoupon: boolean;
    deleteCoupon: boolean;

    // -------------------------------------------------------------
    // Logs
    // -------------------------------------------------------------

    viewExceptionLogs: boolean;
    viewEcommerceLogs: boolean;
    viewContentLogs: boolean;

    constructor(dto?: IUserRoleDto | any) {
        super(dto);
        dto = dto || {} as IUserRoleDto;

        this.name = dto.name;
        this.accessAdmin = dto.accessAdmin;
        this.accessAdminEcommerce = dto.accessAdminEcommerce;
        this.accessAdminOrders = dto.accessAdminOrders;
        this.accessAdminBlog = dto.accessAdminBlog;
        this.accessAdminMedia = dto.accessAdminMedia;
        this.accessAdminContent = dto.accessAdminContent;
        this.accessAdminEmails = dto.accessAdminEmails;
        this.accessAdminSeo = dto.accessAdminSeo;
        this.accessAdminUsers = dto.accessAdminUsers;
        this.accessAdminCountries = dto.accessAdminCountries;
        this.accessAdminSettings = dto.accessAdminSettings;
        this.editOrder = dto.editOrder;
        this.createProduct = dto.createProduct;
        this.editProduct = dto.editProduct;
        this.editProductAllLocalizedKits = dto.editProductAllLocalizedKits;
        this.deleteProduct = dto.deleteProduct;
        this.createAttribute = dto.createAttribute;
        this.editAttribute = dto.editAttribute;
        this.editAttributeName = dto.editAttributeName;
        this.editAttributeAllLocalizedKits = dto.editAttributeAllLocalizedKits;
        this.deleteAttribute = dto.deleteAttribute;
        this.createAttributeOption = dto.createAttributeOption;
        this.editAttributeOption = dto.editAttributeOption;
        this.editAttributeOptionName = dto.editAttributeOptionName;
        this.editAttributeOptionAllLocalizedKits = dto.editAttributeOptionAllLocalizedKits;
        this.deleteAttributeOption = dto.deleteAttributeOption;
        this.deleteAttributeOption = dto.deleteAttributeOption;
        this.createVariant = dto.createVariant;
        this.editVariant = dto.editVariant;
        this.editVariantName = dto.editVariantName;
        this.editVariantSelector = dto.editVariantSelector;
        this.editVariantAllLocalizedKits = dto.editVariantAllLocalizedKits;
        this.deleteVariant = dto.deleteVariant;
        this.createVariantOption = dto.createVariantOption;
        this.editVariantOption = dto.editVariantOption;
        this.editVariantOptionName = dto.editVariantOptionName;
        this.editVariantOptionAllLocalizedKits = dto.editVariantOptionAllLocalizedKits;
        this.deleteVariantOption = dto.deleteVariantOption;
        this.createShippingBox = dto.createShippingBox;
        this.editShippingBox = dto.editShippingBox;
        this.editShippingBoxName = dto.editShippingBoxName;
        this.editShippingBoxAllLocalizedKits = dto.editShippingBoxAllLocalizedKits;
        this.deleteShippingBox = dto.deleteShippingBox;
        this.createCategory = dto.createCategory;
        this.editCategory = dto.editCategory;
        this.editCategoryName = dto.editCategoryName;
        this.editCategoryUrl = dto.editCategoryUrl;
        this.editCategoryAllLocalizedKits = dto.editCategoryAllLocalizedKits;
        this.deleteCategory = dto.deleteCategory;
        this.createBlogPost = dto.createBlogPost;
        this.editBlogPost = dto.editBlogPost;
        this.deleteBlogPost = dto.deleteBlogPost;
        this.createContentSection = dto.createContentSection;
        this.editContentSection = dto.editContentSection;
        this.editContentSectionName = dto.editContentSectionName;
        this.editContentSectionAllLocalizedKits = dto.editContentSectionAllLocalizedKits;
        this.deleteContentSection = dto.deleteContentSection;
        this.createEmail = dto.createEmail;
        this.editEmail = dto.editEmail;
        this.editEmailName = dto.editEmailName;
        this.editEmailAllLocalizedKits = dto.editEmailAllLocalizedKits;
        this.deleteEmail = dto.deleteEmail;
        this.createSeoSection = dto.createSeoSection;
        this.editSeoSection = dto.editSeoSection;
        this.editSeoSectionPage = dto.editSeoSectionPage;
        this.editSeoSectionAllLocalizedKits = dto.editSeoSectionAllLocalizedKits;
        this.deleteSeoSection = dto.deleteSeoSection;
        this.createUser = dto.createUser;
        this.editUser = dto.editUser;
        this.deleteUser = dto.deleteUser;
        this.createUserRole = dto.createUserRole;
        this.editUserRole = dto.editUserRole;
        this.deleteUserRole = dto.deleteUserRole;
        this.createCountry = dto.createCountry;
        this.editCountry = dto.editCountry;
        this.deleteCountry = dto.deleteCountry;
        this.editCoupon = dto.editCoupon;
        this.createCoupon = dto.createCoupon;
        this.deleteCoupon = dto.deleteCoupon;
        this.viewExceptionLogs = dto.viewExceptionLogs;
        this.viewEcommerceLogs = dto.viewEcommerceLogs;
        this.viewContentLogs = dto.viewContentLogs;
        this.createMailingList = dto.createMailingList;
        this.deleteMailingList = dto.deleteMailingList;
        this.editMailingList = dto.editMailingList;
        this.editMailingListAllLocalizedKits = dto.editMailingListAllLocalizedKits;
        this.editMailingListName = dto.editMailingListName;
    }
}
