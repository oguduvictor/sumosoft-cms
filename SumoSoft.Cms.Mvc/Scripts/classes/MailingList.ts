﻿import { BaseEntity } from "./BaseEntity";
import { IMailingListDto } from '../interfaces/IMailingListDto';
import { MailingListLocalizedKit } from './MailingListLocalizedKit';
import { MailingListSubscription } from './mailingListSubscription';

export class MailingList extends BaseEntity implements IMailingListDto {
    name: string;
    sortOrder: number;
    mailingListSubscriptions: Array<MailingListSubscription>;
    mailingListLocalizedKits: Array<MailingListLocalizedKit>;
    localizedTitle: string;
    localizedDescription: string;
    totalUsers: number;
    totalSubscribers: number;
   
    constructor(dto?: IMailingListDto | any) {
        super(dto);

        dto = dto || ({} as IMailingListDto);
        this.name = dto.name;
        this.sortOrder = dto.sortOrder;
        this.localizedTitle = dto.localizedTitle;
        this.localizedDescription = dto.localizedDescription;
        this.totalUsers = dto.totalUsers;
        this.totalSubscribers = dto.totalSubscribers;
        this.mailingListSubscriptions = dto.mailingListSubscriptions ? dto.mailingListSubscriptions.map(x => new MailingListSubscription(x)) : new Array<MailingListSubscription>();
        this.mailingListLocalizedKits = dto.mailingListLocalizedKits ? dto.mailingListLocalizedKits.map(x => new MailingListLocalizedKit(x)) : new Array<MailingListLocalizedKit>();
    }
}
