﻿import { CartErrorCode } from '../enums/CartErrorCode';
import { ICartError } from '../interfaces/ICartError';

export class CartError implements ICartError {
	code: CartErrorCode;
	message: string;
	targetId: string;

	constructor(dto: ICartError) {
		dto = dto || ({} as ICartError);
		this.code = dto.code;
		this.message = dto.message;
		this.targetId = dto.targetId;
	}
}
