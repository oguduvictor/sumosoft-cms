﻿import { BaseEntity } from './BaseEntity';
import { CartShippingBox } from './CartShippingBox';
import { Coupon } from './Coupon';
import { ICartDto } from '../interfaces/ICartDto';
import { Address } from './Address';
import { User } from './User';
import { UserCredit } from './UserCredit';
import { CartError } from './CartError';
import { SentEmail } from './SentEmail';

export class Cart extends BaseEntity implements ICartDto {
	cartShippingBoxes: Array<CartShippingBox>;
	shippingAddress: Address;
	billingAddress: Address;
	user: User;
	coupon: Coupon;
	userCredit: UserCredit;
	couponValue: number;
	totalBeforeTax: number;
	totalAfterTax: number;
	totalToBePaid: number;
    errors: Array<CartError>;
    sentEmails: Array<SentEmail>;

	getCartShippingBoxByName(shippingBoxName: string) {
		return this.cartShippingBoxes.filter(
			x => x.shippingBox && x.shippingBox.name === shippingBoxName
		)[0];
	}

	constructor(dto?: ICartDto | any) {
		super(dto);

		dto = dto || ({} as ICartDto);
		this.cartShippingBoxes = dto.cartShippingBoxes
			? dto.cartShippingBoxes.map(x => new CartShippingBox(x))
			: new Array<CartShippingBox>();
		this.shippingAddress = dto.shippingAddress
			? new Address(dto.shippingAddress)
			: null;
		this.billingAddress = dto.billingAddress
			? new Address(dto.billingAddress)
			: null;
		this.user = dto.user ? new User(dto.user) : null;
		this.coupon = dto.coupon ? new Coupon(dto.coupon) : null;
		this.userCredit = dto.userCredit
			? new UserCredit(dto.userCredit)
			: null;
        this.couponValue = dto.couponValue;
        this.sentEmails = dto.sentEmails ? dto.sentEmails.map(x => new SentEmail(x)) : new Array<SentEmail>();
		this.totalBeforeTax = dto.totalBeforeTax;
		this.totalAfterTax = dto.totalAfterTax;
		this.totalToBePaid = dto.totalToBePaid;
		this.errors = dto.errors
			? dto.errors.map(x => new CartError(x))
			: new Array<CartError>();
	}
}
