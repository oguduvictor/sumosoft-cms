﻿import { BaseEntity } from './BaseEntity';
import { ISentEmailDto } from '../interfaces/ISentEmailDto';
import { Email } from './Email';
import { Order } from './Order';
import { Coupon } from './Coupon';
import { User } from './User';

export class SentEmail extends BaseEntity implements ISentEmailDto {
	email: Email;
	user: User;
	order: Order;
	coupon: Coupon;
	from: string;
	to: string;
	reference: string;

	constructor(dto?: ISentEmailDto | any) {
		super(dto);
		dto = dto || ({} as ISentEmailDto);

		this.email = dto.email ? new Email(dto.email) : null;
		this.user = dto.user ? new User(dto.user) : null;
		this.order = dto.order ? new Order(dto.order) : null;
		this.coupon = dto.coupon ? new Coupon(dto.coupon) : null;
		this.from = dto.from;
		this.to = dto.to;
		this.reference = dto.reference;
	}
}
