﻿import { BaseEntity } from "./BaseEntity";
import { VariantTypesEnum } from "../enums/VariantTypesEnum";
import { OrderItem } from "./OrderItem";
import { IOrderItemVariantDto } from "../interfaces/IOrderItemVariantDto";

export class OrderItemVariant extends BaseEntity implements IOrderItemVariantDto {
    orderItem: OrderItem;
    variantType: VariantTypesEnum;
    variantName: string;
    variantLocalizedTitle: string;
    booleanValue: boolean;
    doubleValue: number;
    integerValue: number;
    stringValue: string;
    jsonValue: string;
    variantOptionName: string;
    variantOptionUrl: string;
    variantOptionCode: string;
    variantOptionLocalizedTitle: string;
    variantOptionLocalizedDescription: string;
    variantOptionLocalizedPriceModifier: number;
    variantUrl: string;
    variantLocalizedDescription: string;

    constructor(dto?: IOrderItemVariantDto | any) {
        super(dto);

        dto = dto || {} as IOrderItemVariantDto;
        this.orderItem = dto.orderItem ? new OrderItem(dto.orderItem) : null;
        this.variantType = dto.variantType;
        this.variantName = dto.variantName;
        this.variantLocalizedTitle = dto.variantLocalizedTitle;
        this.booleanValue = dto.booleanValue;
        this.doubleValue = dto.doubleValue;
        this.integerValue = dto.integerValue;
        this.stringValue = dto.stringValue;
        this.jsonValue = dto.jsonValue;
        this.variantOptionName = dto.variantOptionName;
        this.variantOptionCode = dto.variantOptionCode;
        this.variantOptionUrl = dto.variantOptionUrl;
        this.variantOptionLocalizedTitle = dto.variantOptionLocalizedTitle;
        this.variantOptionLocalizedDescription = dto.variantOptionLocalizedDescription;
        this.variantOptionLocalizedPriceModifier = dto.variantOptionLocalizedPriceModifier;
        this.variantLocalizedDescription = dto.variantLocalizedDescription;
        this.variantUrl = dto.variantUrl;
    }
}