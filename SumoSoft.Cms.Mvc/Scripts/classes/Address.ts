﻿import { IAddressDto } from "../interfaces/IAddressDto";
import { BaseEntity } from "./BaseEntity";
import { User } from "./User";
import { Country } from "./Country";

export class Address extends BaseEntity implements IAddressDto {
    user: User;
    firstName: string;
    lastName: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    country: Country;
    stateCountyProvince: string;
    postcode: string;
    phoneNumber: string;

    constructor(dto?: IAddressDto | any) {
        super(dto);

        dto = dto || {} as IAddressDto;
        this.user = dto.user ? new User(dto.user) : null;
        this.firstName = dto.firstName;
        this.lastName = dto.lastName;
        this.addressLine1 = dto.addressLine1;
        this.addressLine2 = dto.addressLine2;
        this.city = dto.city;
        this.country = dto.country ? new Country(dto.country) : null;
        this.stateCountyProvince = dto.stateCountyProvince;
        this.postcode = dto.postcode;
        this.phoneNumber = dto.phoneNumber;
    }
}