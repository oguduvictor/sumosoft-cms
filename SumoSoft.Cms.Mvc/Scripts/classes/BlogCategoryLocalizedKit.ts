﻿import { BlogCategory } from "./BlogCategory";
import { BaseEntity } from "./BaseEntity";
import { Country } from "./Country";
import { IBlogCategoryLocalizedKitDto } from "../interfaces/IBlogCategoryLocalizedKitDto";

export class BlogCategoryLocalizedKit extends BaseEntity implements IBlogCategoryLocalizedKitDto {
    blogCategory: BlogCategory;
    country: Country;
    title: string;
    description: string;

    constructor(dto?: IBlogCategoryLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IBlogCategoryLocalizedKitDto;
        this.blogCategory = dto.blogCategory ? new BlogCategory(dto.blogCategory) : null;
        this.country = dto.country ? new Country(dto.country) : null;
        this.title = dto.title;
        this.description = dto.description;
    }
}