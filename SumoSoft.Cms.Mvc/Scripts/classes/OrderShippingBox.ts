﻿import { BaseEntity } from "./BaseEntity";
import { Order } from "./Order";
import { OrderItem } from "./OrderItem";
import { IOrderShippingBoxDto } from "../interfaces/IOrderShippingBoxDto";
import { OrderShippingBoxStatusEnum } from "../enums/OrderShippingBoxStatusEnum";

export class OrderShippingBox extends BaseEntity implements IOrderShippingBoxDto {
    order: Order;
    name: string;
    title: string;
    description: string;
    internalDescription: string;
    minDays: number;
    maxDays: number;
    countWeekends: boolean;
    trackingCode: string;
    shippingPriceBeforeTax: number;
    shippingPriceAfterTax: number;
    status: OrderShippingBoxStatusEnum;
    statusDescriptions: Array<string>;
    orderItems = new Array<OrderItem>();

    constructor(dto?: IOrderShippingBoxDto | any) {
        super(dto);

        dto = dto || {} as IOrderShippingBoxDto;
        this.order = dto.order ? new Order(dto.order) : null;
        this.name = dto.name;
        this.title = dto.title;
        this.description = dto.description;
        this.internalDescription = dto.internalDescription;
        this.minDays = dto.minDays;
        this.maxDays = dto.maxDays;
        this.countWeekends = dto.countWeekends;
        this.trackingCode = dto.trackingCode;
        this.shippingPriceBeforeTax = dto.shippingPriceBeforeTax;
        this.shippingPriceAfterTax = dto.shippingPriceAfterTax;
        this.status = dto.status;
        this.statusDescriptions = dto.statusDescriptions || new Array<string>();
        this.orderItems = dto.orderItems ? dto.orderItems.map(x => new OrderItem(x)) : new Array<OrderItem>();
    }
}