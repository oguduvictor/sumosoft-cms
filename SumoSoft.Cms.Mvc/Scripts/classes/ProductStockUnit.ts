﻿import { BaseEntity } from "./BaseEntity";
import { VariantOption } from "./VariantOption";
import { Category } from "./Category";
import { Product } from "./Product";
import { IProductStockUnitDto } from "../interfaces/IProductStockUnitDto";
import { ProductStockUnitLocalizedKit } from "./ProductStockUnitLocalizedKit";

export class ProductStockUnit extends BaseEntity implements IProductStockUnitDto {
    product: Product;
    productStockUnitLocalizedKits = new Array<ProductStockUnitLocalizedKit>();
    variantOptions = new Array<VariantOption>();
    categories = new Array<Category>();
    dispatchDate: string;
    stock: number;
    code: string;
    enablePreorder: boolean;
    dispatchTime: number;
    localizedBasePrice: number;
    localizedSalePrice: number;
    localizedMembershipSalePrice: number;

    matches(selectedVariantOptions: Array<VariantOption>) {
        return this.variantOptions.every(variantOption => selectedVariantOptions.some(x => x && x.id && x.id === variantOption.id));
    }

    hasCategories(categories: Category[]) {
        return categories.every(x => this.categories.some(y => y.id === x.id));
    }

    getLocalizedBasePrice(languageCode?: string) {
        const productStockUnitLocalizedKit = this.productStockUnitLocalizedKits.getByLanguageCode(languageCode);
        return productStockUnitLocalizedKit
            ? productStockUnitLocalizedKit.basePrice
            : this.localizedBasePrice;
    }

    getLocalizedSalePrice(languageCode?: string) {
        const productStockUnitLocalizedKit = this.productStockUnitLocalizedKits.getByLanguageCode(languageCode);
        return productStockUnitLocalizedKit
            ? productStockUnitLocalizedKit.salePrice
            : this.localizedSalePrice;
    }

    constructor(dto?: IProductStockUnitDto | any) {
        super(dto);

        dto = dto || {} as IProductStockUnitDto;
        this.product = dto.product ? new Product(dto.product) : null;
        this.productStockUnitLocalizedKits = dto.productStockUnitLocalizedKits ? dto.productStockUnitLocalizedKits.map(x => new ProductStockUnitLocalizedKit(x)) : new Array<ProductStockUnitLocalizedKit>();
        this.variantOptions = dto.variantOptions ? dto.variantOptions.map(x => new VariantOption(x)) : new Array<VariantOption>();
        this.categories = dto.categories ? dto.categories.map(x => new Category(x)) : new Array<Category>();
        this.dispatchDate = dto.dispatchDate;
        this.stock = dto.stock;
        this.code = dto.code;
        this.enablePreorder = dto.enablePreorder;
        this.dispatchTime = dto.dispatchTime;
        this.isDeleted = dto.isDeleted;
        this.localizedBasePrice = dto.localizedBasePrice;
        this.localizedSalePrice = dto.localizedSalePrice;
        this.localizedMembershipSalePrice = dto.localizedMembershipSalePrice;
    }
}
