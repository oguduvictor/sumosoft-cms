﻿import { BaseEntity } from "./BaseEntity";
import { IShippingBoxLocalizedKitDto } from "../interfaces/IShippingBoxLocalizedKitDto";
import { Country } from "./Country";
import { ShippingBox } from "./ShippingBox";

export class ShippingBoxLocalizedKit extends BaseEntity implements IShippingBoxLocalizedKitDto {
    title: string;
    description: string;
    carrier: string;
    internalDescription: string;
    minDays: number;
    maxDays: number;
    countWeekends: boolean;
    price: number;
    freeShippingMinimumPrice: number;
    shippingBox: ShippingBox;
    country: Country;

    constructor(dto?: IShippingBoxLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IShippingBoxLocalizedKitDto;
        this.title = dto.title;
        this.description = dto.description;
        this.carrier = dto.carrier;
        this.internalDescription = dto.internalDescription;
        this.minDays = dto.minDays;
        this.maxDays = dto.maxDays;
        this.countWeekends = dto.countWeekends;
        this.price = dto.price;
        this.freeShippingMinimumPrice = dto.freeShippingMinimumPrice;
        this.shippingBox = dto.shippingBox ? new ShippingBox(dto.shippingBox) : null;
        this.country = dto.country ? new Country(dto.country) : null;
    }
}