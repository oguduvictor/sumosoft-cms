﻿import { BaseEntity } from "./BaseEntity";
import { VariantTypesEnum } from "../enums/VariantTypesEnum";
import { VariantOption } from "./VariantOption";
import { IVariantDto } from "../interfaces/IVariantDto";
import { VariantLocalizedKit } from "./VariantLocalizedKit";

export class Variant extends BaseEntity implements IVariantDto {
    name: string;
    localizedTitle: string;
    localizedDescription: string;
    url: string;
    sortOrder: number;
    defaultDoubleValue: number;
    defaultIntegerValue: number;
    defaultStringValue: string;
    defaultJsonValue: string;
    defaultBooleanValue: boolean;
    createProductStockUnits: boolean;
    createProductImageKits: boolean;
    type: VariantTypesEnum;
    typeDescriptions: Array<string>;
    defaultVariantOptionValue: VariantOption;
    variantOptions = new Array<VariantOption>();
    variantLocalizedKits = new Array<VariantLocalizedKit>();

    getLocalizedTitle(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.variantLocalizedKits, languageCode).title;
    }

    getLocalizedDescription(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.variantLocalizedKits, languageCode).description;
    }

    getVariantOptionByName(variantOptionName: string) {
        return this.variantOptions.filter(x => x.name === variantOptionName)[0];
    }

    getVariantOptionByUrl(variantOptionUrl: string) {
        return this.variantOptions.filter(x => x.url === variantOptionUrl)[0];
    }

    constructor(dto?: IVariantDto | any) {
        super(dto);

        dto = dto || {} as IVariantDto;
        this.name = dto.name;
        this.localizedTitle = dto.localizedTitle;
        this.localizedDescription = dto.localizedDescription;
        this.url = dto.url;
        this.sortOrder = dto.sortOrder;
        this.defaultDoubleValue = dto.defaultDoubleValue;
        this.defaultIntegerValue = dto.defaultIntegerValue;
        this.defaultStringValue = dto.defaultStringValue;
        this.defaultJsonValue = dto.defaultJsonValue;
        this.defaultBooleanValue = dto.defaultBooleanValue;
        this.createProductStockUnits = dto.createProductStockUnits;
        this.createProductImageKits = dto.createProductImageKits;
        this.type = dto.type;
        this.typeDescriptions = dto.typeDescriptions || new Array<string>();
        this.defaultVariantOptionValue = dto.defaultVariantOptionValue ? new VariantOption(dto.defaultVariantOptionValue) : null;
        this.variantOptions = dto.variantOptions ? dto.variantOptions.map(x => new VariantOption(x)) : new Array<VariantOption>();
        this.variantLocalizedKits = dto.variantLocalizedKits ? dto.variantLocalizedKits.map(x => new VariantLocalizedKit(x)) : new Array<VariantLocalizedKit>();
    }
}