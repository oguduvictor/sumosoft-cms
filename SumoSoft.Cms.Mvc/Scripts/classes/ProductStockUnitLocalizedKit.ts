﻿import { BaseEntity } from "./BaseEntity";
import { IProductStockUnitLocalizedKitDto } from "../interfaces/IProductStockUnitLocalizedKitDto";
import { Country } from "./Country";
import { ProductStockUnit } from "./ProductStockUnit";

export class ProductStockUnitLocalizedKit extends BaseEntity implements IProductStockUnitLocalizedKitDto {
    productStockUnit: ProductStockUnit;
    country: Country;
    basePrice: number;
    salePrice: number;
    membershipSalePrice: number;

    constructor(dto?: IProductStockUnitLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IProductStockUnitLocalizedKitDto;
        this.productStockUnit = dto.productStockUnit ? new ProductStockUnit(dto.productStockUnit) : null;
        this.country = dto.country ? new Country(dto.country) : null;
        this.basePrice = dto.basePrice;
        this.salePrice = dto.salePrice;
        this.membershipSalePrice = dto.membershipSalePrice;
    }
}