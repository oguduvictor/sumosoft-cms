﻿import { SeoSectionLocalizedKit } from "./SeoSectionLocalizedKit";
import { BaseEntity } from "../classes/BaseEntity";
import { ISeoSectionDto } from "../interfaces/ISeoSectionDto";

export class SeoSection extends BaseEntity implements ISeoSectionDto {
    id: string;
    page: string;
    seoSectionLocalizedKits: Array<SeoSectionLocalizedKit>;
    localizedTitle: string;
    localizedDescription: string;
    localizedImage: string;

    getLocalizedTitle?(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.seoSectionLocalizedKits, languageCode).title;
    }

    getLocalizedDescription?(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.seoSectionLocalizedKits, languageCode).description;
    }

    getLocalizedImage?(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.seoSectionLocalizedKits, languageCode).image;
    }

    constructor(dto?: ISeoSectionDto | any) {
        super(dto);

        dto = dto || {} as ISeoSectionDto;
        this.page = dto.page;
        this.localizedTitle = dto.localizedTitle;
        this.localizedDescription = dto.localizedDescription;
        this.localizedImage = dto.localizedImage;
        this.seoSectionLocalizedKits = dto.seoSectionLocalizedKits ? dto.seoSectionLocalizedKits.map(x => new SeoSectionLocalizedKit(x)) : new Array<SeoSectionLocalizedKit>();
    }
}