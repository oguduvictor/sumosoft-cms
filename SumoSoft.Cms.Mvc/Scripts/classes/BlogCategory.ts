﻿import { IBlogCategoryDto } from "../interfaces/IBlogCategoryDto";
import { BaseEntity } from "./BaseEntity";
import { BlogPost } from "./BlogPost";
import { BlogCategoryLocalizedKit } from "./BlogCategoryLocalizedKit";

export class BlogCategory extends BaseEntity implements IBlogCategoryDto {
    name: string;
    url: string;
    blogPosts: Array<BlogPost>;
    blogCategoryLocalizedKits: Array<BlogCategoryLocalizedKit>;
    localizedTitle: string;
    localizedDescription: string;

    constructor(dto?: IBlogCategoryDto | any) {
        super(dto);

        dto = dto || {} as IBlogCategoryDto;
        this.name = dto.name;
        this.url = dto.url;
        this.blogPosts = dto.blogPosts ? dto.blogPosts.map(x => new BlogPost(x)) : new Array<BlogPost>();
        this.blogCategoryLocalizedKits = dto.blogCategoryLocalizedKits ? dto.blogCategoryLocalizedKits.map(x => new BlogCategoryLocalizedKit(x)) : new Array<BlogCategoryLocalizedKit>();
        this.localizedTitle = dto.localizedTitle;
        this.localizedDescription = dto.localizedDescription;
    }
}    