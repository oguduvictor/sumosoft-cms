﻿import { ICountryDto } from "../interfaces/ICountryDto";
import { BaseEntity } from "./BaseEntity";
import { Tax } from "./Tax";
import { ShippingBox } from "./ShippingBox";

export class Country extends BaseEntity implements ICountryDto {
    flagIcon: string;
    url: string;
    name: string;
    languageCode: string;
    isDefault: boolean;
    localize: boolean;
    currencySymbol: string;
    iso4217CurrencySymbol: string;
    urlForStringConcatenation: string;
    paymentAccountId: string;
    taxes: Array<Tax>;
    shippingBoxes: Array<ShippingBox>;

    getUrlForStringConcatenation() {
        return !this.url || this.url === "" ? "" : `/${this.url}`;
    }

    constructor(dto?: ICountryDto | any) {
        super(dto);

        dto = dto || {} as ICountryDto;
        this.flagIcon = dto.flagIcon;
        this.url = dto.url;
        this.name = dto.name;
        this.languageCode = dto.languageCode;
        this.isDefault = dto.isDefault;
        this.localize = dto.localize;
        this.currencySymbol = dto.currencySymbol;
        this.iso4217CurrencySymbol = dto.iso4217CurrencySymbol;
        this.urlForStringConcatenation = dto.urlForStringConcatenation;
        this.paymentAccountId = dto.paymentAccountId;
        this.taxes = dto.taxes ? dto.taxes.map(x => new Tax(x)) : new Array<Tax>();
        this.shippingBoxes = dto.shippingBoxes ? dto.shippingBoxes.map(x => new ShippingBox(x)) : new Array<ShippingBox>();
    }
}