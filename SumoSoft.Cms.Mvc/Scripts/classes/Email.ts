﻿import { BaseEntity } from "./BaseEntity";
import { IEmailDto } from "../interfaces/IEmailDto";
import { EmailLocalizedKit } from "./EmailLocalizedKit";
import { ContentSection } from './ContentSection';

export class Email extends BaseEntity implements IEmailDto {
    name: string;
    viewName: string;
    contentSection: ContentSection;
    emailLocalizedKits = new Array<EmailLocalizedKit>();
    localizedFrom: string;
    localizedDisplayName: string;
    localizedReplyTo: string;

    getLocalizedFrom(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.emailLocalizedKits, languageCode).from;
    }

    getLocalizedDisplayName(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.emailLocalizedKits, languageCode).displayName;
    }

    getLocalizedReplyTo(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.emailLocalizedKits, languageCode).replyTo;
    }

    constructor(dto?: IEmailDto | any) {
        super(dto);

        dto = dto || {} as IEmailDto;
        this.name = dto.name;
        this.contentSection = dto.contentSection ? new ContentSection(dto.contentSection) : null;
        this.viewName = dto.viewName;
        this.localizedFrom = dto.localizedFrom;
        this.localizedDisplayName = dto.localizedDisplayName;
        this.localizedReplyTo = dto.localizedReplyTo;
        this.emailLocalizedKits = dto.emailLocalizedKits ? dto.emailLocalizedKits.map(x => new EmailLocalizedKit(x)) : new Array<EmailLocalizedKit>();
    }
}