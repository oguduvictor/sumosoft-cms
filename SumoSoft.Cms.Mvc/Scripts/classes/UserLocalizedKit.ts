﻿import { BaseEntity } from "./BaseEntity";
import { IUserLocalizedKitDto } from "../interfaces/IUserLocalizedKitDto";
import { User } from "./User";
import { UserCredit } from "./UserCredit";
import { Country } from "./Country";

export class UserLocalizedKit extends BaseEntity implements IUserLocalizedKitDto {
    country: Country;
    user: User;
    userCredits = new Array<UserCredit>();

    constructor(dto?: IUserLocalizedKitDto | any) {
        super(dto);
        dto = dto || {} as IUserLocalizedKitDto;

        this.user = dto.user ? new User(dto.user) : null;
        this.country = dto.country ? new Country(dto.country) : null;
        this.userCredits = dto.userCredits ? dto.userCredits.map(x => new UserCredit(x)) : new Array<UserCredit>();
    }
}