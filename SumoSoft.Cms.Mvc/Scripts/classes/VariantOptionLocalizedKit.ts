﻿import { BaseEntity } from "./BaseEntity";
import { IVariantOptionLocalizedKitDto } from "../interfaces/IVariantOptionLocalizedKitDto";
import { VariantOption } from "./VariantOption";
import { Country } from "./Country";

export class VariantOptionLocalizedKit extends BaseEntity implements IVariantOptionLocalizedKitDto {
    title: string;
    description: string;
    priceModifier: number;
    variantOption: VariantOption;
    country: Country;

    constructor(dto?: IVariantOptionLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IVariantOptionLocalizedKitDto;
        this.title = dto.title;
        this.description = dto.description;
        this.priceModifier = dto.priceModifier;
        this.variantOption = dto.variantOption ? new VariantOption(dto.variantOption) : null;
        this.country = dto.country ? new Country(dto.country) : null;
    }
}