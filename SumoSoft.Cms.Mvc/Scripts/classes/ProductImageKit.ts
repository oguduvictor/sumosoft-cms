﻿import { BaseEntity } from "./BaseEntity";
import { VariantOption } from "./VariantOption";
import { ProductImage } from "./ProductImage";
import { Product } from "./Product";
import { Category } from "./Category";
import { IProductImageKitDto } from "../interfaces/IProductImageKitDto";

export class ProductImageKit extends BaseEntity implements IProductImageKitDto {
    product: Product;
    variantOptions = new Array<VariantOption>();
    productImages = new Array<ProductImage>();
    categories = new Array<Category>();
    relatedProductImageKits = new Array<ProductImageKit>();

    hasCategories(categories: Category[]) {
        return categories.every(x => this.categories.some(y => y.id === x.id));
    }

    getProductImageByName(productImageName: string): ProductImage {
        return this.productImages.filter(x => x.name === productImageName)[0];
    }

    matches(selectedVariantOptions: Array<VariantOption>) {
        return this.variantOptions.every(variantOption => selectedVariantOptions.some(x => x && x.id === variantOption.id));
    }

    constructor(dto?: IProductImageKitDto | any) {
        super(dto);

        dto = dto || {} as IProductImageKitDto;
        this.product = dto.product ? new Product(dto.product) : null;
        this.variantOptions = dto.variantOptions ? dto.variantOptions.map(x => new VariantOption(x)) : new Array<VariantOption>();
        this.productImages = dto.productImages ? dto.productImages.map(x => new ProductImage(x)) : new Array<ProductImage>();
        this.categories = dto.categories ? dto.categories.map(x => new Category(x)) : new Array<Category>();
        this.relatedProductImageKits = dto.relatedProductImageKits ? dto.relatedProductImageKits.map(x => new ProductImageKit(x)) : new Array<ProductImageKit>();
    }
}
