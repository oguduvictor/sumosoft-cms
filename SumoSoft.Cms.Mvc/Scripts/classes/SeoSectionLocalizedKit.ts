﻿import { BaseEntity } from "./BaseEntity";
import { Country } from "./Country";
import { SeoSection } from "./SeoSection";
import { ISeoSectionLocalizedKitDto } from "../interfaces/ISeoSectionLocalizedKitDto";

export class SeoSectionLocalizedKit extends BaseEntity implements ISeoSectionLocalizedKitDto {
    country: Country;
    seoSection: SeoSection;
    title: string;
    description: string;
    image: string;

    constructor(dto?: ISeoSectionLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as ISeoSectionLocalizedKitDto;
        this.seoSection = dto.seoSection ? new SeoSection(dto.seoSection) : null;
        this.country = dto.country ? new Country(dto.country) : null;
        this.title = dto.title;
        this.description = dto.description;
        this.image = dto.image;
    }
}


