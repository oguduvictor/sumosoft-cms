﻿import { BaseEntity } from './BaseEntity';
import { IWishListDto } from '../interfaces/IWishListDto';
import { User } from './User';
import { CartItem } from './CartItem';

export class WishList extends BaseEntity implements IWishListDto {
	user: User;
	cartItems = new Array<CartItem>();

	constructor(dto?: IWishListDto | any) {
		super(dto);

		dto = dto || ({} as IWishListDto);
		this.user = dto.user ? new User(dto.user) : null;
		this.cartItems = dto.cartItems
			? dto.cartItems.map(x => new CartItem(x))
			: new Array<CartItem>();
	}
}
