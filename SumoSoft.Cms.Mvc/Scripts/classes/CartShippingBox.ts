﻿import { BaseEntity } from './BaseEntity';
import { Cart } from './Cart';
import { ShippingBox } from './ShippingBox';
import { CartItem } from './CartItem';
import { ICartShippingBoxDto } from '../interfaces/ICartShippingBoxDto';

export class CartShippingBox extends BaseEntity
	implements ICartShippingBoxDto {
	shippingBox: ShippingBox;
	cart: Cart;
	cartItems = new Array<CartItem>();
	shippingPriceBeforeTax: number;
	shippingPriceAfterTax: number;
	totalBeforeTax: number;
	totalAfterTax: number;

	constructor(dto?: ICartShippingBoxDto | any) {
		super(dto);

		dto = dto || ({} as ICartShippingBoxDto);
		this.shippingBox = dto.shippingBox
			? new ShippingBox(dto.shippingBox)
			: null;
		this.cart = dto.cart ? new Cart(dto.cart) : null;
		this.cartItems = dto.cartItems
			? dto.cartItems.map(x => new CartItem(x))
			: new Array<CartItem>();
		this.shippingPriceBeforeTax = dto.shippingPriceBeforeTax;
		this.shippingPriceAfterTax = dto.shippingPriceAfterTax;
		this.totalBeforeTax = dto.totalBeforeTax;
		this.totalAfterTax = dto.totalAfterTax;
	}
}
