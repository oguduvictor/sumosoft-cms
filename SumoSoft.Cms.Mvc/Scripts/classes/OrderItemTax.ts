﻿import { BaseEntity } from "./BaseEntity";
import { OrderItem } from "./OrderItem";
import { IOrderItemTaxDto } from '../interfaces/IOrderItemTaxDto';

export class OrderItemTax extends BaseEntity implements IOrderItemTaxDto {
    orderItem: OrderItem;
    name: string;
    code: string;
    amount: number;

    constructor(dto?: IOrderItemTaxDto | any) {
        super(dto);
        dto = dto || {} as IOrderItemTaxDto;
        this.orderItem = dto.orderItem ? new OrderItem(dto.orderItem) : null;
        this.name = dto.name;
        this.code = dto.code;
        this.amount = dto.amount;
    }
}