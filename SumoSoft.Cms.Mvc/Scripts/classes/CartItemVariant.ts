﻿import { BaseEntity } from "./BaseEntity";
import { CartItem } from "./CartItem";
import { Variant } from "./Variant";
import { VariantOption } from "./VariantOption";
import { ICartItemVariantDto } from "../interfaces/ICartItemVariantDto";

export class CartItemVariant extends BaseEntity implements ICartItemVariantDto {
    cartItem: CartItem;
    variant: Variant;
    booleanValue: boolean;
    doubleValue: number;
    integerValue: number;
    stringValue: string;
    jsonValue: string;
    variantOptionValue: VariantOption;
    priceModifier: number;

    /** Return the price modifier of the VariantOption that is currently selected */
    getPriceModifier(languageCode?: string) {
        const priceModifier = languageCode ?
            this.variantOptionValue && this.variantOptionValue.getLocalizedPriceModifier(languageCode) :
            this.variantOptionValue && this.variantOptionValue.localizedPriceModifier;
        return priceModifier || 0;
    }

    getDefaultVariantOptionValue() {
        const productVariant = this.cartItem.product.getProductVariantByName(this.variant && this.variant.name);
        if (!productVariant) return null;
        return this.cartItem && this.cartItem.product && this.cartItem.product.getProductVariantByName(this.variant && this.variant.name) ?
            productVariant.defaultVariantOptionValue :
            productVariant.variantOptions[0];
    }

    constructor(dto?: ICartItemVariantDto | any) {
        super(dto);
        dto = dto || {} as ICartItemVariantDto;

        this.cartItem = dto.cartItem ? new CartItem(dto.cartItem) : null;
        this.variant = dto.variant ? new Variant(dto.variant) : null;
        this.booleanValue = dto.booleanValue;
        this.doubleValue = dto.doubleValue;
        this.integerValue = dto.integerValue;
        this.stringValue = dto.stringValue;
        this.jsonValue = dto.jsonValue;
        this.variantOptionValue = dto.variantOptionValue ? new VariantOption(dto.variantOptionValue) : null;
        this.priceModifier = dto.priceModifier;
    }
}