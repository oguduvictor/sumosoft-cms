﻿import { FileDto } from "./FileDto";
import { IDirectoryDto } from "../interfaces/IDirectoryDto";

export class DirectoryDto implements IDirectoryDto {
    name: string;
    localPath: string;
    size: number;
    creationTime: string;
    useAzureStorage: boolean;
    files = new Array<FileDto>();
    directories = new Array<DirectoryDto>();
    parent: DirectoryDto;
    isRoot: boolean;

    constructor(dto?: IDirectoryDto | any) {
        dto = dto || {} as IDirectoryDto;
        this.name = dto.name;
        this.localPath = dto.localPath;
        this.size = dto.size;
        this.creationTime = dto.creationTime;
        this.useAzureStorage = dto.useAzureStorage;
        this.files = dto.files ? dto.files.map(x => new FileDto(x)) : new Array<FileDto>();
        this.directories = dto.directories ? dto.directories.map(x => new DirectoryDto(x)) : new Array<DirectoryDto>();
        this.parent = dto.parent ? new DirectoryDto(dto.parent) : null;
        this.isRoot = dto.isRoot;
    }
}