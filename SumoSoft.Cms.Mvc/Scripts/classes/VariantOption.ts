﻿import { BaseEntity } from './BaseEntity';
import { IVariantOptionDto } from '../interfaces/IVariantOptionDto';
import { Variant } from './Variant';
import { VariantOptionLocalizedKit } from './VariantOptionLocalizedKit';

export class VariantOption extends BaseEntity implements IVariantOptionDto {
	name: string;
	image: string;
	url: string;
	code: string;
	sortOrder: number;
	tags: string;
	localizedIsDisabled: boolean;
	localizedTitle: string;
	localizedDescription: string;
	variant: Variant;
	variantOptionLocalizedKits = new Array<VariantOptionLocalizedKit>();
	localizedPriceModifier: number;

	getLocalizedIsDisabled(languageCode?: string) {
		return languageCode
			? this.getLocalizedKitByLanguageCode(
				this.variantOptionLocalizedKits,
				languageCode
			).isDisabled
			: this.isDisabled;
	}

	getLocalizedTitle(languageCode?: string) {
		return languageCode
			? this.getLocalizedKitByLanguageCode(
				this.variantOptionLocalizedKits,
				languageCode
			).title
			: this.localizedTitle;
	}

	getLocalizedDescription(languageCode?: string) {
		return languageCode
			? this.getLocalizedKitByLanguageCode(
				this.variantOptionLocalizedKits,
				languageCode
			).description
			: this.localizedDescription;
	}

	getLocalizedPriceModifier(languageCode?: string) {
		return languageCode
			? this.getLocalizedKitByLanguageCode(
				this.variantOptionLocalizedKits,
				languageCode
			).priceModifier || 0
			: this.localizedPriceModifier;
	}

	constructor(dto?: IVariantOptionDto | any) {
		super(dto);

		dto = dto || ({} as IVariantOptionDto);
		this.name = dto.name;
		this.image = dto.image;
		this.url = dto.url;
		this.code = dto.code;
		this.sortOrder = dto.sortOrder;
		this.tags = dto.tags;
		this.localizedIsDisabled = dto.localizedIsDisabled;
		this.localizedTitle = dto.localizedTitle;
		this.localizedDescription = dto.localizedDescription;
		this.variant = dto.variant ? new Variant(dto.variant) : null;
		this.variantOptionLocalizedKits = dto.variantOptionLocalizedKits
			? dto.variantOptionLocalizedKits.map(
				x => new VariantOptionLocalizedKit(x)
			)
			: new Array<VariantOptionLocalizedKit>();
		this.localizedPriceModifier = dto.localizedPriceModifier;
	}
}
