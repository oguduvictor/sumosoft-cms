﻿import { BaseEntity } from "./BaseEntity";
import { IOrderCreditDto } from "../interfaces/IOrderCreditDto";

export class OrderCredit extends BaseEntity implements IOrderCreditDto {
    amount: number;
    reference: string;

    constructor(dto?: IOrderCreditDto | any) {
        super(dto);

        dto = dto || {} as IOrderCreditDto;
        this.amount = dto.amount;
        this.reference = dto.reference;
    }
}