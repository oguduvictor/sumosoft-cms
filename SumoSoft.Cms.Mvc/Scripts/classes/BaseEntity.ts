﻿import { IBaseEntityDto } from "../interfaces/IBaseEntityDto";
import { ILocalizedKitDto } from "../interfaces/ILocalizedKitDto";
import { Tax } from "./Tax";

export class BaseEntity implements IBaseEntityDto {
    id: string;
    isDisabled: boolean;
    createdDate: string;
    modifiedDate: string;
    deletedDate: string;
    isNew: boolean;
    isModified: boolean;
    isDeleted: boolean;

    getLocalizedKitByLanguageCode<T extends ILocalizedKitDto>(localizedKits: Array<T>, languageCode: string): T {
        return localizedKits.filter(x => x.country.languageCode === languageCode)[0] || {} as T;
    }

    addTaxes(totalBeforeTax: number, taxes: Array<Tax>) {
        var totalTax = 0;
        taxes.forEach(tax => {
            if (tax.amount > 0) {
                totalTax = totalTax + tax.amount;
            } else {
                totalTax = totalTax + totalBeforeTax * tax.percentage / 100;
            }
        });
        return totalBeforeTax + totalTax;
    }

    constructor(dto: IBaseEntityDto) {
        dto = dto || {} as IBaseEntityDto;

        this.id = dto.id || newGuid();
        this.isDisabled = dto.isDisabled;
        this.createdDate = dto.createdDate;
        this.modifiedDate = dto.modifiedDate;
        this.deletedDate = dto.deletedDate;
        this.isNew = dto.isNew;
        this.isModified = dto.isModified;
        this.isDeleted = dto.isDeleted;
    }
}

function newGuid(): string {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, c => {
        const r = Math.random() * 16 | 0;
        const v = c === "x" ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}