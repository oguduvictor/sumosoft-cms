﻿import { User } from "./User";
import { BaseEntity } from "./BaseEntity";
import { ILogDto } from "../interfaces/ILogDto";

export class Log extends BaseEntity implements ILogDto {
    message: string;
    details: string;
    user: User;

    constructor(dto?: ILogDto | any) {
        super(dto);

        dto = dto || {} as ILogDto;
        this.createdDate = dto.createdDate;
        this.details = dto.details;
        this.message = dto.message;
        this.user = dto.user ? new User(dto.user) : null;
    }
}