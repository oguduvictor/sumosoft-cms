﻿import { BaseEntity } from "./BaseEntity";
import { IProductVariantDto } from "../interfaces/IProductVariantDto";
import { Product } from "./Product";
import { VariantOption } from "./VariantOption";
import { Variant } from "./Variant";

export class ProductVariant extends BaseEntity implements IProductVariantDto {
    defaultBooleanValue: boolean;
    defaultDoubleValue: number;
    defaultIntegerValue: number;
    defaultStringValue: string;
    defaultJsonValue: string;
    product: Product;
    variant: Variant;
    defaultVariantOptionValue: VariantOption;
    variantOptions = new Array<VariantOption>();

    getVariantOptionByName(variantOptionName: string) {
        return this.variantOptions.filter(x => x.name === variantOptionName)[0];
    }

    getVariantOptionByUrl(variantOptionUrl: string) {
        return this.variantOptions.filter(x => x.url === variantOptionUrl)[0];
    }

    constructor(dto?: IProductVariantDto | any) {
        super(dto);

        dto = dto || {} as IProductVariantDto;
        this.defaultBooleanValue = dto.defaultBooleanValue;
        this.defaultDoubleValue = dto.defaultDoubleValue;
        this.defaultIntegerValue = dto.defaultIntegerValue;
        this.defaultStringValue = dto.defaultStringValue;
        this.defaultJsonValue = dto.defaultJsonValue;
        this.product = dto.product ? new Product(dto.product) : null;
        this.variant = dto.variant ? new Variant(dto.variant) : null;
        this.defaultVariantOptionValue = dto.defaultVariantOptionValue ? new VariantOption(dto.defaultVariantOptionValue) : null;
        this.variantOptions = dto.variantOptions ? dto.variantOptions.map(x => new VariantOption(x)) : new Array<VariantOption>();
    }
}