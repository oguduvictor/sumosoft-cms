import { BaseEntity } from "./BaseEntity";
import { Attribute } from "./Attribute";
import { AttributeOption } from "./AttributeOption";
import { ContentSection } from "./ContentSection";
import { User } from "./User";
import { IUserAttributeDto } from "../interfaces/IUserAttributeDto";

export class UserAttribute extends BaseEntity implements IUserAttributeDto {
    user: User;
    attribute: Attribute;
    stringValue: string;
    booleanValue: boolean;
    dateTimeValue: string;
    imageValue: string;
    doubleValue: number;
    attributeOptionValue: AttributeOption;
    contentSectionValue: ContentSection;
    jsonValue: string;

    constructor(dto?: IUserAttributeDto | any) {
        super(dto);

        dto = dto || ({} as IUserAttributeDto);
        this.user = dto.user ? new User(dto.user) : null;
        this.attribute = dto.attribute ? new Attribute(dto.attribute) : null;
        this.stringValue = dto.stringValue;
        this.booleanValue = dto.booleanValue;
        this.dateTimeValue = dto.dateTimeValue;
        this.imageValue = dto.imageValue;
        this.doubleValue = dto.doubleValue;
        this.attributeOptionValue = dto.attributeOptionValue ? new AttributeOption(dto.attributeOptionValue) : null;
        this.contentSectionValue = dto.contentSectionValue ? new ContentSection(dto.contentSectionValue) : null;
        this.jsonValue = dto.jsonValue;
    }
}
