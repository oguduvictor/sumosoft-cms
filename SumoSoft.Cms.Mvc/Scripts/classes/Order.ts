﻿import { IOrderDto } from "../interfaces/IOrderDto";
import { OrderCoupon } from "./OrderCoupon";
import { OrderCredit } from "./OrderCredit";
import { SentEmail } from "./SentEmail";
import { OrderShippingBox } from "./OrderShippingBox";
import { OrderAddress } from "./OrderAddress";
import { BaseEntity } from "./BaseEntity";
import { OrderStatusEnum } from "../enums/OrderStatusEnum";

export class Order extends BaseEntity implements IOrderDto {
    orderNumber: number;
    status: OrderStatusEnum;
    tags: string;
    statusDescriptions: Array<string>;
    countryName: string;
    countryLanguageCode: string;
    currencySymbol: string;
    iso4217CurrencySymbol: string;
    userId: string;
    userFirstName: string;
    userLastName: string;
    userEmail: string;
    transactionId: string;
    orderShippingBoxes = new Array<OrderShippingBox>();
    shippingAddress: OrderAddress;
    billingAddress: OrderAddress;
    comments: string;
    sentEmails: Array<SentEmail>;
    orderCoupon: OrderCoupon;
    orderCredit: OrderCredit;
    totalBeforeTax: number;
    totalAfterTax: number;
    totalToBePaid: number;
    totalPaid: number;

    constructor(dto?: IOrderDto | any) {
        super(dto);

        dto = dto || {} as IOrderDto;
        this.createdDate = dto.createdDate;
        this.orderNumber = dto.orderNumber;
        this.status = dto.status;
        this.statusDescriptions = dto.statusDescriptions || new Array<string>();
        this.countryName = dto.countryName;
        this.countryLanguageCode = dto.countryLanguageCode;
        this.currencySymbol = dto.currencySymbol;
        this.iso4217CurrencySymbol = dto.iso4217CurrencySymbol;
        this.userId = dto.userId;
        this.userFirstName = dto.userFirstName;
        this.userLastName = dto.userLastName;
        this.userEmail = dto.userEmail;
        this.transactionId = dto.transactionId;
        this.orderShippingBoxes = dto.orderShippingBoxes
            ? dto.orderShippingBoxes.map(x => new OrderShippingBox(x))
            : new Array<OrderShippingBox>();
        this.shippingAddress = dto.shippingAddress ? new OrderAddress(dto.shippingAddress) : null;
        this.billingAddress = dto.billingAddress ? new OrderAddress(dto.billingAddress) : null;
        this.comments = dto.comments;
        this.sentEmails = dto.sentEmails ? dto.sentEmails.map(x => new SentEmail(x)) : new Array<SentEmail>();
        this.orderCoupon = dto.orderCoupon ? new OrderCoupon(dto.orderCoupon) : null;
        this.orderCredit = dto.orderCredit ? new OrderCredit(dto.orderCredit) : null;
        this.totalBeforeTax = dto.totalBeforeTax;
        this.totalAfterTax = dto.totalAfterTax;
        this.totalToBePaid = dto.totalToBePaid;
        this.totalPaid = dto.totalPaid;
    }
}