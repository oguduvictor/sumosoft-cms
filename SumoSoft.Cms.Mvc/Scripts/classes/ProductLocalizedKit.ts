﻿import { IProductLocalizedKitDto } from "../interfaces/IProductLocalizedKitDto";
import { Product } from "./Product";
import { Country } from "./Country";
import { BaseEntity } from "./BaseEntity";
import { ILocalizedKitDto } from "../interfaces/ILocalizedKitDto";

export class ProductLocalizedKit extends BaseEntity implements IProductLocalizedKitDto {
    title: string;
    description: string;
    metaTitle: string;
    metaDescription: string;
    country: Country;
    product: Product;

    constructor(dto?: IProductLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IProductLocalizedKitDto;
        this.title = dto.title;
        this.description = dto.description;
        this.metaTitle = dto.metaTitle;
        this.metaDescription = dto.metaDescription;
        this.country = dto.country ? new Country(dto.country) : null;
        this.product = dto.product ? new Product(dto.product) : null;
    }
}
