﻿import { Country } from './Country';
import { IUserDto } from '../interfaces/IUserDto';
import { Address } from './Address';
import { Cart } from './Cart';
import { UserLocalizedKit } from './UserLocalizedKit';
import { UserRole } from './UserRole';
import { BaseEntity } from './BaseEntity';
import { GenderEnum } from '../enums/GenderEnum';
import { UserAttribute } from './UserAttribute';
import { UserVariant } from './UserVariant';
import { MailingListSubscription } from './mailingListSubscription';

export class User extends BaseEntity implements IUserDto {
	firstName: string;
	lastName: string;
	fullName: string;
	createdDate: string;
	email: string;
	password: string;
	role: UserRole;
	lastLogin: string;
	addresses: Array<Address>;
	gender: GenderEnum;
	genderDescriptions: Array<string>;
	loginProvider: string;
	isRegistered: boolean;
	showMembershipSalePrice: boolean;
	cart: Cart;
	country: Country;
	userAttributes: Array<UserAttribute>;
	userLocalizedKits: Array<UserLocalizedKit>;
	userVariants: Array<UserVariant>;
	mailingListSubscriptions: Array<MailingListSubscription>;

	getUserAttributeByName(attributeName: string) {
		return this.userAttributes.filter(
			x => x.attribute && x.attribute.name === attributeName
		)[0];
	}

	getUserAttributeByUrl(attributeUrl: string) {
		return this.userAttributes.filter(
			x => x.attribute && x.attribute.url === attributeUrl
		)[0];
	}

	constructor(dto?: IUserDto | any) {
		super(dto);

		dto = dto || ({} as IUserDto);

		this.firstName = dto.firstName;
		this.lastName = dto.lastName;
		this.fullName = dto.fullName;
		this.email = dto.email;
		this.password = dto.password;
		this.gender = dto.gender;
		this.genderDescriptions = dto.genderDescriptions || new Array<string>();
		this.loginProvider = dto.loginProvider;
		this.isRegistered = dto.isRegistered;
		this.showMembershipSalePrice = dto.showMembershipSalePrice;
		this.role = dto.role ? new UserRole(dto.role) : null;
		this.lastLogin = dto.lastLogin;
		this.cart = dto.cart ? new Cart(dto.cart) : null;
		this.country = dto.country ? new Country(dto.country) : null;
		this.mailingListSubscriptions = dto.mailingListSubscriptions
			? dto.mailingListSubscriptions.map(
					x => new MailingListSubscription(x)
			  )
			: new Array<MailingListSubscription>();
		this.userAttributes = dto.userAttributes
			? dto.userAttributes.map(x => new UserAttribute(x))
			: new Array<UserAttribute>();
		this.userLocalizedKits = dto.userLocalizedKits
			? dto.userLocalizedKits.map(x => new UserLocalizedKit(x))
			: new Array<UserLocalizedKit>();
		this.addresses = dto.addresses
			? dto.addresses.map(x => new Address(x))
			: new Array<Address>();
		this.userVariants = dto.userVariants
			? dto.userVariants.map(x => new UserVariant(x))
			: new Array<UserVariant>();
	}
}
