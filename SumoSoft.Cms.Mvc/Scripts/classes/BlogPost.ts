﻿import { IBlogPostDto } from "../interfaces/IBlogPostDto";
import { BaseEntity } from "./BaseEntity";
import { BlogComment } from "./BlogComment";
import { User } from "./User";
import { BlogCategory } from "./BlogCategory";

export class BlogPost extends BaseEntity implements IBlogPostDto {
    title: string;
    content: string;
    isPublished: boolean;
    publicationDate: string;
    author: User;
    featuredImage: string;
    featuredImageHorizontalCrop: string;
    featuredImageVerticalCrop: string;
    url: string;
    metaTitle: string;
    metaDescription: string;
    excerpt: string;
    contentPreview: string;
    blogCategories: Array<BlogCategory>;
    blogComments: Array<BlogComment>;

    constructor(dto?: IBlogPostDto | any) {
        super(dto);

        dto = dto || {} as IBlogPostDto;
        this.title = dto.title;
        this.content = dto.content;
        this.isPublished = dto.isPublished;
        this.publicationDate = dto.publicationDate;
        this.author = dto.author ? new User(dto.author) : null;
        this.featuredImage = dto.featuredImage;
        this.featuredImageHorizontalCrop = dto.featuredImageHorizontalCrop;
        this.featuredImageVerticalCrop = dto.featuredImageVerticalCrop;
        this.url = dto.url;
        this.metaTitle = dto.metaTitle;
        this.metaDescription = dto.metaDescription;
        this.excerpt = dto.excerpt;
        this.contentPreview = dto.contentPreview;
        this.blogCategories = dto.blogCategories ? dto.blogCategories.map(x => new BlogCategory(x)) : new Array<BlogCategory>();
        this.blogComments = dto.blogComments ? dto.blogComments.map(x => new BlogComment(x)) : new Array<BlogComment>();
    }
}
