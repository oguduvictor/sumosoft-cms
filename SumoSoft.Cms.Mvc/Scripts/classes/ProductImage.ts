﻿import { BaseEntity } from "./BaseEntity";
import { IProductImageDto } from "../interfaces/IProductImageDto";
import { ProductImageKit } from "./ProductImageKit";

export class ProductImage extends BaseEntity implements IProductImageDto {
    name: string;
    url: string;
    altText: string;
    sortOrder: number;
    productImageKit: ProductImageKit;

    constructor(dto?: IProductImageDto | any) {
        super(dto);

        dto = dto || {} as IProductImageDto;
        this.name = dto.name;
        this.url = dto.url;
        this.altText = dto.altText;
        this.sortOrder = dto.sortOrder;
        this.productImageKit = dto.productImageKit ? new ProductImageKit(dto.productImageKit) : null;
    }
}
