﻿import { IShippingBoxDto } from "../interfaces/IShippingBoxDto";
import { Product } from "./Product";
import { ShippingBoxLocalizedKit } from "./ShippingBoxLocalizedKit";
import { BaseEntity } from "./BaseEntity";
import { Country } from "./Country";
import { Tax } from "./Tax";

export class ShippingBox extends BaseEntity implements IShippingBoxDto {
    sortOrder: number;
    name: string;
    countries = new Array<Country>();
    products = new Array<Product>();
    shippingBoxLocalizedKits = new Array<ShippingBoxLocalizedKit>();
    requiresShippingAddress: boolean;
    localizedTitle: string;
    localizedDescription: string;
    localizedInternalDescription: string;
    localizedPrice: number;
    localizedMinDays: number;
    localizedMaxDays: number;
    localizedCountWeekends: boolean;
    localizedFreeShippingMinimumPrice: number;
    localizedIsDisabled: boolean;

    getLocalizedTitle(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).title
            : this.localizedTitle;
    }

    getLocalizedDescription(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).description
            : this.localizedDescription;
    }

    getLocalizedInternalDescription(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).internalDescription
            : this.localizedInternalDescription;
    }

    getLocalizedPrice(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).price
            : this.localizedPrice;
    }

    getLocalizedMinDays(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).minDays
            : this.localizedMinDays;
    }

    getLocalizedMaxDays(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).maxDays
            : this.localizedMaxDays;
    }

    getLocalizedCountWeekends(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).countWeekends
            : this.localizedCountWeekends;
    }

    getLocalizedFreeShippingMinimumPrice(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).freeShippingMinimumPrice
            : this.localizedFreeShippingMinimumPrice;
    }

    getLocalizedIsDisabled(languageCode?: string) {
        return languageCode
            ? this.getLocalizedKitByLanguageCode(this.shippingBoxLocalizedKits, languageCode).isDisabled
            : this.isDisabled;
    }

    getShippingPriceBeforeTax(languageCode?: string, allCartItemsTotal = 0) {
        const freeShippingMinimumPrice = this.getLocalizedFreeShippingMinimumPrice(languageCode);
        if (freeShippingMinimumPrice > 0 && allCartItemsTotal > freeShippingMinimumPrice) {
            return 0;
        }
        return this.getLocalizedPrice();
    }

    getShippingPriceAfterTax(shippingCountryTaxes: Array<Tax>, languageCode?: string, allCartItemsTotal = 0) {
        const shippingPriceBeforeTax = this.getShippingPriceBeforeTax(languageCode, allCartItemsTotal);
        const shippingTaxes = shippingCountryTaxes.filter(x => x.applyToShippingPrice);
        return this.addTaxes(shippingPriceBeforeTax, shippingTaxes);
    }

    constructor(dto?: IShippingBoxDto | any) {
        super(dto);

        dto = dto || {} as IShippingBoxDto;
        this.name = dto.name;
        this.requiresShippingAddress = dto.requiresShippingAddress;
        this.countries = dto.countries ? dto.countries.map(x => new Country(x)) : new Array<Country>();
        this.localizedTitle = dto.localizedTitle;
        this.localizedDescription = dto.localizedDescription;
        this.localizedInternalDescription = dto.localizedInternalDescription;
        this.localizedMinDays = dto.localizedMinDays;
        this.localizedMaxDays = dto.localizedMaxDays;
        this.localizedCountWeekends = dto.localizedCountWeekends;
        this.localizedPrice = dto.localizedPrice;
        this.localizedFreeShippingMinimumPrice = dto.localizedFreeShippingMinimumPrice;
        this.localizedIsDisabled = dto.localizedIsDisabled;
        this.sortOrder = dto.sortOrder;
        this.products = dto.products ? dto.products.map(x => new Product(x)) : new Array<Product>();
        this.shippingBoxLocalizedKits = dto.shippingBoxLocalizedKits ? dto.shippingBoxLocalizedKits.map(x => new ShippingBoxLocalizedKit(x)) : new Array<ShippingBoxLocalizedKit>();
    }
}