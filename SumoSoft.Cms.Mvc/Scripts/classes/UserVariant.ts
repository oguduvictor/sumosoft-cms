﻿import { BaseEntity } from "./BaseEntity";
import { Variant } from "./Variant";
import { VariantOption } from "./VariantOption";
import { User } from './User';
import { IUserVariantDto } from '../interfaces/IUserVariantDto';

export class UserVariant extends BaseEntity implements IUserVariantDto {
    user: User;
    variant: Variant;
    booleanValue: boolean;
    doubleValue: number;
    integerValue: number;
    stringValue: string;
    jsonValue: string;
    variantOptionValue: VariantOption;

    constructor(dto?: IUserVariantDto | any) {
        super(dto);
        dto = dto || {} as IUserVariantDto;

        this.user = dto.user ? new User(dto.cartItem) : null;
        this.variant = dto.variant ? new Variant(dto.variant) : null;
        this.booleanValue = dto.booleanValue;
        this.doubleValue = dto.doubleValue;
        this.integerValue = dto.integerValue;
        this.stringValue = dto.stringValue;
        this.jsonValue = dto.jsonValue;
        this.variantOptionValue = dto.variantOptionValue ? new VariantOption(dto.variantOptionValue) : null;
    }
}