﻿import { BaseEntity } from "./BaseEntity";
import { IEmailLocalizedKitDto } from "../interfaces/IEmailLocalizedKitDto";
import { Email } from "./Email";
import { Country } from "./Country";

export class EmailLocalizedKit extends BaseEntity implements IEmailLocalizedKitDto {
    email: Email;
    country: Country;
    from: string;
    displayName: string;
    replyTo: string;

    constructor(dto?: IEmailLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IEmailLocalizedKitDto;
        this.email = dto.email ? new Email(dto.email) : null;
        this.country = dto.country ? new Country(dto.country) : null;
        this.from = dto.from;
        this.displayName = dto.displayName;
        this.replyTo = dto.replyTo;
    }
}