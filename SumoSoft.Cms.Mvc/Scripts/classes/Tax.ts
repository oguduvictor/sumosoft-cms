﻿import { Country } from "./Country";
import { CartItem } from "./CartItem";
import { BaseEntity } from "./BaseEntity";
import { ITaxDto } from "../interfaces/ITaxDto";

export class Tax extends BaseEntity implements ITaxDto {
    country: Country;
    cartItem: CartItem;
    name: string;
    code: string;
    percentage: number;
    applyToShippingPrice: boolean;
    applyToProductPrice: boolean;
    amount?: number;

    constructor(dto?: ITaxDto | any) {
        super(dto);

        dto = dto || {} as ITaxDto;
        this.country = dto.country ? new Country(dto.country) : null;
        this.cartItem = dto.cartItem ? new CartItem(dto.cartItem) : null;
        this.name = dto.name;
        this.code = dto.code;
        this.percentage = dto.percentage;
        this.applyToShippingPrice = dto.applyToShippingPrice;
        this.applyToProductPrice = dto.applyToProductPrice;
        this.amount = dto.amount;
    }
}