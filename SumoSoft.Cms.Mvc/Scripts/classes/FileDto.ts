﻿import { BaseEntity } from "./BaseEntity";
import { IFileDto } from "../interfaces/IFileDto";
import { DirectoryDto } from "./DirectoryDto";

export class FileDto extends BaseEntity implements IFileDto {
    name: string;
    extension: string;
    localPath: string;
    localPathWithVersion: string;
    absolutePath: string;
    absolutePathWithVersion: string;
    size: number;
    heavyLoadingFileSize: number;
    creationTime: string;
    useAzureStorage: boolean;
    directory: DirectoryDto;

    constructor(dto?: IFileDto | any) {
        super(dto);

        dto = dto || {} as IFileDto;
        this.name = dto.name;
        this.extension = dto.extension;
        this.localPath = dto.localPath;
        this.localPathWithVersion = dto.localPathWithVersion;
        this.absolutePath = dto.absolutePath;
        this.absolutePathWithVersion = dto.absolutePathWithVersion;
        this.size = dto.size;
        this.heavyLoadingFileSize = dto.heavyLoadingFileSize;
        this.creationTime = dto.creationTime;
        this.useAzureStorage = dto.useAzureStorage;
        this.directory = dto.directory ? new DirectoryDto(dto.directory) : null;
    }
}