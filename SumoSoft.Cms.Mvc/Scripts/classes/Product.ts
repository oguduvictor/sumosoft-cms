﻿import { BaseEntity } from "./BaseEntity";
import { IProductDto } from "../interfaces/IProductDto";
import { ProductVariant } from "./ProductVariant";
import { Category } from "./Category";
import { ProductStockUnit } from "./ProductStockUnit";
import { ProductLocalizedKit } from "./ProductLocalizedKit";
import { ShippingBox } from "./ShippingBox";
import { ProductAttribute } from "./ProductAttribute";
import { ProductImageKit } from "./ProductImageKit";
import { VariantOption } from "./VariantOption";
import { VariantTypesEnum } from "../enums/VariantTypesEnum";

export class Product extends BaseEntity implements IProductDto {
    name: string;
    url: string;
    code: string;
    taxCode: string;
    comments: string;
    tags: string;
    categories = new Array<Category>();
    productVariants = new Array<ProductVariant>();
    productAttributes = new Array<ProductAttribute>();
    productStockUnits = new Array<ProductStockUnit>();
    productImageKits = new Array<ProductImageKit>();
    shippingBoxes = new Array<ShippingBox>();
    productLocalizedKits = new Array<ProductLocalizedKit>();
    relatedProducts = new Array<Product>();
    localizedTitle: string;
    localizedDescription: string;
    localizedMetaTitle: string;
    localizedMetaDescription: string;
    totalBeforeTaxWithoutSalePrice: number;
    totalAfterTaxWithoutSalePrice: number;
    totalBeforeTax: number;
    totalAfterTax: number;

    hasCategories(categories: Category[]) {
        return categories.every(x => this.categories.some(y => y.id === x.id));
    }

    getLocalizedTitle(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.productLocalizedKits, languageCode).title;
    }

    getLocalizedDescription(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.productLocalizedKits, languageCode).description;
    }

    getLocalizedMetaTitle(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.productLocalizedKits, languageCode).metaTitle;
    }

    getLocalizedMetaDescription(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.productLocalizedKits, languageCode).metaDescription;
    }

    getProductAttributeByName(attributeName: string, nullSafe = false) {
        var productAttribute = this.productAttributes.find(x => x.attribute && x.attribute.name === attributeName);
        return nullSafe ? productAttribute || new ProductAttribute() : productAttribute;
    }

    getProductAttributeByUrl(attributeUrl: string, nullSafe = false) {
        var productAttribute = this.productAttributes.find(x => x.attribute && x.attribute.url === attributeUrl);
        return nullSafe ? productAttribute || new ProductAttribute() : productAttribute;
    }

    getProductVariantByName(variantName: string, nullSafe = false) {
        var productVariant = this.productVariants.find(x => x.variant && x.variant.name === variantName);
        return nullSafe ? productVariant || new ProductVariant() : productVariant;
    }

    getProductVariantByUrl(variantUrl: string, nullSafe = false) {
        var productVariant = this.productVariants.find(x => x.variant && x.variant.url === variantUrl);
        return nullSafe ? productVariant || new ProductVariant() : productVariant;
    }

    getDefaultVariantOptions() {
        return this.productVariants
            .filter(x => x.variant.type === VariantTypesEnum.Options)
            .map(x => x.defaultVariantOptionValue || x.variant.defaultVariantOptionValue)
            .filter(x => x);
    }

    getProductImageKit(variantOptions?: Array<VariantOption>) {
        variantOptions = variantOptions || this.getDefaultVariantOptions();
        return this.productImageKits.filter(x => x.matches(variantOptions))[0];
    }

    getProductStockUnit(variantOptions?: Array<VariantOption>) {
        variantOptions = variantOptions || this.getDefaultVariantOptions();
        return this.productStockUnits.filter(x => x.matches(variantOptions))[0];
    }

    getStock(variantOptions: Array<VariantOption>) {
        return this.getProductStockUnit(variantOptions) ? this.getProductStockUnit(variantOptions).stock : 0;
    }

    constructor(dto?: IProductDto | any) {
        super(dto);

        dto = dto || {} as IProductDto;
        this.name = dto.name;
        this.url = dto.url;
        this.code = dto.code;
        this.taxCode = dto.taxCode;
        this.comments = dto.comments;
        this.tags = dto.tags;
        this.categories = dto.categories ? dto.categories.map(x => new Category(x)) : new Array<Category>();
        this.productAttributes = dto.productAttributes ? dto.productAttributes.map(x => new ProductAttribute(x)) : new Array<ProductAttribute>();
        this.productVariants = dto.productVariants ? dto.productVariants.map(x => new ProductVariant(x)) : new Array<ProductVariant>();
        this.productStockUnits = dto.productStockUnits ? dto.productStockUnits.map(x => new ProductStockUnit(x)) : new Array<ProductStockUnit>();
        this.productImageKits = dto.productImageKits ? dto.productImageKits.map(x => new ProductImageKit(x)) : new Array<ProductImageKit>();
        this.shippingBoxes = dto.shippingBoxes ? dto.shippingBoxes.map(x => new ShippingBox(x)) : new Array<ShippingBox>();
        this.productLocalizedKits = dto.productLocalizedKits ? dto.productLocalizedKits.map(x => new ProductLocalizedKit(x)) : new Array<ProductLocalizedKit>();
        this.relatedProducts = dto.relatedProducts ? dto.relatedProducts.map(x => new Product(x)) : new Array<Product>();
        this.localizedTitle = dto.localizedTitle;
        this.localizedDescription = dto.localizedDescription;
        this.localizedMetaTitle = dto.localizedMetaTitle;
        this.localizedMetaDescription = dto.localizedMetaDescription;
        this.totalBeforeTaxWithoutSalePrice = dto.totalBeforeTaxWithoutSalePrice;
        this.totalAfterTaxWithoutSalePrice = dto.totalAfterTaxWithoutSalePrice;
        this.totalBeforeTax = dto.totalBeforeTax;
        this.totalAfterTax = dto.totalAfterTax;
    }
}
