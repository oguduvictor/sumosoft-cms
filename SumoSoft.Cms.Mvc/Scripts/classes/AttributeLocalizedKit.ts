﻿import { BaseEntity } from "./BaseEntity";
import { Country } from "./Country";
import { Attribute } from "./Attribute";
import { IAttributeLocalizedKitDto } from "../interfaces/IAttributeLocalizedKitDto";

export class AttributeLocalizedKit extends BaseEntity implements IAttributeLocalizedKitDto {
    country: Country;
    attribute: Attribute;
    title: string;
    description: string;

    constructor(dto?: IAttributeLocalizedKitDto | any) {
        super(dto);

        dto = dto || ({} as IAttributeLocalizedKitDto);
        this.country = dto.country ? new Country(dto.country) : null;
        this.attribute = dto.attribute ? new Attribute(dto.attribute) : null;
        this.title = dto.title;
        this.description = dto.description;
    }
}
