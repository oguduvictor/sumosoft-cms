﻿import { BaseEntity } from './BaseEntity';
import { Country } from './Country';
import { User } from './User';
import { ICouponDto } from '../interfaces/ICouponDto';
import { Category } from './Category';

export class Coupon extends BaseEntity implements ICouponDto {
	code: string;
	amount: number;
	percentage: number;
	country: Country;
	published: boolean;
	expirationDate: string;
	validityTimes: number;
	categories: Array<Category>;
	referee: User;
	refereeReward: number;
	recipient: string;
	note: string;
	allowOnSale: boolean;

	constructor(dto?: ICouponDto | any) {
		super(dto);

		dto = dto || ({} as ICouponDto);
		this.code = dto.code;
		this.amount = dto.amount;
		this.percentage = dto.percentage;
		this.country = dto.country ? new Country(dto.country) : null;
		this.published = dto.published;
		this.expirationDate = dto.expirationDate;
		this.validityTimes = dto.validityTimes;
		this.referee = dto.referee ? new User(dto.referee) : null;
		this.categories = dto.categories
			? dto.categories.map(x => new Category(x))
			: new Array<Category>();
		this.refereeReward = dto.refereeReward;
		this.recipient = dto.recipient;
		this.note = dto.note;
		this.allowOnSale = dto.allowOnSale;
	}
}
