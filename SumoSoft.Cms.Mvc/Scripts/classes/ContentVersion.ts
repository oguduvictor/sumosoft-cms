﻿import { BaseEntity } from "./BaseEntity";
import { ContentSectionLocalizedKit } from "./ContentSectionLocalizedKit";
import { IContentVersionDto } from "../interfaces/IContentVersionDto";

export class ContentVersion extends BaseEntity implements IContentVersionDto {
    
    content : string;
    contentSectionLocalizedKit: ContentSectionLocalizedKit;
   
    constructor(dto?: IContentVersionDto | any) {
       
        super(dto);

        dto = dto || {} as IContentVersionDto;
        this.content = dto.content;
        this.contentSectionLocalizedKit = dto.contentSectionLocalizedKit ? new ContentSectionLocalizedKit(dto.contentSectionLocalizedKit) : null;
    }
}
