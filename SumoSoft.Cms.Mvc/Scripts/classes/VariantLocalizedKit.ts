﻿import { BaseEntity } from "./BaseEntity";
import { IVariantLocalizedKitDto } from "../interfaces/IVariantLocalizedKitDto";
import { Country } from "./Country";
import { Variant } from "./Variant";

export class VariantLocalizedKit extends BaseEntity implements IVariantLocalizedKitDto {
    title: string;
    description: string;
    country: Country;
    variant: Variant;

    constructor(dto?: IVariantLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IVariantLocalizedKitDto;
        this.title = dto.title;
        this.description = dto.description;
        this.country = dto.country ? new Country(dto.country) : null;
        this.variant = dto.variant ? new Variant(dto.variant) : null;
    }
}
