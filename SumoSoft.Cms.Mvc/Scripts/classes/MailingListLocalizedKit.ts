﻿import { BaseEntity } from "./BaseEntity";
import { IMailingListLocalizedKitDto } from '../interfaces/IMailingListLocalizedKitDto';
import { MailingList } from './MailingList';
import { Country } from './Country';

export class MailingListLocalizedKit extends BaseEntity implements IMailingListLocalizedKitDto {
    mailingList: MailingList;
    country: Country;
    title: string;
    description: string;

    constructor(dto?: IMailingListLocalizedKitDto | any) {
        super(dto);

        dto = dto || ({} as IMailingListLocalizedKitDto);
        this.title = dto.title;
        this.description = dto.description;
        this.mailingList = dto.mailingList ? new MailingList(dto.mailingList) : null;
        this.country = dto.country ? new Country(dto.country) : null;
    }
}
