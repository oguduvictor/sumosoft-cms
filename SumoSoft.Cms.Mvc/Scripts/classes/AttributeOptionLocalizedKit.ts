﻿import { Country } from "./Country";
import { BaseEntity } from "./BaseEntity";
import { AttributeOption } from "./AttributeOption";
import { IAttributeOptionLocalizedKitDto } from "../interfaces/IAttributeOptionLocalizedKitDto";

export class AttributeOptionLocalizedKit extends BaseEntity implements IAttributeOptionLocalizedKitDto {
    country: Country;
    title: string;
    attributeOption: AttributeOption;
    description: string;

    constructor(dto?: IAttributeOptionLocalizedKitDto | any) {
        super(dto);

        dto = dto || ({} as IAttributeOptionLocalizedKitDto);
        this.country = dto.country ? new Country(dto.country) : null;
        this.title = dto.title;
        this.attributeOption = dto.attributeOption ? new AttributeOption(dto.attributeOption) : null;
        this.description = dto.description;
    }
}
