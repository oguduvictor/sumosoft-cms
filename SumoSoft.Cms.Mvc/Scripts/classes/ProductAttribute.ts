﻿import { BaseEntity } from "./BaseEntity";
import { Product } from "./Product";
import { IProductAttributeDto } from "../interfaces/IProductAttributeDto";
import { Attribute } from "./Attribute";
import { AttributeOption } from "./AttributeOption";
import { ContentSection } from "./ContentSection";

export class ProductAttribute extends BaseEntity implements IProductAttributeDto {
    product: Product;
    attribute: Attribute;
    stringValue: string;
    booleanValue: boolean;
    dateTimeValue: string;
    imageValue: string;
    doubleValue: number;
    attributeOptionValue: AttributeOption;
    contentSectionValue: ContentSection;
    jsonValue: string;

    constructor(dto?: IProductAttributeDto | any) {
        super(dto);

        dto = dto || ({} as IProductAttributeDto);
        this.product = dto.product ? new Product(dto.product) : null;
        this.attribute = dto.attribute ? new Attribute(dto.attribute) : null;
        this.stringValue = dto.stringValue;
        this.booleanValue = dto.booleanValue;
        this.dateTimeValue = dto.dateTimeValue;
        this.imageValue = dto.imageValue;
        this.doubleValue = dto.doubleValue;
        this.attributeOptionValue = dto.attributeOptionValue ? new AttributeOption(dto.attributeOptionValue) : null;
        this.contentSectionValue = dto.contentSectionValue ? new ContentSection(dto.contentSectionValue) : null;
        this.jsonValue = dto.jsonValue;
    }
}
