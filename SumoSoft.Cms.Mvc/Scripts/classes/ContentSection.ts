﻿import { BaseEntity } from './BaseEntity';
import { IContentSectionDto } from '../interfaces/IContentSectionDto';
import { ProductAttribute } from './ProductAttribute';
import { ContentSectionLocalizedKit } from './ContentSectionLocalizedKit';

export class ContentSection extends BaseEntity implements IContentSectionDto {
    name: string;
    schema: string;
    localizedContent: string;
    productAttributes: Array<ProductAttribute>;
    contentSectionLocalizedKits: Array<ContentSectionLocalizedKit>;

    constructor(dto?: IContentSectionDto | any) {
        super(dto);

        dto = dto || {} as IContentSectionDto;

        this.name = dto.name;
        this.schema = dto.schema;
        this.localizedContent = dto.localizedContent;
        this.productAttributes = dto.productAttributes ? dto.productAttributes.map(x => new ProductAttribute(x)) : new Array<ProductAttribute>();
        this.contentSectionLocalizedKits = dto.contentSectionLocalizedKits ? dto.contentSectionLocalizedKits.map(x => new ContentSectionLocalizedKit(x)) : new Array<ContentSectionLocalizedKit>();
    }
}