﻿import { BaseEntity } from "./BaseEntity";
import { IUserCreditDto } from "../interfaces/IUserCreditDto";
import { UserLocalizedKit } from "./UserLocalizedKit";

export class UserCredit extends BaseEntity implements IUserCreditDto {
    amount: number;
    reference: string;
    userLocalizedKit: UserLocalizedKit;

    constructor(dto?: IUserCreditDto | any) {
        super(dto);

        dto = dto || {} as IUserCreditDto;
        this.amount = dto.amount;
        this.reference = dto.reference;
        this.userLocalizedKit = dto.userLocalizedKit ? new UserLocalizedKit(dto.userLocalizedKit) : null;

    }
}