﻿import { IMailingListSubscription } from '../interfaces/IMailingListSubscription';
import { User } from './User';
import { MailingList } from './MailingList';
import { MailingListSubscriptionStatusEnum } from '../enums/MailingListSubscriptionStatusEnum';

export class MailingListSubscription implements IMailingListSubscription {
    user: User;
    mailingList: MailingList;
    status: MailingListSubscriptionStatusEnum;
    statusDescriptions: Array<string>;

    constructor(dto?: IMailingListSubscription) {
        dto = dto || ({} as IMailingListSubscription);

        this.status = dto.status;
        this.statusDescriptions = dto.statusDescriptions || new Array<string>();
        this.user = dto.user ? new User(dto.user) : null;
        this.mailingList = dto.mailingList ? new MailingList(dto.mailingList) : null;
    }
}