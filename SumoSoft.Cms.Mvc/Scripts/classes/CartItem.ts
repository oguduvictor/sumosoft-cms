﻿import { BaseEntity } from './BaseEntity';
import { CartShippingBox } from './CartShippingBox';
import { CartItemVariant } from './CartItemVariant';
import { Product } from './Product';
import { ProductAttribute } from './ProductAttribute';
import { ProductVariant } from './ProductVariant';
import { ProductImageKit } from './ProductImageKit';
import { ProductStockUnit } from './ProductStockUnit';
import { VariantOption } from './VariantOption';
import { ICartItemDto } from '../interfaces/ICartItemDto';
import { Tax } from './Tax';

export class CartItem extends BaseEntity implements ICartItemDto {
	cartShippingBox: CartShippingBox;
	product: Product;
	quantity: number;
	image: string;
	cartItemVariants: Array<CartItemVariant>;
	minEta: string;
	maxEta: string;
	couponApplies: boolean;
	subtotalBeforeTax: number;
	subtotalAfterTax: number;
	subtotalBeforeTaxWithoutSalePrice: number;
	subtotalAfterTaxWithoutSalePrice: number;
	totalBeforeTax: number;
	totalAfterTax: number;
	taxesOverride: Array<Tax>;

	getCartItemVariantByName(variantName: string, nullSafe = false) {
		var cartItemVariant = this.cartItemVariants.find(
			x => x.variant && x.variant.name === variantName
		);
		return nullSafe
			? cartItemVariant || new CartItemVariant()
			: cartItemVariant;
	}

	getCartItemVariantByUrl(variantUrl: string, nullSafe = false) {
		var cartItemVariant = this.cartItemVariants.find(
			x => x.variant && x.variant.url === variantUrl
		);
		return nullSafe
			? cartItemVariant || new CartItemVariant()
			: cartItemVariant;
	}

	getProductAttributeByName(
		attributeName: string,
		nullSafe = false
	): ProductAttribute {
		return this.product.getProductAttributeByName(attributeName, nullSafe);
	}

	getProductAttributeByUrl(
		attributeUrl: string,
		nullSafe = false
	): ProductAttribute {
		return this.product.getProductAttributeByUrl(
			attributeUrl,
			nullSafe
		);
	}

	getProductVariantByName(
		variantName: string,
		nullSafe = false
	): ProductVariant {
		return this.product.getProductVariantByName(variantName, nullSafe);
	}

	getProductVariantByUrl(
		variantUrl: string,
		nullSafe = false
	): ProductVariant {
		return this.product.getProductVariantByUrl(
			variantUrl,
			nullSafe
		);
	}

	getSelectedVariantOptions(): Array<VariantOption> {
		return this.cartItemVariants
			.filter(x => x.variantOptionValue != null)
			.map(x => x.variantOptionValue);
	}

	getProductImageKit(
		selectedVariantOptions?: Array<VariantOption>
	): ProductImageKit {
		selectedVariantOptions =
			selectedVariantOptions && selectedVariantOptions.length >= 1
				? selectedVariantOptions
				: this.getSelectedVariantOptions();
		return this.product.getProductImageKit(selectedVariantOptions);
	}

	getProductStockUnit(
		selectedVariantOptions?: Array<VariantOption>
	): ProductStockUnit {
		selectedVariantOptions =
			selectedVariantOptions && selectedVariantOptions.length >= 1
				? selectedVariantOptions
				: this.getSelectedVariantOptions();
		return (
			this.product &&
			this.product.getProductStockUnit(selectedVariantOptions)
		);
	}

	getStock(selectedVariantOptions?: Array<VariantOption>) {
		selectedVariantOptions =
			selectedVariantOptions && selectedVariantOptions.length >= 1
				? selectedVariantOptions
				: this.getSelectedVariantOptions();
		return this.getProductStockUnit(selectedVariantOptions)
			? this.getProductStockUnit(selectedVariantOptions).stock
			: 0;
	}

	getTaxes(shippingCountryTaxes: Array<Tax>) {
		const taxes = this.taxesOverride.length
			? this.taxesOverride
			: shippingCountryTaxes;
		const productTaxes =
			taxes.filter(x => x.applyToProductPrice) ||
			new Array<Tax>();
		return productTaxes;
	}

	constructor(dto?: ICartItemDto | any) {
		super(dto);

		dto = dto || ({} as ICartItemDto);
		this.cartShippingBox = dto.cartShippingBox
			? new CartShippingBox(dto.cartShippingBox)
			: null;
		this.product = dto.product ? new Product(dto.product) : null;
		this.quantity = dto.quantity;
		this.image = dto.image;
		this.cartItemVariants = dto.cartItemVariants
			? dto.cartItemVariants.map(x => new CartItemVariant(x))
			: new Array<CartItemVariant>();
		this.minEta = dto.minEta;
		this.maxEta = dto.maxEta;
		this.couponApplies = dto.couponApplies;
		this.subtotalBeforeTax = dto.subtotalBeforeTax;
		this.subtotalAfterTax = dto.subtotalAfterTax;
		this.subtotalBeforeTaxWithoutSalePrice =
			dto.subtotalBeforeTaxWithoutSalePrice;
		this.subtotalAfterTaxWithoutSalePrice =
			dto.subtotalAfterTaxWithoutSalePrice;
		this.totalBeforeTax = dto.totalBeforeTax;
		this.totalAfterTax = dto.totalAfterTax;
		this.taxesOverride = dto.taxesOverride
			? dto.taxesOverride.map(x => new Tax(x))
			: new Array<Tax>();
	}
}
