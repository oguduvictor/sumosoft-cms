﻿import { BaseEntity } from "./BaseEntity"
import { ContentSection } from "./ContentSection"
import { Country } from "./Country";
import { IContentSectionLocalizedKitDto } from "../interfaces/IContentSectionLocalizedKitDto";
import { ContentVersion } from "./ContentVersion";

export class ContentSectionLocalizedKit extends BaseEntity implements IContentSectionLocalizedKitDto {
    country: Country;
    content: string;
    contentSection: ContentSection;
    contentVersions: Array<ContentVersion>;

    constructor(dto?: IContentSectionLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as IContentSectionLocalizedKitDto;
        this.country = new Country(dto.country);
        this.content = dto.content;
        this.contentSection = new ContentSection(dto.contentSection);
        this.contentVersions = dto.contentVersions ? dto.contentVersions.map(x => new ContentVersion(x)) : new Array<ContentVersion>();
    }
}