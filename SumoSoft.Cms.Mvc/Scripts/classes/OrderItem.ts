﻿import { OrderItemStockStatusEnum } from "../enums/OrderItemStockStatusEnum";
import { OrderShippingBox } from "./OrderShippingBox";
import { OrderCoupon } from "./OrderCoupon";
import { OrderItemVariant } from "./OrderItemVariant";
import { IOrderItemDto } from "../interfaces/IOrderItemDto";
import { BaseEntity } from "./BaseEntity";
import { OrderItemTax } from "./OrderItemTax";
import { OrderItemStatusEnum } from "../enums/OrderItemStatusEnum";

export class OrderItem extends BaseEntity implements IOrderItemDto {
    orderShippingBox: OrderShippingBox;
    productId: string;
    productTitle: string;
    productName: string;
    productUrl: string;
    productCode: string;
    productQuantity: number;
    productStockUnitId: string;
    productStockUnitCode: string;
    productStockUnitReleaseDate: string;
    productStockUnitEnablePreorder: boolean;
    productStockUnitShipsIn: number;
    productStockUnitBasePrice: number;
    productStockUnitSalePrice: number;
    productStockUnitMembershipSalePrice: number;
    image: string;
    orderItemVariants: Array<OrderItemVariant>;
    orderTaxes: Array<OrderItemTax>;
    status: OrderItemStatusEnum;
    statusDescriptions: Array<string>;
    stockStatus: OrderItemStockStatusEnum;
    stockStatusDescriptions: Array<string>;
    orderCoupon: OrderCoupon;
    minEta: string;
    maxEta: string;
    subtotalBeforeTax: number;
    subtotalAfterTax: number;
    totalBeforeTax: number;
    totalAfterTax: number;

    getOrderItemVariantByName(variantName: string) {
        return this.orderItemVariants.filter(x => x.variantName === variantName)[0];
    }

    constructor(dto?: IOrderItemDto | any) {
        super(dto);

        dto = dto || {} as IOrderItemDto;
        this.orderShippingBox = dto.orderShippingBox ? new OrderShippingBox(dto.orderShippingBox) : null;
        this.productId = dto.productId;
        this.productTitle = dto.productTitle;
        this.productName = dto.productName;
        this.productUrl = dto.productUrl;
        this.productCode = dto.productCode;
        this.productQuantity = dto.productQuantity;
        this.productStockUnitId = dto.productStockUnitId;
        this.productStockUnitCode = dto.productStockUnitCode;
        this.productStockUnitReleaseDate = dto.productStockUnitReleaseDate;
        this.productStockUnitEnablePreorder = dto.productStockUnitEnablePreorder;
        this.productStockUnitShipsIn = dto.productStockUnitShipsIn;
        this.productStockUnitBasePrice = dto.productStockUnitBasePrice;
        this.productStockUnitSalePrice = dto.productStockUnitSalePrice;
        this.productStockUnitMembershipSalePrice = dto.productStockUnitMembershipSalePrice;
        this.image = dto.image;
        this.orderItemVariants = dto.orderItemVariants ? dto.orderItemVariants.map(x => new OrderItemVariant(x)) : new Array<OrderItemVariant>();
        this.orderTaxes = dto.orderTaxes ? dto.orderTaxes.map(x => new OrderItemTax(x)) : new Array<OrderItemTax>();
        this.status = dto.status;
        this.statusDescriptions = dto.statusDescriptions || new Array<string>();
        this.stockStatus = dto.stockStatus;
        this.stockStatusDescriptions = dto.stockStatusDescriptions || new Array<string>();
        this.orderCoupon = dto.orderCoupon ? new OrderCoupon(dto.orderCoupon) : null;
        this.minEta = dto.minEta;
        this.maxEta = dto.maxEta;
        this.subtotalBeforeTax = dto.subtotalBeforeTax;
        this.subtotalAfterTax = dto.subtotalAfterTax;
        this.totalBeforeTax = dto.totalBeforeTax;
        this.totalAfterTax = dto.totalAfterTax;
    }
}