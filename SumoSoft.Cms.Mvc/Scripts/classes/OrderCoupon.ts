﻿import { IOrderCouponDto } from "../interfaces/IOrderCouponDto";
import { BaseEntity } from "./BaseEntity";

export class OrderCoupon extends BaseEntity implements IOrderCouponDto {
    code: string;
    amount: number;

    constructor(dto?: IOrderCouponDto | any) {
        super(dto);

        dto = dto || {} as IOrderCouponDto;
        this.code = dto.code;
        this.amount = dto.amount;
    }
}