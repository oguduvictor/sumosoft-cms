﻿import { BaseEntity } from "./BaseEntity";
import { IOrderAddressDto } from "../interfaces/IOrderAddressDto";

export class OrderAddress extends BaseEntity implements IOrderAddressDto {
    addressFirstName: string;
    addressLastName: string;
    addressLine1: string;
    addressLine2: string;
    countryName: string;
    stateCountyProvince: string;
    city: string;
    postcode: string;
    phoneNumber: string;

    constructor(dto?: IOrderAddressDto | any) {
        super(dto);

        dto = dto || {} as IOrderAddressDto;
        this.addressFirstName = dto.addressFirstName;
        this.addressLastName = dto.addressLastName;
        this.addressLine1 = dto.addressLine1;
        this.addressLine2 = dto.addressLine2;
        this.countryName = dto.countryName;
        this.stateCountyProvince = dto.stateCountyProvince;
        this.city = dto.city;
        this.postcode = dto.postcode;
        this.phoneNumber = dto.phoneNumber;
    }
}