﻿import { BaseEntity } from "./BaseEntity";
import { IAttributeOptionDto } from "../interfaces/IAttributeOptionDto";
import { Attribute } from "./Attribute";
import { AttributeOptionLocalizedKit } from "./AttributeOptionLocalizedKit";

export class AttributeOption extends BaseEntity implements IAttributeOptionDto {
    attribute: Attribute;
    name: string;
    url: string;
    attributeOptionLocalizedKits: Array<AttributeOptionLocalizedKit>;
    localizedTitle: string;
    tags: string;

    getLocalizedTitle?(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.attributeOptionLocalizedKits, languageCode).title;
    }

    constructor(dto?: IAttributeOptionDto | any) {
        super(dto);

        dto = dto || ({} as IAttributeOptionDto);
        this.attribute = dto.attribute ? new Attribute(dto.attribute) : null;
        this.name = dto.name;
        this.url = dto.url;
        this.tags = dto.tags;
        this.attributeOptionLocalizedKits = dto.attributeOptionLocalizedKits
            ? dto.attributeOptionLocalizedKits.map(x => new AttributeOptionLocalizedKit(x))
            : new Array<AttributeOptionLocalizedKit>();
        this.localizedTitle = dto.localizedTitle;
    }
}
