﻿import { Country } from "./Country";
import { Category } from "./Category";
import { ICategoryLocalizedKitDto } from "../interfaces/ICategoryLocalizedKitDto";
import { BaseEntity } from "./BaseEntity";

export class CategoryLocalizedKit extends BaseEntity implements ICategoryLocalizedKitDto {
    country: Country;
    category: Category;
    image: string;
    title: string;
    description: string;
    featuredTitle: string;
    featuredDescription: string;
    featuredImage: string;
    metaTitle: string;
    metaDescription: string;

    constructor(dto?: ICategoryLocalizedKitDto | any) {
        super(dto);

        dto = dto || {} as ICategoryLocalizedKitDto;
        this.country = dto.country ? new Country(dto.country) : null;
        this.category = dto.category ? new Category(dto.category) : null;
        this.title = dto.title;
        this.image = dto.image;
        this.description = dto.description;
        this.featuredTitle = dto.featuredTitle;
        this.featuredDescription = dto.featuredDescription;
        this.featuredImage = dto.featuredImage;
        this.metaTitle = dto.metaTitle;
        this.metaDescription = dto.metaDescription;
    }
}