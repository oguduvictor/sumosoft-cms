﻿import { User } from "./User";
import { BaseEntity } from "./BaseEntity";
import { BlogPost } from "./BlogPost";
import { IBlogCommentDto } from "../interfaces/IBlogCommentDto";

export class BlogComment extends BaseEntity implements IBlogCommentDto {
    title: string;
    author: User;
    email: string;
    url: string;
    body: string;
    published: boolean;
    post: BlogPost;

    constructor(dto?: IBlogCommentDto | any) {
        super(dto);

        dto = dto || {} as IBlogCommentDto;
        this.title = dto.title;
        this.author = dto.author ? new User(dto.author) : null;
        this.email = dto.email;
        this.url = dto.url;
        this.body = dto.body;
        this.published = dto.published;
        this.post = dto.post ? new BlogPost(dto.post) : null;
    }
}
