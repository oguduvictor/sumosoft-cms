﻿import { Product } from "./Product";
import { CategoryLocalizedKit } from "./CategoryLocalizedKit";
import { ICategoryDto } from "../interfaces/ICategoryDto";
import { BaseEntity } from "./BaseEntity";
import { ProductStockUnit } from "./ProductStockUnit";
import { ProductImageKit } from "./ProductImageKit";
import { CategoryTypesEnum } from "../enums/CategoryTypesEnum";

export class Category extends BaseEntity implements ICategoryDto {
    name: string;
    url: string;
    sortOrder: number;
    tags: string;
    productsSortOrder: string;
    productStockUnitsSortOrder: string;
    productImageKitsSortOrder: string;
    typeDescriptions: Array<string>;
    type: CategoryTypesEnum;
    parent: Category;
    children = new Array<Category>();
    products = new Array<Product>();
    productStockUnits = new Array<ProductStockUnit>();
    productImageKits = new Array<ProductImageKit>();
    categoryLocalizedKits = new Array<CategoryLocalizedKit>();
    localizedTitle: string;
    localizedDescription: string;
    localizedImage: string;
    localizedFeaturedTitle: string;
    localizedFeaturedDescription: string;
    localizedFeaturedImage: string;

    getLocalizedTitle(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.categoryLocalizedKits, languageCode).title;
    }

    getLocalizedDescription(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.categoryLocalizedKits, languageCode).description;
    }

    getLocalizedFeaturedTitle(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.categoryLocalizedKits, languageCode).featuredTitle;
    }

    getLocalizedFeaturedDescription(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.categoryLocalizedKits, languageCode).featuredDescription;
    }

    getLocalizedMetaTitle(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.categoryLocalizedKits, languageCode).metaTitle;
    }

    getLocalizedMetaDescription(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.categoryLocalizedKits, languageCode).metaDescription;
    }

    getLocalizedImage(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.categoryLocalizedKits, languageCode).image;
    }

    constructor(dto?: ICategoryDto | any) {
        super(dto);

        dto = dto || {} as ICategoryDto;
        this.name = dto.name;
        this.url = dto.url;
        this.sortOrder = dto.sortOrder;
        this.tags = dto.tags;
        this.productsSortOrder = dto.productsSortOrder;
        this.productStockUnitsSortOrder = dto.productStockUnitsSortOrder;
        this.productImageKitsSortOrder = dto.productImageKitsSortOrder;
        this.typeDescriptions = dto.typeDescriptions;
        this.type = dto.type;
        this.parent = dto.parent ? new Category(dto.parent) : null;
        this.children = dto.children ? dto.children.map(x => new Category(x)) : new Array<Category>();
        this.products = dto.products ? dto.products.map(x => new Product(x)) : new Array<Product>();
        this.productStockUnits = dto.productStockUnits ? dto.productStockUnits.map(x => new ProductStockUnit(x)) : new Array<ProductStockUnit>();
        this.productImageKits = dto.productImageKits ? dto.productImageKits.map(x => new ProductImageKit(x)) : new Array<ProductImageKit>();
        this.categoryLocalizedKits = dto.categoryLocalizedKits ? dto.categoryLocalizedKits.map(x => new CategoryLocalizedKit(x)) : new Array<CategoryLocalizedKit>();
        this.localizedTitle = dto.localizedTitle;
        this.localizedDescription = dto.localizedDescription;
        this.localizedImage = dto.localizedImage;
        this.localizedFeaturedTitle = dto.localizedFeaturedTitle;
        this.localizedFeaturedDescription = dto.localizedFeaturedDescription;
        this.localizedFeaturedImage = dto.localizedFeaturedImage;
    }
}