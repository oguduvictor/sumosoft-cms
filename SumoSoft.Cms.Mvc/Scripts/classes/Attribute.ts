﻿import { BaseEntity } from "./BaseEntity";
import { AttributeTypesEnum } from "../enums/AttributeTypesEnum";
import { IAttributeDto } from "../interfaces/IAttributeDto";
import { AttributeLocalizedKit } from "./AttributeLocalizedKit";
import { AttributeOption } from "./AttributeOption";

export class Attribute extends BaseEntity implements IAttributeDto {
    name: string;
    sortOrder: number;
    type: AttributeTypesEnum;
    url: string;
    typeDescriptions: Array<string>;
    localizedTitle: string;
    attributeOptions: Array<AttributeOption>;
    attributeLocalizedKits: Array<AttributeLocalizedKit>;

    getLocalizedTitle?(languageCode: string) {
        return this.getLocalizedKitByLanguageCode(this.attributeLocalizedKits, languageCode).title;
    }

    constructor(dto?: IAttributeDto | any) {
        super(dto);

        dto = dto || ({} as IAttributeDto);
        this.name = dto.name;
        this.sortOrder = dto.sortOrder;
        this.type = dto.type;
        this.url = dto.url;
        this.typeDescriptions = dto.typeDescriptions || new Array<string>();
        this.localizedTitle = dto.localizedTitle;
        this.attributeOptions = dto.attributeOptions ? dto.attributeOptions.map(x => new AttributeOption(x)) : new Array<AttributeOption>();
        this.attributeLocalizedKits = dto.attributeLocalizedKits ? dto.attributeLocalizedKits.map(x => new AttributeLocalizedKit(x)) : new Array<AttributeLocalizedKit>();
    }
}
