﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Filters
{
    using System.Web;
    using System.Web.Mvc;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class CmsIpAddressFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var requestUserHostAddress = HttpContext.Current.Request.UserHostAddress;

            if (!IsIpAddressAllowed(requestUserHostAddress?.Trim()))
            {
                context.Result = new HttpStatusCodeResult(401);
            }

            base.OnActionExecuting(context);
        }

        private static bool IsIpAddressAllowed(string ipAddress)
        {
            if (string.IsNullOrWhiteSpace(ipAddress))
            {
                return false;
            }

            var utilityService = DependencyResolver.Current.GetService<IUtilityService>();

            var addresses = utilityService.GetAppSetting("AllowedIpAddresses");

            return addresses.Contains(ipAddress);
        }

    }
}