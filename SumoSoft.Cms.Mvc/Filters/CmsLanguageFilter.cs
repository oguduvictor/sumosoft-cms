﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Filters
{
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Web.Mvc;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Extensions;

    public class CmsLanguageFilter : IAuthorizationFilter // Use IAuthorizationFilter instead of ActionFilterAttribute so it's executed before anything else
    {
        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// Sets the CultureInfo of the current thread according to the Url parameter.
        /// </summary>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            using (var dbContext = new CmsDbContext())
            {
                var countryUrl = filterContext.RouteData.GetCountry();

                var country = string.IsNullOrEmpty(countryUrl) ? dbContext.Countries.FirstOrDefault(x => x.IsDefault) : dbContext.Countries.FirstOrDefault(x => x.Localize && x.Url == countryUrl);

                if (country == null)
                {
                    filterContext.Result = new RedirectResult("/");

                    return;
                }

                var cultureInfo = CultureInfo.CreateSpecificCulture(country.LanguageCode ?? string.Empty);

                Thread.CurrentThread.CurrentCulture = cultureInfo;
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
            }
        }
    }
}