﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Filters
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Enums;

    public class CmsExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            while (exception.InnerException != null)
            {
                exception = exception.InnerException;
            }

            using (var dbContext = new CmsDbContext())
            {
                var authenticatedUserId = HttpContext.Current.User?.Identity?.Name;

                var authenticatedUser = string.IsNullOrEmpty(authenticatedUserId)
                    ? null
                    : dbContext.Users.Find(Guid.Parse(authenticatedUserId));

                var log = new Log
                {
                    Type = LogTypesEnum.Exception,
                    Message = $"Exception in {HttpContext.Current.Request.Url.AbsoluteUri}",
                    Details =
                        $"Request.Url.AbsoluteUri: {HttpContext.Current.Request.Url.AbsoluteUri}{Environment.NewLine}" +
                        $"----------------------------------------------------" + Environment.NewLine +
                        $"Message:" + Environment.NewLine +
                        $"----------------------------------------------------" + Environment.NewLine +
                        $"{exception?.Message}{Environment.NewLine}" +
                        $"----------------------------------------------------" + Environment.NewLine +
                        $"StackTrace:" + Environment.NewLine +
                        $"----------------------------------------------------" + Environment.NewLine +
                        $"{exception?.StackTrace}",
                    User = authenticatedUser
                };

                dbContext.Logs.Add(log);

                dbContext.SaveChanges();
            }
        }
    }
}