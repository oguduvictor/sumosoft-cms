﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Filters
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using System.Web.Security;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class CmsAuthorizationFilter : IAuthorizationFilter
    {
        /// <inheritdoc />
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                using (var dbContext = new CmsDbContext())
                {
                    User user;

                    try
                    {
                        var userId = new Guid(HttpContext.Current.User.Identity.Name);

                        user = dbContext.Users.Find(userId);
                    }
                    catch (Exception)
                    {
                        user = null;
                    }

                    if (user == null || user.IsDisabled)
                    {
                        FormsAuthentication.SignOut();

                        HttpContext.Current.User = null;

                        filterContext.Result = new RedirectResult("/" + filterContext.RequestContext.RouteData.GetCountry());
                    }
                }
            }
            else
            {
                // ---------------------------------------------------------------------
                // USER NOT AUTHENTICATED
                // ---------------------------------------------------------------------

                var isAuthorizationNeeded =
                    filterContext.Controller.GetType().GetCustomAttributes(true).Any(attribute => attribute is IAuthorizationFilter) ||
                    filterContext.ActionDescriptor.GetCustomAttributes(true).Any(attribute => attribute is IAuthorizationFilter);

                if (!isAuthorizationNeeded)
                {
                    return;
                }

                var currentExecutionFilePath = filterContext.RequestContext.HttpContext.Request.CurrentExecutionFilePath;

                var area = filterContext.HttpContext.Request.RequestContext.RouteData.DataTokens["area"]?.ToString();

                if (area != null && area.Equals("Admin", StringComparison.CurrentCultureIgnoreCase))
                {
                    // ---------------------------------------------------------------------
                    // REDIRECTO TO ADMIN LOGIN PAGE
                    // ---------------------------------------------------------------------

                    filterContext.Result = new RedirectResult($"~/admin/account/login?returnUrl={currentExecutionFilePath}");
                }
                else
                {
                    // ---------------------------------------------------------------------
                    // REDIRECT TO WEB.CONFIG LOGIN PAGE
                    // ---------------------------------------------------------------------

                    var configuration = WebConfigurationManager.OpenWebConfiguration("/");
                    var authenticationSection = (AuthenticationSection)configuration.GetSection("system.web/authentication");
                    var loginUrl = authenticationSection.Forms.LoginUrl.Split(new[] { "~/" }, StringSplitOptions.RemoveEmptyEntries);

                    if (loginUrl.Length <= 0)
                    {
                        return;
                    }

                    var country = filterContext.RequestContext.RouteData.GetCountry();

                    filterContext.Result = new RedirectResult($"~/{Path.Combine(country, loginUrl[0])}?returnUrl={currentExecutionFilePath}");
                }
            }
        }
    }
}