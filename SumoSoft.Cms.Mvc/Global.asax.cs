﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc
{
    using System;
    using System.Data.Entity;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using FluentValidation.Mvc;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents();

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CmsDbContext, DataAccess.Migrations.Configuration>());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            DisplayModesConfig.RegisterDisplayModes();

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FluentValidationModelValidatorProvider.Configure();
        }

        protected void Application_BeginRequest()
        {
            var requestDbContextService = DependencyResolver.Current.GetService<IRequestDbContextService>();

            requestDbContextService.Set();
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            var cmsService = DependencyResolver.Current.GetService<IRequestDbContextService>();

            cmsService.Dispose();
        }
    }
}