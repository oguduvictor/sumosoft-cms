const path = require('path');
const Webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const Merge = require("webpack-merge");
const WebpackCleanupPlugin = require("webpack-cleanup-plugin");
const StringReplacePlugin = require("string-replace-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

var DefaultConfig = [
    {
        mode: "development",
        entry: {
            admin: [
                "./node_modules/nestable2/jquery.nestable.js",
                "./Scripts/prototypes/index.js",
                "./Areas/Admin/Scripts/Components/ReactDomRender.tsx",
                "./Areas/Admin/Scripts/shared/App.ts",
                "./node_modules/nestable2/jquery.nestable.css",
                "./Areas/Admin/Content/css/admin.less"
            ]
        },
        output: {
            path: path.resolve(__dirname, "Areas/Admin/Scripts/bundles"),
            publicPath: "/Areas/Admin/Scripts/bundles/",
            filename: "[name].bundle.js",
            chunkFilename: "[chunkhash].chunk.js"
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"]
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: StringReplacePlugin.replace({
                        replacements: [
                            {
                                pattern: /_import\(/g,
                                replacement: function (match, p1, offset, string) {
                                    return "import(";
                                }
                            }
                        ]
                    })
                },
                {
                    test: /\.less|\.css$/,
                    use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader?sourceMap", "less-loader"]
                },
                {
                    test: /\.tsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "ts-loader",
                    options: {
                        transpileOnly: true
                    }
                }
            ]
        },
        plugins: [
            new StringReplacePlugin(),
            new WebpackCleanupPlugin({
                quiet: true
            }),
            new MiniCssExtractPlugin({
                filename: "admin.css"
            }),
            new Webpack.WatchIgnorePlugin([
                /\.js$/,
                /\.d\.ts$/
            ]),
            new ForkTsCheckerWebpackPlugin()
        ]
    },
    // ---------------------------------------------------
    // sumoJS
    // ---------------------------------------------------
    {
        mode: "production",
        entry: [
            "./node_modules/url-search-params-polyfill/index.js",
            "./Scripts/sumoJS/sumoJS.ts"
        ],
        output: {
            path: path.resolve(__dirname, "Scripts/sumoJS"),
            filename: "index.js",
            libraryTarget: "umd"
        },
        resolve: {
            extensions: [".ts", ".js"]
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "ts-loader",
                    options: {
                        transpileOnly: true
                    }
                }
            ]
        },
        plugins: [
            new ForkTsCheckerWebpackPlugin()
        ]
    },
    // ---------------------------------------------------
    // sumoEnhancedEcommerce
    // ---------------------------------------------------
    {
        mode: "development",
        entry: [
            "./Scripts/sumoEnhancedEcommerce/sumoEnhancedEcommerce.ts"
        ],
        output: {
            path: path.resolve(__dirname, "Scripts/sumoEnhancedEcommerce"),
            filename: "index.js",
            libraryTarget: "umd"
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"]
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "ts-loader"
                }
            ]
        }
    },
    // ---------------------------------------------------
    // sumoSignalR
    // ---------------------------------------------------
    {
        mode: "development",
        entry: [
            "./Scripts/sumoSignalR/sumoSignalR.ts"
        ],
        output: {
            path: path.resolve(__dirname, "Scripts/sumoSignalR"),
            filename: "index.js",
            libraryTarget: "umd"
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"]
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "ts-loader"
                }
            ]
        }
    },
    // ---------------------------------------------------
    // prototypes
    // ---------------------------------------------------
    {
        mode: "development",
        entry: [
            "./Scripts/prototypes/Date.ts",
            "./Scripts/prototypes/Array.ts",
            "./Scripts/prototypes/String.ts"
        ],
        output: {
            path: path.resolve(__dirname, "Scripts/prototypes"),
            filename: "index.js",
            libraryTarget: "umd"
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"]
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "ts-loader"
                }
            ]
        }
    }
];

module.exports = function () {

    if (process.env.NODE_ENV === "production") {

        console.log("Generating production bundles...");

        var minifyConfig = {
            mode: "production"
        };

        return DefaultConfig.map((x) => Merge(x, minifyConfig));
    }

    console.log("Generating development bundles...");

    return DefaultConfig;
};