﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class CmsApiWishListController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly ICmsService cmsService;
        private readonly IUtilityService utilityService;

        public CmsApiWishListController(
            ICmsService cmsService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.cmsService = cmsService;
            this.utilityService = utilityService;
        }

        [HttpPost]
        public virtual JsonResult GetWishList()
        {
            var wishList = this.cmsService.GetWishList(this.requestDbContext) ?? new WishList();

            var wishListDto = new WishListDto
            {
                CartItems = wishList.CartItems.Select(cartItem => new CartItemDto
                {
                    Id = cartItem.Id,
                    Quantity = cartItem.Quantity,
                    Image = cartItem.Image,
                    CartShippingBox = cartItem.CartShippingBox == null ? null : new CartShippingBoxDto
                    {
                        Id = cartItem.CartShippingBox.Id,
                        ShippingBox = cartItem.CartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                        {
                            Id = cartItem.CartShippingBox.ShippingBox.Id,
                            Name = cartItem.CartShippingBox.ShippingBox.Name,
                            LocalizedTitle = cartItem.CartShippingBox.ShippingBox.GetLocalizedTitle(),
                            LocalizedDescription = cartItem.CartShippingBox.ShippingBox.GetLocalizedDescription(),
                            LocalizedPrice = cartItem.CartShippingBox.ShippingBox.GetLocalizedPrice()
                        }
                    },
                    Product = cartItem.Product == null ? null : new ProductDto
                    {
                        Id = cartItem.Product.Id,
                        Name = cartItem.Product.Name,
                        Url = cartItem.Product.Url,
                        Code = cartItem.Product.Code,
                        TaxCode = cartItem.Product.TaxCode,
                        Categories = cartItem.Product.Categories.Select(category => new CategoryDto
                        {
                            Id = category.Id,
                            Name = category.Name,
                            Url = category.Url,
                            LocalizedTitle = category.GetLocalizedTitle()
                        }).ToList(),
                        ProductAttributes = cartItem.Product.ProductAttributes.Select(productAttribute => new ProductAttributeDto
                        {
                            Id = productAttribute.Id,
                            Attribute = productAttribute.Attribute == null ? null : new AttributeDto
                            {
                                Id = productAttribute.Attribute.Id,
                                Name = productAttribute.Attribute.Name,
                                Url = productAttribute.Attribute.Url,
                                Type = productAttribute.Attribute.Type
                            },
                            AttributeOptionValue = productAttribute.AttributeOptionValue == null ? null : new AttributeOptionDto
                            {
                                Id = productAttribute.AttributeOptionValue.Id,
                                Name = productAttribute.AttributeOptionValue.Name,
                                Url = productAttribute.AttributeOptionValue.Url,
                                LocalizedTitle = productAttribute.AttributeOptionValue.GetLocalizedTitle()
                            },
                            ContentSectionValue = productAttribute.ContentSectionValue == null ? null : new ContentSectionDto
                            {
                                Id = productAttribute.ContentSectionValue.Id,
                                Name = productAttribute.ContentSectionValue.Name,
                                LocalizedContent = productAttribute.ContentSectionValue.GetLocalizedContent()
                            },
                            DoubleValue = productAttribute.DoubleValue,
                            StringValue = productAttribute.StringValue,
                            ImageValue = productAttribute.ImageValue,
                            BooleanValue = productAttribute.BooleanValue,
                            DateTimeValue = productAttribute.DateTimeValue
                        }).ToList(),
                        ProductVariants = cartItem.Product.ProductVariants.Select(productVariant => new ProductVariantDto
                        {
                            Id = productVariant.Id,
                            Variant = productVariant.Variant == null ? null : new VariantDto
                            {
                                Id = productVariant.Variant.Id,
                                Name = productVariant.Variant.Name,
                                Url = productVariant.Variant.Url,
                                CreateProductImageKits = productVariant.Variant.CreateProductImageKits,
                                CreateProductStockUnits = productVariant.Variant.CreateProductStockUnits
                            },
                            VariantOptions = productVariant
                                    .VariantOptions
                                    .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled())
                                    .OrderBySortOrder()
                                    .ThenBy(x => x.Name)
                                    .Select(variantOption => new VariantOptionDto
                                    {
                                        Id = variantOption.Id,
                                        Name = variantOption.Name,
                                        Url = variantOption.Url,
                                        Image = variantOption.Image,
                                        LocalizedTitle = variantOption.GetLocalizedTitle(),
                                        LocalizedDescription = variantOption.GetLocalizedDescription(),
                                        LocalizedPriceModifier = variantOption.GetLocalizedPriceModifier(),
                                        LocalizedIsDisabled = variantOption.GetLocalizedIsDisabled()
                                    }).ToList()
                        }).ToList(),
                        ShippingBoxes = cartItem.Product.ShippingBoxes.OrderBySortOrder().Select(shippingBox => new ShippingBoxDto
                        {
                            Id = shippingBox.Id,
                            Name = shippingBox.Name,
                            LocalizedTitle = shippingBox.GetLocalizedTitle(),
                            LocalizedDescription = shippingBox.GetLocalizedDescription(),
                            LocalizedPrice = shippingBox.GetLocalizedPrice(),
                            LocalizedFreeShippingMinimumPrice = shippingBox.GetLocalizedFreeShippingMinimumPrice(),
                            LocalizedMinDays = shippingBox.GetLocalizedMinDays(),
                            LocalizedMaxDays = shippingBox.GetLocalizedMaxDays(),
                            Countries = shippingBox.Countries.Select(country => new CountryDto
                            {
                                Id = country.Id,
                                Name = country.Name
                            }).ToList()
                        }).ToList(),
                        ProductStockUnits = cartItem.GetProductStockUnit().InList().Select(productStockUnit => new ProductStockUnitDto
                        {
                            Id = productStockUnit.Id,
                            DispatchDate = productStockUnit.DispatchDate,
                            EnablePreorder = productStockUnit.EnablePreorder,
                            DispatchTime = productStockUnit.DispatchTime,
                            Stock = productStockUnit.Stock,
                            Code = productStockUnit.Code,
                            LocalizedBasePrice = productStockUnit.GetLocalizedBasePrice(),
                            LocalizedSalePrice = productStockUnit.GetLocalizedSalePrice(),
                            VariantOptions = productStockUnit.VariantOptions.Select(variantOption => new VariantOptionDto
                            {
                                Id = variantOption.Id,
                                Name = variantOption.Name,
                                Url = variantOption.Url
                            }).ToList()
                        }).ToList(),
                        LocalizedTitle = cartItem.Product.GetLocalizedTitle(),
                        LocalizedDescription = cartItem.Product.GetLocalizedDescription(),
                        LocalizedMetaTitle = cartItem.Product.GetLocalizedMetaTitle(),
                        LocalizedMetaDescription = cartItem.Product.GetLocalizedMetaDescription()
                    },
                    CartItemVariants = cartItem.CartItemVariants.Select(cartItemVariant => new CartItemVariantDto
                    {
                        Id = cartItemVariant.Id,
                        CartItem = cartItemVariant.CartItem == null ? null : new CartItemDto
                        {
                            Id = cartItemVariant.CartItem.Id
                        },
                        Variant = cartItemVariant.Variant == null ? null : new VariantDto
                        {
                            Id = cartItemVariant.Variant.Id,
                            Name = cartItemVariant.Variant.Name,
                            Url = cartItemVariant.Variant.Url,
                            LocalizedTitle = cartItemVariant.Variant.GetLocalizedTitle()
                        },
                        VariantOptionValue = cartItemVariant.VariantOptionValue == null ? null : new VariantOptionDto
                        {
                            Id = cartItemVariant.VariantOptionValue.Id,
                            Name = cartItemVariant.VariantOptionValue.Name,
                            Url = cartItemVariant.VariantOptionValue.Url,
                            Image = cartItemVariant.VariantOptionValue.Image,
                            LocalizedTitle = cartItemVariant.VariantOptionValue.GetLocalizedTitle(),
                            LocalizedDescription = cartItemVariant.VariantOptionValue.GetLocalizedDescription(),
                            LocalizedPriceModifier = cartItemVariant.VariantOptionValue.GetLocalizedPriceModifier()
                        },
                        BooleanValue = cartItemVariant.BooleanValue,
                        DoubleValue = cartItemVariant.DoubleValue,
                        IntegerValue = cartItemVariant.IntegerValue,
                        StringValue = cartItemVariant.StringValue,
                        JsonValue = cartItemVariant.JsonValue
                    }).ToList(),
                    SubtotalBeforeTax = cartItem.GetSubtotalBeforeTax(this.requestDbContext),
                    SubtotalAfterTax = cartItem.GetSubtotalAfterTax(this.requestDbContext),
                    SubtotalBeforeTaxWithoutSalePrice = cartItem.GetSubtotalBeforeTaxWithoutSalePrice(this.requestDbContext),
                    SubtotalAfterTaxWithoutSalePrice = cartItem.GetSubtotalAfterTaxWithoutSalePrice(this.requestDbContext),
                    TotalBeforeTax = cartItem.GetTotalBeforeTax(this.requestDbContext),
                    TotalAfterTax = cartItem.GetTotalAfterTax(this.requestDbContext)
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(wishListDto);
        }

        [HttpPost]
        public virtual JsonResult GetWishListLite()
        {
            var wishList = this.cmsService.GetWishList(this.requestDbContext) ?? new WishList();

            var cartItems = wishList.CartItems.Select(cartItem => new CartItemDto
            {
                Id = cartItem.Id,
                Product = cartItem.Product == null ? null : new ProductDto
                {
                    Id = cartItem.Product.Id
                }
            }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(cartItems);
        }

        [HttpPost]
        public virtual JsonResult AddToWishList(CartItem cartItem)
        {
            cartItem = this.cmsService.AddToWishList(this.requestDbContext, cartItem);

            return this.utilityService.GetCamelCaseJsonResult(cartItem.Id);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItemId"></param>
        [HttpPost]
        public virtual void RemoveFromWishList(Guid cartItemId)
        {
            this.cmsService.RemoveFromWishList(this.requestDbContext, cartItemId);
        }
    }
}