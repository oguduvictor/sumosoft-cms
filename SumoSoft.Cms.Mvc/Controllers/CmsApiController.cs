﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FluentValidation;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.LogServices.Interfaces;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class CmsApiController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly ICmsService cmsService;
        private readonly IGeolocalizationService geolocalizationService;
        private readonly IAuthenticationService authenticationService;
        private readonly IUtilityService utilityService;
        private readonly IContentSectionService contentSectionService;
        private readonly ILogService logService;
        private readonly ICountryService countryService;

        public CmsApiController(
            ICmsService cmsService,
            IGeolocalizationService geolocalizationService,
            IRequestDbContextService requestDbContextService,
            IAuthenticationService authenticationService,
            IUtilityService utilityService,
            IContentSectionService contentSectionService,
            ILogService logService,
            ICountryService countryService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.cmsService = cmsService;
            this.geolocalizationService = geolocalizationService;
            this.authenticationService = authenticationService;
            this.utilityService = utilityService;
            this.contentSectionService = contentSectionService;
            this.logService = logService;
            this.countryService = countryService;
        }

        [HttpPost]
        public virtual JsonResult GetOrder(int orderNumber)
        {
            var order = this.requestDbContext.Orders.FirstOrDefault(x => x.OrderNumber == orderNumber);

            var orderDto = order == null ? null : new OrderDto
            {
                Id = order.Id,
                CreatedDate = order.CreatedDate,
                OrderNumber = order.OrderNumber,
                Status = order.Status,
                StatusDescriptions = order.Status.GetType().GetDescriptions(),
                CountryName = order.CountryName,
                CountryLanguageCode = order.CountryLanguageCode,
                CurrencySymbol = order.CurrencySymbol,
                Iso4217CurrencySymbol = order.ISO4217CurrencySymbol,
                UserId = order.UserId,
                UserFirstName = order.UserFirstName,
                UserLastName = order.UserLastName,
                UserEmail = order.UserEmail,
                TransactionId = order.TransactionId,
                OrderShippingBoxes = order.OrderShippingBoxes.Select(shippingBox => new OrderShippingBoxDto
                {
                    Id = shippingBox.Id,
                    CountWeekends = shippingBox.CountWeekends,
                    Description = shippingBox.Description,
                    InternalDescription = shippingBox.InternalDescription,
                    IsDisabled = shippingBox.IsDisabled,
                    MaxDays = shippingBox.MaxDays,
                    MinDays = shippingBox.MinDays,
                    Name = shippingBox.Name,
                    Order = shippingBox.Order == null ? null : new OrderDto(shippingBox.Order.Id),
                    ShippingPriceAfterTax = shippingBox.ShippingPriceAfterTax,
                    ShippingPriceBeforeTax = shippingBox.ShippingPriceBeforeTax,
                    Status = shippingBox.Status,
                    StatusDescriptions = shippingBox.Status.GetType().GetDescriptions(),
                    Title = shippingBox.Title,
                    TrackingCode = shippingBox.TrackingCode,
                    OrderItems = shippingBox.OrderItems.Select(orderItem => new OrderItemDto
                    {
                        Id = orderItem.Id,
                        Image = orderItem.Image,
                        IsDisabled = orderItem.IsDisabled,
                        StockStatusDescriptions = orderItem.StockStatus.GetType().GetDescriptions(),
                        ProductCode = orderItem.ProductCode,
                        ProductId = orderItem.ProductId,
                        ProductName = orderItem.ProductName,
                        ProductStockUnitCode = orderItem.ProductStockUnitCode,
                        ProductTitle = orderItem.ProductTitle,
                        ProductUrl = orderItem.ProductUrl,
                        ProductStockUnitId = orderItem.ProductStockUnitId,
                        ProductStockUnitSalePrice = orderItem.ProductStockUnitSalePrice,
                        ProductStockUnitBasePrice = orderItem.ProductStockUnitBasePrice,
                        ProductQuantity = orderItem.ProductQuantity,
                        ProductStockUnitShipsIn = orderItem.ProductStockUnitShipsIn,
                        Status = orderItem.Status,
                        StatusDescriptions = orderItem.Status.GetType().GetDescriptions(),
                        StockStatus = orderItem.StockStatus,
                        MinEta = orderItem.MinEta,
                        MaxEta = orderItem.MaxEta,
                        SubtotalAfterTax = orderItem.SubtotalAfterTax,
                        SubtotalBeforeTax = orderItem.SubtotalBeforeTax,
                        TotalAfterTax = orderItem.TotalAfterTax,
                        TotalBeforeTax = orderItem.TotalBeforeTax,
                        OrderShippingBox = orderItem.OrderShippingBox == null ? null : new OrderShippingBoxDto(orderItem.OrderShippingBox.Id),
                        OrderTaxes = orderItem.OrderTaxes.Select(orderItemTax => new OrderItemTaxDto
                        {
                            Id = orderItemTax.Id,
                            Amount = orderItemTax.Amount,
                            Code = orderItemTax.Code,
                            IsDisabled = orderItemTax.IsDisabled,
                            Name = orderItemTax.Name,
                            OrderItem = orderItemTax.OrderItem == null ? null : new OrderItemDto(orderItemTax.OrderItem.Id)
                        }).ToList()
                    }).ToList()
                }).ToList(),
                ShippingAddress = order.ShippingAddress == null ? null : new OrderAddressDto
                {
                    Id = order.ShippingAddress.Id,
                    AddressFirstName = order.ShippingAddress.AddressFirstName,
                    AddressLastName = order.ShippingAddress.AddressLastName,
                    AddressLine1 = order.ShippingAddress.AddressLine1,
                    AddressLine2 = order.ShippingAddress.AddressLine2,
                    City = order.ShippingAddress.City,
                    CountryName = order.ShippingAddress.CountryName,
                    IsDisabled = order.ShippingAddress.IsDisabled,
                    PhoneNumber = order.ShippingAddress.PhoneNumber,
                    Postcode = order.ShippingAddress.Postcode,
                    StateCountyProvince = order.ShippingAddress.StateCountyProvince
                },
                Comments = order.Comments,
                SentEmails = order.SentEmails.Select(x => new SentEmailDto
                {
                    Id = x.Id,
                    Email = x.Email == null ? null : new EmailDto
                    {
                        Id = x.Email.Id
                    }
                }).ToList(),
                OrderCoupon = order.OrderCoupon == null ? null : new OrderCouponDto
                {
                    Id = order.OrderCoupon.Id,
                    Code = order.OrderCoupon.Code,
                    Amount = order.OrderCoupon.Amount,
                    IsDisabled = order.OrderCoupon.IsDisabled
                },
                OrderCredit = order.OrderCredit == null ? null : new OrderCreditDto
                {
                    Id = order.OrderCredit.Id,
                    Amount = order.OrderCredit.Amount,
                    Reference = order.OrderCredit.Reference,
                    IsDisabled = order.OrderCredit.IsDisabled
                },
                TotalBeforeTax = order.TotalBeforeTax,
                TotalAfterTax = order.TotalAfterTax,
                TotalToBePaid = order.TotalToBePaid,
                TotalPaid = order.TotalPaid
            };

            return this.utilityService.GetCamelCaseJsonResult(orderDto);
        }

        [HttpPost]
        public virtual JsonResult ContentSections(List<string> contentSectionNames, string languageCode = null)
        {
            var contentSections = contentSectionNames.Select(contentSectionName => new ContentSectionDto
            {
                Name = contentSectionName,
                LocalizedContent = this.contentSectionService.ContentSection(this.requestDbContext, contentSectionName, null, languageCode)
            });

            return this.utilityService.GetCamelCaseJsonResult(contentSections);
        }

        [HttpPost]
        public virtual void Log(LogTypesEnum type, string message, string details = null)
        {
            var user = this.authenticationService.GetAuthenticatedUser(this.requestDbContext) ??
                       this.authenticationService.GetPartiallyAuthenticatedUser(this.requestDbContext);

            this.logService.Log(this.requestDbContext, type, message, details, user);
        }

        [HttpPost]
        public virtual JsonResult GetShippingBoxes()
        {
            var shippingBoxes = this.requestDbContext.ShippingBoxes.AsEnumerable().Select(x => new ShippingBoxDto
            {
                Id = x.Id,
                Name = x.Name,
                RequiresShippingAddress = x.RequiresShippingAddress,
                LocalizedTitle = x.GetLocalizedTitle(),
                Countries = x.Countries.Select(country => new CountryDto
                {
                    Id = country.Id,
                    LanguageCode = country.LanguageCode,
                    Localize = country.Localize,
                    Name = country.Name
                }).ToList()
            }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(shippingBoxes);
        }

        [HttpPost]
        public virtual JsonResult GetUserLocation()
        {
            var countryCode = this.geolocalizationService.GetCountryFromIp(this.Request.UserHostAddress);

            var country = this.requestDbContext.Countries.FirstOrDefault(x => x.LanguageCode.EndsWith("-" + countryCode)) ??
                          this.requestDbContext.Countries.FirstOrDefault(x => x.IsDefault);

            if (country == null)
            {
                return null;
            }

            var countryDto = new CountryDto
            {
                Id = country.Id,
                Name = country.Name,
                LanguageCode = country.LanguageCode,
                Localize = country.Localize,
                Url = country.Url,
                IsDisabled = country.IsDisabled,
                CurrencySymbol = country.GetCurrencySymbol(),
                Iso4217CurrencySymbol = country.GetIso4217CurrencySymbol()
            };

            return this.utilityService.GetCamelCaseJsonResult(countryDto);
        }

        [HttpPost]
        public virtual JsonResult GetUserAddresses()
        {
            var user = this.authenticationService.GetAuthenticatedUser(this.requestDbContext).AsNotNull();

            var addresses = user.Addresses
                .OrderByDescending(x => x.Id == user.Cart?.ShippingAddress?.Id)
                .ThenByDescending(x => x.Id == user.Cart?.BillingAddress?.Id)
                .ThenByDescending(x => x.ModifiedDate)
                .Select(x => new AddressDto
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    AddressLine1 = x.AddressLine1,
                    AddressLine2 = x.AddressLine2,
                    City = x.City,
                    Country = x.Country == null ? new CountryDto() : new CountryDto
                    {
                        Id = x.Country.Id,
                        Name = x.Country.Name,
                        IsDefault = x.Country.IsDefault,
                        Localize = x.Country.Localize,
                        IsDisabled = x.Country.IsDisabled
                    },
                    StateCountyProvince = x.StateCountyProvince,
                    Postcode = x.Postcode,
                    PhoneNumber = x.PhoneNumber,
                    IsDisabled = x.IsDisabled,
                    User = x.User == null ? null : new UserDto
                    {
                        Id = x.User.Id,
                        FirstName = x.User.FirstName,
                        LastName = x.User.LastName,
                        Email = x.User.Email
                    }
                }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(addresses);
        }

        [HttpPost]
        public virtual JsonResult AddOrUpdateAddress(Address address, bool setUser = false, bool setAsCartShippingAddress = false, bool setAsCartBillingAddress = false)
        {
            var addOrUpdateAddressResponse = this.cmsService.AddOrUpdateAddress(this.requestDbContext, address, setUser, setAsCartShippingAddress, setAsCartBillingAddress);

            var formResponse = new FormResponse<AddressDto>();

            if (addOrUpdateAddressResponse.IsValid)
            {
                var dbAddress = addOrUpdateAddressResponse.Target.AsNotNull();

                formResponse.Target = new AddressDto
                {
                    Id = dbAddress.Id,
                    FirstName = dbAddress.FirstName,
                    LastName = dbAddress.LastName,
                    AddressLine1 = dbAddress.AddressLine1,
                    AddressLine2 = dbAddress.AddressLine2,
                    City = dbAddress.City,
                    Country = dbAddress.Country == null ? new CountryDto() : new CountryDto
                    {
                        Id = dbAddress.Country.Id,
                        Name = dbAddress.Country.Name,
                        IsDefault = dbAddress.Country.IsDefault,
                        Localize = dbAddress.Country.Localize,
                        IsDisabled = dbAddress.Country.IsDisabled
                    },
                    StateCountyProvince = dbAddress.StateCountyProvince,
                    Postcode = dbAddress.Postcode,
                    PhoneNumber = dbAddress.PhoneNumber,
                    IsDisabled = dbAddress.IsDisabled,
                    User = dbAddress.User == null ? null : new UserDto
                    {
                        Id = dbAddress.User.Id,
                        FirstName = dbAddress.User.FirstName,
                        LastName = dbAddress.User.LastName,
                        Email = dbAddress.User.Email
                    }
                };
            }
            else
            {
                formResponse.Errors.AddRange(addOrUpdateAddressResponse.Errors);
            }

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult DeleteAddress(Guid addressId)
        {
            var formResponse = new FormResponse();

            this.requestDbContext.Addresses.Delete(addressId, true);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult GetCurrentCountry()
        {
            var country = this.countryService.GetCurrentCountry(this.requestDbContext);

            var countryDto = new CountryDto
            {
                Id = country.Id,
                Name = country.Name,
                CurrencySymbol = country.GetCurrencySymbol(),
                FlagIcon = country.FlagIcon,
                IsDefault = country.IsDefault,
                IsDisabled = country.IsDisabled,
                Iso4217CurrencySymbol = country.GetIso4217CurrencySymbol(),
                LanguageCode = country.LanguageCode,
                Localize = country.Localize,
                PaymentAccountId = country.PaymentAccountId,
                Url = country.Url,
                Taxes = country.Taxes.Select(tax => new TaxDto
                {
                    Id = tax.Id,
                    Name = tax.Name,
                    IsDisabled = tax.IsDisabled,
                    Amount = tax.Amount,
                    ApplyToProductPrice = tax.ApplyToProductPrice,
                    ApplyToShippingPrice = tax.ApplyToShippingPrice,
                    Code = tax.Code,
                    Percentage = tax.Percentage,
                    CartItem = tax.CartItem == null ? null : new CartItemDto(tax.CartItem.Id),
                    Country = tax.Country == null ? null : new CountryDto(tax.Country.Id),
                    ShippingAddress = tax.ShippingAddress == null ? null : new AddressDto(tax.ShippingAddress.Id)
                }).ToList()
            };

            return this.utilityService.GetCamelCaseJsonResult(countryDto);
        }

        [HttpPost]
        public virtual JsonResult GetCountries()
        {
            var countryDtos = this.requestDbContext.Countries.AsEnumerable().OrderBy(x => x.Name).Select(c => new CountryDto
            {
                Id = c.Id,
                LanguageCode = c.LanguageCode,
                CurrencySymbol = c.GetCurrencySymbol(),
                FlagIcon = c.FlagIcon,
                Iso4217CurrencySymbol = c.GetIso4217CurrencySymbol(),
                Name = c.Name,
                Localize = c.Localize,
                IsDisabled = c.IsDisabled,
                IsDefault = c.IsDefault,
                Url = c.Url
            });

            return this.utilityService.GetCamelCaseJsonResult(countryDtos);
        }

        [HttpPost]
        public virtual JsonResult EmailIsRegistered(string email)
        {
            var validator = new InlineValidator<string>();

            validator
                .RuleFor(x => x)
                .NotEmpty()
                .EmailAddress()
                .WithName("Email")
                .Configure(c => c.CascadeMode = CascadeMode.StopOnFirstFailure);

            var validationResult = validator.Validate(email);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponseJson(validationResult);
            }

            var user = this.requestDbContext.Users.FirstOrDefault(x => x.Email.Equals(email));

            var formResponse = new FormResponse<bool>
            {
                Target = user?.IsRegistered ?? false
            };

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult GetAuthenticatedUser()
        {
            var user = this.authenticationService.GetAuthenticatedUser(this.requestDbContext);

            if (user == null)
            {
                return null;
            }

            var userDto = new UserDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = user.Password,
                Role = user.Role == null ? null : new UserRoleDto
                {
                    Id = user.Role.Id,
                    Name = user.Role.Name,
                    AccessAdmin = user.Role.AccessAdmin
                },
                LastLogin = user.LastLogin,
                Gender = user.Gender,
                LoginProvider = user.LoginProvider,
                Country = user.Country == null ? null : new CountryDto
                {
                    Id = user.Country.Id,
                    Name = user.Country.Name,
                    FlagIcon = user.Country.FlagIcon,
                    Url = user.Country.Url,
                    LanguageCode = user.Country.LanguageCode,
                    IsDefault = user.Country.IsDefault,
                    Localize = user.Country.Localize,
                    CurrencySymbol = user.Country.GetCurrencySymbol(),
                    Iso4217CurrencySymbol = user.Country.GetIso4217CurrencySymbol()
                }
            };

            return this.utilityService.GetCamelCaseJsonResult(userDto);
        }

        [HttpPost]
        public virtual JsonResult RegisterUser(User user)
        {
            var formResponse = this.authenticationService.RegisterUser(this.requestDbContext, user);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult AuthenticateUser(string email, string password)
        {
            var formResponse = this.authenticationService.AuthenticateUser(this.requestDbContext, email, password);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual double GetUserCreditTotal()
        {
            var cart = this.cmsService.GetCart(this.requestDbContext) ?? this.cmsService.CreateCart(this.requestDbContext);

            var creditsTotal = cart.User?.UserLocalizedKits.GetByLanguageCode()?.UserCredits.Sum(x => x.Amount) ?? 0;

            return creditsTotal;
        }

        [HttpPost]
        public virtual JsonResult SetShippingBox(Guid cartItemId, string shippingBoxName)
        {
            var formResponse = new FormResponse();

            this.cmsService.SetShippingBox(this.requestDbContext, cartItemId, shippingBoxName);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}