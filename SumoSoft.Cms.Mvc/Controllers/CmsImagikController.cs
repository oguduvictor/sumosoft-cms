﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Runtime.Caching;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.LogServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class CmsImagikController : Controller
    {
        private readonly IBitmapService bitmapService;
        private readonly IAzureStorageService azureStorageService;
        private readonly IUtilityService utilityService;
        private readonly ILogService logService;

        public CmsImagikController(
            IBitmapService bitmapService,
            IAzureStorageService azureStorageService,
            IUtilityService utilityService,
            ILogService logService)
        {
            this.bitmapService = bitmapService;
            this.azureStorageService = azureStorageService;
            this.utilityService = utilityService;
            this.logService = logService;
        }

        [Route("imagik")]
        [OutputCache(Duration = int.MaxValue, VaryByParam = "*")]
        public async Task<ActionResult> Index(string url = null, int? width = null, bool save = false)
        {
            if (string.IsNullOrEmpty(url))
            {
                return null;
            }

            if (width == null)
            {
                return this.RedirectPermanent(url);
            }

            if (width < 1 || width > 3000)
            {
                return null;
            }

            var mediaEndpoint = this.utilityService.GetAppSetting("MediaEndpoint");

            if (!url.ContainsCaseInsensitive(mediaEndpoint))
            {
                return null;
            }

            var containerName = this.utilityService.GetAppSetting("AzureStorageContainer");

            if (!url.ContainsCaseInsensitive(containerName))
            {
                return null;
            }

            var uri = new Uri(url);
            var localPath = uri.LocalPath;
            var fileNameAndExtension = localPath.Split('/').Last();
            var fileName = fileNameAndExtension.SubstringUpToFirst(".");
            var compressedFileQuery = uri.Query
                .Replace("?", "-")
                .Replace("&", "-")
                .Replace("=", "-");
            var compressedFileNameAndExtension = $"{fileName}{compressedFileQuery}-{width}px.jpg";
            var compressedFileUrl = url
                .Replace($"/{containerName}", "/imagik")
                .Replace(fileNameAndExtension, compressedFileNameAndExtension)
                .SubstringUpToFirst("?");

            // -------------------------------------------------------------
            // Check first if a compressed image already exists.
            // If a compressed images doesn't exist yet, create it.
            // -------------------------------------------------------------

            try
            {
                var request = WebRequest.Create(new Uri(compressedFileUrl));

                request.Method = "HEAD";

                request.GetResponse().Close();

                return this.Redirect(compressedFileUrl);
            }
            catch (Exception)
            {
                if (this.MaxRequestFilter())
                {
                    return null;
                }

                // -----------------------------------------------
                // Handle 404 file nor found exception
                // -----------------------------------------------

                MemoryStream sourceFileMemoryStream;

                try
                {
                    var bytes = await new WebClient().DownloadDataTaskAsync(url);

                    sourceFileMemoryStream = new MemoryStream(bytes);
                }
                catch (Exception)
                {
                    return null;
                }

                // -----------------------------------------------
                // Generate compressed file
                // -----------------------------------------------

                var compressedFileMemoryStream = this.bitmapService.ResizeImage(80, sourceFileMemoryStream, width.Value);

                if (save)
                {
                    var imagikDirectoryLocalPath = compressedFileUrl
                        .Replace(mediaEndpoint, "")
                        .Replace(compressedFileNameAndExtension, "")
                        .Replace($"/{containerName}", "/imagik");

                    this.azureStorageService.UploadFile(imagikDirectoryLocalPath, compressedFileMemoryStream, compressedFileNameAndExtension);
                }

                return this.File(compressedFileMemoryStream, "image/jpeg");
            }
        }

        private class MaxRequestFilterDto
        {
            public DateTimeOffset Expiration { get; set; }

            public int RequestCount { get; set; }
        }

        private bool MaxRequestFilter()
        {
            var maxRequestTimeSpan = this.utilityService.GetAppSetting("ImagikMaxRequestTimeSpan", "60").ToInt(); // seconds
            var maxRequestCount = this.utilityService.GetAppSetting("ImagikMaxRequestCount", "1000").ToInt();

            var ip = System.Web.HttpContext.Current.Request.UserHostAddress.AsNotNull();

            var userIdentifyer = "Imagik_MaxRequestFilter+" + ip;

            var lockOutFilterDto = (MaxRequestFilterDto)MemoryCache.Default.Get(userIdentifyer);

            if (lockOutFilterDto == null || lockOutFilterDto.Expiration < DateTimeOffset.Now)
            {
                lockOutFilterDto = new MaxRequestFilterDto
                {
                    Expiration = DateTimeOffset.Now.AddSeconds(maxRequestTimeSpan),
                    RequestCount = 1
                };
            }
            else
            {
                lockOutFilterDto.RequestCount++;
            }

            if (lockOutFilterDto.RequestCount > maxRequestCount)
            {
                MemoryCache.Default.Set(userIdentifyer, lockOutFilterDto, DateTimeOffset.MaxValue);

                this.logService.LogAsync(LogTypesEnum.Exception, $"The following IP has been locked out by the Imagik server: {ip}");

                return true;
            }

            MemoryCache.Default.Set(userIdentifyer, lockOutFilterDto, DateTimeOffset.Now.AddSeconds(maxRequestTimeSpan));

            return false;
        }
    }
}