﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class CmsApiCartController : Controller
    {
        private readonly CmsDbContext requestDbContext;
        private readonly ICmsService cmsService;
        private readonly IUtilityService utilityService;

        public CmsApiCartController(
            ICmsService cmsService,
            IRequestDbContextService requestDbContextService,
            IUtilityService utilityService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.cmsService = cmsService;
            this.utilityService = utilityService;
        }

        [HttpPost]
        public virtual JsonResult GetCart()
        {
            var cartDto = this.cmsService.GetCartDto(this.requestDbContext);

            return this.utilityService.GetCamelCaseJsonResult(cartDto);
        }

        [HttpPost]
        public virtual JsonResult GetCartItem(Guid id)
        {
            var cartItemDto = this.cmsService.GetCartItemDto(this.requestDbContext, id);

            return this.utilityService.GetCamelCaseJsonResult(cartItemDto);
        }

        [HttpPost]
        public virtual JsonResult GetCartLite()
        {
            var cart = this.cmsService.GetCart(this.requestDbContext) ?? new Cart();

            var cartItems = cart.CartShippingBoxes
                .SelectMany(x => x.CartItems)
                .Select(cartItem => new CartItemDto
                {
                    Id = cartItem.Id,
                    Product = cartItem.Product == null ? null : new ProductDto
                    {
                        Id = cartItem.Product.Id
                    }
                }).ToList();

            return this.utilityService.GetCamelCaseJsonResult(cartItems);
        }

        [HttpPost]
        public virtual JsonResult ProductToCartItem(string productUrl, List<string> selectedVariantOptionUrls = null)
        {
            var cartItemDto = this.cmsService.ProductToCartItemDto(this.requestDbContext, productUrl, selectedVariantOptionUrls);

            return this.utilityService.GetCamelCaseJsonResult(cartItemDto);
        }

        [HttpPost]
        public virtual JsonResult AddToCart(CartItem cartItem, bool returnCart = false)
        {
            this.cmsService.AddToCart(this.requestDbContext, cartItem);

            if (returnCart)
            {
                var cartDto = this.cmsService.GetCartDto(this.requestDbContext);

                return this.utilityService.GetCamelCaseJsonResult(cartDto);
            }

            return null;
        }

        [HttpPost]
        public virtual JsonResult UpdateCartItem(CartItem cartItem, bool returnCart = false)
        {
            this.cmsService.UpdateCartItem(this.requestDbContext, cartItem);

            if (returnCart)
            {
                var cartDto = this.cmsService.GetCartDto(this.requestDbContext);

                return this.utilityService.GetCamelCaseJsonResult(cartDto);
            }

            return null;
        }

        [HttpPost]
        public virtual JsonResult UpdateCartItemQuantity(Guid cartItemId, int quantity, bool returnCart = false)
        {
            this.cmsService.UpdateCartItemQuantity(this.requestDbContext, cartItemId, quantity);

            if (returnCart)
            {
                var cartDto = this.cmsService.GetCartDto(this.requestDbContext);

                return this.utilityService.GetCamelCaseJsonResult(cartDto);
            }

            return null;
        }

        [HttpPost]
        public virtual void RemoveFromCart(Guid cartItemId)
        {
            this.cmsService.RemoveFromCart(this.requestDbContext, cartItemId);
        }

        [HttpPost]
        public virtual JsonResult ApplyCouponToCart(string couponCode)
        {
            var formResponse = this.cmsService.ApplyCouponToCart(this.requestDbContext, couponCode);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual void RemoveCouponFromCart()
        {
            this.cmsService.RemoveCouponFromCart(this.requestDbContext);
        }

        [HttpPost]
        public virtual JsonResult ApplyUserCreditToCart(double amount)
        {
            var formResponse = this.cmsService.ApplyUserCreditToCart(this.requestDbContext, Math.Abs(amount));

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult RemoveUserCreditFromCart()
        {
            var formResponse = this.cmsService.RemoveCreditFromCart(this.requestDbContext);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult SetCartShippingAddress(Guid? addressId)
        {
            var formResponse = this.cmsService.SetCartShippingAddress(this.requestDbContext, addressId);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }

        [HttpPost]
        public virtual JsonResult SetCartBillingAddress(Guid addressId)
        {
            var formResponse = this.cmsService.SetCartBillingAddress(this.requestDbContext, addressId);

            return this.utilityService.GetCamelCaseJsonResult(formResponse);
        }
    }
}