﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc
{
    using System.Web.Mvc;
    using SumoSoft.Cms.Mvc.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CmsLanguageFilter());
            filters.Add(new CmsAuthorizationFilter());
            filters.Add(new CmsExceptionFilter());
        }
    }
}