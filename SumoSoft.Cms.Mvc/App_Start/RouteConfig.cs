﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //---------------------------------------------------------------------------------------------------
            //https://blogs.msdn.microsoft.com/webdev/2013/10/17/attribute-routing-in-asp-net-mvc-5
            //---------------------------------------------------------------------------------------------------

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            routes.LowercaseUrls = true;

            routes.MapRoute(
                name: "Country",
                url: "{country}/{controller}/{action}/{url}",
                defaults: new { controller = "Home", action = "Index", url = UrlParameter.Optional },
                constraints: new { country = @"^[A-Za-z]{2}$" }
            );

            routes.MapRoute(
                name: "Default-Country",
                url: "{controller}/{action}/{url}",
                defaults: new { controller = "Home", action = "Index", url = UrlParameter.Optional }
            );
        }
    }
}