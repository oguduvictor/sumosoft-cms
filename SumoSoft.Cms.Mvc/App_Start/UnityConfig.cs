#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc
{
    using System.Web;
    using System.Web.Mvc;
    using FluentValidation;
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;
    using SumoSoft.Cms.AuthenticationServices;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.AuthenticationServices.Validators;
    using SumoSoft.Cms.AutoTagServices;
    using SumoSoft.Cms.AutoTagServices.Interfaces;
    using SumoSoft.Cms.ContentSectionServices;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.CountryServices;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Extensions.Services;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.EmailServices;
    using SumoSoft.Cms.EmailServices.Interfaces;
    using SumoSoft.Cms.LogServices;
    using SumoSoft.Cms.LogServices.Interfaces;
    using SumoSoft.Cms.MemoryCacheServices;
    using SumoSoft.Cms.MemoryCacheServices.Interfaces;
    using SumoSoft.Cms.Mvc.Areas.Admin.Dtos;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services;
    using SumoSoft.Cms.Mvc.Areas.Admin.Services.Interfaces;
    using SumoSoft.Cms.Mvc.Areas.Admin.Validators;
    using SumoSoft.Cms.RequestDbContextServices;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.Services;
    using SumoSoft.Cms.Services.Dto;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices;
    using SumoSoft.Cms.UtilityServices.Interfaces;
    using Unity;
    using Unity.Mvc5;

    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // ----------------------------------------------------------------------
            // Admin Services
            // ----------------------------------------------------------------------

            container.RegisterType<IAdminAccountService, AdminAccountService>();
            container.RegisterType<IAdminBlogCategoryService, AdminBlogCategoryService>();
            container.RegisterType<IAdminContentSectionService, AdminContentSectionService>();
            container.RegisterType<IAdminCountryService, AdminCountryService>();
            container.RegisterType<IAdminAttributeService, AdminAttributeService>();
            container.RegisterType<IAdminCartService, AdminCartService>();
            container.RegisterType<IAdminCategoryService, AdminCategoryService>();
            container.RegisterType<IAdminCouponService, AdminCouponService>();
            container.RegisterType<IAdminOrderService, AdminOrderService>();
            container.RegisterType<IAdminProductService, AdminProductService>();
            container.RegisterType<IAdminShippingBoxService, AdminShippingBoxService>();
            container.RegisterType<IAdminVariantService, AdminVariantService>();
            container.RegisterType<IAdminEmailService, AdminEmailService>();
            container.RegisterType<IAdminMailingListService, AdminMailingListService>();
            container.RegisterType<IAdminMediaService, AdminMediaService>();
            container.RegisterType<IAdminNotificationService, AdminNotificationService>();
            container.RegisterType<IAdminPostsService, AdminPostsService>();
            container.RegisterType<IAdminSeoService, AdminSeoService>();
            container.RegisterType<IAdminSettingService, AdminSettingService>();
            container.RegisterType<IAdminUserService, AdminUserService>();
            container.RegisterType<IAdminUtilityService, AdminUtilityService>();

            // ----------------------------------------------------------------------
            // DTO Validators
            // ----------------------------------------------------------------------

            container.RegisterType<IValidator<AddressDto>, AddressDtoValidator>();
            container.RegisterType<IValidator<AttributeDto>, AttributeDtoValidator>();
            container.RegisterType<IValidator<AttributeLocalizedKitDto>, AttributeLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<AttributeOptionLocalizedKitDto>, AttributeOptionLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<AuthenticateUserDto>, AuthenticateUserValidator>();
            container.RegisterType<IValidator<BatchRenameFilesDto>, BatchRenameFilesDtoValidator>();
            container.RegisterType<IValidator<BlogCategoryDto>, BlogCategoryDtoValidator>();
            container.RegisterType<IValidator<BlogCategoryLocalizedKitDto>, BlogCategoryLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<BlogPostDto>, BlogPostDtoValidator>();
            container.RegisterType<IValidator<CmsSettingsDto>, CmsSettingsDtoValidator>();
            container.RegisterType<IValidator<ContentSectionDto>, ContentSectionDtoValidator>();
            container.RegisterType<IValidator<ContentSectionLocalizedKitDto>, ContentSectionLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<CountryDto>, CountryDtoValidator>();
            container.RegisterType<IValidator<DirectoryDto>, DirectoryDtoValidator>();
            container.RegisterType<IValidator<CategoryDto>, CategoryDtoValidator>();
            container.RegisterType<IValidator<CategoryLocalizedKitDto>, CategoryLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<CouponDto>, CouponValidator>();
            container.RegisterType<IValidator<OrderDto>, OrderDtoValidator>();
            container.RegisterType<IValidator<OrderItemDto>, OrderItemDtoValidator>();
            container.RegisterType<IValidator<OrderItemVariantDto>, OrderItemVariantDtoValidator>();
            container.RegisterType<IValidator<OrderShippingBoxDto>, OrderShippingBoxDtoValidator>();
            container.RegisterType<IValidator<ProductAttributeDto>, ProductAttributeDtoValidator>();
            container.RegisterType<IValidator<ProductDto>, ProductDtoValidator>();
            container.RegisterType<IValidator<ProductImageDto>, ProductImageDtoValidator>();
            container.RegisterType<IValidator<ProductImageKitDto>, ProductImageKitDtoValidator>();
            container.RegisterType<IValidator<ProductLocalizedKitDto>, ProductLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<ProductStockUnitDto>, ProductStockUnitDtoValidator>();
            container.RegisterType<IValidator<ProductStockUnitLocalizedKitDto>, ProductStockUnitLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<ProductVariantDto>, ProductVariantDtoValidator>();
            container.RegisterType<IValidator<ShippingBoxDto>, ShippingBoxDtoValidator>();
            container.RegisterType<IValidator<ShippingBoxLocalizedKitDto>, ShippingBoxLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<TaxDto>, TaxDtoValidator>();
            container.RegisterType<IValidator<VariantDto>, VariantDtoValidator>();
            container.RegisterType<IValidator<VariantLocalizedKitDto>, VariantLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<VariantOptionDto>, VariantOptionDtoValidator>();
            container.RegisterType<IValidator<VariantOptionLocalizedKitDto>, VariantOptionLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<EditContentSectionsDto>, EditContentSectionsDtoValidator>();
            container.RegisterType<IValidator<EditCouponDto>, EditCouponDtoValidator>();
            container.RegisterType<IValidator<EditProductAttributesDto>, EditProductAttributesDtoValidator>();
            container.RegisterType<IValidator<EditProductImageKitsDto>, EditProductImageKitsDtoValidator>();
            container.RegisterType<IValidator<EditProductStockUnitsDto>, EditProductStockUnitsDtoValidator>();
            container.RegisterType<IValidator<EditProductVariantsDto>, EditProductVariantsDtoValidator>();
            container.RegisterType<IValidator<EmailDto>, EmailDtoValidator>();
            container.RegisterType<IValidator<EmailLocalizedKitDto>, EmailLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<HttpFileCollectionBase>, ImportProductsFileValidator>();
            container.RegisterType<IValidator<MailingListDto>, MailingListDtoValidator>();
            container.RegisterType<IValidator<MailingListLocalizedKitDto>, MailingListLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<RenameFileDto>, RenameFileDtoValidator>();
            container.RegisterType<IValidator<ResizeFileDto>, ResizeFileDtoValidator>();
            container.RegisterType<IValidator<SendEmailConfirmationModalDto>, SendEmailConfirmationModalDtoValidator>();
            container.RegisterType<IValidator<SeoSectionDto>, SeoSectionDtoValidator>();
            container.RegisterType<IValidator<SeoSectionLocalizedKitDto>, SeoSectionLocalizedKitDtoValidator>();
            container.RegisterType<IValidator<UserDto>, UserDtoValidator>();
            container.RegisterType<IValidator<UserRoleDto>, UserRoleDtoValidator>();

            // ----------------------------------------------------------------------
            // Domain Extension Services
            // ----------------------------------------------------------------------

            container.RegisterType<IAttributeExtensionService, AttributeExtensionService>();
            container.RegisterType<IAttributeOptionExtensionService, AttributeOptionExtensionService>();
            container.RegisterType<IBlogCategoryExtensionService, BlogCategoryExtensionService>();
            container.RegisterType<IBlogPostExtensionService, BlogPostExtensionService>();
            container.RegisterType<IContentSectionExtensionService, ContentSectionExtensionService>();
            container.RegisterType<ICountryExtensionService, CountryExtensionService>();
            container.RegisterType<IDoubleExtensionService, DoubleExtensionService>();
            container.RegisterType<ICartExtensionService, CartExtensionService>();
            container.RegisterType<ICartItemExtensionService, CartItemExtensionService>();
            container.RegisterType<ICartItemVariantExtensionService, CartItemVariantExtensionService>();
            container.RegisterType<ICartShippingBoxExtensionService, CartShippingBoxExtensionService>();
            container.RegisterType<ICategoryExtensionService, CategoryExtensionService>();
            container.RegisterType<IOrderItemExtensionService, OrderItemExtensionService>();
            container.RegisterType<IOrderItemVariantExtensionService, OrderItemVariantExtensionService>();
            container.RegisterType<IProductExtensionService, ProductExtensionService>();
            container.RegisterType<IProductImageKitExtensionService, ProductImageKitExtensionService>();
            container.RegisterType<IProductStockUnitExtensionService, ProductStockUnitExtensionService>();
            container.RegisterType<IProductVariantExtensionService, ProductVariantExtensionService>();
            container.RegisterType<IShippingBoxExtensionService, ShippingBoxExtensionService>();
            container.RegisterType<IVariantExtensionService, VariantExtensionService>();
            container.RegisterType<IVariantOptionExtensionService, VariantOptionExtensionService>();
            container.RegisterType<IEmailExtensionService, EmailExtensionService>();
            container.RegisterType<IIEnumerableExtensionService, IEnumerableExtensionService>();
            container.RegisterType<IIListExtensionService, IListExtensionService>();
            container.RegisterType<IIOrderedEnumerableExtensionService, IOrderedEnumerableExtensionService>();
            container.RegisterType<IITaggableExtensionService, ITaggableExtensionService>();
            container.RegisterType<IMailingListExtensionService, MailingListExtensionService>();
            container.RegisterType<ISeoSectionExtensionService, SeoSectionExtensionService>();
            container.RegisterType<IUserExtensionService, UserExtensionService>();

            // ----------------------------------------------------------------------
            // Generic Services
            // ----------------------------------------------------------------------

            container.RegisterType<IAuthenticationService, AuthenticationService>();
            container.RegisterType<IAzureStorageService, AzureStorageService>();
            container.RegisterType<IBitmapService, BitmapService>();
            container.RegisterType<ICmsService, CmsService>();
            container.RegisterType<IContentSectionService, ContentSectionService>();
            container.RegisterType<ICountryService, CountryService>();
            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<IGeolocalizationService, GeolocalizationService>();
            container.RegisterType<ILocalStorageService, LocalStorageService>();
            container.RegisterType<ILogService, LogService>();
            container.RegisterType<ICmsMemoryCacheService, CmsMemoryCacheService>();
            container.RegisterType<IRequestDbContextService, RequestDbContextService>();
            container.RegisterType<IUtilityService, UtilityService>();
            container.RegisterType<IAutoTagService, AutoTagService>();

            // ----------------------------------------------------------------------
            // SignalR Hubs
            // ----------------------------------------------------------------------

            GlobalHost.DependencyResolver.Register(typeof(IHubActivator), () => new UnityHubActivator(container));

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}