﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc
{
    using System.Web.WebPages;

    public class DisplayModesConfig
    {
        public static void RegisterDisplayModes()
        {
            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode("Override"));
        }
    }
}
