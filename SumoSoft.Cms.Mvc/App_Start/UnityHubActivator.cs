﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc
{
    using Microsoft.AspNet.SignalR.Hubs;
    using Unity;

    public class UnityHubActivator : IHubActivator
    {
        private readonly IUnityContainer container;

        public UnityHubActivator(IUnityContainer container)
        {
            this.container = container;
        }

        public IHub Create(HubDescriptor descriptor)
        {
            return (IHub)this.container.Resolve(descriptor.HubType);
        }
    }
}