﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc.Attributes
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;

    public class AuthorizeAdminAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            using (var dbContext = new CmsDbContext())
            {
                User user;

                try
                {
                    var userId = new Guid(HttpContext.Current.User.Identity.Name);

                    user = dbContext.Users.Find(userId);
                }
                catch (Exception)
                {
                    user = null;
                }

                return user?.Role?.AccessAdmin ?? false;
            }
        }
    }
}