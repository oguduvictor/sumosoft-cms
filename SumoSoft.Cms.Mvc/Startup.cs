﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Mvc
{
    using Owin;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // -----------------------------------------------------------------------------------------------
            // Map SignalR
            // -----------------------------------------------------------------------------------------------

            app.MapSignalR();
        }
    }
}