﻿namespace SumoSoft.Cms.LogServices.Interfaces
{
    using System.Threading.Tasks;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Enums;

    /// <summary>
    ///
    /// </summary>
    public interface ILogService
    {
        /// <summary>
        /// Logs a notification.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="type"></param>
        /// <param name="message"></param>
        /// <param name="details"></param>
        /// <param name="user"></param>
        void Log(CmsDbContext dbContext, LogTypesEnum type, string message, string details = null, User user = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <param name="details"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task LogAsync(LogTypesEnum type, string message, string details = null, User user = null);
    }
}