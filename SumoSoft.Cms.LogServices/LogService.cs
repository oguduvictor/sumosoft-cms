﻿namespace SumoSoft.Cms.LogServices
{
    using System.Threading.Tasks;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.LogServices.Interfaces;

    /// <inheritdoc />
    public class LogService : ILogService
    {
        /// <inheritdoc />
        public virtual void Log(CmsDbContext dbContext, LogTypesEnum type, string message, string details = null, User user = null)
        {
            dbContext.Logs.Add(CreateLog(type, message, details, user));

            dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public virtual async Task LogAsync(LogTypesEnum type, string message, string details = null, User user = null)
        {
            using (var threadSafeDbContext = new CmsDbContext())
            {
                threadSafeDbContext.Logs.Add(CreateLog(type, message, details, user));

                await threadSafeDbContext.SaveChangesAsync();
            }
        }

        private static Log CreateLog(LogTypesEnum type, string message, string details, User user)
        {
            return new Log
            {
                Type = type,
                Message = message,
                Details = details,
                User = user
            };
        }
    }
}
