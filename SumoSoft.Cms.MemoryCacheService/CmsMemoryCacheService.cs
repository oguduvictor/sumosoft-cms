﻿namespace SumoSoft.Cms.MemoryCacheServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Caching;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.MemoryCacheServices.Interfaces;

    /// <inheritdoc />
    public class CmsMemoryCacheService : ICmsMemoryCacheService
    {
        /// <inheritdoc />
        public virtual int RemoveAllWhere(Func<KeyValuePair<string, object>, bool> clause)
        {
            var memoryCacheDefault = MemoryCache.Default;

            var cacheEntries = memoryCacheDefault.Where(clause).ToList();

            foreach (var keyValuePair in cacheEntries)
            {
                memoryCacheDefault.Remove(keyValuePair.Key);
            }

            return cacheEntries.Count;
        }

        /// <inheritdoc />
        public virtual T Get<T>(string cacheKey, IReadOnlyCollection<BaseEntity> dependencies)
        {
            this.RemoveAllWhere(x => x.Key == null);

            var memoryCacheWrapper = GetMemoryCacheWrapper<T>(cacheKey, dependencies);

            if (memoryCacheWrapper == null || !memoryCacheWrapper.IsValid)
            {
                return default;
            }

            return memoryCacheWrapper.Value;
        }

        /// <inheritdoc />
        public virtual void Set<T>(string cacheKey, T value, DateTimeOffset absoluteExpiration, IReadOnlyCollection<BaseEntity> dependencies)
        {
            dependencies = dependencies ?? new List<BaseEntity>();
            dependencies = dependencies.Where(x => x != null).ToList();

            var memoryCacheWrapper = new MemoryCacheWrapper<T>
            {
                CreatedDate = DateTime.Now,
                Dependencies = dependencies.Select(x => x.Id).ToList(),
                Value = value
            };

            MemoryCache.Default.Set(cacheKey, memoryCacheWrapper, absoluteExpiration);
        }

        /// <inheritdoc />
        public virtual T GetOrSet<T>(string cacheKey, DateTimeOffset absoluteExpiration, IReadOnlyCollection<BaseEntity> dependencies, Func<T> setter)
        {
            dependencies = dependencies ?? new List<BaseEntity>();
            dependencies = dependencies.Where(x => x != null).ToList();

            var memoryCacheWrapper = GetMemoryCacheWrapper<T>(cacheKey, dependencies);

            if (memoryCacheWrapper == null || !memoryCacheWrapper.IsValid)
            {
                var value = setter();

                this.Set(cacheKey, value, absoluteExpiration, dependencies);

                return value;
            }

            return memoryCacheWrapper.Value;
        }

        [CanBeNull]
        private static MemoryCacheWrapper<T> GetMemoryCacheWrapper<T>(string cacheKey, IReadOnlyCollection<BaseEntity> dependencies)
        {
            dependencies = dependencies ?? new List<BaseEntity>();
            dependencies = dependencies.Where(x => x != null).ToList();

            var cacheResult = (MemoryCacheWrapper<T>)MemoryCache.Default.Get(cacheKey);

            if (cacheResult == null)
            {
                return null;
            }

            if (dependencies.Count != cacheResult.Dependencies.Count)
            {
                cacheResult.IsValid = false;

                return cacheResult;
            }

            if (!dependencies.Select(x => x.Id).ContainsAll(cacheResult.Dependencies))
            {
                cacheResult.IsValid = false;

                return cacheResult;
            }

            if (dependencies.Any(x => x.ModifiedDate > cacheResult.CreatedDate))
            {
                cacheResult.IsValid = false;

                return cacheResult;
            }

            return cacheResult;
        }

        private class MemoryCacheWrapper<T>
        {
            public DateTime CreatedDate { get; set; }

            public List<Guid> Dependencies { get; set; } = new List<Guid>();

            public T Value { get; set; }

            public bool IsValid { get; set; } = true;
        }
    }
}
