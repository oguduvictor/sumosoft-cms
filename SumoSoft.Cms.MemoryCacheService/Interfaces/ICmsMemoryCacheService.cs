﻿namespace SumoSoft.Cms.MemoryCacheServices.Interfaces
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain;

    /// <summary>
    ///
    /// </summary>
    public interface ICmsMemoryCacheService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="clause"></param>
        int RemoveAllWhere(Func<KeyValuePair<string, object>, bool> clause);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="dependencies"></param>
        /// <returns></returns>
        [CanBeNull]
        T Get<T>(string cacheKey, IReadOnlyCollection<BaseEntity> dependencies);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="dependencies"></param>
        void Set<T>(string cacheKey, T value, DateTimeOffset absoluteExpiration, IReadOnlyCollection<BaseEntity> dependencies);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="dependencies"></param>
        /// <param name="setter"></param>
        /// <returns></returns>
        [NotNull]
        T GetOrSet<T>(string cacheKey, DateTimeOffset absoluteExpiration, IReadOnlyCollection<BaseEntity> dependencies, Func<T> setter);
    }
}