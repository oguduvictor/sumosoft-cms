﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SumoSoft.Cms.GeoIp.Tests.Fakes.Seed;

namespace SumoSoft.Cms.GeoIp.Tests
{
    [TestClass]
    public class GeoIpTest
    {
        public List<string> Address { get; set; }

        public GeoIpTest()
        {
            Address = FakeCountryData.SeedAddress();
        }

        [TestMethod]
        public void GetCountry_NullIpAddress()
        {
            // Arrange
            var countryService = new GeoIpCountryService();

            // Act
            var result = countryService.GetInfoByIp("");

            // Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetCountry_InvalidIpAddress()
        {
            // Arrange
            var countryService = new GeoIpCountryService();

            // Act
            var result = countryService.GetInfoByIp("Invalid");

            // Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetCountry_ValidIpAddress()
        {
            // Arrange
            var countryService = new GeoIpCountryService();

            // Act
            var random = new Random();
            var result = countryService.GetInfoByIp(Address[random.Next(Address.Count)]);

            // Assert
            Assert.IsNotNull(result);
        }
    }
}