﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public class CountryList
    {
        public static List<string> GetNames()
        {
            return new List<string> {
            // ----------------------------------------------------
            // Africa
            // ----------------------------------------------------

            "Algeria",
            "Angola",
            "Benin",
            "Botswana",
            "British Indian Ocean Territory",
            "Burkina Faso",
            "Burundi",
            "Cameroon",
            "Cape Verde",
            "Central African Republic",
            "Chad",
            "Comoros",
            "Congo",
            "Congo (Democratic Republic of the)",
            "Côte d`Ivoire",
            "Djibouti",
            "Egypt",
            "Equatorial Guinea",
            "Eritrea",
            "Ethiopia",
            "Gabon",
            "Gambia",
            "Ghana",
            "Guinea",
            "Guinea-Bissau",
            "Kenya",
            "Lesotho",
            "Liberia",
            "Libyan Arab Jamahiriya",
            "Madagascar",
            "Malawi",
            "Mali",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Morocco",
            "Mozambique",
            "Namibia",
            "Niger",
            "Nigeria",
            "Réunion",
            "Rwanda",
            "Saint Helena",
            "São Tomé and Príncipe",
            "Senegal",
            "Seychelles",
            "Sierra Leone",
            "Somalia",
            "South Africa",
            "Sudan",
            "Swaziland",
            "Tanzania(United Republic of )",
            "Togo",
            "Tunisia",
            "Uganda",
            "Western Sahara",
            "Zambia",
            "Zimbabwe",

            // ----------------------------------------------------
            // Asia
            // ----------------------------------------------------

            "Afghanistan",
            "Armenia",
            "Azerbaijan",
            "Bahrain",
            "Bangladesh",
            "Bhutan",
            "Brunei Darussalam",
            "Cambodia",
            "China (People`s Republic of )",
            "Chinese Taipei",
            "Cyprus",
            "Georgia",
            "Hong Kong",
            "China",
            "India",
            "Indonesia",
            "Iraq",
            "Islamic Republic of Iran",
            "Israel",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Korea (Democratic People`s Republic of)",
            "Kuwait",
            "Kyrgyzstan",
            "Lao People`s Democratic Republic",
            "Lebanon",
            "Macau",
            "Malaysia",
            "Maldives",
            "Mongolia",
            "Myanmar",
            "Nepal",
            "Oman",
            "Pakistan",
            "Palestine",
            "Philippines",
            "Qatar",
            "Republic of Korea",
            "Saudi Arabia",
            "Singapore",
            "Sri Lanka",
            "Syrian Arab Republic",
            "Tajikistan",
            "Thailand",
            "Timor-Leste",
            "Turkey",
            "Turkmenistan",
            "United Arab Emirates",
            "Uzbekistan",
            "Vietnam",
            "Yemen",

            // ----------------------------------------------------
            // Oceania
            // ----------------------------------------------------

            "American Samoa",
            "Ashmore and Cartier Islands",
            "Australia",
            "Christmas Island",
            "Cocos (Keeling) Islands",
            "Cook Islands",
            "Federated States of Micronesia",
            "Fiji",
            "French Polynesia",
            "Guam",
            "Kiribati",
            "Marshall Islands",
            "Nauru",
            "New Caledonia",
            "New Zealand",
            "Niue",
            "Norfolk Island",
            "Northern Mariana Islands",
            "Palau",
            "Pitcairn Islands",
            "Samoa",
            "Tokelau",
            "Tonga",
            "Tuvalu",
            "Vanuatu",
            "Wallis and Futuna",

            // ----------------------------------------------------
            // Antartica
            // ----------------------------------------------------

            "Antarctica",
            "Bouvet Island",
            "French Southern Territories",
            "Heard Island and McDonald Islands",
            "South Georgia and the South Sandwich Islands",

            // ----------------------------------------------------
            // Europe
            // ----------------------------------------------------

            "Åland Islands",
            "Albania",
            "Andorra",
            "Austria",
            "Belarus",
            "Belgium",
            "Bosnia and Herzegovina",
            "Bulgaria",
            "Croatia",
            "Czech Republic",
            "Denmark",
            "Estonia",
            "Faroe Islands",
            "Finland",
            "France",
            "Germany",
            "Gibraltar",
            "Greece",
            "Guernsey",
            "Hungary",
            "Iceland",
            "Ireland",
            "Isle of Man",
            "Italy",
            "Jersey",
            "Kosovo",
            "Latvia",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Moldova (Republic of)",
            "Monaco",
            "Montenegro",
            "Netherlands",
            "Norway",
            "Poland",
            "Portugal",
            "Republic of Macedonia",
            "Romania",
            "Russian Federation",
            "San Marino",
            "Serbia",
            "Slovakia",
            "Slovenia",
            "Spain",
            "Svalbard and Jan Mayen",
            "Sweden",
            "Switzerland",
            "United Kingdom",
            "Ukraine",
            "Vatican City",

            // ----------------------------------------------------
            // America
            // ----------------------------------------------------

            "Anguilla",
            "Antigua and Barbuda",
            "Argentina",
            "Aruba",
            "Bahamas",
            "Barbados",
            "Belize",
            "Bermuda",
            "Bolivia",
            "Brazil",
            "British Virgin Islands",
            "Canada",
            "Cayman Islands",
            "Chile",
            "Colombia",
            "Costa Rica",
            "Cuba",
            "Dominica",
            "Dominican Republic",
            "Ecuador",
            "El Salvador",
            "Falkland Islands (Malvinas)",
            "French Guiana",
            "Greenland",
            "Grenada",
            "Guadeloupe",
            "Guatemala",
            "Guyana",
            "Haiti",
            "Honduras",
            "Jamaica",
            "Martinique",
            "Mexico",
            "Montserrat",
            "Netherlands Antilles",
            "Nicaragua",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Puerto Rico",
            "Saint Barthélemy",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Martin",
            "Saint Pierre and Miquelon",
            "Saint Vincent and the Grenadines",
            "Solomon Islands",
            "Suriname",
            "Trinidad and Tobago",
            "Turks and Caicos Islands",
            "United States Minor Outlying Islands",
            "United States",
            "Uruguay",
            "Venezuela",
            "Virgin Islands"
        };
        }

        public static List<Country> GetCountries()
        {
            var allCountries = new List<Country>();

            var cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            foreach (var cultureInfo in cultures)
            {
                // Exclude regions with non-standard language code

                if (cultureInfo.Name.Any(char.IsDigit))
                {
                    continue;
                }

                try
                {
                    var regionInfo = new RegionInfo(cultureInfo.LCID);

                    allCountries.Add(new Country
                    {
                        Name = regionInfo.EnglishName,
                        LanguageCode = cultureInfo.Name,
                        FlagIcon = "flag-icon-" + regionInfo.TwoLetterISORegionName.ToLower(),
                        Url = regionInfo.TwoLetterISORegionName.ToLower()
                    });
                }
                catch (Exception)
                {
                    // ignore
                }
            }

            var countries = new List<Country>();

            var groups = allCountries.GroupBy(x => x.Name);

            foreach (var group in groups)
            {
                var defaultCountry = group.First();

                // ---------------------------------------------
                // Property corrections:
                // ---------------------------------------------

                switch (group.Key)
                {
                    case "United Kingdom":
                        defaultCountry = group.First(x => x.LanguageCode == "en-GB");
                        defaultCountry.IsDefault = true;
                        defaultCountry.Url = string.Empty;
                        defaultCountry.Localize = true;
                        break;
                    case "United States":
                        defaultCountry = group.First(x => x.LanguageCode == "en-US");
                        break;
                }

                countries.Add(defaultCountry);
            }

            return countries.OrderBy(x => x.Name).ToList();
        }
    }
}