﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Constants
{
    public class ContentSectionName
    {
        public const string Cms_CartErrors = "Cms_CartErrors";

        public const string Cms_CategoryTypesEnum_Products = "Cms_CategoryTypesEnum_Products";
        public const string Cms_CategoryTypesEnum_Products_FallbackValue = "Products";

        public const string Cms_CategoryTypesEnum_ProductImageKits = "Cms_CategoryTypesEnum_ProductImageKits";
        public const string Cms_CategoryTypesEnum_ProductImageKits_FallbackValue = "Product Image Kits";

        public const string Cms_CategoryTypesEnum_ProductStockUnits = "Cms_CategoryTypesEnum_ProductStockUnits";
        public const string Cms_CategoryTypesEnum_ProductStockUnits_FallbackValue = "Product Stock Units";

        public const string Cms_Register_AlreadyRegisteredWarning = "Cms_Register_AlreadyRegisteredWarning";
        public const string Cms_Register_AlreadyRegisteredWarning_FallbackValue = "This email has already been registered.";

        public const string Cms_Register_WeakPasswordWarning = "Cms_Register_WeakPasswordWarning";
        public const string Cms_Register_WeakPasswordWarning_FallbackValue = "This password is too weak.";

        public const string Cms_Login_AccountLockedWarning = "Cms_Login_AccountLockedWarning";
        public const string Cms_Login_AccountLockedWarning_FallbackValue = "Maximum number of login attempts reached. Please wait for 10 minutes before trying again.";

        public const string Cms_Login_AccountNotFoundWarning = "Cms_Login_AccountNotFoundWarning";
        public const string Cms_Login_AccountNotFoundWarning_FallbackValue = "This email is not associated to an account.";

        public const string Cms_Login_WrongCredentialsWarning = "Cms_Login_WrongCredentialsWarning";
        public const string Cms_Login_WrongCredentialsWarning_FallbackValue = "Wrong email or password.";

        public const string Cms_Admin_DuplicatedLocalizedKitsFound = "Cms_Admin_DuplicatedLocalizedKitsFound";
        public const string Cms_Admin_DuplicatedLocalizedKitsFound_FallbackValue = "Multiple Localized Kits associated to the same Country are not allowed";

        public const string Cms_Admin_DuplicatedVariantOptionName = "Cms_Admin_DuplicatedVariantOptionName";
        public const string Cms_Admin_DuplicatedVariantOptionName_FallbackValue = "Variant Option Names must be unique.";

        public const string Cms_Admin_DuplicatedVariantOptionUrl = "Cms_Admin_DuplicatedVariantOptionUrl";
        public const string Cms_Admin_DuplicatedVariantOptionUrl_FallbackValue = "Variant Option Url must be unique.";

        public const string Cms_UserCredit_InsufficientFundsError = "Cms_UserCredit_InsufficientFundsError";
        public const string Cms_UserCredit_InsufficientFundsError_FallbackValue = "Insufficient Funds.";

        public const string Cms_UserCredit_InvalidAmountError = "Cms_UserCredit_InvalidAmountError";
        public const string Cms_UserCredit_InvalidAmountError_FallbackValue = "Amount must be greater than 0.";

        public const string Cms_UserCredit_ExcessiveAmountError = "Cms_UserCredit_ExcessiveAmountError";
        public const string Cms_UserCredit_ExcessiveAmountError_FallbackValue = "The amount exceeds the Cart total.";
    }
}