﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Constants
{
    using System;

    public class SharedConstants
    {
        // Cookies
        public const string PartiallyAuthenticatedUserId = "PartiallyAuthenticatedUserId";

        // Website
        public const string FlagIconsPath = "~/Areas/Admin/Content/flags/4x3/";
        public const string FlagIconClass = "flag-icon-";

        // Default User Roles
        public const string DefaultRoleSystemAdmin = "System Administrator";
        public const string DefaultRoleAdmin = "Administrator";
        public const string DefaultUserRole = "User";

        // Db
        public const string DbConnectionString = "DbConnectionString";

        // Media
        public const string BackupsFolderName = "_backups";
        public const string UploadFolder = "uploads";
        public const string UploadPath = "~/Content/" + UploadFolder;
        public const string UploadPathIndex = "Content\\" + UploadFolder;
        public static readonly string[] ValidMediaExtensions = { ".tif", ".tiff", ".jpg", ".jpeg", ".txt", ".png", ".gif", ".pdf", ".svg", ".mp4" };

        // Coupons
        public const string CouponCodeAlphanumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public const int CouponCodeLenght = 6;

        public const string UnchangedStringValue = "EBD2F6A5-908A-4219-8BF6-4B62AD9656E7";
        public static readonly Guid UnchangedGuidValue = new Guid("EBD2F6A5-908A-4219-8BF6-4B62AD9656E7");
    }
}
