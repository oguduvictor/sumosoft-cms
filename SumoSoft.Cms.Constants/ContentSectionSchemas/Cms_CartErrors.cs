﻿namespace SumoSoft.Cms.Constants.ContentSectionSchemas
{
#pragma warning disable 1591

    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using JetBrains.Annotations;

    [Description("Use this ContentSection customize the messages returned by the Cart.GetErrors() method")]
    public class Cms_CartErrors
    {
        [CanBeNull]
        [Display(
            Name = "CartItem With Disabled Product",
            Description = "Use the tag [[Product]] to insert the name of the CartItem Product")]
        public string CartItemWithDisabledProduct { get; set; }

        [CanBeNull]
        [Display(
            Name = "CartItem With Out Of Stock Product",
            Description = "Use the tag [[Product]] to insert the name of the CartItem Product")]
        public string CartItemWithOutOfStockProduct { get; set; }

        [CanBeNull]
        [Display(
            Name = "CartItem With Invalid ShippingBox",
            Description = "Use the tag [[Product]] to insert the name of the CartItem Product")]
        public string CartItemWithInvalidShippingBox { get; set; }

        [CanBeNull]
        [Display(
            Name = "Invalid Coupon",
            Description = "")]
        public string InvalidCoupon { get; set; }

        [CanBeNull]
        [Display(
            Name = "Invalid User Credit",
            Description = "")]
        public string InvalidUserCredit { get; set; }

        [CanBeNull]
        [Display(
            Name = "Invalid Tax Override",
            Description = "")]
        public string InvalidTaxOverride { get; set; }

        [CanBeNull]
        [Display(
            Name = "Localized Shipping Address Country Not Matching Current Country",
            Description = "")]
        public string LocalizedShippingAddressCountryNotMatchingCurrentCountry { get; set; }

        [CanBeNull]
        [Display(
            Name = "Non Localized Shipping Address Country Not Matching Default Country",
            Description = "")]
        public string NonLocalizedShippingAddressCountryNotMatchingDefaultCountry { get; set; }

        [CanBeNull]
        [Display(
            Name = "Shipping Address Required",
            Description = "")]
        public string ShippingAddressRequired { get; set; }
    }
}
