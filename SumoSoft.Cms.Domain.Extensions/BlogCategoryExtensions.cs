﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class BlogCategoryExtensions
    {
        private static IBlogCategoryExtensionService _blogCategoryExtensionService => DependencyResolver.Current.GetService<IBlogCategoryExtensionService>();

        public static string GetLocalizedTitle(this BlogCategory blogCategory, string languageCode = null)
        {
            return _blogCategoryExtensionService.GetLocalizedTitle(blogCategory, languageCode);
        }

        public static string GetLocalizedDescription(this BlogCategory blogCategory, string languageCode = null)
        {
            return _blogCategoryExtensionService.GetLocalizedDescription(blogCategory, languageCode);
        }
    }
}