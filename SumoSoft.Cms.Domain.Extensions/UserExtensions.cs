﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class UserExtensions
    {
        private static IUserExtensionService _userExtensionService => DependencyResolver.Current.GetService<IUserExtensionService>();

        [NotNull]
        public static ICollection<UserCredit> GetLocalizedUserCredits(this User user, string languageCode = null)
        {
            return _userExtensionService.GetLocalizedUserCredits(user, languageCode);
        }

        /// <summary>
        /// Gets a UserAttribute by its Name.
        /// </summary>
        [CanBeNull]
        public static UserAttribute GetUserAttributeByName(this User user, string attributeName)
        {
            return _userExtensionService.GetUserAttributeByName(user, attributeName);
        }
    }
}