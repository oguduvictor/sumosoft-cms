﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class ContentSectionExtensions
    {
        private static IContentSectionExtensionService _contentSectionExtensionService => DependencyResolver.Current.GetService<IContentSectionExtensionService>();

        /// <summary>
        ///  Returns the localized Content or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedContent(this ContentSection contentSection, string languageCode = null)
        {
            return _contentSectionExtensionService.GetLocalizedContent(contentSection, languageCode);
        }
    }
}