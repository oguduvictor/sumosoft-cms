﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class CartItemExtension
    {
        private static ICartItemExtensionService _cartItemExtensionService => DependencyResolver.Current.GetService<ICartItemExtensionService>();

        /// <summary>
        /// Gets the CartItemProductVariant by its Variant Name.
        /// </summary>
        [CanBeNull]
        public static CartItemVariant GetCartItemVariantByName(this CartItem cartItem, string variantName)
        {
            return _cartItemExtensionService.GetCartItemVariantByName(cartItem, variantName);
        }

        [CanBeNull]
        public static ProductAttribute GetProductAttributeByName(this CartItem cartItem, string attributeName)
        {
            return _cartItemExtensionService.GetProductAttributeByName(cartItem, attributeName);
        }

        [CanBeNull]
        public static ProductVariant GetProductVariantByName(this CartItem cartItem, string variantName)
        {
            return _cartItemExtensionService.GetProductVariantByName(cartItem, variantName);
        }

        public static List<VariantOption> GetSelectedVariantOptions(this CartItem cartItem)
        {
            return _cartItemExtensionService.GetSelectedVariantOptions(cartItem);
        }

        [CanBeNull]
        public static ProductImageKit GetProductImageKit(this CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            return _cartItemExtensionService.GetProductImageKit(cartItem, selectedVariantOptions);
        }

        [CanBeNull]
        public static ProductStockUnit GetProductStockUnit(this CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            return _cartItemExtensionService.GetProductStockUnit(cartItem, selectedVariantOptions);
        }

        public static int GetStock(this CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            return _cartItemExtensionService.GetStock(cartItem, selectedVariantOptions);
        }

        /// <summary>
        /// Evaluate if the current ProductStockUnit has 0 Stock and Preorder is not enabled.
        /// The method returns false if no ProductStockUnit exists.
        /// </summary>
        public static bool IsOutOfStock(this CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            return _cartItemExtensionService.IsOutOfStock(cartItem, selectedVariantOptions);
        }

        public static DateTime GetMinEta(this CartItem cartItem, string languageCode = null)
        {
            return _cartItemExtensionService.GetMinEta(cartItem, languageCode);
        }

        public static DateTime GetMaxEta(this CartItem cartItem, string languageCode = null)
        {
            return _cartItemExtensionService.GetMaxEta(cartItem, languageCode);
        }

        /// <summary>
        /// Return the list of Taxes applied to the current CartItem.
        /// </summary>
        public static List<Tax> GetTaxes(this CartItem cartItem, IEnumerable<Tax> countryTaxes)
        {
            return _cartItemExtensionService.GetTaxes(cartItem, countryTaxes);
        }

        /// <summary>
        /// The Total of a CartItem before applying the Coupon and the UserCredit, before Tax.
        /// </summary>
        public static double GetSubtotalBeforeTax(this CartItem cartItem, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartItemExtensionService.GetSubtotalBeforeTax(dbContext, cartItem, languageCode);
        }

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, after Tax.
        /// </summary>
        public static double GetSubtotalAfterTax(this CartItem cartItem, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            return _cartItemExtensionService.GetSubtotalAfterTax(dbContext, cartItem, countryTaxes, languageCode);
        }

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, without taking into account the "Sale at price", before Tax.
        /// </summary>
        public static double GetSubtotalBeforeTaxWithoutSalePrice(this CartItem cartItem, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartItemExtensionService.GetSubtotalBeforeTaxWithoutSalePrice(dbContext, cartItem, languageCode);
        }

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, without taking into account the "Sale at price", after Tax.
        /// </summary>
        public static double GetSubtotalAfterTaxWithoutSalePrice(this CartItem cartItem, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            return _cartItemExtensionService.GetSubtotalAfterTaxWithoutSalePrice(dbContext, cartItem, countryTaxes, languageCode);
        }

        public static bool CouponApplies(this CartItem cartItem, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartItemExtensionService.CouponApplies(dbContext, cartItem, languageCode);
        }

        /// <summary>
        /// The share of the Coupon applied to the current item, calculated on the Subtotal before tax.
        /// </summary>
        public static double GetCouponShareValue(this CartItem cartItem, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartItemExtensionService.GetCouponShareValue(dbContext, cartItem, languageCode);
        }

        /// <summary>
        /// The total price of the CartItem before-tax.
        /// </summary>
        public static double GetTotalBeforeTax(this CartItem cartItem, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartItemExtensionService.GetTotalBeforeTax(dbContext, cartItem, languageCode);
        }

        /// <summary>
        /// The total price of the CartItem after-tax.
        /// </summary>
        public static double GetTotalAfterTax(this CartItem cartItem, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            return _cartItemExtensionService.GetTotalAfterTax(dbContext, cartItem, countryTaxes, languageCode);
        }

        public static double GetSubtotalAfterTax(this CartItem cartItem, CmsDbContext dbContext)
        {
            return _cartItemExtensionService.GetSubtotalAfterTax(dbContext, cartItem);
        }

        public static double GetSubtotalAfterTaxWithoutSalePrice(this CartItem cartItem, CmsDbContext dbContext)
        {
            return _cartItemExtensionService.GetSubtotalAfterTaxWithoutSalePrice(dbContext, cartItem);
        }

        public static double GetTotalAfterTax(this CartItem cartItem, CmsDbContext dbContext)
        {
            return _cartItemExtensionService.GetTotalAfterTax(dbContext, cartItem);
        }
    }
}