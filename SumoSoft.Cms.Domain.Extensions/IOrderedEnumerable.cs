﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class IOrderedEnumerable
    {
        private static IIOrderedEnumerableExtensionService _iOrderedEnumerableExtensionService => DependencyResolver.Current.GetService<IIOrderedEnumerableExtensionService>();

        public static IOrderedEnumerable<Product> ThenByCategoryNames(this IOrderedEnumerable<Product> products, List<string> categoryNames)
        {
            return _iOrderedEnumerableExtensionService.ThenByCategoryNames(products, categoryNames);
        }

        public static IOrderedEnumerable<ProductImageKit> ThenByProductCategoryNames(this IOrderedEnumerable<ProductImageKit> productImageKits, List<string> categoryNames)
        {
            return _iOrderedEnumerableExtensionService.ThenByProductCategoryNames(productImageKits, categoryNames);
        }

        public static IOrderedEnumerable<T> ThenByIds<T>(this IOrderedEnumerable<T> list, string ids) where T : BaseEntity
        {
            return _iOrderedEnumerableExtensionService.ThenByIds(list, ids);
        }
    }
}
