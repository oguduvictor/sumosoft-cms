﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class VariantExtensions
    {
        private static IVariantExtensionService _variantExtensionService => DependencyResolver.Current.GetService<IVariantExtensionService>();

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this Variant variant, string languageCode = null)
        {
            return _variantExtensionService.GetLocalizedTitle(variant, languageCode);
        }

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this Variant variant, string languageCode = null)
        {
            return _variantExtensionService.GetLocalizedDescription(variant, languageCode);
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this Variant variant, string variantOptionName)
        {
            return _variantExtensionService.GetVariantOptionByName(variant, variantOptionName);
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByUrl(this Variant variant, string variantOptionUrl)
        {
            return _variantExtensionService.GetVariantOptionByUrl(variant, variantOptionUrl);
        }
    }
}
