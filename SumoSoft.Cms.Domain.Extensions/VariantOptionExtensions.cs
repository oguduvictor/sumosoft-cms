﻿// ReSharper disable InheritdocConsiderUsage

#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public static class VariantOptionExtensions
    {
        private static IVariantOptionExtensionService _variantOptionExtensionService => DependencyResolver.Current.GetService<IVariantOptionExtensionService>();

        /// <summary>
        ///  Returns the localized IsDisabled property.
        /// </summary>
        public static bool GetLocalizedIsDisabled(this VariantOption variantOption, string languageCode = null)
        {
            return _variantOptionExtensionService.GetLocalizedIsDisabled(variantOption, languageCode);
        }

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this VariantOption variantOption, string languageCode = null)
        {
            return _variantOptionExtensionService.GetLocalizedTitle(variantOption, languageCode);
        }

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this VariantOption variantOption, string languageCode = null)
        {
            return _variantOptionExtensionService.GetLocalizedDescription(variantOption, languageCode);
        }

        /// <summary>
        ///  Returns the localized Title.
        /// </summary>
        public static double GetLocalizedPriceModifier(this VariantOption variantOption, string languageCode = null)
        {
            return _variantOptionExtensionService.GetLocalizedPriceModifier(variantOption, languageCode);
        }
    }
}