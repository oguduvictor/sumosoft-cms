﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Domain.Interfaces;

    public static class IListExtension
    {
        private static IIListExtensionService _iListExtensionService => DependencyResolver.Current.GetService<IIListExtensionService>();

        [CanBeNull]
        public static T GetByLanguageCode<T>(this IList<T> localizedKits, string languageCode = null) where T : BaseEntity, ILocalizedKit
        {
            return _iListExtensionService.GetByLanguageCode(localizedKits, languageCode);
        }

        [CanBeNull]
        public static T GetByDefaultCountry<T>(this IList<T> localizedKits) where T : BaseEntity, ILocalizedKit
        {
            return _iListExtensionService.GetByDefaultCountry(localizedKits);
        }

        /// <summary>
        /// Returns a List where the only item is the LocalizedKit of the Default Country.
        /// Use this method to create a smaller DTO.
        /// </summary>
        [NotNull]
        public static List<T> FilterByDefaultCountry<T>(this IList<T> localizedKits) where T : class, ILocalizedKit
        {
            return _iListExtensionService.FilterByDefaultCountry(localizedKits);
        }

        public static List<T> OrderByDefaultCountry<T>(this IList<T> localizedKits) where T : ILocalizedKit
        {
            return _iListExtensionService.OrderByDefaultCountry(localizedKits);
        }

        public static List<T> OrderByCurrentCountry<T>(this IList<T> localizedKits) where T : ILocalizedKit
        {
            return _iListExtensionService.OrderByCurrentCountry(localizedKits);
        }

        public static IOrderedEnumerable<T> OrderBySortOrder<T>(this IList<T> list) where T : ISortable
        {
            return _iListExtensionService.OrderBySortOrder(list);
        }

        /// <summary>
        /// Check if a List of Entities contains a certain item, based on its Id.
        /// </summary>
        public static bool ContainsById<T>(this IList<T> items, T item) where T : BaseEntity
        {
            return _iListExtensionService.ContainsById(items, item);
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IList<T> items, params T[] args) where T : BaseEntity
        {
            return _iListExtensionService.ContainsAllById(items, args);
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IList<T> items, List<T> args) where T : BaseEntity
        {
            return _iListExtensionService.ContainsAllById(items, args);
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IList<T> items, params T[] args) where T : BaseEntity
        {
            return _iListExtensionService.ContainsAnyById(items, args);
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IList<T> items, List<T> args) where T : BaseEntity
        {
            return _iListExtensionService.ContainsAnyById(items, args);
        }
    }
}