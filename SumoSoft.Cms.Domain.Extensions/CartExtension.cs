﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Dto;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class CartExtension
    {
        private static ICartExtensionService _cartExtensionService => DependencyResolver.Current.GetService<ICartExtensionService>();

        [CanBeNull]
        public static CartShippingBox GetCartShippingBoxByName(this Cart cart, string shippingBoxName)
        {
            return _cartExtensionService.GetCartShippingBoxByName(cart, shippingBoxName);
        }

        /// <summary>
        /// The coupon value, calculated on the net price of the Cart Items.
        /// </summary>
        public static double GetCouponValue(this Cart cart, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartExtensionService.GetCouponValue(dbContext, cart, languageCode);
        }

        /// <summary>
        /// The Total value of the Cart, before Tax.
        /// </summary>
        public static double GetTotalBeforeTax(this Cart cart, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartExtensionService.GetTotalBeforeTax(dbContext, cart, languageCode);
        }

        /// <summary>
        /// The Total value of the Cart, after Tax.
        /// </summary>
        public static double GetTotalAfterTax(this Cart cart, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            return _cartExtensionService.GetTotalAfterTax(dbContext, cart, countryTaxes, languageCode);
        }

        public static double GetTotalAfterTax(this Cart cart, CmsDbContext dbContext)
        {
            return _cartExtensionService.GetTotalAfterTax(dbContext, cart);
        }

        /// <summary>
        /// The TotalAfterTax minus the UserCredit Amount, if applied.
        /// </summary>
        public static double GetTotalToBePaid(this Cart cart, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            return _cartExtensionService.GetTotalToBePaid(dbContext, cart, countryTaxes, languageCode);
        }

        /// <summary>
        /// The TotalAfterTax minus the UserCredit Amount, if applied.
        /// </summary>
        public static double GetTotalToBePaid(this Cart cart, CmsDbContext dbContext)
        {
            return _cartExtensionService.GetTotalToBePaid(dbContext, cart);
        }

        public static List<CartError> GetErrors(this Cart cart, CmsDbContext dbContext)
        {
            return _cartExtensionService.GetErrors(dbContext, cart);
        }
    }
}