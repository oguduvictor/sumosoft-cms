﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class CartShippingBoxExtension
    {
        private static ICartShippingBoxExtensionService _cartShippingBoxExtensionService => DependencyResolver.Current.GetService<ICartShippingBoxExtensionService>();

        /// <summary>
        /// Get the Shipping price before-tax. If the sum of all the Cart Items in the cart is higher than the FreeShippingMinimumPrice, returns 0.
        /// </summary>
        public static double GetShippingPriceBeforeTax(this CartShippingBox cartShippingBox, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartShippingBoxExtensionService.GetShippingPriceBeforeTax(dbContext, cartShippingBox, languageCode);
        }

        /// <summary>
        /// Get the Shipping price after-tax.
        /// </summary>
        public static double GetShippingPriceAfterTax(this CartShippingBox cartShippingBox, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            return _cartShippingBoxExtensionService.GetShippingPriceAfterTax(dbContext, cartShippingBox, countryTaxes, languageCode);
        }

        public static double GetTotalBeforeTax(this CartShippingBox cartShippingBox, CmsDbContext dbContext, string languageCode = null)
        {
            return _cartShippingBoxExtensionService.GetTotalBeforeTax(dbContext, cartShippingBox, languageCode);
        }

        public static double GetTotalAfterTax(this CartShippingBox cartShippingBox, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            return _cartShippingBoxExtensionService.GetTotalAfterTax(dbContext, cartShippingBox, countryTaxes, languageCode);
        }

        public static double GetShippingPriceAfterTax(this CartShippingBox cartShippingBox, CmsDbContext dbContext)
        {
            return _cartShippingBoxExtensionService.GetShippingPriceAfterTax(dbContext, cartShippingBox);
        }

        public static double GetTotalAfterTax(this CartShippingBox cartShippingBox, CmsDbContext dbContext)
        {
            return _cartShippingBoxExtensionService.GetTotalAfterTax(dbContext, cartShippingBox);
        }
    }
}