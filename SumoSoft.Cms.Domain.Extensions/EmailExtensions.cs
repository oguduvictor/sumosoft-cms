﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public static class EmailExtensions
    {
        private static IEmailExtensionService _emailExtensionService => DependencyResolver.Current.GetService<IEmailExtensionService>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public static string GetLocalizedFrom(this Email email, string languageCode = null)
        {
            return _emailExtensionService.GetLocalizedFrom(email, languageCode);
        }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public static string GetLocalizedDisplayName(this Email email, string languageCode = null)
        {
            return _emailExtensionService.GetLocalizedDisplayName(email, languageCode);
        }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public static string GetLocalizedReplyTo(this Email email, string languageCode = null)
        {
            return _emailExtensionService.GetLocalizedReplyTo(email, languageCode);
        }
    }
}
