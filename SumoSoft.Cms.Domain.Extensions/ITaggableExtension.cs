﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Domain.Interfaces;

    public static class ITaggableExtension
    {
        private static IITaggableExtensionService _iTaggableExtensionService => DependencyResolver.Current.GetService<IITaggableExtensionService>();

        public static List<string> GetTagList(this ITaggable iTaggable)
        {
            return _iTaggableExtensionService.GetTagList(iTaggable);
        }

        public static bool HasTags(this ITaggable iTaggable, params string[] tags)
        {
            return _iTaggableExtensionService.HasTags(iTaggable, tags);
        }
    }
}
