﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Domain.Interfaces;

    public static class IEnumerableExtensions
    {
        private static IIEnumerableExtensionService _iEnumerableExtensionService => DependencyResolver.Current.GetService<IIEnumerableExtensionService>();

        /// <summary>
        /// Check if a List of Entities contains a certain item, based on its Id.
        /// </summary>
        public static bool ContainsById<T>(this IEnumerable<T> items, T item) where T : BaseEntity
        {
            return _iEnumerableExtensionService.ContainsById(items, item);
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IEnumerable<T> items, params T[] args) where T : BaseEntity
        {
            return _iEnumerableExtensionService.ContainsAnyById(items, args);
        }

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAnyById<T>(this IEnumerable<T> items, List<T> args) where T : BaseEntity
        {
            return _iEnumerableExtensionService.ContainsAnyById(items, args);
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IEnumerable<T> items, params T[] args) where T : BaseEntity
        {
            return _iEnumerableExtensionService.ContainsAllById(items, args);
        }

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on theirs Id.
        /// </summary>
        public static bool ContainsAllById<T>(this IEnumerable<T> items, List<T> args) where T : BaseEntity
        {
            return _iEnumerableExtensionService.ContainsAllById(items, args);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<T> OrderBySortOrder<T>(this IEnumerable<T> items) where T : ISortable
        {
            return _iEnumerableExtensionService.OrderBySortOrder(items);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="products"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<Product> OrderByCategoryNames(this IEnumerable<Product> products, List<string> categoryNames)
        {
            return _iEnumerableExtensionService.OrderByCategoryNames(products, categoryNames);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKits"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<ProductImageKit> OrderByProductCategoryNames(this IEnumerable<ProductImageKit> productImageKits, List<string> categoryNames)
        {
            return _iEnumerableExtensionService.OrderByProductCategoryNames(productImageKits, categoryNames);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="idList"></param>
        /// <returns></returns>
        public static IOrderedEnumerable<T> OrderByIds<T>(this IEnumerable<T> list, IReadOnlyList<Guid> idList) where T : BaseEntity
        {
            return _iEnumerableExtensionService.OrderByIds(list, idList);
        }
    }
}
