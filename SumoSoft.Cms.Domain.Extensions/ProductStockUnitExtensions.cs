﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class ProductStockUnitExtensions
    {
        private static IProductStockUnitExtensionService _productStockUnitExtensionService => DependencyResolver.Current.GetService<IProductStockUnitExtensionService>();

        public static bool HasCategories(this ProductStockUnit productStockUnit, params string[] categoryNames)
        {
            return _productStockUnitExtensionService.HasCategories(productStockUnit, categoryNames);
        }

        public static bool HasCategories(this ProductStockUnit productStockUnit, params Category[] categories)
        {
            return _productStockUnitExtensionService.HasCategories(productStockUnit, categories);
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this ProductStockUnit productStockUnit, string variantOptionName)
        {
            return _productStockUnitExtensionService.GetVariantOptionByName(productStockUnit, variantOptionName);
        }

        /// <summary>
        /// Returns whether all the VariantOptions of the current ProductStockUnit are
        /// included  in a superset of VariantOptions passed as parameter. Use this method
        /// to find which ProductStockUnit matches the selected options of a CartItem.
        /// </summary>
        public static bool Matches(this ProductStockUnit productStockUnit, List<VariantOption> selectedVariantOptions)
        {
            return _productStockUnitExtensionService.Matches(productStockUnit, selectedVariantOptions);
        }

        /// <summary>
        ///  Returns the localized BasePrice.
        /// </summary>
        public static double GetLocalizedBasePrice(this ProductStockUnit productStockUnit, string languageCode = null)
        {
            return _productStockUnitExtensionService.GetLocalizedBasePrice(productStockUnit, languageCode);
        }

        /// <summary>
        /// Returns the localized SalePrice.
        /// </summary>
        public static double GetLocalizedSalePrice(this ProductStockUnit productStockUnit, string languageCode = null)
        {
            return _productStockUnitExtensionService.GetLocalizedSalePrice(productStockUnit, languageCode);
        }

        /// <summary>
        /// Returns the localized MembershipSalePrice.
        /// </summary>
        public static double GetLocalizedMembershipSalePrice(this ProductStockUnit productStockUnit, string languageCode = null)
        {
            return _productStockUnitExtensionService.GetLocalizedMembershipSalePrice(productStockUnit, languageCode);
        }
    }
}
