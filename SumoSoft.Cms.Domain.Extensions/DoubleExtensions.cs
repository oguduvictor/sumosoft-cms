﻿namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public static class DoubleExtensions
    {
        private static IDoubleExtensionService _doubleExtensionService => DependencyResolver.Current.GetService<IDoubleExtensionService>();

        /// <summary>
        ///
        /// </summary>
        /// <param name="netValue"></param>
        /// <param name="taxes"></param>
        /// <returns></returns>
        public static double AddTaxes(this double netValue, IEnumerable<Tax> taxes)
        {
            return _doubleExtensionService.AddTaxes(netValue, taxes);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="grossValue"></param>
        /// <param name="taxes"></param>
        /// <returns></returns>
        public static double RemoveTaxes(this double grossValue, IEnumerable<Tax> taxes)
        {
            return _doubleExtensionService.RemoveTaxes(grossValue, taxes);
        }
    }
}
