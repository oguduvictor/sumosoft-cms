﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class CartItemVariantExtensions
    {
        private static ICartItemVariantExtensionService _cartItemVariantExtensionService => DependencyResolver.Current.GetService<ICartItemVariantExtensionService>();

        /// <summary>
        /// Return the price modifier of the VariantOption that is currently selected.
        /// </summary>
        public static double GetPriceModifier(this CartItemVariant cartItemVariant, string languageCode = null)
        {
            return _cartItemVariantExtensionService.GetPriceModifier(cartItemVariant, languageCode);
        }
    }
}
