﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class AttributeOptionExtensions
    {
        private static IAttributeOptionExtensionService _attributeOptionExtensionService => DependencyResolver.Current.GetService<IAttributeOptionExtensionService>();

        /// <summary>
        ///  Returns the localized Text or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this AttributeOption attributeOption, string languageCode = null)
        {
            return _attributeOptionExtensionService.GetLocalizedTitle(attributeOption, languageCode);
        }
    }
}
