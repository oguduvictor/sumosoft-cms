﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Linq;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public static class CategoryExtensions
    {
        private static ICategoryExtensionService _categoryExtensionService => DependencyResolver.Current.GetService<ICategoryExtensionService>();

        /// <summary>
        ///  Return the localized IsDisabled property.
        /// </summary>
        public static bool GetLocalizedIsDisabled(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedIsDisabled(category, languageCode);
        }

        /// <summary>
        ///  Return the localized Name or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedTitle(category, languageCode);
        }

        /// <summary>
        ///  Return the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedDescription(category, languageCode);
        }

        /// <summary>
        ///  Return the localized Image or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedImage(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedImage(category, languageCode);
        }

        /// <summary>
        /// Get the Category localized title when featured in a collection banner.
        /// </summary>
        [NotNull]
        public static string GetLocalizedFeaturedTitle(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedFeaturedTitle(category, languageCode);
        }

        /// <summary>
        /// Get the Category localized description when featured in a collection banner.
        /// </summary>
        [NotNull]
        public static string GetLocalizedFeaturedDescription(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedFeaturedDescription(category, languageCode);
        }

        /// <summary>
        /// Get the Category localized image when featured in a collection banner.
        /// </summary>
        public static string GetLocalizedFeaturedImage(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedFeaturedImage(category, languageCode);
        }

        /// <summary>
        ///  Returns the localized MetaTitle or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedMetaTitle(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedMetaTitle(category, languageCode);
        }

        /// <summary>
        ///  Returns the localized MetaDescription or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedMetaDescription(this Category category, string languageCode = null)
        {
            return _categoryExtensionService.GetLocalizedMetaDescription(category, languageCode);
        }

        /// <summary>
        /// Return the concatenation of the category url with the url of all its parents.
        /// </summary>
        public static string GetUrl(this Category category)
        {
            return _categoryExtensionService.GetUrl(category);
        }

        public static IOrderedEnumerable<Product> GetSortedProducts(this Category category, bool includeDisabled)
        {
            return _categoryExtensionService.GetSortedProducts(category, includeDisabled);
        }

        public static IOrderedEnumerable<ProductImageKit> GetSortedProductImageKits(this Category category, bool includeDisabled)
        {
            return _categoryExtensionService.GetSortedProductImageKits(category, includeDisabled);
        }

        public static IOrderedEnumerable<ProductStockUnit> GetSortedProductStockUnits(this Category category, bool includeDisabled)
        {
            return _categoryExtensionService.GetSortedProductStockUnits(category, includeDisabled);
        }
    }
}