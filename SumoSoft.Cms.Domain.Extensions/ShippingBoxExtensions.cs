﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class ShippingBoxExtensions
    {
        private static IShippingBoxExtensionService _shippingBoxExtensionService => DependencyResolver.Current.GetService<IShippingBoxExtensionService>();

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedTitle(shippingBox, languageCode);
        }

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedDescription(shippingBox, languageCode);
        }

        /// <summary>
        ///  Returns the localized InternalDescription or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedInternalDescription(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedInternalDescription(shippingBox, languageCode);
        }

        /// <summary>
        ///  Returns the localized Price or the default country version.
        /// </summary>
        public static double GetLocalizedPrice(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedPrice(shippingBox, languageCode);
        }

        public static int GetLocalizedMinDays(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedMinDays(shippingBox, languageCode);
        }

        public static int GetLocalizedMaxDays(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedMaxDays(shippingBox, languageCode);
        }

        public static bool GetLocalizedCountWeekends(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedCountWeekends(shippingBox, languageCode);
        }

        public static double GetLocalizedFreeShippingMinimumPrice(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedFreeShippingMinimumPrice(shippingBox, languageCode);
        }

        /// <summary>
        /// Get the Shipping price before-tax. If the total of all CartItems is higher than the FreeShippingMinimumPrice, return 0.
        /// </summary>
        public static double GetShippingPriceBeforeTax(this ShippingBox shippingBox, string languageCode = null, double allCartItemsTotal = 0)
        {
            return _shippingBoxExtensionService.GetShippingPriceBeforeTax(shippingBox, languageCode, allCartItemsTotal);
        }

        /// <summary>
        /// Get the Shipping price after-tax. If the total of all CartItems is higher than the FreeShippingMinimumPrice, return 0.
        /// </summary>
        public static double GetShippingPriceAfterTax(this ShippingBox shippingBox, IEnumerable<Tax> countryTaxes, string languageCode = null, double allCartItemsTotal = 0)
        {
            return _shippingBoxExtensionService.GetShippingPriceAfterTax(shippingBox, countryTaxes, languageCode, allCartItemsTotal);
        }

        public static bool GetLocalizedIsDisabled(this ShippingBox shippingBox, string languageCode = null)
        {
            return _shippingBoxExtensionService.GetLocalizedIsDisabled(shippingBox, languageCode);
        }
    }
}
