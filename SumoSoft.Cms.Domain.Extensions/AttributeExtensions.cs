﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class AttributeExtensions
    {
        private static IAttributeExtensionService _attributeExtensionService => DependencyResolver.Current.GetService<IAttributeExtensionService>();

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this Attribute attribute, string languageCode = null)
        {
            return _attributeExtensionService.GetLocalizedTitle(attribute, languageCode);
        }
    }
}