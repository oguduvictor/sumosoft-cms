﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class OrderItemExtensions
    {
        private static IOrderItemExtensionService _orderItemExtensionService => DependencyResolver.Current.GetService<IOrderItemExtensionService>();

        [CanBeNull]
        public static OrderItemVariant GetOrderItemVariantByName(this OrderItem orderItem, string variantName)
        {
            return _orderItemExtensionService.GetOrderItemVariantByName(orderItem, variantName);
        }
    }
}