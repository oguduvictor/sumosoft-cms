﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class ProductExtension
    {
        private static IProductExtensionService _productExtensionService => DependencyResolver.Current.GetService<IProductExtensionService>();

        public static bool HasCategories(this Product product, params string[] categoryNames)
        {
            return _productExtensionService.HasCategories(product, categoryNames);
        }

        public static bool HasCategories(this Product product, params Category[] categories)
        {
            return _productExtensionService.HasCategories(product, categories);
        }

        /// <summary>
        /// Return the list of ShippingBoxes that are available for a certain Shipping Country.
        /// </summary>
        public static List<ShippingBox> GetShippingBoxesForShippingCountry(this Product product, Country shippingCountry)
        {
            return _productExtensionService.GetShippingBoxesForShippingCountry(product, shippingCountry);
        }

        /// <summary>
        ///  Returns the localized IsDisabled property.
        /// </summary>
        public static bool GetLocalizedIsDisabled(this Product product, string languageCode = null)
        {
            return _productExtensionService.GetLocalizedIsDisabled(product, languageCode);
        }

        /// <summary>
        ///  Returns the localized Name or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this Product product, string languageCode = null)
        {
            return _productExtensionService.GetLocalizedTitle(product, languageCode);
        }

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this Product product, string languageCode = null)
        {
            return _productExtensionService.GetLocalizedDescription(product, languageCode);
        }

        /// <summary>
        ///  Returns the localized MetaTitle or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedMetaTitle(this Product product, string languageCode = null)
        {
            return _productExtensionService.GetLocalizedMetaTitle(product, languageCode);
        }

        /// <summary>
        ///  Returns the localized MetaDescription or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedMetaDescription(this Product product, string languageCode = null)
        {
            return _productExtensionService.GetLocalizedMetaDescription(product, languageCode);
        }

        /// <summary>
        /// Gets an productAttribute by its Name.
        /// </summary>
        [CanBeNull]
        public static ProductAttribute GetProductAttributeByName(this Product product, string attributeName)
        {
            return _productExtensionService.GetProductAttributeByName(product, attributeName);
        }

        /// <summary>
        /// Gets an productVariant by its Name.
        /// </summary>
        [CanBeNull]
        public static ProductVariant GetProductVariantByName(this Product product, string variantName)
        {
            return _productExtensionService.GetProductVariantByName(product, variantName);
        }

        [NotNull]
        public static List<VariantOption> GetDefaultVariantOptions(this Product product)
        {
            return _productExtensionService.GetDefaultVariantOptions(product);
        }

        [CanBeNull]
        public static ProductImageKit GetProductImageKit(this Product product, List<VariantOption> variantOptions = null)
        {
            return _productExtensionService.GetProductImageKit(product, variantOptions);
        }

        [CanBeNull]
        public static ProductStockUnit GetProductStockUnit(this Product product, List<VariantOption> variantOptions = null)
        {
            return _productExtensionService.GetProductStockUnit(product, variantOptions);
        }

        public static int GetStock(this Product product, List<VariantOption> selectedVariantOptions)
        {
            return _productExtensionService.GetStock(product, selectedVariantOptions);
        }

        /// <summary>
        /// Return the Total before tax without considering the SalePrice.
        /// </summary>
        public static double GetTotalBeforeTaxWithoutSalePrice(this Product product, CmsDbContext dbContext, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            return _productExtensionService.GetTotalBeforeTaxWithoutSalePrice(dbContext, product, variantOptions, languageCode);
        }

        /// <summary>
        /// Return the Total after tax without considering the SalePrice.
        /// </summary>
        public static double GetTotalAfterTaxWithoutSalePrice(this Product product, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            return _productExtensionService.GetTotalAfterTaxWithoutSalePrice(dbContext, product, countryTaxes, variantOptions, languageCode);
        }

        /// <summary>
        /// Returns the final Price of a Product, given a selection of VariantOptions.
        /// </summary>
        public static double GetTotalBeforeTax(this Product product, CmsDbContext dbContext, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            return _productExtensionService.GetTotalBeforeTax(dbContext, product, variantOptions, languageCode);
        }

        /// <summary>
        /// Returns the final Price of a Product including Taxes.
        /// </summary>
        public static double GetTotalAfterTax(this Product product, CmsDbContext dbContext, IEnumerable<Tax> countryTaxes, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            return _productExtensionService.GetTotalAfterTax(dbContext, product, countryTaxes, variantOptions, languageCode);
        }

        public static double GetTotalAfterTaxWithoutSalePrice(this Product product, CmsDbContext dbContext, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            return _productExtensionService.GetTotalAfterTaxWithoutSalePrice(dbContext, product, variantOptions, languageCode);
        }

        public static double GetTotalAfterTax(this Product product, CmsDbContext dbContext, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            return _productExtensionService.GetTotalAfterTax(dbContext, product, variantOptions, languageCode);
        }

        public static List<ShippingBox> GetShippingBoxesForShippingCountry(this Product product, CmsDbContext dbContext)
        {
            return _productExtensionService.GetShippingBoxesForShippingCountry(dbContext, product);
        }
    }
}