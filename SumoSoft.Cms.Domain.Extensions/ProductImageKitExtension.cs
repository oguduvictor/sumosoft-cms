﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class ProductImageKitExtensions
    {
        private static IProductImageKitExtensionService _productImageKitExtensionService => DependencyResolver.Current.GetService<IProductImageKitExtensionService>();

        public static bool HasCategories(this ProductImageKit productImageKit, params string[] categoryNames)
        {
            return _productImageKitExtensionService.HasCategories(productImageKit, categoryNames);
        }

        public static bool HasCategories(this ProductImageKit productImageKit, params Category[] categories)
        {
            return _productImageKitExtensionService.HasCategories(productImageKit, categories);
        }

        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this ProductImageKit productImageKit, string variantOptionName)
        {
            return _productImageKitExtensionService.GetVariantOptionByName(productImageKit, variantOptionName);
        }

        /// <summary>
        /// Returns whether all the VariantOptions of the current productImageKit are
        /// included  in a superset of VariantOptions passed as parameter. Use this method
        /// to find which productImageKit matches the selected options of a CartItem.
        /// </summary>
        public static bool Matches(this ProductImageKit productImageKit, List<VariantOption> selectedVariantOptions)
        {
            return _productImageKitExtensionService.Matches(productImageKit, selectedVariantOptions);
        }

        [CanBeNull]
        public static ProductImage GetProductImageByName(this ProductImageKit productImageKit, string imageName)
        {
            return _productImageKitExtensionService.GetProductImageByName(productImageKit, imageName);
        }
    }
}
