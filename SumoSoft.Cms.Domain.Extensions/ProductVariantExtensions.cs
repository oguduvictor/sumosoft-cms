﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class ProductVariantExtensions
    {
        private static IProductVariantExtensionService _productVariantExtensionService => DependencyResolver.Current.GetService<IProductVariantExtensionService>();

        /// <summary>
        /// This method tries to return the Product.DefaultVariantOptionValue. If null, it returns the Variant.DefaultVariantOptionValue.
        /// If even this last value is null, it returns Variant?.VariantOptions.OrderBy(x => x.SortOrder).FirstOrDefault().
        /// </summary>
        [CanBeNull]
        public static VariantOption GetDefaultVariantOptionValue(this ProductVariant productVariant)
        {
            return _productVariantExtensionService.GetDefaultVariantOptionValue(productVariant);
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public static VariantOption GetVariantOptionByName(this ProductVariant productVariant, string variantOptionName)
        {
            return _productVariantExtensionService.GetVariantOptionByName(productVariant, variantOptionName);
        }
    }
}