﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class MailingListExtensions
    {
        private static IMailingListExtensionService _mailingListExtensionService => DependencyResolver.Current.GetService<IMailingListExtensionService>();

        /// <summary>
        ///  Return the localized IsDisabled property.
        /// </summary>
        public static bool GetLocalizedIsDisabled(this MailingList mailingList, string languageCode = null)
        {
            return _mailingListExtensionService.GetLocalizedIsDisabled(mailingList, languageCode);
        }

        /// <summary>
        ///  Return the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this MailingList mailingList, string languageCode = null)
        {
            return _mailingListExtensionService.GetLocalizedTitle(mailingList, languageCode);
        }

        /// <summary>
        ///  Return the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this MailingList mailingList, string languageCode = null)
        {
            return _mailingListExtensionService.GetLocalizedDescription(mailingList, languageCode);
        }
    }
}
