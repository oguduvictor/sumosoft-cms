﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class SeoSectionExtensions
    {
        private static ISeoSectionExtensionService _seoSectionExtensionService => DependencyResolver.Current.GetService<ISeoSectionExtensionService>();

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedTitle(this SeoSection seoSection, string languageCode = null)
        {
            return _seoSectionExtensionService.GetLocalizedTitle(seoSection, languageCode);
        }

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedDescription(this SeoSection seoSection, string languageCode = null)
        {
            return _seoSectionExtensionService.GetLocalizedDescription(seoSection, languageCode);
        }

        /// <summary>
        ///  Returns the localized Image or the default country version.
        /// </summary>
        [NotNull]
        public static string GetLocalizedImage(this SeoSection seoSection, string languageCode = null)
        {
            return _seoSectionExtensionService.GetLocalizedImage(seoSection, languageCode);
        }
    }
}
