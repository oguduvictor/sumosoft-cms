﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Web.Mvc;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public static class BlogPostExtensions
    {
        private static IBlogPostExtensionService _blogPostExtensionService => DependencyResolver.Current.GetService<IBlogPostExtensionService>();

        public static string GetContentPreview(this BlogPost blogPost, int length)
        {
            return _blogPostExtensionService.GetContentPreview(blogPost, length);
        }
    }
}