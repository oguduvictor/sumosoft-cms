﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Extensions
{
    using System.Globalization;
    using System.Web.Mvc;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public static class CountryExtensions
    {
        private static ICountryExtensionService _countryExtensionService => DependencyResolver.Current.GetService<ICountryExtensionService>();

        [CanBeNull]
        public static CultureInfo GetCultureInfo(this Country country)
        {
            return _countryExtensionService.GetCultureInfo(country);
        }

        [CanBeNull]
        public static RegionInfo GetRegionInfo(this Country country)
        {
            return _countryExtensionService.GetRegionInfo(country);
        }

        /// <summary>
        /// Return the currency symbol ($, £...)
        /// </summary>
        [CanBeNull]
        public static string GetCurrencySymbol(this Country country)
        {
            return _countryExtensionService.GetCurrencySymbol(country);
        }

        /// <summary>
        /// Return the ISO4217 Currency Symbol (USD, GBP...)
        /// </summary>
        [CanBeNull]
        public static string GetIso4217CurrencySymbol(this Country country)
        {
            return _countryExtensionService.GetIso4217CurrencySymbol(country);
        }

        /// <summary>
        /// Returns the Country Url preceded by the slash [/] character if the string is not empty.
        /// Useful for string concatenations like: Country.GetUrlForStringConcatenation() + "/controller/action".
        /// </summary>
        public static string GetUrlForStringConcatenation(this Country country)
        {
            return _countryExtensionService.GetUrlForStringConcatenation(country);
        }
    }
}