﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class ProductImageKitExtensionService : IProductImageKitExtensionService
    {
        /// <inheritdoc />
        public virtual bool HasCategories(ProductImageKit productImageKit, params string[] categoryNames)
        {
            return categoryNames.Intersect(productImageKit.Categories.Select(x => x.Name)).Count() == categoryNames.Length;
        }

        /// <inheritdoc />
        public virtual bool HasCategories(ProductImageKit productImageKit, params Category[] categories)
        {
            return this.HasCategories(productImageKit, categories.Select(x => x.Name).ToArray());
        }

        /// <inheritdoc />
        public virtual VariantOption GetVariantOptionByName(ProductImageKit productImageKit, string variantOptionName)
        {
            return productImageKit.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }

        /// <inheritdoc />
        public virtual bool Matches(ProductImageKit productImageKit, List<VariantOption> selectedVariantOptions)
        {
            return productImageKit.VariantOptions.All(x => selectedVariantOptions.Any(s => s.Id == x.Id));
        }

        /// <inheritdoc />
        public virtual ProductImage GetProductImageByName(ProductImageKit productImageKit, string imageName)
        {
            return productImageKit.ProductImages.FirstOrDefault(x => x.Name == imageName);
        }
    }
}