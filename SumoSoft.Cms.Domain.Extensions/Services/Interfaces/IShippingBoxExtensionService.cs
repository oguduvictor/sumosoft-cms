﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IShippingBoxExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        string GetLocalizedTitle(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedDescription(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        ///  Returns the localized InternalDescription or the default country version.
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedInternalDescription(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        ///  Returns the localized Price or the default country version.
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetLocalizedPrice(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        int GetLocalizedMinDays(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        int GetLocalizedMaxDays(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        bool GetLocalizedCountWeekends(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetLocalizedFreeShippingMinimumPrice(ShippingBox shippingBox, string languageCode = null);

        /// <summary>
        /// Get the Shipping price before-tax. If the total of all CartItems is higher than the FreeShippingMinimumPrice, return 0.
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <param name="allCartItemsTotal"></param>
        /// <returns></returns>
        double GetShippingPriceBeforeTax(ShippingBox shippingBox, string languageCode = null, double allCartItemsTotal = 0);

        /// <summary>
        /// Get the Shipping price after-tax. If the total of all CartItems is higher than the FreeShippingMinimumPrice, return 0.
        /// </summary>
        double GetShippingPriceAfterTax(ShippingBox shippingBox, IEnumerable<Tax> countryTaxes, string languageCode = null, double allCartItemsTotal = 0);

        /// <summary>
        ///
        /// </summary>
        /// <param name="shippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        bool GetLocalizedIsDisabled(ShippingBox shippingBox, string languageCode);
    }
}