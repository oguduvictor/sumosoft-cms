﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public interface IIListExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localizedKits"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [CanBeNull]
        T GetByLanguageCode<T>(IList<T> localizedKits, string languageCode = null) where T : BaseEntity, ILocalizedKit;

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localizedKits"></param>
        /// <returns></returns>
        [CanBeNull]
        T GetByDefaultCountry<T>(IList<T> localizedKits) where T : BaseEntity, ILocalizedKit;

        /// <summary>
        /// Returns a List where the only item is the LocalizedKit of the Default Country.
        /// Use this method to create a smaller DTO.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localizedKits"></param>
        /// <returns></returns>
        [NotNull]
        List<T> FilterByDefaultCountry<T>(IList<T> localizedKits) where T : class, ILocalizedKit;

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localizedKits"></param>
        /// <returns></returns>
        List<T> OrderByDefaultCountry<T>(IList<T> localizedKits) where T : ILocalizedKit;

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localizedKits"></param>
        /// <returns></returns>
        List<T> OrderByCurrentCountry<T>(IList<T> localizedKits) where T : ILocalizedKit;

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        IOrderedEnumerable<T> OrderBySortOrder<T>(IList<T> list) where T : ISortable;

        /// <summary>
        /// Check if a List of Entities contains a certain item, based on its Id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        bool ContainsById<T>(IList<T> items, T item) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on their Ids.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAllById<T>(IList<T> items, params T[] args) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on their Ids.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAllById<T>(IList<T> items, List<T> args) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on their Ids.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAnyById<T>(IList<T> items, params T[] args) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on theirs Id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAnyById<T>(IList<T> items, List<T> args) where T : BaseEntity;
    }
}