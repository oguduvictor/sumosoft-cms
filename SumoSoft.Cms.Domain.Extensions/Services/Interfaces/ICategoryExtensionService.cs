﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Linq;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface ICategoryExtensionService
    {
        /// <summary>
        ///  Return the localized IsDisabled property.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        bool GetLocalizedIsDisabled(Category category, string languageCode = null);

        /// <summary>
        ///  Return the localized Name or the default country version.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedTitle(Category category, string languageCode = null);

        /// <summary>
        ///  Return the localized Description or the default country version.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedDescription(Category category, string languageCode = null);

        /// <summary>
        ///  Return the localized Image or the default country version.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedImage(Category category, string languageCode = null);

        /// <summary>
        /// Get the Category localized title when featured in a collection banner.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedFeaturedTitle(Category category, string languageCode = null);

        /// <summary>
        /// Get the Category localized description when featured in a collection banner.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedFeaturedDescription(Category category, string languageCode = null);

        /// <summary>
        /// Get the Category localized image when featured in a collection banner.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        string GetLocalizedFeaturedImage(Category category, string languageCode = null);

        /// <summary>
        ///  Returns the localized MetaTitle or the default country version.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedMetaTitle(Category category, string languageCode = null);

        /// <summary>
        ///  Returns the localized MetaDescription or the default country version.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedMetaDescription(Category category, string languageCode = null);

        /// <summary>
        /// Return the concatenation of the category url with the url of all its parents.
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        string GetUrl(Category category);

        /// <summary>
        ///
        /// </summary>
        /// <param name="category"></param>
        /// <param name="includeDisabled"></param>
        /// <returns></returns>
        IOrderedEnumerable<Product> GetSortedProducts(Category category, bool includeDisabled);

        /// <summary>
        ///
        /// </summary>
        /// <param name="category"></param>
        /// <param name="includeDisabled"></param>
        /// <returns></returns>
        IOrderedEnumerable<ProductImageKit> GetSortedProductImageKits(Category category, bool includeDisabled);

        /// <summary>
        ///
        /// </summary>
        /// <param name="category"></param>
        /// <param name="includeDisabled"></param>
        /// <returns></returns>
        IOrderedEnumerable<ProductStockUnit> GetSortedProductStockUnits(Category category, bool includeDisabled);
    }
}