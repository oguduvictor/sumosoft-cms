﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IBlogPostExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="blogPost"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        string GetContentPreview(BlogPost blogPost, int length);
    }
}