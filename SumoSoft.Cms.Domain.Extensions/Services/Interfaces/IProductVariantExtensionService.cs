﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IProductVariantExtensionService
    {
        /// <summary>
        /// This method tries to return the Product.DefaultVariantOptionValue. If null, it returns the Variant.DefaultVariantOptionValue.
        /// If even this last value is null, it returns Variant?.VariantOptions.OrderBy(x => x.SortOrder).FirstOrDefault().
        /// </summary>
        /// <param name="productVariant"></param>
        /// <returns></returns>
        [CanBeNull]
        VariantOption GetDefaultVariantOptionValue(ProductVariant productVariant);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productVariant"></param>
        /// <param name="variantOptionName"></param>
        /// <returns></returns>
        VariantOption GetVariantOptionByName(ProductVariant productVariant, string variantOptionName);
    }
}
