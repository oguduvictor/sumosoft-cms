﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IUserExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        ICollection<UserCredit> GetLocalizedUserCredits(User user, string languageCode = null);

        /// <summary>
        /// Gets a UserAttribute by its Name.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        [CanBeNull]
        UserAttribute GetUserAttributeByName(User user, string attributeName);
    }
}