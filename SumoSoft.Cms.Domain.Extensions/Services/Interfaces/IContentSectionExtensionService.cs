﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IContentSectionExtensionService
    {
        /// <summary>
        ///  Returns the localized Content or the default country version.
        /// </summary>
        [NotNull]
        string GetLocalizedContent(ContentSection contentSection, string languageCode = null);
    }
}