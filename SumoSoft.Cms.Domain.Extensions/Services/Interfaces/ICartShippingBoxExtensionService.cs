﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;

    /// <summary>
    ///
    /// </summary>
    public interface ICartShippingBoxExtensionService
    {
        /// <summary>
        /// Get the Shipping price before-tax. If the sum of all the Cart Items in the cart is higher than the FreeShippingMinimumPrice, returns 0.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartShippingBox"></param>
        /// <param name="languageCode"></param>
        double GetShippingPriceBeforeTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, string languageCode = null);

        /// <summary>
        /// Get the Shipping price after-tax.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartShippingBox"></param>
        /// <param name="countryTaxes"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetShippingPriceAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, IEnumerable<Tax> countryTaxes, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartShippingBox"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetTotalBeforeTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartShippingBox"></param>
        /// <param name="countryTaxes"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetTotalAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, IEnumerable<Tax> countryTaxes, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartShippingBox"></param>
        /// <returns></returns>
        double GetShippingPriceAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartShippingBox"></param>
        /// <returns></returns>
        double GetTotalAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox);
    }
}