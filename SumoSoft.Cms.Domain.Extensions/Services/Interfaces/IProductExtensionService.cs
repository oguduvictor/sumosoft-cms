﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.DataAccess;

    /// <summary>
    ///
    /// </summary>
    public interface IProductExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        List<VariantOption> GetDefaultVariantOptions(Product product);

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        string GetLocalizedDescription(Product product, string languageCode = null);

        /// <summary>
        ///  Returns the localized IsDisabled property.
        /// </summary>
        bool GetLocalizedIsDisabled(Product product, string languageCode = null);

        /// <summary>
        ///  Returns the localized MetaDescription or the default country version.
        /// </summary>
        string GetLocalizedMetaDescription(Product product, string languageCode = null);

        /// <summary>
        ///  Returns the localized MetaTitle or the default country version.
        /// </summary>
        string GetLocalizedMetaTitle(Product product, string languageCode = null);

        /// <summary>
        ///  Returns the localized Name or the default country version.
        /// </summary>
        string GetLocalizedTitle(Product product, string languageCode = null);

        /// <summary>
        /// Gets an ProductAttribute by its Name.
        /// </summary>
        ProductAttribute GetProductAttributeByName(Product product, string attributeName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="product"></param>
        /// <param name="variantOptions"></param>
        /// <returns></returns>
        ProductImageKit GetProductImageKit(Product product, List<VariantOption> variantOptions = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="product"></param>
        /// <param name="variantOptions"></param>
        /// <returns></returns>
        ProductStockUnit GetProductStockUnit(Product product, List<VariantOption> variantOptions = null);

        /// <summary>
        /// Gets an ProductVariant by its Name.
        /// </summary>
        ProductVariant GetProductVariantByName(Product product, string variantName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        List<ShippingBox> GetShippingBoxesForShippingCountry(CmsDbContext dbContext, Product product);

        /// <summary>
        /// Return the list of ShippingBoxes that are available for a certain Shipping Country.
        /// </summary>
        List<ShippingBox> GetShippingBoxesForShippingCountry(Product product, Country shippingCountry);

        /// <summary>
        ///
        /// </summary>
        /// <param name="product"></param>
        /// <param name="selectedVariantOptions"></param>
        /// <returns></returns>
        int GetStock(Product product, List<VariantOption> selectedVariantOptions);

        /// <summary>
        /// Returns the final Price of a Product including Taxes.
        /// </summary>
        double GetTotalAfterTax(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null);

        /// <summary>
        /// Returns the final Price of a Product including Taxes.
        /// </summary>
        double GetTotalAfterTax(CmsDbContext dbContext, Product product, IEnumerable<Tax> countryTaxes, List<VariantOption> variantOptions = null, string languageCode = null);

        /// <summary>
        /// Return the Total before tax without considering the SalePrice.
        /// </summary>
        double GetTotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null);

        /// <summary>
        /// Return the Total before tax without considering the SalePrice.
        /// </summary>
        double GetTotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, Product product, IEnumerable<Tax> countryTaxes, List<VariantOption> variantOptions = null, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="product"></param>
        /// <param name="variantOptions"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetTotalBeforeTax(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null);

        /// <summary>
        /// Return the Total before tax without considering the SalePrice.
        /// </summary>
        double GetTotalBeforeTaxWithoutSalePrice(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="product"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        bool HasCategories(Product product, params Category[] categories);

        /// <summary>
        ///
        /// </summary>
        /// <param name="product"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        bool HasCategories(Product product, params string[] categoryNames);
    }
}