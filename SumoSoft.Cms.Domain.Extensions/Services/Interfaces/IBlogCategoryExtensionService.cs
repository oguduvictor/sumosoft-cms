﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IBlogCategoryExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="blogCategory"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        string GetLocalizedTitle(BlogCategory blogCategory, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="blogCategory"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        string GetLocalizedDescription(BlogCategory blogCategory, string languageCode = null);
    }
}
