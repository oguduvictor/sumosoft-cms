﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IEmailExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedFrom(Email email, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedDisplayName(Email email, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedReplyTo(Email email, string languageCode = null);
    }
}