﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IAttributeOptionExtensionService
    {
        /// <summary>
        ///  Returns the localized Text or the default country version.
        /// </summary>
        [NotNull]
        string GetLocalizedTitle(AttributeOption attributeOption, string languageCode = null);
    }
}