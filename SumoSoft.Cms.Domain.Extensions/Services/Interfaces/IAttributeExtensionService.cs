﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IAttributeExtensionService
    {
        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        [NotNull]
        string GetLocalizedTitle(Attribute attribute, string languageCode = null);
    }
}