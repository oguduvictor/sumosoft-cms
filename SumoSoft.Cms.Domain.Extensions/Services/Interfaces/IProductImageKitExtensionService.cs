﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IProductImageKitExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKit"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        bool HasCategories(ProductImageKit productImageKit, params string[] categoryNames);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKit"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        bool HasCategories(ProductImageKit productImageKit, params Category[] categories);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKit"></param>
        /// <param name="variantOptionName"></param>
        /// <returns></returns>
        [CanBeNull]
        VariantOption GetVariantOptionByName(ProductImageKit productImageKit, string variantOptionName);

        /// <summary>
        /// Returns whether all the VariantOptions of the current ProductImageKit are
        /// included  in a superset of VariantOptions passed as parameter. Use this method
        /// to find which ProductImageKit matches the selected options of a CartItem.
        /// </summary>
        /// <param name="productImageKit"></param>
        /// <param name="selectedVariantOptions"></param>
        /// <returns></returns>
        bool Matches(ProductImageKit productImageKit, List<VariantOption> selectedVariantOptions);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKit"></param>
        /// <param name="imageName"></param>
        /// <returns></returns>
        [CanBeNull]
        ProductImage GetProductImageByName(ProductImageKit productImageKit, string imageName);
    }
}