﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface ICartItemVariantExtensionService
    {
        /// <summary>
        /// Return the price modifier of the VariantOption that is currently selected.
        /// </summary>
        double GetPriceModifier(CartItemVariant cartItemVariant, string languageCode = null);
    }
}