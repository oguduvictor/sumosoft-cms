﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public interface IITaggableExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="iTaggable"></param>
        /// <returns></returns>
        List<string> GetTagList(ITaggable iTaggable);

        /// <summary>
        ///
        /// </summary>
        /// <param name="iTaggable"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        bool HasTags(ITaggable iTaggable, params string[] tags);
    }
}