﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IVariantOptionExtensionService
    {
        /// <summary>
        ///  Returns the localized IsDisabled property.
        /// </summary>
        /// <param name="variantOption"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        bool GetLocalizedIsDisabled(VariantOption variantOption, string languageCode = null);

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        /// <param name="variantOption"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedTitle(VariantOption variantOption, string languageCode = null);

        /// <summary>
        /// Returns the localized Description or the default country version.
        /// </summary>
        /// <param name="variantOption"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedDescription(VariantOption variantOption, string languageCode = null);

        /// <summary>
        /// Returns the localized Title.
        /// </summary>
        /// <param name="variantOption"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetLocalizedPriceModifier(VariantOption variantOption, string languageCode = null);
    }
}