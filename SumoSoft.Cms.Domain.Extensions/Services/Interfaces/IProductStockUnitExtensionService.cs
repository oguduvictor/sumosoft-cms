﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IProductStockUnitExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="productStockUnit"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        bool HasCategories(ProductStockUnit productStockUnit, params string[] categoryNames);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productStockUnit"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        bool HasCategories(ProductStockUnit productStockUnit, params Category[] categories);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productStockUnit"></param>
        /// <param name="variantOptionName"></param>
        /// <returns></returns>
        [CanBeNull]
        VariantOption GetVariantOptionByName(ProductStockUnit productStockUnit, string variantOptionName);

        /// <summary>
        /// Returns whether all the VariantOptions of the current ProductStockUnit are
        /// included  in a superset of VariantOptions passed as parameter. Use this method
        /// to find which ProductStockUnit matches the selected options of a CartItem.
        /// </summary>
        /// <param name="productStockUnit"></param>
        /// <param name="selectedVariantOptions"></param>
        /// <returns></returns>
        bool Matches(ProductStockUnit productStockUnit, List<VariantOption> selectedVariantOptions);

        /// <summary>
        ///  Returns the localized BasePrice.
        /// </summary>
        /// <param name="productStockUnit"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetLocalizedBasePrice(ProductStockUnit productStockUnit, string languageCode = null);

        /// <summary>
        /// Returns the localized SalePrice.
        /// </summary>
        /// <param name="productStockUnit"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetLocalizedSalePrice(ProductStockUnit productStockUnit, string languageCode = null);

        /// <summary>
        /// Returns the localized MembershipSalePrice.
        /// </summary>
        /// <param name="productStockUnit"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetLocalizedMembershipSalePrice(ProductStockUnit productStockUnit, string languageCode = null);
    }
}