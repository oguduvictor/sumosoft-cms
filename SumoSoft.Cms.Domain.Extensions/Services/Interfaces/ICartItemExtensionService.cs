﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;

    /// <summary>
    ///
    /// </summary>
    public interface ICartItemExtensionService
    {
        /// <summary>
        /// Gets the CartItemProductVariant by its Variant Name.
        /// </summary>
        [CanBeNull]
        CartItemVariant GetCartItemVariantByName(CartItem cartItem, string variantName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        [CanBeNull]
        ProductAttribute GetProductAttributeByName(CartItem cartItem, string attributeName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="variantName"></param>
        /// <returns></returns>
        [CanBeNull]
        ProductVariant GetProductVariantByName(CartItem cartItem, string variantName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        List<VariantOption> GetSelectedVariantOptions(CartItem cartItem);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="selectedVariantOptions"></param>
        /// <returns></returns>
        [CanBeNull]
        ProductImageKit GetProductImageKit(CartItem cartItem, List<VariantOption> selectedVariantOptions = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="selectedVariantOptions"></param>
        /// <returns></returns>
        [CanBeNull]
        ProductStockUnit GetProductStockUnit(CartItem cartItem, List<VariantOption> selectedVariantOptions = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="selectedVariantOptions"></param>
        /// <returns></returns>
        int GetStock(CartItem cartItem, List<VariantOption> selectedVariantOptions = null);

        /// <summary>
        /// Evaluate if the current ProductStockUnit has 0 Stock and Preorder is not enabled.
        /// The method returns false if no ProductStockUnit exists.
        /// </summary>
        bool IsOutOfStock(CartItem cartItem, List<VariantOption> selectedVariantOptions = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        DateTime GetMinEta(CartItem cartItem, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        DateTime GetMaxEta(CartItem cartItem, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItem"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        double GetSubtotalBeforeTax(CmsDbContext dbContext, CartItem cartItem, string languageCode = null);

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, after Tax using taxes from shipping/current country.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        double GetSubtotalAfterTax(CmsDbContext dbContext, CartItem cartItem);

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, after Tax.
        /// </summary>
        double GetSubtotalAfterTax(CmsDbContext dbContext, CartItem cartItem, IEnumerable<Tax> countryTaxes, string languageCode = null);

        /// <summary>
        /// The total price of the CartItem after tax using taxes from shipping/current country.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        double GetTotalAfterTax(CmsDbContext dbContext, CartItem cartItem);

        /// <summary>
        /// The total price of the CartItem after-tax.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItem"></param>
        /// <param name="countryTaxes"></param>
        /// <param name="languageCode"></param>
        double GetTotalAfterTax(CmsDbContext dbContext, CartItem cartItem, IEnumerable<Tax> countryTaxes, string languageCode = null);

        /// <summary>
        /// The total price of the CartItem before-tax.
        /// </summary>
        double GetTotalBeforeTax(CmsDbContext dbContext, CartItem cartItem, string languageCode = null);

        /// <summary>
        /// Return the list of Taxes applied to the current CartItem.
        /// </summary>
        List<Tax> GetTaxes(CartItem cartItem, IEnumerable<Tax> countryTaxes);

        /// <summary>
        /// The share of the Coupon applied to the current item, calculated on the Subtotal before tax.
        /// </summary>
        double GetCouponShareValue(CmsDbContext dbContext, CartItem cartItem, string languageCode = null);

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, without taking into account the "Sale at price", before Tax.
        /// </summary>
        double GetSubtotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, CartItem cartItem, IEnumerable<Tax> countryTaxes, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="dbContext"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        bool CouponApplies(CmsDbContext dbContext, CartItem cartItem, string languageCode = null);

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, without taking into account the "Sale at price", before Tax from shipping/current country.
        /// </summary>
        double GetSubtotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, CartItem cartItem);

        /// <summary>
        /// The Subtotal of a CartItem before applying the Coupon and the UserCredit, without taking into account the "Sale at price", before Tax.
        /// </summary>
        double GetSubtotalBeforeTaxWithoutSalePrice(CmsDbContext dbContext, CartItem cartItem, string languageCode = null);
    }
}