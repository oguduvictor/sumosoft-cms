﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    public interface IMailingListExtensionService
    {
        bool GetLocalizedIsDisabled(MailingList mailingList, string languageCode);

        string GetLocalizedTitle(MailingList mailingList, string languageCode);

        string GetLocalizedDescription(MailingList mailingList, string languageCode);
    }
}
