﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IVariantExtensionService
    {
        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        /// <param name="variant"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedDescription(Variant variant, string languageCode = null);

        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        /// <param name="variant"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedTitle(Variant variant, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="variant"></param>
        /// <param name="variantOptionName"></param>
        /// <returns></returns>
        [CanBeNull]
        VariantOption GetVariantOptionByName(Variant variant, string variantOptionName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="variant"></param>
        /// <param name="variantOptionUrl"></param>
        /// <returns></returns>
        [CanBeNull]
        VariantOption GetVariantOptionByUrl(Variant variant, string variantOptionUrl);
    }
}