﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///
    /// </summary>
    public interface IIOrderedEnumerableExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="products"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        IOrderedEnumerable<Product> ThenByCategoryNames(IOrderedEnumerable<Product> products, List<string> categoryNames);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKits"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        IOrderedEnumerable<ProductImageKit> ThenByProductCategoryNames(IOrderedEnumerable<ProductImageKit> productImageKits, List<string> categoryNames);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        IOrderedEnumerable<T> ThenByIds<T>(IOrderedEnumerable<T> list, string ids) where T : BaseEntity;
    }
}