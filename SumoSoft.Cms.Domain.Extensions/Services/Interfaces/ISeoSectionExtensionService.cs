﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface ISeoSectionExtensionService
    {
        /// <summary>
        ///  Returns the localized Title or the default country version.
        /// </summary>
        /// <param name="seoSection"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedTitle(SeoSection seoSection, string languageCode = null);

        /// <summary>
        ///  Returns the localized Description or the default country version.
        /// </summary>
        /// <param name="seoSection"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedDescription(SeoSection seoSection, string languageCode = null);

        /// <summary>
        ///  Returns the localized Image or the default country version.
        /// </summary>
        /// <param name="seoSection"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        [NotNull]
        string GetLocalizedImage(SeoSection seoSection, string languageCode = null);
    }
}