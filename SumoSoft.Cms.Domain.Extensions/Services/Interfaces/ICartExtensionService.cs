﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Dto;

    /// <summary>
    ///
    /// </summary>
    public interface ICartExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="cart"></param>
        /// <param name="shippingBoxName"></param>
        /// <returns></returns>
        [CanBeNull]
        CartShippingBox GetCartShippingBoxByName(Cart cart, string shippingBoxName);

        /// <summary>
        /// The coupon value, calculated on the net price of the Cart Items.
        /// </summary>
        double GetCouponValue(CmsDbContext dbContext, Cart cart, string languageCode = null);

        /// <summary>
        /// The Total value of the Cart, before Tax.
        /// </summary>
        double GetTotalBeforeTax(CmsDbContext dbContext, Cart cart, string languageCode = null);

        /// <summary>
        /// The Total value of the Cart, after Tax.
        /// </summary>
        double GetTotalAfterTax(CmsDbContext dbContext, Cart cart, IEnumerable<Tax> countryTaxes, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cart"></param>
        /// <returns></returns>
        double GetTotalAfterTax(CmsDbContext dbContext, Cart cart);


        /// <summary>
        /// The TotalAfterTax minus the UserCredit Amount, if applied.
        /// </summary>
        double GetTotalToBePaid(CmsDbContext dbContext, Cart cart, IEnumerable<Tax> countryTaxes, string languageCode = null);

        /// <summary>
        /// The TotalAfterTax minus the UserCredit Amount, if applied.
        /// </summary>
        double GetTotalToBePaid(CmsDbContext dbContext, Cart cart);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cart"></param>
        /// <returns></returns>
        List<CartError> GetErrors(CmsDbContext dbContext, Cart cart);
    }
}