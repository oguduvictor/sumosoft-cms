﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface IOrderItemExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="orderItem"></param>
        /// <param name="variantName"></param>
        /// <returns></returns>
        [CanBeNull]
        OrderItemVariant GetOrderItemVariantByName(OrderItem orderItem, string variantName);
    }
}