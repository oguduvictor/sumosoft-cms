﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    ///
    /// </summary>
    public interface IDoubleExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="netValue"></param>
        /// <param name="taxes"></param>
        /// <returns></returns>
        double AddTaxes(double netValue, IEnumerable<Tax> taxes);


        /// <summary>
        ///
        /// </summary>
        /// <param name="grossValue"></param>
        /// <param name="taxes"></param>
        /// <returns></returns>
        double RemoveTaxes(double grossValue, IEnumerable<Tax> taxes);
    }
}