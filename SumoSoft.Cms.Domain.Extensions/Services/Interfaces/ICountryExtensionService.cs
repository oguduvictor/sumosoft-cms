﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System.Globalization;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public interface ICountryExtensionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        [CanBeNull]
        CultureInfo GetCultureInfo(Country country);

        /// <summary>
        ///
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        [CanBeNull]
        RegionInfo GetRegionInfo(Country country);

        /// <summary>
        /// Return the currency symbol ($, £...)
        /// </summary>
        [CanBeNull]
        string GetCurrencySymbol(Country country);

        /// <summary>
        /// Return the ISO4217 Currency Symbol (USD, GBP...)
        /// </summary>
        [CanBeNull]
        string GetIso4217CurrencySymbol(Country country);

        /// <summary>
        /// Returns the Country Url preceded by the slash [/] character if the string is not empty.
        /// Useful for string concatenations like: Country.GetUrlForStringConcatenation() + "/controller/action".
        /// </summary>
        string GetUrlForStringConcatenation(Country country);
    }
}