﻿namespace SumoSoft.Cms.Domain.Extensions.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public interface IIEnumerableExtensionService
    {
        /// <summary>
        /// Check if a List of Entities contains a certain item, based on its Id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        bool ContainsById<T>(IEnumerable<T> items, T item) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on their Ids.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAnyById<T>(IEnumerable<T> items, params T[] args) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains any of the items of another List, based on their Ids.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAnyById<T>(IEnumerable<T> items, List<T> args) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on their Ids.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAllById<T>(IEnumerable<T> items, params T[] args) where T : BaseEntity;

        /// <summary>
        /// Check if a List of Entities contains all items of another List, based on their Ids.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        bool ContainsAllById<T>(IEnumerable<T> items, List<T> args) where T : BaseEntity;

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        IOrderedEnumerable<T> OrderBySortOrder<T>(IEnumerable<T> items) where T : ISortable;

        /// <summary>
        ///
        /// </summary>
        /// <param name="products"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        IOrderedEnumerable<Product> OrderByCategoryNames(IEnumerable<Product> products, List<string> categoryNames);

        /// <summary>
        ///
        /// </summary>
        /// <param name="productImageKits"></param>
        /// <param name="categoryNames"></param>
        /// <returns></returns>
        IOrderedEnumerable<ProductImageKit> OrderByProductCategoryNames(IEnumerable<ProductImageKit> productImageKits, List<string> categoryNames);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="idList"></param>
        /// <returns></returns>
        IOrderedEnumerable<T> OrderByIds<T>(IEnumerable<T> list, IReadOnlyList<Guid> idList) where T : BaseEntity;
    }
}