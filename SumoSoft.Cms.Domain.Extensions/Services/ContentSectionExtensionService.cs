﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class ContentSectionExtensionService : IContentSectionExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedContent(ContentSection contentSection, string languageCode = null)
        {
            var content = contentSection.ContentSectionLocalizedKits.GetByLanguageCode(languageCode)?.Content;

            if (string.IsNullOrEmpty(content))
            {
                content = contentSection.ContentSectionLocalizedKits.GetByDefaultCountry()?.Content;
            }

            if (string.IsNullOrEmpty(content))
            {
                content = string.Empty;
            }

            return content;
        }
    }
}
