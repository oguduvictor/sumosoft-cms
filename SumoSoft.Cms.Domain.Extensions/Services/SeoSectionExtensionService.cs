﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class SeoSectionExtensionService : ISeoSectionExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedTitle(SeoSection seoSection, string languageCode = null)
        {
            return
                seoSection.SeoSectionLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                seoSection.SeoSectionLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedDescription(SeoSection seoSection, string languageCode = null)
        {
            return
                seoSection.SeoSectionLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                seoSection.SeoSectionLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedImage(SeoSection seoSection, string languageCode = null)
        {
            return
                seoSection.SeoSectionLocalizedKits.GetByLanguageCode(languageCode)?.Image ??
                seoSection.SeoSectionLocalizedKits.GetByDefaultCountry()?.Image ??
                string.Empty;
        }
    }
}
