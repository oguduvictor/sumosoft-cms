﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class ProductStockUnitExtensionService : IProductStockUnitExtensionService
    {
        /// <inheritdoc />
        public virtual bool HasCategories(ProductStockUnit productStockUnit, params string[] categoryNames)
        {
            return categoryNames.Intersect(productStockUnit.Categories.Select(x => x.Name)).Count() == categoryNames.Length;
        }

        /// <inheritdoc />
        public virtual bool HasCategories(ProductStockUnit productStockUnit, params Category[] categories)
        {
            return this.HasCategories(productStockUnit, categories.Select(x => x.Name).ToArray());
        }

        /// <inheritdoc />
        public virtual VariantOption GetVariantOptionByName(ProductStockUnit productStockUnit, string variantOptionName)
        {
            return productStockUnit.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }

        /// <inheritdoc />
        public virtual bool Matches(ProductStockUnit productStockUnit, List<VariantOption> selectedVariantOptions)
        {
            return productStockUnit.VariantOptions.All(x => selectedVariantOptions.Any(s => s.Id == x.Id));
        }

        /// <inheritdoc />
        public virtual double GetLocalizedBasePrice(ProductStockUnit productStockUnit, string languageCode = null)
        {
            return productStockUnit.ProductStockUnitLocalizedKits.GetByLanguageCode(languageCode)?.BasePrice ?? 0;
        }

        /// <inheritdoc />
        public virtual double GetLocalizedSalePrice(ProductStockUnit productStockUnit, string languageCode = null)
        {
            return productStockUnit.ProductStockUnitLocalizedKits.GetByLanguageCode(languageCode)?.SalePrice ?? 0;
        }

        /// <inheritdoc />
        public virtual double GetLocalizedMembershipSalePrice(ProductStockUnit productStockUnit, string languageCode = null)
        {
            return productStockUnit.ProductStockUnitLocalizedKits.GetByLanguageCode(languageCode)?.MembershipSalePrice ?? 0;
        }

    }
}
