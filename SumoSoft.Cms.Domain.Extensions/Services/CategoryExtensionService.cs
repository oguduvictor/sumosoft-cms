﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class CategoryExtensionService : ICategoryExtensionService
    {
        /// <inheritdoc />
        public virtual bool GetLocalizedIsDisabled(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.IsDisabled ??
                false;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedTitle(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedDescription(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedImage(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.Image ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.Image ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedFeaturedTitle(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.FeaturedTitle ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.FeaturedTitle ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedFeaturedDescription(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.FeaturedDescription ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.FeaturedDescription ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedFeaturedImage(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.FeaturedImage ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.FeaturedImage ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedMetaTitle(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.MetaTitle ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.MetaTitle ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedMetaDescription(Category category, string languageCode = null)
        {
            return
                category.CategoryLocalizedKits.GetByLanguageCode(languageCode)?.MetaDescription ??
                category.CategoryLocalizedKits.GetByDefaultCountry()?.MetaDescription ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetUrl(Category category)
        {
            var url = category.Url;

            var parent = category.Parent;

            while (parent != null)
            {
                url = string.Join("/", parent.Url, url);

                parent = parent.Parent;
            }

            return url;
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<Product> GetSortedProducts(Category category, bool includeDisabled)
        {
            var products = includeDisabled
                ? category.Products
                : category.Products.Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled());

            if (string.IsNullOrEmpty(category.ProductsSortOrder))
            {
                return products.OrderBy(x => x.Url);
            }

            var idList = category.ProductsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

            return products.OrderByIds(idList);
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<ProductImageKit> GetSortedProductImageKits(Category category, bool includeDisabled)
        {
            var productImageKits = includeDisabled
                ? category.ProductImageKits
                : category.ProductImageKits.Where(x => !x.IsDisabled && x.Product != null && !x.Product.IsDisabled && !x.Product.GetLocalizedIsDisabled());

            if (string.IsNullOrEmpty(category.ProductImageKitsSortOrder))
            {
                return productImageKits.OrderBy(x => x.Product?.Url);
            }

            var idList = category.ProductImageKitsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

            return productImageKits.OrderByIds(idList);
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<ProductStockUnit> GetSortedProductStockUnits(Category category, bool includeDisabled)
        {
            var productStockUnits = includeDisabled
                ? category.ProductStockUnits
                : category.ProductStockUnits.Where(x => !x.IsDisabled && x.Product != null && !x.Product.IsDisabled && !x.Product.GetLocalizedIsDisabled());

            if (string.IsNullOrEmpty(category.ProductStockUnitsSortOrder))
            {
                return productStockUnits.OrderBy(x => x.Product?.Url);
            }

            var idList = category.ProductStockUnitsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

            return productStockUnits.OrderByIds(idList);
        }
    }
}