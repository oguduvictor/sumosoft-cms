﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class ProductVariantExtensionService : IProductVariantExtensionService
    {
        /// <inheritdoc />
        public virtual VariantOption GetDefaultVariantOptionValue(ProductVariant productVariant)
        {
            return
                productVariant.DefaultVariantOptionValue ??
                productVariant.Variant?.DefaultVariantOptionValue ??
                productVariant.Variant?.VariantOptions.OrderBy(x => x.SortOrder).FirstOrDefault();
        }

        /// <inheritdoc />
        public virtual VariantOption GetVariantOptionByName(ProductVariant productVariant, string variantOptionName)
        {
            return productVariant.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }
    }
}