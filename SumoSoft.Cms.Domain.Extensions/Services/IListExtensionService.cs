﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <inheritdoc />
    public class IListExtensionService : IIListExtensionService
    {
        /// <inheritdoc />
        public virtual T GetByLanguageCode<T>(IList<T> localizedKits, string languageCode = null) where T : BaseEntity, ILocalizedKit
        {
            languageCode = languageCode ?? Thread.CurrentThread.CurrentCulture.Name;

            return localizedKits.FirstOrDefault(x => x.Country != null && x.Country.LanguageCode == languageCode);
        }

        /// <inheritdoc />
        public virtual T GetByDefaultCountry<T>(IList<T> localizedKits) where T : BaseEntity, ILocalizedKit
        {
            return localizedKits.FirstOrDefault(x => x.Country != null && x.Country.IsDefault);
        }

        /// <inheritdoc />
        public virtual List<T> FilterByDefaultCountry<T>(IList<T> localizedKits) where T : class, ILocalizedKit
        {
            return localizedKits.Where(x => x.Country?.IsDefault ?? false).ToList();
        }

        /// <inheritdoc />
        public virtual List<T> OrderByDefaultCountry<T>(IList<T> localizedKits) where T : ILocalizedKit
        {
            return localizedKits.OrderByDescending(x => x.Country?.IsDefault).ThenBy(x => x.Country?.Name).ToList();
        }

        /// <inheritdoc />
        public virtual List<T> OrderByCurrentCountry<T>(IList<T> localizedKits) where T : ILocalizedKit
        {
            return localizedKits.OrderByDescending(x => x.Country?.LanguageCode == Thread.CurrentThread.CurrentCulture.Name).ThenBy(x => x.Country.Name).ToList();
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<T> OrderBySortOrder<T>(IList<T> list) where T : ISortable
        {
            return list.OrderBy(x => x.SortOrder);
        }

        /// <inheritdoc />
        public virtual bool ContainsById<T>(IList<T> items, T item) where T : BaseEntity
        {
            return items.Select(x => x.Id).Contains(item.Id);
        }

        /// <inheritdoc />
        public virtual bool ContainsAllById<T>(IList<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Count() == args.Length;
        }

        /// <inheritdoc />
        public virtual bool ContainsAllById<T>(IList<T> items, List<T> args) where T : BaseEntity
        {
            return this.ContainsAllById(items, args.ToArray());
        }

        /// <inheritdoc />
        public virtual bool ContainsAnyById<T>(IList<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Any();
        }

        /// <inheritdoc />
        public virtual bool ContainsAnyById<T>(IList<T> items, List<T> args) where T : BaseEntity
        {
            return this.ContainsAnyById(items, args.ToArray());
        }
    }
}