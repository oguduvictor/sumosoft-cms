﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class DoubleExtensionService : IDoubleExtensionService
    {
        /// <inheritdoc />
        public virtual double AddTaxes(double netValue, IEnumerable<Tax> taxes)
        {
            double totalTax = 0;

            foreach (var tax in taxes)
            {
                if (tax.Amount > 0)
                {
                    totalTax = totalTax + tax.Amount.Value;
                }
                else
                {
                    totalTax = totalTax + netValue * tax.Percentage / 100;
                }
            }

            return netValue + totalTax;
        }

        /// <inheritdoc />
        public virtual double RemoveTaxes(double grossValue, IEnumerable<Tax> taxes)
        {
            double totalTax = 0;

            foreach (var tax in taxes)
            {
                if (tax.Amount > 0)
                {
                    totalTax = totalTax + tax.Amount.Value;
                }
                else
                {
                    totalTax = totalTax + (grossValue - grossValue * (100 / (100 + tax.Percentage)));
                }
            }

            return grossValue - totalTax;
        }
    }
}