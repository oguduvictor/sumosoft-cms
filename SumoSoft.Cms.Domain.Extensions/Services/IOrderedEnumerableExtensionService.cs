﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class IOrderedEnumerableExtensionService : IIOrderedEnumerableExtensionService
    {
        /// <inheritdoc />
        public virtual IOrderedEnumerable<Product> ThenByCategoryNames(IOrderedEnumerable<Product> products, List<string> categoryNames)
        {
            for (var i = 0; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                products = products.ThenByDescending(x => x.HasCategories(categoryName));
            }

            return products;
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<ProductImageKit> ThenByProductCategoryNames(IOrderedEnumerable<ProductImageKit> productImageKits, List<string> categoryNames)
        {
            for (var i = 0; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                productImageKits = productImageKits.ThenByDescending(x => x.Product.AsNotNull().HasCategories(categoryName));
            }

            return productImageKits;
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<T> ThenByIds<T>(IOrderedEnumerable<T> list, string ids) where T : BaseEntity
        {
            if (string.IsNullOrEmpty(ids))
            {
                return list;
            }

            var idList = ids.Split(',').Select(x => new Guid(x.Trim())).ToList();

            for (var i = 0; i < idList.Count; i++)
            {
                var id = idList.ElementAtOrDefault(i);

                list = list.ThenByDescending(x => x.Id == id);
            }

            return list;
        }
    }
}