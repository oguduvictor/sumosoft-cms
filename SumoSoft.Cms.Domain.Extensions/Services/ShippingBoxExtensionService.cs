﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class ShippingBoxExtensionService : IShippingBoxExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedTitle(ShippingBox shippingBox, string languageCode = null)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedDescription(ShippingBox shippingBox, string languageCode = null)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedInternalDescription(ShippingBox shippingBox, string languageCode = null)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.InternalDescription ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.InternalDescription ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual double GetLocalizedPrice(ShippingBox shippingBox, string languageCode = null)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.Price ?? 0;
        }

        /// <inheritdoc />
        public virtual int GetLocalizedMinDays(ShippingBox shippingBox, string languageCode = null)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.MinDays ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.MinDays ??
                0;
        }

        /// <inheritdoc />
        public virtual int GetLocalizedMaxDays(ShippingBox shippingBox, string languageCode = null)
        {
            return
                shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.MaxDays ??
                shippingBox.ShippingBoxLocalizedKits.GetByDefaultCountry()?.MaxDays ??
                0;
        }

        /// <inheritdoc />
        public virtual bool GetLocalizedCountWeekends(ShippingBox shippingBox, string languageCode = null)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.CountWeekends ?? false;
        }

        /// <inheritdoc />
        public virtual double GetLocalizedFreeShippingMinimumPrice(ShippingBox shippingBox, string languageCode = null)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.FreeShippingMinimumPrice ?? 0;
        }

        /// <inheritdoc />
        public virtual double GetShippingPriceBeforeTax(ShippingBox shippingBox, string languageCode = null, double allCartItemsTotal = 0)
        {
            var freeShippingMinimumPrice = this.GetLocalizedFreeShippingMinimumPrice(shippingBox, languageCode);

            if (freeShippingMinimumPrice > 0 && allCartItemsTotal > freeShippingMinimumPrice)
            {
                return 0;
            }

            return this.GetLocalizedPrice(shippingBox, languageCode);
        }

        /// <inheritdoc />
        public virtual double GetShippingPriceAfterTax(ShippingBox shippingBox, IEnumerable<Tax> countryTaxes, string languageCode = null, double allCartItemsTotal = 0)
        {
            var shippingPriceBeforeTax = this.GetShippingPriceBeforeTax(shippingBox, languageCode, allCartItemsTotal);

            var shippingTaxes = countryTaxes.AsNotNull().Where(x => x.ApplyToShippingPrice).ToList();

            return shippingPriceBeforeTax.AddTaxes(shippingTaxes);
        }

        /// <inheritdoc />
        public virtual bool GetLocalizedIsDisabled(ShippingBox shippingBox, string languageCode)
        {
            return shippingBox.ShippingBoxLocalizedKits.GetByLanguageCode(languageCode)?.IsDisabled ?? false;
        }
    }
}