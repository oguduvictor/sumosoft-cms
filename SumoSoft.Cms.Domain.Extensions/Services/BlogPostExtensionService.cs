﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Text.RegularExpressions;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class BlogPostExtensionService : IBlogPostExtensionService
    {
        /// <inheritdoc />
        public virtual string GetContentPreview(BlogPost blogPost, int length)
        {
            var result = blogPost.Content;

            if (string.IsNullOrEmpty(result))
            {
                return string.Empty;
            }

            result = Regex.Replace(result, @"<[^>]*(>|$)", string.Empty); // remove html tags
            result = Regex.Replace(result, @"\s+", " "); // remove line breaks and tabs

            result = System.Web.HttpUtility.HtmlDecode(result);

            if (result.Length > length)
            {
                result = result.Substring(0, result.Substring(0, length).LastIndexOf(" ", StringComparison.Ordinal)); // get substring without cutting words
                result = result + "..."; // add ellipsis
            }

            return result;
        }
    }
}
