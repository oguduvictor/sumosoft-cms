﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Domain.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class ITaggableExtensionService : IITaggableExtensionService
    {
        /// <inheritdoc />
        public virtual List<string> GetTagList(ITaggable iTaggable)
        {
            return string.IsNullOrEmpty(iTaggable.Tags)
                ? new List<string>()
                : $"{iTaggable.Tags},{iTaggable.AutoTags}".Split(',').ToList();
        }

        /// <inheritdoc />
        public virtual bool HasTags(ITaggable iTaggable, params string[] tags)
        {
            return this.GetTagList(iTaggable).ContainsAll(tags);
        }
    }
}