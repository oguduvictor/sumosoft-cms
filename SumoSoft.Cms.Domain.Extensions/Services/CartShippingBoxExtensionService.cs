﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class CartShippingBoxExtensionService : ICartShippingBoxExtensionService
    {
        private readonly CountryServices.Interfaces.ICountryService countryService;

        /// <inheritdoc />
        public CartShippingBoxExtensionService(
            CountryServices.Interfaces.ICountryService countryService)
        {
            this.countryService = countryService;
        }

        /// <inheritdoc />
        public virtual double GetShippingPriceBeforeTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, string languageCode = null)
        {
            var cartItemsTotalBeforeTax = cartShippingBox.Cart?.CartShippingBoxes.SelectMany(x => x.CartItems).Sum(c => c.GetTotalBeforeTax(dbContext, languageCode)) ?? 0;

            return cartShippingBox.ShippingBox?.GetShippingPriceBeforeTax(languageCode, cartItemsTotalBeforeTax) ?? 0;
        }

        /// <inheritdoc />
        public virtual double GetShippingPriceAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            var cartItemsTotalAfterTax = cartShippingBox.Cart?.CartShippingBoxes.SelectMany(x => x.CartItems).Sum(c => c.GetTotalAfterTax(dbContext, countryTaxes, languageCode)) ?? 0;

            return cartShippingBox.ShippingBox?.GetShippingPriceAfterTax(countryTaxes, languageCode, cartItemsTotalAfterTax) ?? 0;
        }

        /// <inheritdoc />
        public virtual double GetTotalBeforeTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, string languageCode = null)
        {
            var cartItemsTotalBeforeTax = cartShippingBox.CartItems.Sum(cartItem => cartItem.GetTotalBeforeTax(dbContext, languageCode));

            var shippingPriceBeforeTax = this.GetShippingPriceBeforeTax(dbContext, cartShippingBox, languageCode);

            return cartItemsTotalBeforeTax + shippingPriceBeforeTax;
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            var cartItemsTotalAfterTax = cartShippingBox.CartItems.Sum(cartItem => cartItem.GetTotalAfterTax(dbContext, countryTaxes, languageCode));

            var shippingPriceAfterTax = this.GetShippingPriceAfterTax(dbContext, cartShippingBox, countryTaxes, languageCode);

            return cartItemsTotalAfterTax + shippingPriceAfterTax;
        }

        /// <inheritdoc />
        public virtual double GetShippingPriceAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox)
        {
            var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cartShippingBox?.Cart);

            var countryTaxes = shippingCountryOrCurrentCountry.Taxes.Where(x => x.CartItem == null);

            return this.GetShippingPriceAfterTax(dbContext, cartShippingBox, countryTaxes);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, CartShippingBox cartShippingBox)
        {
            var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cartShippingBox?.Cart);

            var countryTaxes = shippingCountryOrCurrentCountry.Taxes.Where(x => x.CartItem == null);

            return this.GetTotalAfterTax(dbContext, cartShippingBox, countryTaxes);
        }
    }
}