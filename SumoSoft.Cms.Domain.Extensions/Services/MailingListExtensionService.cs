﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    public class MailingListExtensionService : IMailingListExtensionService
    {
        public string GetLocalizedDescription(MailingList mailingList, string languageCode)
        {
            return
                mailingList.MailingListLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                mailingList.MailingListLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        public bool GetLocalizedIsDisabled(MailingList mailingList, string languageCode)
        {
            return mailingList.MailingListLocalizedKits.GetByLanguageCode(languageCode)?.IsDisabled ?? false;
        }

        public string GetLocalizedTitle(MailingList mailingList, string languageCode)
        {
            return
                mailingList.MailingListLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                mailingList.MailingListLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }
    }
}
