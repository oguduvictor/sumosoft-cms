﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class AttributeOptionExtensionService : IAttributeOptionExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedTitle(AttributeOption attributeOption, string languageCode = null)
        {
            return
                attributeOption.AttributeOptionLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                attributeOption.AttributeOptionLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }
    }
}
