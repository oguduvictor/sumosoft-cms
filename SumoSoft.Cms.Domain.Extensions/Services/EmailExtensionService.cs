﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class EmailExtensionService : IEmailExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedFrom(Email email, string languageCode = null)
        {
            return
                email.EmailLocalizedKits.GetByLanguageCode(languageCode)?.From ??
                email.EmailLocalizedKits.GetByDefaultCountry()?.From ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedReplyTo(Email email, string languageCode = null)
        {
            return
                email.EmailLocalizedKits.GetByLanguageCode(languageCode)?.ReplyTo ??
                email.EmailLocalizedKits.GetByDefaultCountry()?.ReplyTo ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedDisplayName(Email email, string languageCode = null)
        {
            return
                email.EmailLocalizedKits.GetByLanguageCode(languageCode)?.DisplayName ??
                email.EmailLocalizedKits.GetByDefaultCountry()?.DisplayName ??
                string.Empty;
        }
    }
}