﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Domain.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class IEnumerableExtensionService : IIEnumerableExtensionService
    {
        /// <inheritdoc />
        public virtual bool ContainsById<T>(IEnumerable<T> items, T item) where T : BaseEntity
        {
            return items.Select(x => x.Id).Contains(item.Id);
        }

        /// <inheritdoc />
        public virtual bool ContainsAnyById<T>(IEnumerable<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Any();
        }

        /// <inheritdoc />
        public virtual bool ContainsAnyById<T>(IEnumerable<T> items, List<T> args) where T : BaseEntity
        {
            return this.ContainsAnyById(items, args.ToArray());
        }

        /// <inheritdoc />
        public virtual bool ContainsAllById<T>(IEnumerable<T> items, params T[] args) where T : BaseEntity
        {
            return items.Select(x => x.Id).Intersect(args.Select(x => x.Id)).Count() == args.Length;
        }

        /// <inheritdoc />
        public virtual bool ContainsAllById<T>(IEnumerable<T> items, List<T> args) where T : BaseEntity
        {
            return this.ContainsAllById(items, args.ToArray());
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<T> OrderBySortOrder<T>(IEnumerable<T> items) where T : ISortable
        {
            return items.OrderBy(x => x.SortOrder);
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<Product> OrderByCategoryNames(IEnumerable<Product> products, List<string> categoryNames)
        {
            var result = products.OrderByDescending(x => x.HasCategories(categoryNames.ElementAtOrDefault(0)));

            for (var i = 1; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                result = result.ThenByDescending(x => x.HasCategories(categoryName));
            }

            return result;
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<ProductImageKit> OrderByProductCategoryNames(IEnumerable<ProductImageKit> productImageKits, List<string> categoryNames)
        {
            var result = productImageKits.OrderByDescending(x => x.Product.AsNotNull().HasCategories(categoryNames.ElementAtOrDefault(0)));

            for (var i = 1; i < categoryNames.Count; i++)
            {
                var categoryName = categoryNames.ElementAtOrDefault(i);

                result = result.ThenByDescending(x => x.Product.AsNotNull().HasCategories(categoryName));
            }

            return result;
        }

        /// <inheritdoc />
        public virtual IOrderedEnumerable<T> OrderByIds<T>(IEnumerable<T> list, IReadOnlyList<Guid> idList) where T : BaseEntity
        {
            var result = list.OrderByDescending(x => x.Id == idList.ElementAtOrDefault(0));

            for (var i = 1; i < idList.Count; i++)
            {
                var id = idList.ElementAtOrDefault(i);

                result = result.ThenByDescending(x => x.Id == id);
            }

            return result;
        }
    }
}