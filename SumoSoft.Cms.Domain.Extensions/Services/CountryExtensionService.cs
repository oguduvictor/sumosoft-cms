﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Globalization;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class CountryExtensionService : ICountryExtensionService
    {
        /// <inheritdoc />
        public virtual CultureInfo GetCultureInfo(Country country)
        {
            try
            {
                return CultureInfo.CreateSpecificCulture(country.LanguageCode ?? string.Empty);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <inheritdoc />
        public virtual RegionInfo GetRegionInfo(Country country)
        {
            var cultureInfo = this.GetCultureInfo(country);

            if (cultureInfo == null)
            {
                return null;
            }

            try
            {
                return new RegionInfo(cultureInfo.LCID);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <inheritdoc />
        public virtual string GetCurrencySymbol(Country country)
        {
            return this.GetCultureInfo(country)?.NumberFormat.CurrencySymbol;
        }

        /// <inheritdoc />
        public virtual string GetIso4217CurrencySymbol(Country country)
        {
            return this.GetRegionInfo(country)?.ISOCurrencySymbol;
        }

        /// <inheritdoc />
        public virtual string GetUrlForStringConcatenation(Country country)
        {
            return string.IsNullOrEmpty(country.Url) ? string.Empty : "/" + country.Url;
        }
    }
}