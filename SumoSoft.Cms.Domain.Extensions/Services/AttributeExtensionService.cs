﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class AttributeExtensionService : IAttributeExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedTitle(Attribute attribute, string languageCode = null)
        {
            return
                attribute.AttributeLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                attribute.AttributeLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }
    }
}
