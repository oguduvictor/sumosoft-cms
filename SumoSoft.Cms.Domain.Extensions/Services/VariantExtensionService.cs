﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class VariantExtensionService : IVariantExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedDescription(Variant variant, string languageCode = null)
        {
            return
                variant.VariantLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                variant.VariantLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedTitle(Variant variant, string languageCode = null)
        {
            return
                variant.VariantLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                variant.VariantLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual VariantOption GetVariantOptionByName(Variant variant, string variantOptionName)
        {
            return variant.VariantOptions.FirstOrDefault(x => x.Name == variantOptionName);
        }

        /// <inheritdoc />
        public virtual VariantOption GetVariantOptionByUrl(Variant variant, string variantOptionUrl)
        {
            return variant.VariantOptions.FirstOrDefault(x => x.Url == variantOptionUrl);
        }
    }
}