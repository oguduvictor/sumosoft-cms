﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class VariantOptionExtensionService : IVariantOptionExtensionService
    {
        /// <inheritdoc />
        public virtual bool GetLocalizedIsDisabled(VariantOption variantOption, string languageCode = null)
        {
            return
                variantOption.VariantOptionLocalizedKits.GetByLanguageCode(languageCode)?.IsDisabled ??
                false;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedTitle(VariantOption variantOption, string languageCode = null)
        {
            return
                variantOption.VariantOptionLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                variantOption.VariantOptionLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedDescription(VariantOption variantOption, string languageCode = null)
        {
            return
                variantOption.VariantOptionLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                variantOption.VariantOptionLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual double GetLocalizedPriceModifier(VariantOption variantOption, string languageCode = null)
        {
            return variantOption.VariantOptionLocalizedKits.GetByLanguageCode(languageCode)?.PriceModifier ?? 0;
        }
    }
}