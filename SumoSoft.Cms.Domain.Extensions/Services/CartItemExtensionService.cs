﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class CartItemExtensionService : ICartItemExtensionService
    {
        private readonly CountryServices.Interfaces.ICountryService countryService;

        /// <inheritdoc />
        public CartItemExtensionService(
            CountryServices.Interfaces.ICountryService countryService)
        {
            this.countryService = countryService;
        }

        /// <inheritdoc />
        public virtual CartItemVariant GetCartItemVariantByName(CartItem cartItem, string variantName)
        {
            return cartItem.CartItemVariants.FirstOrDefault(x => x.Variant?.Name == variantName);
        }

        /// <inheritdoc />
        public virtual ProductAttribute GetProductAttributeByName(CartItem cartItem, string attributeName)
        {
            return cartItem.Product?.GetProductAttributeByName(attributeName);
        }

        /// <inheritdoc />
        public virtual ProductVariant GetProductVariantByName(CartItem cartItem, string variantName)
        {
            return cartItem.Product?.GetProductVariantByName(variantName);
        }

        /// <inheritdoc />
        public virtual List<VariantOption> GetSelectedVariantOptions(CartItem cartItem)
        {
            return cartItem.CartItemVariants.Where(x => x.VariantOptionValue != null).Select(y => y.VariantOptionValue).ToList();
        }

        /// <inheritdoc />
        public virtual ProductImageKit GetProductImageKit(CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            selectedVariantOptions = selectedVariantOptions ?? this.GetSelectedVariantOptions(cartItem);

            return cartItem.Product?.GetProductImageKit(selectedVariantOptions);
        }

        /// <inheritdoc />
        public virtual ProductStockUnit GetProductStockUnit(CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            selectedVariantOptions = selectedVariantOptions ?? this.GetSelectedVariantOptions(cartItem);

            return cartItem.Product?.GetProductStockUnit(selectedVariantOptions);
        }

        /// <inheritdoc />
        public virtual int GetStock(CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            selectedVariantOptions = selectedVariantOptions ?? this.GetSelectedVariantOptions(cartItem);

            return this.GetProductStockUnit(cartItem, selectedVariantOptions)?.Stock ?? 0;
        }

        /// <inheritdoc />
        public virtual bool IsOutOfStock(CartItem cartItem, List<VariantOption> selectedVariantOptions = null)
        {
            if (cartItem.Product == null)
            {
                return true;
            }

            if (!cartItem.Product.ProductStockUnits.Any())
            {
                return true;
            }

            selectedVariantOptions = selectedVariantOptions ?? this.GetSelectedVariantOptions(cartItem);

            var productStockUnit = this.GetProductStockUnit(cartItem, selectedVariantOptions);

            if (productStockUnit == null)
            {
                return true;
            }

            return productStockUnit.Stock < 1;
        }

        /// <inheritdoc />
        public virtual DateTime GetMinEta(CartItem cartItem, string languageCode = null)
        {
            var productStockUnit = this.GetProductStockUnit(cartItem) ?? throw new Exception($"{cartItem.Product?.GetLocalizedTitle()} ProductStockUnit cannot be null");

            var shippingBox = cartItem.CartShippingBox?.ShippingBox ?? throw new Exception("ShippingBox cannot be null");

            var startDate = productStockUnit.DispatchDate > DateTime.Now
                ? productStockUnit.DispatchDate
                : DateTime.Now;

            return shippingBox.GetLocalizedCountWeekends(languageCode)
                ? startDate.AddWorkingDays(productStockUnit.DispatchTime).AddDays(shippingBox.GetLocalizedMinDays(languageCode))
                : startDate.AddWorkingDays(productStockUnit.DispatchTime).AddWorkingDays(shippingBox.GetLocalizedMinDays(languageCode));
        }

        /// <inheritdoc />
        public virtual DateTime GetMaxEta(CartItem cartItem, string languageCode = null)
        {
            var productStockUnit = this.GetProductStockUnit(cartItem) ?? throw new Exception($"{cartItem.Product?.GetLocalizedTitle()} ProductStockUnit cannot be null");

            var shippingBox = cartItem.CartShippingBox?.ShippingBox ?? throw new Exception("ShippingBox cannot be null");

            var startDate = productStockUnit.DispatchDate > DateTime.Now
                ? productStockUnit.DispatchDate
                : DateTime.Now;

            return shippingBox.GetLocalizedCountWeekends(languageCode)
                ? startDate.AddWorkingDays(productStockUnit.DispatchTime).AddDays(shippingBox.GetLocalizedMaxDays(languageCode))
                : startDate.AddWorkingDays(productStockUnit.DispatchTime).AddWorkingDays(shippingBox.GetLocalizedMaxDays(languageCode));
        }

        /// <inheritdoc />
        public virtual bool CouponApplies(CmsDbContext dbContext, CartItem cartItem, string languageCode = null)
        {
            var coupon = cartItem.CartShippingBox?.Cart?.Coupon;

            if (coupon == null)
            {
                return false;
            }

            if (!coupon.AllowOnSale)
            {
                if (this.GetSubtotalBeforeTaxWithoutSalePrice(dbContext, cartItem, languageCode) - this.GetSubtotalBeforeTax(dbContext, cartItem, languageCode) > 0)
                {
                    return false;
                }
            }

            if (!coupon.Categories.Any())
            {
                return true;
            }

            foreach (var couponCategory in coupon.Categories)
            {
                if (cartItem.Product != null && cartItem.Product.HasCategories(couponCategory))
                {
                    return true;
                }

                var productImageKit = this.GetProductImageKit(cartItem);

                if (productImageKit != null && productImageKit.HasCategories(couponCategory))
                {
                    return true;
                }

                var productStockUnit = this.GetProductStockUnit(cartItem);

                if (productStockUnit != null && productStockUnit.HasCategories(couponCategory))
                {
                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc />
        public virtual double GetSubtotalBeforeTax(CmsDbContext dbContext, CartItem cartItem, string languageCode = null)
        {
            var selectedVariantOptions = cartItem.CartItemVariants.Where(x => x.VariantOptionValue != null).Select(x => x.VariantOptionValue).ToList();

            var productTotalBeforeTax = cartItem.Product?.GetTotalBeforeTax(dbContext, selectedVariantOptions, languageCode) ?? 0;

            var subtotalBeforeTax = productTotalBeforeTax * cartItem.Quantity;

            return subtotalBeforeTax.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, CartItem cartItem, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            var totalBeforeTax = this.GetTotalBeforeTax(dbContext, cartItem, languageCode);

            var taxes = this.GetTaxes(cartItem, countryTaxes);

            return totalBeforeTax.AddTaxes(taxes);
        }

        /// <inheritdoc />
        public virtual double GetTotalBeforeTax(CmsDbContext dbContext, CartItem cartItem, string languageCode = null)
        {
            var subtotalBeforeTax = this.GetSubtotalBeforeTax(dbContext, cartItem, languageCode);

            var couponValue = this.GetCouponShareValue(dbContext, cartItem, languageCode);

            var total = subtotalBeforeTax - couponValue;

            return total.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual List<Tax> GetTaxes(CartItem cartItem, IEnumerable<Tax> countryTaxes)
        {
            var taxes = cartItem.TaxesOverride.Any() ? cartItem.TaxesOverride : countryTaxes;

            var productTaxes = taxes.AsNotNull().Where(x => x.ApplyToProductPrice).ToList();

            return productTaxes;
        }

        /// <inheritdoc />
        public virtual double GetCouponShareValue(CmsDbContext dbContext, CartItem cartItem, string languageCode = null)
        {
            var coupon = cartItem.CartShippingBox?.Cart?.Coupon;

            if (coupon == null)
            {
                return 0;
            }

            if (this.CouponApplies(dbContext, cartItem, languageCode))
            {
                var couponCartItems = cartItem.CartShippingBox?.Cart?.CartShippingBoxes
                    .SelectMany(x => x.CartItems)
                    .Where(c => this.CouponApplies(dbContext, c, languageCode))
                    .ToList();

                var couponCartItemsSubtotalBeforeTax = couponCartItems?.Sum(c => this.GetSubtotalBeforeTax(dbContext, c, languageCode)) ?? 0;

                var couponValue = cartItem.CartShippingBox?.Cart.GetCouponValue(dbContext, languageCode) ?? 0;

                var coefficient = this.GetSubtotalBeforeTax(dbContext, cartItem, languageCode) / couponCartItemsSubtotalBeforeTax;

                return couponValue * coefficient;
            }

            return 0;
        }

        /// <inheritdoc />
        public virtual double GetSubtotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, CartItem cartItem, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            var taxes = this.GetTaxes(cartItem, countryTaxes);

            return this.GetSubtotalBeforeTaxWithoutSalePrice(dbContext, cartItem, languageCode).AddTaxes(taxes);
        }

        /// <inheritdoc />
        public virtual double GetSubtotalBeforeTaxWithoutSalePrice(CmsDbContext dbContext, CartItem cartItem, string languageCode = null)
        {
            var selectedVariantOptions = cartItem.CartItemVariants.Where(x => x.VariantOptionValue != null).Select(x => x.VariantOptionValue).ToList();

            var productTotalBeforeTaxWithoutSalePrice = cartItem.Product?.GetTotalBeforeTaxWithoutSalePrice(dbContext, selectedVariantOptions, languageCode) ?? 0;

            return productTotalBeforeTaxWithoutSalePrice * cartItem.Quantity;
        }

        /// <inheritdoc />
        public virtual double GetSubtotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, CartItem cartItem)
        {
            var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cartItem.CartShippingBox?.Cart);

            var countryTaxes = shippingCountryOrCurrentCountry.Taxes.Where(x => x.CartItem == null);

            return this.GetSubtotalAfterTaxWithoutSalePrice(dbContext, cartItem, countryTaxes);
        }

        /// <inheritdoc />
        public virtual double GetSubtotalAfterTax(CmsDbContext dbContext, CartItem cartItem, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            var taxes = this.GetTaxes(cartItem, countryTaxes);

            return this.GetSubtotalBeforeTax(dbContext, cartItem, languageCode).AddTaxes(taxes);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, CartItem cartItem)
        {
            var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cartItem.CartShippingBox?.Cart);

            var countryTaxes = shippingCountryOrCurrentCountry.Taxes.Where(x => x.CartItem == null);

            return this.GetTotalAfterTax(dbContext, cartItem, countryTaxes);
        }

        /// <inheritdoc />
        public virtual double GetSubtotalAfterTax(CmsDbContext dbContext, CartItem cartItem)
        {
            var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cartItem.CartShippingBox?.Cart);

            var countryTaxes = shippingCountryOrCurrentCountry.Taxes.Where(x => x.CartItem == null);

            return this.GetSubtotalAfterTax(dbContext, cartItem, countryTaxes);
        }
    }
}
