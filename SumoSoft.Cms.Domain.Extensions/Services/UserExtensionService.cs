﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class UserExtensionService : IUserExtensionService
    {
        /// <inheritdoc />
        public virtual ICollection<UserCredit> GetLocalizedUserCredits(User user, string languageCode = null)
        {
            return user.UserLocalizedKits.GetByLanguageCode(languageCode)?.UserCredits.OrderByDescending(x => x.CreatedDate).ToList() ?? new List<UserCredit>();
        }

        /// <inheritdoc />
        public virtual UserAttribute GetUserAttributeByName(User user, string attributeName)
        {
            return user.UserAttributes.FirstOrDefault(x => x.Attribute?.Name == attributeName);
        }
    }
}