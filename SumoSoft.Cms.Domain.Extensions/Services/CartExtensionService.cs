﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.Constants.ContentSectionSchemas;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Extensions.Dto;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class CartExtensionService : ICartExtensionService
    {
        private readonly ICountryService countryService;
        private readonly IContentSectionService contentSectionService;

        /// <inheritdoc />
        public CartExtensionService(
            ICountryService countryService,
            IContentSectionService contentSectionService)
        {
            this.countryService = countryService;
            this.contentSectionService = contentSectionService;
        }

        /// <inheritdoc />
        public virtual CartShippingBox GetCartShippingBoxByName(Cart cart, string shippingBoxName)
        {
            return cart.CartShippingBoxes.FirstOrDefault(x => x.ShippingBox?.Name == shippingBoxName);
        }

        /// <inheritdoc />
        public virtual double GetCouponValue(CmsDbContext dbContext, Cart cart, string languageCode = null)
        {
            if (cart.Coupon == null)
            {
                return 0;
            }

            var couponCartItems = cart.CartShippingBoxes.SelectMany(x => x.CartItems).Where(c => c.CouponApplies(dbContext, languageCode));

            var cartItemsSubtotalBeforeTax = couponCartItems.Sum(y => y.GetSubtotalBeforeTax(dbContext, languageCode));

            if (cart.Coupon.Amount > 0)
            {
                return cart.Coupon.Amount.ToMaximum(cartItemsSubtotalBeforeTax);
            }

            return cartItemsSubtotalBeforeTax.CalculatePercentage((double)cart.Coupon?.Percentage);
        }

        /// <inheritdoc />
        public virtual double GetTotalBeforeTax(CmsDbContext dbContext, Cart cart, string languageCode = null)
        {
            var totalBeforeTax = cart.CartShippingBoxes.Sum(x => x.GetTotalBeforeTax(dbContext, languageCode));

            return totalBeforeTax.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, Cart cart, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            var totalAfterTax = cart.CartShippingBoxes.Sum(x => x.GetTotalAfterTax(dbContext, countryTaxes, languageCode));

            return totalAfterTax.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, Cart cart)
        {
            var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cart);

            var taxes = shippingCountryOrCurrentCountry.Taxes.Where(x => x.CartItem == null);

            return this.GetTotalAfterTax(dbContext, cart, taxes);
        }

        /// <inheritdoc />
        public virtual double GetTotalToBePaid(CmsDbContext dbContext, Cart cart, IEnumerable<Tax> countryTaxes, string languageCode = null)
        {
            var userCreditAmount = cart.UserCredit?.Amount ?? 0;

            var total = this.GetTotalAfterTax(dbContext, cart, countryTaxes, languageCode) + userCreditAmount;

            return total.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetTotalToBePaid(CmsDbContext dbContext, Cart cart)
        {
            var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cart);

            var taxes = shippingCountryOrCurrentCountry.Taxes.Where(x => x.CartItem == null);

            return this.GetTotalToBePaid(dbContext, cart, taxes);
        }

        /// <inheritdoc />
        public virtual List<CartError> GetErrors(CmsDbContext dbContext, Cart cart)
        {
            var errors = new List<CartError>();

            var shippingAddress = cart.ShippingAddress;

            var currentCountry = this.countryService.GetCurrentCountry(dbContext);

            var shippingAddressCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cart);

            var contentSectionFallBackValue = new Cms_CartErrors
            {
                CartItemWithDisabledProduct = "The product [[Product]] is no longer available.",
                CartItemWithOutOfStockProduct = "The product [[Product]] is out of stock.",
                CartItemWithInvalidShippingBox = "The product [[Product]] cannot be shipped to [[ShippingCountry]] using the current shipping method.",
                InvalidCoupon = "Invalid Coupon.",
                InvalidTaxOverride = "The current Tax Override is obsolete.",
                InvalidUserCredit = "Your User Credits cannot be used in the current country.",
                LocalizedShippingAddressCountryNotMatchingCurrentCountry = "When the ShippingAddress Country is localized, the current Country should match.",
                NonLocalizedShippingAddressCountryNotMatchingDefaultCountry = "When the ShippingAddress Country is not localized, the current Country should be the default one.",
                ShippingAddressRequired = "Please select a shipping address."
            }.ToCamelCaseJson();

            var contentSection = this.contentSectionService.ContentSectionAs<Cms_CartErrors>(dbContext,
                ContentSectionName.Cms_CartErrors,
                contentSectionFallBackValue, true);

            // -------------------------------------------------------
            // Check if the ShippingAddress is required
            // -------------------------------------------------------

            if (shippingAddress == null && cart.CartShippingBoxes.Any(csb => csb.ShippingBox?.RequiresShippingAddress ?? false))
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.ShippingAddressRequired,
                    Message = contentSection.ShippingAddressRequired
                });
            }

            // -------------------------------------------------------
            // Check if any Product has been disabled
            // -------------------------------------------------------

            var cartItemsWithDisabledProducts = cart.CartShippingBoxes
                .SelectMany(c => c.CartItems)
                .Where(ci =>
                    ci.Product == null ||
                    ci.Product.IsDisabled ||
                    ci.Product.GetLocalizedIsDisabled())
                .ToList();

            foreach (var cartItemWithDisabledProduct in cartItemsWithDisabledProducts)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.CartItemWithDisabledProduct,
                    Message = contentSection.CartItemWithDisabledProduct?
                        .Replace("[[Product]]", cartItemWithDisabledProduct.Product.GetLocalizedTitle())
                        .Replace("[[ShippingCountry]]", shippingAddressCountryOrCurrentCountry.Name),
                    TargetId = cartItemWithDisabledProduct.Id
                });
            }

            // -------------------------------------------------------
            // Check if any CartItem is out of stock
            // -------------------------------------------------------

            var cartItemWithOutOfStockProducts = cart.CartShippingBoxes
                .SelectMany(c => c.CartItems)
                .Where(ci => ci.IsOutOfStock())
                .ToList();

            foreach (var cartItemWithOutOfStockProduct in cartItemWithOutOfStockProducts)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.CartItemWithOutOfStockProduct,
                    Message = contentSection.CartItemWithOutOfStockProduct?.Replace("[[Product]]", cartItemWithOutOfStockProduct.Product.GetLocalizedTitle()),
                    TargetId = cartItemWithOutOfStockProduct.Id,
                });
            }

            // -------------------------------------------------------
            // Check if the ShippingBoxes are
            // compatible with the shipping Country.
            // -------------------------------------------------------

            var cartItemsWithInvalidShippingBox = cart.CartShippingBoxes
                .Where(x =>
                    x.ShippingBox == null ||
                    !x.ShippingBox.Countries.ContainsById(shippingAddressCountryOrCurrentCountry))
                .SelectMany(x => x.CartItems)
                .ToList();

            foreach (var cartItemWithInvalidShippingBox in cartItemsWithInvalidShippingBox)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.CartItemWithInvalidShippingBox,
                    Message = contentSection.CartItemWithInvalidShippingBox?.Replace("[[Product]]", cartItemWithInvalidShippingBox.Product.GetLocalizedTitle()),
                    TargetId = cartItemWithInvalidShippingBox.Id
                });
            }

            // -------------------------------------------------------
            // Check if coupon is still valid
            // -------------------------------------------------------

            var coupon = cart.Coupon;

            if (coupon != null)
            {
                if (coupon.Country != null && coupon.Country.LanguageCode != this.countryService.GetCurrentLanguageCode() ||
                    coupon.ExpirationDate < DateTime.Now ||
                    !coupon.Published)
                {
                    errors.Add(new CartError
                    {
                        Code = CartErrorCode.InvalidCoupon,
                        Message = contentSection.InvalidCoupon,
                        TargetId = coupon.Id
                    });
                }
            }

            // -------------------------------------------------------
            // Check if the Shipping country is
            // compatible with the current Country.
            // -------------------------------------------------------

            if (shippingAddressCountryOrCurrentCountry.Localize &&
                shippingAddressCountryOrCurrentCountry.Id != currentCountry.Id &&
                !shippingAddressCountryOrCurrentCountry.IsDisabled)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.LocalizedShippingAddressCountryNotMatchingCurrentCountry,
                    Message = contentSection.LocalizedShippingAddressCountryNotMatchingCurrentCountry,
                    TargetId = null
                });
            }

            if ((shippingAddressCountryOrCurrentCountry.IsDisabled || !shippingAddressCountryOrCurrentCountry.Localize) &&
                !currentCountry.IsDefault)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.NonLocalizedShippingAddressCountryNotMatchingDefaultCountry,
                    Message = contentSection.NonLocalizedShippingAddressCountryNotMatchingDefaultCountry,
                    TargetId = null
                });
            }

            // -------------------------------------------------------
            // Check if the TaxesOverride are still valid
            // -------------------------------------------------------

            var invalidTaxesOverride = cart.CartShippingBoxes
                .SelectMany(x => x.CartItems)
                .SelectMany(c => c.TaxesOverride)
                .Where(t =>
                    t.ShippingAddress?.Id != shippingAddress?.Id ||
                    t.ShippingAddress?.ModifiedDate > shippingAddress?.ModifiedDate)
                .ToList();

            foreach (var tax in invalidTaxesOverride)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.InvalidTaxOverride,
                    Message = contentSection.InvalidTaxOverride,
                    TargetId = tax.Id
                });
            }

            // -------------------------------------------------------
            // Check if the UserCredit localized kit Country
            // matched the current country
            // -------------------------------------------------------

            var userCreditLocalizedKitCountryId = cart.UserCredit?.UserLocalizedKit?.Country?.Id;

            if (userCreditLocalizedKitCountryId != null && userCreditLocalizedKitCountryId != currentCountry.Id)
            {
                errors.Add(new CartError
                {
                    Code = CartErrorCode.InvalidUserCredit,
                    Message = contentSection.InvalidUserCredit,
                    TargetId = cart.UserCredit.Id
                });
            }

            return errors;
        }
    }
}