﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class CartItemVariantExtensionService : ICartItemVariantExtensionService
    {
        /// <inheritdoc />
        public virtual double GetPriceModifier(CartItemVariant cartItemVariant, string languageCode = null)
        {
            return cartItemVariant.VariantOptionValue?.GetLocalizedPriceModifier(languageCode) ?? 0;
        }
    }
}