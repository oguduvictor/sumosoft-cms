﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class ProductExtensionService : IProductExtensionService
    {
        private readonly IAuthenticationService authenticationService;
        private readonly CountryServices.Interfaces.ICountryService countryService;

        /// <inheritdoc />
        public ProductExtensionService(
            IAuthenticationService authenticationService,
            CountryServices.Interfaces.ICountryService countryService)
        {
            this.authenticationService = authenticationService;
            this.countryService = countryService;
        }

        /// <inheritdoc />
        public virtual bool HasCategories(Product product, params string[] categoryNames)
        {
            return categoryNames.Intersect(product.Categories.Select(x => x.Name)).Count() == categoryNames.Length;
        }

        /// <inheritdoc />
        public virtual bool HasCategories(Product product, params Category[] categories)
        {
            return this.HasCategories(product, categories.Select(x => x.Name).ToArray());
        }

        /// <inheritdoc />
        public virtual List<ShippingBox> GetShippingBoxesForShippingCountry(CmsDbContext dbContext, Product product)
        {
            return this.GetShippingBoxesForShippingCountry(product, this.GetShippingCountryOrCurrentCountry(dbContext));
        }

        /// <inheritdoc />
        public virtual List<ShippingBox> GetShippingBoxesForShippingCountry(Product product, Country shippingCountry)
        {
            return product.ShippingBoxes
                .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled() && x.Countries.ContainsById(shippingCountry))
                .OrderBySortOrder()
                .ToList();
        }

        /// <inheritdoc />
        public virtual bool GetLocalizedIsDisabled(Product product, string languageCode = null)
        {
            return product.ProductLocalizedKits.GetByLanguageCode(languageCode)?.IsDisabled ?? false;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string GetLocalizedTitle(Product product, string languageCode = null)
        {
            return
                product.ProductLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string GetLocalizedDescription(Product product, string languageCode = null)
        {
            return
                product.ProductLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string GetLocalizedMetaTitle(Product product, string languageCode = null)
        {
            return
                product.ProductLocalizedKits.GetByLanguageCode(languageCode)?.MetaTitle ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.MetaTitle ??
                string.Empty;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string GetLocalizedMetaDescription(Product product, string languageCode = null)
        {
            return
                product.ProductLocalizedKits.GetByLanguageCode(languageCode)?.MetaDescription ??
                product.ProductLocalizedKits.GetByDefaultCountry()?.MetaDescription ??
                string.Empty;
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual ProductAttribute GetProductAttributeByName(Product product, string attributeName)
        {
            return product.ProductAttributes.FirstOrDefault(x => x.Attribute?.Name == attributeName);
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual ProductVariant GetProductVariantByName(Product product, string variantName)
        {
            return product.ProductVariants.FirstOrDefault(x => x.Variant?.Name == variantName);
        }

        /// <inheritdoc />
        [NotNull]
        public virtual List<VariantOption> GetDefaultVariantOptions(Product product)
        {
            return product.ProductVariants
                .Where(x => x.Variant?.Type == VariantTypesEnum.Options)
                .Select(x => x.DefaultVariantOptionValue ?? x.Variant.DefaultVariantOptionValue)
                .Where(x => x != null)
                .ToList();
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual ProductImageKit GetProductImageKit(Product product, List<VariantOption> variantOptions = null)
        {
            variantOptions = variantOptions ?? this.GetDefaultVariantOptions(product);

            return product.ProductImageKits.FirstOrDefault(x => x.Matches(variantOptions));
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual ProductStockUnit GetProductStockUnit(Product product, List<VariantOption> variantOptions = null)
        {
            variantOptions = variantOptions ?? this.GetDefaultVariantOptions(product);

            return product.ProductStockUnits.FirstOrDefault(x => x.Matches(variantOptions));
        }

        /// <inheritdoc />
        public virtual int GetStock(Product product, List<VariantOption> selectedVariantOptions)
        {
            return this.GetProductStockUnit(product, selectedVariantOptions)?.Stock ?? 0;
        }

        /// <inheritdoc />
        public virtual double GetTotalBeforeTaxWithoutSalePrice(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            variantOptions = variantOptions ?? this.GetDefaultVariantOptions(product);

            var productStockUnit = this.GetProductStockUnit(product, variantOptions);

            if (productStockUnit == null)
            {
                return 0;
            }

            var salePrice = this.GetApplicableSalePrice(dbContext, productStockUnit, languageCode);

            if (salePrice > 0)
            {
                var basePrice = productStockUnit.GetLocalizedBasePrice(languageCode);

                var discount = basePrice - salePrice;

                return this.GetTotalBeforeTax(dbContext, product, variantOptions, languageCode) + discount;
            }

            return this.GetTotalBeforeTax(dbContext, product, variantOptions, languageCode);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, Product product, IEnumerable<Tax> countryTaxes, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            variantOptions = variantOptions ?? this.GetDefaultVariantOptions(product);

            var totalBeforeTax = this.GetTotalBeforeTaxWithoutSalePrice(dbContext, product, variantOptions, languageCode);

            var productTaxes = countryTaxes.Where(x => x.ApplyToProductPrice).ToList();

            return totalBeforeTax.AddTaxes(productTaxes);
        }

        /// <inheritdoc />
        public virtual double GetTotalBeforeTax(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            variantOptions = variantOptions ?? this.GetDefaultVariantOptions(product);

            var productStockUnit = this.GetProductStockUnit(product, variantOptions);

            if (productStockUnit == null)
            {
                return 0;
            }

            var basePrice = productStockUnit.GetLocalizedBasePrice(languageCode);
            var salePrice = this.GetApplicableSalePrice(dbContext, productStockUnit, languageCode);
            var productPrice = salePrice > 0 ? salePrice : basePrice;
            var productPriceIncludingVariants = productPrice + variantOptions.Sum(x => x.GetLocalizedPriceModifier(languageCode));

            return productPriceIncludingVariants.ToMinimum(0);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, Product product, IEnumerable<Tax> countryTaxes, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            variantOptions = variantOptions ?? this.GetDefaultVariantOptions(product);

            var totalBeforeTax = this.GetTotalBeforeTax(dbContext, product, variantOptions, languageCode);

            var productTaxes = countryTaxes.Where(x => x.ApplyToProductPrice).ToList();

            return totalBeforeTax.AddTaxes(productTaxes);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTaxWithoutSalePrice(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            var countryTaxes = this.GetShippingCountryOrCurrentCountry(dbContext).Taxes.Where(x => x.CartItem == null);

            return this.GetTotalAfterTaxWithoutSalePrice(dbContext, product, countryTaxes, variantOptions, languageCode);
        }

        /// <inheritdoc />
        public virtual double GetTotalAfterTax(CmsDbContext dbContext, Product product, List<VariantOption> variantOptions = null, string languageCode = null)
        {
            var countryTaxes = this.GetShippingCountryOrCurrentCountry(dbContext).Taxes.Where(x => x.CartItem == null);

            return this.GetTotalAfterTax(dbContext, product, countryTaxes, variantOptions, languageCode);
        }

        private Country GetShippingCountryOrCurrentCountry(CmsDbContext dbContext)
        {
            var user =
                this.authenticationService.GetAuthenticatedUser(dbContext) ??
                this.authenticationService.GetPartiallyAuthenticatedUser(dbContext);

            return this.countryService.GetShippingCountryOrCurrentCountry(dbContext, user?.Cart);
        }

        /// <summary>
        /// If the the User property "ShowMembershipSalePrice" is set to true and the MembershipSalePrice is > 0,
        /// this method returns the MembershipSalePrice. If not, it returns the SalePrice.
        /// </summary>
        private double GetApplicableSalePrice(CmsDbContext dbContext, ProductStockUnit productStockUnit, string languageCode)
        {
            var user =
                this.authenticationService.GetAuthenticatedUser(dbContext) ??
                this.authenticationService.GetPartiallyAuthenticatedUser(dbContext);

            if (user != null && user.ShowMembershipSalePrice)
            {
                var membershipSalePrice = productStockUnit.GetLocalizedMembershipSalePrice(languageCode);

                return membershipSalePrice > 0 ? membershipSalePrice : productStockUnit.GetLocalizedSalePrice(languageCode);
            }

            return productStockUnit.GetLocalizedSalePrice(languageCode);
        }
    }
}
