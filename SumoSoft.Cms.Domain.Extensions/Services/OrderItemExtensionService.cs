﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using System;
    using System.Linq;
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public class OrderItemExtensionService : IOrderItemExtensionService
    {
        /// <inheritdoc />
        public virtual OrderItemVariant GetOrderItemVariantByName(OrderItem orderItem, string variantName)
        {
            return orderItem.OrderItemVariants.FirstOrDefault(x => x.VariantName == variantName);
        }
    }
}