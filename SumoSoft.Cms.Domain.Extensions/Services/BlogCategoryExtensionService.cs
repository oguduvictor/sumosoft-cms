﻿namespace SumoSoft.Cms.Domain.Extensions.Services
{
    using SumoSoft.Cms.Domain.Extensions.Services.Interfaces;

    /// <inheritdoc />
    public class BlogCategoryExtensionService : IBlogCategoryExtensionService
    {
        /// <inheritdoc />
        public virtual string GetLocalizedTitle(BlogCategory blogCategory, string languageCode = null)
        {
            return
                blogCategory.BlogCategoryLocalizedKits.GetByLanguageCode(languageCode)?.Title ??
                blogCategory.BlogCategoryLocalizedKits.GetByDefaultCountry()?.Title ??
                string.Empty;
        }

        /// <inheritdoc />
        public virtual string GetLocalizedDescription(BlogCategory blogCategory, string languageCode = null)
        {
            return
                blogCategory.BlogCategoryLocalizedKits.GetByLanguageCode(languageCode)?.Description ??
                blogCategory.BlogCategoryLocalizedKits.GetByDefaultCountry()?.Description ??
                string.Empty;
        }
    }
}
