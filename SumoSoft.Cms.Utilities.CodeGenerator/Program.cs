﻿namespace SumoSoft.Cms.Utilities.CodeGenerator
{
    using System.IO;
    using System.Linq;
    using SumoSoft.Cms.Extensions;

    internal class Program
    {
        private static void Main()
        {
            //CreateSumoJsTypeDefinitions();
            CreateDbSetExtension("MailingListLocalizedKit");
        }

        private static void CreateSumoJsTypeDefinitions()
        {
            const string folder = "C:\\Users\\franc\\source\\repos\\SumoSoft.Cms\\SumoSoft.Cms.Mvc\\Scripts\\sumoJS\\";

            var allLines = File.ReadAllLines(folder + "sumoJS.ts");

            var functionLines = allLines.Where(x =>
                x.StartsWith("export") ||
                x.StartsWith("/"));

            using (var streamWriter = File.CreateText(folder + "index.d.ts"))
            {
                streamWriter.WriteLine("export as namespace sumoJS;");
                streamWriter.WriteLine("export = sumoJS;");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("import { IFormResponse } from \"../interfaces/IFormResponse\";");
                streamWriter.WriteLine("import { CartItem } from \"./../classes/CartItem\";");
                streamWriter.WriteLine("import { ContentSection } from \"./../classes/ContentSection\";");
                streamWriter.WriteLine("import { ProductAttribute } from \"./../classes/ProductAttribute\";");
                streamWriter.WriteLine("import { Country } from \"./../classes/Country\";");
                streamWriter.WriteLine("import { User } from \"./../classes/User\";");
                streamWriter.WriteLine("import { Cart } from \"./../classes/Cart\";");
                streamWriter.WriteLine("import { Address } from \"../classes/Address\";");
                streamWriter.WriteLine("import { WishList } from \"../classes/WishList\";");
                streamWriter.WriteLine("import { Order } from \"../classes/Order\";");
                streamWriter.WriteLine("import { ShippingBox } from \"../classes/ShippingBox\";");
                streamWriter.WriteLine("import { LogTypesEnum } from \"../enums/LogTypesEnum\";");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("declare namespace sumoJS {");

                foreach (var functionLine in functionLines)
                {
                    if (functionLine.StartsWith("export"))
                    {
                        streamWriter.WriteLine("    " + functionLine
                                                   .Replace("export ", "")
                                                   .Replace(" = false", "?: boolean")
                                                   .ReplaceLast("{", ";"));
                    }
                    else
                    {
                        streamWriter.WriteLine("    " + functionLine);
                    }
                }

                streamWriter.WriteLine("}");
            }
        }

        private static void CreateDbSetExtension(string entityName)
        {
            var className = entityName + "DbSetExtension";

            using (var streamWriter = File.CreateText("C:\\Users\\franc\\source\\repos\\SumoSoft.Cms\\SumoSoft.Cms.DataAccess.Extensions\\" + className + ".cs"))
            {
                streamWriter.WriteLine("namespace SumoSoft.Cms.DataAccess.Extensions");
                streamWriter.WriteLine("{");
                streamWriter.WriteLine($"	public static class {className}");
                streamWriter.WriteLine("	{");
                streamWriter.WriteLine($"       public static void Delete(this DbSet<{entityName}> dbSet, Guid? entityId, bool saveChanges)");
                streamWriter.WriteLine("	    {");
                streamWriter.WriteLine("	        var dbContext = dbSet.GetContext();");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("	        if (entityId == null) return;");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("	        var dbEntity = dbSet.Find(entityId);");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("	        if (dbEntity == null) return;");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("	        // todo: check if we need to add stuff here");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("	        dbSet.Remove(dbEntity);");
                streamWriter.WriteLine("");
                streamWriter.WriteLine("	        if (saveChanges) dbContext.SaveChanges();");
                streamWriter.WriteLine("	    }");
                streamWriter.WriteLine("	}");
                streamWriter.WriteLine("}");
            }
        }
    }
}
