﻿export enum BrontoCartPhaseEnum {
    None = 0,
    Shopping = 10,
    Billing = 20,
    Shipping = 30,
    ShippingMethod = 40,
    Payment = 50,
    OrderReview = 60,
    Order = 70
}
