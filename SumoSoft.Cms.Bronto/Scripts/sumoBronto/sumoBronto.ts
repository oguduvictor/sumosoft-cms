﻿import { BrontoCartPhaseEnum } from "./enums/BrontoCartPhaseEnum";
import * as sumoJS from "../sumoJS";

export function pushCart(cartPhase: BrontoCartPhaseEnum) {
    const brontoTid = getBrontoTrackingId();

    $.post(sumoJS.getLocalizedUrl("/Bronto/PushCartAsync"), { cartPhase, brontoTid });
};

export function pushOrder(orderNumber: number) {
    const brontoTid = getBrontoTrackingId();
    
    $.post(sumoJS.getLocalizedUrl("/Bronto/PushOrderAsync"), { orderNumber, brontoTid }, () => {
        try {
            localStorage.removeItem("brontoTrackingId");
        } catch (e) {
            console.error('LocalStorage is not supported in this browser');
        }
    });
};

export function addOrUpdateSubscription(email: string, isSubscribed: boolean, countryId?: string, firstName?: string, lastName?: string) {
    $.post(sumoJS.getLocalizedUrl("/Bronto/AddOrUpdateSubscription"), { email, isSubscribed, countryId, firstName, lastName });
};

export function setBrontoTrackingId() {
    const brontoTid = sumoJS.getQueryStringParameter("_bta_tid");
    
    if (!brontoTid) {
        return;
    }

    try {
        localStorage.setItem("brontoTrackingId", brontoTid);   
    } catch (e) {
        console.error('LocalStorage is not supported in this browser');
    }
};

function getBrontoTrackingId() {
    try {
        const brontoTid = localStorage.getItem("brontoTrackingId");

        return (!brontoTid || brontoTid === "undefined" || brontoTid === "null") ? null : brontoTid;   
    } catch (e) {
        return null;
    }
}