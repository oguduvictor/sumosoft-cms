﻿import { BrontoCartPhaseEnum } from "./enums/BrontoCartPhaseEnum";

export as namespace sumoBronto;
export = sumoBronto;

declare namespace sumoBronto {
    export function pushCart(cartPhase: BrontoCartPhaseEnum);

    export function pushOrder(orderNumber: number);

    export function addOrUpdateSubscription(email: string, isSubscribed: boolean, countryId?: string, firstName?: string, lastName?: string);

    export function setBrontoTrackingId();
}
