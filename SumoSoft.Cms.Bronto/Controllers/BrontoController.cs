﻿namespace SumoSoft.Cms.Bronto.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using SumoSoft.Cms.Bronto.Enums;
    using SumoSoft.Cms.Bronto.Services;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class BrontoController : Controller
    {
        private readonly CmsBrontoService cmsBrontoService;
        private readonly CmsDbContext requestDbContext;

        /// <summary>
        ///
        /// </summary>
        /// <param name="cmsBrontoService"></param>
        /// <param name="requestDbContextService"></param>
        public BrontoController(CmsBrontoService cmsBrontoService, IRequestDbContextService requestDbContextService)
        {
            this.cmsBrontoService = cmsBrontoService;
            this.requestDbContext = requestDbContextService.Get();
        }

        /// <summary>
        /// Creates or updates a Bronto contact.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="isSubscribed"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{country:length(2)}/Bronto/AddOrUpdateSubscription", Order = 1)]
        [Route("Bronto/AddOrUpdateSubscription", Order = 2)]
        public void AddOrUpdateSubscription(string email, bool isSubscribed, Guid? countryId = null, string firstName = null, string lastName = null)
        {
            var userCountry = (countryId.HasValue ? this.requestDbContext.Countries.Find(countryId) : null) ?? this.requestDbContext.Countries.FirstOrDefault(x => x.IsDefault);

            this.cmsBrontoService.AddOrUpdateSubscription(email, userCountry, isSubscribed, firstName, lastName);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [Route("{country:length(2)}/Bronto/GenerateBrontoWebformUrl", Order = 1)]
        [Route("Bronto/GenerateBrontoWebformUrl", Order = 2)]
        public string GenerateBrontoWebformUrl(string email)
        {
            return this.cmsBrontoService.GenerateBrontoWebformUrl(email);
        }

        /// <summary>
        /// Updates Bronto cart phase.
        /// </summary>
        /// <param name="cartPhase"></param>
        /// <param name="brontoTid"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{country:length(2)}/Bronto/PushCartAsync", Order = 1)]
        [Route("Bronto/PushCartAsync", Order = 2)]
        public async Task PushCartAsync(BrontoCartPhaseEnum cartPhase, string brontoTid)
        {
            await this.cmsBrontoService.PushCartAsync(this.requestDbContext, cartPhase, brontoTid);
        }

        /// <summary>
        /// Creates a Bronto order.
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <param name="brontoTid"></param>
        [HttpPost]
        [Route("{country:length(2)}/Bronto/PushOrderAsync", Order = 1)]
        [Route("Bronto/PushOrderAsync", Order = 2)]
        public async Task PushOrderAsync(int orderNumber, string brontoTid)
        {
            await this.cmsBrontoService.PushOrderAsync(this.requestDbContext, orderNumber, brontoTid);
        }
    }
}
