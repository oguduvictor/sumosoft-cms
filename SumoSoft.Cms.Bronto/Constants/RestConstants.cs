﻿namespace SumoSoft.Cms.Bronto.Constants
{
    internal class RestConstants
    {
        public const string BrontoHost = "https://rest.bronto.com";
        public const string BrontoAuthPath = "https://auth.bronto.com/oauth2/token";
        public const string OrderPath = "orders";
        public const string CartPath = "carts";
        public const string CreateContact = "createContact";
        public const string IgnoreInvalidTid = "ignoreInvalidTid";
        public const string GrantType = "grant_type";
        public const string ClientId = "client_id";
        public const string ClientSecret = "client_secret";
        public const string ClientCredentials = "client_credentials";
        public const string AccessToken = "access_token";
        public const string Bearer = "Bearer";
        public const string BrontoCartIdCookieName = "BrontoCartId";
    }
}
