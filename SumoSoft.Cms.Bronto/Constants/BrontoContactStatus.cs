﻿namespace SumoSoft.Cms.Bronto.Constants
{
    internal class BrontoContactStatus
    {
        public const string Transactional = "transactional";
        public const string Onboarding = "onboarding";
        public const string Unconfirmed = "unconfirmed";
        public const string Unsubscribed = "unsub";
        public const string Bounce = "bounce";
        public const string Active = "active";
    }
}
