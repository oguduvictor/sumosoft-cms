﻿namespace SumoSoft.Cms.Bronto.Constants
{
    internal class BrontoFieldName
    {
        public const string FirstName = "firstname";
        public const string LastName = "lastname";
        public const string Country = "country";
        public const string CountryCode = "country_code";
        public const string CountryUrl = "country_url";
    }
}
