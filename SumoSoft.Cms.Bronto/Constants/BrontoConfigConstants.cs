﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace SumoSoft.Cms.Bronto.Constants
{
    public class BrontoConfigConstants
    {
        public const string ColorVariantName = "BrontoColorVariantName";
        public const string ImageName = "BrontoImageName";
        public const string CartUrl = "BrontoCartUrl";
        public const string ProductBaseUrl = "BrontoProductBaseUrl";
        public const string SoapApiToken = "BrontoSoapApiToken";
        public const string RestClientId = "BrontoRestClientId";
        public const string RestClientSecret = "BrontoRestClientSecret";
        public const string ListApiId = "BrontoListApiId";
        public const string SiteId = "BrontoSiteId";
        public const string ManagePreferenceWebformId = "BrontoManagePreferenceWebformId";
        public const string WebformSharedSecret = "BrontoWebformSharedSecret";
        public const string WebformUrlDomain = "BrontoWebformUrlDomain";
        public const string DomainUrl = "DomainUrl";
    }
}
