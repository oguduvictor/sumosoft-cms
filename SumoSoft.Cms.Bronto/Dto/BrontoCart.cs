﻿namespace SumoSoft.Cms.Bronto.Dto
{
    using System.Collections.Generic;

    internal class BrontoCart
    {
        public string Phase { get; set; }

        public string Currency { get; set; }

        public double DiscountAmount { get; set; }

        public double TaxAmount { get; set; }

        public double GrandTotal { get; set; }

        public double Subtotal { get; set; }

        public string EmailAddress { get; set; }

        public string Url { get; set; }

        public string CustomerCartId { get; set; }

        public string Tid { get; set; }

        public List<LineItem> LineItems { get; set; } = new List<LineItem>();
    }
}
