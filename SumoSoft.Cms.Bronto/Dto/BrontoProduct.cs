﻿namespace SumoSoft.Cms.Bronto.Dto
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class BrontoProduct
    {
        /// <summary>
        /// Unique Product Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Quantity of Product.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Url of Image.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Possible values: male | female | unisex.
        /// </summary>
        [CanBeNull]
        public string Gender { get; set; }

        /// <summary>
        /// Possible values: in stock | out of stock | preorder.
        /// </summary>
        [CanBeNull]
        public string Availability { get; set; }

        /// <summary>
        /// Product color
        /// Sample values: Green | Red | Black.
        /// </summary>
        [CanBeNull]
        public string Color { get; set; }

        /// <summary>
        /// Required, include only 1 and most relevant category
        /// Example: Clothing Accessories > Clothing > Outerwear > Coats  Jackets.
        /// </summary>
        [CanBeNull]
        public string GoogleProductCategory { get; set; }

        /// <summary>
        /// Titles for localized countries.
        /// </summary>
        public Dictionary<string, string> LocalizedKitTitles { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Descriptions for localized countries.
        /// </summary>
        public Dictionary<string, string> LocalizedKitDescriptions { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Base Price with currency for localized countries.
        /// </summary>
        public Dictionary<string, string> LocalizedKitBasePrices { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Sale Prices with currency for localized countries.
        /// </summary>
        public Dictionary<string, string> LocalizedKitSalePrices { get; set; } = new Dictionary<string, string>();

        /// <summary>
        ///
        /// </summary>
        public Dictionary<string, string> LocalizedKitProductUrls { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Label assigned to product to help shopping campaigns and bidding
        /// Sample values: Sale | Clearance | Seasonal.
        /// </summary>
        [CanBeNull]
        public string CustomLabel0 { get; set; }
    }
}
