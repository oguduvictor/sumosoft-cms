﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Bronto.Dto
{
    internal class LineItem
    {
        public string Sku { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public double UnitPrice { get; set; }

        public double SalePrice { get; set; }

        public int Quantity { get; set; }

        public double TotalPrice { get; set; }

        public string ImageUrl { get; set; }

        public string ProductUrl { get; set; }

        public string Other { get; set; }
    }
}
