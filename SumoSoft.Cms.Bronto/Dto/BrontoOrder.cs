﻿namespace SumoSoft.Cms.Bronto.Dto
{
    using System.Collections.Generic;

    internal class BrontoOrder
    {
        public string Status => "PROCESSED";

        public string Currency { get; set; }

        public double DiscountAmount { get; set; }

        public double TaxAmount { get; set; }

        public double GrandTotal { get; set; }

        public double Subtotal { get; set; }

        public string CustomerOrderId { get; set; }

        public string EmailAddress { get; set; }

        public string CartId { get; set; }

        public string Tid { get; set; }

        public string OrderDate { get; set; }

        public List<LineItem> LineItems { get; set; } = new List<LineItem>();
    }
}
