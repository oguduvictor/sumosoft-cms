﻿namespace SumoSoft.Cms.Bronto.Enums
{
    using System.ComponentModel;

    /// <summary>
    ///
    /// </summary>
    public enum BrontoCartPhaseEnum
    {
        /// <summary>
        ///
        /// </summary>
        [Description("")]
        None = 0,

        /// <summary>
        ///
        /// </summary>
        [Description("SHOPPING")]
        Shopping = 10,

        /// <summary>
        ///
        /// </summary>
        [Description("BILLING")]
        Billing = 20,

        /// <summary>
        ///
        /// </summary>
        [Description("SHIPPING_INFO")]
        Shipping = 30,

        /// <summary>
        ///
        /// </summary>
        [Description("SHIPPING_METHOD")]
        ShippingMethod = 40,

        /// <summary>
        ///
        /// </summary>
        [Description("PAYMENT")]
        Payment = 50,

        /// <summary>
        ///
        /// </summary>
        [Description("ORDER_REVIEW")]
        OrderReview = 60,

        /// <summary>
        ///
        /// </summary>
        [Description("ORDER_COMPLETE")]
        Order = 70
    }
}
