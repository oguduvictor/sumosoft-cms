﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Bronto.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.Bronto.BrontoWebReference;
    using SumoSoft.Cms.Bronto.Constants;
    using SumoSoft.Cms.Bronto.Dto;
    using SumoSoft.Cms.Bronto.Enums;
    using SumoSoft.Cms.Bronto.Extensions;
    using SumoSoft.Cms.Bronto.Validators;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class CmsBrontoService
    {
        private readonly IUtilityService utilityService;
        private readonly ICountryService countryService;
        private readonly IAuthenticationService authenticationService;
        private string accessToken;

        private static readonly HttpClient httpClient = new HttpClient();

        public CmsBrontoService(
            IUtilityService utilityService,
            ICountryService countryService,
            IAuthenticationService authenticationService)
        {
            this.utilityService = utilityService;
            this.countryService = countryService;
            this.authenticationService = authenticationService;
        }

        public void AddOrUpdateSubscription(string email, Country userCountry, bool isSubscribed, string firstName = null, string lastName = null)
        {
            var validationResult = new EmailValidator().Validate(email);

            if (!validationResult.IsValid)
            {
                throw new Exception("Email is not valid");
            }

            var contactListId = this.utilityService.GetAppSetting(BrontoConfigConstants.ListApiId);

            var brontoSoapClient = this.GetBrontoSoapClient();

            var brontoContact = new contactObject
            {
                email = email,
                fields = GetContactFields(brontoSoapClient, userCountry, firstName, lastName),
                status = isSubscribed ? BrontoContactStatus.Onboarding : BrontoContactStatus.Unsubscribed,
                listIds = new[] { contactListId }
            };

            brontoContact.listIds.ToList().Add(contactListId);

            if (isSubscribed)
            {
                brontoSoapClient.addOrUpdateContacts(new[] { brontoContact });
            }
            else
            {
                brontoSoapClient.updateContacts(new[] { brontoContact });
            }
        }

        public void UpdateContactsCountryUrlAndCode(CmsDbContext dbContext)
        {
            var brontoSoapClient = this.GetBrontoSoapClient();

            var pageNumber = 1;
            var fields = brontoSoapClient.readFields(new fieldsFilter(), 1, 100, true);

            var firstNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.FirstName);
            var lastNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.LastName);
            var countryNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.Country);
            var countryCodeFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryCode);
            var countryUrlFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryUrl);

            var fieldIds = new[] { countryUrlFieldObject?.id, countryCodeFieldObject?.id, firstNameFieldObject?.id, lastNameFieldObject?.id, countryNameFieldObject?.id };

            var contactFilter = new contactFilter
            {
                listId = new[] { this.utilityService.GetAppSetting(BrontoConfigConstants.ListApiId) }
            };

            var brontoContacts = brontoSoapClient.readContacts(contactFilter, true, true, fieldIds, pageNumber, false, false, false, false, false, false, false, false, true, true, true, true);

            while (brontoContacts?.Any() ?? false)
            {
                foreach (var contactObject in brontoContacts)
                {
                    var countryNameField = contactObject.fields.FirstOrDefault(x => x.fieldId == countryNameFieldObject?.id).AsNotNull();
                    var countryCodeField = contactObject.fields.FirstOrDefault(x => x.fieldId == countryCodeFieldObject?.id).AsNotNull();
                    var countryUrlField = contactObject.fields.FirstOrDefault(x => x.fieldId == countryUrlFieldObject?.id).AsNotNull();

                    var dbCountry = dbContext.Countries.FirstOrDefault(x => x.Name == countryNameField.content) ?? dbContext.Countries.First(x => x.IsDefault);

                    countryUrlField.content = $"/{dbCountry.Url ?? string.Empty}";
                    countryCodeField.content = dbCountry.LanguageCode?.Split('-').Last().ToLower();
                    countryNameField.content = dbCountry.Name;

                    brontoSoapClient.updateContacts(new[] { contactObject });
                }

                pageNumber++;

                brontoContacts = brontoSoapClient.readContacts(contactFilter, true, true, fieldIds, pageNumber, true, true, true, true, true, true, true, true, true, true, true, true);
            }
        }

        public void UpdateAllUnsubscribedUsersStatus(CmsDbContext dbContext)
        {
            var brontoSoapClient = this.GetBrontoSoapClient();

            var pageNumber = 1;
            var fields = brontoSoapClient.readFields(new fieldsFilter(), 1, 100, true);

            var firstNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.FirstName);
            var lastNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.LastName);
            var countryNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.Country);
            var countryCodeFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryCode);
            var countryUrlFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryUrl);

            var fieldIds = new[] { countryUrlFieldObject?.id, countryCodeFieldObject?.id, firstNameFieldObject?.id, lastNameFieldObject?.id, countryNameFieldObject?.id };

            var contactFilter = new contactFilter
            {
                listId = new[] { this.utilityService.GetAppSetting(BrontoConfigConstants.ListApiId) },
                status = new[] { BrontoContactStatus.Unsubscribed }
            };

            var brontoContacts = brontoSoapClient.readContacts(contactFilter, true, true, fieldIds, pageNumber, false, false, false, false, false, false, false, false, true, true, true, true);

            var contactsModified = false;

            while (brontoContacts?.Any() ?? false)
            {
                var unsubscribedEmails = brontoContacts.Select(x => x.email);

                //var subscribedDbUsers = dbContext.Users.Where(x => x.Newsletter && unsubscribedEmails.Contains(x.Email)).ToList();

                //subscribedDbUsers.ForEach(user => { user.Newsletter = false; });

                contactsModified = true;

                pageNumber++;

                brontoContacts = brontoSoapClient.readContacts(contactFilter, true, true, fieldIds, pageNumber, true, true, true, true, true, true, true, true, true, true, true, true);
            }

            if (contactsModified)
            {
                dbContext.SaveChanges();
            }
        }

        public void UpdateRecentlyUnsubscribedUserStatus(CmsDbContext dbContext)
        {
            var brontoSoapClient = this.GetBrontoSoapClient();

            var pageNumber = 1;

            var fields = brontoSoapClient.readFields(new fieldsFilter(), 1, 100, true);

            var firstNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.FirstName);
            var lastNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.LastName);
            var countryNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.Country);
            var countryCodeFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryCode);
            var countryUrlFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryUrl);

            var fieldIds = new[] { countryUrlFieldObject?.id, countryCodeFieldObject?.id, firstNameFieldObject?.id, lastNameFieldObject?.id, countryNameFieldObject?.id };

            var contactFilter = new contactFilter
            {
                listId = new[] { this.utilityService.GetAppSetting(BrontoConfigConstants.ListApiId) },
                status = new[] { BrontoContactStatus.Unsubscribed },
                modified = new[] {
                    new dateValue
                    {
                        value = DateTime.Today.AddDays(-1),
                        @operator = filterOperator.GreaterThanEqualTo,
                        operatorSpecified = true,
                        valueSpecified = true
                    }
                }
            };

            var brontoContacts = brontoSoapClient.readContacts(contactFilter, true, true, fieldIds, pageNumber, false, false, false, false, false, false, false, false, true, true, true, true);

            var contactsModified = false;

            while (brontoContacts?.Any() ?? false)
            {
                var unsubscribedEmails = brontoContacts.Select(x => x.email).ToList();

                //var subscribedDbUsers = dbContext.Users.Where(x => x.Newsletter && unsubscribedEmails.Contains(x.Email)).ToList();

                //subscribedDbUsers.ForEach(user => { user.Newsletter = false; });

                contactsModified = true;

                pageNumber++;

                brontoContacts = brontoSoapClient.readContacts(contactFilter, true, true, fieldIds, pageNumber, true, true, true, true, true, true, true, true, true, true, true, true);
            }

            if (contactsModified)
            {
                dbContext.SaveChanges();
            }
        }

        public async Task PushCartAsync(CmsDbContext dbContext, BrontoCartPhaseEnum cartPhase, string brontoTid)
        {
            var cart = this.authenticationService.GetAuthenticatedUser(dbContext, true)?.Cart;

            var brontoCart = this.CreateBrontoCartObject(dbContext, cart, cartPhase, brontoTid);

            var brontoCartId = this.utilityService.GetCookie(RestConstants.BrontoCartIdCookieName)?.Value;

            var brontoCartPostUrl = $"{RestConstants.BrontoHost}/{RestConstants.CartPath}{(string.IsNullOrEmpty(brontoCartId) ? "" : $"/{brontoCartId}")}?{RestConstants.CreateContact}=true&{RestConstants.IgnoreInvalidTid}=true";

            var validationResult = new BrontoCartValidator().Validate(brontoCart);

            if (!validationResult.IsValid)
            {
                return;
            }

            var response = await this.PostAsync(brontoCart, brontoCartPostUrl);

            if (string.IsNullOrEmpty(response))
            {
                return;
            }

            var savedBrontoCartId = response.FromJson<Dictionary<string, object>>()["cartId"].ToString();

            try
            {
                this.utilityService.SetCookie(RestConstants.BrontoCartIdCookieName, savedBrontoCartId, 5);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public async Task PushOrderAsync(CmsDbContext dbContext, int orderNumber, string brontoTid)
        {
            var brontoCartId = this.utilityService.GetCookie(RestConstants.BrontoCartIdCookieName)?.Value;

            var brontoColorVariantName = this.utilityService.GetAppSetting(BrontoConfigConstants.ColorVariantName);

            var brontoProductBaseUrl = this.utilityService.GetAppSetting(BrontoConfigConstants.ProductBaseUrl);

            var domainUrl = this.utilityService.GetAppSetting(BrontoConfigConstants.DomainUrl);

            if (!string.IsNullOrEmpty(brontoCartId))
            {
                var postData = new Dictionary<string, string> { { "phase", BrontoCartPhaseEnum.Order.GetDescription() } };

                var brontoUpdateCartUrl = $"{RestConstants.BrontoHost}/{RestConstants.CartPath}/{brontoCartId}?{RestConstants.CreateContact}=true&{RestConstants.IgnoreInvalidTid}=true";

                await this.PostAsync(postData.ToJson(), brontoUpdateCartUrl);
            }

            var order = dbContext.Orders.FirstOrDefault(x => x.OrderNumber == orderNumber).AsNotNull();

            var orderItems = order.OrderShippingBoxes.SelectMany(x => x.OrderItems).ToList();

            var subtotalAfterTax = orderItems.Sum(x => x.SubtotalAfterTax);

            var subtotalBeforeTax = orderItems.Sum(x => x.SubtotalBeforeTax);

            var currentCountry = this.countryService.GetCurrentCountry(dbContext);

            var brontoOrder = new BrontoOrder
            {
                CartId = brontoCartId,
                Currency = order.ISO4217CurrencySymbol,
                CustomerOrderId = orderNumber.ToString(),
                DiscountAmount = order.OrderCoupon?.Amount ?? 0,
                EmailAddress = order.UserEmail,
                GrandTotal = order.TotalAfterTax,
                Subtotal = subtotalAfterTax,
                OrderDate = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                TaxAmount = subtotalAfterTax - subtotalBeforeTax,
                Tid = string.IsNullOrEmpty(brontoTid) ? null : brontoTid,
                LineItems = orderItems.Select(orderItem => new LineItem
                {
                    Sku = orderItem.ProductStockUnitId.ToString(),
                    Name = orderItem.ProductTitle,
                    Description = dbContext.Products.FirstOrDefault(x => x.Id == orderItem.ProductId)?.GetLocalizedDescription(order.CountryLanguageCode),
                    Category = string.Empty,
                    UnitPrice = orderItem.ProductStockUnitBasePrice,
                    SalePrice = orderItem.ProductStockUnitSalePrice,
                    Quantity = orderItem.ProductQuantity,
                    TotalPrice = orderItem.TotalAfterTax,
                    ImageUrl = orderItem.Image,
                    ProductUrl = $"{domainUrl}{(string.IsNullOrEmpty(currentCountry.Url) ? string.Empty : $"/{currentCountry.Url}")}/{brontoProductBaseUrl}/{orderItem.ProductUrl}?color={orderItem.GetOrderItemVariantByName(brontoColorVariantName)?.VariantOptionUrl}".RegexReplace("\\b\\/+\\b", "/"),
                    Other = orderItem.GetOrderItemVariantByName(brontoColorVariantName)?.VariantOptionUrl
                }).ToList()
            };

            var brontoOrderUrl = $"{RestConstants.BrontoHost}/{RestConstants.OrderPath}?{RestConstants.CreateContact}=true&{RestConstants.IgnoreInvalidTid}=true";

            await this.PostAsync(brontoOrder, brontoOrderUrl);

            try
            {
                this.utilityService.SetCookie(RestConstants.BrontoCartIdCookieName, brontoCartId, 0);
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="productStockUnit"></param>
        /// <param name="additionalVariantOptions"></param>
        /// <returns></returns>
        public BrontoProduct MapBrontoProduct(CmsDbContext dbContext, ProductStockUnit productStockUnit, IEnumerable<VariantOption> additionalVariantOptions = null)
        {
            var brontoColorVariantName = this.utilityService.GetAppSetting(BrontoConfigConstants.ColorVariantName);

            var domainUrl = this.utilityService.GetAppSetting(BrontoConfigConstants.DomainUrl);

            var brontoImageName = this.utilityService.GetAppSetting(BrontoConfigConstants.ImageName);

            var brontoProductBaseUrl = this.utilityService.GetAppSetting(BrontoConfigConstants.ProductBaseUrl);

            var variantOptions = new List<VariantOption>();

            variantOptions.AddRange(productStockUnit.VariantOptions);

            variantOptions.AddRange(additionalVariantOptions.AsNotNull());

            variantOptions = variantOptions.DistinctBy(x => x.Id);

            var product = productStockUnit.Product.AsNotNull();

            var colorVariantOption = variantOptions.FirstOrDefault(x => x.Variant?.Name == brontoColorVariantName);

            var brontoProduct = new BrontoProduct
            {
                Id = productStockUnit.Id.ToString(),
                ImageUrl = product.GetProductImageKit()?.GetProductImageByName(brontoImageName)?.Url,
                Quantity = productStockUnit.Stock,
                Availability = productStockUnit.EnablePreorder ? "preorder" : productStockUnit.Stock > 0 ? "in stock" : "out of stock",
                Color = string.IsNullOrEmpty(colorVariantOption?.GetLocalizedDescription()) ? colorVariantOption?.GetLocalizedTitle() : colorVariantOption.GetLocalizedDescription()
            };

            var localizedCountries = dbContext.Countries.Where(x => x.Localize && !x.IsDisabled)
                                                        .OrderByDescending(x => x.IsDefault)
                                                        .ThenByDescending(x => x.Url == "us")
                                                        .ThenBy(x => x.LanguageCode)
                                                        .ToList();

            localizedCountries.ForEach(country =>
            {
                var currencySymbol = country.GetIso4217CurrencySymbol();
                var stockLocalizedLanguageCode = country.LanguageCode;
                var stockLocalizedCountryUrl = string.IsNullOrEmpty(country.Url) ? string.Empty : $"/{country.Url}";
                var stockLocalizedProductUrl = $"{domainUrl}{stockLocalizedCountryUrl}{brontoProductBaseUrl}/{product.Url}".RegexReplace("\\b\\/+\\b", "/");

                brontoProduct.LocalizedKitTitles.Add($"title_{stockLocalizedLanguageCode}", product.GetLocalizedTitle(stockLocalizedLanguageCode));

                brontoProduct.LocalizedKitDescriptions.Add($"description_{stockLocalizedLanguageCode}", product.GetLocalizedDescription(stockLocalizedLanguageCode));

                brontoProduct.LocalizedKitBasePrices.Add($"base_price_{stockLocalizedLanguageCode}", GetFormattedPrice(currencySymbol, product.GetTotalAfterTaxWithoutSalePrice(dbContext, country.Taxes, variantOptions, stockLocalizedLanguageCode)));

                brontoProduct.LocalizedKitSalePrices.Add($"sale_price_{stockLocalizedLanguageCode}", GetFormattedPrice(currencySymbol, product.GetTotalAfterTax(dbContext, country.Taxes, variantOptions, stockLocalizedLanguageCode)));

                brontoProduct.LocalizedKitProductUrls.Add($"product_url_{stockLocalizedLanguageCode}", stockLocalizedProductUrl);
            });

            return brontoProduct;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="brontoProducts"></param>
        /// <returns></returns>
        public MemoryStream CreateFeed(List<BrontoProduct> brontoProducts)
        {
            var rows = new List<string>();

            var csvHeadersList = new List<string>
            {
                "product_id",
                "quantity",
                "google product category",
                "image_url",
                "availability",
                "gender",
                "color",
                "custom label 0"
            };

            brontoProducts = brontoProducts.Where(x => !string.IsNullOrEmpty(x.GoogleProductCategory)).ToList().DistinctBy(x => x.Id);

            brontoProducts.ForEach(brontoProduct =>
            {
                var rowData = new List<string>
                {
                    brontoProduct.Id.ToValidCsvString(),
                    brontoProduct.Quantity.ToString().ToValidCsvString(),
                    brontoProduct.GoogleProductCategory.ToValidCsvString(),
                    brontoProduct.ImageUrl.ToValidCsvString(),
                    brontoProduct.Availability.ToValidCsvString(),
                    brontoProduct.Gender.ToValidCsvString(),
                    brontoProduct.Color.ToValidCsvString(),
                    brontoProduct.CustomLabel0.ToValidCsvString()
                };

                for (var i = 0; i < brontoProduct.LocalizedKitBasePrices.Count; i++)
                {
                    var localizedKitTitle = brontoProduct.LocalizedKitTitles.ElementAt(i);
                    var localizedDescription = brontoProduct.LocalizedKitDescriptions.ElementAt(i);
                    var localizedKitBasePrice = brontoProduct.LocalizedKitBasePrices.ElementAt(i);
                    var localizedKitSalePrice = brontoProduct.LocalizedKitSalePrices.ElementAt(i);
                    var localizedKitProductUrl = brontoProduct.LocalizedKitProductUrls.ElementAt(i);

                    csvHeadersList.Add(localizedKitTitle.Key.ToValidCsvString());
                    csvHeadersList.Add(localizedDescription.Key.ToValidCsvString());
                    csvHeadersList.Add(localizedKitBasePrice.Key.ToValidCsvString());
                    csvHeadersList.Add(localizedKitSalePrice.Key.ToValidCsvString());
                    csvHeadersList.Add(localizedKitProductUrl.Key.ToValidCsvString());

                    rowData.Add(localizedKitTitle.Value.ToValidCsvString());
                    rowData.Add(localizedDescription.Value.ToValidCsvString());
                    rowData.Add(localizedKitBasePrice.Value.ToValidCsvString());
                    rowData.Add(localizedKitSalePrice.Value.ToValidCsvString());
                    rowData.Add(localizedKitProductUrl.Value.ToValidCsvString());
                }

                rows.Add(string.Join("\t", rowData));
            });

            rows.Insert(0, string.Join("\t", csvHeadersList.Distinct()) + Environment.NewLine);

            var csvString = string.Join(Environment.NewLine, rows);

            var csvBytes = new UTF8Encoding().GetBytes(csvString);

            return new MemoryStream(csvBytes);
        }

        public string GenerateBrontoWebformUrl(string contactEmail)
        {
            var siteId = this.utilityService.GetAppSetting(BrontoConfigConstants.SiteId);

            var webformId = this.utilityService.GetAppSetting(BrontoConfigConstants.ManagePreferenceWebformId);

            var webformSharedSecret = this.utilityService.GetAppSetting(BrontoConfigConstants.WebformSharedSecret);

            var webformUrlDomain = this.utilityService.GetAppSetting(BrontoConfigConstants.WebformUrlDomain);

            var keyBytes = Encoding.ASCII.GetBytes(webformSharedSecret);

            var messageBytes = Encoding.ASCII.GetBytes($"{siteId}{webformId}{contactEmail}");

            var bytes = new HMACSHA256(keyBytes).ComputeHash(messageBytes);

            var validationHash = BitConverter.ToString(bytes).Replace("-", "").ToLower();

            return $"{webformUrlDomain}/public/webform/lookup/{webformId}/{siteId}/manpref/{contactEmail}/{validationHash}";
        }

        private BrontoSoapApiImplService GetBrontoSoapClient()
        {
            var brontoSoapClient = new BrontoSoapApiImplService();

            var sessionId = brontoSoapClient.login(this.utilityService.GetAppSetting(BrontoConfigConstants.SoapApiToken));

            var sessionHeader = new sessionHeader
            {
                sessionId = sessionId
            };

            brontoSoapClient.sessionHeaderValue = sessionHeader;

            return brontoSoapClient;
        }

        private async Task<string> GetBrontoRestAccessTokenAsync()
        {
            var clientId = this.utilityService.GetAppSetting(BrontoConfigConstants.RestClientId);

            var clientSecret = this.utilityService.GetAppSetting(BrontoConfigConstants.RestClientSecret);

            var parameters = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>(RestConstants.GrantType, RestConstants.ClientCredentials),
                new KeyValuePair<string, string>(RestConstants.ClientId, clientId),
                new KeyValuePair<string, string>(RestConstants.ClientSecret, clientSecret)
            });

            var response = await httpClient.PostAsync(RestConstants.BrontoAuthPath, parameters);

            response.EnsureSuccessStatusCode();

            var jsonStringResult = await response.Content.ReadAsStringAsync();

            var accessToken = jsonStringResult.FromJson<Dictionary<string, string>>()[RestConstants.AccessToken];

            return accessToken;
        }

        private BrontoCart CreateBrontoCartObject(CmsDbContext dbContext, Cart cart, BrontoCartPhaseEnum cartPhase, string brontoTid)
        {
            if (cart == null)
            {
                return new BrontoCart();
            }

            var brontoColorVariantName = this.utilityService.GetAppSetting(BrontoConfigConstants.ColorVariantName);

            var domainUrl = this.utilityService.GetAppSetting(BrontoConfigConstants.DomainUrl);

            var brontoImageName = this.utilityService.GetAppSetting(BrontoConfigConstants.ImageName);

            var brontoProductBaseUrl = this.utilityService.GetAppSetting(BrontoConfigConstants.ProductBaseUrl);

            var cartUrl = this.countryService.GetLocalizedUrl(this.utilityService.GetAppSetting(BrontoConfigConstants.CartUrl));

            var currentCountry = this.countryService.GetCurrentCountry(dbContext);

            return new BrontoCart
            {
                Phase = cartPhase.GetDescription(),
                Currency = currentCountry.GetIso4217CurrencySymbol(),
                DiscountAmount = cart.GetCouponValue(dbContext),
                TaxAmount = cart.GetTotalAfterTax(dbContext) - cart.GetTotalBeforeTax(dbContext),
                GrandTotal = cart.GetTotalToBePaid(dbContext),
                Subtotal = cart.GetTotalAfterTax(dbContext),
                EmailAddress = cart.User?.Email,
                Url = $"{domainUrl}/{cartUrl}".RegexReplace("\\b\\/+\\b", "/"),
                CustomerCartId = Guid.NewGuid().ToString(),
                Tid = string.IsNullOrEmpty(brontoTid) ? null : brontoTid,
                LineItems = cart.CartShippingBoxes.SelectMany(x => x.CartItems).Select(cartItem => new LineItem
                {
                    Sku = cartItem.GetProductStockUnit(cartItem.GetSelectedVariantOptions())?.Id.ToString(),
                    Name = cartItem.Product.GetLocalizedTitle(),
                    Description = cartItem.Product.GetLocalizedDescription(),
                    Category = string.Empty,
                    UnitPrice = cartItem.GetProductStockUnit()?.GetLocalizedBasePrice() ?? 0,
                    SalePrice = cartItem.GetProductStockUnit()?.GetLocalizedSalePrice() ?? 0,
                    Quantity = cartItem.Quantity,
                    TotalPrice = cartItem.GetTotalAfterTax(dbContext),
                    ImageUrl = cartItem.GetProductImageKit()?.GetProductImageByName(brontoImageName)?.Url,
                    ProductUrl = $"{domainUrl}{(string.IsNullOrEmpty(currentCountry.Url) ? string.Empty : $"/{currentCountry.Url}")}/{brontoProductBaseUrl}/{cartItem.Product?.Url}?color={cartItem.GetCartItemVariantByName(brontoColorVariantName)?.VariantOptionValue?.Url}".RegexReplace("\\b\\/+\\b", "/"),
                    Other = cartItem.GetCartItemVariantByName(brontoColorVariantName)?.VariantOptionValue?.GetLocalizedTitle(currentCountry.LanguageCode)
                }).ToList()
            };
        }

        private async Task<string> PostAsync<T>(T data, string url)
        {
            this.accessToken = string.IsNullOrEmpty(this.accessToken) ? await this.GetBrontoRestAccessTokenAsync() : this.accessToken;

            var content = new StringContent(data.ToCamelCaseJson(), Encoding.UTF8, "application/json");

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(RestConstants.Bearer, this.accessToken);

            var response = await httpClient.PostAsync(url, content);

            return response.IsSuccessStatusCode ? await response.Content.ReadAsStringAsync() : string.Empty;
        }

        private static contactField[] GetContactFields(BrontoSoapApiImplService brontoSoapClient, Country userCountry, string firstName, string lastName)
        {
            var fieldFilter = new fieldsFilter();

            var fields = brontoSoapClient.readFields(fieldFilter, 1, 100, true);

            var firstNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.FirstName);
            var lastNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.LastName);
            var countryNameFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.Country);
            var countryCodeFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryCode);
            var countryUrlFieldObject = fields.FirstOrDefault(x => x.name.ToLower() == BrontoFieldName.CountryUrl);

            return new[]
            {
                new contactField {content = firstName, fieldId = firstNameFieldObject?.id},
                new contactField {content = lastName, fieldId = lastNameFieldObject?.id},
                new contactField {content = userCountry?.Name, fieldId = countryNameFieldObject?.id},
                new contactField {content = userCountry?.LanguageCode?.Split('-').Last().ToLower(), fieldId = countryCodeFieldObject?.id},
                new contactField {content = $"/{userCountry?.Url ?? string.Empty}", fieldId = countryUrlFieldObject?.id}
            };
        }

        /// <summary>
        /// Format: 100.00 USD.
        /// </summary>
        private static string GetFormattedPrice(string currencySymbol, double price)
        {
            return $"{price.Round(0):0.00} {currencySymbol}";
        }
    }
}
