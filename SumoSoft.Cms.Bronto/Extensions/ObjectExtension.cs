﻿namespace SumoSoft.Cms.Bronto.Extensions
{
    using Newtonsoft.Json;

    internal static class ObjectExtension
    {
        /// <summary>
        /// Converts a json string to an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static T FromJson<T>(this string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString, new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
            });
        }
    }
}
