﻿namespace SumoSoft.Cms.Bronto.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Bronto.Dto;
    using SumoSoft.Cms.Bronto.Enums;
    using SumoSoft.Cms.Extensions;

    internal class BrontoCartValidator : AbstractValidator<BrontoCart>
    {
        public BrontoCartValidator()
        {
            this.RuleFor(x => x.CustomerCartId).NotEmpty();
            this.RuleFor(x => x.Phase).NotEmpty();
            this.RuleFor(x => x.Phase)
                .Must(x => x.EqualsToAny(
                    BrontoCartPhaseEnum.Shopping.GetDescription(),
                    BrontoCartPhaseEnum.Order.GetDescription(),
                    BrontoCartPhaseEnum.Billing.GetDescription(),
                    BrontoCartPhaseEnum.Payment.GetDescription(),
                    BrontoCartPhaseEnum.OrderReview.GetDescription(),
                    BrontoCartPhaseEnum.Shipping.GetDescription(),
                    BrontoCartPhaseEnum.ShippingMethod.GetDescription()));

            this.RuleFor(x => x.LineItems).NotEmpty();
        }
    }
}
