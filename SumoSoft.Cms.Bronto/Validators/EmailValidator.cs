﻿namespace SumoSoft.Cms.Bronto.Validators
{
    using FluentValidation;

    internal class EmailValidator : AbstractValidator<string>
    {
        public EmailValidator()
        {
            this.RuleFor(email => email).NotEmpty();
            this.RuleFor(email => email).EmailAddress();
        }
    }
}
