﻿using FluentValidation;
using SumoSoft.Cms.Bronto.Dto;

namespace SumoSoft.Cms.Bronto.Validators
{
    internal class BrontoUserDtoValidator : AbstractValidator<BrontoUserDto>
    {
        public BrontoUserDtoValidator()
        {
            RuleFor(x => x.Email).EmailAddress();
        }
    }
}
