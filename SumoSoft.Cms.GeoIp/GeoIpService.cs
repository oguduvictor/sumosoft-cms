﻿//using System;
//using System.Net;
//using System.Reflection;
//using System.Text;

//namespace SumoSoft.Cms.GeoIp
//{
//    public class GeoIpService
//    {
//        /// <summary>
//        /// Gets the ISO 3166-1 country code from the user IP
//        /// </summary>
//        /// <param name="ipAddress"></param>
//        /// <returns></returns>
//        public string GetCountryFromIp(string ipAddress)
//        {
//            const string csvPath = "SumoSoft.Cms.GeoIp.Data.Output.csv";

//            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(csvPath))
//            {
//                if (stream == null)
//                {
//                    throw new NullReferenceException("No IP database was found at " + csvPath);
//                }

//                var ip = IPAddress.Parse(ipAddress);

//                var rows = CsvHelper.GetRows(() => stream, Encoding.UTF8);

//                foreach (var row in rows)
//                {
//                    var columns = row.Split(';');

//                    var addressRange = new IpAddressRange(IPAddress.Parse(columns[0]), IPAddress.Parse(columns[1]));

//                    if (addressRange.IsInRange(ip))
//                    {
//                        return columns[2];
//                    }
//                }
//            }

//            return string.Empty;
//        }
//    }
//}
