@echo off

SET "CDIR=%~dp0"

:: for loop requires removing trailing backslash from %~dp0 output
SET "CDIR=%CDIR:~0,-1%"

FOR %%i IN ("%CDIR%") DO SET "package=%%~nxi"

SET /P version=Enter Version of %package% to delete: 

.\NuGet.exe delete %package% %version% -Source http://sumosoft-nuget.azurewebsites.net/nuget -ApiKey Sumosoft007

PAUSE