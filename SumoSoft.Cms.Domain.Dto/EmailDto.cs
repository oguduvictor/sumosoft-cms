﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class EmailDto : BaseEntityDto
    {
        public EmailDto()
        {
        }

        public EmailDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string ViewName { get; set; }

        [CanBeNull]
        public ContentSectionDto ContentSection { get; set; }

        [NotNull]
        public List<EmailLocalizedKitDto> EmailLocalizedKits { get; set; } = new List<EmailLocalizedKitDto>();

        [CanBeNull]
        public string LocalizedFrom { get; set; }

        [CanBeNull]
        public string LocalizedDisplayName { get; set; }

        [CanBeNull]
        public string LocalizedReplyTo { get; set; }
    }
}