﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class SeoSectionDto : BaseEntityDto
    {
        public SeoSectionDto()
        {
        }

        public SeoSectionDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Page { get; set; }

        [NotNull]
        public virtual List<SeoSectionLocalizedKitDto> SeoSectionLocalizedKits { get; set; } = new List<SeoSectionLocalizedKitDto>();

        [CanBeNull]
        public string LocalizedTitle { get; set; }

        [CanBeNull]
        public string LocalizedDescription { get; set; }

        [CanBeNull]
        public string LocalizedImage { get; set; }
    }
}