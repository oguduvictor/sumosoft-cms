﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using JetBrains.Annotations;

    public class OrderItemTaxDto : BaseEntityDto
    {
        [CanBeNull]
        public OrderItemDto OrderItem { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        public double Amount { get; set; }
    }
}