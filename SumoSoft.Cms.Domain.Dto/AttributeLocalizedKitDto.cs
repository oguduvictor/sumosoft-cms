﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class AttributeLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public AttributeLocalizedKitDto()
        {
        }

        public AttributeLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public AttributeDto Attribute { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }
    }
}