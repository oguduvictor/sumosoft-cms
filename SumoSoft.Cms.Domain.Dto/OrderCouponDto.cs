﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class OrderCouponDto : BaseEntityDto
    {
        public OrderCouponDto()
        {
        }

        public OrderCouponDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Code { get; set; }

        public double Amount { get; set; }
    }
}