﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class CartShippingBoxDto : BaseEntityDto
    {
        public CartShippingBoxDto()
        {
        }

        public CartShippingBoxDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public ShippingBoxDto ShippingBox { get; set; }

        [CanBeNull]
        public CartDto Cart { get; set; }

        [NotNull]
        public List<CartItemDto> CartItems { get; set; } = new List<CartItemDto>();

        public double ShippingPriceBeforeTax { get; set; }

        public double ShippingPriceAfterTax { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }
    }
}