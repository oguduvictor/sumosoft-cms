﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class VariantDto : BaseEntityDto, ISortableDto
    {
        public VariantDto()
        {
        }

        public VariantDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        public int SortOrder { get; set; }

        [NotNull]
        public List<VariantOptionDto> VariantOptions { get; set; } = new List<VariantOptionDto>();

        public VariantTypesEnum Type { get; set; }

        [NotNull]
        public List<string> TypeDescriptions { get; set; } = new List<string>();

        [NotNull]
        public List<VariantLocalizedKitDto> VariantLocalizedKits { get; set; } = new List<VariantLocalizedKitDto>();

        public bool DefaultBooleanValue { get; set; }

        public double DefaultDoubleValue { get; set; }

        public int DefaultIntegerValue { get; set; }

        [CanBeNull]
        public string DefaultStringValue { get; set; }

        [CanBeNull]
        public string DefaultJsonValue { get; set; }

        [CanBeNull]
        public VariantOptionDto DefaultVariantOptionValue { get; set; }

        public bool CreateProductStockUnits { get; set; }

        public bool CreateProductImageKits { get; set; }

        [CanBeNull]
        public string LocalizedTitle { get; set; }

        [CanBeNull]
        public string LocalizedDescription { get; set; }
    }
}