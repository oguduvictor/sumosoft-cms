﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class EmailLocalizedKitDto : BaseEntityDto
    {
        public EmailLocalizedKitDto()
        {
        }

        public EmailLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public EmailDto Email { get; set; }

        /// <summary>
        /// Gets or sets the From.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the ReplyTo.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string ReplyTo { get; set; }

        /// <summary>
        /// Gets or sets the DisplayName.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string DisplayName { get; set; }
    }
}