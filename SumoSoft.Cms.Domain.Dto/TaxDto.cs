﻿#pragma warning disable 1591
namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class TaxDto : BaseEntityDto
    {
        public TaxDto()
        {
        }

        public TaxDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public CartItemDto CartItem { get; set; }

        [CanBeNull]
        public AddressDto ShippingAddress { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        public double Percentage { get; set; }

        public double? Amount { get; set; }

        public bool ApplyToShippingPrice { get; set; } = true;

        public bool ApplyToProductPrice { get; set; } = true;
    }
}