﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using JetBrains.Annotations;

    public class SeoSectionLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        [CanBeNull]
        public virtual CountryDto Country { get; set; }

        [CanBeNull]
        public virtual SeoSectionDto SeoSection { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Image.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Image { get; set; }
    }
}