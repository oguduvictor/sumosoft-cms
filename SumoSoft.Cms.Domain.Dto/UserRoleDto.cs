﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using JetBrains.Annotations;

    public class UserRoleDto : BaseEntityDto
    {
        public UserRoleDto()
        {
        }

        public UserRoleDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        // -------------------------------------------------------------
        // Access to Administration panel sections
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the administration panel")]
        public bool AccessAdmin { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access any of the Ecommerce sections")]
        public bool AccessAdminEcommerce { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Orders section")]
        public bool AccessAdminOrders { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Blog section")]
        public bool AccessAdminBlog { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Media section")]
        public bool AccessAdminMedia { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Content section")]
        public bool AccessAdminContent { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Emails section")]
        public bool AccessAdminEmails { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Seo section")]
        public bool AccessAdminSeo { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Users section")]
        public bool AccessAdminUsers { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Countries section")]
        public bool AccessAdminCountries { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAccessAreas, Description = "Access the Settings section")]
        public bool AccessAdminSettings { get; set; }

        // -------------------------------------------------------------
        // Orders section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupOrders, Description = "Edit an existing Order")]
        public bool EditOrder { get; set; }

        // -------------------------------------------------------------
        // Product section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProducts, Description = "Create a new Product")]
        public bool CreateProduct { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProducts, Description = "Edit an existing Product")]
        public bool EditProduct { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProducts, Description = "Edit all the Product's LocalizedKits")]
        public bool EditProductAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProducts, Description = "Delete a Product")]
        public bool DeleteProduct { get; set; }

        // -------------------------------------------------------------
        // Attribute section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Create a new Attribute")]
        public bool CreateAttribute { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Edit an existing Attribute")]
        public bool EditAttribute { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Edit the Name of an existing Attribute")]
        public bool EditAttributeName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Edit all the Attribute's LocalizedKits")]
        public bool EditAttributeAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Delete an Attribute")]
        public bool DeleteAttribute { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Create a new AttributeOption")]
        public bool CreateAttributeOption { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Edit an existing AttributeOption")]
        public bool EditAttributeOption { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Edit the Name of an existing AttributeOption")]
        public bool EditAttributeOptionName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Edit all the AttributeOption's LocalizedKits")]
        public bool EditAttributeOptionAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupAttributes, Description = "Delete an AttributeOption")]
        public bool DeleteAttributeOption { get; set; }

        // -------------------------------------------------------------
        // Variant section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Create a new Variant")]
        public bool CreateVariant { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Edit an existing Variant")]
        public bool EditVariant { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Edit the Name of an existing Variant")]
        public bool EditVariantName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Edit the Option Selector of an existing Variant")]
        public bool EditVariantSelector { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Edit all the Variant's LocalizedKits")]
        public bool EditVariantAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Delete a Variant")]
        public bool DeleteVariant { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Create a new VariantOption")]
        public bool CreateVariantOption { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Edit an existing VariantOption")]
        public bool EditVariantOption { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Edit the Name of an existing VariantOption")]
        public bool EditVariantOptionName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Edit all the VariantOption's LocalizedKits")]
        public bool EditVariantOptionAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupVariants, Description = "Delete a VariantOption")]
        public bool DeleteVariantOption { get; set; }

        // -------------------------------------------------------------
        // ShippingBox section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupShippingBoxes, Description = "Create a new ShippingBox")]
        public bool CreateShippingBox { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupShippingBoxes, Description = "Edit an existing ShippingBox")]
        public bool EditShippingBox { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupShippingBoxes, Description = "Edit the Name of an existing ShippingBox")]
        public bool EditShippingBoxName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupShippingBoxes, Description = "Edit all the ShippingBox's LocalizedKits")]
        public bool EditShippingBoxAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupShippingBoxes, Description = "Delete a ShippingBox")]
        public bool DeleteShippingBox { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProductCategories, Description = "Create a newCategory")]

        public bool CreateCategory { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProductCategories, Description = "Edit an existing Category")]
        public bool EditCategory { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProductCategories, Description = "Edit the Name of an existing Category")]
        public bool EditCategoryName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProductCategories, Description = "Edit the Url of an existing Category")]
        public bool EditCategoryUrl { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProductCategories, Description = "Edit all the Category's LocalizedKits")]
        public bool EditCategoryAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupProductCategories, Description = "Delete a Category")]
        public bool DeleteCategory { get; set; }

        // -------------------------------------------------------------
        // Blog section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupBlogPosts, Description = "Create a new BlogPost")]
        public bool CreateBlogPost { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupBlogPosts, Description = "Edit an existing BlogPost")]
        public bool EditBlogPost { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupBlogPosts, Description = "Delete a BlogPost")]
        public bool DeleteBlogPost { get; set; }

        // -------------------------------------------------------------
        // Content section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupContentSections, Description = "Create a new ContentSection")]
        public bool CreateContentSection { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupContentSections, Description = "Edit an existing ContentSection")]
        public bool EditContentSection { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupContentSections, Description = "Edit the Name of an existing ContentSection")]
        public bool EditContentSectionName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupContentSections, Description = "Edit all the ContentSection's LocalizedKits")]
        public bool EditContentSectionAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupContentSections, Description = "Delete a ContentSection")]
        public bool DeleteContentSection { get; set; }

        // -------------------------------------------------------------
        // Email section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Create a new Email")]
        public bool CreateEmail { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Edit an existing Email")]
        public bool EditEmail { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Edit the Name of an existing Email")]
        public bool EditEmailName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Edit all the Email's LocalizedKits")]
        public bool EditEmailAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Delete an Email")]
        public bool DeleteEmail { get; set; }

        // -------------------------------------------------------------
        // Mailing List section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Create a new Mailing list")]
        public bool CreateMailingList { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Edit an existing Mailing list")]
        public bool EditMailingList { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Edit an existing Mailing list Name")]
        public bool EditMailingListName { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Edit all the Mailing List's LocalizedKits")]
        public bool EditMailingListAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupEmails, Description = "Delete a Mailing list")]
        public bool DeleteMailingList { get; set; }

        // -------------------------------------------------------------
        // Seo section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupSeoSections, Description = "Create a new SeoSection")]
        public bool CreateSeoSection { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupSeoSections, Description = "Edit an existing SeoSection")]
        public bool EditSeoSection { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupSeoSections, Description = "Edit the Page of an existing SeoSection")]
        public bool EditSeoSectionPage { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupSeoSections, Description = "Edit all the SeoSection's LocalizedKits")]
        public bool EditSeoSectionAllLocalizedKits { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupSeoSections, Description = "Delete a SeoSection")]
        public bool DeleteSeoSection { get; set; }

        // -------------------------------------------------------------
        // User section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupUsers, Description = "Create a new User")]
        public bool CreateUser { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupUsers, Description = "Edit an existing User")]
        public bool EditUser { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupUsers, Description = "Delete a User")]
        public bool DeleteUser { get; set; }

        // -------------------------------------------------------------
        // UserRole section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupUsers, Description = "Create a new UserRole")]
        public bool CreateUserRole { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupUsers, Description = "Edit an existing UserRole")]
        public bool EditUserRole { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupUsers, Description = "Delete a UserRole")]
        public bool DeleteUserRole { get; set; }

        // -------------------------------------------------------------
        // Country section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupCountries, Description = "Create a new Country")]
        public bool CreateCountry { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupCountries, Description = "Edit an existing Country")]
        public bool EditCountry { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupCountries, Description = "Delete a Country")]
        public bool DeleteCountry { get; set; }

        // -------------------------------------------------------------
        // Coupon section
        // -------------------------------------------------------------

        [Display(Name = permissionDisplayName, GroupName = permissionGroupCoupons, Description = "Create a new Coupon")]
        public bool CreateCoupon { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupCoupons, Description = "Edit an existing Coupon")]
        public bool EditCoupon { get; set; }

        [Display(Name = permissionDisplayName, GroupName = permissionGroupCoupons, Description = "Delete a Coupon")]
        public bool DeleteCoupon { get; set; }

        // -------------------------------------------------------------
        // Logs
        // -------------------------------------------------------------

        /// <summary>
        /// View code exception logs.
        /// </summary>
        [Display(Name = permissionDisplayName, GroupName = permissionGroupNotifications, Description = "View code exception logs")]
        public bool ViewExceptionLogs { get; set; }

        /// <summary>
        /// View ecommerce related logs.
        /// </summary>
        [Display(Name = permissionDisplayName, GroupName = permissionGroupNotifications, Description = "View ecommerce related logs")]
        public bool ViewEcommerceLogs { get; set; }

        /// <summary>
        /// View content related logs.
        /// </summary>
        [Display(Name = permissionDisplayName, GroupName = permissionGroupNotifications, Description = "View content related logs")]
        public bool ViewContentLogs { get; set; }

        private const string permissionDisplayName = "Permission";
        private const string permissionGroupAccessAreas = "Access specific areas of the Administration panel";
        private const string permissionGroupOrders = "Orders";
        private const string permissionGroupProducts = "Products";
        private const string permissionGroupAttributes = "Attributes";
        private const string permissionGroupVariants = "Variants";
        private const string permissionGroupShippingBoxes = "ShippingBoxes";
        private const string permissionGroupProductCategories = "Product Categories";
        private const string permissionGroupBlogPosts = "Blog Posts";
        private const string permissionGroupContentSections = "Content Sections";
        private const string permissionGroupSeoSections = "Seo Sections";
        private const string permissionGroupUsers = "Users";
        private const string permissionGroupEmails = "Emails";
        private const string permissionGroupCountries = "Countries";
        private const string permissionGroupCoupons = "Coupons";
        private const string permissionGroupNotifications = "Notifications";
    }
}