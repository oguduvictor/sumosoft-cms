﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class AddressDto : BaseEntityDto
    {
        public AddressDto()
        {
        }

        public AddressDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public UserDto User { get; set; }

        [CanBeNull]
        public string FirstName { get; set; }

        [CanBeNull]
        public string LastName { get; set; }

        [CanBeNull]
        public string AddressLine1 { get; set; }

        [CanBeNull]
        public string AddressLine2 { get; set; }

        [CanBeNull]
        public string City { get; set; }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public string StateCountyProvince { get; set; }

        [CanBeNull]
        public string Postcode { get; set; }

        [CanBeNull]
        public string PhoneNumber { get; set; }
    }
}