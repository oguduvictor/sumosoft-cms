﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class AttributeOptionDto : BaseEntityDto
    {
        public AttributeOptionDto()
        {
        }

        public AttributeOptionDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Tags { get; set; }

        public AttributeDto Attribute { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [NotNull]
        public List<AttributeOptionLocalizedKitDto> AttributeOptionLocalizedKits { get; set; } = new List<AttributeOptionLocalizedKitDto>();

        [CanBeNull]
        public string LocalizedTitle { get; set; }
    }
}