﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class OrderAddressDto : BaseEntityDto
    {
        public OrderAddressDto()
        {
        }

        public OrderAddressDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string AddressFirstName { get; set; }

        [CanBeNull]
        public string AddressLastName { get; set; }

        [CanBeNull]
        public string AddressLine1 { get; set; }

        [CanBeNull]
        public string AddressLine2 { get; set; }

        [CanBeNull]
        public string CountryName { get; set; }

        [CanBeNull]
        public string StateCountyProvince { get; set; }

        [CanBeNull]
        public string City { get; set; }

        [CanBeNull]
        public string Postcode { get; set; }

        [CanBeNull]
        public string PhoneNumber { get; set; }
    }
}