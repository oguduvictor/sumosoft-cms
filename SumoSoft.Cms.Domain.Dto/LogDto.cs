﻿#pragma warning disable 1591
namespace SumoSoft.Cms.Domain.Dto
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class LogDto : BaseEntityDto
    {
        public LogTypesEnum Type { get; set; }

        [CanBeNull]
        public string Message { get; set; }

        [CanBeNull]
        public string Details { get; set; }

        public UserDto User { get; set; }
    }
}