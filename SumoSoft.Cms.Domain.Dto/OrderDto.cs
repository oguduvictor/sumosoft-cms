﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class OrderDto : BaseEntityDto
    {
        public OrderDto()
        {
        }

        public OrderDto(Guid id)
        {
            this.Id = id;
        }

        public int? OrderNumber { get; set; }

        public OrderStatusEnum Status { get; set; }

        [CanBeNull]
        public string Tags { get; set; }

        [NotNull]
        public List<string> StatusDescriptions { get; set; } = new List<string>();

        [CanBeNull]
        public string CountryName { get; set; }

        [CanBeNull]
        public string CountryLanguageCode { get; set; }

        /// <summary>
        /// Currency Symbol ($, £...)
        /// </summary>
        [CanBeNull]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Three letters ISO4217 Currency Symbol (USD, GBP...)
        /// </summary>
        [CanBeNull]
        public string Iso4217CurrencySymbol { get; set; }

        public Guid UserId { get; set; }

        [CanBeNull]
        public string UserFirstName { get; set; }

        [CanBeNull]
        public string UserLastName { get; set; }

        [CanBeNull]
        public string UserEmail { get; set; }

        [CanBeNull]
        public string TransactionId { get; set; }

        [NotNull]
        public List<OrderShippingBoxDto> OrderShippingBoxes { get; set; } = new List<OrderShippingBoxDto>();

        [CanBeNull]
        public OrderAddressDto ShippingAddress { get; set; }

        [CanBeNull]
        public OrderAddressDto BillingAddress { get; set; }

        [CanBeNull]
        public string Comments { get; set; }

        [NotNull]
        public List<SentEmailDto> SentEmails { get; set; } = new List<SentEmailDto>();

        [CanBeNull]
        public OrderCouponDto OrderCoupon { get; set; }

        [CanBeNull]
        public OrderCreditDto OrderCredit { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }

        public double TotalToBePaid { get; set; }

        public double TotalPaid { get; set; }
    }
}