﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ProductVariantDto : BaseEntityDto
    {
        public ProductVariantDto()
        {
        }

        public ProductVariantDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public ProductDto Product { get; set; }

        [CanBeNull]
        public VariantDto Variant { get; set; }

        [NotNull]
        public List<VariantOptionDto> VariantOptions { get; set; } = new List<VariantOptionDto>();

        public bool DefaultBooleanValue { get; set; }

        public double DefaultDoubleValue { get; set; }

        public int DefaultIntegerValue { get; set; }

        [CanBeNull]
        public string DefaultStringValue { get; set; }

        [CanBeNull]
        public string DefaultJsonValue { get; set; }

        [CanBeNull]
        public VariantOptionDto DefaultVariantOptionValue { get; set; }
    }
}