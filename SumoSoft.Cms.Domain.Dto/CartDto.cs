﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Extensions.Dto;

    public class CartDto : BaseEntityDto
    {
        public CartDto()
        {
        }

        public CartDto(Guid id)
        {
            this.Id = id;
        }

        [NotNull]
        public List<CartShippingBoxDto> CartShippingBoxes { get; set; } = new List<CartShippingBoxDto>();

        [CanBeNull]
        public AddressDto ShippingAddress { get; set; }

        [CanBeNull]
        public AddressDto BillingAddress { get; set; }

        [CanBeNull]
        public UserDto User { get; set; }

        [CanBeNull]
        public CouponDto Coupon { get; set; }

        [CanBeNull]
        public UserCreditDto UserCredit { get; set; }

        [NotNull]
        public List<SentEmailDto> SentEmails { get; set; } = new List<SentEmailDto>();

        [CanBeNull]
        public List<CartError> Errors { get; set; }

        public double CouponValue { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }

        public double TotalToBePaid { get; set; }
    }
}