﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;

    public class WishListDto : BaseEntityDto
    {
        public WishListDto()
        {
        }

        public WishListDto(Guid id)
        {
            this.Id = id;
        }

        public UserDto User { get; set; }

        public List<CartItemDto> CartItems { get; set; } = new List<CartItemDto>();
    }
}