﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using JetBrains.Annotations;

    public class CmsSettingsDto : BaseEntityDto
    {
        public bool IsSeeded { get; set; }

        [CanBeNull]
        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        [CanBeNull]
        public string SmtpDisplayName { get; set; }

        public string SmtpEmail { get; set; }

        public string SmtpPassword { get; set; }

        public bool UseAzureStorage { get; set; }
    }
}