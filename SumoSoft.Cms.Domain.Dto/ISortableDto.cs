﻿

#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    public interface ISortableDto
    {
        int SortOrder { get; set; }

    }
}
