﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using JetBrains.Annotations;

    public class ProductStockUnitLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        [CanBeNull]
        public ProductStockUnitDto ProductStockUnit { get; set; }

        [CanBeNull]
        public CountryDto Country { get; set; }

        /// <summary>
        /// Gets or sets the BasePrice.
        /// </summary>
        public double BasePrice { get; set; }

        /// <summary>
        /// Gets or sets the SalePrice.
        /// </summary>
        public double SalePrice { get; set; }

        /// <summary>
        /// Gets or sets the MembershipSalePrice.
        /// </summary>
        public double MembershipSalePrice { get; set; }
    }
}
