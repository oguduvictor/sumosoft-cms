﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class CartItemDto : BaseEntityDto
    {
        public CartItemDto()
        {
        }

        public CartItemDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CartShippingBoxDto CartShippingBox { get; set; }

        [CanBeNull]
        public virtual WishListDto WishList { get; set; }

        [CanBeNull]
        public ProductDto Product { get; set; }

        public int Quantity { get; set; } = 1;

        [CanBeNull]
        public string Image { get; set; }

        [NotNull]
        public List<CartItemVariantDto> CartItemVariants { get; set; } = new List<CartItemVariantDto>();

        [NotNull]
        public List<TaxDto> TaxesOverride { get; set; } = new List<TaxDto>();

        public DateTime MinEta { get; set; }

        public DateTime MaxEta { get; set; }

        public bool CouponApplies { get; set; }

        public double SubtotalBeforeTax { get; set; }

        public double SubtotalAfterTax { get; set; }

        public double SubtotalBeforeTaxWithoutSalePrice { get; set; }

        public double SubtotalAfterTaxWithoutSalePrice { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }
    }
}