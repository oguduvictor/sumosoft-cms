﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using SumoSoft.Cms.Domain.Interfaces;

    public class MailingListLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public MailingListDto MailingList { get; set; }

        public CountryDto Country { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        public string Description { get; set; }
    }
}
