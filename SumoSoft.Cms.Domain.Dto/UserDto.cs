﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class UserDto : BaseEntityDto
    {
        public UserDto()
        {
        }

        public UserDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string FirstName { get; set; }

        [CanBeNull]
        public string LastName { get; set; }

        [NotNull]
        public string FullName => $"{this.FirstName} {this.LastName}";

        [CanBeNull]
        public string Email { get; set; }

        [CanBeNull]
        public string Password { get; set; }

        [CanBeNull]
        public UserRoleDto Role { get; set; }

        public DateTime? LastLogin { get; set; }

        [NotNull]
        public IList<AddressDto> Addresses { get; set; } = new List<AddressDto>();

        public GenderEnum Gender { get; set; }

        [NotNull]
        public List<string> GenderDescriptions { get; set; } = new List<string>();

        [CanBeNull]
        public string LoginProvider { get; set; }

        public bool ShowMembershipSalePrice { get; set; }

        [CanBeNull]
        public WishListDto WishList { get; set; }

        [CanBeNull]
        public CartDto Cart { get; set; }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [NotNull]
        public List<UserAttributeDto> UserAttributes { get; set; } = new List<UserAttributeDto>();

        [NotNull]
        public List<UserLocalizedKitDto> UserLocalizedKits { get; set; } = new List<UserLocalizedKitDto>();

        [NotNull]
        public List<MailingListSubscriptionDto> MailingListSubscriptions { get; set; } = new List<MailingListSubscriptionDto>();

        [NotNull]
        public virtual List<UserVariantDto> UserVariants { get; set; } = new List<UserVariantDto>();

        public bool IsRegistered => !string.IsNullOrEmpty(this.Email) && !string.IsNullOrEmpty(this.Password);
    }
}