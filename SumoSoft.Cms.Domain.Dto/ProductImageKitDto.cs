﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public class ProductImageKitDto : BaseEntityDto
    {
        public ProductImageKitDto()
        {
        }

        public ProductImageKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public ProductDto Product { get; set; }

        [NotNull]
        public List<VariantOptionDto> VariantOptions { get; set; } = new List<VariantOptionDto>();

        [NotNull]
        public List<ProductImageDto> ProductImages { get; set; } = new List<ProductImageDto>();

        [NotNull]
        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<ProductImageKitDto> RelatedProductImageKits { get; set; } = new List<ProductImageKitDto>();

        public bool Matches(ICollection<VariantOptionDto> selectedVariantOptions)
        {
            return this.VariantOptions.All(variantOption => selectedVariantOptions.Any(x => x.Id == variantOption.Id));
        }
    }
}