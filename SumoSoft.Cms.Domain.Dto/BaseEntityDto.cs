﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class BaseEntityDto
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public bool? IsDisabled { get; set; }

        public DateTime? CreatedDate { get; set; }

        [CanBeNull]
        public DateTime? DeletedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Determines whether the entity is new or it has already been saved into the database.
        /// </summary>
        public bool? IsNew { get; set; }

        /// <summary>
        /// Determines whether the entity has been modified.
        /// </summary>
        public bool? IsModified { get; set; }

        /// <summary>
        /// Determines whether the entity has been deleted.
        /// </summary>
        public bool? IsDeleted { get; set; }
    }
}