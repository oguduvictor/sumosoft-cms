﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class ShippingBoxLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public ShippingBoxLocalizedKitDto()
        {
        }

        public ShippingBoxLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        public CountryDto Country { get; set; }

        [CanBeNull]
        public ShippingBoxDto ShippingBox { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the InternalDescription.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string InternalDescription { get; set; }

        /// <summary>
        /// Gets or sets the Carrier.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Carrier { get; set; }

        /// <summary>
        /// Gets or sets the MinDays.
        /// </summary>
        public int MinDays { get; set; }

        /// <summary>
        /// Gets or sets the MaxDays.
        /// </summary>
        public int MaxDays { get; set; }

        /// <summary>
        /// Gets or sets the CountWeekends.
        /// </summary>
        public bool CountWeekends { get; set; }

        /// <summary>
        /// Gets or sets the Price.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Gets or sets the FreeShippingMinimumPrice.
        /// </summary>
        public double FreeShippingMinimumPrice { get; set; }
    }
}