﻿#pragma warning disable 1591
namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class BlogCommentDto : BaseEntityDto
    {
        public BlogCommentDto()
        {
        }

        public BlogCommentDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Title { get; set; }

        public UserDto Author { get; set; }

        [CanBeNull]
        public string Email { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string Body { get; set; }

        public bool Published { get; set; }

        public BlogPostDto Post { get; set; }
    }
}