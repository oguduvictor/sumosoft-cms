﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class OrderItemDto : BaseEntityDto
    {
        public OrderItemDto()
        {
        }

        public OrderItemDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public OrderShippingBoxDto OrderShippingBox { get; set; }

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }

        [CanBeNull]
        public string ProductName { get; set; }

        [CanBeNull]
        public string ProductUrl { get; set; }

        [CanBeNull]
        public string ProductCode { get; set; }

        public int ProductQuantity { get; set; } = 1;

        public Guid ProductStockUnitId { get; set; }

        public DateTime ProductStockUnitReleaseDate { get; set; }

        public bool ProductStockUnitEnablePreorder { get; set; }

        public int ProductStockUnitShipsIn { get; set; }

        [CanBeNull]
        public string ProductStockUnitCode { get; set; }

        public double ProductStockUnitBasePrice { get; set; }

        public double ProductStockUnitSalePrice { get; set; }

        public double ProductStockUnitMembershipSalePrice { get; set; }

        [CanBeNull]
        public string Image { get; set; }

        [NotNull]
        public List<OrderItemVariantDto> OrderItemVariants { get; set; } = new List<OrderItemVariantDto>();

        [NotNull]
        public virtual List<OrderItemTaxDto> OrderTaxes { get; set; } = new List<OrderItemTaxDto>();

        public OrderItemStatusEnum Status { get; set; }

        [NotNull]
        public List<string> StatusDescriptions { get; set; } = new List<string>();

        public OrderItemStockStatusEnum StockStatus { get; set; } = OrderItemStockStatusEnum.Decreased;

        [NotNull]
        public List<string> StockStatusDescriptions { get; set; } = new List<string>();

        public DateTime MinEta { get; set; }

        public DateTime MaxEta { get; set; }

        public double SubtotalBeforeTax { get; set; }

        public double SubtotalAfterTax { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }
    }
}