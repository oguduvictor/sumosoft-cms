﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class CartItemVariantDto : BaseEntityDto
    {
        public CartItemVariantDto()
        {
        }

        public CartItemVariantDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CartItemDto CartItem { get; set; }

        [CanBeNull]
        public VariantDto Variant { get; set; }

        public bool BooleanValue { get; set; }

        public double DoubleValue { get; set; }

        public int IntegerValue { get; set; }

        [CanBeNull]
        public string StringValue { get; set; }

        [CanBeNull]
        public string JsonValue { get; set; }

        [CanBeNull]
        public VariantOptionDto VariantOptionValue { get; set; }

        public double PriceModifier { get; set; }
    }
}