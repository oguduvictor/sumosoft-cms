﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ContentSectionDto : BaseEntityDto
    {
        public ContentSectionDto()
        {
        }

        public ContentSectionDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Schema { get; set; }

        [NotNull]
        public List<ProductAttributeDto> ProductAttributes { get; set; } = new List<ProductAttributeDto>();

        [NotNull]
        public List<ContentSectionLocalizedKitDto> ContentSectionLocalizedKits { get; set; } = new List<ContentSectionLocalizedKitDto>();

        [CanBeNull]
        public string LocalizedContent { get; set; }
    }
}