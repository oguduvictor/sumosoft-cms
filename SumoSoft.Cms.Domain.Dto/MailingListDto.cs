﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;

    public class MailingListDto : BaseEntityDto, ISortableDto
    {
        public MailingListDto()
        {
        }

        public MailingListDto(Guid id)
        {
            this.Id = id;
        }

        public string Name { get; set; }

        public int SortOrder { get; set; }

        public List<MailingListSubscriptionDto> MailingListSubscriptions { get; set; } = new List<MailingListSubscriptionDto>();

        public List<MailingListLocalizedKitDto> MailingListLocalizedKits { get; set; } = new List<MailingListLocalizedKitDto>();

        public string LocalizedTitle { get; set; }

        public string LocalizedDescription { get; set; }

        public int TotalUsers { get; set; }

        public int TotalSubscribers { get; set; }
    }
}
