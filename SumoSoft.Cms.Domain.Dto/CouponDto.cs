﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class CouponDto : BaseEntityDto
    {
        public CouponDto()
        {
        }

        public CouponDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Code { get; set; }

        public double Amount { get; set; }

        public double Percentage { get; set; }

        [CanBeNull]
        public CountryDto Country { get; set; }

        public bool Published { get; set; }

        public DateTime ExpirationDate { get; set; }

        public int ValidityTimes { get; set; }

        [NotNull]
        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();

        [CanBeNull]
        public UserDto Referee { get; set; }

        public double RefereeReward { get; set; }

        [CanBeNull]
        public string Recipient { get; set; }

        [CanBeNull]
        public string Note { get; set; }

        public bool AllowOnSale { get; set; }
    }
}