﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class OrderCreditDto : BaseEntityDto
    {
        public OrderCreditDto()
        {
        }

        public OrderCreditDto(Guid id)
        {
            this.Id = id;
        }

        public double Amount { get; set; }

        [CanBeNull]
        public string Reference { get; set; }
    }
}