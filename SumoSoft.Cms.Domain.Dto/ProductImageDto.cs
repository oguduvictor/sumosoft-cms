﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class ProductImageDto : BaseEntityDto, ISortableDto
    {
        public ProductImageDto()
        {
        }

        public ProductImageDto(Guid id)
        {
            this.Id = id;
        }

        public ProductImageKitDto ProductImageKit { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string AltText { get; set; }

        public int SortOrder { get; set; }
    }
}