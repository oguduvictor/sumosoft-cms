﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class OrderShippingBoxDto : BaseEntityDto
    {
        public OrderShippingBoxDto()
        {
        }

        public OrderShippingBoxDto(Guid id)
        {
            this.Id = id;
        }

        public OrderDto Order { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Title { get; set; }

        [CanBeNull]
        public string Description { get; set; }

        [CanBeNull]
        public string InternalDescription { get; set; }

        public int MinDays { get; set; }

        public int MaxDays { get; set; }

        public bool CountWeekends { get; set; }

        public double ShippingPriceBeforeTax { get; set; }

        public double ShippingPriceAfterTax { get; set; }

        [CanBeNull]
        public string TrackingCode { get; set; }

        public OrderShippingBoxStatusEnum Status { get; set; }

        [NotNull]
        public List<string> StatusDescriptions { get; set; } = new List<string>();

        [NotNull]
        public virtual List<OrderItemDto> OrderItems { get; set; } = new List<OrderItemDto>();
    }
}