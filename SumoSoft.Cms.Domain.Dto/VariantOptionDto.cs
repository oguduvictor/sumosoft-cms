﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class VariantOptionDto : BaseEntityDto, ISortableDto
    {
        public VariantOptionDto()
        {
        }

        public VariantOptionDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        [CanBeNull]
        public string Image { get; set; }

        public int SortOrder { get; set; }

        [CanBeNull]
        public string Tags { get; set; }

        public bool LocalizedIsDisabled { get; set; }

        [CanBeNull]
        public string LocalizedTitle { get; set; }

        [CanBeNull]
        public string LocalizedDescription { get; set; }

        [CanBeNull]
        public VariantDto Variant { get; set; }

        [NotNull]
        public List<VariantOptionLocalizedKitDto> VariantOptionLocalizedKits { get; set; } = new List<VariantOptionLocalizedKitDto>();

        public double LocalizedPriceModifier { get; set; }
    }
}