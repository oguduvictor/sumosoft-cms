﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class MailingListSubscriptionDto
    {
        [NotNull]
        public MailingListDto MailingList { get; set; }

        [NotNull]
        public UserDto User { get; set; }

        public MailingListSubscriptionStatusEnum Status { get; set; }

        public List<string> StatusDescriptions { get; set; }
    }
}
