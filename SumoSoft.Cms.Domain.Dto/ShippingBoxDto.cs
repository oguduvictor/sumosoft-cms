﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ShippingBoxDto : BaseEntityDto, ISortableDto
    {
        public ShippingBoxDto()
        {
        }

        public ShippingBoxDto(Guid id)
        {
            this.Id = id;
        }

        public int SortOrder { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        public bool RequiresShippingAddress { get; set; }

        [NotNull]
        public List<CountryDto> Countries { get; set; } = new List<CountryDto>();

        [NotNull]
        public List<ProductDto> Products { get; set; } = new List<ProductDto>();

        [NotNull]
        public List<ShippingBoxLocalizedKitDto> ShippingBoxLocalizedKits { get; set; } = new List<ShippingBoxLocalizedKitDto>();

        [CanBeNull]
        public string LocalizedTitle { get; set; }

        [CanBeNull]
        public string LocalizedDescription { get; set; }

        [CanBeNull]
        public string LocalizedInternalDescription { get; set; }

        public double LocalizedPrice { get; set; }

        public int LocalizedMinDays { get; set; }

        public int LocalizedMaxDays { get; set; }

        public bool LocalizedCountWeekends { get; set; }

        public double LocalizedFreeShippingMinimumPrice { get; set; }
    }
}