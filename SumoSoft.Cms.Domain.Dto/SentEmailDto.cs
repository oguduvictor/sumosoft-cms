﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class SentEmailDto : BaseEntityDto
    {
        public SentEmailDto()
        {
        }

        public SentEmailDto(Guid id)
        {
            this.Id = id;
        }

        public EmailDto Email { get; set; }

        public UserDto User { get; set; }

        public OrderDto Order { get; set; }

        public CouponDto Coupon { get; set; }

        [CanBeNull]
        public string From { get; set; }

        [CanBeNull]
        public string To { get; set; }

        [CanBeNull]
        public string Reference { get; set; }
    }
}