﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ProductDto : BaseEntityDto
    {
        public ProductDto()
        {
        }

        public ProductDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        [CanBeNull]
        public string TaxCode { get; set; }

        [CanBeNull]
        public string Comments { get; set; }

        [CanBeNull]
        public string Tags { get; set; }

        [NotNull]
        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<ProductAttributeDto> ProductAttributes { get; set; } = new List<ProductAttributeDto>();

        [NotNull]
        public List<ProductVariantDto> ProductVariants { get; set; } = new List<ProductVariantDto>();

        [NotNull]
        public List<ProductStockUnitDto> ProductStockUnits { get; set; } = new List<ProductStockUnitDto>();

        [NotNull]
        public List<ProductImageKitDto> ProductImageKits { get; set; } = new List<ProductImageKitDto>();

        [NotNull]
        public List<ShippingBoxDto> ShippingBoxes { get; set; } = new List<ShippingBoxDto>();

        [NotNull]
        public List<ProductDto> RelatedProducts { get; set; } = new List<ProductDto>();

        [NotNull]
        public List<ProductLocalizedKitDto> ProductLocalizedKits { get; set; } = new List<ProductLocalizedKitDto>();

        [CanBeNull]
        public string LocalizedTitle { get; set; }

        [CanBeNull]
        public string LocalizedDescription { get; set; }

        [CanBeNull]
        public string LocalizedMetaTitle { get; set; }

        [CanBeNull]
        public string LocalizedMetaDescription { get; set; }

        public double TotalBeforeTaxWithoutSalePrice { get; set; }

        public double TotalAfterTaxWithoutSalePrice { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }
    }
}