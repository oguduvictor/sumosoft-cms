﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ContentSectionLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public ContentSectionLocalizedKitDto()
        {
        }

        public ContentSectionLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        public CountryDto Country { get; set; }

        public ContentSectionDto ContentSection { get; set; }

        /// <summary>
        /// Gets or sets the Content.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Content { get; set; }

        public virtual List<ContentVersionDto> ContentVersions { get; set; } = new List<ContentVersionDto>();
    }
}