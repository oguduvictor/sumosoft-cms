﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using JetBrains.Annotations;

    public class BlogCategoryLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public BlogCategoryDto BlogCategory { get; set; }

        public CountryDto Country { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }
    }
}