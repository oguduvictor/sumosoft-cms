﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class CategoryDto : BaseEntityDto
    {
        public CategoryDto()
        {
        }

        public CategoryDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        public int SortOrder { get; set; }

        [CanBeNull]
        public string Tags { get; set; }

        [CanBeNull]
        public string ProductsSortOrder { get; set; }

        [CanBeNull]
        public string ProductStockUnitsSortOrder { get; set; }

        [CanBeNull]
        public string ProductImageKitsSortOrder { get; set; }

        public List<string> TypeDescriptions { get; set; }

        public CategoryTypesEnum Type { get; set; }

        [CanBeNull]
        public CategoryDto Parent { get; set; }

        [NotNull]
        public List<CategoryDto> Children { get; set; } = new List<CategoryDto>();

        [NotNull]
        public List<ProductDto> Products { get; set; } = new List<ProductDto>();

        [NotNull]
        public List<ProductStockUnitDto> ProductStockUnits { get; set; } = new List<ProductStockUnitDto>();

        [NotNull]
        public List<ProductImageKitDto> ProductImageKits { get; set; } = new List<ProductImageKitDto>();

        [NotNull]
        public List<CategoryLocalizedKitDto> CategoryLocalizedKits { get; set; } = new List<CategoryLocalizedKitDto>();

        public bool LocalizedIsDisabled { get; set; }

        [CanBeNull]
        public string LocalizedTitle { get; set; }

        [CanBeNull]
        public string LocalizedDescription { get; set; }

        [CanBeNull]
        public string LocalizedImage { get; set; }

        [CanBeNull]
        public string LocalizedFeaturedTitle { get; set; }

        [CanBeNull]
        public string LocalizedFeaturedDescription { get; set; }

        [CanBeNull]
        public string LocalizedFeaturedImage { get; set; }

    }
}