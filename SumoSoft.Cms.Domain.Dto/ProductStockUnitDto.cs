﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

    public class ProductStockUnitDto : BaseEntityDto
    {
        public ProductStockUnitDto()
        {
        }

        public ProductStockUnitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public ProductDto Product { get; set; }

        [NotNull]
        public List<ProductStockUnitLocalizedKitDto> ProductStockUnitLocalizedKits { get; set; } = new List<ProductStockUnitLocalizedKitDto>();

        [NotNull]
        public List<VariantOptionDto> VariantOptions { get; set; } = new List<VariantOptionDto>();

        public DateTime DispatchDate { get; set; } = DateTime.Now;

        public int Stock { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        public bool EnablePreorder { get; set; }

        public int DispatchTime { get; set; }

        [NotNull]
        public List<CategoryDto> Categories { get; set; } = new List<CategoryDto>();

        public bool Matches(ICollection<VariantOptionDto> selectedVariantOptions)
        {
            return this.VariantOptions.All(variantOption => selectedVariantOptions.Any(x => x?.Id == variantOption.Id));
        }

        public double LocalizedBasePrice { get; set; }

        public double LocalizedSalePrice { get; set; }

        public double LocalizedMembershipSalePrice { get; set; }
    }
}