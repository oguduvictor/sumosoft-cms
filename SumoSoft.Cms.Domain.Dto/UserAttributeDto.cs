﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class UserAttributeDto : BaseEntityDto
    {
        public UserAttributeDto()
        {
        }

        public UserAttributeDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public UserDto User { get; set; }

        [CanBeNull]
        public AttributeDto Attribute { get; set; }

        [CanBeNull]
        public string StringValue { get; set; }

        public bool BooleanValue { get; set; }

        public DateTime? DateTimeValue { get; set; }

        [CanBeNull]
        public string ImageValue { get; set; }

        public double DoubleValue { get; set; }

        [CanBeNull]
        public AttributeOptionDto AttributeOptionValue { get; set; }

        [CanBeNull]
        public ContentSectionDto ContentSectionValue { get; set; }

        [CanBeNull]
        public string JsonValue { get; set; }

    }
}