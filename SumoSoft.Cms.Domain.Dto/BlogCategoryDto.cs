﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class BlogCategoryDto : BaseEntityDto
    {
        public BlogCategoryDto()
        {
        }

        public BlogCategoryDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [NotNull]
        public List<BlogPostDto> BlogPosts { get; set; } = new List<BlogPostDto>();

        [NotNull]
        public List<BlogCategoryLocalizedKitDto> BlogCategoryLocalizedKits { get; set; } = new List<BlogCategoryLocalizedKitDto>();

        public string LocalizedTitle { get; set; }

        public string LocalizedDescription { get; set; }
    }
}