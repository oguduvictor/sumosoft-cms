﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Dto
{
    using System;
    using JetBrains.Annotations;

    public class ProductLocalizedKitDto : BaseEntityDto, ILocalizedKitDto
    {
        public ProductLocalizedKitDto()
        {
        }

        public ProductLocalizedKitDto(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public CountryDto Country { get; set; }

        [CanBeNull]
        public ProductDto Product { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the MetaTitle.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string MetaTitle { get; set; }

        /// <summary>
        /// Gets or sets the MetaDescription.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string MetaDescription { get; set; }
    }
}