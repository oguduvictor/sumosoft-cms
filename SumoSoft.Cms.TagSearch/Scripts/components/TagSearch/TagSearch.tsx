﻿// ----------------------------------------------------------------
//
//     _____                       _____        __ _   
//    / ____|                     / ____|      / _| |  
//   | (___  _   _ _ __ ___   ___| (___   ___ | |_| |_ 
//    \___ \| | | | '_ ` _ \ / _ \\___ \ / _ \|  _| __|
//    ____) | |_| | | | | | | (_) |___) | (_) | | | |_ 
//   |_____/ \__,_|_| |_| |_|\___/_____/ \___/|_|  \__|
//
//
// ----------------------------------------------------------------

import * as React from "react";

export class TagGroup {
    sortOrder: number;
    title: string;
    tags: Array<Tag>;
}

export class Tag {
    sortOrder: number;
    attributeUrl: string;
    attributeOptionUrl: string;
    variantUrl: string;
    variantOptionUrl: string;
    categoryUrl: string;
    title: string;
}

export interface ITagSearchProps {
    placeholder: string;
    selectedTags: Array<Tag>;
    tagGroups: Array<TagGroup>;
    handleChangeSelectedTags(selectedTags: Array<Tag>);
    handleTagGroupDropdownToggle?(isOpen: boolean);
}

export class TagSearch extends React.Component<ITagSearchProps, undefined> {

    static defaultProps: ITagSearchProps;

    constructor(props) {
        super(props);
    }

    componentDidMount(): void {
        // ----------------------------------------------------------------
        // Close TagGroups when clicking outside of them
        // ----------------------------------------------------------------
        $(document).mousedown(e => {
            var tagGroupDropdown = $(".tag-group-dropdown");
            var element = e.target as any;
            if (!tagGroupDropdown.is(element) && !tagGroupDropdown.has(element).length) {
                this.closeTagGroups();
            }
        });
    }

    componentWillReceiveProps() {
        this.setState({
            selectedTags: this.props.selectedTags
        });
    }

    openTagGroup(e) {
        $(".tag-group-dropdown").removeClass("open");
        $(e.target).next().addClass("open");
        this.props.handleTagGroupDropdownToggle(true);
    }

    closeTagGroups() {
        $(".tag-group-dropdown").removeClass("open");
        this.props.handleTagGroupDropdownToggle(false);
    }

    handleTagSearchBarKeyDown(e) {
        if (e.key === "Enter" && e.target.value) {
            const tag = new Tag();
            tag.title = e.target.value;
            this.addTag(tag);
            e.target.value = "";
        }
        if (e.key === "Backspace" && !e.target.value) {
            this.removeTag(this.props.selectedTags[this.props.selectedTags.length - 1]);
        }
    }

    isTagSelected(tag: Tag): boolean {
        return this.props.selectedTags.some(x => JSON.stringify(x) === JSON.stringify(tag));
    }

    addTag(tag: Tag) {
        let selectedTags = this.props.selectedTags;
        if (tag.categoryUrl) {
            selectedTags = selectedTags.filter(x => !x.categoryUrl);
        }
        if (tag.attributeUrl) {
            selectedTags = selectedTags.filter(x => x.attributeUrl !== tag.attributeUrl);
        }
        if (tag.variantUrl) {
            selectedTags = selectedTags.filter(x => x.variantUrl !== tag.variantUrl);
        }
        selectedTags.push(tag);
        this.props.handleChangeSelectedTags(selectedTags);
    }

    removeTag(tag: Tag) {
        const selectedTags = this.props.selectedTags.filter(x => JSON.stringify(x) !== JSON.stringify(tag));
        this.props.handleChangeSelectedTags(selectedTags);
    }

    toggleTagFromTagGroup(tag: Tag) {
        this.isTagSelected(tag) ? this.removeTag(tag) : this.addTag(tag);
        this.closeTagGroups();
    }

    render() {
        return (
            <div className="tag-search-component">
                <div className="tag-search-bar">
                    <div className="search-icon"></div>
                    {
                        this.props.selectedTags.map((tag, i) => (
                            <div
                                key={i}
                                data-categoryurl={tag.categoryUrl}
                                data-attributeurl={tag.attributeUrl}
                                data-attributeoptionurl={tag.attributeOptionUrl}
                                data-varianturl={tag.variantUrl}
                                data-variantoptionurl={tag.variantOptionUrl}
                                data-title={tag.title}
                                className="tag">
                                {tag.title}
                                <span className="remove-tag" onClick={() => this.removeTag(tag)}>✕</span>
                            </div>
                        ))
                    }
                    <input
                        onKeyDown={(e) => this.handleTagSearchBarKeyDown(e)}
                        placeholder={this.props.placeholder} />
                </div>
                <div className="tag-groups">
                    {
                        this.props.tagGroups &&
                        this.props.tagGroups.filter(tagGroup => tagGroup.tags && tagGroup.tags.length > 0).orderBy(tag => tag.sortOrder).map((tagGroup, i) => (
                            <div key={i} className="tag-group">
                                <div
                                    data-title={tagGroup.title}
                                    className={tagGroup.tags.some(tag => this.isTagSelected(tag)) ? "tag-group-title active" : "tag-group-title"}
                                    onClick={(e) => this.openTagGroup(e)}>
                                    {
                                        tagGroup.title
                                    }
                                </div>
                                <div className="tag-group-dropdown">
                                    {
                                        tagGroup.tags &&
                                        tagGroup.tags.orderBy(tag => tag.sortOrder).map((tag, ii) =>
                                            <div key={ii}
                                                data-categoryurl={tag.categoryUrl}
                                                data-attributeurl={tag.attributeUrl}
                                                data-attributeoptionurl={tag.attributeOptionUrl}
                                                data-varianturl={tag.variantUrl}
                                                data-variantoptionurl={tag.variantOptionUrl}
                                                data-title={tag.title}
                                                className={this.isTagSelected(tag) ? "tag active" : "tag"}
                                                onClick={() => this.toggleTagFromTagGroup(tag)}>
                                                {
                                                    tag.title
                                                }
                                            </div>
                                        )
                                    }
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        );
    }
}

TagSearch.defaultProps = {
    placeholder: "Search..."
} as ITagSearchProps