﻿namespace SumoSoft.Cms.TagSearch.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RequestDbContextServices.Interfaces;
    using SumoSoft.Cms.TagSearch.Dto;
    using SumoSoft.Cms.TagSearch.Interfaces;

    /// <inheritdoc />
    public class TagSearchService : ITagSearchService
    {
        private readonly CmsDbContext requestDbContext;
        private readonly ICountryService countryService;

        /// <inheritdoc />
        public TagSearchService(
            IRequestDbContextService requestDbContextService,
            ICountryService countryService)
        {
            this.requestDbContext = requestDbContextService.Get();
            this.countryService = countryService;
        }

        /// <inheritdoc />
        [NotNull]
        public IOrderedQueryable<ProductImageKit> GetProductImageKits(CmsDbContext dbContext, List<Tag> tags)
        {
            tags = tags ?? new List<Tag>();

            var countyLanguageCode = this.countryService.GetCurrentLanguageCode();

            // --------------------------------------------------------------------------------------
            // IQueriable
            // --------------------------------------------------------------------------------------

            var iQueriableResult = dbContext.ProductImageKits
                .Where(x =>
                    !x.IsDisabled &&
                    !x.Product.IsDisabled &&
                    (
                        x.Product.ProductLocalizedKits.FirstOrDefault(lk => lk.Country.LanguageCode == countyLanguageCode) == null ||
                        !x.Product.ProductLocalizedKits.FirstOrDefault(lk => lk.Country.LanguageCode == countyLanguageCode).IsDisabled
                    ) &&
                    x.VariantOptions.All(vo =>
                        !vo.IsDisabled &&
                        (
                            vo.VariantOptionLocalizedKits.FirstOrDefault(lk => lk.Country.LanguageCode == countyLanguageCode) == null ||
                            !vo.VariantOptionLocalizedKits.FirstOrDefault(lk => lk.Country.LanguageCode == countyLanguageCode).IsDisabled
                        )));

            // --------------------------------------------------------------------------------------
            // Filter by AttributeOptions
            // --------------------------------------------------------------------------------------

            var attributeOptionUrls = tags
                    .Where(tag => !string.IsNullOrEmpty(tag.AttributeOptionUrl) &&
                                  !string.IsNullOrEmpty(tag.AttributeUrl))
                    .Select(tag => tag.AttributeUrl + tag.AttributeOptionUrl)
                    .ToList();

            if (attributeOptionUrls.Any())
            {
                iQueriableResult = iQueriableResult.Where(productImageKit =>
                    !attributeOptionUrls.Any() ||
                    productImageKit.Product.ProductAttributes.Any(pa => attributeOptionUrls.Any(tag => tag == pa.Attribute.Url + pa.AttributeOptionValue.Url)));
            }

            // --------------------------------------------------------------------------------------
            // Filter by VariantOptions
            // --------------------------------------------------------------------------------------

            var variantOptionUrls = tags
                .Where(tag => !string.IsNullOrEmpty(tag.VariantOptionUrl) &&
                              !string.IsNullOrEmpty(tag.VariantUrl))
                .Select(tag => tag.VariantUrl + tag.VariantOptionUrl)
                .ToList();

            if (variantOptionUrls.Any())
            {
                iQueriableResult = iQueriableResult.Where(productImageKit =>
                    !variantOptionUrls.Any() ||
                    productImageKit.VariantOptions.Any(vo => variantOptionUrls.Any(tag => tag == vo.Variant.Url + vo.Url)) ||
                    productImageKit.Product.ProductVariants.Where(pv => !pv.Variant.CreateProductImageKits).SelectMany(pv => pv.VariantOptions).Any(vo => variantOptionUrls.Any(tag => tag == vo.Variant.Url + vo.Url)));
            }

            // --------------------------------------------------------------------------------------
            // Filter by Category
            // --------------------------------------------------------------------------------------

            var categoryUrl = tags.FirstOrDefault(x => !string.IsNullOrEmpty(x.CategoryUrl))?.CategoryUrl;

            var category = dbContext.Categories.FirstOrDefault(x => x.Url == categoryUrl);

            if (category != null)
            {
                switch (category.Type)
                {
                    case CategoryTypesEnum.ProductImageKits:
                        {
                            iQueriableResult = iQueriableResult.Where(productImageKit => productImageKit.Categories.Any(c => c.Url == categoryUrl));

                            break;
                        }
                    case CategoryTypesEnum.Products:
                        {
                            iQueriableResult = iQueriableResult.Where(productImageKit => productImageKit.Product.Categories.Any(c => c.Url == categoryUrl));

                            break;
                        }
                    case CategoryTypesEnum.ProductStockUnits:
                        {
                            throw new NotImplementedException();
                        }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            // --------------------------------------------------------------------------------------
            // Filter by keywords
            // --------------------------------------------------------------------------------------

            var keywordTags = tags.Where(x =>
                    string.IsNullOrEmpty(x.CategoryUrl) &&
                    string.IsNullOrEmpty(x.AttributeOptionUrl) &&
                    string.IsNullOrEmpty(x.VariantOptionUrl) &&
                    !string.IsNullOrEmpty(x.Title))
                .ToList();

            // --------------------------------------------------------------------------------------
            // Keywords with a Variant constraint, to be matched only against VariantOption keywords
            // --------------------------------------------------------------------------------------

            var keywordTagsWithVariantConstraint = keywordTags.Where(x => !string.IsNullOrEmpty(x.VariantUrl));

            foreach (var tag in keywordTagsWithVariantConstraint)
            {
                iQueriableResult = iQueriableResult
                    .Select(x => new
                    {
                        ProductImageKit = x,
                        VariantOption = x.VariantOptions.FirstOrDefault(vo => vo.Variant.Url == tag.VariantUrl)
                    })
                    .Where(x =>
                        x.VariantOption != null &&
                        x.VariantOption.Tags.Contains(tag.Title) ||
                        x.VariantOption.AutoTags.Contains(tag.Title) ||
                        x.VariantOption.VariantOptionLocalizedKits.FirstOrDefault(lk => lk.Country.LanguageCode == countyLanguageCode).Title.Contains(tag.Title) ||
                        x.VariantOption.VariantOptionLocalizedKits.FirstOrDefault(lk => lk.Country.LanguageCode == countyLanguageCode).Description.Contains(tag.Title) ||
                        x.VariantOption.VariantOptionLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).Title.Contains(tag.Title) ||
                        x.VariantOption.VariantOptionLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).Description.Contains(tag.Title))
                    .Select(x => x.ProductImageKit);
            }

            // --------------------------------------------------------------------------------------
            // Keywords with an Attribute constraint, to be matched only against AttributeOption keywords
            // --------------------------------------------------------------------------------------

            var keywordTagsWithAttributeConstraint = keywordTags.Where(x => !string.IsNullOrEmpty(x.AttributeUrl));

            foreach (var tag in keywordTagsWithAttributeConstraint)
            {
                iQueriableResult = iQueriableResult
                    .Select(x => new
                    {
                        ProductImageKit = x,
                        AttributeOption = x.Product.ProductAttributes.FirstOrDefault(ao => ao.Attribute.Url == tag.AttributeUrl).AttributeOptionValue
                    })
                    .Where(x =>
                        x.AttributeOption != null &&
                        x.AttributeOption.Tags.Contains(tag.Title) ||
                        x.AttributeOption.AutoTags.Contains(tag.Title) ||
                        x.AttributeOption.AttributeOptionLocalizedKits.FirstOrDefault(lk => lk.Country.LanguageCode == countyLanguageCode).Title.Contains(tag.Title) ||
                        x.AttributeOption.AttributeOptionLocalizedKits.FirstOrDefault(lk => lk.Country.IsDefault).Title.Contains(tag.Title))
                    .Select(x => x.ProductImageKit);
            }

            // --------------------------------------------------------------------------------------
            // Keywords without any constraint
            // --------------------------------------------------------------------------------------

            var keywordsWithoutConstraint = keywordTags.Where(x =>
                    string.IsNullOrEmpty(x.CategoryUrl) &&
                    string.IsNullOrEmpty(x.AttributeUrl) &&
                    string.IsNullOrEmpty(x.AttributeOptionUrl) &&
                    string.IsNullOrEmpty(x.VariantUrl) &&
                    string.IsNullOrEmpty(x.VariantOptionUrl) &&
                    !string.IsNullOrEmpty(x.Title))
                .ToList();

            foreach (var tag in keywordsWithoutConstraint)
            {
                iQueriableResult = iQueriableResult.Where(x =>
                    x.Product.Tags.Contains(tag.Title) ||
                    x.Product.AutoTags.Contains(tag.Title));
            }

            // --------------------------------------------------------------------------------------
            // Sorting
            // --------------------------------------------------------------------------------------

            IOrderedQueryable<ProductImageKit> iOrderedQueryableResult;

            if (category == null)
            {
                iOrderedQueryableResult = iQueriableResult
                    .OrderBy(x => x.Product.Url);
            }
            else
            {
                List<Guid> idList;

                switch (category.Type)
                {
                    case CategoryTypesEnum.ProductImageKits when !string.IsNullOrEmpty(category.ProductImageKitsSortOrder):
                        {
                            idList = category.ProductImageKitsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

                            var firstId = idList[0];

                            iOrderedQueryableResult = iQueriableResult.OrderByDescending(x => x.Id == firstId);

                            foreach (var id in idList.Skip(1))
                            {
                                iOrderedQueryableResult = iOrderedQueryableResult.ThenByDescending(x => x.Id == id);
                            }

                            iOrderedQueryableResult = iOrderedQueryableResult.ThenByDescending(x => x.Product.Url);

                            break;
                        }
                    case CategoryTypesEnum.Products when !string.IsNullOrEmpty(category.ProductsSortOrder):
                        {
                            idList = category.ProductsSortOrder.Split(',').Select(x => new Guid(x.Trim())).ToList();

                            var firstId = idList[0];

                            iOrderedQueryableResult = iQueriableResult.OrderByDescending(x => x.Product.Id == firstId);

                            foreach (var id in idList.Skip(1))
                            {
                                iOrderedQueryableResult = iOrderedQueryableResult.ThenByDescending(x => x.Product.Id == id);
                            }

                            iOrderedQueryableResult = iOrderedQueryableResult.ThenByDescending(x => x.Product.Url);

                            break;
                        }
                    case CategoryTypesEnum.ProductStockUnits:
                        {
                            throw new NotImplementedException();
                        }
                    default:
                        iOrderedQueryableResult = iQueriableResult.OrderBy(x => x.Product.Url);
                        break;
                }
            }

            return iOrderedQueryableResult;
        }

        /// <inheritdoc />
        public TagGroup GetVariantTagGroup(IEnumerable<ProductImageKit> productImageKits, string variantName, int sortOrder)
        {
            var variantOptions = productImageKits
                .SelectMany(x => x.Product?.GetProductVariantByName(variantName)?.VariantOptions ?? new List<VariantOption>())
                .Distinct()
                .OrderBy(x => x.SortOrder)
                .ThenBy(x => x.Name)
                .ToList();

            if (!variantOptions.Any())
            {
                return null;
            }

            var tagGroupTitle = variantOptions.FirstOrDefault()?.Variant?.GetLocalizedTitle();

            return new TagGroup
            {
                SortOrder = sortOrder,
                Title = tagGroupTitle,
                Tags = variantOptions.Select(vo => new Tag
                {
                    SortOrder = vo.SortOrder,
                    VariantUrl = vo.Variant?.Url,
                    VariantOptionUrl = vo.Url,
                    Title = vo.GetLocalizedTitle()
                }).ToList()
            };
        }

        /// <inheritdoc />
        public TagGroup GetAttributeTagGroup(IEnumerable<ProductImageKit> productImageKits, string attributeName, int sortOrder)
        {
            var attributeOptions = productImageKits
                .Select(x => x.Product?.GetProductAttributeByName(attributeName)?.AttributeOptionValue)
                .Where(x => x != null)
                .Distinct()
                .OrderBy(x => x.Name)
                .ToList();

            if (!attributeOptions.Any())
            {
                return null;
            }

            var tagGroupTitle = attributeOptions.FirstOrDefault()?.Attribute.GetLocalizedTitle();

            return new TagGroup
            {
                SortOrder = sortOrder,
                Title = tagGroupTitle,
                Tags = attributeOptions.Select(ao => new Tag
                {
                    //SortOrder = ao.SortOrder,
                    AttributeUrl = ao.Attribute?.Url,
                    AttributeOptionUrl = ao.Url,
                    Title = ao.GetLocalizedTitle()
                }).ToList()
            };
        }

        /// <inheritdoc />
        public TagGroup GetCategoryTagGroup(IEnumerable<ProductImageKit> productImageKits, int sortOrder)
        {
            var categories = productImageKits
                .SelectMany(x => x.Categories.Concat(x.Product.AsNotNull().Categories))
                .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled())
                .Distinct()
                .ToList();

            if (!categories.Any())
            {
                return null;
            }

            return new TagGroup
            {
                SortOrder = sortOrder,
                Title = "Category",
                Tags = categories.Select(category => new Tag
                {
                    SortOrder = category.SortOrder,
                    CategoryUrl = category.Url,
                    Title = category.GetLocalizedTitle()
                }).ToList()
            };
        }

        /// <inheritdoc />
        public TagGroup GetCategoryTagGroup(string rootCategoryUrl, int sortOrder)
        {
            var categories = string.IsNullOrEmpty(rootCategoryUrl)
                ? this.requestDbContext.Categories.Where(x => !x.IsDisabled).OrderBy(x => x.SortOrder).AsEnumerable().Where(x => !x.GetLocalizedIsDisabled() && x.Parent == null).ToList()
                : this.requestDbContext.Categories.FirstOrDefault(x => x.Url == rootCategoryUrl)?.Children;

            if (categories == null || !categories.Any())
            {
                return null;
            }

            return new TagGroup
            {
                SortOrder = sortOrder,
                Title = "Category",
                Tags = categories.Select(category => new Tag
                {
                    SortOrder = category.SortOrder,
                    CategoryUrl = category.Url,
                    Title = category.GetLocalizedTitle()
                }).ToList()
            };
        }
    }
}
