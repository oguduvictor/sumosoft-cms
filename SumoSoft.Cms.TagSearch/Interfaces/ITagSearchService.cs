﻿namespace SumoSoft.Cms.TagSearch.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.TagSearch.Dto;

    /// <summary>
    ///
    /// </summary>
    public interface ITagSearchService
    {
        /// <summary>
        ///
        /// </summary>
        IOrderedQueryable<ProductImageKit> GetProductImageKits(CmsDbContext dbContext, List<Tag> tags);

        /// <summary>
        ///
        /// </summary>
        TagGroup GetAttributeTagGroup(IEnumerable<ProductImageKit> productImageKits, string attributeName, int sortOrder);

        /// <summary>
        ///
        /// </summary>
        TagGroup GetVariantTagGroup(IEnumerable<ProductImageKit> productImageKits, string variantName, int sortOrder);

        /// <summary>
        ///
        /// </summary>
        TagGroup GetCategoryTagGroup(IEnumerable<ProductImageKit> productImageKits, int sortOrder);

        /// <summary>
        ///
        /// </summary>
        /// <param name="rootCategoryUrl"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        TagGroup GetCategoryTagGroup(string rootCategoryUrl, int sortOrder);
    }
}