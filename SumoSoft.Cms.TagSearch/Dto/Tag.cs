﻿namespace SumoSoft.Cms.TagSearch.Dto
{
    /// <summary>
    ///
    /// </summary>
    public class Tag
    {
        /// <summary>
        ///
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        ///  When set, a match is tried against the target Attribute Url.
        /// </summary>
        public string AttributeUrl { get; set; }

        /// <summary>
        ///  When set, a match is tried against the target AttributeOption Url.
        /// </summary>
        public string AttributeOptionUrl { get; set; }

        /// <summary>
        /// When set, a match is tried against the target Variant Url.
        /// </summary>
        public string VariantUrl { get; set; }

        /// <summary>
        /// When set, a match is tried against the target VariantOption Url.
        /// </summary>
        public string VariantOptionUrl { get; set; }

        /// <summary>
        /// When set, a match is tried against the target Category Url.
        /// </summary>
        public string CategoryUrl { get; set; }

        /// <summary>
        /// The displayed title. When no other constraint is provided, a match is tried against all the target keywords.
        /// </summary>
        public string Title { get; set; }
    }
}