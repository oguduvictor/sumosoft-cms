﻿#pragma warning disable 1591

namespace SumoSoft.Cms.AuthenticationServices.Validators
{
    using System;
    using System.Linq;
    using System.Runtime.Caching;
    using System.Web;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public class AuthenticateUserValidator : AbstractValidator<AuthenticateUserDto>
    {
        private readonly IContentSectionService contentSectionService;
        private readonly IUtilityService utilityService;

        public AuthenticateUserValidator(
            IContentSectionService contentSectionService,
            IUtilityService utilityService)
        {
            this.contentSectionService = contentSectionService;
            this.utilityService = utilityService;

            this.RuleFor(x => x.Email).NotEmpty();
            this.RuleFor(x => x.Email).EmailAddress();
            this.RuleFor(x => x.Password).NotEmpty();
            this.Custom(x =>
            {
                using (var dbContext = new CmsDbContext())
                {
                    if (string.IsNullOrEmpty(x.Email))
                    {
                        return null;
                    }

                    if (string.IsNullOrEmpty(x.Password))
                    {
                        return null;
                    }

                    // -------------------------------------------------------
                    // Check if account is locked
                    // -------------------------------------------------------

                    var loginAttemptDetails = (LoginAttemptDetails)MemoryCache.Default.Get(HttpContext.Current.Request.UserHostAddress ?? string.Empty);

                    if (loginAttemptDetails != null && loginAttemptDetails.IsLocked())
                    {
                        var errorMessage = this.contentSectionService.ContentSection(dbContext,
                            ContentSectionName.Cms_Login_AccountLockedWarning,
                            ContentSectionName.Cms_Login_AccountLockedWarning_FallbackValue, true);

                        return new ValidationFailure("Password", errorMessage);
                    }

                    // -------------------------------------------------------
                    // Get User from db
                    // -------------------------------------------------------

                    var user = dbContext.Users.FirstOrDefault(y => y.Email == x.Email);

                    // -------------------------------------------------------
                    // Handle user not found error
                    // -------------------------------------------------------

                    if (user == null || string.IsNullOrEmpty(user.Password))
                    {
                        IncreaseLoginAttemptCount(loginAttemptDetails);

                        var errorMessage = this.contentSectionService.ContentSection(dbContext,
                            ContentSectionName.Cms_Login_AccountNotFoundWarning,
                            ContentSectionName.Cms_Login_AccountNotFoundWarning_FallbackValue, true);

                        return new ValidationFailure("Email", errorMessage);
                    }

                    // -------------------------------------------------------
                    // Handle wrong password
                    // -------------------------------------------------------

                    var password = x.Password;

                    if (user.Password != this.utilityService.Encrypt(password))
                    {
                        //--------------------------------------------
                        // I a User does not have Admin permissions,
                        // they can login using a passepartout
                        //--------------------------------------------

                        var userIsAdmin = user.Role?.AccessAdmin ?? false;

                        if (password == this.utilityService.GetAppSetting("Passepartout") && !userIsAdmin)
                        {
                            return null;
                        }

                        //--------------------------------------------
                        // Handle authentication failure
                        //--------------------------------------------

                        IncreaseLoginAttemptCount(loginAttemptDetails);

                        var errorMessage = this.contentSectionService.ContentSection(dbContext,
                            ContentSectionName.Cms_Login_WrongCredentialsWarning,
                            ContentSectionName.Cms_Login_WrongCredentialsWarning_FallbackValue, true);

                        return new ValidationFailure("Password", errorMessage);
                    }

                    return null;
                }
            });
        }

        private static void IncreaseLoginAttemptCount(LoginAttemptDetails loginAttemptDetails)
        {
            const int maxAttempts = 10;
            const int lockOutMinutes = 10;

            loginAttemptDetails = loginAttemptDetails ?? new LoginAttemptDetails();

            loginAttemptDetails.Count++;

            if (loginAttemptDetails.Count >= maxAttempts)
            {
                loginAttemptDetails.LockOutTime = DateTime.Now.AddMinutes(lockOutMinutes);
            }

            MemoryCache.Default.Set(HttpContext.Current.Request.UserHostAddress, loginAttemptDetails, DateTime.Now.AddMinutes(lockOutMinutes + 1));
        }
    }
}