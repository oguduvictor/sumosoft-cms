﻿#pragma warning disable 1591

namespace SumoSoft.Cms.AuthenticationServices.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Extensions;

    internal class RegisterUserValidator : AbstractValidator<User>
    {
        public RegisterUserValidator()
        {
            this.CascadeMode = CascadeMode.StopOnFirstFailure;

            this.RuleFor(x => x.FirstName)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.LastName)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.Email)
                .NotEmpty()
                .Length(1, 100)
                .EmailAddress()
                .MustNotContainScript();

            this.RuleFor(x => x.Password)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();
        }
    }
}