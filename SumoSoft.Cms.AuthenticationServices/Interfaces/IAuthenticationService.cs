﻿#pragma warning disable 1591

namespace SumoSoft.Cms.AuthenticationServices.Interfaces
{
    using System;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Dto;

    public interface IAuthenticationService
    {
        Guid? GetAuthenticatedUserIdFromCookie();

        void PartiallyAuthenticateUser(User user);

        User GetPartiallyAuthenticatedUser(CmsDbContext dbContext, bool asNoTracking = false);

        User GetAuthenticatedUser(CmsDbContext dbContext, bool asNoTracking = false);

        FormResponse RegisterUser(CmsDbContext dbContext, User newUser);

        FormResponse AuthenticateUser(CmsDbContext dbContext, AuthenticateUserDto authenticateUserDto);

        FormResponse AuthenticateUser(CmsDbContext dbContext, string email, string password);

        void UserSignIn(CmsDbContext dbContext, Guid userId);

        void UserSignOut();
    }
}