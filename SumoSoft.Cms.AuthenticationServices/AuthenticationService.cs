﻿namespace SumoSoft.Cms.AuthenticationServices
{
    using System;
    using System.Linq;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Security;
    using FluentValidation;
    using JetBrains.Annotations;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.AuthenticationServices.Validators;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <inheritdoc/>
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUtilityService utilityService;
        private readonly ICountryService countryService;
        private readonly IContentSectionService contentSectionService;
        private readonly IValidator<AuthenticateUserDto> authenticateUserValidator;

        /// <inheritdoc />
        public AuthenticationService(
            IUtilityService utilityService,
            ICountryService countryService,
            IValidator<AuthenticateUserDto> authenticateUserValidator,
            IContentSectionService contentSectionService)
        {
            this.utilityService = utilityService;
            this.countryService = countryService;
            this.authenticateUserValidator = authenticateUserValidator;
            this.contentSectionService = contentSectionService;
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual Guid? GetAuthenticatedUserIdFromCookie()
        {
            if (HttpContext.Current?.User?.Identity?.IsAuthenticated ?? false)
            {
                return new Guid(HttpContext.Current.User.Identity.Name);
            }

            return null;
        }

        /// <inheritdoc />
        public virtual void PartiallyAuthenticateUser(User user)
        {
            if (user == null)
            {
                this.utilityService.RemoveCookie(SharedConstants.PartiallyAuthenticatedUserId);
            }
            else
            {
                this.utilityService.SetCookie(SharedConstants.PartiallyAuthenticatedUserId, user.Id.ToString(), 90);
            }
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual User GetPartiallyAuthenticatedUser(CmsDbContext dbContext, bool asNoTracking = false)
        {
            var cookie = this.utilityService.GetCookie(SharedConstants.PartiallyAuthenticatedUserId);

            if (cookie == null)
            {
                return null;
            }

            Guid userId;

            try
            {
                userId = new Guid(cookie.Value);
            }
            catch (Exception)
            {
                this.utilityService.RemoveCookie(SharedConstants.PartiallyAuthenticatedUserId);

                return null;
            }

            var user = asNoTracking ?
                dbContext.Users.AsNoTracking().FirstOrDefault(x => x.Id == userId) :
                dbContext.Users.Find(userId);

            if (user == null)
            {
                this.utilityService.RemoveCookie(SharedConstants.PartiallyAuthenticatedUserId);
            }

            return user;
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual User GetAuthenticatedUser(CmsDbContext dbContext, bool asNoTracking = false)
        {
            var userId = this.GetAuthenticatedUserIdFromCookie();

            if (userId == null)
            {
                return null;
            }

            return asNoTracking ?
                dbContext.Users.AsNoTracking().FirstOrDefault(x => x.Id == userId) :
                dbContext.Users.Find(userId);
        }

        /// <inheritdoc />
        public virtual FormResponse RegisterUser(CmsDbContext dbContext, User newUser)
        {
            // -------------------------------------------------------
            // Validation
            // -------------------------------------------------------

            var validationResult = new RegisterUserValidator().Validate(newUser);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponse(validationResult);
            }

            var response = new FormResponse();

            // -----------------------------------------------------------------
            // Normalize
            // -----------------------------------------------------------------

            newUser.Email = newUser.Email?.ToPrettyEmailAddress();
            newUser.FirstName = newUser.FirstName?.Trim().ToLower().ToUpperFirstLetter();
            newUser.LastName = newUser.LastName?.Trim().ToLower().ToUpperFirstLetter();

            // -----------------------------------------------------------------
            // Check if a User with the same Email exists and
            // compare it to the current PAU.
            // -----------------------------------------------------------------

            var otherUserSameEmail = dbContext.Users.FirstOrDefault(x => x.Email == newUser.Email);

            if (otherUserSameEmail != null)
            {
                if (otherUserSameEmail.IsRegistered)
                {
                    // -----------------------------------------------------------------
                    // A registered User already exists with this email.
                    // A validation error will be returned.
                    // -----------------------------------------------------------------

                    var errorMessage = this.contentSectionService.ContentSection(dbContext,
                        ContentSectionName.Cms_Register_AlreadyRegisteredWarning,
                        ContentSectionName.Cms_Register_AlreadyRegisteredWarning_FallbackValue,
                        true);

                    response.Errors.Add(new FormError("Email", errorMessage));

                    return response;
                }
                else
                {
                    // -----------------------------------------------------------------------------
                    // An unregistered User already exists with this email.
                    // Set its Email to null.
                    // ------------------------------------------------------------------------------

                    otherUserSameEmail.Email = null;
                }
            }

            // -----------------------------------------------------------------
            // Check password strength
            // -----------------------------------------------------------------

            var requiredPasswordStrength = newUser.Role.AsNotNull().AccessAdmin ?
                this.utilityService.GetAppSetting("AdminPasswordStrength").ToInt() :
                this.utilityService.GetAppSetting("UserPasswordStrength").ToInt();

            if (this.utilityService.CheckPasswordStrength(newUser.Password) < requiredPasswordStrength)
            {
                var errorMessage = this.contentSectionService.ContentSection(dbContext,
                    ContentSectionName.Cms_Register_WeakPasswordWarning,
                    ContentSectionName.Cms_Register_WeakPasswordWarning_FallbackValue, true);

                response.Errors.Add(new FormError("Password", errorMessage));

                return response;
            }

            var user = new User
            {
                Email = newUser.Email,
                Password = this.utilityService.Encrypt(newUser.Password),
                FirstName = newUser.FirstName,
                LastName = newUser.LastName,
                LastLogin = DateTime.Now,
                Role = newUser.Role ?? dbContext.UserRoles.FirstOrDefault(x => x.Name == SharedConstants.DefaultUserRole),
                Country = newUser.Country ?? this.countryService.GetCurrentCountry(dbContext),
                LoginProvider = newUser.LoginProvider,
                ShowMembershipSalePrice = newUser.ShowMembershipSalePrice,
                Gender = newUser.Gender
            };

            dbContext.Users.Add(user);

            dbContext.SaveChanges();

            var authenticationResponse = this.AuthenticateUser(dbContext, newUser.Email, newUser.Password);

            response.Errors.AddRange(authenticationResponse.Errors);

            return response;
        }

        /// <inheritdoc />
        public virtual FormResponse AuthenticateUser(CmsDbContext dbContext, AuthenticateUserDto authenticateUserDto)
        {
            // -------------------------------------------------------
            // Validation
            // -------------------------------------------------------

            var validationResult = this.authenticateUserValidator.Validate(authenticateUserDto);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponse(validationResult);
            }

            // ----------------------------------------------------------
            // Handle the Partially Authenticated User
            // ----------------------------------------------------------

            var authenticatingUser = dbContext.Users.First(x => x.Email == authenticateUserDto.Email);

            var partiallyAuthenticatedUser = this.GetPartiallyAuthenticatedUser(dbContext);

            if (partiallyAuthenticatedUser != null)
            {
                if (!partiallyAuthenticatedUser.IsRegistered)
                {
                    // -------------------------------------------------------------
                    // Keep most recent cart between authenticating user and PAU.
                    // -------------------------------------------------------------

                    var pauCartLatestUpdate = partiallyAuthenticatedUser.Cart?.CartShippingBoxes
                        .SelectMany(x => x.CartItems)
                        .Select(x => x.ModifiedDate)
                        .OrderByDescending(x => x)
                        .FirstOrDefault();

                    var authenticatingUserCartLatestUpdate = authenticatingUser.Cart?.CartShippingBoxes
                        .SelectMany(x => x.CartItems)
                        .Select(x => x.ModifiedDate)
                        .OrderByDescending(x => x)
                        .FirstOrDefault();

                    if (pauCartLatestUpdate > authenticatingUserCartLatestUpdate)
                    {
                        dbContext.Carts.Delete(authenticatingUser.Cart?.Id, true);

                        authenticatingUser.Cart = partiallyAuthenticatedUser.Cart;

                        dbContext.SaveChanges();
                    }

                    // ----------------------------------------------------------
                    // Inherit Membership Sale Price from PAU.
                    // ----------------------------------------------------------

                    if (partiallyAuthenticatedUser.ShowMembershipSalePrice)
                    {
                        authenticatingUser.ShowMembershipSalePrice = true;

                        dbContext.SaveChanges();
                    }

                    // ----------------------------------------------------------
                    // Delete PAU if the email is null or empty.
                    // ----------------------------------------------------------

                    if (string.IsNullOrEmpty(partiallyAuthenticatedUser.Email))
                    {
                        dbContext.Users.Delete(partiallyAuthenticatedUser.Id, true);
                    }
                }

                // ----------------------------------------------------------
                // Sign out PAU.
                // ----------------------------------------------------------

                this.PartiallyAuthenticateUser(null);
            }

            // -------------------------------------------------------
            // Sign in User
            // -------------------------------------------------------

            this.UserSignIn(dbContext, authenticatingUser.Id);

            return this.utilityService.GetFormResponse(validationResult);
        }

        /// <inheritdoc />
        public virtual FormResponse AuthenticateUser(CmsDbContext dbContext, string email, string password)
        {
            return this.AuthenticateUser(dbContext, new AuthenticateUserDto
            {
                Email = email,
                Password = password
            });
        }

        /// <inheritdoc />
        public virtual void UserSignIn(CmsDbContext dbContext, Guid userId)
        {
            try
            {
                FormsAuthentication.SetAuthCookie(userId.ToString(), true);

                var user = dbContext.Users.Find(userId);

                if (user == null)
                {
                    throw new NullReferenceException();
                }

                user.LastLogin = DateTime.Now;

                dbContext.SaveChanges();
            }
            catch
            {
                // Exception ignored for Unit Testing purposes
            }

            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(userId.ToString()), new string[0]);
        }

        /// <inheritdoc />
        public virtual void UserSignOut()
        {
            FormsAuthentication.SignOut();

            HttpContext.Current.User = null;
        }
    }
}
