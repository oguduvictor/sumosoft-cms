namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ShippingBox_RequiresShippingAddress : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceShippingBoxes", "RequiresShippingAddress", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceShippingBoxes", "RequiresShippingAddress");
        }
    }
}
