namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class UserRole_MailingList_Permissions : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.UserRoles", "CreateMailingList", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditMailingList", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditMailingListName", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "EditMailingListAllLocalizedKits", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.UserRoles", "DeleteMailingList", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.UserRoles", "DeleteMailingList");
            this.DropColumn("dbo.UserRoles", "EditMailingListAllLocalizedKits");
            this.DropColumn("dbo.UserRoles", "EditMailingListName");
            this.DropColumn("dbo.UserRoles", "EditMailingList");
            this.DropColumn("dbo.UserRoles", "CreateMailingList");
        }
    }
}
