namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class ContentSection_AddSchema : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.ContentSections", "Schema", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.ContentSections", "Schema");
        }
    }
}
