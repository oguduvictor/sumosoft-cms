namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class SeoSection_AddImage : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.SeoSectionLocalizedKits", "Image", c => c.String());
        }

        public override void Down()
        {
            this.DropColumn("dbo.SeoSectionLocalizedKits", "Image");
        }
    }
}
