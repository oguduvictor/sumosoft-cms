namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Attribute_AddUserAttribute : DbMigration
    {
        public override void Up()
        {
            this.RenameTable(name: "dbo.EcommerceAttributes", newName: "Attributes");
            this.RenameTable(name: "dbo.EcommerceAttributeLocalizedKits", newName: "AttributeLocalizedKits");
            this.RenameTable(name: "dbo.EcommerceAttributeOptions", newName: "AttributeOptions");
            this.RenameTable(name: "dbo.EcommerceAttributeOptionLocalizedKits", newName: "AttributeOptionLocalizedKits");
            this.RenameColumn(table: "dbo.AttributeOptionLocalizedKits", name: "EcommerceAttributeOption_Id", newName: "AttributeOption_Id");
            this.RenameIndex(table: "dbo.AttributeOptionLocalizedKits", name: "IX_EcommerceAttributeOption_Id", newName: "IX_AttributeOption_Id");
            this.CreateTable(
                "dbo.UserAttributes",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    StringValue = c.String(),
                    BooleanValue = c.Boolean(nullable: false),
                    DateTimeValue = c.DateTime(),
                    ImageValue = c.String(),
                    DoubleValue = c.Double(nullable: false),
                    JsonValue = c.String(),
                    IsDisabled = c.Boolean(nullable: false),
                    CreatedDate = c.DateTime(nullable: false),
                    DeletedDate = c.DateTime(),
                    ModifiedDate = c.DateTime(nullable: false),
                    Attribute_Id = c.Guid(),
                    AttributeOptionValue_Id = c.Guid(),
                    ContentSectionValue_Id = c.Guid(),
                    User_Id = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attributes", t => t.Attribute_Id)
                .ForeignKey("dbo.AttributeOptions", t => t.AttributeOptionValue_Id)
                .ForeignKey("dbo.ContentSections", t => t.ContentSectionValue_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Attribute_Id)
                .Index(t => t.AttributeOptionValue_Id)
                .Index(t => t.ContentSectionValue_Id)
                .Index(t => t.User_Id);

            this.AddColumn("dbo.EcommerceProductAttributes", "JsonValue", c => c.String());
        }

        public override void Down()
        {
            this.DropForeignKey("dbo.UserAttributes", "User_Id", "dbo.Users");
            this.DropForeignKey("dbo.UserAttributes", "ContentSectionValue_Id", "dbo.ContentSections");
            this.DropForeignKey("dbo.UserAttributes", "AttributeOptionValue_Id", "dbo.AttributeOptions");
            this.DropForeignKey("dbo.UserAttributes", "Attribute_Id", "dbo.Attributes");
            this.DropIndex("dbo.UserAttributes", new[] { "User_Id" });
            this.DropIndex("dbo.UserAttributes", new[] { "ContentSectionValue_Id" });
            this.DropIndex("dbo.UserAttributes", new[] { "AttributeOptionValue_Id" });
            this.DropIndex("dbo.UserAttributes", new[] { "Attribute_Id" });
            this.DropColumn("dbo.EcommerceProductAttributes", "JsonValue");
            this.DropTable("dbo.UserAttributes");
            this.RenameIndex(table: "dbo.AttributeOptionLocalizedKits", name: "IX_AttributeOption_Id", newName: "IX_EcommerceAttributeOption_Id");
            this.RenameColumn(table: "dbo.AttributeOptionLocalizedKits", name: "AttributeOption_Id", newName: "EcommerceAttributeOption_Id");
            this.RenameTable(name: "dbo.AttributeOptionLocalizedKits", newName: "EcommerceAttributeOptionLocalizedKits");
            this.RenameTable(name: "dbo.AttributeOptions", newName: "EcommerceAttributeOptions");
            this.RenameTable(name: "dbo.AttributeLocalizedKits", newName: "EcommerceAttributeLocalizedKits");
            this.RenameTable(name: "dbo.Attributes", newName: "EcommerceAttributes");
        }
    }
}
