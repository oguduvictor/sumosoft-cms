namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class EcommerceCoupon_AllowOnSale : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.EcommerceCoupons", "AllowOnSale", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.EcommerceCoupons", "AllowOnSale");
        }
    }
}
