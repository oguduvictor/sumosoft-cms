namespace SumoSoft.Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class UserRole_AddEditVariantSelector : DbMigration
    {
        public override void Up()
        {
            this.AddColumn("dbo.UserRoles", "EditVariantSelector", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            this.DropColumn("dbo.UserRoles", "EditVariantSelector");
        }
    }
}
