﻿namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class ProductDbSetExtension
    {
        public static void Delete(this DbSet<Product> products, Guid? productId, bool saveChanges)
        {
            var dbContext = products.GetContext();

            if (productId == null)
            {
                return;
            }

            var dbProduct = dbContext.Products.Find(productId);

            if (dbProduct == null)
            {
                return;
            }

            dbProduct.Categories.Clear();
            dbProduct.ShippingBoxes.Clear();
            dbProduct.RelatedProducts.Clear();

            var relatedProducts = dbContext.Products.Where(x => x.RelatedProducts.Any(rp => rp.Id == productId)).ToList();

            foreach (var product in relatedProducts)
            {
                product.RelatedProducts.Remove(product);
            }

            var cartItems = dbContext.CartItems.Where(x => x.Product.Id == productId).ToList();

            foreach (var cartItem in cartItems)
            {
                dbContext.CartItems.Delete(cartItem.Id, false);
            }

            var productLocalizedKits = dbProduct.ProductLocalizedKits.ToList();

            foreach (var productLocalizedKit in productLocalizedKits)
            {
                dbContext.ProductLocalizedKits.Delete(productLocalizedKit.Id, false);
            }

            var productAttributes = dbProduct.ProductAttributes.ToList();

            foreach (var productAttribute in productAttributes)
            {
                dbContext.ProductAttributes.Delete(productAttribute.Id, false);
            }

            var productVariants = dbProduct.ProductVariants.ToList();

            foreach (var productVariant in productVariants)
            {
                dbContext.ProductVariants.Delete(productVariant.Id, false);
            }

            var productStockUnits = dbProduct.ProductStockUnits.ToList();

            foreach (var productStockUnit in productStockUnits)
            {
                dbContext.ProductStockUnits.Delete(productStockUnit.Id, false);
            }

            var productImageKits = dbProduct.ProductImageKits.ToList();

            foreach (var productImageKit in productImageKits)
            {
                dbContext.ProductImageKits.Delete(productImageKit.Id, false);
            }

            dbContext.Products.Remove(dbProduct);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}