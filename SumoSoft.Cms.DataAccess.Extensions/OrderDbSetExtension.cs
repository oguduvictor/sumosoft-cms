namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class OrderDbSetExtension
    {
        public static void Delete(this DbSet<Order> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var orderShippingAddress = dbEntity.ShippingAddress;
            if (orderShippingAddress != null)
            {
                dbContext.OrderAddresses.Remove(orderShippingAddress);
            }

            var orderBillingAddress = dbEntity.BillingAddress;
            if (orderBillingAddress != null)
            {
                dbContext.OrderAddresses.Remove(orderBillingAddress);
            }

            var orderCoupon = dbEntity.OrderCoupon;
            if (orderCoupon != null)
            {
                dbContext.OrderCoupons.Remove(orderCoupon);
            }

            var orderCredit = dbEntity.OrderCredit;
            if (orderCredit != null)
            {
                dbContext.OrderCredits.Remove(orderCredit);
            }

            var orderShippingBoxes = dbContext.OrderShippingBoxes.Where(x => x.Order.Id == entityId).ToList();
            foreach (var orderShippingBox in orderShippingBoxes)
            {
                dbContext.OrderShippingBoxes.Delete(orderShippingBox.Id, false);
            }

            var sentEmails = dbEntity.SentEmails;
            dbContext.SentEmails.RemoveRange(sentEmails);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}