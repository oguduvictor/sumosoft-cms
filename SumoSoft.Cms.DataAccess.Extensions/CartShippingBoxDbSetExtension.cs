namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class CartShippingBoxDbSetExtension
    {
        public static void Delete(this DbSet<CartShippingBox> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var cartItems = dbEntity.CartItems.ToList();

            foreach (var cartItem in cartItems)
            {
                dbContext.CartItems.Delete(cartItem.Id, false);
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}