namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class AddressDbSetExtension
    {
        public static void Delete(this DbSet<Address> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var cartsWithShippingAddress = dbContext.Carts
                .Where(x => x.ShippingAddress != null && x.ShippingAddress.Id == entityId).ToList();
            foreach (var cart in cartsWithShippingAddress)
            {
                cart.ShippingAddress = null;
            }

            var cartsWithBillingAddress = dbContext.Carts
                .Where(x => x.BillingAddress != null && x.BillingAddress.Id == entityId).ToList();
            foreach (var cart in cartsWithBillingAddress)
            {
                cart.BillingAddress = null;
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}