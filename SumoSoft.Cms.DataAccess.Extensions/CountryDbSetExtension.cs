namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class CountryDbSetExtension
    {
        public static void Delete(this DbSet<Country> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var cartsShippingAddress = dbContext.Carts.Where(x => x.ShippingAddress.Country.Id == entityId).ToList();

            foreach (var cart in cartsShippingAddress)
            {
                cart.ShippingAddress = null;
            }

            var addresses = dbContext.Addresses.Where(x => x.Country.Id == entityId).ToList();
            dbContext.Addresses.RemoveRange(addresses);

            var users = dbContext.Users.Where(x => x.Country.Id == entityId).ToList();
            foreach (var user in users)
            {
                user.Country = null;
            }

            var userCreditsLocalizedKit = dbContext.UserCredits.Where(x => x.UserLocalizedKit.Country.Id == entityId).ToList();
            dbContext.UserCredits.RemoveRange(userCreditsLocalizedKit);

            var userLocalizedKits = dbContext.UserLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.UserLocalizedKits.RemoveRange(userLocalizedKits);

            var taxes = dbContext.Taxes.Where(x => x.Country.Id == entityId).ToList();
            dbContext.Taxes.RemoveRange(taxes);

            var shippingBoxLocalizedKits = dbContext.ShippingBoxLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.ShippingBoxLocalizedKits.RemoveRange(shippingBoxLocalizedKits);

            var attributeLocalizedKits = dbContext.AttributeLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.AttributeLocalizedKits.RemoveRange(attributeLocalizedKits);

            var attributeOptionLocalizedKits = dbContext.AttributeOptionLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.AttributeOptionLocalizedKits.RemoveRange(attributeOptionLocalizedKits);

            var variantLocalizedKits = dbContext.VariantLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.VariantLocalizedKits.RemoveRange(variantLocalizedKits);

            var variantOptionLocalizedKits = dbContext.VariantOptionLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.VariantOptionLocalizedKits.RemoveRange(variantOptionLocalizedKits);

            var productLocalizedKits = dbContext.ProductLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.ProductLocalizedKits.RemoveRange(productLocalizedKits);

            var sentEmails = dbContext.SentEmails.Where(x => x.Coupon != null && x.Coupon.Country.Id == entityId).ToList();
            dbContext.SentEmails.RemoveRange(sentEmails);

            var cartsCoupon = dbContext.Carts.Where(x => x.Coupon != null && x.Coupon.Country.Id == entityId).ToList();
            foreach (var cart in cartsCoupon)
            {
                cart.Coupon = null;
            }

            var coupons = dbContext.Coupons.Where(x => x.Country.Id == entityId).ToList();
            dbContext.Coupons.RemoveRange(coupons);

            var categoryLocalizedKits = dbContext.CategoryLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.CategoryLocalizedKits.RemoveRange(categoryLocalizedKits);

            var contentSectionLocalizedKits = dbContext.ContentSectionLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.ContentSectionLocalizedKits.RemoveRange(contentSectionLocalizedKits);

            var seoSectionLocalizedKits = dbContext.SeoSectionLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.SeoSectionLocalizedKits.RemoveRange(seoSectionLocalizedKits);

            var emailLocalizedKits = dbContext.EmailLocalizedKits.Where(x => x.Country.Id == entityId).ToList();
            dbContext.EmailLocalizedKits.RemoveRange(emailLocalizedKits);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}