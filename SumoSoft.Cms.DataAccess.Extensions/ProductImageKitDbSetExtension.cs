namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class ProductImageKitDbSetExtension
    {
        public static void Delete(this DbSet<ProductImageKit> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbEntity.VariantOptions.Clear();

            var categoriesDbSet = dbContext.Categories.ToList();

            foreach (var category in categoriesDbSet)
            {
                category.ProductImageKits.Remove(dbEntity);
            }

            dbContext.ProductImages.RemoveRange(dbEntity.ProductImages);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}