namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public static class AttributeDbSetExtension
    {
        public static void Delete(this DbSet<Domain.Attribute> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var productAttributes = dbContext.ProductAttributes.Where(x => x.Attribute.Id == entityId).ToList();

            foreach (var productAttribute in productAttributes)
            {
                dbContext.ProductAttributes.Delete(productAttribute.Id, false);
            }

            var userAttributes = dbContext.UserAttributes.Where(x => x.Attribute.Id == entityId).ToList();

            foreach (var userAttribute in userAttributes)
            {
                dbContext.UserAttributes.Delete(userAttribute.Id, false);
            }

            dbContext.AttributeLocalizedKits.RemoveRange(dbEntity.AttributeLocalizedKits);

            var attributeOptions = dbEntity.AttributeOptions.ToList();

            foreach (var attributeOption in attributeOptions)
            {
                dbContext.AttributeOptions.Delete(attributeOption.Id, false);
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}