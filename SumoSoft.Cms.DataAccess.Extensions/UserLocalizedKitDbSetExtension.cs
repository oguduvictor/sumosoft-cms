namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class UserLocalizedKitDbSetExtension
    {
        public static void Delete(this DbSet<UserLocalizedKit> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var userCredits = dbContext.UserCredits.Where(x => x.UserLocalizedKit.Id == entityId).ToList();

            foreach (var userCredit in userCredits)
            {
                dbContext.UserCredits.Delete(userCredit.Id, false);
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}