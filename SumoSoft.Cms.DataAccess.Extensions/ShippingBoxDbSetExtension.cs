namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class ShippingBoxDbSetExtension
    {
        public static void Delete(this DbSet<ShippingBox> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbEntity.Countries.Clear();

            dbEntity.Products.Clear();

            dbContext.ShippingBoxLocalizedKits.RemoveRange(dbEntity.ShippingBoxLocalizedKits);

            var cartShippingBoxes = dbContext.CartShippingBoxes.Where(x => x.ShippingBox.Id == entityId).ToList();

            foreach (var cartShippingBox in cartShippingBoxes)
            {
                dbContext.CartShippingBoxes.Delete(cartShippingBox.Id, false);
            }

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}