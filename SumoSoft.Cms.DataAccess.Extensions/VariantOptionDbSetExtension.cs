namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class VariantOptionDbSetExtension
    {
        public static void Delete(this DbSet<VariantOption> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var cartItemVariants = dbContext.CartItemVariants.Where(x => x.VariantOptionValue.Id == entityId).ToList();
            foreach (var x in cartItemVariants)
            {
                dbContext.CartItemVariants.Delete(x.CartItem?.Id, false);
            }

            var productVariants = dbContext.ProductVariants.Where(x => x.VariantOptions.Any(y => y.Id == entityId));

            foreach (var productVariant in productVariants)
            {
                productVariant.VariantOptions.Remove(dbEntity);
            }

            var productVariantsWhereDefaultValue = dbContext.ProductVariants.Where(x => x.DefaultVariantOptionValue.Id == entityId);

            foreach (var productVariant in productVariantsWhereDefaultValue)
            {
                productVariant.DefaultVariantOptionValue = null;
            }

            var variantsWhereDefaultValue = dbContext.Variants.Where(x => x.DefaultVariantOptionValue.Id == entityId);

            foreach (var variant in variantsWhereDefaultValue)
            {
                variant.DefaultVariantOptionValue = null;
            }

            var productImageKits = dbContext.ProductImageKits.Where(x => x.VariantOptions.Any(y => y.Id == entityId));

            foreach (var productImageKit in productImageKits)
            {
                dbContext.ProductImageKits.Delete(productImageKit.Id, false);
            }

            var productStockUnits = dbContext.ProductStockUnits.Where(x => x.VariantOptions.Any(y => y.Id == entityId));

            foreach (var stockUnit in productStockUnits)
            {
                dbContext.ProductStockUnits.Delete(stockUnit.Id, false);
            }

            var variantOptionLocalizedKits = dbContext.VariantOptionLocalizedKits.Where(x => x.VariantOption.Id == entityId);

            dbContext.VariantOptionLocalizedKits.RemoveRange(variantOptionLocalizedKits);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}