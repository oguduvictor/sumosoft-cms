﻿namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class VariantDbSetExtension
    {
        public static void Delete(this DbSet<Variant> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var variant = dbContext.Variants.Find(entityId);

            if (variant == null)
            {
                return;
            }

            var cartItemVariants = dbContext.CartItemVariants.Where(x => x.Variant.Id == entityId).ToList();

            foreach (var cartItemVariant in cartItemVariants)
            {
                dbContext.CartItemVariants.Delete(cartItemVariant.Id, false);
            }

            var productVariants = dbContext.ProductVariants.Where(x => x.Variant.Id == entityId).ToList();

            foreach (var productVariant in productVariants)
            {
                dbContext.ProductVariants.Delete(productVariant.Id, false);
            }

            var userVariants = dbContext.UserVariants.Where(x => x.Variant.Id == entityId).ToList();

            foreach (var userVariant in userVariants)
            {
                dbContext.UserVariants.Delete(userVariant.Id, false);
            }

            var variantLocalizedKits = variant.VariantLocalizedKits.ToList();

            foreach (var variantLocalizedKit in variantLocalizedKits)
            {
                dbContext.VariantLocalizedKits.Delete(variantLocalizedKit.Id, false);
            }

            var variantOptions = variant.VariantOptions.ToList();

            foreach (var variantOption in variantOptions)
            {
                dbContext.VariantOptions.Delete(variantOption.Id, false);
            }

            dbContext.Variants.Remove(variant);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}