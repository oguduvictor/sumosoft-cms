namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class EmailDbSetExtension
    {
        public static void Delete(this DbSet<Email> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            dbContext.EmailLocalizedKits.RemoveRange(dbEntity.EmailLocalizedKits);

            var sentEmails = dbContext.SentEmails.Where(x => x.Email.Id == entityId).ToList();

            dbContext.SentEmails.RemoveRange(sentEmails);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}