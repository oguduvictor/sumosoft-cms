﻿namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using SumoSoft.Cms.Domain;

    public static class CategoryLocalizedKitDbSetExtension
    {
        public static void Delete(this DbSet<CategoryLocalizedKit> categoryLocalizedKits, Guid? categoryLocalizedKitId, bool saveChanges)
        {
            var dbContext = categoryLocalizedKits.GetContext();

            if (categoryLocalizedKitId == null)
            {
                return;
            }

            var categoryLocalizedKit = dbContext.CategoryLocalizedKits.Find(categoryLocalizedKitId);

            if (categoryLocalizedKit == null)
            {
                return;
            }

            dbContext.CategoryLocalizedKits.Remove(categoryLocalizedKit);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}