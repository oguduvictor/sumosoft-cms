namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class CartDbSetExtension
    {
        public static void Delete(this DbSet<Cart> dbSet, Guid? entityId, bool saveChanges)
        {
            var dbContext = dbSet.GetContext();

            if (entityId == null)
            {
                return;
            }

            var dbEntity = dbSet.Find(entityId);

            if (dbEntity == null)
            {
                return;
            }

            var user = dbEntity.User;
            if (user != null)
            {
                user.Cart = null;
            }

            var cartShippingBoxes = dbEntity.CartShippingBoxes.ToList();

            foreach (var cartShippingBox in cartShippingBoxes)
            {
                dbContext.CartShippingBoxes.Delete(cartShippingBox.Id, false);
            }

            var sentEmails = dbEntity.SentEmails;
            dbContext.SentEmails.RemoveRange(sentEmails);

            dbSet.Remove(dbEntity);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}