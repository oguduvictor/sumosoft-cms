﻿#pragma warning disable 1591

namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Reflection;
    using SumoSoft.Cms.Extensions;

    public enum PreviousValueMatch
    {
        Exact = 0,
        Contains = 1,
        StartsWith = 2,
        EndsWith = 3
    }

    /// <summary>
    ///
    /// </summary>
    public static class DbSetExtension
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbSet"></param>
        /// <returns></returns>
        public static CmsDbContext GetContext<TEntity>(this DbSet<TEntity> dbSet) where TEntity : class
        {
            var internalSet = dbSet.GetType().GetField("_internalSet", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(dbSet);

            var internalContext = internalSet?.GetType().BaseType?.GetField("_internalContext", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(internalSet);

            return (CmsDbContext)internalContext?.GetType().GetProperty("Owner", BindingFlags.Instance | BindingFlags.Public)?.GetValue(internalContext, null);
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbSet"></param>
        /// <param name="propertyName"></param>
        /// <param name="previousValue"></param>
        /// <param name="newValue"></param>
        /// <param name="match"></param>
        public static void BulkUpdate<TEntity>(this DbSet<TEntity> dbSet, string propertyName, string previousValue, string newValue, PreviousValueMatch match = PreviousValueMatch.Exact) where TEntity : class
        {
            if (match == PreviousValueMatch.Exact && previousValue == newValue)
            {
                return;
            }

            var tableName = dbSet.ToString()
                .SubstringFromFirst("[dbo].[")
                .SubstringUpToFirst("]");

            var sqlCommand = $"UPDATE [{tableName}]\n";

            if (newValue == null)
            {
                sqlCommand = sqlCommand + $"SET [{propertyName}] = NULL\n";
            }
            else
            {
                sqlCommand = sqlCommand + $"SET [{propertyName}] = '{newValue}'\n";
            }

            if (previousValue == null)
            {
                if (match == PreviousValueMatch.Exact)
                {
                    sqlCommand = sqlCommand + $"WHERE [{propertyName}] IS NULL";
                }
                else
                {
                    throw new Exception("When 'previousValue' is null, the match type can only be PreviousValueMatch.Exact");
                }
            }
            else
            {
                switch (match)
                {
                    case PreviousValueMatch.Exact:
                        sqlCommand = sqlCommand + $"WHERE [{propertyName}] = '{previousValue}'";
                        break;
                    case PreviousValueMatch.Contains:
                        sqlCommand = sqlCommand + $"WHERE [{propertyName}] LIKE '%{previousValue}%'";
                        break;
                    case PreviousValueMatch.StartsWith:
                        sqlCommand = sqlCommand + $"WHERE [{propertyName}] LIKE '{previousValue}%'";
                        break;
                    case PreviousValueMatch.EndsWith:
                        sqlCommand = sqlCommand + $"WHERE [{propertyName}] LIKE '%{previousValue}'";
                        break;
                }
            }

            dbSet.GetContext().Database.ExecuteSqlCommand(sqlCommand);
        }
    }
}
