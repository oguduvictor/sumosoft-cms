﻿namespace SumoSoft.Cms.DataAccess.Extensions
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using SumoSoft.Cms.Domain;

    public static class CategoryDbSetExtension
    {
        public static void Delete(this DbSet<Category> categories, Guid? categoryId, bool saveChanges)
        {
            var dbContext = categories.GetContext();

            if (categoryId == null)
            {
                return;
            }

            var dbCategory = categories.Find(categoryId);

            if (dbCategory == null)
            {
                return;
            }

            dbCategory.Children.Clear();
            dbCategory.Products.Clear();
            dbCategory.ProductImageKits.Clear();
            dbCategory.ProductStockUnits.Clear();

            var categoryLocalizedKits = dbCategory.CategoryLocalizedKits.ToList();

            foreach (var categoryLocalizedKit in categoryLocalizedKits)
            {
                dbContext.CategoryLocalizedKits.Delete(categoryLocalizedKit.Id, false);
            }

            var coupons = dbContext.Coupons.ToList();

            foreach (var coupon in coupons)
            {
                coupon.Categories.Remove(dbCategory);
            }

            dbContext.Categories.Remove(dbCategory);

            if (saveChanges)
            {
                dbContext.SaveChanges();
            }
        }
    }
}