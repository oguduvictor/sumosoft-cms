﻿namespace SumoSoft.Cms.ContentSectionServices.Interfaces
{
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;

    /// <summary>
    ///
    /// </summary>
    public interface IContentSectionService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="productAttribute"></param>
        /// <returns></returns>
        string ContentSection(CmsDbContext dbContext, ProductAttribute productAttribute);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="contentSectionName"></param>
        /// <returns></returns>
        string ContentSection(CmsDbContext dbContext, string contentSectionName);

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbContext"></param>
        /// <param name="contentSectionName"></param>
        /// <returns></returns>
        T ContentSectionAs<T>(CmsDbContext dbContext, string contentSectionName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="contentSectionName"></param>
        /// <param name="fallbackValue"></param>
        /// <param name="createifMissing"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T ContentSectionAs<T>(CmsDbContext dbContext, string contentSectionName, string fallbackValue, bool createifMissing);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="contentSectionName"></param>
        /// <param name="fallbackValue"></param>
        /// <returns></returns>
        string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="contentSectionName"></param>
        /// <param name="fallbackValue"></param>
        /// <param name="createifMissing"></param>
        /// <returns></returns>
        string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue, bool createifMissing);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="contentSectionName"></param>
        /// <param name="fallbackValue"></param>
        /// <param name="createifMissing"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue, bool createifMissing, string languageCode);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="contentSectionName"></param>
        /// <param name="fallbackValue"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue, string languageCode);
    }
}