﻿#pragma warning disable 4014

namespace SumoSoft.Cms.ContentSectionServices
{
    using System;
    using System.Linq;
    using System.Web;
    using JetBrains.Annotations;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.LogServices.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <inheritdoc />
    public class ContentSectionService : IContentSectionService
    {
        private readonly ILogService logService;
        private readonly ICountryService countryService;
        private readonly IUtilityService utilityService;

        /// <inheritdoc />
        public ContentSectionService(
            ILogService logService,
            ICountryService countryService,
            IUtilityService utilityService)
        {
            this.logService = logService;
            this.countryService = countryService;
            this.utilityService = utilityService;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue, bool createifMissing, string languageCode)
        {
            languageCode = string.IsNullOrEmpty(languageCode) ? this.countryService.GetCurrentLanguageCode() : languageCode;

            var content = dbContext.ContentSectionLocalizedKits
                .Where(x => !x.IsDisabled && x.ContentSection.Name == contentSectionName && x.Country.LanguageCode == languageCode)
                .Select(x => x.Content)
                .FirstOrDefault();

            if (string.IsNullOrEmpty(content))
            {
                content = dbContext.ContentSectionLocalizedKits
                    .Where(x => !x.IsDisabled && x.ContentSection.Name == contentSectionName && x.Country.IsDefault)
                    .Select(x => x.Content)
                    .FirstOrDefault();
            }

            if (!string.IsNullOrEmpty(content))
            {
                return content;
            }

            var contentSectionExists = dbContext.ContentSections.Any(x => x.Name == contentSectionName);

            if (!contentSectionExists)
            {
                if (createifMissing)
                {
                    var contentSection = new ContentSection();
                    var contentSectionLocalizedKit = new ContentSectionLocalizedKit();

                    contentSection.Name = contentSectionName;
                    contentSection.Schema = fallbackValue.StartsWith("{") && fallbackValue.EndsWith("}") ? contentSectionName : null;
                    contentSection.ContentSectionLocalizedKits.Add(contentSectionLocalizedKit);

                    contentSectionLocalizedKit.ContentSection = contentSection;
                    contentSectionLocalizedKit.Content = fallbackValue;
                    contentSectionLocalizedKit.Country = dbContext.Countries.FirstOrDefault(x => x.IsDefault);

                    dbContext.ContentSections.Add(contentSection);

                    dbContext.SaveChanges();
                }
                else
                {
                    var logMessage = $"The Content Section '{contentSectionName}' was not found in {HttpContext.Current.Request.Url.AbsoluteUri}";

                    var existingLog = dbContext.Logs.Any(x => x.Message == logMessage);

                    if (!existingLog)
                    {
                        this.logService.LogAsync(LogTypesEnum.Content, logMessage);
                    }
                }
            }

            return fallbackValue ?? string.Empty;
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue, bool createifMissing)
        {
            return this.ContentSection(dbContext, contentSectionName, fallbackValue, createifMissing, null);
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue, string languageCode)
        {
            return this.ContentSection(dbContext, contentSectionName, fallbackValue, false, languageCode);
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string ContentSection(CmsDbContext dbContext, string contentSectionName, string fallbackValue)
        {
            return this.ContentSection(dbContext, contentSectionName, fallbackValue, false, null);
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string ContentSection(CmsDbContext dbContext, string contentSectionName)
        {
            return this.ContentSection(dbContext, contentSectionName, "", false, this.countryService.GetCurrentLanguageCode());
        }

        /// <inheritdoc />
        [NotNull]
        public virtual string ContentSection(CmsDbContext dbContext, ProductAttribute productAttribute)
        {
            return
                productAttribute == null
                    ? string.Empty
                    : this.ContentSection(dbContext, productAttribute.ContentSectionValue?.Name, null);
        }

        /// <inheritdoc />
        public virtual T ContentSectionAs<T>(CmsDbContext dbContext, string contentSectionName)
        {
            var contentSectionJson = this.ContentSection(dbContext, contentSectionName);

            if (string.IsNullOrEmpty(contentSectionJson))
            {
                return default;
            }

            try
            {
                return this.utilityService.FromJson<T>(contentSectionJson);
            }
            catch (Exception)
            {
                return default;
            }
        }

        /// <inheritdoc />
        public T ContentSectionAs<T>(CmsDbContext dbContext, string contentSectionName, string fallbackValue, bool createifMissing)
        {
            var contentSectionJson = this.ContentSection(dbContext, contentSectionName, fallbackValue, createifMissing);

            if (string.IsNullOrEmpty(contentSectionJson))
            {
                return default;
            }

            try
            {
                return this.utilityService.FromJson<T>(contentSectionJson);
            }
            catch (Exception)
            {
                return default;
            }
        }
    }
}
