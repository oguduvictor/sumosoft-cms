﻿namespace SumoSoft.Cms.AutoTagServices.Interfaces
{
    using SumoSoft.Cms.Domain;

    public interface IAutoTagService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        string GetAutoTags(Order order);

        /// <summary>
        ///
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        string GetAutoTags(Product product);
    }
}
