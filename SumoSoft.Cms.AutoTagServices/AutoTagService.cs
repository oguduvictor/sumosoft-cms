﻿namespace SumoSoft.Cms.AutoTagServices
{
    using System.Collections.Generic;
    using System.Linq;
    using SumoSoft.Cms.AutoTagServices.Interfaces;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Extensions;

    public class AutoTagService : IAutoTagService
    {
        public virtual string GetAutoTags(Order order)
        {
            var tags = new List<string>();

            foreach (var orderItem in order.OrderShippingBoxes.SelectMany(x => x.OrderItems))
            {
                tags.AddIfNotNullOrEmpty(orderItem.ProductName);
                tags.AddIfNotNullOrEmpty(orderItem.ProductTitle);
            }

            return string.Join(",", tags).ToLower();
        }

        public virtual string GetAutoTags(Product product)
        {
            var tags = new List<string>();

            foreach (var productLocalizedKit in product.ProductLocalizedKits)
            {
                tags.AddIfNotNullOrEmpty(productLocalizedKit.Title);
            }

            foreach (var category in product.Categories)
            {
                tags.AddRange(category.GetTagList());

                foreach (var categoryLocalizedKit in category.CategoryLocalizedKits)
                {
                    tags.AddIfNotNullOrEmpty(categoryLocalizedKit.Title);
                }
            }

            return string.Join(",", tags).ToLower();
        }
    }
}
