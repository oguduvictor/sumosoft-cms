﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Enums
{
    using System.ComponentModel;

    public enum OrderStatusEnum
    {
        [Description("Placed")]
        Placed = 0,
        [Description("Confirmed")]
        Confirmed = 50,
        [Description("Processing")]
        Processing = 100,
        [Description("Completed")]
        Completed = 200,
        [Description("Cancelled")]
        Cancelled = 300
    }
}