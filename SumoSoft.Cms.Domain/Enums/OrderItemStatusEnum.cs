﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Enums
{
    using System.ComponentModel;

    public enum OrderItemStatusEnum
    {
        [Description("Default")]
        Default = 0,
        [Description("Return requested")]
        ReturnRequested = 100,
        [Description("Return completed")]
        ReturnCompleted = 200,
        [Description("Return denied")]
        ReturnDenied = 300
    }
}