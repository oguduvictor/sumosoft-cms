﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Enums
{
    using System.ComponentModel;

    public enum VariantTypesEnum
    {
        [Description("Boolean")]
        Boolean = 0,
        [Description("Options")]
        Options = 1,
        [Description("Double")]
        Double = 2,
        [Description("Integer")]
        Integer = 3,
        [Description("String")]
        String = 4,
        [Description("Json")]
        Json = 5
    }
}