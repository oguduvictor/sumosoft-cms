﻿namespace SumoSoft.Cms.Domain.Enums
{
    using System.ComponentModel;

    /// <summary>
    ///
    /// </summary>
    public enum AttributeTypesEnum
    {
        /// <summary>
        /// Use a String Attribute for internal codes, links or any other string connected to the parent entity.
        /// </summary>
        [Description("String")]
        String = 0,

        /// <summary>
        /// Use an Options Attribute when multiple options are available.
        /// </summary>
        [Description("Options")]
        Options = 2,

        /// <summary>
        /// Use Boolean attribute for express true / false values, for example
        /// to define if a certain product is available for pre-order.
        /// </summary>
        [Description("Boolean")]
        Boolean = 3,

        /// <summary>
        /// Use the DateTime Attribute to associate a date and time to the parent entity.
        /// For example, the expiration date.
        /// </summary>
        [Description("DateTime")]
        DateTime = 4,

        /// <summary>
        /// The Image Attribute associates an image Url to the parent entity
        /// </summary>
        [Description("Image")]
        Image = 5,

        /// <summary>
        /// Use the Double Attribute to define a numeric value for the parent entity.
        /// For example, its weight or measurements.
        /// </summary>
        [Description("Double")]
        Double = 6,

        /// <summary>
        /// Use a ContentSection Attribute to associate localized pieces of content to the parent entity.
        /// </summary>
        [Description("ContentSection")]
        ContentSection = 7,

        /// <summary>
        /// Use a Json Attribute to associate structured data to the parent entity.
        /// </summary>
        [Description("Json")]
        Json = 8
    }
}