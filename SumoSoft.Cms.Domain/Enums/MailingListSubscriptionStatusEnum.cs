﻿namespace SumoSoft.Cms.Domain.Enums
{
#pragma warning disable 1591

    using System.ComponentModel;

    public enum MailingListSubscriptionStatusEnum
    {
        [Description("Subscribed")]
        Subscribed = 0,

        [Description("Unsubscribed")]
        Unsubscribed = 5,

        [Description("Invalid")]
        Invalid = 10
    }
}
