﻿namespace SumoSoft.Cms.Domain.Enums
{
    /// <summary>
    ///
    /// </summary>
    public enum LogTypesEnum
    {
        /// <summary>
        /// General notifications, visible to any administrator
        /// </summary>
        General = 0,

        /// <summary>
        /// Code exceptions, visible to developers
        /// </summary>
        Exception = 10,

        /// <summary>
        /// Ecommerce notifications
        /// </summary>
        Ecommerce = 20,

        /// <summary>
        /// Content management notifications
        /// </summary>
        Content = 30
    }
}