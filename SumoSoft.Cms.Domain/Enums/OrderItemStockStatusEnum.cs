﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Enums
{
    using System.ComponentModel;

    public enum OrderItemStockStatusEnum
    {
        [Description("Decreased")]
        Decreased = 100,
        [Description("Restored")]
        Restored = 200,
    }
}