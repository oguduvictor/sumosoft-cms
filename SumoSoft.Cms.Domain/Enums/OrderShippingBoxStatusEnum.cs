﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Enums
{
    using System.ComponentModel;

    public enum OrderShippingBoxStatusEnum
    {
        [Description("Preparing for dispatch")]
        Preparing = 0,
        [Description("Dispatched")]
        Dispatched = 100,
        [Description("Returned")]
        Returned = 200
    }
}