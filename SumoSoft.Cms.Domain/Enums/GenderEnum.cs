﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Enums
{
    using System.ComponentModel;

    public enum GenderEnum
    {
        [Description("None")]
        None = 0,
        [Description("Male")]
        Male = 10,
        [Description("Female")]
        Female = 20
    }
}
