﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class MailingListSubscription
    {
        [NotNull]
        public virtual MailingList MailingList { get; set; }

        public Guid MailingListId { get; set; }

        [NotNull]
        public virtual User User { get; set; }

        public Guid UserId { get; set; }

        public MailingListSubscriptionStatusEnum Status { get; set; }
    }
}
