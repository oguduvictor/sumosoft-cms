﻿#pragma warning disable 1591
namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    public class SentEmail : BaseEntity
    {
        /// <summary>
        /// Sent Email.
        /// </summary>
        [CanBeNull]
        public virtual Email Email { get; set; }

        /// <summary>
        /// The User associated to the sent Email.
        /// </summary>
        [CanBeNull]
        public virtual User User { get; set; }

        /// <summary>
        /// The Order associated to the sent Email.
        /// </summary>
        [CanBeNull]
        public virtual Order Order { get; set; }

        /// <summary>
        /// The Coupon associated to the sent Email.
        /// </summary>
        [CanBeNull]
        public virtual Coupon Coupon { get; set; }

        /// <summary>
        /// The Cart associated to the sent Email
        /// </summary>
        [CanBeNull]
        public virtual Cart Cart { get; set; }

        /// <summary>
        /// The email address used to send the email.
        /// </summary>
        [CanBeNull]
        public string From { get; set; }

        /// <summary>
        /// The email accounts of the recipients, comma separated.
        /// </summary>
        [CanBeNull]
        public string To { get; set; }

        [CanBeNull]
        public string Reference { get; set; }
    }
}