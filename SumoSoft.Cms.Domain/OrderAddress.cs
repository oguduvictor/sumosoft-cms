﻿namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class OrderAddress : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string AddressFirstName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string AddressLastName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string AddressLine1 { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string AddressLine2 { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string CountryName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string StateCountyProvince { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string City { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Postcode { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string PhoneNumber { get; set; }

    }
}