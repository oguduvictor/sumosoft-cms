﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class UserRole : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        // -------------------------------------------------------------
        // Access to Administration panel sections
        // -------------------------------------------------------------

        /// <summary>
        /// Access the Administration panel.
        /// </summary>
        public bool AccessAdmin { get; set; }

        /// <summary>
        /// Access any of the Ecommerce sections.
        /// </summary>
        public bool AccessAdminEcommerce { get; set; }

        /// <summary>
        /// Access the Orders section.
        /// </summary>
        public bool AccessAdminOrders { get; set; }

        /// <summary>
        /// Access the Blog section.
        /// </summary>
        public bool AccessAdminBlog { get; set; }

        /// <summary>
        /// Access the Media section.
        /// </summary>
        public bool AccessAdminMedia { get; set; }

        /// <summary>
        /// Access the Content section.
        /// </summary>
        public bool AccessAdminContent { get; set; }

        /// <summary>
        /// Access the Emails section.
        /// </summary>
        public bool AccessAdminEmails { get; set; }

        /// <summary>
        /// Access the Seo section.
        /// </summary>
        public bool AccessAdminSeo { get; set; }

        /// <summary>
        /// Access the Users section.
        /// </summary>
        public bool AccessAdminUsers { get; set; }

        /// <summary>
        /// Access the Countries section.
        /// </summary>
        public bool AccessAdminCountries { get; set; }

        /// <summary>
        /// Access the Settings section.
        /// </summary>
        public bool AccessAdminSettings { get; set; }

        // -------------------------------------------------------------
        // Orders section
        // -------------------------------------------------------------

        /// <summary>
        /// Edit an existing Order.
        /// </summary>
        public bool EditOrder { get; set; }

        // -------------------------------------------------------------
        // Product section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Product.
        /// </summary>
        public bool CreateProduct { get; set; }

        /// <summary>
        /// Edit an existing Product.
        /// </summary>
        public bool EditProduct { get; set; }

        /// <summary>
        /// Edit all the Product's LocalizedKits.
        /// </summary>
        public bool EditProductAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a Product.
        /// </summary>
        public bool DeleteProduct { get; set; }

        // -------------------------------------------------------------
        // Attribute section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Attribute.
        /// </summary>
        public bool CreateAttribute { get; set; }

        /// <summary>
        /// Edit an existing Attribute.
        /// </summary>
        public bool EditAttribute { get; set; }

        /// <summary>
        /// Edit the Name of an existing Attribute.
        /// </summary>
        public bool EditAttributeName { get; set; }

        /// <summary>
        /// Edit all the Attribute's LocalizedKits.
        /// </summary>
        public bool EditAttributeAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a Attribute.
        /// </summary>
        public bool DeleteAttribute { get; set; }

        // -------------------------------------------------------------
        // AttributeOption section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new AttributeOption.
        /// </summary>
        public bool CreateAttributeOption { get; set; }

        /// <summary>
        /// Edit an existing AttributeOption.
        /// </summary>
        public bool EditAttributeOption { get; set; }

        /// <summary>
        /// Edit the Name of an existing AttributeOption.
        /// </summary>
        public bool EditAttributeOptionName { get; set; }

        /// <summary>
        /// Edit all the Attribute's AttributeOption.
        /// </summary>
        public bool EditAttributeOptionAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a AttributeOption.
        /// </summary>
        public bool DeleteAttributeOption { get; set; }

        // -------------------------------------------------------------
        // Variant section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Variant.
        /// </summary>
        public bool CreateVariant { get; set; }

        /// <summary>
        /// Edit an existing Variant.
        /// </summary>
        public bool EditVariant { get; set; }

        /// <summary>
        /// Edit the Name of an existing Variant.
        /// </summary>
        public bool EditVariantName { get; set; }

        /// <summary>
        /// Edit the Option Selector of an existing Variant.
        /// </summary>
        public bool EditVariantSelector { get; set; }

        /// <summary>
        /// Edit all the Variant's LocalizedKits.
        /// </summary>
        public bool EditVariantAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a Variant.
        /// </summary>
        public bool DeleteVariant { get; set; }

        // -------------------------------------------------------------
        // VariantOption section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new VariantOption.
        /// </summary>
        public bool CreateVariantOption { get; set; }

        /// <summary>
        /// Edit an existing VariantOption.
        /// </summary>
        public bool EditVariantOption { get; set; }

        /// <summary>
        /// Edit the Name of an existing VariantOption.
        /// </summary>
        public bool EditVariantOptionName { get; set; }

        /// <summary>
        /// Edit all the VariantOption's LocalizedKits.
        /// </summary>
        public bool EditVariantOptionAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a VariantOption.
        /// </summary>
        public bool DeleteVariantOption { get; set; }

        // -------------------------------------------------------------
        // ShippingBox section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new ShippingBox.
        /// </summary>
        public bool CreateShippingBox { get; set; }

        /// <summary>
        /// Edit an existing ShippingBox.
        /// </summary>
        public bool EditShippingBox { get; set; }

        /// <summary>
        /// Edit the Name of an existing ShippingBox.
        /// </summary>
        public bool EditShippingBoxName { get; set; }

        /// <summary>
        /// Edit all the ShippingBox's LocalizedKits.
        /// </summary>
        public bool EditShippingBoxAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a ShippingBox.
        /// </summary>
        public bool DeleteShippingBox { get; set; }

        // -------------------------------------------------------------
        // Category section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Category.
        /// </summary>
        public bool CreateCategory { get; set; }

        /// <summary>
        /// Edit an existing Category.
        /// </summary>
        public bool EditCategory { get; set; }

        /// <summary>
        /// Edit the Name of an existing Category.
        /// </summary>
        public bool EditCategoryName { get; set; }

        /// <summary>
        /// Edit the Url of an existing Category.
        /// </summary>
        public bool EditCategoryUrl { get; set; }

        /// <summary>
        /// Edit all the Category's LocalizedKits.
        /// </summary>
        public bool EditCategoryAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a Category.
        /// </summary>
        public bool DeleteCategory { get; set; }

        // -------------------------------------------------------------
        // Blog section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new BlogPost.
        /// </summary>
        public bool CreateBlogPost { get; set; }

        /// <summary>
        /// Edit an existing BlogPost.
        /// </summary>
        public bool EditBlogPost { get; set; }

        /// <summary>
        /// Delete a BlogPost.
        /// </summary>
        public bool DeleteBlogPost { get; set; }

        // -------------------------------------------------------------
        // Content section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new ContentSection.
        /// </summary>
        public bool CreateContentSection { get; set; }

        /// <summary>
        /// Edit an existing ContentSection.
        /// </summary>
        public bool EditContentSection { get; set; }

        /// <summary>
        /// Edit the Name of an existing ContentSection.
        /// </summary>
        public bool EditContentSectionName { get; set; }

        /// <summary>
        /// Edit all the ContentSection's LocalizedKits.
        /// </summary>
        public bool EditContentSectionAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a ContentSection.
        /// </summary>
        public bool DeleteContentSection { get; set; }

        // -------------------------------------------------------------
        // Email section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Email.
        /// </summary>
        public bool CreateEmail { get; set; }

        /// <summary>
        /// Edit an existing Email.
        /// </summary>
        public bool EditEmail { get; set; }

        /// <summary>
        /// Edit the Name of an existing Email.
        /// </summary>
        public bool EditEmailName { get; set; }

        /// <summary>
        /// Edit all the Email's LocalizedKits.
        /// </summary>
        public bool EditEmailAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a Email.
        /// </summary>
        public bool DeleteEmail { get; set; }

        // -------------------------------------------------------------
        // Mailing List section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Mailing List.
        /// </summary>
        public bool CreateMailingList { get; set; }

        /// <summary>
        /// Edit an existing Mailing List.
        /// </summary>
        public bool EditMailingList { get; set; }

        /// <summary>
        /// Edit the Name of an existing Mailing List Name.
        /// </summary>
        public bool EditMailingListName { get; set; }

        /// <summary>
        /// Edit all the Mailing List's LocalizedKits.
        /// </summary>
        public bool EditMailingListAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a Mailing List.
        /// </summary>
        public bool DeleteMailingList { get; set; }

        // -------------------------------------------------------------
        // Seo section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new SeoSection.
        /// </summary>
        public bool CreateSeoSection { get; set; }

        /// <summary>
        /// Edit an existing SeoSection.
        /// </summary>
        public bool EditSeoSection { get; set; }

        /// <summary>
        /// Edit the Page of an existing SeoSection.
        /// </summary>
        public bool EditSeoSectionPage { get; set; }

        /// <summary>
        /// Edit all the SeoSection's LocalizedKits.
        /// </summary>
        public bool EditSeoSectionAllLocalizedKits { get; set; }

        /// <summary>
        /// Delete a SeoSection.
        /// </summary>
        public bool DeleteSeoSection { get; set; }

        // -------------------------------------------------------------
        // User section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new User.
        /// </summary>
        public bool CreateUser { get; set; }

        /// <summary>
        /// Edit an existing User.
        /// </summary>
        public bool EditUser { get; set; }

        /// <summary>
        /// Delete a User.
        /// </summary>
        public bool DeleteUser { get; set; }

        // -------------------------------------------------------------
        // UserRole section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new UserRole.
        /// </summary>
        public bool CreateUserRole { get; set; }

        /// <summary>
        /// Edit an existing UserRole.
        /// </summary>
        public bool EditUserRole { get; set; }

        /// <summary>
        /// Delete a UserRole.
        /// </summary>
        public bool DeleteUserRole { get; set; }

        // -------------------------------------------------------------
        // Country section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Country.
        /// </summary>
        public bool CreateCountry { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool EditCountry { get; set; }

        /// <summary>
        /// Delete a Country.
        /// </summary>
        public bool DeleteCountry { get; set; }

        // -------------------------------------------------------------
        // Coupon section
        // -------------------------------------------------------------

        /// <summary>
        /// Create a new Coupon.
        /// </summary>
        public bool CreateCoupon { get; set; }

        /// <summary>
        /// Edit a Coupon.
        /// </summary>
        public bool EditCoupon { get; set; }

        /// <summary>
        /// Delete a Coupon.
        /// </summary>
        public bool DeleteCoupon { get; set; }

        // -------------------------------------------------------------
        // Logs
        // -------------------------------------------------------------

        /// <summary>
        /// View code exception logs.
        /// </summary>
        public bool ViewExceptionLogs { get; set; }

        /// <summary>
        /// View ecommerce related logs.
        /// </summary>
        public bool ViewEcommerceLogs { get; set; }

        /// <summary>
        /// View content related logs.
        /// </summary>
        public bool ViewContentLogs { get; set; }
    }
}