﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using SumoSoft.Cms.Domain.Interfaces;

    public class MailingListLocalizedKit : BaseEntity, ILocalizedKit
    {
        public virtual MailingList MailingList { get; set; }

        public virtual Country Country { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        public string Description { get; set; }
    }
}
