﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Interfaces;

    public class Variant : BaseEntity, ISortable
    {
        public Variant()
        {
        }

        public Variant(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<VariantOption> VariantOptions { get; set; } = new List<VariantOption>();

        /// <summary>
        ///
        /// </summary>
        public VariantTypesEnum Type { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<VariantLocalizedKit> VariantLocalizedKits { get; set; } = new List<VariantLocalizedKit>();

        /// <summary>
        /// The default Boolean value of a Variant of Type "Boolean".
        /// </summary>
        public bool DefaultBooleanValue { get; set; }

        /// <summary>
        /// The default Double value of a Variant of Type "Double".
        /// </summary>
        public double DefaultDoubleValue { get; set; }

        /// <summary>
        /// The default Integer value of a Variant of Type "Integer".
        /// </summary>
        public int DefaultIntegerValue { get; set; }

        /// <summary>
        /// The default String value of a Variant of Type "String".
        /// </summary>
        [CanBeNull]
        public string DefaultStringValue { get; set; }

        /// <summary>
        /// The default value of a Variant of Type "Json".
        /// </summary>
        [CanBeNull]
        public string DefaultJsonValue { get; set; }

        /// <summary>
        /// The default VariantOption of a Variant of Type "Options".
        /// </summary>
        [CanBeNull]
        public virtual VariantOption DefaultVariantOptionValue { get; set; }

        /// <summary>
        /// Sets whether this ProductVariant will affect the stock of the product.
        /// </summary>
        public bool CreateProductStockUnits { get; set; }

        /// <summary>
        /// Sets whether this ProductVariant will affect the product images.
        /// </summary>
        public bool CreateProductImageKits { get; set; }
    }
}