﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class ContentSection : BaseEntity
    {
        public ContentSection()
        {
        }

        public ContentSection(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        /// Unique identifier.
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        /// The name of the schema to be used when reading the content.
        /// </summary>
        [CanBeNull]
        public string Schema { get; set; }

        /// <summary>
        /// ProductAttributes of Type "ContentSection" referencing the current ContentSection.
        /// </summary>
        [NotNull]
        public virtual List<ProductAttribute> ProductAttributes { get; set; } = new List<ProductAttribute>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<ContentSectionLocalizedKit> ContentSectionLocalizedKits { get; set; } = new List<ContentSectionLocalizedKit>();
    }
}