﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class ContentSectionLocalizedKit : BaseEntity, ILocalizedKit
    {
        public ContentSectionLocalizedKit()
        {
        }

        public ContentSectionLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual ContentSection ContentSection { get; set; }

        /// <summary>
        /// Gets or sets the current content version.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Content { get; set; }

        /// <summary>
        /// The previews content versions.
        /// </summary>
        [NotNull]
        public virtual List<ContentVersion> ContentVersions { get; set; } = new List<ContentVersion>();
    }
}