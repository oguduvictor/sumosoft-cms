﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class CartItem : BaseEntity
    {
        public CartItem()
        {
        }

        public CartItem(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public virtual CartShippingBox CartShippingBox { get; set; }

        [CanBeNull]
        public virtual WishList WishList { get; set; }

        [CanBeNull]
        public virtual Product Product { get; set; }

        public int Quantity { get; set; } = 1;

        [CanBeNull]
        public string Image { get; set; }

        [NotNull]
        public virtual List<CartItemVariant> CartItemVariants { get; set; } = new List<CartItemVariant>();

        /// <summary>
        /// If this collection contains one or more items, the Country taxes are overridden.
        /// </summary>
        [NotNull]
        public virtual List<Tax> TaxesOverride { get; set; } = new List<Tax>();
    }
}