﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    public class CategoryLocalizedKit : BaseEntity, ILocalizedKit
    {
        [CanBeNull]
        public virtual Country Country { get; set; }

        [CanBeNull]
        public virtual Category Category { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Image.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets the FeaturedTitle.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string FeaturedTitle { get; set; }

        /// <summary>
        /// Gets or sets the FeaturedDescription.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string FeaturedDescription { get; set; }

        /// <summary>
        /// Gets or sets the FeaturedImage.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string FeaturedImage { get; set; }

        /// <summary>
        /// Gets or sets the MetaTitle.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string MetaTitle { get; set; }

        /// <summary>
        /// Gets or sets the MetaDescription.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string MetaDescription { get; set; }
    }
}