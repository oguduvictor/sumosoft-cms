﻿#pragma warning disable 1591
namespace SumoSoft.Cms.Domain.Interfaces
{
    /// <summary>
    /// An entity implementing this interface contains a SortOrder property, useful for Sorting collections in a predictable way.
    /// </summary>
    public interface ISortable
    {
        int SortOrder { get; set; }
    }
}
