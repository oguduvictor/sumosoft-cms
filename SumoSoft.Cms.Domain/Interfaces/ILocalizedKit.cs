﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Interfaces
{
    using System;
    using JetBrains.Annotations;

    public interface ILocalizedKit
    {
        Guid Id { get; set; }

        [CanBeNull]
        Country Country { get; set; }
    }
}