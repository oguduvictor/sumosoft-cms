﻿
#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    public class OrderCredit : BaseEntity
    {
        public double Amount { get; set; }

        [CanBeNull]
        public string Reference { get; set; }
    }
}