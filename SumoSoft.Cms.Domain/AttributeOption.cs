﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class AttributeOption : BaseEntity, ITaggable
    {
        /// <summary>
        ///
        /// </summary>
        public virtual Attribute Attribute { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Tags { get; set; }

        [CanBeNull]
        public string AutoTags { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<AttributeOptionLocalizedKit> AttributeOptionLocalizedKits { get; set; } = new List<AttributeOptionLocalizedKit>();
    }
}