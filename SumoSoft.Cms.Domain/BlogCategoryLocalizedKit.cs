﻿namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class BlogCategoryLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        public virtual BlogCategory BlogCategory { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual Country Country { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }
    }
}
