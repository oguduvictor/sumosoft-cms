﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    public class OrderItemTax : BaseEntity
    {
        [CanBeNull]
        public virtual OrderItem OrderItem { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        public double Amount { get; set; }
    }
}
