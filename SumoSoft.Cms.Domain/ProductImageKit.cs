﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ProductImageKit : BaseEntity
    {
        public ProductImageKit()
        {
        }

        public ProductImageKit(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public virtual Product Product { get; set; }

        [NotNull]
        public virtual List<VariantOption> VariantOptions { get; set; } = new List<VariantOption>();

        [NotNull]
        public virtual List<ProductImage> ProductImages { get; set; } = new List<ProductImage>();

        [NotNull]
        public virtual List<Category> Categories { get; set; } = new List<Category>();

        [NotNull]
        public virtual List<ProductImageKit> RelatedProductImageKits { get; set; } = new List<ProductImageKit>();
    }
}