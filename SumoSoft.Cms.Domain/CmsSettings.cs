﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class CmsSettings : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        public bool IsSeeded { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string SmtpHost { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int SmtpPort { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string SmtpDisplayName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string SmtpEmail { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string SmtpPassword { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool UseAzureStorage { get; set; }
    }
}