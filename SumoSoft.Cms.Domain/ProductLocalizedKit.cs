﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    public class ProductLocalizedKit : BaseEntity, ILocalizedKit
    {
        public ProductLocalizedKit()
        {
        }

        public ProductLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Product Product { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the MetaTitle.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string MetaTitle { get; set; }

        /// <summary>
        /// Gets or sets the MetaDescription.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string MetaDescription { get; set; }
    }
}