﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;

    public class OrderItem : BaseEntity
    {
        [CanBeNull]
        public virtual OrderShippingBox OrderShippingBox { get; set; }

        public Guid ProductId { get; set; }

        [CanBeNull]
        public string ProductTitle { get; set; }

        [CanBeNull]
        public string ProductName { get; set; }

        [CanBeNull]
        public string ProductUrl { get; set; }

        [CanBeNull]
        public string ProductCode { get; set; }

        public int ProductQuantity { get; set; } = 1;

        public Guid ProductStockUnitId { get; set; }

        public DateTime ProductStockUnitReleaseDate { get; set; } = DateTime.Now;

        public bool ProductStockUnitEnablePreorder { get; set; }

        public int ProductStockUnitShipsIn { get; set; }

        [CanBeNull]
        public string ProductStockUnitCode { get; set; }

        public double ProductStockUnitBasePrice { get; set; }

        public double ProductStockUnitSalePrice { get; set; }

        public double ProductStockUnitMembershipSalePrice { get; set; }

        [CanBeNull]
        public string Image { get; set; }

        [NotNull]
        public virtual List<OrderItemVariant> OrderItemVariants { get; set; } = new List<OrderItemVariant>();

        [NotNull]
        public virtual List<OrderItemTax> OrderTaxes { get; set; } = new List<OrderItemTax>();

        public OrderItemStatusEnum Status { get; set; }

        public OrderItemStockStatusEnum StockStatus { get; set; } = OrderItemStockStatusEnum.Decreased;

        public DateTime MinEta { get; set; } = DateTime.Now;

        public DateTime MaxEta { get; set; } = DateTime.Now;

        public double SubtotalBeforeTax { get; set; }

        public double SubtotalAfterTax { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }

    }
}