﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using JetBrains.Annotations;

    public class UserVariant : BaseEntity
    {
        public UserVariant()
        {
        }

        public UserVariant(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public virtual User User { get; set; }

        [CanBeNull]
        public virtual Variant Variant { get; set; }

        public bool BooleanValue { get; set; }

        public double DoubleValue { get; set; }

        public int IntegerValue { get; set; }

        [CanBeNull]
        public string StringValue { get; set; }

        [CanBeNull]
        public string JsonValue { get; set; }

        [CanBeNull]
        public virtual VariantOption VariantOptionValue { get; set; }
    }
}
