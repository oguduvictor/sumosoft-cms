﻿namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class UserCredit : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        public virtual UserLocalizedKit UserLocalizedKit { get; set; }

        /// <summary>
        ///
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Reference { get; set; }
    }
}