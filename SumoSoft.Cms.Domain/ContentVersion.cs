﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    public class ContentVersion : BaseEntity
    {
        [CanBeNull]
        public virtual ContentSectionLocalizedKit ContentSectionLocalizedKit { get; set; }

        [CanBeNull]
        public string Content { get; set; }
    }
}
