﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class Category : BaseEntity, ISortable, ITaggable
    {
        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        public int SortOrder { get; set; }

        [CanBeNull]
        public string Tags { get; set; }

        [CanBeNull]
        public string AutoTags { get; set; }

        [CanBeNull]
        public string ProductsSortOrder { get; set; }

        [CanBeNull]
        public string ProductStockUnitsSortOrder { get; set; }

        [CanBeNull]
        public string ProductImageKitsSortOrder { get; set; }

        public CategoryTypesEnum Type { get; set; }

        [CanBeNull]
        public virtual Category Parent { get; set; }

        [NotNull]
        public virtual List<Category> Children { get; set; } = new List<Category>();

        [NotNull]
        public virtual List<Product> Products { get; set; } = new List<Product>();

        [NotNull]
        public virtual List<ProductStockUnit> ProductStockUnits { get; set; } = new List<ProductStockUnit>();

        [NotNull]
        public virtual List<ProductImageKit> ProductImageKits { get; set; } = new List<ProductImageKit>();

        [NotNull]
        public virtual List<CategoryLocalizedKit> CategoryLocalizedKits { get; set; } = new List<CategoryLocalizedKit>();
    }
}