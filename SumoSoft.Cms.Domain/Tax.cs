﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using JetBrains.Annotations;

    public class Tax : BaseEntity
    {
        public Tax()
        {
        }

        public Tax(Guid id)
        {
            this.Id = id;
        }

        [CanBeNull]
        public virtual Country Country { get; set; }

        [CanBeNull]
        public virtual CartItem CartItem { get; set; }

        /// <summary>
        /// If set, the Tax will only be considered valid if the current Shipping Address matches this value.
        /// If not, the the Tax will be deleted from the CartItem.
        /// </summary>
        [CanBeNull]
        public virtual Address ShippingAddress { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Code { get; set; }

        public double Percentage { get; set; }

        /// <summary>
        /// When set, this property takes precedence over the "Percentage" property.
        /// Use this property when the Tax has already been calculated, for example in the case of CartItem.TaxesOverride.
        /// </summary>
        public double? Amount { get; set; }

        public bool ApplyToShippingPrice { get; set; } = true;

        public bool ApplyToProductPrice { get; set; } = true;
    }
}
