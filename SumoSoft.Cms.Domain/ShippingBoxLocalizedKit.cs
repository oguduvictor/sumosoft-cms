﻿namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class ShippingBoxLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual ShippingBox ShippingBox { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string InternalDescription { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Carrier { get; set; }

        /// <summary>
        /// Gets or sets the MinDays.
        /// </summary>
        public int MinDays { get; set; }

        /// <summary>
        /// Gets or sets the MaxDays.
        /// </summary>
        public int MaxDays { get; set; }

        /// <summary>
        /// Gets or sets the CountWeekends.
        /// </summary>
        public bool CountWeekends { get; set; }

        /// <summary>
        /// Gets or sets the Price.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Gets or sets the FreeShippingMinimumPrice.
        /// </summary>
        public double FreeShippingMinimumPrice { get; set; }
    }
}