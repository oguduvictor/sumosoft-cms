﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    public class VariantOptionLocalizedKit : BaseEntity, ILocalizedKit
    {
        public VariantOptionLocalizedKit()
        {
        }

        public VariantOptionLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual VariantOption VariantOption { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the PriceModifier.
        /// </summary>
        public double PriceModifier { get; set; }

    }
}