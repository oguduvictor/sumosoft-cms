﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class Cart : BaseEntity
    {
        public Cart()
        {
        }

        public Cart(Guid id)
        {
            this.Id = id;
        }

        [NotNull]
        public virtual List<CartShippingBox> CartShippingBoxes { get; set; } = new List<CartShippingBox>();

        [CanBeNull]
        public virtual Address ShippingAddress { get; set; }

        [CanBeNull]
        public virtual Address BillingAddress { get; set; }

        [CanBeNull]
        public virtual User User { get; set; }

        [CanBeNull]
        public virtual Coupon Coupon { get; set; }

        /// <summary>
        /// Negative UserCredit applied to the current Cart as a discount.
        /// </summary>
        [CanBeNull]
        public virtual UserCredit UserCredit { get; set; }

        [NotNull]
        public virtual List<SentEmail> SentEmails { get; set; } = new List<SentEmail>();
    }
}