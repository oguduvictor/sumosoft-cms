﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class Email : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        /// The name of the view used as MVC template for the current Email, excluding the .cshtml extension.
        /// </summary>
        [CanBeNull]
        public string ViewName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual ContentSection ContentSection { get; set; }

        [NotNull]
        public virtual List<EmailLocalizedKit> EmailLocalizedKits { get; set; } = new List<EmailLocalizedKit>();
    }
}
