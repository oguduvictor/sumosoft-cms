﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Interfaces;

    public class Order : BaseEntity, ITaggable
    {
        public int? OrderNumber { get; set; }

        public OrderStatusEnum Status { get; set; }

        [CanBeNull]
        public string Tags { get; set; }

        [CanBeNull]
        public string AutoTags { get; set; }

        [CanBeNull]
        public string CountryName { get; set; }

        [CanBeNull]
        public string CountryLanguageCode { get; set; }

        /// <summary>
        /// Currency Symbol ($, £...)
        /// </summary>
        [CanBeNull]
        public string CurrencySymbol { get; set; }

        /// <summary>
        /// Three letters ISO4217 Currency Symbol (USD, GBP...)
        /// </summary>
        [CanBeNull]
        public string ISO4217CurrencySymbol { get; set; }

        public Guid UserId { get; set; }

        [CanBeNull]
        public string UserFirstName { get; set; }

        [CanBeNull]
        public string UserLastName { get; set; }

        [CanBeNull]
        public string UserEmail { get; set; }

        [CanBeNull]
        public string TransactionId { get; set; }

        [NotNull]
        public virtual List<OrderShippingBox> OrderShippingBoxes { get; set; } = new List<OrderShippingBox>();

        [CanBeNull]
        public virtual OrderAddress ShippingAddress { get; set; }

        [CanBeNull]
        public virtual OrderAddress BillingAddress { get; set; }

        [CanBeNull]
        public string Comments { get; set; }

        [NotNull]
        public virtual List<SentEmail> SentEmails { get; set; } = new List<SentEmail>();

        [CanBeNull]
        public virtual OrderCoupon OrderCoupon { get; set; }

        /// <summary>
        /// Negative Credit value applied to the current Cart as a discount.
        /// </summary>
        [CanBeNull]
        public virtual OrderCredit OrderCredit { get; set; }

        public double TotalBeforeTax { get; set; }

        public double TotalAfterTax { get; set; }

        public double TotalToBePaid { get; set; }

        public double TotalPaid { get; set; }
    }
}