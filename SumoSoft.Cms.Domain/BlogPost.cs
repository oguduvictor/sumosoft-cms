﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class BlogPost : BaseEntity
    {
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// The current content version.
        /// </summary>
        [CanBeNull]
        public string Content { get; set; }

        public bool IsPublished { get; set; }

        public DateTime PublicationDate { get; set; } = DateTime.Now;

        public virtual User Author { get; set; }

        [CanBeNull]
        public string FeaturedImage { get; set; }

        [CanBeNull]
        public string FeaturedImageHorizontalCrop { get; set; } = "50%";

        [CanBeNull]
        public string FeaturedImageVerticalCrop { get; set; } = "50%";

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string MetaTitle { get; set; }

        [CanBeNull]
        public string MetaDescription { get; set; }

        [CanBeNull]
        public string Excerpt { get; set; }

        [NotNull]
        public virtual List<BlogCategory> BlogCategories { get; set; } = new List<BlogCategory>();

        [NotNull]
        public virtual List<BlogComment> BlogComments { get; set; } = new List<BlogComment>();
    }
}