﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class Product : BaseEntity, ITaggable
    {
        public Product()
        {
        }

        public Product(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }


        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        /// Used uniquely identify a Product from an external source, like a third party Stock Management Software.
        /// </summary>
        [CanBeNull]
        public string Code { get; set; }

        /// <summary>
        /// Used to identify a Product type for tax purposes.
        /// </summary>
        [CanBeNull]
        public string TaxCode { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Comments { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Tags { get; set; }

        [CanBeNull]
        public string AutoTags { get; set; }

        [NotNull]
        public virtual List<Category> Categories { get; set; } = new List<Category>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<ProductAttribute> ProductAttributes { get; set; } = new List<ProductAttribute>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<ProductVariant> ProductVariants { get; set; } = new List<ProductVariant>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<ProductStockUnit> ProductStockUnits { get; set; } = new List<ProductStockUnit>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<ProductImageKit> ProductImageKits { get; set; } = new List<ProductImageKit>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<ShippingBox> ShippingBoxes { get; set; } = new List<ShippingBox>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<Product> RelatedProducts { get; set; } = new List<Product>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<ProductLocalizedKit> ProductLocalizedKits { get; set; } = new List<ProductLocalizedKit>();
    }
}