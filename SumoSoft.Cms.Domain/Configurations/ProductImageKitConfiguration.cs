﻿namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

#pragma warning disable 1591
    public class ProductImageKitConfiguration : EntityTypeConfiguration<ProductImageKit>
    {
        public ProductImageKitConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.HasMany(a => a.VariantOptions).WithMany();
            this.HasMany(x => x.RelatedProductImageKits).WithOptional();
        }
    }
}
