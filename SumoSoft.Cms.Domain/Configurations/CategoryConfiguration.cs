﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    public class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.Property(c => c.Url).HasMaxLength(250).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            this.Property(c => c.Name).HasMaxLength(250).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            this.HasOptional(c => c.Parent).WithMany(x => x.Children);
            this.HasMany(c => c.Children).WithOptional(c => c.Parent);
            this.HasMany(c => c.Products).WithMany(c => c.Categories);
            this.HasMany(c => c.ProductStockUnits).WithMany(c => c.Categories);
            this.HasMany(c => c.ProductImageKits).WithMany(c => c.Categories);
        }
    }
}