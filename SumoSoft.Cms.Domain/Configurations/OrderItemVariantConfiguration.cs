﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class OrderItemVariantConfiguration : EntityTypeConfiguration<OrderItemVariant>
    {
        public OrderItemVariantConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.Property(c => c.StringValue).HasMaxLength(500);
            this.Property(c => c.JsonValue).HasMaxLength(4000);
        }
    }
}