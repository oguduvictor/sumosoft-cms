﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.Property(c => c.Url).HasMaxLength(250).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            this.HasMany(x => x.ShippingBoxes).WithMany(x => x.Products); // specify x => x.Products because ShippingBox contains a Navigation Property
            this.HasMany(x => x.RelatedProducts).WithMany().Map(x =>
            {
                x.MapLeftKey("ProductID");
                x.MapRightKey("RelatedProductID");
                x.ToTable("RelatedProducts");
            });
        }
    }
}