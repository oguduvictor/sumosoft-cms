﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    public class VariantOptionConfiguration : EntityTypeConfiguration<VariantOption>
    {
        public VariantOptionConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.Property(x => x.Url).HasMaxLength(250).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            this.Property(c => c.Name).HasMaxLength(250).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            this.HasOptional(x => x.Variant).WithMany(x => x.VariantOptions);
        }
    }
}