﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class UserCreditConfiguration : EntityTypeConfiguration<UserCredit>
    {
        public UserCreditConfiguration()
        {
            this.HasKey(u => u.Id);
            this.Property(u => u.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
        }
    }
}