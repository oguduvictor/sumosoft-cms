﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class ContentVersionConfiguration : EntityTypeConfiguration<ContentVersion>
    {
        public ContentVersionConfiguration()
        {
            this.HasKey(x => x.Id);
            this.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
        }
    }
}
