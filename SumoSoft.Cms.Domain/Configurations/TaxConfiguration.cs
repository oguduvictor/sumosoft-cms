﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class TaxConfiguration : EntityTypeConfiguration<Tax>
    {
        public TaxConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.HasOptional(x => x.Country).WithMany(x => x.Taxes).Map(x => x.MapKey("Country_Id"));
            this.HasOptional(x => x.CartItem).WithMany(x => x.TaxesOverride).Map(x => x.MapKey("CartItem_Id"));
        }
    }
}