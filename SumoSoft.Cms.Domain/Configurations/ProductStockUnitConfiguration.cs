﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class ProductStockUnitConfiguration : EntityTypeConfiguration<ProductStockUnit>
    {
        public ProductStockUnitConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.HasMany(a => a.VariantOptions).WithMany();
        }
    }
}
