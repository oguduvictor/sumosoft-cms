﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public class OrderItemTaxConfiguration : EntityTypeConfiguration<OrderItemTax>
    {
        public OrderItemTaxConfiguration()
        {
            this.HasKey(a => a.Id);
            this.Property(a => a.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();
            this.HasOptional(x => x.OrderItem).WithMany(x => x.OrderTaxes).Map(x => x.MapKey("OrderItem_Id"));
        }
    }
}