﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain.Configurations
{
    using System.Data.Entity.ModelConfiguration;

    public class MailingListSubscriptionConfiguration : EntityTypeConfiguration<MailingListSubscription>
    {
        public MailingListSubscriptionConfiguration()
        {
            this.HasKey(a => new { a.MailingListId, a.UserId });
            this.Property(x => x.UserId).HasColumnName("User_Id");
            this.Property(x => x.MailingListId).HasColumnName("MailingList_Id");
            this.HasRequired(x => x.MailingList).WithMany(x => x.MailingListSubscriptions);
            this.HasRequired(x => x.User).WithMany(x => x.MailingListSubscriptions);
        }
    }
}
