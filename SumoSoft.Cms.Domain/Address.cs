﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;

    public class Address : BaseEntity
    {
        [CanBeNull]
        public virtual User User { get; set; }

        [CanBeNull]
        public string FirstName { get; set; }

        [CanBeNull]
        public string LastName { get; set; }

        [CanBeNull]
        public string AddressLine1 { get; set; }

        [CanBeNull]
        public string AddressLine2 { get; set; }

        [CanBeNull]
        public string City { get; set; }

        [CanBeNull]
        public virtual Country Country { get; set; }

        [CanBeNull]
        public string StateCountyProvince { get; set; }

        [CanBeNull]
        public string Postcode { get; set; }

        [CanBeNull]
        public string PhoneNumber { get; set; }
    }
}