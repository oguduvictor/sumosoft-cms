﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class Country : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        public Country()
        {
        }

        /// <summary>
        ///
        /// </summary>
        public Country(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string FlagIcon { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        /// ISO 639-1 language code.
        /// </summary>
        [CanBeNull]
        public string LanguageCode { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Create a Country-specific version of the website.
        /// </summary>
        public bool Localize { get; set; }

        /// <summary>
        /// The payment account Id associated to this country.
        /// </summary>
        [CanBeNull]
        public string PaymentAccountId { get; set; }

        /// <summary>
        /// The list of Taxes imposed in the Country.
        /// </summary>
        [NotNull]
        public virtual List<Tax> Taxes { get; set; } = new List<Tax>();

        /// <summary>
        /// The list of ShippingBoxes that are valid in this country.
        /// </summary>
        [NotNull]
        public virtual List<ShippingBox> ShippingBoxes { get; set; } = new List<ShippingBox>();
    }
}