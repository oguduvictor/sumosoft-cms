﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using SumoSoft.Cms.Domain.Interfaces;

    public class ProductStockUnitLocalizedKit : BaseEntity, ILocalizedKit
    {
        public ProductStockUnitLocalizedKit()
        {
        }

        public ProductStockUnitLocalizedKit(Guid id)
        {
            this.Id = id;
        }

        public virtual ProductStockUnit ProductStockUnit { get; set; }

        public virtual Country Country { get; set; }

        /// <summary>
        /// Gets or sets the BasePrice.
        /// </summary>
        public double BasePrice { get; set; }

        /// <summary>
        /// Gets or sets the SalePrice.
        /// </summary>
        public double SalePrice { get; set; }

        /// <summary>
        /// Gets or sets the MembershipSalePrice.
        /// </summary>
        public double MembershipSalePrice { get; set; }
    }
}
