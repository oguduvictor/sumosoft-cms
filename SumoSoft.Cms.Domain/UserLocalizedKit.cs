﻿namespace SumoSoft.Cms.Domain
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class UserLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual User User { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<UserCredit> UserCredits { get; set; } = new List<UserCredit>();
    }
}