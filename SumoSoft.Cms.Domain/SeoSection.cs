﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    /// <summary>
    ///
    /// </summary>
    public class SeoSection : BaseEntity
    {
        public SeoSection()
        {
        }

        public SeoSection(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Page { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<SeoSectionLocalizedKit> SeoSectionLocalizedKits { get; set; } = new List<SeoSectionLocalizedKit>();
    }
}