﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Interfaces;

    public class Attribute : BaseEntity, ISortable
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Name { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        ///
        /// </summary>
        public AttributeTypesEnum Type { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string Url { get; set; }

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<AttributeOption> AttributeOptions { get; set; } = new List<AttributeOption>();

        /// <summary>
        ///
        /// </summary>
        [NotNull]
        public virtual List<AttributeLocalizedKit> AttributeLocalizedKits { get; set; } = new List<AttributeLocalizedKit>();

    }
}