﻿namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    /// <summary>
    ///
    /// </summary>
    public class VariantLocalizedKit : BaseEntity, ILocalizedKit
    {
        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Country Country { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Variant Variant { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// If null or empty, the default country's value will be used as a fallback.
        /// </summary>
        [CanBeNull]
        public string Description { get; set; }
    }
}