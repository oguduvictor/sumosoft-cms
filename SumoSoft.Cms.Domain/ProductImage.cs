﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain.Interfaces;

    public class ProductImage : BaseEntity, ISortable
    {
        public virtual ProductImageKit ProductImageKit { get; set; }

        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Url { get; set; }

        [CanBeNull]
        public string AltText { get; set; }

        public int SortOrder { get; set; }
    }
}