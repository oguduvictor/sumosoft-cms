﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using JetBrains.Annotations;

    public class UserAttribute : BaseEntity
    {
        /// <summary>
        ///
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        ///
        /// </summary>
        public virtual Attribute Attribute { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string StringValue { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool BooleanValue { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime? DateTimeValue { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string ImageValue { get; set; }

        /// <summary>
        ///
        /// </summary>
        public double DoubleValue { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual AttributeOption AttributeOptionValue { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual ContentSection ContentSectionValue { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public string JsonValue { get; set; }
    }
}