﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using JetBrains.Annotations;

    public class BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public bool IsDisabled { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.Now;

        [CanBeNull]
        public DateTime? DeletedDate { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}