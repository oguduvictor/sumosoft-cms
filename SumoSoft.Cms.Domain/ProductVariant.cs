﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Domain
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ProductVariant : BaseEntity
    {
        public ProductVariant()
        {
        }

        public ProductVariant(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Product Product { get; set; }

        /// <summary>
        ///
        /// </summary>
        [CanBeNull]
        public virtual Variant Variant { get; set; }

        /// <summary>
        /// The list of VariantOptions available for the current Product.
        /// </summary>
        [NotNull]
        public virtual List<VariantOption> VariantOptions { get; set; } = new List<VariantOption>();

        /// <summary>
        /// The default Boolean value of a Variant of Type "Boolean".
        /// </summary>
        public bool DefaultBooleanValue { get; set; }

        /// <summary>
        /// The default Double value of a Variant of Type "Double".
        /// </summary>
        public double DefaultDoubleValue { get; set; }

        /// <summary>
        /// The default Integer value of a Variant of Type "Integer".
        /// </summary>
        public int DefaultIntegerValue { get; set; }

        /// <summary>
        /// The default String value of a Variant of Type "String".
        /// </summary>
        [CanBeNull]
        public string DefaultStringValue { get; set; }

        /// <summary>
        /// The default value of a Variant of Type "Json".
        /// </summary>
        [CanBeNull]
        public string DefaultJsonValue { get; set; }

        /// <summary>
        /// The default VariantOption of a Variant of Type "Options".
        /// </summary>
        [CanBeNull]
        public virtual VariantOption DefaultVariantOptionValue { get; set; }
    }
}