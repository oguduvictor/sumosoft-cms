﻿namespace SumoSoft.Cms.RedisCache.Interfaces
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Domain;

    public interface ICmsRedisCacheService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="dependencies"></param>
        /// <returns></returns>
        [CanBeNull]
        string Get(string cacheKey, IReadOnlyCollection<BaseEntity> dependencies);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        /// <param name="relativeExpiration"></param>
        /// <param name="dependencies"></param>
        void Set(string cacheKey, string value, TimeSpan relativeExpiration, IReadOnlyCollection<BaseEntity> dependencies);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="relativeExpiration"></param>
        /// <param name="dependencies"></param>
        /// <param name="setter"></param>
        /// <returns></returns>
        [NotNull]
        string GetOrSet(string cacheKey, TimeSpan relativeExpiration, IReadOnlyCollection<BaseEntity> dependencies, Func<string> setter);
    }
}
