﻿namespace SumoSoft.Cms.RedisCache
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using StackExchange.Redis;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.RedisCache.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <inheritdoc />
    public class CmsRedisCacheService : ICmsRedisCacheService
    {
        private static IUtilityService _utilityService => DependencyResolver.Current.GetService<IUtilityService>();

        /// <inheritdoc />
        public string Get(string cacheKey, IReadOnlyCollection<BaseEntity> dependencies)
        {
            var cacheWrapper = GetCacheWrapper(cacheKey, dependencies);

            if (cacheWrapper == null || !cacheWrapper.IsValid)
            {
                return string.Empty;
            }

            return cacheWrapper.Value;
        }

        /// <inheritdoc />
        public void Set(string cacheKey, string value, TimeSpan relativeExpiration, IReadOnlyCollection<BaseEntity> dependencies)
        {
            dependencies = (dependencies ?? new List<BaseEntity>()).Where(x => x != null).ToList();

            var cacheWrapper = new RedisCacheWrapper
            {
                CreatedDate = DateTime.Now,
                Dependencies = dependencies.Select(x => x.Id).ToList(),
                Value = value
            };

            using (var redisConnection = GetRedisConnection())
            {
                redisConnection.GetDatabase().StringSet(cacheKey, cacheWrapper.ToCamelCaseJson(), relativeExpiration);
            }
        }

        /// <inheritdoc />
        public string GetOrSet(string cacheKey, TimeSpan relativeExpiration, IReadOnlyCollection<BaseEntity> dependencies, Func<string> setter)
        {
            var cacheWrapper = GetCacheWrapper(cacheKey, dependencies);

            if (cacheWrapper != null && cacheWrapper.IsValid)
            {
                return cacheWrapper.Value;
            }

            var value = setter();

            this.Set(cacheKey, value, relativeExpiration, dependencies);

            return value;
        }

        private static ConnectionMultiplexer GetRedisConnection()
        {
            var redisDbConnectionString = _utilityService.GetAppSetting("RedisConnectionString");

            return ConnectionMultiplexer.Connect(redisDbConnectionString);
        }

        private static RedisCacheWrapper GetCacheWrapper(string cacheKey, IReadOnlyCollection<BaseEntity> dependencies)
        {
            dependencies = (dependencies ?? new List<BaseEntity>()).Where(x => x != null).ToList();

            using (var redisConnection = GetRedisConnection())
            {
                var redisCacheValue = redisConnection.GetDatabase().StringGet(cacheKey);

                if (redisCacheValue.IsNull)
                {
                    return null;
                }

                var cacheResult = _utilityService.FromJson<RedisCacheWrapper>(redisCacheValue);

                if (dependencies.Count != cacheResult.Dependencies.Count ||
                    dependencies.Any(x => x.ModifiedDate > cacheResult.CreatedDate) ||
                    !dependencies.Select(x => x.Id).ContainsAll(cacheResult.Dependencies))
                {
                    cacheResult.IsValid = false;
                }

                return cacheResult;
            }
        }

        private class RedisCacheWrapper
        {
            public DateTime CreatedDate { get; set; }

            public List<Guid> Dependencies { get; set; } = new List<Guid>();

            public string Value { get; set; }

            public bool IsValid { get; set; } = true;
        }
    }
}
