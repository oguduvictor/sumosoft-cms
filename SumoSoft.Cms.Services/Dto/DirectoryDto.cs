﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Services.Dto
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class DirectoryDto
    {
        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string LocalPath { get; set; }

        public long Size { get; set; }

        public DateTime CreationTime { get; set; }

        public bool UseAzureStorage { get; set; }

        public List<FileDto> Files { get; set; } = new List<FileDto>();

        public List<DirectoryDto> Directories { get; set; } = new List<DirectoryDto>();

        public DirectoryDto Parent { get; set; }

        public bool IsRoot { get; set; }
    }
}