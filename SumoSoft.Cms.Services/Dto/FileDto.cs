﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Services.Dto
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Web;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Services.Interfaces;

    // -----------------------------------------------------------------------
    // Properties example:
    //
    // Name = file
    // Extension = .jpeg
    // LocalPath = /content/img/file.jpeg
    // AbsolutePath = www.domain.com/content/img/file.jpeg
    // -----------------------------------------------------------------------

    public class FileDto
    {
        [CanBeNull]
        public string Name { get; set; }

        [CanBeNull]
        public string Extension { get; set; }

        [CanBeNull]
        public string LocalPath { get; set; }

        [CanBeNull]
        public string LocalPathWithVersion => this.LocalPath + "?v=" + this.GetHash();

        [CanBeNull]
        public string AbsolutePath { get; set; }

        [CanBeNull]
        public string AbsolutePathWithVersion => this.AbsolutePath + "?v=" + this.GetHash();

        public long Size { get; set; }

        public DateTime CreationTime { get; set; } = DateTime.Now;

        public bool UseAzureStorage { get; set; }

        public long HeavyLoadingFileSize { get; set; }

        [CanBeNull]
        public DirectoryDto GetDirectory(ILocalStorageService localStorageService)
        {
            if (string.IsNullOrEmpty(this.LocalPath))
            {
                return null;
            }

            var directoryFullName = HttpUtility.UrlDecode(this.LocalPath.Replace(this.Name + this.Extension, "").TrimEnd('/'));

            return localStorageService.GetDirectoryDto(directoryFullName);
        }

        [CanBeNull]
        public DirectoryDto GetDirectory(IAzureStorageService azureStorageService)
        {
            if (string.IsNullOrEmpty(this.LocalPath))
            {
                return null;
            }

            var directoryFullName = HttpUtility.UrlDecode(this.LocalPath.Replace(this.Name + this.Extension, "").TrimEnd('/'));

            return azureStorageService.GetDirectoryDto(directoryFullName);
        }

        private string GetHash()
        {
            if (this.UseAzureStorage)
            {
                return this.Size.ToString();
            }

            var serverPath = HttpContext.Current.Server.MapPath(this.LocalPath);

            var fileInfo = new FileInfo(serverPath);

            using (var fileStream = fileInfo.OpenRead())
            {
                using (var md5 = MD5.Create())
                {
                    return BitConverter.ToString(md5.ComputeHash(fileStream)).Replace("-", "").ToLower();
                }
            }
        }
    }
}