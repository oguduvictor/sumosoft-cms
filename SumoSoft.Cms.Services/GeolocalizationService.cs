﻿namespace SumoSoft.Cms.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Text;
    using SumoSoft.Cms.Services.Interfaces;

    /// <inheritdoc />
    public class GeolocalizationService : IGeolocalizationService
    {
        /// <inheritdoc />
        public virtual string GetCountryFromIp(string ipAddress)
        {
            const string csvPath = "SumoSoft.Cms.Services.Csv.Output.csv";

            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(csvPath))
            {
                if (stream == null)
                {
                    throw new NullReferenceException("No IP database was found at " + csvPath);
                }

                var ip = IPAddress.Parse(ipAddress);

                var rows = GetRows(() => stream, Encoding.UTF8);

                foreach (var row in rows)
                {
                    var columns = row.Split(';');

                    var addressRange = new IpAddressRange(IPAddress.Parse(columns[0]), IPAddress.Parse(columns[1]));

                    if (addressRange.IsInRange(ip))
                    {
                        return columns[2];
                    }
                }
            }

            return string.Empty;
        }

        internal class IpAddressRange
        {
            private readonly AddressFamily addressFamily;
            private readonly byte[] lowerBytes;
            private readonly byte[] upperBytes;

            public IpAddressRange(IPAddress lowerInclusive, IPAddress upperInclusive)
            {
                this.addressFamily = lowerInclusive.AddressFamily;
                this.lowerBytes = lowerInclusive.GetAddressBytes();
                this.upperBytes = upperInclusive.GetAddressBytes();
            }

            public bool IsInRange(IPAddress address)
            {
                if (address.AddressFamily != this.addressFamily)
                {
                    return false;
                }

                var addressBytes = address.GetAddressBytes();

                bool lowerBoundary = true, upperBoundary = true;

                for (var i = 0; i < this.lowerBytes.Length && (lowerBoundary || upperBoundary); i++)
                {
                    if (lowerBoundary && addressBytes[i] < this.lowerBytes[i] ||
                        upperBoundary && addressBytes[i] > this.upperBytes[i])
                    {
                        return false;
                    }

                    lowerBoundary &= addressBytes[i] == this.lowerBytes[i];
                    upperBoundary &= addressBytes[i] == this.upperBytes[i];
                }

                return true;
            }
        }

        private static IEnumerable<string> GetRows(Func<Stream> streamProvider, Encoding encoding)
        {
            using (var stream = streamProvider())
            using (var reader = new StreamReader(stream, encoding))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }

        private static void CreateCsvOutput()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var outputFile = $"{AppDomain.CurrentDomain.BaseDirectory}App_Data\\Output.csv";

            const string inputResource = "SumoSoft.Cms.Services.Csv.Input.csv";


            using (var objWriter = new StreamWriter(outputFile))
            using (var stream = assembly.GetManifestResourceStream(inputResource))
            {
                if (stream == null)
                {
                    return;
                }

                var allInput = GetRows(() => stream, Encoding.UTF8).ToList();

                var sb = new StringBuilder();

                allInput.ForEach(country =>
                {
                    var fields = country.Split(',', ';').ToList();

                    sb.Append(fields[0] + ';');
                    sb.Append(fields[1] + ';');
                    sb.Append(fields.Last() + ";\n");

                });

                objWriter.Write(sb.ToString());
            }
        }
    }
}
