﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Services.Validators
{
    using FluentValidation;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Extensions;

    public class AddOrUpdateAddressValidator : AbstractValidator<Address>
    {
        public AddOrUpdateAddressValidator()
        {
            this.RuleFor(x => x.FirstName)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.LastName)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.AddressLine1)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.AddressLine2)
                .MustNotContainScript();

            this.RuleFor(x => x.City)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.Postcode)
                .NotEmpty()
                .Length(1, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.StateCountyProvince)
                .Length(0, 100)
                .MustNotContainScript();

            this.RuleFor(x => x.Country)
                .NotNull();

            this.When(x => x.Country != null, () =>
            {
                this.RuleFor(x => x.Country.Id).NotEmpty();
            });
        }
    }
}
