﻿#pragma warning disable 1591

namespace SumoSoft.Cms.Services.Validators
{
    using System;
    using FluentValidation;
    using FluentValidation.Results;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;

    public class ApplyCouponToCartValidator : AbstractValidator<Coupon>
    {
        private readonly ICountryService countryService;
        private readonly IAuthenticationService authenticationService;

        /// <summary>
        ///
        /// </summary>
        public ApplyCouponToCartValidator(
            ICountryService countryService,
            IAuthenticationService authenticationService)
        {
            this.countryService = countryService;
            this.authenticationService = authenticationService;

            this.Custom(coupon =>
            {
                using (var dbContext = new CmsDbContext())
                {
                    if (coupon == null)
                    {
                        return new ValidationFailure("Code", "This Coupon code is invalid.");
                    }

                    if (coupon.Country != null && coupon.Country.LanguageCode != this.countryService.GetCurrentLanguageCode())
                    {
                        return new ValidationFailure("Country", "This Coupon cannot be used in the current Country.");
                    }

                    if (coupon.ExpirationDate < DateTime.Now)
                    {
                        return new ValidationFailure("ExpirationDate", "This Coupon is expired.");
                    }

                    if (coupon.ValidityTimes < 1)
                    {
                        return new ValidationFailure("ValidityTimes", "This Coupon has already been used.");
                    }

                    if (!coupon.Published)
                    {
                        return new ValidationFailure("Published", "This Coupon code is invalid.");
                    }

                    if (coupon.Referee != null)
                    {
                        var authenticatedUser = this.authenticationService.GetAuthenticatedUser(dbContext);

                        if (authenticatedUser == null)
                        {
                            return new ValidationFailure("Referee", "Please login before using this Coupon.");
                        }

                        if (coupon.Referee.Id == authenticatedUser.Id)
                        {
                            return new ValidationFailure("Referee", "A Coupon cannot be used by the same person who generated it.");
                        }
                    }

                    return null;
                }
            });
        }
    }
}