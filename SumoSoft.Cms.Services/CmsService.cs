﻿namespace SumoSoft.Cms.Services
{
    using System.Linq;
    using SumoSoft.Cms.AuthenticationServices.Interfaces;
    using SumoSoft.Cms.AutoTagServices.Interfaces;
    using SumoSoft.Cms.ContentSectionServices.Interfaces;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.LogServices.Interfaces;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.Services.Validators;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    public partial class CmsService : ICmsService
    {
        private readonly ICountryService countryService;
        private readonly IUtilityService utilityService;
        private readonly IAuthenticationService authenticationService;
        private readonly ILogService logService;
        private readonly IContentSectionService contentSectionService;
        private readonly ApplyCouponToCartValidator applyCouponToCartValidator;
        private readonly AddOrUpdateAddressValidator addOrUpdateAddressValidator;
        private readonly IAutoTagService autoTagService;

        /// <inheritdoc />
        public CmsService(
            ICountryService countryService,
            IUtilityService utilityService,
            IAuthenticationService authenticationService,
            ILogService logService,
            IContentSectionService contentSectionService,
            ApplyCouponToCartValidator applyCouponToCartValidator,
            AddOrUpdateAddressValidator addOrUpdateAddressValidator,
            IAutoTagService autoTagService)
        {
            this.countryService = countryService;
            this.utilityService = utilityService;
            this.authenticationService = authenticationService;
            this.logService = logService;
            this.contentSectionService = contentSectionService;
            this.applyCouponToCartValidator = applyCouponToCartValidator;
            this.addOrUpdateAddressValidator = addOrUpdateAddressValidator;
            this.autoTagService = autoTagService;
        }

        /// <inheritdoc />
        public virtual SeoSection SeoSection(CmsDbContext dbContext, string page, bool notifyifMissing = false)
        {
            var seoSection = dbContext.SeoSections.FirstOrDefault(x => x.Page == page);

            if (seoSection == null && notifyifMissing)
            {
                this.logService.LogAsync(LogTypesEnum.Content, $"SeoSection '{page}' not found");
            }

            return seoSection;
        }
    }
}
