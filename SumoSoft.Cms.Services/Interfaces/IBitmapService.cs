﻿namespace SumoSoft.Cms.Services.Interfaces
{
    using System.Drawing;
    using System.IO;

    /// <summary>
    ///
    /// </summary>
    public interface IBitmapService
    {
        /// <summary>
        /// Resize Image method for Azure Storage.
        /// </summary>
        /// <param name="compression"></param>
        /// <param name="imageStream"></param>
        /// <param name="newWidth"></param>
        /// <returns></returns>
        MemoryStream ResizeImage(int compression, MemoryStream imageStream, int newWidth);

        /// <summary>
        /// Resize Image method for Local Storage.
        /// </summary>
        /// <param name="compression"></param>
        /// <param name="imageStream"></param>
        /// <param name="newWidth"></param>
        /// <param name="newPath"></param>
        void ResizeImage(int compression, MemoryStream imageStream, int newWidth, string newPath);
    }
}