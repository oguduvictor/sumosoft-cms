﻿namespace SumoSoft.Cms.Services.Interfaces
{
    using System.Collections.Generic;
    using System.IO;
    using Microsoft.WindowsAzure.Storage.Blob;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Services.Dto;

    /// <summary>
    ///
    /// </summary>
    public interface IAzureStorageService
    {
        /// <summary>
        /// Create a zip backup of a Directory in the Backup Container.
        /// </summary>
        /// <param name="localPath"></param>
        void BackupDirectory(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="currentDirectory"></param>
        /// <param name="newDirectoryName"></param>
        /// <returns></returns>
        FormResponse CreateDirectory(string currentDirectory, string newDirectoryName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        FormResponse DeleteDirectory(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        FormResponse DeleteFile(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        CloudBlobDirectory GetCloudBlobDirectory(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="localPath"></param>
        /// <returns></returns>
        CloudBlobDirectory GetCloudBlobDirectory(string connectionString, string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        CloudBlockBlob GetCloudBlockBlob(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="localPath"></param>
        /// <returns></returns>
        CloudBlockBlob GetCloudBlockBlob(string connectionString, string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        IEnumerable<CloudBlockBlob> GetCloudBlockBlobs(string localPath);

        /// <summary>
        /// Return all the filed contained in a certain directory, including all its sub-directories.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="localPath"></param>
        /// <returns></returns>
        IEnumerable<CloudBlockBlob> GetCloudBlockBlobs(string connectionString, string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="keywordFilter"></param>
        /// <returns></returns>
        DirectoryDto GetDirectoryDto(string localPath = "", string keywordFilter = "");

        /// <summary>
        /// Maps a Blob to an object of Type FileDto.
        /// </summary>
        /// <param name="localPath"></param>
        /// <returns></returns>
        FileDto GetFileDto(string localPath);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="newFileName"></param>
        /// <returns></returns>
        FormResponse RenameFile(string localPath, string newFileName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        FormResponse ResizeFile(string localPath, ResizeFileDto model);

        /// <summary>
        ///
        /// </summary>
        /// <param name="directoryLocalPath"></param>
        /// <param name="fileStream"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        FormResponse UploadFile(string directoryLocalPath, Stream fileStream, string fileName);
    }
}