﻿namespace SumoSoft.Cms.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Dto;

    /// <summary>
    ///
    /// </summary>
    public interface ICmsService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="amount"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        FormResponse<double> ApplyUserCreditToCart(CmsDbContext dbContext, double amount, string reference = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cart"></param>
        /// <returns></returns>
        FormResponse<double> RemoveCreditFromCart(CmsDbContext dbContext, Cart cart = null);

        /// <summary>
        /// Add an item to the current cart.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItem"></param>
        void AddToCart(CmsDbContext dbContext, CartItem cartItem);

        /// <summary>
        /// Add an item to the Wish List.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        CartItem AddToWishList(CmsDbContext dbContext, CartItem cartItem);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="user"></param>
        /// <param name="amount"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        bool AddUserCredit(CmsDbContext dbContext, User user, double amount, string reference = null);

        /// <summary>
        /// Creates a Cart for the current Authenticated User or Partial User.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        Cart CreateCart(CmsDbContext dbContext);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="shippingBoxId"></param>
        /// <param name="cart"></param>
        /// <returns></returns>
        CartShippingBox CreateCartShippingBox(CmsDbContext dbContext, Guid? shippingBoxId, Cart cart = null);

        /// <summary>
        /// Creates and saves a new order from the current Cart.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cart"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        Order CreateOrder(CmsDbContext dbContext, Cart cart = null, string transactionId = null);

        /// <summary>
        /// Creates a Wish List for the current Authenticated User or Partial User.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        WishList CreateWishList(CmsDbContext dbContext);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartId"></param>
        void DeleteCart(CmsDbContext dbContext, Guid? cartId = null);

        /// <summary>
        /// Gets the Cart of the current Authenticated User or Partial User.
        /// If the cart contains a disabled Product, it will be removed.
        /// If the cart contains an invalid Coupon, it will be removed.        ///. </summary>
        /// <param name="dbContext"></param>
        /// <param name="asNoTracking"></param>
        /// <returns></returns>
        [CanBeNull]
        Cart GetCart(CmsDbContext dbContext, bool asNoTracking = false);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        CartDto GetCartDto(CmsDbContext dbContext);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        CartItemDto GetCartItemDto(CmsDbContext dbContext, Guid id);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="shippingBoxId"></param>
        /// <param name="cart"></param>
        /// <returns></returns>
        CartShippingBox GetCartShippingBox(CmsDbContext dbContext, Guid? shippingBoxId, Cart cart = null);

        /// <summary>
        /// Gets the Wish List of the current Authenticated User or Partial User.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        WishList GetWishList(CmsDbContext dbContext);

        /// <summary>
        /// Creates a new CartItem from a Product.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="product"></param>
        /// <param name="selectedVariantOptions"></param>
        /// <returns></returns>
        CartItem ProductToCartItem(CmsDbContext dbContext, Product product, List<VariantOption> selectedVariantOptions = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="productUrl"></param>
        /// <param name="selectedVariantOptionUrls"></param>
        /// <returns></returns>
        CartItemDto ProductToCartItemDto(CmsDbContext dbContext, string productUrl, List<string> selectedVariantOptionUrls = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="couponCode"></param>
        /// <returns></returns>
        FormResponse ApplyCouponToCart(CmsDbContext dbContext, string couponCode);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cart"></param>
        void RemoveCouponFromCart(CmsDbContext dbContext, Cart cart = null);

        /// <summary>
        /// Remove an item from the current cart.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItemId"></param>
        void RemoveFromCart(CmsDbContext dbContext, Guid cartItemId);

        /// <summary>
        /// Remove an item from the current Wish List.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItemId"></param>
        void RemoveFromWishList(CmsDbContext dbContext, Guid cartItemId);

        /// <summary>
        /// Adds a new address or updates an address if it already exists.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="address"></param>
        /// <param name="setUser"></param>
        /// <param name="setAsCartShippingAddress"></param>
        /// <param name="setAsCartBillingAddress"></param>
        /// <param name="cart"></param>
        FormResponse<Address> AddOrUpdateAddress(CmsDbContext dbContext, Address address, bool setUser = false, bool setAsCartShippingAddress = false, bool setAsCartBillingAddress = false, Cart cart = null);

        /// <summary>
        /// Set the Cart Billing Address.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="addressId"></param>
        /// <param name="cart"></param>
        FormResponse SetCartBillingAddress(CmsDbContext dbContext, Guid? addressId, Cart cart = null);

        /// <summary>
        /// Set the Cart Shipping Address.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="addressId"></param>
        /// <param name="cart"></param>
        FormResponse SetCartShippingAddress(CmsDbContext dbContext, Guid? addressId, Cart cart = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItemId"></param>
        /// <param name="shippingBoxName"></param>
        /// <param name="cart"></param>
        void SetShippingBox(CmsDbContext dbContext, Guid cartItemId, string shippingBoxName, Cart cart = null);

        /// <summary>
        /// Updates an CartItem.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItem"></param>
        void UpdateCartItem(CmsDbContext dbContext, CartItem cartItem);

        /// <summary>
        /// Updates an CartItem Quantity.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cartItemId"></param>
        /// <param name="quantity"></param>
        void UpdateCartItemQuantity(CmsDbContext dbContext, Guid cartItemId, int quantity);

        /// <summary>
        /// Return a SeoSection.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="page"></param>
        /// <param name="notifyifMissing"></param>
        /// <returns></returns>
        SeoSection SeoSection(CmsDbContext dbContext, string page, bool notifyifMissing = false);
    }
}