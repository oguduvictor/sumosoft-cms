﻿namespace SumoSoft.Cms.Services.Interfaces
{
    using System.IO;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Services.Dto;

    /// <summary>
    ///
    /// </summary>
    public interface ILocalStorageService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="localPath"></param>
        /// <param name="backupFileName"></param>
        void BackupDirectory(string localPath, string backupFileName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="currentDirectory"></param>
        /// <param name="newDirectoryName"></param>
        /// <returns></returns>
        FormResponse CreateDirectory(string currentDirectory, string newDirectoryName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="sourceDirectoryName"></param>
        /// <param name="destinationArchiveFileName"></param>
        void CreateZipFileFromDirectory(string sourceDirectoryName, string destinationArchiveFileName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        FormResponse DeleteDirectory(string fullName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        FormResponse DeleteFile(string fullName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="keywordFilter"></param>
        /// <returns></returns>
        DirectoryDto GetDirectoryDto(string fullName = "", string keywordFilter = "");

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        FileDto GetFileDto(string fullName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        FormResponse RenameFile(string fullName, string newName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        FormResponse ResizeFile(string fullName, ResizeFileDto model);

        /// <summary>
        ///
        /// </summary>
        /// <param name="fullName"></param>
        /// <param name="stream"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        FormResponse UploadFile(string fullName, Stream stream, string fileName);
    }
}