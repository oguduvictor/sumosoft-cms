﻿namespace SumoSoft.Cms.Services.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IGeolocalizationService
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns>string.</returns>
        string GetCountryFromIp(string ipAddress);
    }
}