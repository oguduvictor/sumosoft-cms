﻿namespace SumoSoft.Cms.Services
{
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using SumoSoft.Cms.Services.Interfaces;

    /// <inheritdoc />
    public class BitmapService : IBitmapService
    {
        // ---------------------------------------------------------------------------------------------------------
        // RESIZE IMAGE METHOD FOR AZURESTORAGEMETHODS
        // ---------------------------------------------------------------------------------------------------------

        /// <inheritdoc />
        public virtual MemoryStream ResizeImage(int compression, MemoryStream imageStream, int newWidth)
        {
            var bitmap = new Bitmap(imageStream, true);

            var newImageStream = new MemoryStream();

            var percentW = bitmap.Width / (float)newWidth;

            using (var bmp = new Bitmap(newWidth, (int)(bitmap.Height / percentW)))
            {
                var jpgEncoder = SetImageEncoder(compression, bitmap, bmp, out var parameters);

                bmp.Save(newImageStream, jpgEncoder, parameters);

                newImageStream.Seek(0, SeekOrigin.Begin);
            }

            return newImageStream;
        }

        // ---------------------------------------------------------------------------------------------------------
        // RESIZE IMAGE METHOD FOR LOCALSTORAGEMETHODS
        // ---------------------------------------------------------------------------------------------------------

        /// <inheritdoc />
        public virtual void ResizeImage(int compression, MemoryStream imageStream, int newWidth, string newPath)
        {
            var bitmap = new Bitmap(imageStream, true);

            var percentW = bitmap.Width / (float)newWidth;

            using (var bmp = new Bitmap(newWidth, (int)(bitmap.Height / percentW)))
            {
                var jpgEncoder = SetImageEncoder(compression, bitmap, bmp, out var parameters);

                bmp.Save(newPath, jpgEncoder, parameters);
            }
        }

        // ---------------------------------------------------------------------------------------------------------
        // PRIVATE METHODS
        // ---------------------------------------------------------------------------------------------------------

        private static ImageCodecInfo SetImageEncoder(int compression, Image image, Image bmp, out EncoderParameters parameters)
        {
            using (var graphics = Graphics.FromImage(bmp))
            {
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.DrawImage(image, 0, 0, bmp.Width, bmp.Height);
            }

            var codecs = ImageCodecInfo.GetImageDecoders();

            var jpgEncoder = codecs.First(codec => codec.FormatID == ImageFormat.Jpeg.Guid);

            parameters = new EncoderParameters(1);

            parameters.Param[0] = new EncoderParameter(Encoder.Quality, compression);

            return jpgEncoder;
        }
    }
}