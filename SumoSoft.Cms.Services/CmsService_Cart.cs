﻿namespace SumoSoft.Cms.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.DataAccess.Extensions;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Dto;
    using SumoSoft.Cms.Domain.Enums;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;

    /// <inheritdoc />
    public partial class CmsService
    {
        /// <inheritdoc />
        public virtual bool AddUserCredit(CmsDbContext dbContext, User user, double amount, string reference = null)
        {
            var userLocalizedKit = user.UserLocalizedKits.GetByLanguageCode();

            if (userLocalizedKit == null)
            {
                userLocalizedKit = new UserLocalizedKit
                {
                    Country = this.countryService.GetCurrentCountry(dbContext),
                    User = user
                };

                dbContext.UserLocalizedKits.Add(userLocalizedKit);
            }

            var userCredit = new UserCredit
            {
                UserLocalizedKit = userLocalizedKit,
                Amount = amount,
                Reference = reference
            };

            dbContext.UserCredits.Add(userCredit);

            dbContext.SaveChanges();

            return true;
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual WishList GetWishList(CmsDbContext dbContext)
        {
            var user = this.authenticationService.GetAuthenticatedUser(dbContext) ??
                       this.authenticationService.GetPartiallyAuthenticatedUser(dbContext);

            return user?.WishList;
        }

        /// <inheritdoc />
        public virtual WishList CreateWishList(CmsDbContext dbContext)
        {
            var user = this.authenticationService.GetAuthenticatedUser(dbContext) ??
                       this.authenticationService.GetPartiallyAuthenticatedUser(dbContext);

            if (user == null)
            {
                user = new User
                {
                    Country = this.countryService.GetCurrentCountry(dbContext),
                    Role = dbContext.UserRoles.FirstOrDefault(x => x.Name == SharedConstants.DefaultUserRole)
                };

                dbContext.Users.Add(user);

                dbContext.SaveChanges();

                this.authenticationService.PartiallyAuthenticateUser(user);
            }

            var wishList = new WishList
            {
                User = user
            };

            dbContext.WishLists.Add(wishList);

            dbContext.SaveChanges();

            return wishList;
        }

        /// <inheritdoc />
        public virtual CartItem AddToWishList(CmsDbContext dbContext, CartItem cartItem)
        {
            var wishList = this.GetWishList(dbContext) ?? this.CreateWishList(dbContext);

            var product = dbContext.Products.Find(cartItem.Product?.Id);

            cartItem.Product = product ?? throw new NullReferenceException();

            foreach (var cartItemVariant in cartItem.CartItemVariants.ToList())
            {
                cartItemVariant.CartItem = cartItem;
                cartItemVariant.Variant = dbContext.Variants.Find(cartItemVariant.Variant?.Id);
                cartItemVariant.VariantOptionValue = dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id);
            }

            cartItem.CartShippingBox = null;
            cartItem.WishList = wishList;

            dbContext.CartItems.Add(cartItem);

            dbContext.SaveChanges();

            return cartItem;
        }

        /// <inheritdoc />
        public virtual void RemoveFromWishList(CmsDbContext dbContext, Guid cartItemId)
        {
            dbContext.CartItems.Delete(cartItemId, true);
        }

        /// <inheritdoc />
        public virtual CartItem ProductToCartItem(CmsDbContext dbContext, Product product, List<VariantOption> selectedVariantOptions = null)
        {
            var shippingBox = product.GetShippingBoxesForShippingCountry(dbContext).FirstOrDefault();

            if (shippingBox == null)
            {
                this.logService.LogAsync(
                    LogTypesEnum.Content,
                    $"No ShippingBox found for the product {product.GetLocalizedTitle()} in ${this.countryService.GetCurrentCountry(dbContext).Name}.");
            }

            var cartItem = new CartItem
            {
                Product = product,
                CartShippingBox = new CartShippingBox
                {
                    ShippingBox = shippingBox ?? product.ShippingBoxes.OrderBySortOrder().FirstOrDefault()
                }
            };

            foreach (var productVariant in product.ProductVariants)
            {
                var cartItemVariant = new CartItemVariant
                {
                    CartItem = cartItem,
                    Variant = productVariant.Variant,
                    BooleanValue = productVariant.DefaultBooleanValue,
                    DoubleValue = productVariant.DefaultDoubleValue,
                    IntegerValue = productVariant.DefaultIntegerValue,
                    StringValue = productVariant.DefaultStringValue,
                    JsonValue = productVariant.DefaultJsonValue,
                };

                var variantOptionValue = selectedVariantOptions?.FirstOrDefault(x => x.Variant?.Id == productVariant.Variant?.Id);

                if (variantOptionValue == null || variantOptionValue.IsDisabled || variantOptionValue.GetLocalizedIsDisabled())
                {
                    variantOptionValue = productVariant.VariantOptions
                        .OrderBySortOrder()
                        .FirstOrDefault(x => !x.IsDisabled && !x.GetLocalizedIsDisabled());
                }

                cartItemVariant.VariantOptionValue = variantOptionValue;

                cartItem.CartItemVariants.Add(cartItemVariant);
            }

            return cartItem;
        }

        /// <inheritdoc />
        public virtual CartItemDto ProductToCartItemDto(CmsDbContext dbContext, string productUrl, List<string> selectedVariantOptionUrls = null)
        {
            var product = dbContext.Products.FirstOrDefault(x => x.Url == productUrl);

            if (product == null)
            {
                return null;
            }

            var selectedVariantOptions = selectedVariantOptionUrls
                ?.SelectMany(variantOptionUrl => product.ProductVariants.SelectMany(pv => pv.VariantOptions).Where(vo => vo.Url == variantOptionUrl))
                .Where(vo => vo != null)
                .ToList();

            var cartItem = this.ProductToCartItem(dbContext, product, selectedVariantOptions);

            return new CartItemDto
            {
                Id = cartItem.Id,
                Quantity = cartItem.Quantity,
                Image = cartItem.Image,
                MinEta = cartItem.GetMinEta(),
                MaxEta = cartItem.GetMaxEta(),
                CartShippingBox = cartItem.CartShippingBox == null ? null : new CartShippingBoxDto
                {
                    Id = cartItem.CartShippingBox.Id,
                    ShippingBox = cartItem.CartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                    {
                        Id = cartItem.CartShippingBox.ShippingBox.Id,
                        Name = cartItem.CartShippingBox.ShippingBox.Name,
                        RequiresShippingAddress = cartItem.CartShippingBox.ShippingBox.RequiresShippingAddress,
                        LocalizedTitle = cartItem.CartShippingBox.ShippingBox.GetLocalizedTitle(),
                        LocalizedDescription = cartItem.CartShippingBox.ShippingBox.GetLocalizedDescription(),
                        LocalizedPrice = cartItem.CartShippingBox.ShippingBox.GetLocalizedPrice()
                    }
                },
                Product = cartItem.Product == null ? null : new ProductDto
                {
                    Id = cartItem.Product.Id,
                    Name = cartItem.Product.Name,
                    Url = cartItem.Product.Url,
                    Code = cartItem.Product.Code,
                    TaxCode = cartItem.Product.TaxCode,
                    Categories = cartItem.Product.Categories.Select(category => new CategoryDto
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Url = category.Url,
                        LocalizedTitle = category.GetLocalizedTitle()
                    }).ToList(),
                    ProductAttributes = cartItem.Product.ProductAttributes.Select(productAttribute => new ProductAttributeDto
                    {
                        Id = productAttribute.Id,
                        Attribute = productAttribute.Attribute == null ? null : new AttributeDto
                        {
                            Id = productAttribute.Attribute.Id,
                            Name = productAttribute.Attribute.Name,
                            Url = productAttribute.Attribute.Url,
                            Type = productAttribute.Attribute.Type
                        },
                        AttributeOptionValue = productAttribute.AttributeOptionValue == null ? null : new AttributeOptionDto
                        {
                            Id = productAttribute.AttributeOptionValue.Id,
                            Name = productAttribute.AttributeOptionValue.Name,
                            Url = productAttribute.AttributeOptionValue.Url,
                            LocalizedTitle = productAttribute.AttributeOptionValue.GetLocalizedTitle()
                        },
                        ContentSectionValue = productAttribute.ContentSectionValue == null ? null : new ContentSectionDto
                        {
                            Id = productAttribute.ContentSectionValue.Id,
                            Name = productAttribute.ContentSectionValue.Name,
                            LocalizedContent = productAttribute.ContentSectionValue.GetLocalizedContent()
                        },
                        DoubleValue = productAttribute.DoubleValue,
                        StringValue = productAttribute.StringValue,
                        ImageValue = productAttribute.ImageValue,
                        BooleanValue = productAttribute.BooleanValue,
                        DateTimeValue = productAttribute.DateTimeValue
                    }).ToList(),
                    ProductVariants = cartItem.Product.ProductVariants.Select(productVariant => new ProductVariantDto
                    {
                        Id = productVariant.Id,
                        Variant = productVariant.Variant == null ? null : new VariantDto
                        {
                            Id = productVariant.Variant.Id,
                            Name = productVariant.Variant.Name,
                            Type = productVariant.Variant.Type,
                            Url = productVariant.Variant.Url,
                            SortOrder = productVariant.Variant.SortOrder,
                            CreateProductImageKits = productVariant.Variant.CreateProductImageKits,
                            CreateProductStockUnits = productVariant.Variant.CreateProductStockUnits
                        },
                        VariantOptions = productVariant
                                .VariantOptions
                                .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled())
                                .OrderBySortOrder()
                                .ThenBy(x => x.Name)
                                .Select(variantOption => new VariantOptionDto
                                {
                                    Id = variantOption.Id,
                                    Name = variantOption.Name,
                                    Url = variantOption.Url,
                                    Image = variantOption.Image,
                                    LocalizedTitle = variantOption.GetLocalizedTitle(),
                                    LocalizedDescription = variantOption.GetLocalizedDescription(),
                                    LocalizedPriceModifier = variantOption.GetLocalizedPriceModifier(),
                                    LocalizedIsDisabled = variantOption.GetLocalizedIsDisabled()
                                }).ToList()
                    }).ToList(),
                    ShippingBoxes = cartItem.Product.ShippingBoxes
                        .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled())
                        .OrderBySortOrder()
                        .Select(shippingBox => new ShippingBoxDto
                        {
                            Id = shippingBox.Id,
                            Name = shippingBox.Name,
                            RequiresShippingAddress = shippingBox.RequiresShippingAddress,
                            LocalizedTitle = shippingBox.GetLocalizedTitle(),
                            LocalizedDescription = shippingBox.GetLocalizedDescription(),
                            LocalizedPrice = shippingBox.GetLocalizedPrice(),
                            LocalizedFreeShippingMinimumPrice = shippingBox.GetLocalizedFreeShippingMinimumPrice(),
                            LocalizedMinDays = shippingBox.GetLocalizedMinDays(),
                            LocalizedMaxDays = shippingBox.GetLocalizedMaxDays(),
                            Countries = shippingBox.Countries.Select(country => new CountryDto
                            {
                                Id = country.Id,
                                Name = country.Name
                            }).ToList()
                        }).ToList(),
                    ProductStockUnits = cartItem.Product.ProductStockUnits.Select(productStockUnit => new ProductStockUnitDto
                    {
                        Id = productStockUnit.Id,
                        DispatchDate = productStockUnit.DispatchDate,
                        EnablePreorder = productStockUnit.EnablePreorder,
                        DispatchTime = productStockUnit.DispatchTime,
                        Stock = productStockUnit.Stock,
                        Code = productStockUnit.Code,
                        LocalizedBasePrice = productStockUnit.GetLocalizedBasePrice(),
                        LocalizedSalePrice = productStockUnit.GetLocalizedSalePrice(),
                        LocalizedMembershipSalePrice = productStockUnit.GetLocalizedMembershipSalePrice(),
                        VariantOptions = productStockUnit.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            Name = variantOption.Name,
                            Url = variantOption.Url,
                            Variant = variantOption.Variant == null ? null : new VariantDto
                            {
                                Id = variantOption.Variant.Id
                            }
                        }).ToList()
                    }).ToList(),
                    ProductImageKits = cartItem.Product.ProductImageKits.Select(productImageKit => new ProductImageKitDto
                    {
                        Id = productImageKit.Id,
                        VariantOptions = productImageKit.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            Name = variantOption.Name,
                            Url = variantOption.Url
                        }).ToList(),
                        ProductImages = productImageKit.ProductImages.OrderBySortOrder().Select(productImage => new ProductImageDto
                        {
                            Id = productImage.Id,
                            Name = productImage.Name,
                            Url = productImage.Url,
                            SortOrder = productImage.SortOrder
                        }).ToList()
                    }).ToList(),
                    LocalizedTitle = cartItem.Product.GetLocalizedTitle(),
                    LocalizedDescription = cartItem.Product.GetLocalizedDescription(),
                    LocalizedMetaTitle = cartItem.Product.GetLocalizedMetaTitle(),
                    LocalizedMetaDescription = cartItem.Product.GetLocalizedMetaDescription()
                },
                CartItemVariants = cartItem.CartItemVariants.Select(cartItemVariant => new CartItemVariantDto
                {
                    Id = cartItemVariant.Id,
                    CartItem = cartItemVariant.CartItem == null ? null : new CartItemDto
                    {
                        Id = cartItemVariant.CartItem.Id
                    },
                    Variant = cartItemVariant.Variant == null ? null : new VariantDto
                    {
                        Id = cartItemVariant.Variant.Id,
                        Name = cartItemVariant.Variant.Name,
                        Type = cartItemVariant.Variant.Type,
                        Url = cartItemVariant.Variant.Url,
                        LocalizedTitle = cartItemVariant.Variant.GetLocalizedTitle(),
                        LocalizedDescription = cartItemVariant.Variant.GetLocalizedDescription()
                    },
                    VariantOptionValue = cartItemVariant.VariantOptionValue == null ? null : new VariantOptionDto
                    {
                        Id = cartItemVariant.VariantOptionValue.Id,
                        Name = cartItemVariant.VariantOptionValue.Name,
                        Url = cartItemVariant.VariantOptionValue.Url,
                        Image = cartItemVariant.VariantOptionValue.Image,
                        LocalizedTitle = cartItemVariant.VariantOptionValue.GetLocalizedTitle(),
                        LocalizedDescription = cartItemVariant.VariantOptionValue.GetLocalizedDescription(),
                        LocalizedPriceModifier = cartItemVariant.VariantOptionValue.GetLocalizedPriceModifier()
                    },
                    BooleanValue = cartItemVariant.BooleanValue,
                    DoubleValue = cartItemVariant.DoubleValue,
                    IntegerValue = cartItemVariant.IntegerValue,
                    StringValue = cartItemVariant.StringValue,
                    JsonValue = cartItemVariant.JsonValue
                }).ToList(),
                SubtotalBeforeTax = cartItem.GetSubtotalBeforeTax(dbContext),
                SubtotalAfterTax = cartItem.GetSubtotalAfterTax(dbContext),
                SubtotalBeforeTaxWithoutSalePrice = cartItem.GetSubtotalBeforeTaxWithoutSalePrice(dbContext),
                SubtotalAfterTaxWithoutSalePrice = cartItem.GetSubtotalAfterTaxWithoutSalePrice(dbContext),
                TotalBeforeTax = cartItem.GetTotalBeforeTax(dbContext),
                TotalAfterTax = cartItem.GetTotalAfterTax(dbContext)
            };
        }

        /// <inheritdoc />
        public virtual Cart GetCart(CmsDbContext dbContext, bool asNoTracking = false)
        {
            var user = this.authenticationService.GetAuthenticatedUser(dbContext, asNoTracking) ??
                       this.authenticationService.GetPartiallyAuthenticatedUser(dbContext, asNoTracking);

            return user?.Cart;
        }

        /// <inheritdoc />
        public virtual CartItemDto GetCartItemDto(CmsDbContext dbContext, Guid id)
        {
            var cartItem = dbContext.CartItems.Find(id);

            if (cartItem == null)
            {
                return null;
            }

            return new CartItemDto
            {
                Id = cartItem.Id,
                Quantity = cartItem.Quantity,
                Image = cartItem.Image,
                MinEta = cartItem.GetMinEta(),
                MaxEta = cartItem.GetMaxEta(),
                CouponApplies = cartItem.CouponApplies(dbContext),
                CartShippingBox = cartItem.CartShippingBox == null ? null : new CartShippingBoxDto
                {
                    Id = cartItem.CartShippingBox.Id,
                    ShippingBox = cartItem.CartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                    {
                        Id = cartItem.CartShippingBox.ShippingBox.Id,
                        Name = cartItem.CartShippingBox.ShippingBox.Name,
                        RequiresShippingAddress = cartItem.CartShippingBox.ShippingBox.RequiresShippingAddress,
                        LocalizedTitle = cartItem.CartShippingBox.ShippingBox.GetLocalizedTitle(),
                        LocalizedDescription = cartItem.CartShippingBox.ShippingBox.GetLocalizedDescription(),
                        LocalizedPrice = cartItem.CartShippingBox.ShippingBox.GetLocalizedPrice()
                    }
                },
                Product = cartItem.Product == null ? null : new ProductDto
                {
                    Id = cartItem.Product.Id,
                    Name = cartItem.Product.Name,
                    Url = cartItem.Product.Url,
                    Code = cartItem.Product.Code,
                    TaxCode = cartItem.Product.TaxCode,
                    Categories = cartItem.Product.Categories.Select(category => new CategoryDto
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Url = category.Url,
                        LocalizedTitle = category.GetLocalizedTitle()
                    }).ToList(),
                    ProductAttributes = cartItem.Product.ProductAttributes.Select(productAttribute => new ProductAttributeDto
                    {
                        Id = productAttribute.Id,
                        Attribute = productAttribute.Attribute == null ? null : new AttributeDto
                        {
                            Id = productAttribute.Attribute.Id,
                            Name = productAttribute.Attribute.Name,
                            Url = productAttribute.Attribute.Url,
                            Type = productAttribute.Attribute.Type
                        },
                        AttributeOptionValue = productAttribute.AttributeOptionValue == null ? null : new AttributeOptionDto
                        {
                            Id = productAttribute.AttributeOptionValue.Id,
                            Name = productAttribute.AttributeOptionValue.Name,
                            Url = productAttribute.AttributeOptionValue.Url,
                            LocalizedTitle = productAttribute.AttributeOptionValue.GetLocalizedTitle()
                        },
                        ContentSectionValue = productAttribute.ContentSectionValue == null ? null : new ContentSectionDto
                        {
                            Id = productAttribute.ContentSectionValue.Id,
                            Name = productAttribute.ContentSectionValue.Name,
                            LocalizedContent = productAttribute.ContentSectionValue.GetLocalizedContent()
                        },
                        DoubleValue = productAttribute.DoubleValue,
                        StringValue = productAttribute.StringValue,
                        ImageValue = productAttribute.ImageValue,
                        BooleanValue = productAttribute.BooleanValue,
                        DateTimeValue = productAttribute.DateTimeValue
                    }).ToList(),
                    ProductVariants = cartItem.Product.ProductVariants.Select(productVariant => new ProductVariantDto
                    {
                        Id = productVariant.Id,
                        Variant = productVariant.Variant == null ? null : new VariantDto
                        {
                            Id = productVariant.Variant.Id,
                            Name = productVariant.Variant.Name,
                            Type = productVariant.Variant.Type,
                            Url = productVariant.Variant.Url,
                            CreateProductImageKits = productVariant.Variant.CreateProductImageKits,
                            CreateProductStockUnits = productVariant.Variant.CreateProductStockUnits
                        },
                        VariantOptions = productVariant
                                .VariantOptions
                                .Where(x => !x.IsDisabled && !x.GetLocalizedIsDisabled())
                                .OrderBySortOrder()
                                .ThenBy(x => x.Name)
                                .Select(variantOption => new VariantOptionDto
                                {
                                    Id = variantOption.Id,
                                    Name = variantOption.Name,
                                    Url = variantOption.Url,
                                    Image = variantOption.Image,
                                    LocalizedTitle = variantOption.GetLocalizedTitle(),
                                    LocalizedDescription = variantOption.GetLocalizedDescription(),
                                    LocalizedPriceModifier = variantOption.GetLocalizedPriceModifier()
                                }).ToList()
                    }).ToList(),
                    ShippingBoxes = cartItem.Product.ShippingBoxes.OrderBySortOrder().Select(shippingBox => new ShippingBoxDto
                    {
                        Id = shippingBox.Id,
                        Name = shippingBox.Name,
                        RequiresShippingAddress = shippingBox.RequiresShippingAddress,
                        LocalizedTitle = shippingBox.GetLocalizedTitle(),
                        LocalizedDescription = shippingBox.GetLocalizedDescription(),
                        LocalizedPrice = shippingBox.GetLocalizedPrice(),
                        LocalizedFreeShippingMinimumPrice = shippingBox.GetLocalizedFreeShippingMinimumPrice(),
                        LocalizedMinDays = shippingBox.GetLocalizedMinDays(),
                        LocalizedMaxDays = shippingBox.GetLocalizedMaxDays(),
                        Countries = shippingBox.Countries.Select(country => new CountryDto
                        {
                            Id = country.Id,
                            Name = country.Name
                        }).ToList()
                    }).ToList(),
                    ProductStockUnits = cartItem.GetProductStockUnit().InList().Select(productStockUnit => new ProductStockUnitDto
                    {
                        Id = productStockUnit.Id,
                        DispatchDate = productStockUnit.DispatchDate,
                        EnablePreorder = productStockUnit.EnablePreorder,
                        DispatchTime = productStockUnit.DispatchTime,
                        Stock = productStockUnit.Stock,
                        Code = productStockUnit.Code,
                        LocalizedBasePrice = productStockUnit.GetLocalizedBasePrice(),
                        LocalizedSalePrice = productStockUnit.GetLocalizedSalePrice(),
                        VariantOptions = productStockUnit.VariantOptions.Select(variantOption => new VariantOptionDto
                        {
                            Id = variantOption.Id,
                            Name = variantOption.Name,
                            Url = variantOption.Url
                        }).ToList()
                    }).ToList(),
                    LocalizedTitle = cartItem.Product.GetLocalizedTitle(),
                    LocalizedDescription = cartItem.Product.GetLocalizedDescription(),
                    LocalizedMetaTitle = cartItem.Product.GetLocalizedMetaTitle(),
                    LocalizedMetaDescription = cartItem.Product.GetLocalizedMetaDescription()
                },
                CartItemVariants = cartItem.CartItemVariants.Select(cartItemVariant => new CartItemVariantDto
                {
                    Id = cartItemVariant.Id,
                    CartItem = cartItemVariant.CartItem == null ? null : new CartItemDto
                    {
                        Id = cartItemVariant.CartItem.Id
                    },
                    Variant = cartItemVariant.Variant == null ? null : new VariantDto
                    {
                        Id = cartItemVariant.Variant.Id,
                        Name = cartItemVariant.Variant.Name,
                        Type = cartItemVariant.Variant.Type,
                        Url = cartItemVariant.Variant.Url,
                        LocalizedTitle = cartItemVariant.Variant.GetLocalizedTitle()
                    },
                    VariantOptionValue = cartItemVariant.VariantOptionValue == null ? null : new VariantOptionDto
                    {
                        Id = cartItemVariant.VariantOptionValue.Id,
                        Name = cartItemVariant.VariantOptionValue.Name,
                        Url = cartItemVariant.VariantOptionValue.Url,
                        Image = cartItemVariant.VariantOptionValue.Image,
                        LocalizedTitle = cartItemVariant.VariantOptionValue.GetLocalizedTitle(),
                        LocalizedDescription = cartItemVariant.VariantOptionValue.GetLocalizedDescription(),
                        LocalizedPriceModifier = cartItemVariant.VariantOptionValue.GetLocalizedPriceModifier()
                    },
                    BooleanValue = cartItemVariant.BooleanValue,
                    DoubleValue = cartItemVariant.DoubleValue,
                    IntegerValue = cartItemVariant.IntegerValue,
                    StringValue = cartItemVariant.StringValue,
                    JsonValue = cartItemVariant.JsonValue
                }).ToList(),
                SubtotalBeforeTax = cartItem.GetSubtotalBeforeTax(dbContext),
                SubtotalAfterTax = cartItem.GetSubtotalAfterTax(dbContext),
                SubtotalBeforeTaxWithoutSalePrice = cartItem.GetSubtotalBeforeTaxWithoutSalePrice(dbContext),
                SubtotalAfterTaxWithoutSalePrice = cartItem.GetSubtotalAfterTaxWithoutSalePrice(dbContext),
                TotalBeforeTax = cartItem.GetTotalBeforeTax(dbContext),
                TotalAfterTax = cartItem.GetTotalAfterTax(dbContext)
            };
        }

        /// <inheritdoc />
        public virtual CartDto GetCartDto(CmsDbContext dbContext)
        {
            var cart = this.GetCart(dbContext);

            return cart == null ? null : new CartDto
            {
                Id = cart.Id,
                CouponValue = cart.GetCouponValue(dbContext),
                ShippingAddress = cart.ShippingAddress == null ? null : new AddressDto
                {
                    Id = cart.ShippingAddress.Id,
                    FirstName = cart.ShippingAddress.FirstName,
                    LastName = cart.ShippingAddress.LastName,
                    AddressLine1 = cart.ShippingAddress.AddressLine1,
                    AddressLine2 = cart.ShippingAddress.AddressLine2,
                    City = cart.ShippingAddress.City,
                    Country = cart.ShippingAddress.Country == null ? null : new CountryDto
                    {
                        Id = cart.ShippingAddress.Country.Id,
                        Name = cart.ShippingAddress.Country.Name,
                        LanguageCode = cart.ShippingAddress.Country.LanguageCode,
                        IsDefault = cart.ShippingAddress.Country.IsDefault,
                        Localize = cart.ShippingAddress.Country.Localize,
                        Url = cart.ShippingAddress.Country.Url,
                        CurrencySymbol = cart.ShippingAddress.Country.GetCurrencySymbol(),
                        Iso4217CurrencySymbol = cart.ShippingAddress.Country.GetIso4217CurrencySymbol(),
                        Taxes = cart.ShippingAddress.Country.Taxes.Select(tax => new TaxDto
                        {
                            Id = tax.Id,
                            Amount = tax.Amount,
                            ApplyToProductPrice = tax.ApplyToProductPrice,
                            ApplyToShippingPrice = tax.ApplyToShippingPrice,
                            CartItem = tax.CartItem == null ? null : new CartItemDto(tax.CartItem.Id),
                            Code = tax.Code,
                            Country = tax.Country == null ? null : new CountryDto
                            {
                                Id = tax.Country.Id,
                                CurrencySymbol = tax.Country.GetCurrencySymbol(),
                                IsDefault = tax.Country.IsDefault,
                                Iso4217CurrencySymbol = tax.Country.GetIso4217CurrencySymbol(),
                                LanguageCode = tax.Country.LanguageCode,
                                Localize = tax.Country.Localize,
                                Name = tax.Country.Name,
                                Url = tax.Country.Url
                            },
                            Percentage = tax.Percentage,
                            Name = tax.Name
                        }).ToList()
                    },
                    StateCountyProvince = cart.ShippingAddress.StateCountyProvince,
                    Postcode = cart.ShippingAddress.Postcode,
                    PhoneNumber = cart.ShippingAddress.PhoneNumber
                },
                BillingAddress = cart.BillingAddress == null ? null : new AddressDto
                {
                    Id = cart.BillingAddress.Id,
                    FirstName = cart.BillingAddress.FirstName,
                    LastName = cart.BillingAddress.LastName,
                    AddressLine1 = cart.BillingAddress.AddressLine1,
                    AddressLine2 = cart.BillingAddress.AddressLine2,
                    City = cart.BillingAddress.City,
                    Country = cart.BillingAddress.Country == null ? null : new CountryDto
                    {
                        Name = cart.BillingAddress.Country.Name,
                        LanguageCode = cart.BillingAddress.Country.LanguageCode,
                        IsDefault = cart.BillingAddress.Country.IsDefault,
                        Localize = cart.BillingAddress.Country.Localize,
                        Url = cart.BillingAddress.Country.Url,
                        CurrencySymbol = cart.BillingAddress.Country.GetCurrencySymbol(),
                        Iso4217CurrencySymbol = cart.BillingAddress.Country.GetIso4217CurrencySymbol(),
                        Taxes = cart.BillingAddress.Country.Taxes.Select(tax => new TaxDto
                        {
                            Id = tax.Id,
                            Amount = tax.Amount,
                            ApplyToProductPrice = tax.ApplyToProductPrice,
                            ApplyToShippingPrice = tax.ApplyToShippingPrice,
                            CartItem = tax.CartItem == null ? null : new CartItemDto(tax.CartItem.Id),
                            Code = tax.Code,
                            Country = tax.Country == null ? null : new CountryDto
                            {
                                Id = tax.Country.Id,
                                CurrencySymbol = tax.Country.GetCurrencySymbol(),
                                IsDefault = tax.Country.IsDefault,
                                Iso4217CurrencySymbol = tax.Country.GetIso4217CurrencySymbol(),
                                LanguageCode = tax.Country.LanguageCode,
                                Localize = tax.Country.Localize,
                                Name = tax.Country.Name,
                                Url = tax.Country.Url
                            },
                            Percentage = tax.Percentage,
                            Name = tax.Name
                        }).ToList()
                    },
                    StateCountyProvince = cart.BillingAddress.StateCountyProvince,
                    Postcode = cart.BillingAddress.Postcode,
                    PhoneNumber = cart.BillingAddress.PhoneNumber
                },
                CartShippingBoxes = cart.CartShippingBoxes.Select(cartShippingBox => new CartShippingBoxDto
                {
                    Id = cartShippingBox.Id,
                    ShippingBox = cartShippingBox.ShippingBox == null ? null : new ShippingBoxDto
                    {
                        Id = cartShippingBox.ShippingBox.Id,
                        Name = cartShippingBox.ShippingBox.Name,
                        RequiresShippingAddress = cartShippingBox.ShippingBox.RequiresShippingAddress,
                        Countries = cartShippingBox.ShippingBox.Countries.Select(country => new CountryDto
                        {
                            Id = country.Id
                        }).ToList(),
                        LocalizedPrice = cartShippingBox.ShippingBox.GetLocalizedPrice(),
                        LocalizedTitle = cartShippingBox.ShippingBox.GetLocalizedTitle(),
                        LocalizedDescription = cartShippingBox.ShippingBox.GetLocalizedDescription(),
                        LocalizedMinDays = cartShippingBox.ShippingBox.GetLocalizedMinDays(),
                        LocalizedMaxDays = cartShippingBox.ShippingBox.GetLocalizedMaxDays(),
                        LocalizedCountWeekends = cartShippingBox.ShippingBox.GetLocalizedCountWeekends(),
                        LocalizedFreeShippingMinimumPrice = cartShippingBox.ShippingBox.GetLocalizedFreeShippingMinimumPrice()
                    },
                    CartItems = cartShippingBox.CartItems.Select(cartItem => this.GetCartItemDto(dbContext, cartItem.Id)).ToList(),
                    ShippingPriceBeforeTax = cartShippingBox.GetShippingPriceBeforeTax(dbContext),
                    ShippingPriceAfterTax = cartShippingBox.GetShippingPriceAfterTax(dbContext),
                    TotalBeforeTax = cartShippingBox.GetTotalBeforeTax(dbContext),
                    TotalAfterTax = cartShippingBox.GetTotalAfterTax(dbContext)
                }).ToList(),
                Coupon = cart.Coupon == null ? null : new CouponDto
                {
                    Id = cart.Coupon.Id,
                    Amount = cart.Coupon.Amount,
                    Percentage = cart.Coupon.Percentage,
                    Code = cart.Coupon.Code,
                    Categories = cart.Coupon.Categories.Select(x => new CategoryDto
                    {
                        Id = x.Id,
                        Type = x.Type
                    }).ToList()
                },
                UserCredit = cart.UserCredit == null ? null : new UserCreditDto
                {
                    Id = cart.UserCredit.Id,
                    Amount = cart.UserCredit.Amount
                },
                Errors = cart.GetErrors(dbContext),
                TotalBeforeTax = cart.GetTotalBeforeTax(dbContext),
                TotalAfterTax = cart.GetTotalAfterTax(dbContext),
                TotalToBePaid = cart.GetTotalToBePaid(dbContext)
            };
        }

        /// <inheritdoc />
        public virtual Cart CreateCart(CmsDbContext dbContext)
        {
            // -------------------------------------------------------------------------------
            // Check if a Cart already exists
            // -------------------------------------------------------------------------------

            var user =
                this.authenticationService.GetAuthenticatedUser(dbContext) ??
                this.authenticationService.GetPartiallyAuthenticatedUser(dbContext);

            if (user == null)
            {
                user = new User
                {
                    Country = this.countryService.GetCurrentCountry(dbContext),
                    Role = dbContext.UserRoles.FirstOrDefault(x => x.Name == SharedConstants.DefaultUserRole)
                };

                dbContext.Users.Add(user);

                dbContext.SaveChanges();

                this.authenticationService.PartiallyAuthenticateUser(user);
            }

            if (user.Cart != null)
            {
                return user.Cart;
            }

            // -------------------------------------------------------------------------------
            // Create the Cart
            // -------------------------------------------------------------------------------

            var cart = new Cart
            {
                User = user
            };

            dbContext.Carts.Add(cart);

            dbContext.SaveChanges();

            return cart;
        }

        /// <inheritdoc />
        public virtual void DeleteCart(CmsDbContext dbContext, Guid? cartId = null)
        {
            cartId = cartId ?? this.GetCart(dbContext)?.Id;

            dbContext.Carts.Delete(cartId, true);
        }

        /// <inheritdoc />
        public virtual CartShippingBox GetCartShippingBox(CmsDbContext dbContext, Guid? shippingBoxId, Cart cart = null)
        {
            if (shippingBoxId == null)
            {
                return null;
            }

            cart = cart ?? this.GetCart(dbContext);

            return cart?.CartShippingBoxes.FirstOrDefault(x => x.ShippingBox?.Id == shippingBoxId);
        }

        /// <inheritdoc />
        public virtual CartShippingBox CreateCartShippingBox(CmsDbContext dbContext, Guid? shippingBoxId, Cart cart = null)
        {
            if (shippingBoxId == null)
            {
                return null;
            }

            // -------------------------------------------------------------------------------
            // Check if a CartShippingBox with the same ShippingBox.Id already exists
            // -------------------------------------------------------------------------------

            var existingCartShippingBox = this.GetCartShippingBox(dbContext, shippingBoxId, cart);

            if (existingCartShippingBox != null)
            {
                return existingCartShippingBox;
            }

            // -------------------------------------------------------------------------------
            // Create the CartShippingBox
            // -------------------------------------------------------------------------------

            var shippingBox = dbContext.ShippingBoxes.Find(shippingBoxId);

            cart = cart ?? this.GetCart(dbContext) ?? this.CreateCart(dbContext);

            var cartShippingBox = new CartShippingBox
            {
                Cart = cart,
                ShippingBox = shippingBox ?? throw new NullReferenceException("ShippingBox was null")
            };

            dbContext.CartShippingBoxes.Add(cartShippingBox);

            dbContext.SaveChanges();

            return cartShippingBox;
        }

        /// <inheritdoc />
        public virtual void SetShippingBox(CmsDbContext dbContext, Guid cartItemId, string shippingBoxName, Cart cart = null)
        {
            var cartItem = dbContext.CartItems.Find(cartItemId);

            if (cartItem == null)
            {
                return;
            }

            var shippingBox = dbContext.ShippingBoxes.FirstOrDefault(x => x.Name == shippingBoxName);

            if (shippingBox == null)
            {
                return;
            }

            // leave this here, it must be declared before cartItem is modified

            var oldCartShippingBox = cartItem.CartShippingBox;

            // ------------------------------------------------------------
            // Move CartItem
            // ------------------------------------------------------------

            var cartShippingBox = this.GetCartShippingBox(dbContext, shippingBox.Id, cart) ?? this.CreateCartShippingBox(dbContext, shippingBox.Id, cart);

            cartItem.CartShippingBox = cartShippingBox;

            dbContext.SaveChanges();

            // ------------------------------------------------------------
            // Delete old CartShippingBox if empty
            // ------------------------------------------------------------

            if (!oldCartShippingBox?.CartItems.Any() ?? false)
            {
                dbContext.CartShippingBoxes.Delete(oldCartShippingBox.Id, true);
            }
        }

        /// <inheritdoc />
        public virtual void AddToCart(CmsDbContext dbContext, CartItem cartItem)
        {
            var shippingBoxId = cartItem.CartShippingBox?.ShippingBox?.Id;

            var product = dbContext.Products.Find(cartItem.Product?.Id);

            if (product == null)
            {
                throw new NullReferenceException("Product not found");
            }

            if (shippingBoxId == null)
            {
                var shippingCountryOrCurrentCountry = this.countryService.GetShippingCountryOrCurrentCountry(dbContext, cartItem.CartShippingBox?.Cart);

                var shippingBox = product.GetShippingBoxesForShippingCountry(shippingCountryOrCurrentCountry).FirstOrDefault();

                if (shippingBox == null)
                {
                    this.logService.Log(dbContext, LogTypesEnum.Ecommerce, $"No available ShippingBox was found for the Product '{product.GetLocalizedTitle()}' in {shippingCountryOrCurrentCountry.Name}");

                    return;
                }

                shippingBoxId = shippingBox.Id;
            }

            cartItem.Product = product;

            foreach (var cartItemVariant in cartItem.CartItemVariants.ToList())
            {
                cartItemVariant.CartItem = cartItem;
                cartItemVariant.Variant = dbContext.Variants.Find(cartItemVariant.Variant?.Id);
                cartItemVariant.VariantOptionValue = dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id);
            }

            cartItem.CartShippingBox =
                this.GetCartShippingBox(dbContext, shippingBoxId) ??
                this.CreateCartShippingBox(dbContext, shippingBoxId);

            dbContext.CartItems.Add(cartItem);

            dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public virtual void UpdateCartItem(CmsDbContext dbContext, CartItem cartItem)
        {
            var shippingBoxId = cartItem.CartShippingBox?.ShippingBox?.Id;

            if (shippingBoxId == null)
            {
                throw new NullReferenceException("The ShippingBox was null");
            }

            cartItem.CartShippingBox = this.GetCartShippingBox(dbContext, shippingBoxId) ?? this.CreateCartShippingBox(dbContext, shippingBoxId);

            var dbCartItem = dbContext.CartItems.Find(cartItem.Id);

            if (dbCartItem == null)
            {
                throw new NullReferenceException("The cartItem was null");
            }

            dbCartItem.CartShippingBox = this.GetCartShippingBox(dbContext, shippingBoxId) ?? this.CreateCartShippingBox(dbContext, shippingBoxId);

            foreach (var cartItemVariant in cartItem.CartItemVariants.ToList())
            {
                var dbCartItemVariant = dbCartItem.CartItemVariants.FirstOrDefault(x => x.Id == cartItemVariant.Id);

                if (dbCartItemVariant == null)
                {
                    dbCartItemVariant = new CartItemVariant
                    {
                        CartItem = dbCartItem,
                        BooleanValue = cartItemVariant.BooleanValue,
                        DoubleValue = cartItemVariant.DoubleValue,
                        IntegerValue = cartItemVariant.IntegerValue,
                        JsonValue = cartItemVariant.JsonValue,
                        StringValue = cartItemVariant.StringValue,
                        Variant = dbContext.Variants.Find(cartItemVariant.Variant?.Id),
                        VariantOptionValue = dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id)
                    };

                    cartItem.CartItemVariants.Add(dbCartItemVariant);
                }
                else
                {
                    dbCartItemVariant.BooleanValue = cartItemVariant.BooleanValue;
                    dbCartItemVariant.DoubleValue = cartItemVariant.DoubleValue;
                    dbCartItemVariant.IntegerValue = cartItemVariant.IntegerValue;
                    dbCartItemVariant.JsonValue = cartItemVariant.JsonValue;
                    dbCartItemVariant.StringValue = cartItemVariant.StringValue;
                    dbCartItemVariant.Variant = dbContext.Variants.Find(cartItemVariant.Variant?.Id);
                    dbCartItemVariant.VariantOptionValue = dbContext.VariantOptions.Find(cartItemVariant.VariantOptionValue?.Id);
                }
            }

            cartItem.TaxesOverride.Clear();
            cartItem.TaxesOverride.AddRange(cartItem.TaxesOverride);

            dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public virtual void UpdateCartItemQuantity(CmsDbContext dbContext, Guid cartItemId, int quantity)
        {
            var cartItem = dbContext.CartItems.Find(cartItemId);

            if (cartItem == null)
            {
                return;
            }

            cartItem.Quantity = quantity.ToMinimum(1);

            dbContext.SaveChanges();
        }

        /// <inheritdoc />
        public virtual void RemoveFromCart(CmsDbContext dbContext, Guid cartItemId)
        {
            var cartItem = dbContext.CartItems.Find(cartItemId);
            var cartShippingBox = cartItem?.CartShippingBox;

            if (cartShippingBox == null)
            {
                return;
            }

            if (cartShippingBox.CartItems.Count > 1)
            {
                dbContext.CartItems.Delete(cartItemId, true);
            }
            else
            {
                dbContext.CartShippingBoxes.Delete(cartShippingBox.Id, true);
            }
        }

        /// <inheritdoc />
        public virtual FormResponse<Address> AddOrUpdateAddress(CmsDbContext dbContext, Address address, bool setUser = false, bool setAsCartShippingAddress = false, bool setAsCartBillingAddress = false, Cart cart = null)
        {
            var formResponse = new FormResponse<Address>();

            var validationResult = this.addOrUpdateAddressValidator.Validate(address);

            if (validationResult.IsValid)
            {
                var dbAdress = dbContext.Addresses.Find(address.Id);

                if (dbAdress == null)
                {
                    address.User = setUser ? this.authenticationService.GetAuthenticatedUser(dbContext) ??
                                             this.authenticationService.GetPartiallyAuthenticatedUser(dbContext) : null;
                    address.Country = dbContext.Countries.Find(address.Country?.Id);

                    dbContext.Addresses.Add(address);

                    formResponse.Target = address;
                }
                else
                {
                    dbAdress.User = setUser ? this.authenticationService.GetAuthenticatedUser(dbContext) ??
                                              this.authenticationService.GetPartiallyAuthenticatedUser(dbContext) : null;
                    dbAdress.Country = dbContext.Countries.Find(address.Country?.Id);
                    dbAdress.AddressLine1 = address.AddressLine1;
                    dbAdress.AddressLine2 = address.AddressLine2;
                    dbAdress.City = address.City;
                    dbAdress.FirstName = address.FirstName;
                    dbAdress.LastName = address.LastName;
                    dbAdress.PhoneNumber = address.PhoneNumber;
                    dbAdress.Postcode = address.Postcode;
                    dbAdress.StateCountyProvince = address.StateCountyProvince;

                    formResponse.Target = dbAdress;
                }

                dbContext.SaveChanges();

                if (setAsCartShippingAddress || setAsCartBillingAddress)
                {
                    cart = cart ?? this.GetCart(dbContext) ?? this.CreateCart(dbContext);

                    if (setAsCartShippingAddress)
                    {
                        var setCartShippingAddressFormResponse = this.SetCartShippingAddress(dbContext, address.Id, cart);

                        formResponse.Errors.AddRange(setCartShippingAddressFormResponse.Errors);
                    }

                    if (setAsCartBillingAddress)
                    {
                        var setCartBillingAddressFormResponse = this.SetCartBillingAddress(dbContext, address.Id, cart);

                        formResponse.Errors.AddRange(setCartBillingAddressFormResponse.Errors);
                    }
                }
            }
            else
            {
                var validationResultErrors = this.utilityService.GetFormResponse(validationResult).Errors;

                formResponse.Errors.AddRange(validationResultErrors);
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse SetCartShippingAddress(CmsDbContext dbContext, Guid? addressId, Cart cart = null)
        {
            var formResponse = new FormResponse();

            cart = cart ?? this.GetCart(dbContext) ?? this.CreateCart(dbContext);

            if (cart == null)
            {
                return formResponse;
            }

            if (addressId == null)
            {
                if (cart.ShippingAddress != null)
                {
                    cart.ShippingAddress = null;

                    dbContext.SaveChanges();
                }
            }
            else
            {
                cart.ShippingAddress = dbContext.Addresses.Find(addressId);

                dbContext.SaveChanges();
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse SetCartBillingAddress(CmsDbContext dbContext, Guid? addressId, Cart cart = null)
        {
            var formResponse = new FormResponse();

            cart = cart ?? this.GetCart(dbContext) ?? this.CreateCart(dbContext);

            if (cart == null)
            {
                return formResponse;
            }

            if (addressId == null)
            {
                if (cart.BillingAddress != null)
                {
                    cart.BillingAddress = null;

                    dbContext.SaveChanges();
                }
            }
            else
            {
                cart.BillingAddress = dbContext.Addresses.Find(addressId);

                dbContext.SaveChanges();
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse<double> ApplyUserCreditToCart(CmsDbContext dbContext, double amount, string reference = null)
        {
            var cart = this.GetCart(dbContext) ?? this.CreateCart(dbContext);

            var formResponse = new FormResponse<double>();

            // ----------------------------------------------------------------------
            // Invalid amount
            // ----------------------------------------------------------------------

            if (amount.ApproximatelyEqualsTo(0))
            {
                var errorMessage = this.contentSectionService.ContentSection(
                    dbContext,
                    ContentSectionName.Cms_UserCredit_InvalidAmountError,
                    ContentSectionName.Cms_UserCredit_InvalidAmountError_FallbackValue, true);

                formResponse.Errors.Add(new FormError(errorMessage));
            }

            // ----------------------------------------------------------------------
            // Insufficient Funds
            // ----------------------------------------------------------------------

            var creditsTotal = cart.User.GetLocalizedUserCredits().Sum(x => x.Amount);

            if (amount > creditsTotal)
            {
                var errorMessage = this.contentSectionService.ContentSection(
                    dbContext,
                    ContentSectionName.Cms_UserCredit_InsufficientFundsError,
                    ContentSectionName.Cms_UserCredit_InsufficientFundsError_FallbackValue, true);

                formResponse.Errors.Add(new FormError(errorMessage));
            }

            // ----------------------------------------------------------------------
            // Excessive amount
            // ----------------------------------------------------------------------

            if (amount > cart.GetTotalAfterTax(dbContext))
            {
                var errorMessage = this.contentSectionService.ContentSection(
                    dbContext,
                    ContentSectionName.Cms_UserCredit_ExcessiveAmountError,
                    ContentSectionName.Cms_UserCredit_ExcessiveAmountError_FallbackValue, true);

                formResponse.Errors.Add(new FormError(errorMessage));
            }

            // ----------------------------------------------------------------------
            // Return errors
            // ----------------------------------------------------------------------

            if (!formResponse.IsValid)
            {
                return formResponse;
            }

            // ----------------------------------------------------------------------
            // Apply credit
            // ----------------------------------------------------------------------

            if (cart.UserCredit != null)
            {
                this.RemoveCreditFromCart(dbContext);
            }

            var userLocalizedKit = cart.User?.UserLocalizedKits.GetByLanguageCode();

            if (userLocalizedKit == null)
            {
                userLocalizedKit = new UserLocalizedKit
                {
                    User = cart.User,
                    Country = this.countryService.GetCurrentCountry(dbContext)
                };

                dbContext.UserLocalizedKits.Add(userLocalizedKit);
            }

            var userCredit = new UserCredit
            {
                Amount = -amount,
                Reference = reference,
                UserLocalizedKit = userLocalizedKit
            };

            dbContext.UserCredits.Add(userCredit);

            cart.UserCredit = userCredit;

            dbContext.SaveChanges();

            formResponse.Target = creditsTotal + userCredit.Amount;

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse<double> RemoveCreditFromCart(CmsDbContext dbContext, Cart cart = null)
        {
            var formResponse = new FormResponse<double>();

            cart = cart ?? this.GetCart(dbContext, true);

            if (cart?.UserCredit == null)
            {
                return formResponse;
            }

            dbContext.UserCredits.Delete(cart.UserCredit.Id, true);

            var creditsTotal = cart.User.GetLocalizedUserCredits().Sum(x => x.Amount);

            formResponse.Target = creditsTotal;

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse ApplyCouponToCart(CmsDbContext dbContext, string couponCode)
        {
            var dbCoupon = dbContext.Coupons.FirstOrDefault(x => x.Code == couponCode);

            var validationResult = this.applyCouponToCartValidator.Validate(dbCoupon);

            if (!validationResult.IsValid)
            {
                return this.utilityService.GetFormResponse(validationResult);
            }

            if (dbCoupon == null)
            {
                throw new NullReferenceException();
            }

            var cart = this.GetCart(dbContext) ?? this.CreateCart(dbContext);

            dbCoupon.ValidityTimes -= 1;

            cart.Coupon = dbCoupon;

            dbContext.SaveChanges();

            return this.utilityService.GetFormResponse(validationResult);
        }

        /// <inheritdoc />
        public virtual void RemoveCouponFromCart(CmsDbContext dbContext, Cart cart = null)
        {
            cart = cart ?? this.GetCart(dbContext);

            var coupon = cart?.Coupon;

            if (coupon == null)
            {
                return;
            }

            coupon.ValidityTimes = coupon.ValidityTimes + 1;

            cart.Coupon = null;

            dbContext.SaveChanges();
        }

        /// <inheritdoc />
        [NotNull]
        public virtual Order CreateOrder(CmsDbContext dbContext, Cart cart = null, string transactionId = null)
        {
            cart = cart ?? this.GetCart(dbContext);

            if (cart == null)
            {
                throw new NullReferenceException("Impossible to create the order: the Cart was null.");
            }

            var currentCountry = this.countryService.GetCurrentCountry(dbContext);

            var order = new Order
            {
                OrderNumber = (dbContext.Orders.Max(x => x.OrderNumber) ?? 0) + 1,
                CountryLanguageCode = currentCountry.LanguageCode,
                CountryName = currentCountry.Name,
                CurrencySymbol = currentCountry.GetCurrencySymbol(),
                ISO4217CurrencySymbol = currentCountry.GetIso4217CurrencySymbol(),
                TransactionId = transactionId,
                TotalBeforeTax = cart.GetTotalBeforeTax(dbContext),
                TotalAfterTax = cart.GetTotalAfterTax(dbContext),
                TotalToBePaid = cart.GetTotalToBePaid(dbContext),
                Status = OrderStatusEnum.Placed
            };

            // --------------------------------------------------------
            // COUPON
            // --------------------------------------------------------

            var coupon = cart.Coupon;

            if (coupon != null)
            {
                var couponAppliedValue = cart.CartShippingBoxes.SelectMany(x => x.CartItems).Sum(x => x.GetCouponShareValue(dbContext));

                if (coupon.Amount > couponAppliedValue)
                {
                    coupon.ValidityTimes += 1;
                    coupon.Amount -= couponAppliedValue;
                }

                order.OrderCoupon = new OrderCoupon
                {
                    Amount = cart.GetCouponValue(dbContext),
                    Code = coupon.Code
                };
            }

            // --------------------------------------------------------
            // CREDIT
            // --------------------------------------------------------

            if (cart.UserCredit != null)
            {
                order.OrderCredit = new OrderCredit
                {
                    Amount = cart.UserCredit.Amount,
                    Reference = cart.UserCredit.Reference
                };
            }

            // --------------------------------------------------------
            // USER
            // --------------------------------------------------------

            if (cart.User != null)
            {
                order.UserId = cart.User.Id;
                order.UserEmail = cart.User.Email;
                order.UserFirstName = cart.User.FirstName;
                order.UserLastName = cart.User.LastName;
            }

            // --------------------------------------------------------
            // SHIPPING ADDRESS
            // --------------------------------------------------------

            if (cart.ShippingAddress != null)
            {
                order.ShippingAddress = new OrderAddress
                {
                    AddressFirstName = cart.ShippingAddress.FirstName,
                    AddressLastName = cart.ShippingAddress.LastName,
                    AddressLine1 = cart.ShippingAddress.AddressLine1,
                    AddressLine2 = cart.ShippingAddress.AddressLine2,
                    City = cart.ShippingAddress.City,
                    Postcode = cart.ShippingAddress.Postcode,
                    StateCountyProvince = cart.ShippingAddress.StateCountyProvince,
                    CountryName = cart.ShippingAddress.Country?.Name,
                    PhoneNumber = cart.ShippingAddress.PhoneNumber
                };
            }

            // --------------------------------------------------------
            // BILLING ADDRESS
            // --------------------------------------------------------

            if (cart.BillingAddress != null && cart.BillingAddress.Id != cart.ShippingAddress?.Id)
            {
                order.BillingAddress = new OrderAddress
                {
                    AddressFirstName = cart.BillingAddress.FirstName,
                    AddressLastName = cart.BillingAddress.LastName,
                    AddressLine1 = cart.BillingAddress.AddressLine1,
                    AddressLine2 = cart.BillingAddress.AddressLine2,
                    City = cart.BillingAddress.City,
                    Postcode = cart.BillingAddress.Postcode,
                    StateCountyProvince = cart.BillingAddress.StateCountyProvince,
                    CountryName = cart.BillingAddress?.Country?.Name,
                    PhoneNumber = cart.BillingAddress.PhoneNumber
                };
            }

            // --------------------------------------------------------
            // SHIPPING BOXES
            // --------------------------------------------------------

            foreach (var cartShippingBox in cart.CartShippingBoxes.Where(x => x.CartItems.Any()))
            {
                var orderShippingBox = new OrderShippingBox
                {
                    Order = order,
                    Name = cartShippingBox.ShippingBox?.Name,
                    Title = cartShippingBox.ShippingBox?.GetLocalizedTitle(),
                    Description = cartShippingBox.ShippingBox?.GetLocalizedDescription(),
                    InternalDescription = cartShippingBox.ShippingBox?.GetLocalizedInternalDescription(),
                    MinDays = cartShippingBox.ShippingBox?.GetLocalizedMinDays() ?? 0,
                    MaxDays = cartShippingBox.ShippingBox?.GetLocalizedMaxDays() ?? 0,
                    CountWeekends = cartShippingBox.ShippingBox?.GetLocalizedCountWeekends() ?? false,
                    ShippingPriceBeforeTax = cartShippingBox.GetShippingPriceBeforeTax(dbContext),
                    ShippingPriceAfterTax = cartShippingBox.GetShippingPriceAfterTax(dbContext),
                    Status = OrderShippingBoxStatusEnum.Preparing
                };

                // --------------------------------------------------------
                // ORDER ITEMS
                // --------------------------------------------------------

                foreach (var cartItem in cartShippingBox.CartItems)
                {
                    var product = cartItem.Product.AsNotNull();
                    var productStockUnit = cartItem.GetProductStockUnit().AsNotNull();

                    var orderItem = new OrderItem
                    {
                        OrderShippingBox = orderShippingBox,
                        ProductId = product.Id,
                        ProductName = product.Name,
                        ProductUrl = product.Url,
                        ProductCode = product.Code,
                        ProductQuantity = cartItem.Quantity,
                        ProductTitle = product.GetLocalizedTitle(),
                        ProductStockUnitId = productStockUnit.Id,
                        ProductStockUnitCode = productStockUnit.Code,
                        ProductStockUnitReleaseDate = productStockUnit.DispatchDate,
                        ProductStockUnitEnablePreorder = productStockUnit.EnablePreorder,
                        ProductStockUnitShipsIn = productStockUnit.DispatchTime,
                        ProductStockUnitBasePrice = productStockUnit.GetLocalizedBasePrice(),
                        ProductStockUnitSalePrice = productStockUnit.GetLocalizedSalePrice(),
                        ProductStockUnitMembershipSalePrice = productStockUnit.GetLocalizedMembershipSalePrice(),
                        Image = cartItem.Image,
                        Status = OrderItemStatusEnum.Default,
                        MinEta = cartItem.GetMinEta(),
                        MaxEta = cartItem.GetMaxEta(),
                        SubtotalBeforeTax = cartItem.GetSubtotalBeforeTax(dbContext),
                        SubtotalAfterTax = cartItem.GetSubtotalAfterTax(dbContext),
                        TotalBeforeTax = cartItem.GetTotalBeforeTax(dbContext),
                        TotalAfterTax = cartItem.GetTotalAfterTax(dbContext),
                    };

                    foreach (var cartItemVariant in cartItem.CartItemVariants)
                    {
                        var orderItemProductVariant = new OrderItemVariant
                        {
                            OrderItem = orderItem,

                            VariantName = cartItemVariant.Variant?.Name,
                            VariantUrl = cartItemVariant.Variant?.Url,
                            VariantType = cartItemVariant.Variant?.Type ?? new VariantTypesEnum(),
                            VariantLocalizedTitle = cartItemVariant.Variant?.GetLocalizedTitle(),
                            VariantLocalizedDescription = cartItemVariant.Variant?.GetLocalizedDescription(),

                            VariantOptionName = cartItemVariant.VariantOptionValue?.Name,
                            VariantOptionUrl = cartItemVariant.VariantOptionValue?.Url,
                            VariantOptionCode = cartItemVariant.VariantOptionValue?.Code,
                            VariantOptionLocalizedTitle = cartItemVariant.VariantOptionValue?.GetLocalizedTitle(),
                            VariantOptionLocalizedDescription = cartItemVariant.VariantOptionValue?.GetLocalizedDescription(),
                            VariantOptionLocalizedPriceModifier = cartItemVariant.VariantOptionValue?.GetLocalizedPriceModifier() ?? 0,

                            BooleanValue = cartItemVariant.BooleanValue,
                            DoubleValue = cartItemVariant.DoubleValue,
                            IntegerValue = cartItemVariant.IntegerValue,
                            StringValue = cartItemVariant.StringValue,
                            JsonValue = cartItemVariant.JsonValue
                        };

                        orderItem.OrderItemVariants.Add(orderItemProductVariant);
                    }

                    orderShippingBox.OrderItems.Add(orderItem);
                }

                order.OrderShippingBoxes.Add(orderShippingBox);
            }

            order.AutoTags = this.autoTagService.GetAutoTags(order);

            dbContext.Orders.Add(order);

            // -----------------------------------------------------------
            // Decrease product stock
            // -----------------------------------------------------------

            foreach (var cartItem in cart.CartShippingBoxes.SelectMany(x => x.CartItems))
            {
                var productStockUnit = cartItem.GetProductStockUnit();

                if (productStockUnit == null)
                {
                    continue;
                }

                productStockUnit.Stock = productStockUnit.Stock - 1;
            }

            dbContext.SaveChanges();

            return order;
        }
    }
}
