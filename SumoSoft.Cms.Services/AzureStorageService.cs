﻿namespace SumoSoft.Cms.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using JetBrains.Annotations;
    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Blob;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Services.Dto;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <inheritdoc />
    public class AzureStorageService : IAzureStorageService
    {
        private readonly IBitmapService bitmapService;
        private readonly IUtilityService utilityService;

        /// <inheritdoc />
        public AzureStorageService(
            IBitmapService bitmapService,
            IUtilityService utilityService)
        {
            this.bitmapService = bitmapService;
            this.utilityService = utilityService;
        }

        // ------------------------------------------------------------------------------------------------------------
        // NOTES:
        //
        // blob.Uri.LocalPath (directory) => /container/folder/
        // blob.Uri.LocalPath (file)      => /container/folder/file.png
        //
        // blob.Name (directory)          => folder
        // blob.Name (file)               => folder/file.png
        //
        // ------------------------------------------------------------------------------------------------------------

        /// <inheritdoc />
        [CanBeNull]
        public virtual FileDto GetFileDto(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The 'localPath' parameter cannot be null or empty.");
            }

            var blob = this.GetCloudBlockBlob(localPath);

            if (blob.Exists())
            {
                return this.CloudBlockBlobToFileDto(blob);
            }

            return null;
        }

        /// <inheritdoc />
        public virtual DirectoryDto GetDirectoryDto(string localPath = "", string keywordFilter = "")
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The 'localPath' parameter cannot be null or empty.");
            }

            keywordFilter = keywordFilter?.Trim();

            var files = new List<FileDto>();

            var directories = new List<DirectoryDto>();

            if (string.IsNullOrEmpty(keywordFilter))
            {
                var blobs = this.GetCloudBlobDirectory(localPath).ListBlobs();

                foreach (var item in blobs.Where(x => !x.Uri.LocalPath.EndsWith(".info")))
                {
                    if (item is CloudBlockBlob)
                    {
                        var newFile = this.CloudBlockBlobToFileDto(item as CloudBlockBlob);
                        newFile.CreationTime = newFile.CreationTime;

                        files.Add(newFile);
                    }
                    else if (item is CloudBlobDirectory)
                    {
                        var newDirectory = this.CloudBlobDirectoryToDirectoryDto(item as CloudBlobDirectory);
                        newDirectory.CreationTime = newDirectory.CreationTime;

                        directories.Add(newDirectory);
                    }
                }
            }
            else
            {
                this.RecursiveSearch(localPath, keywordFilter, files, directories);
            }

            return new DirectoryDto
            {
                UseAzureStorage = true,
                Name = GetDirectoryName(localPath),
                LocalPath = localPath,
                Size = 0,
                CreationTime = DateTime.Now,
                Files = files,
                Directories = directories,
                Parent = new DirectoryDto
                {
                    LocalPath = GetDirectoryParentLocalPath(localPath)
                },
                IsRoot = localPath.TrimEnd('/').EndsWith(this.utilityService.GetAppSetting("AzureStorageContainer"))
            };
        }

        /// <inheritdoc />
        public virtual CloudBlockBlob GetCloudBlockBlob(string connectionString, string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The 'localPath' parameter cannot be null or empty.");
            }

            var containerName = GetContainerName(localPath);

            var container = this.GetBlobContainer(connectionString, containerName);

            var blobName = GetBlobName(localPath);

            return container.GetBlockBlobReference(blobName);
        }

        /// <inheritdoc />
        public virtual CloudBlockBlob GetCloudBlockBlob(string localPath)
        {
            var connectionString = this.utilityService.GetAppSetting("AzureStorageConnectionString");

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("'AzureStorageConnectionString' setting not found in Web.config.");
            }

            return this.GetCloudBlockBlob(connectionString, localPath);
        }

        /// <inheritdoc />
        public virtual IEnumerable<CloudBlockBlob> GetCloudBlockBlobs(string connectionString, string localPath)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("The parameter 'connectionString' cannot be null or empty.");
            }

            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The parameter 'localPath' cannot be null or empty.");
            }

            var result = new List<CloudBlockBlob>();

            var directory = this.GetCloudBlobDirectory(connectionString, localPath);

            var blobs = directory.ListBlobs();

            foreach (var blob in blobs)
            {
                switch (blob)
                {
                    case CloudBlockBlob item:
                        result.Add(item);
                        break;
                    case CloudBlobDirectory _:
                        result.AddRange(this.GetCloudBlockBlobs(connectionString, blob.Uri.LocalPath));
                        break;
                }
            }

            return result;
        }

        /// <inheritdoc />
        public virtual IEnumerable<CloudBlockBlob> GetCloudBlockBlobs(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The 'localPath' parameter cannot be null or empty.");
            }

            var connectionString = this.utilityService.GetAppSetting("AzureStorageConnectionString");

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("'AzureStorageConnectionString' setting not found in Web.config.");
            }

            return this.GetCloudBlockBlobs(connectionString, localPath);
        }

        /// <inheritdoc />
        public virtual CloudBlobDirectory GetCloudBlobDirectory(string connectionString, string localPath)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("The parameter 'connectionString' cannot be null or empty.");
            }

            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The parameter 'localPath' cannot be null or empty.");
            }

            var containerName = GetContainerName(localPath);

            var container = this.GetBlobContainer(connectionString, containerName);

            var blobName = GetBlobName(localPath);

            var cloudBlobDirectory = container.GetDirectoryReference(blobName);

            return cloudBlobDirectory;
        }

        /// <inheritdoc />
        public virtual CloudBlobDirectory GetCloudBlobDirectory(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The parameter 'localPath' cannot be null or empty.");
            }

            var connectionString = this.utilityService.GetAppSetting("AzureStorageConnectionString");

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("'AzureStorageConnectionString' setting not found in Web.config.");
            }

            return this.GetCloudBlobDirectory(connectionString, localPath);
        }

        /// <inheritdoc />
        public virtual void BackupDirectory(string localPath)
        {
            // --------------------------------------------------------------------
            // Source storage account
            // --------------------------------------------------------------------

            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The parameter 'localPath' cannot be null or empty.");
            }

            var connectionString = this.utilityService.GetAppSetting("AzureStorageConnectionString");

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("'AzureStorageConnectionString' setting not found in Web.config.");
            }

            // --------------------------------------------------------------------
            // Destination storage account
            // --------------------------------------------------------------------

            var backupConnectionString = this.utilityService.GetAppSetting("AzureStorageBackupConnectionString");

            if (string.IsNullOrEmpty(backupConnectionString))
            {
                throw new NullReferenceException("'AzureStorageBackupConnectionString' setting not found in Web.config.");
            }

            var backupContainerName = this.utilityService.GetAppSetting("AzureStorageBackupContainer");

            if (string.IsNullOrEmpty(backupContainerName))
            {
                throw new NullReferenceException("'AzureStorageBackupContainer' setting not found in Web.config.");
            }

            // --------------------------------------------------------------------
            // Copy blobs
            // --------------------------------------------------------------------

            var blobs = this.GetCloudBlockBlobs(connectionString, localPath);

            var containerName = GetContainerName(localPath);

            var backupTargetFolder = $"{containerName}/{backupContainerName}/{DateTime.Now:dd-MM-yyyy-hh-mm}";

            foreach (var blob in blobs)
            {
                var blobLocalPath = blob.Uri.LocalPath;

                var targetBlobLocalPath = blobLocalPath.ReplaceFirst(containerName, backupTargetFolder);

                var targetBlob = this.GetCloudBlockBlob(backupConnectionString, targetBlobLocalPath);

                targetBlob.StartCopy(blob);
            }
        }

        /// <inheritdoc />
        public virtual FormResponse RenameFile(string localPath, string newFileName)
        {
            var formResponse = new FormResponse();

            using (var stream = new MemoryStream())
            {
                try
                {
                    var blob = this.GetCloudBlockBlob(localPath);

                    blob.DownloadToStream(stream);

                    stream.Seek(0, SeekOrigin.Begin);

                    var errors = this.UploadFile(GetDirectoryParentLocalPath(localPath), stream, newFileName + Path.GetExtension(localPath)).Errors;

                    formResponse.Errors.AddRange(errors);

                    blob.Delete();
                }
                catch (Exception e)
                {
                    //formResponse.Errors.Add(e.Message);
                    formResponse.Errors.Add(new FormError(e.Message));
                }

            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse DeleteFile(string localPath)
        {
            var formResponse = new FormResponse();

            try
            {
                var blob = this.GetCloudBlockBlob(localPath);
                var result = blob.DeleteIfExists();
                if (!result)
                {
                    formResponse.Errors.Add(new FormError($"File: {localPath} could not be deleted"));
                }
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse CreateDirectory(string currentDirectory, string newDirectoryName)
        {
            var formResponse = new FormResponse();

            try
            {
                var localPath = currentDirectory + newDirectoryName + "/Blob.info";

                var blob = this.GetCloudBlockBlob(localPath);

                blob.UploadText(DateTime.Now.ToString("yy-mm-dd"));
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse DeleteDirectory(string localPath)
        {
            var formResponse = new FormResponse();

            try
            {
                var directory = this.GetCloudBlobDirectory(localPath);

                if (directory == null)
                {
                    formResponse.Errors.Add(new FormError($"Directory for localPath: {localPath} could not be found"));

                    return formResponse;
                }

                var listBlobs = directory.ListBlobs();

                foreach (var item in listBlobs)
                {
                    if (item is CloudBlockBlob blob)
                    {
                        blob.Delete();
                    }
                    else if (item is CloudBlobDirectory)
                    {
                        var blobDirectory = item as CloudBlobDirectory;

                        var errors = this.DeleteDirectory(blobDirectory.Uri.LocalPath).Errors;

                        formResponse.Errors.AddRange(errors);
                    }
                }
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse UploadFile(string directoryLocalPath, Stream fileStream, string fileName)
        {
            var formResponse = new FormResponse();

            try
            {
                var fileLocalPath = (directoryLocalPath + "/" + fileName)
                    .Replace("//", "/")
                    .Replace("//", "/");

                var existingBlob = this.GetCloudBlockBlob(fileLocalPath);

                existingBlob?.DeleteIfExists();

                var blob = this.GetCloudBlockBlob(fileLocalPath);

                blob.Properties.ContentType = GetFileContentType(Path.GetExtension(fileName));
                blob.Properties.CacheControl = "public, max-age=864000"; // 10 days

                blob.UploadFromStream(fileStream, AccessCondition.GenerateIfNotExistsCondition());

                fileStream.Seek(0, SeekOrigin.Begin);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse ResizeFile(string localPath, ResizeFileDto model)
        {
            var formResponse = new FormResponse();

            try
            {
                var blob = this.GetCloudBlockBlob(localPath);

                // ------------------------------------------------
                // CREATE MEMORY STREAM
                // ------------------------------------------------

                var memoryStream = new MemoryStream();

                blob.DownloadToStream(memoryStream);

                memoryStream.Seek(0, SeekOrigin.Begin);

                // ------------------------------------------------
                // SET NEW IMAGE VALUES
                // ------------------------------------------------

                string newLocalPath;

                if (model.KeepOriginal)
                {
                    newLocalPath = $"{model.Destination}{Path.GetFileNameWithoutExtension(blob.Uri.LocalPath)}_{model.Width}{Path.GetExtension(blob.Uri.LocalPath)}";
                }
                else
                {
                    newLocalPath = model.Destination + Path.GetFileName(blob.Uri.LocalPath);
                }

                var newImage = this.bitmapService.ResizeImage(model.Compression, memoryStream, model.Width);

                if (!model.KeepOriginal)
                {
                    this.DeleteFile(localPath);
                }

                // ------------------------------------------------
                // UPLOAD NEW IMAGE
                // ------------------------------------------------

                var newBlob = this.GetCloudBlockBlob(newLocalPath);

                newBlob.Properties.ContentType = GetFileContentType(Path.GetExtension(newLocalPath));

                newBlob.UploadFromStream(newImage);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        // ---------------------------------------------------------------------------------------------------------
        // PRIVATE AND INTERNAL METHODS
        // ---------------------------------------------------------------------------------------------------------

        internal static string GetContainerName(string localPath)
        {
            if (string.IsNullOrEmpty(localPath))
            {
                throw new NullReferenceException("The parameter 'localPath' in AzureStorageMethods cannot be null or empty.");
            }

            if (!localPath.StartsWith("/"))
            {
                throw new Exception("The parameter 'localPath' in AzureStorageMethods must start with a slash character, " +
                                    "followed by the name of the container. For example: '/my-container/my-folder/'");
            }

            var containerName = localPath.Trim('/').SubstringUpToFirst("/");

            if (string.IsNullOrEmpty(containerName))
            {
                throw new Exception("The parameter 'localPath' must include the name of the container.");
            }

            return containerName;
        }

        /// <summary>
        /// The Blob Name does not include the Container name.
        /// </summary>
        internal static string GetBlobName(string localPath)
        {
            var containerName = GetContainerName(localPath);

            return localPath
                .ReplaceFirst(containerName, "")
                .Replace("//", "/")
                .Trim('/')
                .Replace("%20", " ");
        }

        internal static string GetDirectoryName(string localPath)
        {
            return localPath.TrimEnd('/').Split('/').LastOrDefault();
        }

        internal static string GetDirectoryParentLocalPath(string localPath)
        {
            var directoryName = GetDirectoryName(localPath);

            return localPath.Substring(0, localPath.LastIndexOf(directoryName, StringComparison.Ordinal));
        }

        private FileDto CloudBlockBlobToFileDto(CloudBlob blob)
        {
            return new FileDto()
            {
                Name = Path.GetFileNameWithoutExtension(blob.Uri.LocalPath),
                Extension = Path.GetExtension(blob.Uri.LocalPath),
                LocalPath = blob.Uri.LocalPath,
                AbsolutePath = this.utilityService.GetAppSetting("MediaEndpoint") + blob.Uri.LocalPath,
                Size = blob.Properties.Length,
                CreationTime = blob.Properties.LastModified.GetValueOrDefault().DateTime,
                UseAzureStorage = true,
                HeavyLoadingFileSize = this.utilityService.GetAppSetting("MediaHeavyLoadingKbSize").ToInt()
            };
        }

        private DirectoryDto CloudBlobDirectoryToDirectoryDto(CloudBlobDirectory cloudBlobDirectory)
        {
            return new DirectoryDto()
            {
                UseAzureStorage = true,
                Name = GetDirectoryName(cloudBlobDirectory.Uri.LocalPath),
                LocalPath = cloudBlobDirectory.Uri.LocalPath,
                Size = 0,
                CreationTime = DateTime.Now,
                Files = null,
                Directories = null,
                Parent = null
            };
        }

        private static string GetFileContentType(string extension)
        {
            string contentType;

            switch (extension.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                case ".JPG":
                case ".JPEG":
                    contentType = "image/jpeg";
                    break;
                case ".png":
                    contentType = "image/png";
                    break;
                case ".gif":
                    contentType = "image/gif";
                    break;
                case ".svg":
                    contentType = "image/svg+xml";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                default:
                    contentType = "application/octet-stream";
                    break;
            }

            return contentType;
        }

        private CloudBlobContainer GetBlobContainer(string connectionString, string containerName)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("The parameter 'connectionString' cannot be null or empty.");
            }

            if (string.IsNullOrEmpty(containerName))
            {
                throw new NullReferenceException("The parameter 'containerName' cannot be null or empty.");
            }

            var storageAccount = CloudStorageAccount.Parse(connectionString);

            var blobClient = storageAccount.CreateCloudBlobClient();

            return blobClient.GetContainerReference(containerName);
        }

        private CloudBlobContainer GetBlobContainer(string containerName)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new NullReferenceException("The parameter 'containerName' cannot be null or empty.");
            }

            var connectionString = this.utilityService.GetAppSetting("AzureStorageConnectionString");

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("'AzureStorageConnectionString' setting not found in Web.config.");
            }

            return this.GetBlobContainer(connectionString, containerName);
        }

        private void RecursiveSearch(string localPath, string keywordFilter, ICollection<FileDto> filesDto, ICollection<DirectoryDto> directoriesDto)
        {
            var blobs = this.GetCloudBlobDirectory(localPath).ListBlobs();

            foreach (var blob in blobs)
            {
                switch (blob)
                {
                    case CloudBlockBlob file:
                        {
                            var fileName = file.Name.Contains("/") ? file.Name.Substring(file.Name.LastIndexOf("/") + 1) : file.Name;

                            if (fileName.EndsWith(".info") || !fileName.ContainsCaseInsensitive(keywordFilter))
                            {
                                continue;
                            }

                            filesDto.Add(this.CloudBlockBlobToFileDto(file));

                            break;
                        }
                    case CloudBlobDirectory directory:
                        {
                            var currentDir = directory.Uri.LocalPath.Substring(
                                directory.Uri.LocalPath.Substring(0, directory.Uri.LocalPath.LastIndexOf("/", StringComparison.InvariantCulture))
                                    .LastIndexOf("/", StringComparison.InvariantCulture) + 1);

                            if (currentDir.ContainsCaseInsensitive(keywordFilter))
                            {
                                directoriesDto.Add(this.CloudBlobDirectoryToDirectoryDto(directory));
                            }

                            this.RecursiveSearch(blob.Uri.LocalPath, keywordFilter, filesDto, directoriesDto);

                            break;
                        }
                }
            }
        }

        // Currently not in use, but keep it just in case:

        //private static CloudBlobContainer CreateBlobContainer(string containerName)
        //{
        //    var container = GetBlobContainer(containerName);

        //    container.CreateIfNotExists();

        //    container.SetPermissions(new BlobContainerPermissions
        //    {
        //        PublicAccess = BlobContainerPublicAccessType.Container
        //    });

        //    return container;
        //}
    }
}