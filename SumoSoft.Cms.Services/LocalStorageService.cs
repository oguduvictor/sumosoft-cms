﻿namespace SumoSoft.Cms.Services
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Web;
    using JetBrains.Annotations;
    using SumoSoft.Cms.Constants;
    using SumoSoft.Cms.Dto;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.Services.Dto;
    using SumoSoft.Cms.Services.Interfaces;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <inheritdoc />
    public class LocalStorageService : ILocalStorageService
    {
        private readonly IBitmapService bitmapService;
        private readonly IUtilityService utilityService;

        /// <inheritdoc />
        public LocalStorageService(
            IBitmapService bitmapService,
            IUtilityService utilityService)
        {
            this.bitmapService = bitmapService;
            this.utilityService = utilityService;
        }

        /// <inheritdoc />
        [CanBeNull]
        public virtual FileDto GetFileDto(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                return null;
            }

            var file = this.GetFileInfoFromFullName(fullName);

            return this.FileInfoToFileDto(file);
        }


        /// <inheritdoc />
        public virtual DirectoryDto GetDirectoryDto(string fullName = "", string keywordFilter = "")
        {
            var rootFolder = this.GetDirectoryInfoFromFullName(SharedConstants.UploadPath);

            keywordFilter = keywordFilter?.Trim();

            DirectoryInfo currentDirectory;

            if (string.IsNullOrEmpty(fullName))
            {
                if (!rootFolder.Exists)
                {
                    rootFolder.Create();
                }

                currentDirectory = rootFolder;
            }
            else
            {
                currentDirectory = this.GetDirectoryInfoFromFullName(fullName);
            }

            var localPath = GetLocalPath(currentDirectory.FullName);

            var files = string.IsNullOrEmpty(keywordFilter) ? currentDirectory.GetFiles() : currentDirectory.GetFiles($"*{keywordFilter}", SearchOption.AllDirectories);

            return new DirectoryDto
            {
                UseAzureStorage = false,
                LocalPath = localPath,
                CreationTime = currentDirectory.CreationTime,
                Name = currentDirectory.Name,
                Parent = new DirectoryDto
                {
                    LocalPath = GetLocalPath(currentDirectory.Parent?.FullName)
                },
                Files = files.Select(this.FileInfoToFileDto).ToList(),
                Directories = (!string.IsNullOrEmpty(keywordFilter) ? currentDirectory.GetDirectories($"*{keywordFilter}*", SearchOption.AllDirectories) : currentDirectory.GetDirectories()).Select(x => new DirectoryDto
                {
                    UseAzureStorage = false,
                    Name = x.Name,
                    LocalPath = GetLocalPath(x.FullName),
                    Size = 0,
                    CreationTime = x.CreationTime
                }).ToList(),
                IsRoot = localPath.AsNotNull().TrimEnd('/').EndsWith("samples") || localPath.AsNotNull().TrimEnd('/').EndsWith(SharedConstants.UploadFolder)
            };
        }

        /// <inheritdoc />
        public virtual FormResponse RenameFile(string fullName, string newName)
        {
            var formResponse = new FormResponse();

            try
            {
                var name = Path.GetFileNameWithoutExtension(newName);

                var directoryName = Path.GetDirectoryName(fullName);

                if (directoryName == null)
                {
                    formResponse.Errors.Add(new FormError($"Directory: {fullName} could not be found"));
                    return formResponse;
                }

                var extension = Path.GetExtension(fullName);

                var newFullName = GetServerPath(Path.Combine(directoryName, $"{name}.{extension?.Substring(1)}"));

                if (File.Exists(newFullName))
                {
                    File.Delete(newFullName);
                }

                File.Move(GetServerPath(fullName), newFullName);
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse DeleteFile(string fullName)
        {
            var formResponse = new FormResponse();

            try
            {
                var file = this.GetFileInfoFromFullName(fullName);

                if (!file.Exists)
                {
                    formResponse.Errors.Add(new FormError($"File: {fullName} does not exist"));
                }
                else
                {
                    file.Delete();
                }
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse CreateDirectory(string currentDirectory, string newDirectoryName)
        {
            var formResponse = new FormResponse();

            try
            {
                var directory = this.GetDirectoryInfoFromFullName(Path.Combine(currentDirectory, newDirectoryName));

                if (!directory.Exists)
                {
                    directory.Create();
                }
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse DeleteDirectory(string fullName)
        {
            var formResponse = new FormResponse();

            try
            {
                var directory = this.GetDirectoryInfoFromFullName(fullName);

                if (!directory.Exists)
                {
                    formResponse.Errors.Add(new FormError($"Directory: {fullName} does not exist"));
                }
                else
                {
                    directory.Delete(true);
                }
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual void BackupDirectory(string localPath, string backupFileName)
        {
            var directory = GetServerPath(localPath);

            this.CreateZipFileFromDirectory(directory, HttpContext.Current.Server.MapPath($"~/App_Data/{backupFileName}"));
        }

        /// <inheritdoc />
        [NotNull]
        public virtual FormResponse UploadFile(string fullName, Stream stream, string fileName)
        {
            var formResponse = new FormResponse();

            try
            {
                using (var fileStream = File.Create(GetServerPath(Path.Combine(fullName, fileName))))
                {
                    stream.CopyTo(fileStream);
                }
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual FormResponse ResizeFile(string fullName, ResizeFileDto model)
        {
            var formResponse = new FormResponse();

            try
            {
                var extension = Path.GetExtension(fullName) ?? string.Empty;

                var newPath = fullName.Replace(extension, "") + $"_{model.Width}{extension}";

                if (!string.IsNullOrEmpty(model.Destination))
                {
                    newPath = GetServerPath(Path.Combine(model.Destination, Path.GetFileName(newPath)));
                }

                var imageStream = new MemoryStream(File.ReadAllBytes(GetServerPath(fullName)));

                this.bitmapService.ResizeImage(model.Compression, imageStream, model.Width, newPath);

                if (model.KeepOriginal)
                {
                    return formResponse;
                }

                this.DeleteFile(fullName);

                var oldImgName = Path.GetFileName(fullName);

                this.RenameFile(newPath, oldImgName);

                return formResponse;
            }
            catch (Exception e)
            {
                formResponse.Errors.Add(new FormError(e.Message));
            }

            return formResponse;
        }

        /// <inheritdoc />
        public virtual void CreateZipFileFromDirectory(string sourceDirectoryName, string destinationArchiveFileName)
        {
            if (string.IsNullOrEmpty(sourceDirectoryName))
            {
                throw new ArgumentNullException(sourceDirectoryName);
            }
            if (string.IsNullOrEmpty(destinationArchiveFileName))
            {
                throw new ArgumentNullException(destinationArchiveFileName);
            }

            var filesToAdd = Directory.GetFiles(sourceDirectoryName, "*", SearchOption.AllDirectories);
            var entryNames = this.GetEntryNames(filesToAdd, sourceDirectoryName, true);
            using (var zipFileStream = new FileStream(destinationArchiveFileName, FileMode.Create))
            {
                using (var archive = new ZipArchive(zipFileStream, ZipArchiveMode.Create))
                {
                    for (int i = 0; i < filesToAdd.Length; i++)
                    {
                        // Add the following condition to do filtering:
                        if (filesToAdd[i].Contains($@"{SharedConstants.UploadPathIndex}\{SharedConstants.BackupsFolderName}"))
                        {
                            continue;
                        }

                        archive.CreateEntryFromFile(filesToAdd[i], entryNames[i], CompressionLevel.Fastest);
                    }
                }
            }
        }

        // ---------------------------------------------------------------------------------------------------------
        // PRIVATE METHODS
        // ---------------------------------------------------------------------------------------------------------

        private static string GetLocalPath(string fullName)
        {
            if (HttpContext.Current.Request.PhysicalApplicationPath != null)
            {
                return @"/" + fullName.Replace(HttpContext.Current.Request.PhysicalApplicationPath, string.Empty).Replace('\\', '/');
            }

            return fullName;
        }

        private static string GetServerPath(string fullName)
        {
            if (HttpContext.Current.Request.PhysicalApplicationPath != null)
            {
                return !fullName.Contains(HttpContext.Current.Request.PhysicalApplicationPath) ? HttpContext.Current.Server.MapPath(fullName) : fullName;
            }

            return fullName;
        }

        private FileInfo GetFileInfoFromFullName(string fullName)
        {
            var serverPath = GetServerPath(fullName);

            return new FileInfo(serverPath);
        }

        private DirectoryInfo GetDirectoryInfoFromFullName(string fullName)
        {
            return new DirectoryInfo(GetServerPath(fullName));
        }

        private FileDto FileInfoToFileDto(FileInfo file)
        {
            return new FileDto()
            {
                Name = file.Name.SubstringUpToFirst(file.Extension),
                Extension = file.Extension,
                LocalPath = GetLocalPath(file.FullName),
                AbsolutePath = this.utilityService.GetDomainUrl() + GetLocalPath(file.FullName),
                Size = file.Length,
                CreationTime = file.CreationTime,
                UseAzureStorage = false,
                HeavyLoadingFileSize = this.utilityService.GetAppSetting("MediaHeavyLoadingKbSize").ToInt()
            };
        }

        private string[] GetEntryNames(string[] names, string sourceFolder, bool includeBaseName)
        {
            if (names == null || names.Length == 0)
            {
                return new string[0];
            }

            if (includeBaseName)
            {
                sourceFolder = Path.GetDirectoryName(sourceFolder);
            }

            int length = string.IsNullOrEmpty(sourceFolder) ? 0 : sourceFolder.Length;
            if (length > 0 && sourceFolder != null && sourceFolder[length - 1] != Path.DirectorySeparatorChar && sourceFolder[length - 1] != Path.AltDirectorySeparatorChar)
            {
                length++;
            }

            var result = new string[names.Length];
            for (int i = 0; i < names.Length; i++)
            {
                result[i] = names[i].Substring(length);
            }

            return result;
        }

    }
}