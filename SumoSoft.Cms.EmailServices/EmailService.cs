﻿namespace SumoSoft.Cms.EmailServices
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Threading;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using SumoSoft.Cms.CountryServices.Interfaces;
    using SumoSoft.Cms.DataAccess;
    using SumoSoft.Cms.Domain;
    using SumoSoft.Cms.Domain.Extensions;
    using SumoSoft.Cms.EmailServices.Interfaces;
    using SumoSoft.Cms.Extensions;
    using SumoSoft.Cms.UtilityServices.Interfaces;

    /// <inheritdoc />
    public class EmailService : IEmailService
    {
        private readonly ICountryService countryService;
        private readonly IUtilityService utilityService;

        /// <inheritdoc />
        public EmailService(
            ICountryService countryService,
            IUtilityService utilityService)
        {
            this.countryService = countryService;
            this.utilityService = utilityService;
        }

        /// <inheritdoc />
        public Dictionary<string, Action<CmsDbContext, string, List<string>, object>> EmailActions { get; set; } = new Dictionary<string, Action<CmsDbContext, string, List<string>, object>>();

        /// <inheritdoc />
        public void ExecuteEmailAction(CmsDbContext dbContext, string emailName, List<string> to, object data = null)
        {
            this.EmailActions.TryGetValue(emailName, out var action);

            if (action == null)
            {
                throw new Exception($"Send Email Action not found for '{emailName}'");
            }

            action(dbContext, emailName, to, data);
        }

        /// <inheritdoc />
        public virtual void SendSingleEmail(CmsDbContext dbContext, string emailName, Dictionary<string, string> substitutions, List<string> to, Stream attachment = null, string languageCode = null)
        {
            languageCode = languageCode ?? this.countryService.GetCurrentLanguageCode();

            var email = dbContext.Emails.First(x => x.Name == emailName);

            var content = email.ContentSection.GetLocalizedContent(languageCode);

            var emailSubject = content.ParseJson().Subject as string;
            var emailBody = this.RenderEmailView(email.ViewName, content, languageCode);

            emailSubject = this.PerformSubstitutions(emailSubject, substitutions);
            emailBody = this.PerformSubstitutions(emailBody, substitutions);

            var cmsSettings = dbContext.CmsSettings.First();

            using (var message = new MailMessage())
            {
                foreach (var emailAddress in to)
                {
                    message.To.Add(emailAddress);
                }

                var from = email.GetLocalizedFrom(languageCode).IfNullOrEmptyConvertTo(cmsSettings.SmtpEmail);
                var displayName = email.GetLocalizedDisplayName(languageCode).IfNullOrEmptyConvertTo(cmsSettings.SmtpDisplayName);
                var replyTo = email.GetLocalizedReplyTo(languageCode);

                message.From = new MailAddress(from, displayName);
                message.Subject = emailSubject;
                message.Body = emailBody;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;

                if (!string.IsNullOrEmpty(replyTo))
                {
                    message.ReplyToList.Add(replyTo);
                }

                if (attachment != null)
                {
                    message.Attachments.Add(new Attachment(attachment, MediaTypeNames.Application.Octet));
                }

                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Host = cmsSettings.SmtpHost ?? string.Empty;
                    smtpClient.Port = cmsSettings.SmtpPort;
                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(cmsSettings.SmtpEmail, this.utilityService.Decrypt(cmsSettings.SmtpPassword));

                    smtpClient.Send(message);
                }
            }
        }

        /// <inheritdoc />
        public virtual void SendMultipleEmails(CmsDbContext dbContext, string emailName, List<Dictionary<string, string>> substitutions, List<string> to, Stream attachment = null, string languageCode = null)
        {
            foreach (var emailAddress in to)
            {
                var index = to.IndexOf(emailAddress);

                this.SendSingleEmail(dbContext, emailName, substitutions.ElementAt(index), emailAddress.InList(), attachment, languageCode);
            }
        }

        /// <inheritdoc />
        public virtual string PerformSubstitutions(string text, Dictionary<string, string> substitutions)
        {
            if (substitutions == null)
            {
                return text;
            }

            foreach (var substitution in substitutions)
            {
                text = text.Replace(substitution.Key, substitution.Value);
            }

            return text;
        }

        /// <inheritdoc />
        public virtual string RenderEmailView(string viewName, object model, string languageCode = null)
        {
            using (var stringWriter = new StringWriter())
            {
                var cultureInfo = CultureInfo.CreateSpecificCulture(languageCode ?? this.countryService.GetCurrentLanguageCode());

                Thread.CurrentThread.CurrentCulture = cultureInfo;
                Thread.CurrentThread.CurrentUICulture = cultureInfo;

                var controller = new FakeController();
                var httpContext = HttpContext.Current ?? new HttpContext(new HttpRequest(null, "http://www.url.com", null), new HttpResponse(stringWriter));
                var wrapper = new HttpContextWrapper(httpContext);
                var routeData = new RouteData();
                routeData.Values.Add("controller", controller.GetType().Name.ToLower().Replace("controller", string.Empty));
                controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);
                var context = controller.ControllerContext;
                var viewEngineResult = ViewEngines.Engines.FindView(context, $"~/Views/Emails/{viewName}.cshtml", null);
                var view = viewEngineResult.View;
                context.Controller.ViewData.Model = model;
                var viewContext = new ViewContext(context, view, context.Controller.ViewData, context.Controller.TempData, stringWriter);
                view.Render(viewContext, stringWriter);

                return stringWriter.ToString();
            }
        }

        /// <inheritdoc />
        public virtual void AddSentEmail(CmsDbContext dbContext, string emailName, string to, string from = null, string reference = null, Guid? orderId = null, Guid? couponId = null, Guid? cartId = null)
        {
            dbContext.SentEmails.Add(new SentEmail
            {
                To = to,
                From = from,
                Reference = reference,
                Email = dbContext.Emails.FirstOrDefault(x => x.Name.Equals(emailName)),
                Order = orderId == null ? null : dbContext.Orders.Find(orderId),
                Coupon = couponId == null ? null : dbContext.Coupons.Find(couponId),
                Cart = cartId == null ? null : dbContext.Carts.Find(cartId)
            });

            dbContext.SaveChanges();
        }

        private class FakeController : Controller
        {
        }
    }
}
