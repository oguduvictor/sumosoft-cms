﻿namespace SumoSoft.Cms.EmailServices.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using SumoSoft.Cms.DataAccess;

    /// <summary>
    ///
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        ///
        /// </summary>
        Dictionary<string, Action<CmsDbContext, string, List<string>, object>> EmailActions { get; set; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="emailName"></param>
        /// <param name="to"></param>
        /// <param name="data"></param>
        void ExecuteEmailAction(CmsDbContext dbContext, string emailName, List<string> to, object data = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="emailName"></param>
        /// <param name="substitutions"></param>
        /// <param name="to"></param>
        /// <param name="attachment"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        void SendSingleEmail(CmsDbContext dbContext, string emailName, Dictionary<string, string> substitutions, List<string> to, Stream attachment = null, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="emailName"></param>
        /// <param name="substitutions"></param>
        /// <param name="to"></param>
        /// <param name="attachment"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        void SendMultipleEmails(CmsDbContext dbContext, string emailName, List<Dictionary<string, string>> substitutions, List<string> to, Stream attachment = null, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="text"></param>
        /// <param name="substitutions"></param>
        /// <returns></returns>
        string PerformSubstitutions(string text, Dictionary<string, string> substitutions);

        /// <summary>
        ///
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        string RenderEmailView(string viewName, object model, string languageCode = null);

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="emailName"></param>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="reference"></param>
        /// <param name="orderId"></param>
        /// <param name="couponId"></param>
        /// <param name="cartId"></param>
        void AddSentEmail(CmsDbContext dbContext, string emailName, string to, string from = null, string reference = null, Guid? orderId = null, Guid? couponId = null, Guid? cartId = null);
    }
}
