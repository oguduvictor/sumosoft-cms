﻿namespace SumoSoft.Cms.Utilities.TsContentSectionSchemasGenerator
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using SumoSoft.Cms.Extensions;

    internal class Program
    {
        private static void Main()
        {
            var sourceFolder = ConfigurationManager.AppSettings["Source"];
            var targetFolder = ConfigurationManager.AppSettings["Target"];

            var schemas = Directory.GetFiles(sourceFolder).Where(x => x.EndsWith(".cs"));

            foreach (var schema in schemas)
            {
                Console.WriteLine("Processing " + schema);

                var allLines = File.ReadAllLines(schema);

                var className = allLines
                    .First(x => x.Contains("public class "))
                    .Replace("public class", "")
                    .Trim();

                var members = allLines.Where(x =>
                    x.Contains("public string") ||
                    x.Contains("public List<string>") ||
                    x.Contains("public List<List<string>>"));

                using (var streamWriter = File.CreateText($"{targetFolder}I{className}.ts"))
                {
                    streamWriter.WriteLine($"export interface I{className} {{");

                    foreach (var member in members)
                    {
                        var propertyName = member
                            .Replace("public string", "")
                            .Replace("public List<string>", "")
                            .Replace("public List<List<string>>", "")
                            .SubstringUpToFirst("{")
                            .Trim();

                        var type = member.Contains("public List<string>")
                            ? "string[]"
                            : member.Contains("public List<List<string>>")
                                ? "string[][]"
                                : "string";

                        streamWriter.WriteLine($"    {propertyName.ToLowerFirstLetter()}: {type};");
                    }

                    streamWriter.Write("}");
                }
            }
        }
    }
}
